//
//  TipsViewController.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/14.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TipsViewController : UIViewController

/** 标题 */
@property(nonatomic, copy) NSString *titleString;
/** 内容 */
@property(nonatomic, copy) NSString *contentString;


@end
