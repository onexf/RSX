//
//  UIViewController+ErrorView.h
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/8/23.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (ErrorView)



/**
 显示错误页面
 */
- (void)showErrorView;

/**
 隐藏错误页面
 */
- (void)hideErrorView;

/**
 重新获取数据加载页面
 */
- (void)reloadAllData;


@end
