//
//  TipsViewController.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/14.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "TipsViewController.h"
#import <STPopup/STPopup.h>

@interface TipsViewController ()

@end

@implementation TipsViewController

{
    UITextView *_textView;
    UILabel *_titleLabel;
    UIButton *_closeButton;
}

- (instancetype)init {
    if (self = [super init]) {
        self.contentSizeInPopup = CGSizeMake(300, 400);
        self.landscapeContentSizeInPopup = CGSizeMake(400, 200);
    }
    return self;
}
- (void)dismissTipsView {
    [self.popupController dismiss];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.popupController.navigationBarHidden = YES;
    self.view.backgroundColor = COLOR_WHITE;
    
    _titleLabel = [UILabel new];
    _titleLabel.font = FONT_TITLE;
    _titleLabel.textColor = COLOR_TEXT;
    _titleLabel.text = self.titleString;
    [self.view addSubview:_titleLabel];
    
    _closeButton = [UIButton new];
    [_closeButton setImage:[UIImage imageNamed:@"close_gray"] forState:UIControlStateNormal];
    [_closeButton addTarget:self action:@selector(dismissTipsView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_closeButton];
    
    _textView = [UITextView new];
    _textView.backgroundColor = [UIColor clearColor];
    _textView.editable = NO;
    _textView.userInteractionEnabled = NO;
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 3;// 字体的行间距
    NSDictionary *attributes = @{
                                     NSFontAttributeName:[UIFont systemFontOfSize:14],
                                     NSParagraphStyleAttributeName:paragraphStyle
                                 };
    _textView.attributedText = [[NSAttributedString alloc] initWithString:self.contentString attributes:attributes];
    [self.view addSubview:_textView];
    [self.view layoutIfNeeded];
    CGFloat height = [self.contentString jk_heightWithFont:FONT_TEXT constrainedToWidth:self.view.frame.size.height - 44] * 1.3 + 111;
    self.contentSizeInPopup = CGSizeMake(300, height);
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];

    _titleLabel.frame = CGRectMake(12, 0, self.view.frame.size.width - 60, 44);
    _closeButton.frame = CGRectMake(self.view.frame.size.width - 44, 0, 44, 44);
    _textView.frame = CGRectMake(8, 44, self.view.frame.size.width - 7, self.view.frame.size.height - 44);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
