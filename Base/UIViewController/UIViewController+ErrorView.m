//
//  UIViewController+ErrorView.m
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/8/23.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "UIViewController+ErrorView.h"

#import <objc/runtime.h>
#import "NSObject+Swizzling.h"
#import <UIView+Shimmer.h>
#import "UIView+Extension.h"
#import <FDFullscreenPopGesture/UINavigationController+FDFullscreenPopGesture.h>


@interface UIViewController ()
//背景
@property (nonatomic, strong) UIView *errorBackGroundView;
/** 图片 */
@property (nonatomic, strong) UIImageView *errorImageView;
/** 文字 */
@property (nonatomic, strong) UILabel *errorLabel;
///** 导航栏 */
//@property(nonatomic, strong) UIView *titleView;


@end

static const void *kerrorBackGroundView = "errorBackGroundView";
static const void *kerrorImageView = "errorImageView";
static const void *kerrorLabel = "errorLabel";
//static const void *ktitleView = "titleView";


@implementation UIViewController (ErrorView)

//- (UIView *)titleView {
//    return objc_getAssociatedObject(self, ktitleView);
//}
//
//- (void)setTitleView:(UIView *)titleView {
//    objc_setAssociatedObject(self, ktitleView, titleView, OBJC_ASSOCIATION_RETAIN);
//}

- (UIView *)errorBackGroundView
{
    return objc_getAssociatedObject(self, kerrorBackGroundView);
}

- (void)setErrorBackGroundView:(UIView *)errorBackGroundView
{
    objc_setAssociatedObject(self, kerrorBackGroundView, errorBackGroundView, OBJC_ASSOCIATION_RETAIN);
}

- (UIImageView *)errorImageView
{
    return objc_getAssociatedObject(self, kerrorImageView);
}

- (void)setErrorImageView:(UIImageView *)errorImageView
{
    objc_setAssociatedObject(self, kerrorImageView, errorImageView, OBJC_ASSOCIATION_RETAIN);
}

- (UILabel *)errorLabel
{
    return objc_getAssociatedObject(self, kerrorLabel);
}

- (void)setErrorLabel:(UILabel *)errorLabel
{
    objc_setAssociatedObject(self, kerrorLabel, errorLabel, OBJC_ASSOCIATION_RETAIN);
}

+ (void)load
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [objc_getClass("UIViewController") swizzleSelector:@selector(viewDidLoad) withSwizzledSelector:@selector(errorViewDidLoad)];
    });
}

- (void)errorViewDidLoad
{
    [self errorViewDidLoad];
//    self.fd_prefersNavigationBarHidden = YES;
    
//    self.titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 64)];
//    self.titleView.backgroundColor = COLOR_MAIN;
//    [self.view addSubview:self.titleView];


    self.errorBackGroundView = [[UIView alloc] initWithFrame:self.view.bounds];
    self.errorBackGroundView.backgroundColor = COLOR_TBBACK;
    
    self.errorImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_littleCat"]];
    [self.errorImageView addTapAction:@selector(reloadData) target:self];
    [self.errorBackGroundView addSubview:self.errorImageView];
    [self.errorImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.errorBackGroundView);
        make.centerY.equalTo(self.errorBackGroundView).offset(-17);
    }];
    
    self.errorLabel = [[UILabel alloc] init];
    [self.errorBackGroundView addSubview:self.errorLabel];
    [self.errorLabel addTapAction:@selector(reloadData) target:self];
    self.errorLabel.textColor = COLOR_SUBTEXT;
    self.errorLabel.font = FONT_TEXT;
    self.errorLabel.text = @"网络异常，点我刷新";
    [self.errorLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.errorImageView);
        make.top.equalTo(self.errorImageView.mas_bottom).offset(10);
    }];
}

- (void)reloadData
{
    [self reloadAllData];
}
- (void)reloadAllData
{
    self.errorLabel.text = @"加载中···";
    [self.errorLabel startShimmering];
    [self.errorImageView jk_rotateByAngle:360 duration:1.5 autoreverse:NO repeatCount:MAXFLOAT timingFunction:[CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionLinear]];
}


- (void)showErrorView
{
    //重置
    self.errorLabel.text = @"网络异常，点我刷新";
    [self.errorLabel stopShimmering];
    
    [self.errorImageView jk_rotateByAngle:0 duration:0 autoreverse:NO repeatCount:0 timingFunction:nil];
    
    //先移除，再添加
    [self.errorBackGroundView removeFromSuperview];
    
    [self.view addSubview:self.errorBackGroundView];
    
    [self.errorImageView jk_shake];
    [self.errorLabel jk_shake];
}

- (void)hideErrorView
{
    [self.errorBackGroundView removeFromSuperview];
}

@end

