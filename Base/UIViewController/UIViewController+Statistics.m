//
//  UIViewController+Statistics.m
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/8/21.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "UIViewController+Statistics.h"
#import "NSObject+Swizzling.h"
#import <objc/runtime.h>
#import "Colors.h"
#import <UMMobClick/MobClick.h>

@implementation UIViewController (Statistics)


+ (void)load
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [objc_getClass("UIViewController") swizzleSelector:@selector(viewDidAppear:) withSwizzledSelector:@selector(goldCatViewDidAppear:)];
        [objc_getClass("UIViewController") swizzleSelector:@selector(viewDidDisappear:) withSwizzledSelector:@selector(goldCatViewDidDisappear:)];
//        [objc_getClass("UIViewController") swizzleSelector:@selector(viewWillAppear:) withSwizzledSelector:@selector(goldCatViewWillAppear:)];
//        [objc_getClass("UIViewController") swizzleSelector:@selector(viewWillDisappear:) withSwizzledSelector:@selector(goldCatViewWillDisappear:)];
//        [objc_getClass("UIViewController") swizzleSelector:@selector(preferredStatusBarStyle) withSwizzledSelector:@selector(goldCatpreferredStatusBarStyle)];

        
    });
}
//- (UIStatusBarStyle)goldCatpreferredStatusBarStyle {
//    if ([self.jk_className isEqualToString:@"HomeViewController"] || [self.jk_className isEqualToString:@"AllViewController"] || [self.jk_className isEqualToString:@"FindViewController"] ||[self.jk_className isEqualToString:@"MineViewController"]) {
//        return UIStatusBarStyleLightContent;
//    } return UIStatusBarStyleDefault;
//}
//- (void)goldCatViewWillDisappear:(BOOL)animated {
//    [self goldCatViewWillDisappear:animated];
//}
//- (void)goldCatViewWillAppear:(BOOL)animated {
//    [self goldCatViewWillAppear:animated];
//}

- (void)goldCatViewDidAppear:(BOOL)animated {
    [self goldCatViewDidAppear:animated];
    if (!([self.jk_className isEqualToString:@"GoldCatBankNavController"] || [self.jk_className isEqualToString:@"GoldCatBankTabBarController"] || [self.jk_className isEqualToString:@"UICompatibilityInputViewController"] || [self.jk_className isEqualToString:@"UICompatibilityInputViewController"] || [self.jk_className isEqualToString:@"WZXLaunchViewController"])) {
        [self jk_performAfter:2.0 block:^{
            [MobClick beginLogPageView:self.title];
//            NSLog(@"进入：%@", self.title);
        }];
    }
}

- (void)goldCatViewDidDisappear:(BOOL)animated {
    [self goldCatViewDidDisappear:animated];
    if (!([self.jk_className isEqualToString:@"GoldCatBankNavController"] || [self.jk_className isEqualToString:@"GoldCatBankTabBarController"] || [self.jk_className isEqualToString:@"UICompatibilityInputViewController"] || [self.jk_className isEqualToString:@"UICompatibilityInputViewController"] || [self.jk_className isEqualToString:@"WZXLaunchViewController"])) {
        [self jk_performAfter:2.0 block:^{
            [MobClick endLogPageView:self.title];
//            NSLog(@"离开：%@", self.title);
        }];
    }
}

@end
