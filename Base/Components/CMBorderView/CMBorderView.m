//
//  CMBorderView.m
//  WLTProject
//
//  Created by gonghaiwei on 14-4-3.
//  Copyright (c) 2014年 luojing. All rights reserved.
//

#import "CMBorderView.h"
@interface CMBorderView (){
    NSMutableArray *linesArray;
}
@end

@implementation CMBorderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor=[UIColor whiteColor];
    }
    return self;
}


-(void)setBorderWidth:(CMViewBorderWidth)borderWidth{
    _borderWidth=borderWidth;
    [self setNeedsDisplay];
}

-(void)setBorderColor:(UIColor *)borderColor{
    _borderColor=borderColor;
    [self setNeedsDisplay];
}

-(void)addLine:(CGPoint)start to:(CGPoint)end width:(CGFloat)width color:(UIColor *)color{
    if(linesArray==nil){
        linesArray=[NSMutableArray array];
    }
    NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:NSStringFromCGPoint(start),@"start",NSStringFromCGPoint(end),@"end",[NSNumber numberWithFloat:width],@"width",color==nil?self.borderColor:color,@"color", nil];
    [linesArray addObject:dict];
    [self setNeedsDisplay];
}

-(void)willMoveToSuperview:(UIView *)newSuperview{
    [super willMoveToSuperview:newSuperview];
    [self setNeedsDisplay];
}

-(void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    [self setNeedsDisplay];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    
    if (self.borderColor==nil)
    {
        self.borderColor=COLOR_LINE;
    }
    // Drawing code
    CGContextRef context=UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(context, self.borderColor.CGColor);
    CGContextSetLineWidth(context, 1.0);
    CGContextSetShouldAntialias(context, NO); //关闭抗锯齿，以免系统画的时候不能自定义线条宽度
    
    if(self.borderWidth.top>0){
        CGContextSetLineWidth(context, self.borderWidth.top);
        CGContextMoveToPoint(context, 0, self.borderWidth.top/2);
        CGContextAddLineToPoint(context, self.frame.size.width,self.borderWidth.top/2);
        CGContextStrokePath(context);
    }
    
    if(self.borderWidth.right>0){
        CGContextSetLineWidth(context, self.borderWidth.right);
        CGContextMoveToPoint(context, self.frame.size.width-self.borderWidth.right/2, 0);
        CGContextAddLineToPoint(context, self.frame.size.width-self.borderWidth.right/2,self.frame.size.height);
        CGContextStrokePath(context);
    }
    
    if(self.borderWidth.bottom>0){
        CGContextSetLineWidth(context, self.borderWidth.bottom);
        CGContextMoveToPoint(context, 0, self.frame.size.height-self.borderWidth.bottom/2);
        CGContextAddLineToPoint(context, self.frame.size.width,self.frame.size.height-self.borderWidth.bottom/2);
        CGContextStrokePath(context);
    }
    
    if(self.borderWidth.left>0){
        CGContextSetLineWidth(context, self.borderWidth.left);
        CGContextMoveToPoint(context, self.borderWidth.left/2, 0);
        CGContextAddLineToPoint(context, self.borderWidth.left/2,self.frame.size.height);
        CGContextStrokePath(context);
    }
    
    if(linesArray){
        for(NSDictionary *dict in linesArray){
            CGContextSetStrokeColorWithColor(context, [(UIColor *)[dict objectForKey:@"color"] CGColor]);
            CGFloat width=[(NSNumber *)[dict objectForKey:@"width"] floatValue];
            CGContextSetLineWidth(context, width);
            
            CGPoint start=CGPointFromString([dict objectForKey:@"start"]);
            CGContextMoveToPoint(context, start.x-width/2, start.y-width/2);
            
            
            
            
            CGPoint end=CGPointFromString([dict objectForKey:@"end"]);
            CGContextAddLineToPoint(context, end.x-width/2,end.y-width/2);
            
            CGContextStrokePath(context);
        }
    }
    

}


@end
