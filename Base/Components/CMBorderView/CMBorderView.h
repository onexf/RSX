//
//  CMBorderView.h
//  WLTProject
//
//  Created by gonghaiwei on 14-4-3.
//  Copyright (c) 2014年 luojing. All rights reserved.
//

#import <UIKit/UIKit.h>
struct CMViewBorderWidth {
    CGFloat top;
    CGFloat right;
    CGFloat bottom;
    CGFloat left;
};
typedef struct CMViewBorderWidth CMViewBorderWidth;

CG_INLINE CMViewBorderWidth CMViewBorderWidthMake(CGFloat top, CGFloat right, CGFloat bottom, CGFloat left)
{
    CMViewBorderWidth border;
    border.top=top;
    border.right=right;
    border.bottom=bottom;
    border.left=left;
    return border;
}

@interface CMBorderView : UIView
@property (nonatomic,assign)CMViewBorderWidth borderWidth;
@property (nonatomic,strong)UIColor *borderColor;
-(void)addLine:(CGPoint)start to:(CGPoint)end width:(CGFloat)width color:(UIColor *)color;
@end
