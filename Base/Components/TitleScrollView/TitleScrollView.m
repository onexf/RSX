//
//  TitleScrollView.m
//  GoldCatBank
//
//  Created by Sunny on 2017/8/29.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "TitleScrollView.h"

@interface TitleScrollView()
{
    NSTimer *timer;
}
@property(nonatomic,strong)UILabel *titleLab;
@property(nonatomic,strong)UILabel *copyTitleLab;
@end
@implementation TitleScrollView


-(void)dealloc
{
    
    [self distory];
}


- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {

        self.showsVerticalScrollIndicator=NO;
        
        self.scrollEnabled=NO;
        [self addSubview:self.titleLab];
        [self addSubview:self.copyTitleLab];
    }
    return self;
}


-(void)setTitle:(NSString *)title{
    _title=title;
    
    self.titleLab.text=title;
    self.copyTitleLab.text=title;
    
}

-(void)setTitleColor:(UIColor *)titleColor
{
    _titleColor=titleColor;
    self.titleLab.textColor=titleColor;
    self.copyTitleLab.textColor=titleColor;

}

-(void)setTitleFont:(UIFont *)titleFont
{
    _titleFont=titleFont;
    self.titleLab.font=titleFont;
    self.copyTitleLab.font=titleFont;

    
}
-(void)setAutoScrollTimeInterval:(CGFloat)autoScrollTimeInterval{
    _autoScrollTimeInterval=autoScrollTimeInterval;
    
    if (!timer)
    {
        
        [self setupTimer];

    }

}


-(UILabel *)titleLab{
    if (!_titleLab)
    {
        _titleLab=[[UILabel alloc]initWithFrame:CGRectZero];
        _titleLab.textAlignment=NSTextAlignmentCenter;
        _titleLab.font=FONT_NAV;
        _titleLab.textColor=UIColorFromHex(0xffffff);
    }
    return _titleLab;
}


-(UILabel *)copyTitleLab{
    if (!_copyTitleLab)
    {
        _copyTitleLab=[[UILabel alloc]initWithFrame:CGRectZero];
        _copyTitleLab.textAlignment=NSTextAlignmentCenter;
        _copyTitleLab.font=FONT_NAV;
        _copyTitleLab.textColor=UIColorFromHex(0xffffff);
    }
    return _copyTitleLab;
}

+ (instancetype)cycleScrollViewWithFrame:(CGRect)frame
{
    TitleScrollView *scrollView = [[self alloc] initWithFrame:frame];
    scrollView.contentSize=CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height*2);
    scrollView.titleLab.frame=CGRectMake(0, 0, scrollView.frame.size.width, scrollView.frame.size.height);
    scrollView.copyTitleLab.frame=CGRectMake(0,scrollView.frame.size.height, scrollView.frame.size.width, scrollView.frame.size.height);
    return scrollView;
}



#pragma mark - actions

- (void)setupTimer
{
    timer = [NSTimer scheduledTimerWithTimeInterval:self.autoScrollTimeInterval target:self selector:@selector(automaticScroll) userInfo:nil repeats:YES];

    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
}


-(void)automaticScroll
{
    [self setContentOffset:CGPointMake(0, self.frame.size.height) animated:YES];
    [self setContentOffset:CGPointMake(0, 0)];
}

- (void)suspend
{
    
    [timer setFireDate:[NSDate distantFuture]];
}

-(void)distory{
    
    [timer invalidate];
    timer = nil;

}

-(void)start{
    
    static NSInteger num=0;
    
    if (num<2)
    {
        num++;
    }
    
    if (num==1)
    {
        //因为第一次进去看不到动画，所以这样写
        [UIView animateWithDuration:0.3 animations:^{
            
            [self setContentOffset:CGPointMake(0, self.frame.size.height)];
 

            
        } completion:^(BOOL finished) {
            
            [self setContentOffset:CGPointMake(0, 0)];
        }];
        
        [self setupTimer];
    }
    else
    {
        
        if (timer)
        {
            [timer setFireDate:[NSDate distantPast]];
        }
        else
        {
            [self setupTimer];
        }
        
    }
    
    
    
    
    
}

@end
