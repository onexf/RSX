//
//  TitleScrollView.h
//  GoldCatBank
//
//  Created by Sunny on 2017/8/29.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TitleScrollView : UIScrollView
@property(nonatomic,strong)NSString *title;
@property(nonatomic,strong)UIColor *titleColor;
@property(nonatomic,strong)UIFont *titleFont;
@property(nonatomic,assign)CGFloat autoScrollTimeInterval;//默认十秒


+ (instancetype)cycleScrollViewWithFrame:(CGRect)frame;

//暂停定时器
- (void)suspend;

//开始定时器
-(void)start;


@end
