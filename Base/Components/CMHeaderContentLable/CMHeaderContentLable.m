//
//  CMHeaderContentLable.m
//  WLTProject
//
//  Created by gonghaiwei on 14-4-3.
//  Copyright (c) 2014年 luojing. All rights reserved.
//

#import "CMHeaderContentLable.h"

@implementation CMHeaderContentLable

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor=[UIColor clearColor];
//        self.contentWidth=MAXFLOAT;
    }
    return self;
}

-(void)setHeader:(NSString *)header{
    _header=header;
    [self setNeedsDisplay];
}

-(void)setContent:(NSString *)content{
    _content=content;
    [self setNeedsDisplay];
}

-(void)drawRect:(CGRect)rect{
    
    if(self.contentFont==nil){
        self.contentFont=self.headerFont;
    }
    if(self.contentTextColor==nil){
        self.contentTextColor=self.headerTextColor;
    }
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    if(self.contentSide==headerContentLable_contentSide_right){
        /*写文字*/
        CGContextSetFillColorWithColor(context, self.headerTextColor.CGColor);
        //CGSize size=[_header sizeWithFont:self.headerFont];
        
        CGSize size=[_header sizeWithAttributes:@{NSFontAttributeName:self.headerFont}];
        
        //[_header drawInRect:CGRectMake(0, (self.frame.size.height-size.height)/2, size.width, size.height) withFont:self.headerFont];
        [_header drawInRect:CGRectMake(0, (self.frame.size.height-size.height)/2, size.width, size.height) withAttributes:@{NSFontAttributeName:self.headerFont,NSForegroundColorAttributeName:self.headerTextColor}];
        CGContextSetFillColorWithColor(context, self.contentTextColor.CGColor);
        
        //size=[_content sizeWithFont:self.contentFont];
        
        size=[_content sizeWithAttributes:@{NSFontAttributeName:self.contentFont}];
        
        //[_content drawInRect:CGRectMake(self.bounds.size.width-size.width, (self.frame.size.height-size.height)/2, size.width, size.height) withFont:self.contentFont];
        
        [_content drawInRect:CGRectMake(self.bounds.size.width-size.width, (self.frame.size.height-size.height)/2, size.width, size.height)  withAttributes:@{NSFontAttributeName:self.contentFont,NSForegroundColorAttributeName:self.contentTextColor}];
        
    }else if (self.contentSide==headerContentLable_contentSide_left){
        /*写文字*/
        CGContextSetFillColorWithColor(context, self.headerTextColor.CGColor);
        //CGSize size=[_header sizeWithFont:self.headerFont];
        
        CGSize size=[_header sizeWithAttributes:@{NSFontAttributeName:self.headerFont}];
        
        //[_header drawInRect:CGRectMake(0, (self.frame.size.height-size.height)/2, size.width, size.height) withFont:self.headerFont];
        [_header drawInRect:CGRectMake(0, (self.frame.size.height-size.height)/2, size.width, size.height) withAttributes:@{NSFontAttributeName:self.headerFont,NSForegroundColorAttributeName:self.headerTextColor}];
        
        CGContextSetFillColorWithColor(context, self.contentTextColor.CGColor);
        CGFloat width=self.frame.size.width-self.contentMargin;
        
        //size=[_content sizeWithFont:self.contentFont constrainedToSize:CGSizeMake(width, MAXFLOAT)];
        CGRect sizeRect=[_content boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.contentFont} context:nil];
        
        //[_content drawInRect:CGRectMake(self.contentMargin, (self.frame.size.height-sizeRect.size.height)/2, sizeRect.size.width, sizeRect.size.height) withFont:self.contentFont];
        
        [_content drawInRect:CGRectMake(self.contentMargin, (self.frame.size.height-sizeRect.size.height)/2, sizeRect.size.width, sizeRect.size.height) withAttributes:@{NSFontAttributeName:self.contentFont,NSForegroundColorAttributeName:self.contentTextColor}];
    }
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
