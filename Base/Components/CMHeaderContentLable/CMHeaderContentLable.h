//
//  CMHeaderContentLable.h
//  WLTProject
//
//  Created by gonghaiwei on 14-4-3.
//  Copyright (c) 2014年 luojing. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum {
    headerContentLable_contentSide_right=0,
    headerContentLable_contentSide_left
}headerContentLable_contentSide;


@interface CMHeaderContentLable : UIView
@property (nonatomic,strong)NSString *header;
@property (nonatomic,strong)NSString *content;
@property (nonatomic,strong)UIColor *headerTextColor;
@property (nonatomic,strong)UIColor *contentTextColor;
@property (nonatomic,strong)UIFont *contentFont;
@property (nonatomic,strong)UIFont *headerFont;
@property (nonatomic,assign)headerContentLable_contentSide contentSide;//内容的显示位置
@property (nonatomic,assign)CGFloat contentMargin;//内容距离边界的距离，当contentSide为headerContentLable_contentSide_right的时候为距离右边的间距，当contentSide为headerContentLable_contentSide_left的时候为距离左边的间距，默认为0
@end
