//
//  NetworkSingleton.m
//  goldcatbank
//
//  Created by 高丹丹 on 2017/8/22.
//  Copyright © 2017年 高丹丹. All rights reserved.
//

#import "NetworkSingleton.h"
#import "RealReachability.h"
#import "UserData.h"
#import "UserAuthData.h"
#import "JPUSHService.h"

@implementation NetworkSingleton

+(NetworkSingleton *)sharedManager{
    static NetworkSingleton *sharedNetworkSingleton = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate,^{
        sharedNetworkSingleton = [[self alloc] init];
    });
    return sharedNetworkSingleton;
}
-(AFHTTPSessionManager *)baseHtppRequest{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setTimeoutInterval:TIMEOUT];
    //header 设置
    
    NSString *token = [[UserData instance] getUserInfo].token;
    
    [manager.requestSerializer setValue:token forHTTPHeaderField:@"Authorization"];
    
    //    [manager.requestSerializer setValue:K_PASS_IP forHTTPHeaderField:@"Host"];
    //    [manager.requestSerializer setValue:@"max-age=0" forHTTPHeaderField:@"Cache-Control"];
    //    [manager.requestSerializer setValue:@"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" forHTTPHeaderField:@"Accept"];
    //    [manager.requestSerializer setValue:@"zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3" forHTTPHeaderField:@"Accept-Language"];
    //    [manager.requestSerializer setValue:@"gzip, deflate" forHTTPHeaderField:@"Accept-Encoding"];
    //    [manager.requestSerializer setValue:@"keep-alive" forHTTPHeaderField:@"Connection"];
    //    [manager.requestSerializer setValue:@"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:35.0) Gecko/20100101 Firefox/35.0" forHTTPHeaderField:@"User-Agent"];
    
    //     设置返回格式
    // manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    //
    //    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain", @"text/html",@"application/javascript",@"application/json", nil];
    
    return manager;
}

-(void)getResultWithParameter:(NSDictionary *)parameter url:(NSString *)url successBlock:(SuccessBlock)successBlock failureBlock:(FailureBlock)failureBlock
{
//    KLog (@"%@===%@",url,parameter);
    AFHTTPSessionManager *manager = [self baseHtppRequest];
    
    NSString *urlStr =nil;
    
    if (SYSTEM_VERSION_LESS_THAN(@"9.0"))
    {
        urlStr=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }
    else
    {
        urlStr=[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    }
    
    [manager GET:urlStr
      parameters:parameter
        progress:nil
         success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
             
             if ([responseObject isKindOfClass:[NSNull class]])
             {
                 [WSProgressHUD showErrorWithStatus:@"服务器连接异常"];
                 return ;
             }
             if ([responseObject objectForKey:@"data"]) {
                 successBlock(responseObject);
//                 KLog (@"url==%@  responseObject====%@",url,responseObject);
                 
                 successBlock(responseObject);
             }
             
             

        }
         failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             NSString *errorStr =
             [NSString stringWithFormat:@"%@---%ld",[error.userInfo objectForKey:@"NSLocalizedDescription"],error.code];
             failureBlock(errorStr);
//             KLog(@"%@",error);
             // 2
             dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                 if ([GLobalRealReachability currentReachabilityStatus] == RealStatusNotReachable)
                 {
                     [WSProgressHUD showErrorWithStatus:@"网络连接失败!"];
                 }
                 
             });
             }];

}

-(void)postResultWithParameter:(NSDictionary *)parameter url:(NSString *)url successBlock:(SuccessBlock)successBlock failureBlock:(FailureBlock)failureBlock
{
//    KLog (@"%@===%@",url,parameter);
    
    AFHTTPSessionManager *manager = [self baseHtppRequest];
    
    NSString *urlStr =nil;
    
    if (SYSTEM_VERSION_LESS_THAN(@"9.0"))
    {
        urlStr=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }
    else
    {
        urlStr=[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    }
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:parameter];
    
    if ([[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleIdentifier"] isEqualToString:@"com.ShanghaiCastle.www"]) {
        dict[@"platform"] = @"ios";
    } else {
        dict[@"platform"] = @"iosmajia";
    }
    dict[@"appID"] = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleIdentifier"];
    NSString *app_Version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSInteger versionValue = app_Version.integerValue;
    dict[@"appVersion"] = @(versionValue);
    
     [manager POST:urlStr
        parameters:dict
          progress:nil
           success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
               
               
              // NSLog(@"  dict===%@responseObject=====%@",dict,responseObject);
               if ([responseObject isKindOfClass:[NSNull class]]) {
                   [WSProgressHUD showErrorWithStatus:@"服务器连接异常"];
                   return ;
               }
        
               NSInteger code = [[responseObject objectForKey:@"code"] integerValue];
               if (code == 200) {
                   
                   if ([[responseObject objectForKey:@"data"] isKindOfClass:[NSNull class]])
                   {
                       failureBlock([responseObject objectForKey:@"msg"]);
                       return;
                   }
                   successBlock([responseObject objectForKey:@"data"]);
               } else {
                   if (code == 401) {
                       UserInfoModel *model = [[UserInfoModel alloc] init];
                       [[UserData instance] updateUserInfo:model];
                       [[UserData instance] setLoginState:NO];
                       [[UserAuthData shareInstance] clearUserAuthData];
                       //注销极光推送
                       [JPUSHService setAlias:nil completion:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
//                           NSLog(@"%s:\n------%ld\n------%@\n-------%ld", __func__, (long)iResCode, iAlias, (long)seq);
                       } seq:0];

                       if ([responseObject[@"msg"] isEqualToString:@"Unauthorized"]) {
//                           NSLog(@"%@", urlStr);
//                           [LoginTool loginAction];
                           failureBlock([responseObject objectForKey:@"您的账号已在其他端登录"]);
                       }
                       failureBlock([responseObject objectForKey:@"msg"]);
                   } else {
                       failureBlock([responseObject objectForKey:@"msg"]);
                   }
               }
               
           } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
               KLog (@"接口：%@\n错误：%@", url, error);
               
//               NSString *errorStr =
//               [error.userInfo objectForKey:@"NSLocalizedDescription"];
               if (failureBlock) {
                   failureBlock(@"网络连接失败!");
               }
               // 2
               dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                   if ([GLobalRealReachability currentReachabilityStatus] == RealStatusNotReachable) {
//                       [WSProgressHUD showErrorWithStatus:@"网络连接失败!"];
                   }
                   
               });

               
           }];
    
}

-(void)upImageWithParameter:(NSDictionary *)parameter imageArray:(NSArray *)images url:(NSString *)url successBlock:(SuccessBlock)successBlock failureBlock:(FailureBlock)failureBlock
{
    
//    KLog (@"%@===%@",url,parameter);
    
    AFHTTPSessionManager *manager = [self baseHtppRequest];
    
    NSString *urlStr =nil;
    
    if (SYSTEM_VERSION_LESS_THAN(@"9.0"))
    {
        urlStr=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }
    else
    {
        urlStr=[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    }
    
    [manager POST:urlStr parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        for (int i=1; i<=images.count; i++) {
            
            NSData *imageData=UIImageJPEGRepresentation(images[i-1], 1.0);
            
            [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"image%d",i] fileName:@"image" mimeType:@"image/jpg"];
        }
        

        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        successBlock(responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
//        KLog (@"error===%@",error);
        
        NSString *errorStr =
        [error.userInfo objectForKey:@"NSLocalizedDescription"];
        if (failureBlock) {
            failureBlock(errorStr);
        }
        // 2
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if ([GLobalRealReachability currentReachabilityStatus] == RealStatusNotReachable) {
                [WSProgressHUD showErrorWithStatus:@"网络连接失败!"];
            }
            
        });

        
    }];
    
}

-(void)putResultWithParameter:(NSDictionary *)parameter url:(NSString *)url successBlock:(SuccessBlock)successBlock failureBlock:(FailureBlock)failureBlock{
    
    AFHTTPSessionManager *manager = [self baseHtppRequest];
    
    NSString *urlStr =nil;
    
    if (SYSTEM_VERSION_LESS_THAN(@"9.0"))
    {
        urlStr=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }
    else
    {
        urlStr=[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    }
    
    [manager PUT:urlStr
      parameters:parameter
         success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
             
             if ([responseObject isKindOfClass:[NSNull class]]) {
                 [WSProgressHUD showErrorWithStatus:@"服务器连接异常"];
                 return ;
             }
             successBlock(responseObject);
             
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             
             NSString *errorStr = [error.userInfo objectForKey:@"NSLocalizedDescription"];
             failureBlock(errorStr);
             // 2
             dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                 if ([GLobalRealReachability currentReachabilityStatus] == RealStatusNotReachable) {
                     
                     [WSProgressHUD showErrorWithStatus:@"网络连接失败!"];
                 }
             });

         }];
    
}
-(void)deleteResultWithParameter:(NSDictionary *)parameter url:(NSString *)url successBlock:(SuccessBlock)successBlock failureBlock:(FailureBlock)failureBlock{
    
    AFHTTPSessionManager *manager = [self baseHtppRequest];
    
    NSString *urlStr =nil;
    
    if (SYSTEM_VERSION_LESS_THAN(@"9.0"))
    {
        urlStr=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }
    else
    {
        urlStr=[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    }
    
    [manager DELETE:urlStr
         parameters:parameter
            success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    [WSProgressHUD showErrorWithStatus:@"服务器连接异常"];
                    return ;
                }
                successBlock(responseObject);

                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                
//                KLog (@"error===%@",error);
                NSString *errorStr = [error.userInfo objectForKey:@"NSLocalizedDescription"];
                failureBlock(errorStr);
                // 2
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    if ([GLobalRealReachability currentReachabilityStatus] == RealStatusNotReachable) {
                        [WSProgressHUD showErrorWithStatus:@"网络连接失败!"];
                    }
                });

            }];
    
}

/**
 参数按照ASICII码顺序，MD5签名sign最后
 */
//-(NSDictionary *)signedDicForMD5:(NSDictionary *)parameter{
//    //forKey:@"sign"]
//    NSMutableDictionary *orignDic=[NSMutableDictionary dictionaryWithDictionary:parameter];
//    
//    NSArray *keys=[[orignDic allKeys] sortedArrayUsingSelector:@selector(compare:)];//localizedCaseInsensitiveCompare  NSComparisonResult
//    //caseInsensitiveCompare比较两个字符串大小，忽略大小写 //compare	比较两个字符串大小
//    
//    NSMutableDictionary *signDic=[NSMutableDictionary dictionary];
//    for(NSString *key in keys)
//    {
//        [signDic setObject:[orignDic objectForKey:key] forKey:key];
//        
//    }
//    
//    [signDic setObject:[self Md5ignForDic:signDic andSortKeyArr:keys] forKey:@"sign"];
//    
//    return signDic;
//}
//-(NSString *)Md5ignForDic:(NSMutableDictionary *)dic andSortKeyArr:(NSArray *)keys{
//    NSString *str=@"";
//    
//    if (keys.count>0)
//    {
//        for (NSString *key in keys) {
//            if (![key isEqualToString:keys[keys.count-1]]) {
//                str=[NSString stringWithFormat:@"%@%@=%@&",str,key,[dic objectForKey:key]];
//            }
//            else{
//                
//                if (keys.count==1) {
//                    str=[NSString stringWithFormat:@"%@=%@&key=%@",key,[dic objectForKey:key],MD5KEYVALUES];
//                }
//                else
//                {
//                    str=[NSString stringWithFormat:@"%@%@=%@&key=%@",str,key,[dic objectForKey:key],MD5KEYVALUES];
//                }
//            }
//            
//        }
//    }
//    else
//    {
//        str=[NSString stringWithFormat:@"key=%@",MD5KEYVALUES];
//    }
//    
//    
//    NSString *signStr=[str md5String];
//    return signStr;
//}


@end
