//
//  GoldCatBankTabBarController.m
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/8/21.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "GoldCatBankTabBarController.h"

#import "GoldCatBankNavController.h"
#import "Colors.h"

#import "HomeViewController.h"
#import "AllViewController.h"
#import "FindViewController.h"
#import "MineViewController.h"

@interface GoldCatBankTabBarController ()

@end

@implementation GoldCatBankTabBarController



+ (void)initialize
{
    NSMutableDictionary *attrs = [NSMutableDictionary dictionary];
    attrs[NSFontAttributeName] = [UIFont systemFontOfSize:12];
    attrs[NSForegroundColorAttributeName] = COLOR_SUBTEXT;
    
    NSMutableDictionary *selectedAttrs = [NSMutableDictionary dictionary];
    selectedAttrs[NSFontAttributeName] = attrs[NSFontAttributeName];
    selectedAttrs[NSForegroundColorAttributeName] = COLOR_MAIN;
    
    UITabBarItem *item = [UITabBarItem appearance];
    [item setTitleTextAttributes:attrs forState:UIControlStateNormal];
    [item setTitleTextAttributes:selectedAttrs forState:UIControlStateSelected];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // 添加子控制器
    [self setupChildVc:[[HomeViewController alloc] init] title:@"首页" image:@"home_normal" selectedImage:@"home_highlight"];
    [self setupChildVc:[[AllViewController alloc] init] title:@"商城" image:@"all_normal" selectedImage:@"all_highlight"];
    [self setupChildVc:[[FindViewController alloc] init] title:@"商铺" image:@"find_normal" selectedImage:@"find_highlight"];
    [self setupChildVc:[[MineViewController alloc] init] title:@"我的" image:@"mine_normal" selectedImage:@"mine_highlight"];
}

- (void)setupChildVc:(UIViewController *)vc title:(NSString *)title image:(NSString *)image selectedImage:(NSString *)selectedImage
{
    vc.navigationItem.title = title;
    vc.tabBarItem.title = title;
    vc.tabBarItem.image = [UIImage imageNamed:image];
    vc.tabBarItem.selectedImage = [UIImage imageNamed:selectedImage];
    GoldCatBankNavController *nav = [[GoldCatBankNavController alloc] initWithRootViewController:vc];
    [self addChildViewController:nav];
}


@end
