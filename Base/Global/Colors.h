//
//  Colors.h
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/8/21.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#ifndef Colors_h
#define Colors_h


#define RGBA(r,g,b,a)            [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]

#define UIColorFromHex(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


//主色调
#define COLOR_MAIN               RGBA(234, 149, 62, 1)//UIColorFromHex(0xfabf13)
//白色
#define COLOR_WHITE              [UIColor whiteColor]
//背景色
#define COLOR_TBBACK             UIColorFromHex(0xF5F5F5)
//分割线颜色
#define COLOR_LINE               UIColorFromHex(0xededed)
//文字黑色
#define COLOR_TEXT               UIColorFromHex(0x212121)
//文字灰色
#define COLOR_SUBTEXT            UIColorFromHex(0x808080)
//文字红色(背景色)
#define COLOR_Red                UIColorFromHex(0xFF3F16)
//按钮默认颜色
#define COLOR_BTN_NORMAL         UIColorFromHex(0xF89721)
//按钮点击颜色
#define COLOR_BTN_SELECT         UIColorFromHex(0xc1c1c1)
//占位文字颜色
#define COLOR_PLACEHOUDER        UIColorFromHex(0xc1c1c1)
//金黄色文字
#define COLOR_GLODTEXT           UIColorFromHex(0xd59a3d)
//金黄色背景
#define COLOR_GLODBACKGROUD      RGBA(234, 149, 62, 1)
//按钮不可点击
#define COLOR_BUTTON_DISABLE     [UIColor lightGrayColor]
//
#define COLOR_MAIN_BLUE          UIColorFromHex(0x1674f6)//RGBA(153, 156, 240, 1)



//边距、高度
#define LEFT_RIGHT_MARGIN        12.0f
#define NORMAL_BUTTON_HEIGHT     45.0f
#define SEP_LINE_HEIGHT          1.0f
#define NORMAL_CELL_HEIGHT       44.0f


#endif /* Colors_h */
