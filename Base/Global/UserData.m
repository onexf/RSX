//
//  UserData.m
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/8/22.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "UserData.h"
#import <MJExtension/MJExtension.h>



@interface UserInfoModel ()

@end

@implementation UserInfoModel



@end



NSString * const userInfoKey =  @"userInfo";
NSString * const loginDataKey = @"loginData";


@interface UserData ()


@end

@implementation UserData

+ (instancetype)instance {
    static UserData *user = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        user = [[UserData alloc] init];
        user.loginState = [[NSUserDefaults standardUserDefaults] boolForKey:@"isLogin"];
    });
    return user;
}

- (instancetype)init {
    if (self = [super init]) {
        [self loadUserData];
    }
    return self;
}

- (void)loadUserData {
    @synchronized(self) {
        if (userInfo) {
            return ;
        }
        NSDictionary *info = [[NSUserDefaults standardUserDefaults]objectForKey:userInfoKey];
        if (info) {
            userInfo = [UserInfoModel mj_objectWithKeyValues:info];
        }
    }
}


- (UserInfoModel *)getUserInfo {
    return userInfo;
}

- (void)setLoginState:(BOOL)loginState{
    _loginState = loginState;
    [[NSUserDefaults standardUserDefaults] setBool:loginState forKey:@"isLogin"];
    [[NSUserDefaults standardUserDefaults] synchronize];
//    if (self.loginStateChanged) {
//        self.loginStateChanged();
//    }
}

- (BOOL)getLoginState{
    if (_loginState) {
        return _loginState;
    }
    return [[NSUserDefaults standardUserDefaults]boolForKey:@"isLogin"];
}

BOOL isLogin(){
    return [[UserData instance] getLoginState] && ([[UserData instance] getUserInfo].userId.length > 0);
}

- (void)save{
    @synchronized(self) {
        if (userInfo) {
            [[NSUserDefaults standardUserDefaults] setObject:[userInfo mj_keyValues] forKey:userInfoKey];
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(void)clearLoginData {
    @synchronized(self) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:loginDataKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}


- (void)updateUserInfo:(UserInfoModel *)user {
    @synchronized(self) {
        userInfo = user;
        [self save];
    }
}

- (void)refreshUserInfo {
    NSString *registrationID = [[NSUserDefaults standardUserDefaults] valueForKey:@"registrationID"];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    dict[@"userId"] = self.getUserInfo.userId;
    dict[@"registrationID"] = registrationID;

}

- (void)refreshUserInfoResponse:(void(^)(UserInfoModel *userInfo, NSString *errMsg))response
{
    NSString *registrationID = [[NSUserDefaults standardUserDefaults] valueForKey:@"registrationID"];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    dict[@"userId"] = self.getUserInfo.userId;
    dict[@"registrationID"] = registrationID;
    
}



@end
