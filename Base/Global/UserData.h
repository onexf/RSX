//
//  UserData.h
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/8/22.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserInfoModel : NSObject

/** 用户id */
@property(nonatomic, copy) NSString *userId;
/** token */
@property(nonatomic, copy) NSString *token;
/** 邀请码 */
@property(nonatomic, copy) NSString *inviteCode;
/** 用户手机号 */
@property(nonatomic, copy) NSString *cellphone;
/** 用户手机号隐藏中间 */
@property(nonatomic, copy) NSString *cellphoneHide;

@end

@interface UserData : NSObject
{
    UserInfoModel *userInfo;
}
/** 登录状态改变 */
//@property (nonatomic, copy) void (^loginStateChanged)();
/** 登录状态 */
@property (nonatomic, assign) BOOL loginState;

/** 单例 */
+ (instancetype)instance;

/**
 获取用户信息

 @return 用户信息模型
 */
- (UserInfoModel *)getUserInfo;

/**
 登录状态

 @return 是否登录
 */
BOOL isLogin();

/**
 *  更新用户信息
 *
 *  @param user <#user description#>
 */
- (void)updateUserInfo:(UserInfoModel *)user;

/**
 *  从服务器更新用户信息到本地
 */
- (void)refreshUserInfo;

- (void)refreshUserInfoResponse:(void(^)(UserInfoModel *userInfo, NSString *errMsg))response;

@end
