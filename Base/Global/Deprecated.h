//
//  Deprecated.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/11/10.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#ifndef Deprecated_h
#define Deprecated_h

#define Deprecated(instead) NS_DEPRECATED(2_0, 2_0, 2_0, 2_0, instead)

#endif /* Deprecated_h */
