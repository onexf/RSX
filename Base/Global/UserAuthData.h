//
//  UserAuthData.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/8/30.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserAuthDataModel : NSObject

/** 账户 */
@property(nonatomic, copy) NSString *userName;
/** 银行卡绑定手机号 */
@property(nonatomic, copy) NSString *mobile;
/** 真实姓名 */
@property(nonatomic, copy) NSString *realName;
/** 身份证号 */
@property(nonatomic, copy) NSString *idCard;
/** 银行卡 */
@property(nonatomic, copy) NSString *bankCard;
/** 是否已经认证（0：未认证；1：已认证）*/
@property(nonatomic, copy) NSString *isAuthentication;
/** 邀请码 */
@property(nonatomic, copy) NSString *inviteCode;
/** 邀请人 */
@property(nonatomic, copy) NSString *invitedBy;
/** 银行码 */
@property(nonatomic, copy) NSString *bankCode;
/** 城市区号 */
@property(nonatomic, copy) NSString *cityCode;
/** 银行卡名字  */
@property(nonatomic, copy) NSString *bankName;
/** 成功邀请好友数量  */
@property(nonatomic, copy) NSString *inviteCount;
/** 邀请好友的奖励金 */
@property(nonatomic, copy) NSString *inviteReward;
/** 优惠券个数 */
@property(nonatomic, copy) NSString *couponCount;
/**是否有交易密码密码*/
@property(nonatomic, copy) NSString *hasTradePwd;
/** 银行卡号后四位 */
@property(nonatomic, copy) NSString *bankCardLastNum;

@end



@interface UserAuthData : NSObject

+ (instancetype)shareInstance;

/** 用户认证数据 */
@property(nonatomic, strong) UserAuthDataModel *userAuthData;
///** 是否已认证 */
//@property(assign) BOOL isAuthed;

/**
 更新用户绑定信息
 */
- (void)refreshUserAuthData;
/**
 删除用户绑定信息
 */
- (void)clearUserAuthData;

/**
 更新最新的用户绑定信息

 @param response 获取到的最新的用户绑定信息
 */
- (void)refreshUserInfoResponse:(void(^)(UserAuthDataModel *userAuthData, NSString *errMsg))response;



@end
