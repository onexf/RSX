//
//  Url.h
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/8/24.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#ifndef Url_h
#define Url_h

#ifdef  DEBUG //121.40.201.35:6060   114.55.234.160:6060

#define ISDEBUG YES

//测服
#define HOST               @"https://rsxapi.zhaojinmao.cn"
//#define HOST               @"https://apiv3.zhaojinmao.cn"

#else

#define ISDEBUG NO
//正服
//#define HOST               @"https://api.zhaojinmao.cn"
#define HOST               @"https://apiv3.zhaojinmao.cn"


#endif


//接口拼接
#define URLString(URI)     [NSString stringWithFormat:@"%@%@",HOST,URI]




#endif /* Url_h */
