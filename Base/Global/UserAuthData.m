//
//  UserAuthData.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/8/30.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "UserAuthData.h"
#import "NetworkSingleton.h"

@implementation UserAuthDataModel

- (NSString *)bankCardLastNum {
    return [self.bankCard substringWithRange:NSMakeRange(self.bankCard.length - 4, 4)];
}

@end

#define UrlUserAuthData            URLString(@"/api/Users/UserDetail")

@implementation UserAuthData


+ (instancetype)shareInstance {
    static UserAuthData *userAuthData = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        userAuthData = [[UserAuthData alloc] init];
    });
    return userAuthData;
}

- (void)refreshUserAuthData {
    if (isLogin()) {
        __weak typeof(self) weakSelf = self;
        [[NetworkSingleton sharedManager] postResultWithParameter:nil url:UrlUserAuthData successBlock:^(id responseBody) {
            weakSelf.userAuthData = [UserAuthDataModel mj_objectWithKeyValues:responseBody];
        } failureBlock:^(NSString *error) {
            NSLog(@"%@", error);
        }];
    }
}

- (void)clearUserAuthData {
    self.userAuthData = nil;
}

- (void)refreshUserInfoResponse:(void(^)(UserAuthDataModel *userAuthData, NSString *errMsg))response {
    __weak typeof(self) weakSelf = self;
    [[NetworkSingleton sharedManager] postResultWithParameter:nil url:UrlUserAuthData successBlock:^(id responseBody) {
        if (isLogin()) {
            UserAuthDataModel *newModel = [UserAuthDataModel mj_objectWithKeyValues:responseBody];
            weakSelf.userAuthData = newModel;
            response(newModel, nil);
        } else {
            response(nil, @"您的账号已在其他端登录");
            [WSProgressHUD showErrorWithStatus:@"您的账号已在其他端登录"];
        }
    } failureBlock:^(NSString *error) {
        response(nil, error);
        [WSProgressHUD showErrorWithStatus:error];
    }];
}

@end
