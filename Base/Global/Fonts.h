//
//  Fonts.h
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/8/21.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#ifndef Fonts_h
#define Fonts_h

#pragma mark - font
//设置字体宏
#define FONT(size)              [UIFont systemFontOfSize:size]
#define FONT_CUS(fontName,s)    [UIFont fontWithName:fontName size:s]
#define FONT_BOLD(size)         [UIFont boldSystemFontOfSize:size]

#define FONT_NAV                 FONT_BOLD(16)
#define FONT_SUBTITLE            FONT_BOLD(15)

//普通按钮字体
#define FONT_BUTTON_TEXT         FONT(18)
//标题
#define FONT_TITLE               FONT(16)
//一般字体
#define FONT_TEXT                FONT(14)
//小号字体
#define FONT_SUBTEXT             FONT(12)
#define FONT_MINTEXT             FONT(10)


#endif /* Fonts_h */
