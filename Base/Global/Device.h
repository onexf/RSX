//
//  Device.h
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/8/21.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#ifndef Device_h
#define Device_h

//打印日志
#ifdef DEBUG
#define KLog( s, ... ) printf("class: <%p %s:(%d) > method: %s \n%s\n", self, [[[NSString stringWithUTF8String:__FILE__] lastPathComponent] UTF8String], __LINE__, __PRETTY_FUNCTION__, [[NSString stringWithFormat:(s), ##__VA_ARGS__] UTF8String] )

#else
#define KLog( s, ... )
#endif

//系统是否是iOS10
#define IS_IOS10 (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_9_x_Max) ? (YES) : (NO)


//4.7英寸屏
#define IS_IPHONE6  ([UIScreen mainScreen].bounds.size.height == 667)
//4.0英寸屏
#define IS_IPHONE5  ([UIScreen mainScreen].bounds.size.height == 568)
//5.5英寸屏
#define IS_IPHONE6P  ([UIScreen mainScreen].bounds.size.height == 736)
#define IS_IPHONEX  ([UIScreen mainScreen].bounds.size.height == 812)

//屏幕宽度
#define SCREEN_WIDTH            [UIScreen mainScreen].bounds.size.width
//屏幕宽度
#define SCREEN_HEIGHT           [UIScreen mainScreen].bounds.size.height
#define TabBarHeight 49
#define NavBarHeight 64

#define NavBarH      (IS_IPHONEX ? 88 : 64)

#define titleY      (IS_IPHONEX ? 44 : 20)

//本地存储 NSUserDefaults
#define UD [NSUserDefaults standardUserDefaults]
#define UDSave(object,key)                                                                                                 \
({                                                                                                                                             \
NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];                                                                          \
[defaults setObject:object forKey:key];                                                                                                    \
[defaults synchronize];                                                                                                                    \
})

#define UDGET(key)  [[NSUserDefaults standardUserDefaults] objectForKey:key]


#pragma mark--app版本号及 ios系统版本号间的比较

#define APP_VERSION [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]

//增加几个版本比较的宏，  用法：if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
#define SYSTEM_VERSION_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)

//大于所填版本
#define SYSTEM_VERSION_GREATER_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)

//小于所填版本
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

//大于等于所填版本
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

//小于于等于所填版本
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)


#endif /* Device_h */
