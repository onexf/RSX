//
//  NSString+Validate.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/10/12.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Validate)

/**
 *  是否是手机号码
 *
 *  @param mobile 手机号string
 *
 *  @return 是否是手机号
 */
BOOL validateMobile(NSString *mobile);

@end
