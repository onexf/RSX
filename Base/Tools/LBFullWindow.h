//
//  LBFullWindow.h
//  LBHunter
//
//  Created by wxf on 2017/4/20.
//  Copyright © 2017年 person. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LBFullWindow : NSObject


+ (void)showImage:(UIImage *)image text:(NSString *)text subText:(NSString *)subText showCloseButton:(BOOL)show;


@end
