//
//  CTMediator+JPushActions.m
//  LBHunter
//
//  Created by wxf on 2017/3/23.
//  Copyright © 2017年 person. All rights reserved.
//

#import "CTMediator+JPushActions.h"
#import "CBWebViewController.h"
#import "CouponsViewController.h"
#import "MessageViewController.h"
#import "BalanceTradeVC.h"

@implementation CTMediator (JPushActions)

- (void)routToJPushViewControllerWithParams:(NSDictionary *)params response:(void(^)(UIViewController *pushToViewContrller))response
{
    NSString *targetType = [[params allKeys] containsObject:@"type"] ? params[@"type"] : nil;
    if ([targetType isEqualToString:@"active"] || [targetType isEqualToString:@"other"] || [targetType isEqualToString:@"product"]) {
        response (nil);
    }
    if ([targetType isEqualToString:@"invited"]) {
        CBWebViewController *webViewController = [[CBWebViewController alloc] init];
        webViewController.urlString = @"https://www.zhaojinmao.cn/testwebapp/index/Inviting/Inviting.html";
        webViewController.showShareButton = YES;
        response(webViewController);
    } else {
        if (isLogin()) {
            if ([targetType isEqualToString:@"coupon"]) {
                CouponsViewController *vc = [[CouponsViewController alloc] init];
                response(vc);
            }
            if ([targetType isEqualToString:@"notice"]) {
                MessageViewController *messageViewController = [[MessageViewController alloc] init];
                response(messageViewController);
            }
            if ([targetType isEqualToString:@"order"]) {
                BalanceTradeVC *vc = [[BalanceTradeVC alloc] init];
                response(vc);
            }
        } else {
            response(nil);
        }
    }
    
}


@end
