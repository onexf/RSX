//
//  UIView+Extension.h
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/8/23.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Extension)


/** 判断一个控件是否真正显示在主窗口 */
- (BOOL)isShowingOnKeyWindow;

/** 添加tap */
- (void)addTapAction:(SEL)tapAction target:(id)target;


- (UIView *)firstResponseView;

- (void)cancelFirstResponse;


@end
