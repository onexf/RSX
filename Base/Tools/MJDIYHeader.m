//
//  MJDIYHeader.m
//  MJRefreshExample
//
//  Created by MJ Lee on 15/6/13.
//  Copyright © 2015年 小码哥. All rights reserved.
//

#import "MJDIYHeader.h"

@interface MJDIYHeader()
@property (weak, nonatomic) UILabel *label;
@property (weak, nonatomic) UIImageView *logo;
/** 随机文案 */
@property(nonatomic, strong) NSArray *strings;
/** 文案 */
@property(nonatomic, strong) NSString *currentString;

@end

@implementation MJDIYHeader

/**
 懒加载
 
 @return 文案
 */
- (NSArray *)strings {
    if (!_strings) {
        _strings = @[@"门槛超低的黄金投资", @"黄金理财专家", @"买金更便宜", @"黄金产品双重收益", @"黄金理财，上招金猫", @"存金提金超便捷", @"您身边的黄金银行"];
        
    }
    return _strings;
}
#pragma mark - 重写方法
#pragma mark 在这里做一些初始化配置（比如添加子控件）
- (void)prepare {
    [super prepare];
    // 设置控件的高度
    self.mj_h = 70;
    
    // 添加label
    UILabel *label = [[UILabel alloc] init];
    label.textColor = COLOR_SUBTEXT;
    label.font = FONT_TEXT;
    label.textAlignment = NSTextAlignmentCenter;
    [self addSubview:label];
    self.label = label;
    // logo
    UIImageView *logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_header_normal"]];
    logo.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:logo];
    self.logo = logo;
}

#pragma mark 在这里设置子控件的位置和尺寸
- (void)placeSubviews {
    [super placeSubviews];
    [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(self).offset(10);
    }];
    [self.logo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.label.mas_bottom).offset(15);
        make.centerX.equalTo(self);
        make.width.height.equalTo(@16);
    }];
}

#pragma mark 监听scrollView的contentOffset改变
- (void)scrollViewContentOffsetDidChange:(NSDictionary *)change {
    [super scrollViewContentOffsetDidChange:change];
}

#pragma mark 监听scrollView的contentSize改变
- (void)scrollViewContentSizeDidChange:(NSDictionary *)change {
    [super scrollViewContentSizeDidChange:change];
}

#pragma mark 监听scrollView的拖拽状态改变
- (void)scrollViewPanStateDidChange:(NSDictionary *)change {
    [super scrollViewPanStateDidChange:change];
}

#pragma mark 监听控件的刷新状态
- (void)setState:(MJRefreshState)state {
    MJRefreshCheckState;
    
    switch (state) {
        case MJRefreshStateIdle:
            self.currentString = self.currentString.length > 0 ? self.currentString : self.strings[arc4random() % 6];
            self.label.text = self.currentString;
            self.logo.image = [UIImage imageNamed:@"icon_header_done"];
            break;
        case MJRefreshStatePulling:
            self.currentString = self.strings[arc4random() % 7];
            self.label.text = self.currentString;
            self.logo.image = [UIImage imageNamed:@"icon_header_normal"];
            break;
        case MJRefreshStateRefreshing:
            self.label.text = self.currentString;
            self.logo.image = [UIImage imageNamed:@"icon_header_loding"];
            break;
            
        default:
            break;
    }
}

#pragma mark 监听拖拽比例（控件被拖出来的比例）
- (void)setPullingPercent:(CGFloat)pullingPercent {
    [super setPullingPercent:pullingPercent];
}

@end
