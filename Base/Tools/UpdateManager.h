//
//  UpdateManager.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/15.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkSingleton.h"


@interface UpdateManager : NSObject

+ (instancetype)instance;
/**
 *  检查是否需要更新
 */
- (void)checkIfNeedUpdate;

@end


@interface NetworkSingleton (Update)

+ (void)update_getVersionDataResponse:(void(^)(NSDictionary *dict, NSString *errMsg))response;


@end
