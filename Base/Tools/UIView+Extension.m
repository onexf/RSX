//
//  UIView+Extension.m
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/8/23.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "UIView+Extension.h"

@implementation UIView (Extension)

- (BOOL)isShowingOnKeyWindow
{
    // 主窗口
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    
    // 以主窗口左上角为坐标原点, 计算self的矩形框
    CGRect newFrame = [keyWindow convertRect:self.frame fromView:self.superview];
    CGRect winBounds = keyWindow.bounds;
    
    // 主窗口的bounds 和 self的矩形框 是否有重叠
    BOOL intersects = CGRectIntersectsRect(newFrame, winBounds);
    
    return !self.isHidden && self.alpha > 0.01 && self.window == keyWindow && intersects;
}

- (void)addTapAction:(SEL)tapAction target:(id)target {
    self.userInteractionEnabled = YES;
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:target action:tapAction];
    [self addGestureRecognizer:gesture];
}


- (UIView *)firstResponseView {
    if (self.isFirstResponder) {
        return self;
    }
    for (UIView *subView in self.subviews) {
        UIView *firstResponse = [subView firstResponseView];
        if (firstResponse != nil) {
            return firstResponse;
        }
    }
    return nil;
}

- (void)cancelFirstResponse {
    UIView *view = [self firstResponseView];
    if (view) {
        [view resignFirstResponder];
    }
}

@end
