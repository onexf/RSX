//
//  TipsTool.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/14.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TipsTool : NSObject



+ (void)showTipsWithTitle:(NSString *)title andContent:(NSString *)content;

@end
