//
//  AppDelegate+YJPushDelegate.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/15.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "AppDelegate+YJPushDelegate.h"
#import "GoldCatBankNavController.h"
#import "GoldCatBankTabBarController.h"
#import "JPUSHService.h"
#import "CTMediator+JPushActions.h"
#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#import <UserNotifications/UserNotifications.h>
#endif

@implementation AppDelegate (YJPushDelegate)

//接受到token
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    [JPUSHService registerDeviceToken:deviceToken];
    
    NSString *regester = [JPUSHService registrationID];
    
    [[NSUserDefaults standardUserDefaults] setValue:regester forKey:@"registrationID"];
    
    if ([self.pushTokenFromApple isEqualToString:regester]) {
        return;
    }
    else{
        self.pushTokenFromApple = regester;
        [self performSelector:@selector(sendRegisterID) withObject:nil afterDelay:1];
    }
    
    NSString* dvsToken =[[[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""];
    //    NSLog(@"--------------deviceToken:%@",dvsToken);
    
    [[NSUserDefaults standardUserDefaults] setValue:dvsToken forKey:@"taobaoToken"];
    if (!dvsToken ) {
        dvsToken = @"获取token失败";
    }
}
//收到通知
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    // Required,For systems with less than or equal to iOS6
    [JPUSHService handleRemoteNotification:userInfo];
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    if (application.applicationState == UIApplicationStateActive) {
        
    } else {
        // IOS 7 Support Required
        [JPUSHService handleRemoteNotification:userInfo];
        completionHandler(UIBackgroundFetchResultNewData);
        NSString *targetType = [[userInfo allKeys] containsObject:@"type"] ? userInfo[@"type"] : nil;
        if (!targetType) {
            return;
        }
        NSString *systemVersion = [UIDevice jk_systemVersion];
        if (systemVersion.floatValue < 8) {
            //
        } else {
            if (self.showNewFeature) {
            } else {
                GoldCatBankTabBarController *tabbarController = (GoldCatBankTabBarController *)[UIApplication sharedApplication].keyWindow.rootViewController;
                GoldCatBankNavController *theNav = tabbarController.selectedViewController;
                if (theNav.childViewControllers.count > 1) {
                    [theNav popToRootViewControllerAnimated:NO];
                }
                NSMutableDictionary *dict= [NSMutableDictionary dictionaryWithDictionary:userInfo];
                [[CTMediator sharedInstance] routToJPushViewControllerWithParams:dict response:^(UIViewController *pushToViewContrller) {
                    if (pushToViewContrller) {
                        [self jk_performAfter:1.0 block:^{
                            if (isLogin()) {
                                [theNav pushViewController:pushToViewContrller animated:YES];
                            }
                        }];
                    }
                }];
            }
            }
    }
}
//注册失败
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    if (isLogin()) {
        //Optional
    }
    NSLog(@"did Fail To Register For Remote Notifications With Error: %@", error);
}
- (void)sendRegisterID {
    NSString *regester = [JPUSHService registrationID];
    if (regester.length < 2) {
        [self performSelector:_cmd withObject:nil afterDelay:5];
    } else {
        if (isLogin()) {
            [[UserData instance] refreshUserInfo];
        }
    }
}

+ (void)testReceiveMessageWithType:(NSString *)type {
}

- (NSString *)pushTokenFromApple {
    return objc_getAssociatedObject(self, "pushTokenFromApple");
}

- (void)setPushTokenFromApple:(NSString *)pushTokenFromApple {
    objc_setAssociatedObject(self, "pushTokenFromApple", pushTokenFromApple, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
