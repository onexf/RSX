//
//  VerifyTradeNumViewController.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/10/19.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VerifyTool.h"
@class PayModel;
@interface VerifyTradeNumViewController : UIViewController
@property (nonatomic, copy) void (^response)(BOOL result, NSString *errMsg);
@property (nonatomic, copy) void (^payResponse)(BOOL result, PayModel *payModel, NSString *errMsg);

/** 交易类型 */
@property(assign) VerifyType tradeType;
/** amount */
@property(nonatomic, copy) NSString *tradeAmount;
/** 订单信息 */
@property(nonatomic, copy) NSDictionary *orderDict;


@end
