//
//  VerifyTool.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/10/19.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "VerifyTool.h"
#import "VerifyTradeNumViewController.h"
#import "GoldCatBankNavController.h"
#import "PayModel.h"
#import "PriceTool.h"
@implementation VerifyTool

+ (void)verifyTradeNumWithType:(VerifyType)type amount:(NSString *)amount response:(void(^)(BOOL result, NSString *errMsg))response {
    VerifyTradeNumViewController *verVC = [[VerifyTradeNumViewController alloc] init];
    verVC.tradeType = type;
    verVC.tradeAmount = [PriceTool numberPointHandle:amount withRoundingStyle:NSRoundDown decimalNum:2];
    verVC.response = ^(BOOL result, NSString *errMsg) {
        response(result , errMsg);
    };
    GoldCatBankNavController *nav = [[GoldCatBankNavController alloc] initWithRootViewController:verVC];
    nav.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:nav animated:YES completion:nil];
}

+ (void)verifyTradeNumWithType:(VerifyType)type amount:(NSString *)amount parameter:(NSDictionary *)parameter response:(void(^)(BOOL result, PayModel *payModel, NSString *errMsg))response {
    VerifyTradeNumViewController *verVC = [[VerifyTradeNumViewController alloc] init];
    verVC.tradeType = type;
    verVC.tradeAmount = [PriceTool numberPointHandle:amount withRoundingStyle:NSRoundDown decimalNum:2];
    verVC.orderDict = parameter;
    verVC.payResponse = ^(BOOL result, PayModel *payModel, NSString *errMsg) {
        response(result , payModel, errMsg);
    };
    GoldCatBankNavController *nav = [[GoldCatBankNavController alloc] initWithRootViewController:verVC];
    nav.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:nav animated:YES completion:nil];
}
@end
