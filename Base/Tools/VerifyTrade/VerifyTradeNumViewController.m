//
//  VerifyTradeNumViewController.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/10/19.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "VerifyTradeNumViewController.h"
#import <FDFullscreenPopGesture/UINavigationController+FDFullscreenPopGesture.h>
#import "UIView+Extension.h"
#import "UIView+YSTextInputKeyboard.h"
#import "AllNetWorkRquest.h"
#import "ResetTradePWDViewController.h"
#import "UserAuthData.h"
#import "JKCountDownButton.h"
#import "WithdrawalsPayModel.h"
#import "PayModel.h"

@interface VerifyTradeNumViewController ()<UITextFieldDelegate>
/** 灰色背景 */
@property(nonatomic, strong) UIView *topView;

/** 交易密码 */
@property(nonatomic, strong) UIView *bottomViewLeft;
/** close */
@property(nonatomic, strong) UIButton *closeImage;
/** 验证密码 */
@property(nonatomic, strong) UILabel *titleLabel;
/** line */
@property(nonatomic, strong) UIView *line;
/** 交易密码 */
@property(nonatomic, strong) UITextField *pwdField;
/** 银行卡支付 subttext */
@property(nonatomic, strong) UILabel *subTextLabel;
/** 按钮 */
@property(nonatomic, strong) UIButton *comitBtn;

/** 验证手机验证码 */
@property(nonatomic, strong) UIView *bottomViewRight;
/** backArrowImage */
@property(nonatomic, strong) UIButton *backArrowImage;
/** rightTitleLabel */
@property(nonatomic, strong) UILabel *rightTitleLabel;
/** 短信验证码发送到 */
@property(nonatomic, strong) UILabel *sendCodeLabel;
/** 短信验证码 */
@property(nonatomic, strong) UITextField *codeTextField;
/** 获取验证码 */
@property(nonatomic, strong) JKCountDownButton *getCodeBtn;
/** 银行卡支付 */
@property(nonatomic, strong) UILabel *subTextLabelRight;
/** 确认 */
@property(nonatomic, strong) UIButton *comitBtnRight;
/** 订单号 */
@property(nonatomic, copy) NSString *orderId;
/** payModel */
@property(nonatomic, strong) PayModel *payModel;

@end

@implementation VerifyTradeNumViewController

- (void)dealOtherAction:(NSDictionary *)dic {
    __weak typeof(self) weakSelf = self;
    [WSProgressHUD showWithMaskType:WSProgressHUDMaskTypeBlack];
    [AllNetWorkRquest goPayWithPayData:dic successBlock:^(id responseBody) {
        [WSProgressHUD dismiss];
        if (responseBody[@"errCode"]) {
            if ([responseBody[@"errCode"] integerValue] >= 0) {
                
                switch (weakSelf.tradeType) {
                    case VerifyTypeUnionPayCharge://充值
                    {
                        //设置订单号
                        WithdrawalsPayModel *mod = [WithdrawalsPayModel mj_objectWithKeyValues:responseBody];
                        weakSelf.orderId = mod.transSerialNumber;
                        
                        [weakSelf.codeTextField becomeFirstResponder];
                        weakSelf.bottomViewLeft.hidden = YES;
                        weakSelf.bottomViewRight.hidden = NO;
                        
                        weakSelf.getCodeBtn.enabled = NO;
                        [weakSelf.getCodeBtn startCountDownWithSecond:59];
                        [weakSelf.getCodeBtn countDownChanging:^NSString *(JKCountDownButton *countDownButton,NSUInteger second) {
                            NSString *title = [NSString stringWithFormat:@"%zds后重发",second];
                            return title;
                        }];
                    }
                        
                        break;
                    case VerifyTypeWithdraw://提现
                    {
                        if (weakSelf.response) {
                            weakSelf.response(YES, responseBody[@"errMsg"]);
                        }
                        [weakSelf viewDismissAction];
                    }
                        break;
                        
                    default:
                        break;
                }
                
            } else {
                if (weakSelf.tradeType == VerifyTypeWithdraw) {
                    weakSelf.response(NO, responseBody[@"errMsg"]);
                } else {
                    [WSProgressHUD showErrorWithStatus:responseBody[@"errMsg"]];
                }
            }
        }
    } failureBlock:^(NSString *error) {
        [WSProgressHUD dismiss];
        if (weakSelf.tradeType == VerifyTypeWithdraw) {
            weakSelf.response(NO, error);
        } else {
            [WSProgressHUD showErrorWithStatus:error];
        }
    }];

}
/**
 处理购买流程
 */
- (void)dealBuyAction {
    __weak typeof(self) weakSelf = self;
    [WSProgressHUD showWithMaskType:WSProgressHUDMaskTypeBlack];
    [AllNetWorkRquest  goPayBalanceAndBankWithPayData:self.orderDict successBlock:^(id responseBody) {
        [WSProgressHUD dismiss];
        //payWay (integer, optional): 支付方式 0：余额；1：银行卡；2：活动；3：余额加银行卡； ,
        //如果是银行卡支付
        if ([responseBody[@"errCode"] integerValue] >= 0) {
            weakSelf.payModel = [PayModel mj_objectWithKeyValues:responseBody];
            
            if (weakSelf.payModel.payWay.integerValue == 1 || weakSelf.payModel.payWay.integerValue == 3) {
                // 银行卡支付或余额银行卡支付
                weakSelf.orderId = responseBody[@"orderNo"];
                [weakSelf.codeTextField becomeFirstResponder];
                weakSelf.bottomViewLeft.hidden = YES;
                weakSelf.bottomViewRight.hidden = NO;
                
                weakSelf.getCodeBtn.enabled = NO;
                [weakSelf.getCodeBtn startCountDownWithSecond:59];
                [weakSelf.getCodeBtn countDownChanging:^NSString *(JKCountDownButton *countDownButton,NSUInteger second) {
                    NSString *title = [NSString stringWithFormat:@"%zds后重发",second];
                    return title;
                }];
            } else {
                //余额或活动
                if (weakSelf.payResponse) {
                    weakSelf.payResponse(YES, weakSelf.payModel,responseBody[@"errMsg"]);
                }
                [weakSelf viewDismissAction];
            }
        } else {//失败
            if (weakSelf.payResponse) {
                weakSelf.payResponse(NO, nil, responseBody[@"errMsg"]);
            }
            [weakSelf viewDismissAction];
        }
    } failureBlock:^(NSString *error) {
        [WSProgressHUD dismiss];
        if (weakSelf.payResponse) {
            weakSelf.payResponse(NO, nil, error);
        }
        [weakSelf viewDismissAction];
    }];
}
/**
 提交验证码
 */
- (void)comitCode {
    NSString *errMsg = nil;
    if (self.codeTextField.text.length <= 0) {
        errMsg = @"请输入短信验证码";
    }
    if (errMsg) {
        [WSProgressHUD showErrorWithStatus:errMsg];
        return;
    }
    NSDictionary *dic = @{
                              @"verCD" : self.codeTextField.text,
                              @"orderId" : self.orderId
                          };
    __weak typeof(self) weakSelf = self;
    [WSProgressHUD showWithMaskType:WSProgressHUDMaskTypeBlack];
    [AllNetWorkRquest goPayByBankCarWithPayData:dic successBlock:^(id responseBody) {
        [WSProgressHUD dismiss];
        if (responseBody[@"errCode"]) {
            if ([responseBody[@"errCode"] integerValue] >= 0) {
                if (weakSelf.tradeType == VerifyTypeUnionPayBuy) {
                    if (weakSelf.payResponse) {
                        weakSelf.payResponse(YES, weakSelf.payModel, responseBody[@"errMsg"]);
                    }
                } else {
                    if (weakSelf.response) {
                        weakSelf.response(YES, responseBody[@"errMsg"]);
                    }
                }
                [weakSelf viewDismissAction];
            } else {
                if (weakSelf.tradeType == VerifyTypeUnionPayCharge) {
                    if (responseBody[@"errMsg"]) {
                        if ([responseBody[@"errMsg"] isEqualToString:@"订单支付中，请勿重复支付"]) {
                            [WSProgressHUD showErrorWithStatus:@"验证码错误，请重新下单"];
                            [weakSelf viewDismissAction];
                        } else {
                            [WSProgressHUD showErrorWithStatus:responseBody[@"errMsg"]];
                        }
                    }
                } else if (weakSelf.tradeType == VerifyTypeUnionPayBuy){
                    if (weakSelf.payResponse) {
                        weakSelf.payResponse(NO, weakSelf.payModel, responseBody[@"errMsg"]);
                    }
                    [weakSelf viewDismissAction];
                } else {
                    if (responseBody[@"errMsg"]) {
                        if (weakSelf.response) {
                            weakSelf.response(NO, responseBody[@"errMsg"]);
                        }
                        [weakSelf viewDismissAction];
                    }
                }
            }
        }
    } failureBlock:^(NSString *error) {
        [WSProgressHUD dismiss];
        if (weakSelf.tradeType == VerifyTypeUnionPayCharge) {
            [WSProgressHUD showErrorWithStatus:error];
        } else {
            if (weakSelf.response) {
                weakSelf.response(NO, error);
            }
        }
    }];
}

/**
 提交支付密码
 */
- (void)comitPwd {
    [self.view endEditing:YES];
    NSDictionary *dic = @{@"pwd" : self.pwdField.text};
    __weak typeof(self) weakSelf = self;
    [WSProgressHUD showWithMaskType:WSProgressHUDMaskTypeBlack];
    [AllNetWorkRquest VerifytradePassword:dic successBlock:^(id responseBody) {
        [WSProgressHUD dismiss];
        if ([responseBody[@"results"] integerValue] == 1) {
            if (weakSelf.tradeType == VerifyTypeUnionPayCharge || weakSelf.tradeType == VerifyTypeUnionPayBuy || weakSelf.tradeType == VerifyTypeWithdraw) {//需要银行卡付款
                [weakSelf.getCodeBtn sendActionsForControlEvents:UIControlEventTouchUpInside];
            } else {
                if (weakSelf.tradeType == VerifyTypeBalancePay) {
                    [weakSelf dealBuyAction];
                } else {
                    if (weakSelf.response) {
                        weakSelf.response(YES, responseBody[@"errMsg"]);
                    }
                }
                [weakSelf viewDismissAction];
            }
        } else {
            weakSelf.pwdField.text = @"";
            weakSelf.comitBtn.enabled = NO;
            [weakSelf verVerifytradePasswordFail:responseBody];
        }
    } failureBlock:^(NSString *error) {
        [WSProgressHUD dismiss];
        [WSProgressHUD showErrorWithStatus:error];
    }];
}

/**
 支付密码错误

 @param dic 错误信息
 */
- (void)verVerifytradePasswordFail:(NSDictionary *)dic {
    if ([dic[@"leftCount"] integerValue] <= 0) {
        [WSProgressHUD showErrorWithStatus:dic[@"errMsg"]];
        return;
    }
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"交易密码错误，还可以再输入 %@次",dic[@"leftCount"]]
                                                                             message:@""
                                                                      preferredStyle:UIAlertControllerStyleAlert ];
    //添加取消到UIAlertController
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"忘记密码" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        ResetTradePWDViewController *resetPWD = [[ResetTradePWDViewController alloc] init];
        //resetPWD.shouldPopToRoot = YES;
        [self.navigationController pushViewController:resetPWD animated:YES];
    }];
    [alertController addAction:cancelAction];
    UIAlertAction *OKAction = [UIAlertAction actionWithTitle:@"重新输入" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.pwdField becomeFirstResponder];
    }];
    [alertController addAction:OKAction];
    [self presentViewController:alertController animated:YES completion:nil];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == self.pwdField) {
        if (textField.secureTextEntry) {
            [textField insertText:self.pwdField.text];
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == self.pwdField) {
        if (textField.text.length == 5 && ![string isEqualToString:@""]){
            self.comitBtn.enabled = YES;
            return YES;
        } else if (textField.text.length == 6) {
            if ([string isEqualToString:@""]) {
                self.comitBtn.enabled = NO;
                return YES;
            } else {
                return NO;
            }
            return YES;
        }
        return YES;
    } return YES;
}

- (void)viewDismissAction {
    [self.pwdField cancelFirstResponse];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.fd_prefersNavigationBarHidden = YES;
    self.view.backgroundColor = RGBA(255, 255, 255, 0.5);
    [self.view addSubview:self.topView];
    [self.view addSubview:self.bottomViewLeft];
    
    if (self.tradeType == VerifyTypeUnionPayCharge || self.tradeType == VerifyTypeUnionPayBuy) {
        [self.view addSubview:self.bottomViewRight];
        self.bottomViewRight.hidden = YES;
    }
    
    [self layOut];
    
    [self setUpText];
}

/**
 设置内容
 */
- (void)setUpText {
    if (self.tradeType == VerifyTypeUnionPayCharge || self.tradeType == VerifyTypeUnionPayBuy) {
        NSString *titleStr = [NSString stringWithFormat:@"在线支付￥%@", self.tradeAmount];
        NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:titleStr];
        [attrStr addAttribute:NSForegroundColorAttributeName
                        value:COLOR_TEXT
                        range:NSMakeRange(0, 4)];
        self.rightTitleLabel.attributedText = attrStr;
        UserAuthDataModel *userModel=[[UserAuthData shareInstance] userAuthData];
        self.subTextLabelRight.text = [NSString stringWithFormat:@"使用%@（%@）付款", userModel.bankName, userModel.bankCardLastNum];
    }
    self.subTextLabel.hidden = !(self.tradeType == VerifyTypeUnionPayCharge || self.tradeType == VerifyTypeUnionPayBuy || self.tradeType == VerifyTypeWithdraw);
    switch (self.tradeType) {
        case VerifyTypeUnionPayCharge://银行卡支付
        case VerifyTypeUnionPayBuy:
            {
                NSString *titleStr = [NSString stringWithFormat:@"在线支付￥%@", self.tradeAmount];
                NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:titleStr];
                [attrStr addAttribute:NSForegroundColorAttributeName
                                value:COLOR_TEXT
                                range:NSMakeRange(0, 4)];
                self.titleLabel.attributedText = attrStr;
                UserAuthDataModel *userModel=[[UserAuthData shareInstance] userAuthData];
                self.subTextLabel.text = [NSString stringWithFormat:@"使用%@（%@）付款", userModel.bankName, userModel.bankCardLastNum];
            }
            break;
        case VerifyTypeBalancePay://余额支付
        {
            NSString *titleStr = [NSString stringWithFormat:@"余额支付￥%@", self.tradeAmount];
            NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:titleStr];
            [attrStr addAttribute:NSForegroundColorAttributeName
                            value:COLOR_TEXT
                            range:NSMakeRange(0, 4)];
            self.titleLabel.attributedText = attrStr;
        }
            break;
        case VerifyTypeWithdraw://提现
        {
            NSString *titleStr = [NSString stringWithFormat:@"提现金额￥%@", self.tradeAmount];
            NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:titleStr];
            [attrStr addAttribute:NSForegroundColorAttributeName
                            value:COLOR_TEXT
                            range:NSMakeRange(0, 4)];
            self.titleLabel.attributedText = attrStr;
            self.subTextLabel.text = @"手续费：5.00元";
        }
            break;
        case VerifyTypeOthe://卖金、赎回
        {
            
        }
            break;
        default:
            break;
    }
}
- (void)layOut {
    [self.bottomViewLeft mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.height.equalTo(@210);
    }];
    [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.bottom.equalTo(self.bottomViewLeft.mas_top);
    }];
    [self.closeImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(self.bottomViewLeft).offset(LEFT_RIGHT_MARGIN);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.closeImage);
        make.centerX.equalTo(self.bottomViewLeft);
    }];
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(10);
        make.left.right.equalTo(self.bottomViewLeft);
        make.height.equalTo(@1);
    }];
    [self.pwdField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.line.mas_bottom).offset(25);
        make.left.equalTo(self.bottomViewLeft).offset(LEFT_RIGHT_MARGIN);
        make.right.equalTo(self.bottomViewLeft).offset(-LEFT_RIGHT_MARGIN);
        make.height.equalTo(@(NORMAL_CELL_HEIGHT));
    }];
    [self.comitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.bottomViewLeft).offset(LEFT_RIGHT_MARGIN);
        make.right.bottom.equalTo(self.bottomViewLeft).offset(-LEFT_RIGHT_MARGIN);
        make.height.equalTo(@(NORMAL_BUTTON_HEIGHT));
    }];
    [self.subTextLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.pwdField.mas_bottom).offset(13);
        make.left.equalTo(self.closeImage);
    }];
    

    if (self.tradeType == VerifyTypeUnionPayCharge || self.tradeType == VerifyTypeUnionPayBuy) {
        [self.bottomViewRight mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.left.equalTo(self.view);
//            make.left.equalTo(self.view.mas_right);
            make.height.width.equalTo(self.bottomViewLeft);
        }];
        [self.backArrowImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.equalTo(self.bottomViewRight).offset(LEFT_RIGHT_MARGIN);
        }];
        [self.rightTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.backArrowImage);
            make.centerX.equalTo(self.bottomViewRight);
        }];
        [self.sendCodeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.rightTitleLabel.mas_bottom).offset(7);
            make.centerX.equalTo(self.rightTitleLabel);
        }];
        [self.getCodeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.sendCodeLabel.mas_bottom).offset(9);
            make.right.equalTo(self.bottomViewRight).offset(-LEFT_RIGHT_MARGIN);
            make.height.equalTo(@(NORMAL_CELL_HEIGHT));
            make.width.equalTo(@76);
        }];
        [self.codeTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bottomViewRight).offset(LEFT_RIGHT_MARGIN);
            make.top.bottom.equalTo(self.getCodeBtn);
            make.right.equalTo(self.getCodeBtn.mas_left);
        }];
        [self.subTextLabelRight mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.codeTextField.mas_bottom).offset(13);
            make.left.equalTo(self.backArrowImage);
        }];
        [self.comitBtnRight mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bottomViewRight).offset(LEFT_RIGHT_MARGIN);
            make.right.bottom.equalTo(self.bottomViewRight).offset(-LEFT_RIGHT_MARGIN);
            make.height.equalTo(@(NORMAL_BUTTON_HEIGHT));
        }];
    }
    
    
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.pwdField becomeFirstResponder];
}
/**
 懒加载
 
 @return 短信验证码
 */
- (UIView *)bottomViewRight {
    if (!_bottomViewRight) {
        _bottomViewRight = [[UIView alloc] init];
        _bottomViewRight.backgroundColor = COLOR_WHITE;
        [_bottomViewRight addSubview:self.backArrowImage];
        [_bottomViewRight addSubview:self.rightTitleLabel];
        [_bottomViewRight addSubview:self.sendCodeLabel];
        [_bottomViewRight addSubview:self.codeTextField];
        [_bottomViewRight addSubview:self.getCodeBtn];
        [_bottomViewRight addSubview:self.subTextLabelRight];
        [_bottomViewRight addSubview:self.comitBtnRight];
    }
    return _bottomViewRight;
}
/**
 懒加载
 
 @return comitBtnRight
 */
- (UIButton *)comitBtnRight {
    if (!_comitBtnRight) {
        _comitBtnRight = [[UIButton alloc] init];
        _comitBtnRight.layer.cornerRadius = 7;
        _comitBtnRight.clipsToBounds = YES;
        [_comitBtnRight jk_setBackgroundColor:COLOR_MAIN forState:UIControlStateNormal];
        [_comitBtnRight setTitle:@"确认" forState:UIControlStateNormal];
        [_comitBtnRight setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
        [_comitBtnRight addTarget:self action:@selector(comitCode) forControlEvents:UIControlEventTouchUpInside];
    }
    return _comitBtnRight;
}
/**
 懒加载
 
 @return subTextLabelRight
 */
- (UILabel *)subTextLabelRight {
    if (!_subTextLabelRight) {
        _subTextLabelRight = [[UILabel alloc] init];
        _subTextLabelRight.textColor = COLOR_SUBTEXT;
        _subTextLabelRight.font = FONT_SUBTEXT;
        UserAuthDataModel *userModel=[[UserAuthData shareInstance] userAuthData];
        _subTextLabelRight.text = [NSString stringWithFormat:@"使用%@（%@）付款", userModel.bankName, userModel.bankCardLastNum];
    }
    return _subTextLabelRight;
}
/**
 懒加载
 
 @return getcodebtn
 */
- (JKCountDownButton *)getCodeBtn {
    if (!_getCodeBtn) {
        _getCodeBtn = [JKCountDownButton buttonWithType:UIButtonTypeCustom];
        [_getCodeBtn jk_setBackgroundColor:COLOR_MAIN forState:UIControlStateNormal];
        [_getCodeBtn jk_setBackgroundColor:COLOR_BUTTON_DISABLE forState:UIControlStateDisabled];
        _getCodeBtn.titleLabel.font = FONT_SUBTEXT;
        [_getCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_getCodeBtn setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
        [_getCodeBtn sizeToFit];
        [_getCodeBtn setTitleColor:COLOR_SUBTEXT forState:UIControlStateDisabled];
        _getCodeBtn.jk_touchAreaInsets = UIEdgeInsetsMake(20, 20, 20, 20);
        
        __weak typeof(self) weakSelf = self;
        [_getCodeBtn countDownButtonHandler:^(JKCountDownButton *sender, NSInteger tag) {
            //发送验证码
            UserAuthDataModel *userAuthData = [[UserAuthData shareInstance] userAuthData];
            NSNumber *amount = [NSNumber numberWithFloat:weakSelf.tradeAmount.floatValue];
            NSDictionary *dic;
            switch (weakSelf.tradeType) {
                case VerifyTypeUnionPayCharge: {
                    dic = @{
                                @"amount" : amount,
                                @"changeType" : [NSNumber numberWithInteger:0],
                                @"realName" : userAuthData.userName,
                                @"idCard" : userAuthData.idCard,
                                @"bankCard" : userAuthData.bankCard,
                                @"bankName" : userAuthData.bankName,
                                @"bankCode" : userAuthData.bankCode,
                                @"mobile" : userAuthData.mobile
                            };
                    [weakSelf dealOtherAction:dic];
                }
                    break;
                case VerifyTypeWithdraw:{
                    dic = @{
                                @"amount" : amount,
                                @"changeType" : [NSNumber numberWithInteger:1],
                                @"realName" : userAuthData.userName,
                                @"mobile" : userAuthData.mobile
                            };
                    [weakSelf dealOtherAction:dic];
                }
                    break;
                case VerifyTypeUnionPayBuy://银行卡购买
                case VerifyTypeBalancePay:
                    [weakSelf dealBuyAction];
                    break;
                default:
                    break;
            }
            //倒计时结束重置
            [sender countDownFinished:^NSString *(JKCountDownButton *countDownButton, NSUInteger second) {
                countDownButton.enabled = YES;
                return @"重新获取";
            }];
        }];
    }
    return _getCodeBtn;
}
/**
 懒加载
 
 @return 验证码输入框
 */
- (UITextField *)codeTextField {
    if (!_codeTextField) {
        _codeTextField = [[UITextField alloc] init];
        _codeTextField.leftViewMode = UITextFieldViewModeAlways;
        _codeTextField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 44)];
        _codeTextField.placeholder = @"请输入短信验证码";
        _codeTextField.backgroundColor = COLOR_WHITE;
        _codeTextField.font = FONT_TEXT;
        _codeTextField.jk_maxLength = 6;
        _codeTextField.keyboardType = UIKeyboardTypeNumberPad;
        _codeTextField.layer.borderColor = COLOR_LINE.CGColor;
        _codeTextField.layer.borderWidth = 1;
        _codeTextField.kbMoving.offset = 71;
    }
    return _codeTextField;
}
/**
 懒加载
 
 @return sendCodeLabel
 */
- (UILabel *)sendCodeLabel {
    if (!_sendCodeLabel) {
        _sendCodeLabel = [[UILabel alloc] init];
        _sendCodeLabel.textColor = COLOR_SUBTEXT;
        _sendCodeLabel.font = FONT_SUBTEXT;
        UserAuthDataModel *userModel=[[UserAuthData shareInstance] userAuthData];
        NSString *phoneStr =userModel.mobile;
        NSString *phoneFormatStr=[phoneStr stringByReplacingOccurrencesOfString:[phoneStr substringWithRange:NSMakeRange(3,4)]withString:@"*****"];
        _sendCodeLabel.text = [NSString stringWithFormat:@"短信验证码已发送到您的手机%@", phoneFormatStr];
    }
    return _sendCodeLabel;
}
/**
 懒加载
 
 @return 在线支付￥
 */
- (UILabel *)rightTitleLabel {
    if (!_rightTitleLabel) {
        _rightTitleLabel = [[UILabel alloc] init];
        _rightTitleLabel.font = FONT_TITLE;
        _rightTitleLabel.textColor = COLOR_Red;
    }
    return _rightTitleLabel;
}
/**
 懒加载
 
 @return backimage
 */
- (UIButton *)backArrowImage {
    if (!_backArrowImage) {
        _backArrowImage = [[UIButton alloc] init];
        [_backArrowImage setImage:[UIImage imageNamed:@"close_gray"] forState:UIControlStateNormal];
        [_backArrowImage addTapAction:@selector(viewDismissAction) target:self];
        _backArrowImage.jk_touchAreaInsets = UIEdgeInsetsMake(20, 20, 20, 20);
    }
    return _backArrowImage;
}
/**
 懒加载
 
 @return 银行卡支付
 */
- (UILabel *)subTextLabel {
    if (!_subTextLabel) {
        _subTextLabel = [[UILabel alloc] init];
        _subTextLabel.textColor = COLOR_SUBTEXT;
        _subTextLabel.font = FONT_SUBTEXT;
    }
    return _subTextLabel;
}
/**
 懒加载
 
 @return 确认
 */
- (UIButton *)comitBtn {
    if (!_comitBtn) {
        _comitBtn = [[UIButton alloc] init];
        [_comitBtn setBackgroundImage:[UIImage jk_imageWithColor:COLOR_MAIN] forState:UIControlStateNormal];
        [_comitBtn setBackgroundImage:[UIImage jk_imageWithColor:COLOR_BUTTON_DISABLE] forState:UIControlStateDisabled];
        _comitBtn.layer.cornerRadius = 7;
        _comitBtn.clipsToBounds = YES;
        [_comitBtn setTitle:@"确认" forState:UIControlStateNormal];
        [_comitBtn setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
        [_comitBtn addTarget:self action:@selector(comitPwd) forControlEvents:UIControlEventTouchUpInside];
        _comitBtn.enabled = NO;
    }
    return _comitBtn;
}
/**
 懒加载
 
 @return 密码
 */
- (UITextField *)pwdField {
    if (!_pwdField) {
        _pwdField = [[UITextField alloc] init];
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 77, 44)];
        UILabel *code = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, 65, 44)];
        code.text = @"交易密码";
        code.font = FONT_TEXT;
        code.textColor = COLOR_TEXT;
        [leftView addSubview:code];
        _pwdField.leftViewMode = UITextFieldViewModeAlways;
        _pwdField.placeholder = @"请输入交易密码";
        _pwdField.leftView = leftView;
        _pwdField.backgroundColor = COLOR_WHITE;
        _pwdField.font = FONT_TEXT;
        _pwdField.jk_maxLength = 6;
        _pwdField.keyboardType = UIKeyboardTypeNumberPad;
        _pwdField.secureTextEntry = YES;
        _pwdField.layer.borderColor = COLOR_LINE.CGColor;
        _pwdField.layer.borderWidth = 1;
        _pwdField.kbMoving.offset = 71;
        _pwdField.delegate = self;
    }
    return _pwdField;
}
/**
 懒加载
 
 @return line
 */
- (UIView *)line {
    if (!_line) {
        _line = [[UIView alloc] init];
        _line.backgroundColor = COLOR_LINE;
        _line.hidden = YES;
    }
    return _line;
}
/**
 懒加载
 
 @return 验证密码
 */
- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = @"请输入交易密码";
        _titleLabel.font = FONT_TITLE;
        _titleLabel.textColor = COLOR_Red;
    }
    return _titleLabel;
}
/**
 懒加载
 
 @return closeImage
 */
- (UIButton *)closeImage {
    if (!_closeImage) {
        _closeImage = [[UIButton alloc] init];
        [_closeImage setImage:[UIImage imageNamed:@"close_gray"] forState:UIControlStateNormal];
        _closeImage.jk_touchAreaInsets = UIEdgeInsetsMake(20, 20, 20, 20);
        [_closeImage addTapAction:@selector(viewDismissAction) target:self];
        
    }
    return _closeImage;
}
/**
 懒加载
 
 @return topView
 */
- (UIView *)topView {
    if (!_topView) {
        _topView = [[UIView alloc] init];
        _topView.backgroundColor = RGBA(77, 77, 77, 0.7);
//        [_topView addTapAction:@selector(viewDismissAction) target:self];
    }
    return _topView;
}
/**
 懒加载
 
 @return bottomView
 */
- (UIView *)bottomViewLeft {
    if (!_bottomViewLeft) {
        _bottomViewLeft = [[UIView alloc] init];
        _bottomViewLeft.backgroundColor = COLOR_WHITE;
        [_bottomViewLeft addSubview:self.closeImage];
        [_bottomViewLeft addSubview:self.titleLabel];
        [_bottomViewLeft addSubview:self.line];
        [_bottomViewLeft addSubview:self.pwdField];
        [_bottomViewLeft addSubview:self.comitBtn];
        [_bottomViewLeft addSubview:self.subTextLabel];
    }
    return _bottomViewLeft;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
