//
//  VerifyTool.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/10/19.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef NS_ENUM(NSInteger, VerifyType) {
    VerifyTypeUnionPayCharge,
    VerifyTypeUnionPayBuy,//银行卡支付，充值
    VerifyTypeBalancePay,//余额支付
    VerifyTypeWithdraw,//提现
    VerifyTypeOthe//卖金、赎回
};
@class PayModel;
@interface VerifyTool : NSObject

@property (nonatomic, copy) void (^response)(BOOL result, NSString *errMsg);

@property (nonatomic, copy) void (^payResponse)(BOOL result, PayModel *payModel, NSString *errMsg);

/**
 验证交易密码

 @param type 交易类型
 @param amount 交易金额
 */
+ (void)verifyTradeNumWithType:(VerifyType)type amount:(NSString *)amount response:(void(^)(BOOL result, NSString *errMsg))response;

/**
 验证交易密码  银行验证码

 @param type 交易类型
 @param amount 交易金额
 @param parameter 买金参数
 @param response 是否成功
 */
+ (void)verifyTradeNumWithType:(VerifyType)type amount:(NSString *)amount parameter:(NSDictionary *)parameter response:(void(^)(BOOL result, PayModel *payModel, NSString *errMsg))response;


@end
