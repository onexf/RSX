//
//  YJPushManager.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/14.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "YJPushManager.h"

#import "JPUSHService.h"
#define CHANNERL @"APPSTORE"

#ifdef DEBUG

#define JPUSHAPPKEY @"4d3b890db3311e19e8c3dead"    //马甲4d3b890db3311e19e8c3dead    正式 99ca7628bf57968df4ee6da0
#define IsProductPush NO

#else

#define JPUSHAPPKEY @"4d3b890db3311e19e8c3dead"
#define IsProductPush YES

#endif

@implementation YJPushManager

+ (instancetype)sharedManager {
    static YJPushManager *sharedManager = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        sharedManager = [[super alloc]init];
    });
    return sharedManager;
}


- (void)startPushWithOption:(NSDictionary *)option {
    [JPUSHService setupWithOption:option appKey:JPUSHAPPKEY channel:CHANNERL apsForProduction:IsProductPush];

    [JPUSHService registerForRemoteNotificationTypes:(UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert) categories:nil];
}

@end
