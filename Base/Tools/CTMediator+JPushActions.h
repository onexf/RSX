//
//  CTMediator+JPushActions.h
//  LBHunter
//
//  Created by wxf on 2017/3/23.
//  Copyright © 2017年 person. All rights reserved.
//

#import "CTMediator.h"

@interface CTMediator (JPushActions)


- (void)routToJPushViewControllerWithParams:(NSDictionary *)params response:(void(^)(UIViewController *pushToViewContrller))response;




@end
