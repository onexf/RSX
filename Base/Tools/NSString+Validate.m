//
//  NSString+Validate.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/10/12.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "NSString+Validate.h"

@implementation NSString (Validate)

BOOL validateMobile(NSString *mobile){
    NSString *MOBILE = @"^1(3[0-9]|4[57]|5[0-35-9]|8[0-9]|7[0135678])\\d{8}$";
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    return [regextestmobile evaluateWithObject:mobile];
}
@end
