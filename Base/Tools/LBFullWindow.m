//
//  LBFullWindow.m
//  LBHunter
//
//  Created by wxf on 2017/4/20.
//  Copyright © 2017年 person. All rights reserved.
//

#import "LBFullWindow.h"
#import "UIView+Extension.h"
@implementation LBFullWindow
static UIWindow *window_;
static NSString *appUrl = @"https://itunes.apple.com/cn/app/id1254137501?mt=8";

//static CGFloat const WindowH = SCREEN_HEIGHT;
//LBUpdate_image_close

+ (void)showImage:(UIImage *)image text:(NSString *)text subText:(NSString *)subText showCloseButton:(BOOL)show
{
    CGFloat WindowH = SCREEN_HEIGHT;
    
    CGFloat width = SCREEN_WIDTH - 104;

    // 创建窗口
    window_.hidden = YES; // 先隐藏之前的window
    window_ = [[UIWindow alloc] init];
    window_.backgroundColor = RGBA(33, 33, 33, 0.7);
    window_.windowLevel = UIWindowLevelAlert;
    window_.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, WindowH);
    window_.hidden = NO;

    
    UIView *backGroundView = [[UIView alloc] init];
    backGroundView.layer.cornerRadius = 10.0;
    backGroundView.backgroundColor = COLOR_WHITE;
    [window_ addSubview:backGroundView];

    
    UIImageView *closeButton = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"LBUpdate_image_close"]];
    [window_ addSubview:closeButton];
    closeButton.hidden = YES;
    closeButton.backgroundColor = COLOR_Red;
    [closeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(backGroundView.mas_top).offset(-35);
        make.right.equalTo(backGroundView);
    }];

    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    [backGroundView addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(closeButton).offset(-25);
        make.centerX.width.equalTo(backGroundView);
        make.height.equalTo(@(width / 1.42));
//        make.width.equalTo(@(width + 30));
    }];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    [backGroundView addSubview:titleLabel];
    titleLabel.textColor = COLOR_WHITE;
    titleLabel.font = FONT_NAV;
    titleLabel.backgroundColor = COLOR_MAIN;
    titleLabel.layer.cornerRadius = 12;
    titleLabel.clipsToBounds = YES;
    titleLabel.text = [NSString stringWithFormat:@"  V%@  ", text];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imageView.mas_bottom);
        make.right.equalTo(backGroundView).offset(-30);
        make.height.equalTo(@25);
    }];
    
    UILabel *subTitleLbael = [[UILabel alloc] init];
    [backGroundView addSubview:subTitleLbael];
    subTitleLbael.textColor = COLOR_TEXT;
    subTitleLbael.font = FONT_TEXT;
    subTitleLbael.numberOfLines = 0;
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:5];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:subText];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, subText.length)];
    subTitleLbael.attributedText = attributedString;
    
    [subTitleLbael mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLabel.mas_bottom).offset(15);
        make.left.equalTo(backGroundView).offset(30);
        make.right.equalTo(backGroundView).offset(-30);
    }];

    UIView *line = [[UIView alloc] init];
    [backGroundView addSubview:line];
    line.backgroundColor = COLOR_LINE;
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(subTitleLbael.mas_bottom).offset(30);
        make.left.right.equalTo(backGroundView);
        make.height.equalTo(@1);
    }];
    
    UIButton *action = [[UIButton alloc] init];
    [backGroundView addSubview:action];
    [action setTitle:@"立即更新" forState:UIControlStateNormal];
    [action setTitleColor:COLOR_MAIN forState:UIControlStateNormal];
    action.titleLabel.font = FONT_NAV;
    [action addTarget:self action:@selector(action) forControlEvents:UIControlEventTouchUpInside];
    [action mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(line.mas_bottom);
        make.centerX.equalTo(backGroundView);
        make.height.equalTo(@44);
    }];

    [backGroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(window_);
        make.width.equalTo(@(width));
        make.bottom.equalTo(action.mas_bottom);
    }];
    
    UIButton *closeBtn = [[UIButton alloc] init];
    [closeBtn setImage:[UIImage imageNamed:@"LBUpdate_image_close"] forState:UIControlStateNormal];
    [closeBtn addTapAction:@selector(hideFullWindow) target:self];
    [closeBtn sizeToFit];
    closeBtn.contentEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10);
    [window_ addSubview:closeBtn];
    closeBtn.hidden = !show;
    
    [closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(backGroundView);
        make.top.equalTo(backGroundView.mas_bottom).offset(15 * SCREEN_WIDTH / 375.0);
    }];

}

+ (void)action
{
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:appUrl]];
    window_ = nil;
}
+ (void)hideFullWindow
{
    window_ = nil;
}


@end
