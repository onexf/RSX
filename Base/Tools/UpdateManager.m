//
//  UpdateManager.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/15.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "UpdateManager.h"
#import "LBFullWindow.h"

static NSString *appUrl = @"https://itunes.apple.com/cn/app/id1254137501?mt=8";
static NSString *timeKey = @"updateTime";

#define currentDateStr ([NSDate jk_currentDateStringWithFormat:@"yyyyMMdd"])//yyyy-MM-dd HH:mm
typedef NS_ENUM(NSInteger,UpdateType){
    UpdateTypeNone=-1,//未调接口检查
    UpdateTypeNoNeed=0,//无需
    UpdateTypeForce=1,//强制
    UpdateTypeVoluntarily=2,//自愿
};

@implementation UpdateManager
{
    UpdateType _type;
    NSString *alertString;
    NSString *versionString;
    
}

+ (instancetype)instance {
    static UpdateManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager =  [[UpdateManager alloc]init];
    });
    return manager;
}
- (instancetype)init {
    if (self = [super init ]) {
        _type = UpdateTypeNone;
    }
    return self;
}
- (NSString *)localVersion {
    NSString *infoPath = [[NSBundle mainBundle] pathForResource:@"Info" ofType:@"plist"];
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:infoPath];
    NSString *version = [dict objectForKey:@"CFBundleShortVersionString"];
    return version;
}
/**
 *  对比版本号
 *
 *  @return 当前版本是否为低版本
 */
- (BOOL)checkIsLowVersion:(NSString *)version {
    NSString *local = [self localVersion];
    local = [local stringByReplacingOccurrencesOfString:@"." withString:@""];
    NSString *server = [version stringByReplacingOccurrencesOfString:@"." withString:@""];
    if ([server integerValue] > [local integerValue]) {
        return YES;
    } else {
        return NO;
    }
}

- (void)checkIfNeedUpdate {
    switch (_type) {
        case UpdateTypeNone:{
            [NetworkSingleton update_getVersionDataResponse:^(NSDictionary *dict, NSString *errMsg) {
                if (dict) {
                    NSString *versonStr = dict[@"version"];
                    versionString = versonStr;
                    NSString *verson = [versonStr stringByReplacingOccurrencesOfString:@"." withString:@""];
                    NSString *subText = dict[@"description"];
                    alertString = subText;
                    _type = [dict[@"isForce"] integerValue];
                    //版本一致，！
                    if (![self checkIsLowVersion:verson]) {
                        _type = UpdateTypeNoNeed;//版本号相同
                    } else {
                        if (_type == UpdateTypeNoNeed) {
                            [LBFullWindow showImage:[UIImage imageNamed:@"LBUpdate_image"] text:versionString subText:subText showCloseButton:YES];
                        } else if (_type == UpdateTypeForce) {//需要强制更新
                            [LBFullWindow showImage:[UIImage imageNamed:@"LBUpdate_image"] text:versionString subText:subText showCloseButton:NO];
                        }
                    }
                }
            }];
        }
            break;
        case UpdateTypeNoNeed:
            break;
        case UpdateTypeForce:
            [LBFullWindow showImage:[UIImage imageNamed:@"LBUpdate_image"] text:versionString subText:alertString showCloseButton:NO];
            break;
        case UpdateTypeVoluntarily:{
            if (![currentDateStr isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:timeKey]]) {
                [[NSUserDefaults standardUserDefaults] setObject:currentDateStr forKey:timeKey];
                [LBFullWindow showImage:[UIImage imageNamed:@"LBUpdate_image"] text:versionString subText:alertString showCloseButton:YES];
            }
        }
            break;
        default:
            break;
    }
}



@end



#define UrlVersion                URLString(@"/api/versions/newversion")

@implementation NetworkSingleton (Update)

+ (void)update_getVersionDataResponse:(void(^)(NSDictionary *dict, NSString *errMsg))response {
    NSDictionary *dict = @{
                               @"category" : @1,
                               @"version" : [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]
                           };
    [[NetworkSingleton sharedManager] postResultWithParameter:dict url:UrlVersion successBlock:^(id responseBody) {
        if ([[responseBody class] isSubclassOfClass:[NSNull class]]) {
            response(nil, responseBody[@"msg"]);
        } else {
            response(responseBody, nil);
        }
    } failureBlock:^(NSString *error) {
        response(nil, error);
    }];
}

@end
