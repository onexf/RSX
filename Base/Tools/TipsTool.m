//
//  TipsTool.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/14.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "TipsTool.h"
#import <STPopup/STPopup.h>
#import "TipsViewController.h"

@interface TipsTool ()<STPopupControllerTransitioning>

@end


@implementation TipsTool

#pragma mark - STPopupControllerTransitioning

- (NSTimeInterval)popupControllerTransitionDuration:(STPopupControllerTransitioningContext *)context
{
    return context.action == 1.7;
}

- (void)popupControllerAnimateTransition:(STPopupControllerTransitioningContext *)context completion:(void (^)())completion
{
    UIView *containerView = context.containerView;
    if (context.action == STPopupControllerTransitioningActionPresent) {
        containerView.transform = CGAffineTransformMakeTranslation(containerView.superview.bounds.size.width - containerView.frame.origin.x, 0);
        
        [UIView animateWithDuration:[self popupControllerTransitionDuration:context] delay:0 usingSpringWithDamping:1 initialSpringVelocity:1 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            context.containerView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished) {
            completion();
        }];
    }
    else {
        [UIView animateWithDuration:[self popupControllerTransitionDuration:context] delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            containerView.transform = CGAffineTransformMakeTranslation(- 2 * (containerView.superview.bounds.size.width - containerView.frame.origin.x), 0);
        } completion:^(BOOL finished) {
            containerView.transform = CGAffineTransformIdentity;
            completion();
        }];
    }
}
+ (void)showTipsWithTitle:(NSString *)title andContent:(NSString *)content {
    TipsViewController *tipsVC = [TipsViewController new];
    tipsVC.titleString = title;
    tipsVC.contentString = content;
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:tipsVC];
    popupController.transitionStyle = STPopupTransitionStyleFade;
    [popupController presentInViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

@end
