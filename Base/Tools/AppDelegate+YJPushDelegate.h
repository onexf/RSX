//
//  AppDelegate+YJPushDelegate.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/15.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (YJPushDelegate)

@property (nonatomic,copy)NSString *pushTokenFromApple;

+ (void)testReceiveMessageWithType:(NSString *)type;

@end
