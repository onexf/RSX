//
//  AppDelegate.m
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/8/21.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "AppDelegate.h"
#import "Colors.h"
#import "GoldCatBankTabBarController.h"
#import "RealReachability.h"
#import "UserAuthData.h"
#import "AppDelegate+UMSocial.h"
#import "IQKeyboardManager.h"
#import "YJPushManager.h"
#import "UpdateManager.h"
#import "JPUSHService.h"
#import <UMMobClick/MobClick.h>
#import "LCNewFeatureVC.h"
#import "CBWebViewController.h"
#import "GoldCatBankNavController.h"
#import "AvoidCrash.h"
#import "NSObject+LBLaunchImage.h"
#import "ActivityView.h"
#import "BannerModel.h"
#import "CBWebViewController.h"
#import "NetworkSingleton+Home.h"
#import <WebKit/WebKit.h>
#import "BannerModel.h"

@interface AppDelegate ()
@property (nonatomic, strong) WKWebView *webView;
@property (assign) BOOL activited;
@property (assign) BOOL doubleClick;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    //初始化操作
    [self launchTools];
    [[YJPushManager sharedManager] startPushWithOption:launchOptions];
    [self setRootViewController];
    [self configUserAgent];

    [self getADImage];
    return YES;
}
- (void)getADImage {
    [NetworkSingleton launch_queryLaunchImageResponse:^(BannerModel *activity, NSString *errMsg) {
        if (activity) {
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:activity];
            UDSave(data, @"ADImage");
        }
    }];
}
- (void)configUserAgent {
    self.webView = [[WKWebView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH, SCREEN_HEIGHT, 1, 1)];
    [self.window addSubview:_webView];
    __weak typeof(self)weakSelf = self;
    [_webView evaluateJavaScript:@"navigator.userAgent" completionHandler:^(id result, NSError *error) {
        if(!error){
            NSString *userAgent = result;
            NSString *newUserAgent = [userAgent stringByAppendingString:@"/==zhaojinmao"];
            NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:newUserAgent, @"UserAgent", nil];
            [[NSUserDefaults standardUserDefaults] registerDefaults:dictionary];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        if (weakSelf.webView.superview) {
            [weakSelf.webView removeFromSuperview];
        }
        if (weakSelf.webView) {
            weakSelf.webView = nil;
        }
    }];
    
}
- (void)setRootViewController {
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = COLOR_WHITE;
    BOOL showNewFeature = [LCNewFeatureVC shouldShowNewFeature];
    self.showNewFeature = showNewFeature;
    if (showNewFeature) {   // 如果需要显示新特性界面
        __weak typeof(self) weakSelf = self;
        LCNewFeatureVC *newFeatureVC = [LCNewFeatureVC newFeatureWithImageName:@"NewFeature"
                                                                    imageCount:3
                                                               showPageControl:YES
                                                                   finishBlock:^{
                                                                       weakSelf.window.rootViewController = [[GoldCatBankTabBarController alloc] init];
                                                                   }];
        newFeatureVC.pointCurrentColor = COLOR_Red;
        self.window.rootViewController = newFeatureVC;
    } else {// 如果不需要显示新特性界面
        self.window.rootViewController = [[GoldCatBankTabBarController alloc] init];
        __weak typeof(self) weakSelf = self;
        [NSObject makeLBLaunchImageAdView:^(LBLaunchImageAdView *imgAdView) {
            //设置广告的类型
            imgAdView.getLBlaunchImageAdViewType(FullScreenAdType);
            NSData *adImageData = UDGET(@"ADImage");
            BannerModel *adImage = [NSKeyedUnarchiver unarchiveObjectWithData:adImageData];
            if (adImage) {
                imgAdView.imgUrl = adImage.picUrl;
            } else {
                imgAdView.imgUrl = @"https://img.zhaojinmao.cn/start.png";
            }
            //自定义跳过按钮
            imgAdView.skipBtn.backgroundColor = [UIColor blackColor];
            //各种点击事件的回调
            imgAdView.clickBlock = ^(clickType type){
                switch (type) {
                    case clickAdType:{
//                        NSLog(@"点击广告回调");
                        if (!self.doubleClick) {
                            self.doubleClick = YES;
                            if (adImage) {
                                //2.在webview中打开
                                CBWebViewController *vc = [[CBWebViewController alloc] init];
                                vc.urlString = adImage.href;
                                if ([adImage.title isEqualToString:@"我是产品"]) {
                                    vc.productID = adImage.sort;
                                    if (adImage.WTF.integerValue == 1) {
                                        vc.isDisplayNativeBtn=YES;
                                    } else {
                                        vc.isDisplayNativeBtn=NO;
                                    }
                                }
                                //广告展示完成回调,设置window根控制器
                                GoldCatBankNavController *navVC =  weakSelf.window.rootViewController.childViewControllers[0];
                                [navVC pushViewController:vc animated:NO];
                            } else {
                                //2.在webview中打开
                                CBWebViewController *vc = [[CBWebViewController alloc] init];
                                vc.urlString = @"https://www.zhaojinmao.cn/testwebapp/prodect/NewbiePrivilege/NewbiePrivilege.html?id=6";
                                vc.productID = @"6";
                                vc.isDisplayNativeBtn=YES;
                                //广告展示完成回调,设置window根控制器
                                GoldCatBankNavController *navVC =  weakSelf.window.rootViewController.childViewControllers[0];
                                [navVC pushViewController:vc animated:NO];
                            }
                        }
                    }
                        break;
                    case skipAdType:
//                        NSLog(@"点击跳过回调");
                        if (!self.activited) {
                            [self performSelector:@selector(getActivityImage) withObject:nil afterDelay:0.3];
                            self.activited = YES;
                        }
                        break;
                    case overtimeAdType:
//                        NSLog(@"倒计时完成后的回调");
                        if (!self.activited) {
                            [self performSelector:@selector(getActivityImage) withObject:nil afterDelay:0.3];
                            self.activited = YES;
                        }
                        break;
                    default:
                        break;
                }
            };
        }];
    }
    [self.window makeKeyAndVisible];
}

- (void)getActivityImage {
    [NetworkSingleton home_queryActivityImageResponse:^(BannerModel *activity, NSString *errMsg) {
        if (activity) {
            ActivityView * popView = [[ActivityView alloc] init];
            popView.title = activity.title.length > 0 ? activity.title : @"活动";
            [popView.adImageView sd_setImageWithURL:[NSURL URLWithString:activity.picUrl]];
            [popView popADWithAnimated:YES];
            popView.goNextBlock = ^{
                CBWebViewController *webViewController = [[CBWebViewController alloc] init];
                webViewController.urlString = activity.href;//https://www.zhaojinmao.cn/test_clothes/index/redbag/index.html
                webViewController.showShareButton = NO;
                [[UIApplication sharedApplication].keyWindow.rootViewController.childViewControllers.firstObject pushViewController:webViewController animated:YES];
            };
        }
    }];
}

- (void)launchTools {
    //键盘处理
    [self KeyBordHandle];
    //友盟分享初始化
    [self um_registerConfigurations];
    //防止不同view同时响应touch事件
    [[UIView appearance] setExclusiveTouch:YES];
    //更新用户绑定信息
    [[UserAuthData shareInstance] refreshUserAuthData];
    //网络状态监控
    [GLobalRealReachability startNotifier];
    //友盟统计
    UMConfigInstance.appKey = @"59f00119734be439b9000012";//马甲59f00119734be439b9000012  正式599163fcbbea836109000127
    UMConfigInstance.channelId = @"App Store";
    [MobClick startWithConfigure:UMConfigInstance];
    
    //启动防止崩溃功能
    [AvoidCrash becomeEffective];
    //监听通知:AvoidCrashNotification, 获取AvoidCrash捕获的崩溃日志的详细信息
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dealwithCrashMessage:) name:AvoidCrashNotification object:nil];
}
- (void)dealwithCrashMessage:(NSNotification *)note {
    //不论在哪个线程中导致的crash，这里都是在主线程
    //注意:所有的信息都在userInfo中
    //你可以在这里收集相应的崩溃信息进行相应的处理(比如传到自己服务器)
    NSLog(@"\n\n在AppDelegate中 方法:dealwithCrashMessage打印\n\n\n\n\n%@\n\n\n\n", note.userInfo);
}

-(void)KeyBordHandle{
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = YES;
    manager.shouldResignOnTouchOutside = YES;
    manager.shouldToolbarUsesTextFieldTintColor = YES;
    manager.enableAutoToolbar = NO;
    
}

- (BOOL)application:(UIApplication *)application shouldAllowExtensionPointIdentifier:(NSString *)extensionPointIdentifier {
    return NO;
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    [JPUSHService setBadge:0];
    [[UpdateManager instance] checkIfNeedUpdate];
    application.applicationIconBadgeNumber = 0;
    if (isLogin()) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"First" object:nil];
//        [[UserData instance] refreshUserInfo];
    }
}
- (BOOL)application:(UIApplication *)application openURL:(nonnull NSURL *)url sourceApplication:(nullable NSString *)sourceApplication annotation:(nonnull id)annotation {
    BOOL result = NO;
    if ([[UMSocialManager defaultManager] handleOpenURL:url]){
        result = YES;
    }
    return result;
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
}
@end
