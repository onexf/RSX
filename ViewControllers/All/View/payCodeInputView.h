//
//  payCodeInputView.h
//  GoldCatBank
//
//  Created by Sunny on 2017/9/7.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol payCodeInputViewDelegate <NSObject>

-(void)clickSendCodeButton;

-(void)finishWithInputCodeView:(NSString *)codeStr;

-(void)payCodeCancel;
@end
@interface payCodeInputView : UIView
@property(nonatomic,copy)NSString *payAmountStr;
@property(nonatomic,strong)UITextField *inputTxtf;
@property(nonatomic,weak)id <payCodeInputViewDelegate>delegate;

//暂停定时器
- (void)suspend;

//开始定时器
-(void)start;

-(void)show;

@end
