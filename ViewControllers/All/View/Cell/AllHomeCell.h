//
//  AllHomeCell.h
//  GoldCatBank
//
//  Created by 高丹丹 on 2017/8/23.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllListModel.h"

@interface AllHomeCell : UITableViewCell
@property(nonatomic,strong)UILabel *pricePercentLab;//价格百分比
@property(nonatomic,strong)CMBorderView *borderView;
-(void)fillCellWithModel:(AllListModel *)model;

@end
