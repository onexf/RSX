//
//  NewAllHomeCell.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/11/16.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AllListModel;

@interface NewAllHomeCell : UITableViewCell

/** model */
@property(nonatomic, strong) AllListModel *model;


+ (instancetype)cellWithTableview:(UITableView *)tableView;

@end
