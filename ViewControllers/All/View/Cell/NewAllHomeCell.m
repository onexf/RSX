//
//  NewAllHomeCell.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/11/16.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "NewAllHomeCell.h"
#import "AllListModel.h"
@interface NewAllHomeCell ()
/** nameLabel */
@property(nonatomic, strong) UILabel *nameLabel;
/** 是否是专享 */
@property(nonatomic, strong) UIButton *isNewerProduct;
/** 价格 */
@property(nonatomic, strong) UILabel *priceLabel;
/** subText */
@property(nonatomic, strong) UILabel *subLeftLabel;
/** 右 */
@property(nonatomic, strong) UILabel *subRightLabel;
/** buycount */
@property(nonatomic, strong) UILabel *countLabel;
/** arrow */
@property(nonatomic, strong) UIImageView *arrowImageView;

@end


@implementation NewAllHomeCell

- (void)setFrame:(CGRect)frame {
//    frame.size.height = 107;
//    frame.origin.y += 0;
    frame.size.height -= 10;
    [super setFrame:frame];
}

- (void)setModel:(AllListModel *)model {
    _model = model;
    self.nameLabel.text = model.productName;
    self.isNewerProduct.backgroundColor = model.isGoldProduct.integerValue > 0 ? COLOR_MAIN : COLOR_MAIN_BLUE;
    self.isNewerProduct.hidden = !(model.isVip.integerValue > 0);
    self.priceLabel.textColor = model.isGoldProduct.integerValue > 0 ? COLOR_MAIN : COLOR_MAIN_BLUE;
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:model.profitRatio];
    if ([model.profitRatio jk_containsaString:@"%"]) {
        [attrStr addAttribute:NSFontAttributeName
                        value:FONT(15)
                        range:NSMakeRange(model.profitRatio.length - 1, 1)];
    } else {
        [attrStr addAttribute:NSFontAttributeName
                        value:FONT(15)
                        range:NSMakeRange(model.profitRatio.length - 3, 3)];
    }
    self.priceLabel.attributedText = attrStr;
    self.subLeftLabel.text = model.smallTitle2;
    self.arrowImageView.image = (model.isGoldProduct.integerValue > 0) ? [UIImage imageNamed:@"All_goldArrow"] : [UIImage imageNamed:@"All_otherArrow"];
    self.arrowImageView.hidden = (model.hasLeaf.integerValue <= 0);
    self.subRightLabel.text = model.smallTitle1;
    NSString *countStr = [NSString stringWithFormat:@"已有%@人购买", model.sellCount];
    NSMutableAttributedString *countAttStr = [[NSMutableAttributedString alloc] initWithString:countStr];
    if (model.isGoldProduct.integerValue > 0) {
        [countAttStr addAttribute:NSForegroundColorAttributeName
                        value:COLOR_MAIN
                        range:NSMakeRange(2, model.sellCount.length)];

    } else {
        [countAttStr addAttribute:NSForegroundColorAttributeName
                            value:COLOR_MAIN_BLUE
                            range:NSMakeRange(2, model.sellCount.length)];
    }
    self.countLabel.attributedText = countAttStr;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(10);
        make.left.equalTo(self.contentView).offset(16);
    }];
    [self.isNewerProduct mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.nameLabel);
        make.left.equalTo(self.nameLabel.mas_right).offset(10);
    }];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.nameLabel.mas_bottom).offset(4);
        make.left.equalTo(self.nameLabel);
    }];
    [self.subLeftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.priceLabel.mas_bottom).offset(4);
        make.left.equalTo(self.priceLabel);
    }];
    [self.arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.contentView).offset(-10);
    }];
    [self.subRightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.priceLabel);
        make.right.equalTo(self.arrowImageView.mas_left).offset(-14);
    }];
    [self.countLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.subLeftLabel);
        make.right.equalTo(self.subRightLabel);
    }];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.nameLabel];
        [self.contentView addSubview:self.isNewerProduct];
        [self.contentView addSubview:self.priceLabel];
        [self.contentView addSubview:self.subLeftLabel];
        [self.contentView addSubview:self.subRightLabel];
        [self.contentView addSubview:self.countLabel];
        [self.contentView addSubview:self.arrowImageView];
    }
    return self;
}
+ (instancetype)cellWithTableview:(UITableView *)tableView {
    static NSString *identifier = @"NewAllHomeCell";
    NewAllHomeCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[NewAllHomeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.contentView.backgroundColor = COLOR_WHITE;
    }
    return cell;
}
/**
 懒加载
 
 @return arrow
 */
- (UIImageView *)arrowImageView {
    if (!_arrowImageView) {
        _arrowImageView = [[UIImageView alloc] init];
        _arrowImageView.hidden = YES;
    }
    return _arrowImageView;
}
/**
 懒加载
 
 @return 购买人数
 */
- (UILabel *)countLabel {
    if (!_countLabel) {
        _countLabel = [[UILabel alloc] init];
        _countLabel.font = FONT_TEXT;
        _countLabel.textColor = COLOR_SUBTEXT;
    }
    return _countLabel;
}
/**
 懒加载
 
 @return sub2
 */
- (UILabel *)subRightLabel {
    if (!_subRightLabel) {
        _subRightLabel = [[UILabel alloc] init];
        _subRightLabel.textColor = COLOR_TEXT;
        _subRightLabel.font = FONT_TEXT;
    }
    return _subRightLabel;
}
/**
 懒加载
 
 @return sub
 */
- (UILabel *)subLeftLabel {
    if (!_subLeftLabel) {
        _subLeftLabel = [[UILabel alloc] init];
        _subLeftLabel.textColor = COLOR_SUBTEXT;
        _subLeftLabel.font = FONT_TEXT;
    }
    return _subLeftLabel;
}
/**
 懒加载
 
 @return 单价或年化收益
 */
- (UILabel *)priceLabel {
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc] init];
        _priceLabel.font = FONT_BOLD(23);
    }
    return _priceLabel;
}
/**
 懒加载
 
 @return 新手专享
 */
- (UIButton *)isNewerProduct {
    if (!_isNewerProduct) {
        _isNewerProduct = [[UIButton alloc] init];
        _isNewerProduct.userInteractionEnabled = NO;
        [_isNewerProduct setTitle:@"专享" forState:UIControlStateNormal];
        [_isNewerProduct setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
        _isNewerProduct.contentEdgeInsets = UIEdgeInsetsMake(0, 3, 0, 3);
        _isNewerProduct.titleLabel.font = FONT(13);
        _isNewerProduct.layer.cornerRadius = 2;
        _isNewerProduct.hidden = YES;
    }
    return _isNewerProduct;
}
/**
 懒加载
 
 @return 产品名
 */
- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.font = FONT_TITLE;
        _nameLabel.textColor = COLOR_TEXT;
    }
    return _nameLabel;
}
@end
