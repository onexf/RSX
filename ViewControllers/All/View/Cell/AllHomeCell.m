//
//  AllHomeCell.m
//  GoldCatBank
//
//  Created by 高丹丹 on 2017/8/23.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "AllHomeCell.h"

#define CellHeight 67

@interface AllHomeCell()

@property(nonatomic,strong)UILabel *nameLab;//产品名称
@property(nonatomic,strong)UILabel *n_contentLab;

@property(nonatomic,strong)UILabel *p_contentLab;
@property(nonatomic,strong)UIImageView *soleImageView;//专享
@property(nonatomic,strong)UIImageView *pointImageView;
@end

@implementation AllHomeCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self.contentView addSubview:self.borderView];
        [self.borderView addSubview:self.nameLab];
        [self.borderView addSubview:self.n_contentLab];
        [self.borderView addSubview:self.pricePercentLab];
        [self.borderView addSubview:self.p_contentLab];
        [self.borderView addSubview:self.soleImageView];
        [self.borderView addSubview:self.pointImageView];
    }
    
    return self;
}

-(UIImageView *)pointImageView
{
    if (!_pointImageView)
    {
        
        UIImage *image=[UIImage imageNamed:@"all_moreIcon"];
        _pointImageView=[[UIImageView alloc]initWithFrame:CGRectMake(_pricePercentLab.ct_right,(CellHeight-image.size.height)/2.0, image.size.width, image.size.height)];
        _pointImageView.image=image;
    }
    
    return _pointImageView;
}

-(CMBorderView *)borderView
{
    if (!_borderView)
    {
        _borderView=[[CMBorderView alloc]initWithFrame:CGRectMake(12.5,0, SCREEN_WIDTH-12.5, CellHeight)];
        _borderView.borderWidth=CMViewBorderWidthMake(0, 0, 0.5, 0);
        _borderView.borderColor=UIColorFromHex(0xdadada);
    }
    return _borderView;
}
-(UILabel *)nameLab
{
    if (!_nameLab)
    {
        _nameLab=[[UILabel alloc]initWithFrame:CGRectMake(0,15, SCREEN_WIDTH/2.0, 20)];
        _nameLab.font=FONT_TEXT;
        _nameLab.textColor=UIColorFromHex(0x333333);
    }
    return _nameLab;
}

-(UILabel *)n_contentLab
{
    if (!_n_contentLab)
    {
        _n_contentLab=[[UILabel alloc]initWithFrame:CGRectMake(_nameLab.ct_left, _nameLab.ct_bottom+5, _nameLab.ct_width, 15)];
        _n_contentLab.font=FONT_SUBTEXT;
        _n_contentLab.textColor=UIColorFromHex(0x999999);
    }
    return _n_contentLab;
}

-(UILabel *)pricePercentLab
{
    if (!_pricePercentLab)
    {
        _pricePercentLab=[[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2.0+20, _nameLab.ct_top, SCREEN_WIDTH/2.0-50, _nameLab.ct_height)];
        _pricePercentLab.font=FONT_BOLD(19);
        _pricePercentLab.textColor=UIColorFromHex(0xeca277);
        
    }
    return _pricePercentLab;
}

-(UILabel *)p_contentLab
{
    if (!_p_contentLab)
    {
        _p_contentLab=[[UILabel alloc]initWithFrame:CGRectMake(_pricePercentLab.ct_left,_n_contentLab.ct_top, _pricePercentLab.ct_width, _n_contentLab.ct_height)];
        _p_contentLab.textColor=_n_contentLab.textColor;
        _p_contentLab.font=_n_contentLab.font;
    }
    return _p_contentLab;
}

-(UIImageView *)soleImageView
{
    if (!_soleImageView)
    {
        UIImage *soleImage=[UIImage imageNamed:@"All_soleIcon"];
        _soleImageView=[[UIImageView alloc]initWithFrame:CGRectMake(_borderView.ct_width-soleImage.size.width, -1, soleImage.size.width, soleImage.size.height)];
        _soleImageView.image=soleImage;
        _soleImageView.hidden=YES;
        
    }
    return _soleImageView;
}

-(void)fillCellWithModel:(AllListModel *)model
{
    self.nameLab.text=model.productName;
    self.n_contentLab.text=model.smallTitle1;
    self.pricePercentLab.text=model.profitRatio;
    self.p_contentLab.text=[NSString stringWithFormat:@"已有%@人购买",model.sellCount];
    
    
    if ([model.hasLeaf integerValue]>0)
    {
        self.pointImageView.hidden=NO;
    }
    else
    {
        self.pointImageView.hidden=YES;
    }
    if (model.isGoldProduct.integerValue>0)
    {
        self.pricePercentLab.textColor=COLOR_GLODBACKGROUD;
    }
    else
    {
        
        self.pricePercentLab.textColor=UIColorFromHex(0x6095ff);

    }
    if ([model.isVip integerValue]>0)
    {
        self.soleImageView.hidden=NO;
    
    }
    else
    {
        self.soleImageView.hidden=YES;
    }
    
    
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
