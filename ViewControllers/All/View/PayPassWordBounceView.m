//
//  PayPassWordBounceView.m
//  GoldCatBank
//
//  Created by 高丹丹 on 2017/9/8.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "PayPassWordBounceView.h"
#import "UserAuthData.h"

@interface PayPassWordBounceView()
@property(nonatomic,strong)CMBorderView *bordView;
@property(nonatomic,strong)UILabel *titlelab;
@property(nonatomic,strong)UILabel *bottomTipLab;

@end

@implementation PayPassWordBounceView

-(id)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    if (self)
    {
        
        self.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
        
        [self addSubview:self.bordView];
        
        
        UIButton *cancelBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        cancelBtn.frame=CGRectMake(10, 10, 40, 30);
        [cancelBtn setImage:[UIImage imageNamed:@"Pay_cancelICon"] forState:UIControlStateNormal];
        [cancelBtn addTarget:self action:@selector(cancelBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.bordView addSubview:cancelBtn];
      
        self.titlelab.text=@"tiittititiiti";
        [self.bordView addSubview:self.titlelab];
        
        UIView *line=[[UIView alloc]initWithFrame:CGRectMake(0, self.titlelab.ct_bottom+10, SCREEN_WIDTH, 0.5)];
        line.backgroundColor=COLOR_LINE;
        [self.bordView addSubview:line];
        
        
        UserAuthDataModel *userModel=[[UserAuthData shareInstance] userAuthData];
        
        
        [self.bordView addSubview:self.inputTxtf];
        
        UILabel *bankTipLab=[[UILabel alloc]initWithFrame:CGRectMake(20,self.inputTxtf.ct_bottom+5, SCREEN_WIDTH-40, 20)];
        bankTipLab.font=FONT(12);
        bankTipLab.textColor=COLOR_SUBTEXT;
        NSString *bankLastNum=[userModel.bankCard substringWithRange:NSMakeRange(userModel.bankCard.length-4, 4)];
        
        bankTipLab.text=[NSString stringWithFormat:@"使用%@（%@）付款",userModel.bankName,bankLastNum];
        
        [self.bordView addSubview:bankTipLab];
        
        self.bottomTipLab=bankTipLab;
        
        UIButton *ensureBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        ensureBtn.frame=CGRectMake(10, bankTipLab.ct_bottom+5, SCREEN_WIDTH-20, 40);
        ensureBtn.titleLabel.font=FONT(16);
        ensureBtn.backgroundColor=COLOR_GLODBACKGROUD;
        ensureBtn.layer.masksToBounds=YES;
        ensureBtn.layer.cornerRadius=8;
        [ensureBtn setTitle:@"确认" forState:UIControlStateNormal];
        [ensureBtn setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
        [self.bordView addSubview:ensureBtn];
        [ensureBtn addTarget:self action:@selector(ensureBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
    }
    
    return self;
}


-(void)setPayAmountStr:(NSString *)payAmountStr
{
    _payAmountStr=payAmountStr;
    
    NSString *str=[NSString stringWithFormat:@"在线支付¥%@",payAmountStr];
    
    NSMutableAttributedString *att=[[NSMutableAttributedString alloc]initWithString:str];
    [att addAttribute:NSForegroundColorAttributeName value:COLOR_Red range:NSMakeRange(4, payAmountStr.length+1)];
    self.bottomTipLab.hidden=NO;
    self.titlelab.attributedText=att;
    
}

-(void)setPayByBalanceAmountStr:(NSString *)payByBalanceAmountStr
{
    _payAmountStr=payByBalanceAmountStr;
    
    NSString *str=[NSString stringWithFormat:@"余额支付¥%@",payByBalanceAmountStr];
    
    NSMutableAttributedString *att=[[NSMutableAttributedString alloc]initWithString:str];
    [att addAttribute:NSForegroundColorAttributeName value:COLOR_Red range:NSMakeRange(4, payByBalanceAmountStr.length+1)];
    self.titlelab.attributedText=att;
    self.bottomTipLab.hidden=YES;
    
}

-(void)setWithdrawalsAmountStr:(NSString *)withdrawalsAmountStr
{
    _withdrawalsAmountStr=withdrawalsAmountStr;
    NSString *str=[NSString stringWithFormat:@"提现金额¥%@",withdrawalsAmountStr];
    
    NSMutableAttributedString *att=[[NSMutableAttributedString alloc]initWithString:str];
    [att addAttribute:NSForegroundColorAttributeName value:COLOR_Red range:NSMakeRange(4, withdrawalsAmountStr.length+1)];
    self.titlelab.attributedText=att;
    
    self.bottomTipLab.text=@"手续费：5.00元";
    self.bottomTipLab.hidden=NO;
}


-(CMBorderView *)bordView
{
    if (!_bordView) {
        
        _bordView=[[CMBorderView alloc]initWithFrame:CGRectMake(0,SCREEN_HEIGHT-200, SCREEN_WIDTH, 200)];
        _bordView.backgroundColor=COLOR_WHITE;
        _bordView.borderWidth=CMViewBorderWidthMake(0.5, 0, 0.5, 0);
        
        UIButton *clearBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        clearBtn.frame=CGRectMake(0, 0, _bordView.ct_width, _bordView.ct_height);
        clearBtn.backgroundColor=[UIColor clearColor];
         [clearBtn addTarget:self action:@selector(clearBtnClick:) forControlEvents:UIControlEventTouchUpInside];
         [_bordView addSubview:clearBtn];
        
        
    }
    
    return _bordView;
}



-(UILabel *)titlelab
{
    if (!_titlelab)
    {
        _titlelab=[[UILabel alloc]initWithFrame:CGRectMake(60, 10, SCREEN_WIDTH-120, 20)];
        _titlelab.font=FONT(15);
        _titlelab.textAlignment=NSTextAlignmentCenter;
        _titlelab.textColor=COLOR_TEXT;
        
    }
    
    return _titlelab;
    
}

-(UITextField *)inputTxtf
{
    if (!_inputTxtf)
    {
        _inputTxtf=[[UITextField alloc]initWithFrame:CGRectMake(10, 60, SCREEN_WIDTH-20, 35)];
        _inputTxtf.font=FONT(13);
        _inputTxtf.textColor=COLOR_TEXT;
        _inputTxtf.placeholder=@"请输入交易密码";
        _inputTxtf.secureTextEntry=YES;
        _inputTxtf.layer.borderColor=COLOR_LINE.CGColor;
        _inputTxtf.layer.borderWidth=1;
        _inputTxtf.keyboardType=UIKeyboardTypePhonePad;
        _inputTxtf.jk_maxLength = 6;

        
        UILabel *leftlab=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 60, 35)];
        leftlab.font=FONT(13);
        leftlab.textAlignment=NSTextAlignmentCenter;
        leftlab.textColor=COLOR_TEXT;
        leftlab.text=@"交易密码";
        
        _inputTxtf.leftViewMode=UITextFieldViewModeAlways;
        _inputTxtf.leftView=leftlab;

  
        
    }
    
    return _inputTxtf;
}


-(void)show{
    self.inputTxtf.text=@"";
    [self.inputTxtf becomeFirstResponder];
}
#pragma mark--- 取消
-(void)cancelBtnClick:(UIButton *)btn
{
    
    
    if (self.delegate&&[self.delegate respondsToSelector:@selector(payPassWordCancel)]) {
        [self.delegate payPassWordCancel];
    }
  //注释
    
    self.inputTxtf.text=@"";
    
    [self.inputTxtf resignFirstResponder];
    
    
}


#pragma mark---确定
-(void)ensureBtnClick:(UIButton *)btn
{
    if (self.inputTxtf.text.length==0)
    {
        [WSProgressHUD showErrorWithStatus:@"请输入交易密码"];
        return;
    }
    
    [self.inputTxtf resignFirstResponder];
    
    if (self.delegate&&[self.delegate respondsToSelector:@selector(finishWithInputChargePasswordView:)]) {
        [self.delegate finishWithInputChargePasswordView:self.inputTxtf.text];
    }
    
}


#pragma mark---用作点击按钮其他区域键盘消逝的问题
-(void)clearBtnClick:(UIButton *)btn{
    
    
}


@end
