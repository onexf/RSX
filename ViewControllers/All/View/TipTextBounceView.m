//
//  TipTextBounceView.m
//  GoldCatBank
//
//  Created by Sunny on 2017/9/14.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "TipTextBounceView.h"
@interface TipTextBounceView()
@property(nonatomic,strong)UIView *backView;
@end


@implementation TipTextBounceView
-(instancetype)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    if (self)
    {
        
        self.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
        
        UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapClick:)];
        [self addGestureRecognizer:tap];
        
        [self addSubview:self.backView];
    }
    
    return self;
}

-(void)setBankListArr:(NSArray *)bankListArr
{
    _bankListArr=bankListArr;
    
    [self setBankBox];
}

-(void)setBoxOri_y:(CGFloat)boxOri_y
{
    _boxOri_y=boxOri_y;
    self.backView.frame=CGRectMake(self.backView.ct_x, boxOri_y, self.backView.ct_width, self.backView.ct_height);
}


-(UIView *)backView
{
    if (!_backView)
    {
        _backView=[[UIView alloc]initWithFrame:CGRectMake(10, SCREEN_HEIGHT/2.0, SCREEN_WIDTH-20, SCREEN_HEIGHT/3.0)];
        _backView.backgroundColor=COLOR_WHITE;
        _backView.layer.masksToBounds=YES;
        _backView.layer.cornerRadius=8;
    }
    
    return _backView;
    
}


-(void)setBankBox{
    
    
//    CGFloat labWidth=(self.backView.ct_width-20)/3.0;
//    
//    for (int i=0; i<self.bankListArr.count; i++)
//    {
//        
//        BankListModel *model=self.bankListArr[i];
//        UILabel *titleLab=[[UILabel alloc]initWithFrame:CGRectMake((10+labWidth)*(i%3), 20+(20+10)*(i/3), labWidth, 20)];
//        titleLab.textAlignment=NSTextAlignmentCenter;
//        titleLab.font=FONT(13);
//        titleLab.text=model.bankName;
//        titleLab.textColor=COLOR_SUBTEXT;
//        
//        [self.backView addSubview:titleLab];
//        
//        if (i==self.bankListArr.count-1)
//        {
//            self.backView.frame=CGRectMake(self.backView.ct_x, self.backView.ct_y, self.backView.ct_width, titleLab.ct_bottom+20);
//        }
//        
//    }
    
    
}

#pragma mark---
-(void)tapClick:(UITapGestureRecognizer *)tap{
    
    [UIView animateWithDuration:0.5 animations:^{
        
        [self setHidden:YES];
        
        if (_viewHidden) {
            
            _viewHidden();
        }
    }];
    
}

@end
