//
//  PasswordView.h
//  GoldCatBank
//
//  Created by Sunny on 2017/9/8.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^ReturnPasswordStringBlock)(NSString *password);

@interface PasswordView : UIView

@property (copy, nonatomic) ReturnPasswordStringBlock returnPasswordStringBlock;

@end
