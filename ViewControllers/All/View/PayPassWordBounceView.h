//
//  PayPassWordBounceView.h
//  GoldCatBank
//
//  Created by 高丹丹 on 2017/9/8.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol PayPassWordBounceViewDelegate <NSObject>

-(void)finishWithInputChargePasswordView:(NSString *)codeStr;

-(void)payPassWordCancel;
@end
@interface PayPassWordBounceView : UIView
@property(nonatomic,copy)NSString *payAmountStr;//银行卡支付
@property(nonatomic,copy)NSString *payByBalanceAmountStr;//余额支付
@property(nonatomic,copy)NSString *withdrawalsAmountStr;//提现
@property(nonatomic,strong)UITextField *inputTxtf;

@property(nonatomic,weak)id <PayPassWordBounceViewDelegate>delegate;



-(void)show;
@end
