//
//  payCodeInputView.m
//  GoldCatBank
//
//  Created by Sunny on 2017/9/7.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "payCodeInputView.h"
#import "UserAuthData.h"

@interface payCodeInputView()
{
    NSTimer *timer;
    
    NSInteger maxTime;
}

@property(nonatomic,strong)CMBorderView *bordView;

@property(nonatomic,strong)UILabel *titlelab;



@property(nonatomic,strong)UILabel *phoneTiplab;

@property(nonatomic,strong)UIButton *sendCodeBtn;
@end

@implementation payCodeInputView

-(void)dealloc
{
    [self destroy];
}

-(id)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    if (self)
    {
    
         maxTime=60;//倒计时180S
        
        self.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
        
        [self addSubview:self.bordView];
        
        
        UIButton *cancelBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        cancelBtn.frame=CGRectMake(10, 10, 40, 30);
        
        [cancelBtn setImage:[UIImage imageNamed:@"Pay_cancelICon"] forState:UIControlStateNormal];
        [cancelBtn addTarget:self action:@selector(cancelBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.bordView addSubview:cancelBtn];
        
        //self.titlelab.text=@"tiittititiiti";
        [self.bordView addSubview:self.titlelab];
        
        UIView *line=[[UIView alloc]initWithFrame:CGRectMake(0, self.titlelab.ct_bottom+10, SCREEN_WIDTH, 0.5)];
        line.backgroundColor=COLOR_LINE;
        [self.bordView addSubview:line];

        
        UserAuthDataModel *userModel=[[UserAuthData shareInstance] userAuthData];

        NSString *phoneStr =userModel.mobile;
        NSString *phoneFormatStr=[phoneStr stringByReplacingOccurrencesOfString:[phoneStr substringWithRange:NSMakeRange(3,4)]withString:@"*****"];
        
        [self.bordView addSubview:self.phoneTiplab];
        self.phoneTiplab.text=[NSString stringWithFormat:@"短信验证码已发送到您的手机%@",phoneFormatStr];
        
        [self.bordView addSubview:self.inputTxtf];
        
        [self.bordView addSubview:self.sendCodeBtn];
        
        UILabel *bankTipLab=[[UILabel alloc]initWithFrame:CGRectMake(20,self.inputTxtf.ct_bottom+5, SCREEN_WIDTH-40, 20)];
        bankTipLab.font=FONT(12);
        bankTipLab.textColor=COLOR_SUBTEXT;
        NSString *bankLastNum=[userModel.bankCard substringWithRange:NSMakeRange(userModel.bankCard.length-4, 4)];
        
        bankTipLab.text=[NSString stringWithFormat:@"使用%@（%@）付款",userModel.bankName,bankLastNum];
        
        [self.bordView addSubview:bankTipLab];
        
        
        UIButton *ensureBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        ensureBtn.frame=CGRectMake(10, bankTipLab.ct_bottom+5, SCREEN_WIDTH-20, 40);
        ensureBtn.titleLabel.font=FONT(16);
        ensureBtn.backgroundColor=COLOR_GLODBACKGROUD;
        ensureBtn.layer.masksToBounds=YES;
        ensureBtn.layer.cornerRadius=8;
        [ensureBtn setTitle:@"确认" forState:UIControlStateNormal];
        [ensureBtn setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
        [self.bordView addSubview:ensureBtn];
        [ensureBtn addTarget:self action:@selector(ensureBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        

        
    }
    
    return self;
}

-(void)setPayAmountStr:(NSString *)payAmountStr
{
    _payAmountStr=payAmountStr;
    
    NSString *str=[NSString stringWithFormat:@"在线支付¥%@",payAmountStr];
    
    NSMutableAttributedString *att=[[NSMutableAttributedString alloc]initWithString:str];
    [att addAttribute:NSForegroundColorAttributeName value:COLOR_Red range:NSMakeRange(str.length-payAmountStr.length-1, payAmountStr.length+1)];
    self.titlelab.attributedText=att;
    
}



-(CMBorderView *)bordView
{
    if (!_bordView) {
        
        _bordView=[[CMBorderView alloc]initWithFrame:CGRectMake(0,SCREEN_HEIGHT-200, SCREEN_WIDTH, 200)];
        _bordView.backgroundColor=COLOR_WHITE;
        _bordView.borderWidth=CMViewBorderWidthMake(0.5, 0, 0.5, 0);
        
        UIButton *clearBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        clearBtn.frame=CGRectMake(0, 0, _bordView.ct_width, _bordView.ct_height);
        clearBtn.backgroundColor=[UIColor clearColor];
        [clearBtn addTarget:self action:@selector(clearBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_bordView addSubview:clearBtn];
        
        
    }
    
    return _bordView;
}

-(UILabel *)phoneTiplab
{
    if (!_phoneTiplab)
    {
        _phoneTiplab=[[UILabel alloc]initWithFrame:CGRectMake(0, _titlelab.ct_bottom+15,SCREEN_WIDTH,20)];
        _phoneTiplab.font=FONT(12);
        _phoneTiplab.textAlignment=NSTextAlignmentCenter;
        _phoneTiplab.textColor=COLOR_SUBTEXT;
        _phoneTiplab.text=[NSString stringWithFormat:@""];
        

    }
    
   return _phoneTiplab;
}

-(UITextField *)inputTxtf
{
    if (!_inputTxtf)
    {
        _inputTxtf=[[UITextField alloc]initWithFrame:CGRectMake(10, _phoneTiplab.ct_bottom+5, SCREEN_WIDTH-20-90, 35)];
        _inputTxtf.font=FONT(13);
        _inputTxtf.textColor=COLOR_TEXT;
        _inputTxtf.placeholder=@"请输入短信验证码";
        _inputTxtf.layer.borderColor=COLOR_LINE.CGColor;
        _inputTxtf.layer.borderWidth=1;
        _inputTxtf.keyboardType=UIKeyboardTypePhonePad;
        UIView *leftView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 35)];
        _inputTxtf.leftViewMode=UITextFieldViewModeAlways;
        _inputTxtf.leftView=leftView;
        
    }
    
    return _inputTxtf;
}

-(UIButton *)sendCodeBtn
{
    if (!_sendCodeBtn)
    {
        _sendCodeBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        _sendCodeBtn.frame=CGRectMake(_inputTxtf.ct_right, _inputTxtf.ct_top, 90, _inputTxtf.ct_height);
        _sendCodeBtn.backgroundColor=UIColorFromHex(0xdadada);
        [_sendCodeBtn setTitle:[NSString stringWithFormat:@"%ldS后重发",maxTime] forState:UIControlStateNormal];
        _sendCodeBtn.titleLabel.font=FONT(12);
        [_sendCodeBtn addTarget:self action:@selector(sendCodeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _sendCodeBtn;
}

-(UILabel *)titlelab
{
    if (!_titlelab)
    {
        _titlelab=[[UILabel alloc]initWithFrame:CGRectMake(60, 10, SCREEN_WIDTH-120, 20)];
        _titlelab.font=FONT(15);
        _titlelab.textAlignment=NSTextAlignmentCenter;
        _titlelab.textColor=COLOR_TEXT;
        
    }
    
    return _titlelab;
    
}

#pragma mark - actions

- (void)setupTimer
{
    
    timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerAction) userInfo:nil repeats:YES];
    
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
}


- (void)suspend
{
    [timer setFireDate:[NSDate distantFuture]];
}

-(void)start{
    
    
    maxTime=60;
    self.sendCodeBtn.backgroundColor=UIColorFromHex(0xdadada);
    
    self.sendCodeBtn.userInteractionEnabled=NO;
    
    
    static NSInteger firstEnter=0;
    
    if (firstEnter==0)
    {
        firstEnter++;
        [self setupTimer];
        
    }
    else
    {
        if (timer)
        {
            [timer setFireDate:[NSDate distantPast]];
        }
        else
        {
            [self setupTimer];
        }

    }
    
}


#pragma mark--销毁

-(void)destroy
{
    [timer invalidate];
    timer = nil;
 
}

#pragma mark--
-(void)timerAction
{

    maxTime--;
    
    if (maxTime<=0)
    {
        self.sendCodeBtn.userInteractionEnabled=YES;
        self.sendCodeBtn.backgroundColor=COLOR_GLODBACKGROUD;
        [self.sendCodeBtn setTitle:@"发送验证码" forState:UIControlStateNormal];
        [self suspend];
        
    }
    else
    {
        
        [self.sendCodeBtn setTitle:[NSString stringWithFormat:@"%ldS后重发",maxTime] forState:UIControlStateNormal];
    }
   
    
}

#pragma mark---发送验证码
-(void)sendCodeBtnClick:(UIButton *)btn
{
    
    [self start];

     
    
    
    if (self.delegate&&[self.delegate respondsToSelector:@selector(clickSendCodeButton)]) {
        [self.delegate clickSendCodeButton];
    }
    
}

-(void)show{
    self.inputTxtf.text=@"";
   [self.inputTxtf becomeFirstResponder];
    [self start];
}
#pragma mark--- 取消
-(void)cancelBtnClick:(UIButton *)btn
{

    
    self.inputTxtf.text=@"";
    
    [self.inputTxtf resignFirstResponder];
    
    if (self.delegate&&[self.delegate respondsToSelector:@selector(payCodeCancel)]) {
        [self.delegate payCodeCancel];
    }
    

}


#pragma mark---确定
-(void)ensureBtnClick:(UIButton *)btn
{
    if (self.inputTxtf.text.length==0)
    {
        [WSProgressHUD showErrorWithStatus:@"请输入短信验证码"];
        return;
    }
    
    
    [self.inputTxtf resignFirstResponder];
    
    
    if (self.delegate&&[self.delegate respondsToSelector:@selector(finishWithInputCodeView:)]) {
        [self.delegate finishWithInputCodeView:self.inputTxtf.text];
    }


}


#pragma mark---用作点击按钮其他区域键盘消逝的问题
-(void)clearBtnClick:(UIButton *)btn{
    
    
}



@end
