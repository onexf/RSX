//
//  TipTextBounceView.h
//  GoldCatBank
//
//  Created by Sunny on 2017/9/14.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^ViewHiddn)(void);
@interface TipTextBounceView : UIView

@property(nonatomic,strong)NSArray *bankListArr;
@property(nonatomic,assign)CGFloat boxOri_y;

@property(nonatomic,copy)ViewHiddn viewHidden;
@end
