//
//  AllNetWorkRquest.h
//  GoldCatBank
//
//  Created by 高丹丹 on 2017/8/25.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkSingleton.h"
#import "AllListModel.h"
#import "ProductDetailModel.h"
#import "PayModel.h"
#import "BalancePayModel.h"


typedef void(^SuccessResponeBlock)(id responseBody);

typedef void(^FailureResponeBlock)(NSString *error);



////全部列表接口专用
//typedef enum{
//    Big_Default  =-1,//代表不传此参数
//    Big_ZhaoCaiGold = 0,//招财金
//    Big_ShengCaiGold = 1,//生财金
//    Big_TeJiaGold = 2,//特价黄金
//    Big_XinShouTeQuanGold = 3,//新手特权金
//    Big_LingQianGold= 4,//零钱罐
//    Big_CunQianGuan = 5,//存钱罐
//    Big_XianShouZhuanXiang = 6,//新手专享
//    Big_CunQianGuan365ShengJiBan = 7//存钱罐365天升级版
//}BigProductType;



@interface AllNetWorkRquest : NSObject

#pragma mark--全部列表接口


+ (void)allListWithBigProductType:(NSInteger)productType successResponse:(void(^)(NSArray<AllListModel *> *list, NSString *errMsg))response failureBlock:(FailureResponeBlock )failureBlock;

#pragma mark--全部列表页刷新金价借口
+ (void)allListRefreshGoldPriceWithResponse:(void(^)(NSString  *goldPrice, NSString *errMsg))response;

#pragma mark--产品详情接口
+ (void)productDetailWithProductId:(NSString *)productId response:(void(^)(ProductDetailModel  *model, NSString *errMsg))response;


#pragma mark--产品详情接口(有陈功失败)
+ (void)productDetailWithProductId:(NSString *)productId response:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock;

#pragma mark--包括绑定银行卡

+ (void)bankIcarBindWithData:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock;

#pragma mark--提现／充值专用
+ (void)goPayWithPayData:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock;


#pragma mark--下单余额/银行卡支付
+ (void)goPayBalanceAndBankWithPayData:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock;

#pragma mark--下单银行卡支付获得支付码
+ (void)goPayByBankCarWithPayData:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock;

#pragma mark--验证交易密码
+ (void)VerifytradePassword:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock;

#pragma mark--交设置密码
+ (void)setTradePassword:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock;

#pragma mark--查找银行卡所在银行是否被支持
+ (void)searchBankIcarIDIsSupport:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock;

#pragma mark--获取银行卡列表和根据银行的名字获取银行信息
+ (void)getBankInfo:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock;

#pragma mark--根据产品ID获取可用优惠券
+ (void)getUseCouponListByProductId:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock;

@end
