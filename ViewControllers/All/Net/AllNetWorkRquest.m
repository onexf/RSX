//
//  AllNetWorkRquest.m
//  GoldCatBank
//
//  Created by 高丹丹 on 2017/8/25.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "AllNetWorkRquest.h"
#import "MJExtension.h"
#import "BannerModel.h"


//接口拼接
#define UrlAllList            URLString(@"/api/product/AllList")
#define UrlGetReatTimeGoldPrice           URLString(@"/api/home/GetReatTimeGoldPrice")
#define UrlProductDetail            URLString(@"/api/product/Detail")



//银行卡绑定
#define UrlBankVerify            URLString(@"/api/Users/BankVerify")


//支付
#define UrlPreChange            URLString(@"/api/money/PreChange")

//用余额/银行卡支付
#define UrlProductPurchase           URLString(@"/api/product/Purchase")

//用银行卡一键支付
#define UrlMoneyPayAction           URLString(@"/api/money/PayAction")

//验证交易密码
#define UrlVerifytradepwd          URLString(@"/api/Users/verifytradepwd")

//设置交易密码
#define UrlSettradepwd          URLString(@"/api/Users/settradepwd")

//查找银行卡所在银行是否被支持
#define UrlIsSupportByBankCardNum          URLString(@"/api/Users/IsSupportByBankCardNum")

//获取银行卡列表和根据银行的名字获取银行信息
#define UrlGetBanklist          URLString(@"/api/banklist/List")

//根据产品ID获取可用优惠券
#define UrlCanUseCouponListByProductId          URLString(@"/api/Coupon/CanUseCouponListByProductId")


@implementation AllNetWorkRquest


+ (void)allListWithBigProductType:(NSInteger)productType successResponse:(void(^)(NSArray<AllListModel *> *list, NSString *errMsg))response failureBlock:(FailureResponeBlock )failureBlock{

    NSDictionary *dict =nil;
    if (productType>=0)
    {
        dict = @{@"bigProductType" :[NSString stringWithFormat:@"%d",productType]};
    }
    
    [[NetworkSingleton sharedManager] postResultWithParameter:dict url:UrlAllList successBlock:^(id responseBody) {
        NSArray <AllListModel *>*result = [AllListModel mj_objectArrayWithKeyValuesArray:responseBody];
        response(result, nil);
    } failureBlock:^(NSString *error) {
        failureBlock(error);
    }];
}



+ (void)allListRefreshGoldPriceWithResponse:(void(^)(NSString  *goldPrice, NSString *errMsg))response{
    
    [[NetworkSingleton sharedManager] postResultWithParameter:nil url:UrlGetReatTimeGoldPrice successBlock:^(id responseBody) {

        NSString *goldPrice=responseBody[@"realTimeGoldPrice"];

        response(goldPrice, nil);
    } failureBlock:^(NSString *error) {
        response(nil, error);
    }];

}

+ (void)productDetailWithProductId:(NSString *)productId response:(void(^)(ProductDetailModel  *model, NSString *errMsg))response{
    
    NSDictionary *dict =dict = @{@"productId" :[NSNumber numberWithInteger:productId.integerValue]};
    
    [[NetworkSingleton sharedManager] postResultWithParameter:dict url:UrlProductDetail successBlock:^(id responseBody) {
        
        ProductDetailModel *model=[ProductDetailModel mj_objectWithKeyValues:responseBody];
        
        
        response(model, nil);
        
    } failureBlock:^(NSString *error) {
        response(nil, error);
    }];
}


+ (void)productDetailWithProductId:(NSString *)productId response:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock{
    
    NSDictionary *dict =dict = @{@"productId" :[NSNumber numberWithInteger:productId.integerValue]};
    
    [[NetworkSingleton sharedManager] postResultWithParameter:dict url:UrlProductDetail successBlock:^(id responseBody) {
        
        successBlock(responseBody);
        
    } failureBlock:^(NSString *error) {
        
        
        failureBlock(error);
    }];
}

+ (void)bankIcarBindWithData:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock
{
    [[NetworkSingleton sharedManager] postResultWithParameter:dic url:UrlBankVerify successBlock:^(id responseBody) {
        
        successBlock(responseBody);
    } failureBlock:^(NSString *error) {
        
        failureBlock(error);
    }];
}



+ (void)goPayWithPayData:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock{
    
    [[NetworkSingleton sharedManager] postResultWithParameter:dic url:UrlPreChange successBlock:^(id responseBody) {
        
        successBlock(responseBody);

    } failureBlock:^(NSString *error) {
       failureBlock(error);
    }];
}



+ (void)goPayBalanceAndBankWithPayData:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock{
    
    [[NetworkSingleton sharedManager] postResultWithParameter:dic url:UrlProductPurchase successBlock:^(id responseBody) {
        
        successBlock(responseBody);
        
    } failureBlock:^(NSString *error) {
        
        failureBlock(error);
    }];
}

+ (void)goPayByBankCarWithPayData:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock{
    
    
    [[NetworkSingleton sharedManager] postResultWithParameter:dic url:UrlMoneyPayAction successBlock:^(id responseBody) {
        
        successBlock(responseBody);
        
    } failureBlock:^(NSString *error) {
        
        failureBlock(error);
    }];
}

+ (void)VerifytradePassword:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock{
    
    
    [[NetworkSingleton sharedManager] postResultWithParameter:dic url:UrlVerifytradepwd successBlock:^(id responseBody) {
        
        successBlock(responseBody);
        
    } failureBlock:^(NSString *error) {
        
        failureBlock(error);
    }];
}


+ (void)setTradePassword:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock{
    
    
    [[NetworkSingleton sharedManager] postResultWithParameter:dic url:UrlSettradepwd successBlock:^(id responseBody) {
        
        successBlock(responseBody);
        
    } failureBlock:^(NSString *error) {
        
        failureBlock(error);
    }];
}


+ (void)searchBankIcarIDIsSupport:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock{
    
    
    [[NetworkSingleton sharedManager] postResultWithParameter:dic url:UrlIsSupportByBankCardNum successBlock:^(id responseBody) {
        
        successBlock(responseBody);
        
    } failureBlock:^(NSString *error) {
        
        failureBlock(error);
    }];
}


+ (void)getBankInfo:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock{
    
    
    [[NetworkSingleton sharedManager] postResultWithParameter:dic url:UrlGetBanklist successBlock:^(id responseBody) {
        
        successBlock(responseBody);
        
    } failureBlock:^(NSString *error) {
        
        failureBlock(error);
    }];
}



+ (void)getUseCouponListByProductId:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock{
    
    
    [[NetworkSingleton sharedManager] postResultWithParameter:dic url:UrlCanUseCouponListByProductId successBlock:^(id responseBody) {
        
        successBlock(responseBody);
        
    } failureBlock:^(NSString *error) {
        
        failureBlock(error);
    }];
}




@end
