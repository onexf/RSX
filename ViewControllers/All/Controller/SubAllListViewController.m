//
//  SubAllListViewController.m
//  GoldCatBank
//
//  Created by 高丹丹 on 2017/8/25.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "SubAllListViewController.h"
#import "AllNetWorkRquest.h"
#import "AllHomeCell.h"
#import "CBWebViewController.h"
#import "MJRefresh.h"
#import "NewAllHomeCell.h"
@interface SubAllListViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong)UITableView *table;

@property(nonatomic,strong)NSMutableArray *dateList;

@end

@implementation SubAllListViewController
-(void)requestData{
    __weak typeof(self) weakSelf = self;
    
    [AllNetWorkRquest allListWithBigProductType:self.productType successResponse:^(NSArray<AllListModel *> *list, NSString *errMsg) {
        if (errMsg) {
            [weakSelf showErrorView];
        } else {
            [weakSelf hideErrorView];
            
            [self.dateList removeAllObjects];
            
            if (list.count > 0) {
                
                 [self.dateList addObjectsFromArray:list];
                
                [weakSelf.table reloadData];
                [weakSelf.table.mj_header endRefreshing];
                
            }
        }
        
    } failureBlock:^(NSString *error) {
        [WSProgressHUD showErrorWithStatus:error];
        [weakSelf.table.mj_header endRefreshing];
    }];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    //    [self.navigationController.navigationBar jk_setBackgroundColor:COLOR_MAIN];
    self.title=self.productName;
    [self.view addSubview:self.table];
    [self requestData];
    
}
-(NSMutableArray *)dateList
{
    if (!_dateList)
    {
        _dateList=[NSMutableArray array];
    }
    
    return _dateList;
}

-(UITableView *)table
{
    if (!_table)
    {
        _table=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT -NavBarHeight) style:UITableViewStylePlain];
        _table.delegate=self;
        _table.dataSource=self;
        _table.mj_header = [MJDIYHeader headerWithRefreshingBlock:^{
            [self requestData];
        }];
        _table.showsVerticalScrollIndicator=NO;
        _table.tableFooterView=[UIView new];
        _table.rowHeight = 103;
        _table.separatorStyle = UITableViewCellSeparatorStyleNone;
        _table.backgroundColor = COLOR_TBBACK;
    }
    
    return _table;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dateList.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.dateList.count<=indexPath.row)
    {
        return nil;
    }
    NewAllHomeCell *cell = [NewAllHomeCell cellWithTableview:tableView];
    AllListModel *model = [self.dateList objectAtIndex:indexPath.row];
    cell.model = model;
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];

    AllListModel *model=[self.dateList objectAtIndex:indexPath.row];
    if ([model.hasLeaf integerValue]>0)
    {
        SubAllListViewController *vc=[[SubAllListViewController alloc]init];
        vc.productName=model.productName;
        vc.productType=model.bigProductType.intValue;
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        CBWebViewController *vc=[[CBWebViewController alloc]init];
        vc.urlString=model.h5URL;
        vc.productID=model.ID;
        vc.isDisplayNativeBtn=YES;
        [self.navigationController pushViewController:vc animated:YES];
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
