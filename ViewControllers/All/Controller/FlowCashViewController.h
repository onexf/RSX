//
//  FlowCashViewController.h
//  GoldCatBank
//
//  Created by 高丹丹 on 2017/8/24.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ProductDetailModel.h"
#import "GoOrderBaseViewController.h"
@interface FlowCashViewController : GoOrderBaseViewController
@property(nonatomic,strong)ProductDetailModel *detailModel;
@end
