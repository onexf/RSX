//
//  FlowCashViewController.m
//  GoldCatBank
//
//  Created by 高丹丹 on 2017/8/24.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "FlowCashViewController.h"
#import "CBWebViewController.h"
#import "BindBankCarViewController.h"
#import "PayViewController.h"
#import "UserAuthData.h"
#import "TitleScrollView.h"
#import "SetChargePasswordViewController.h"

#define InputTxfTag 10  //第一行输入UITextField的tag
#define ContentLabTag 100  //第二行变化CMHeadContentLab的tag

@interface FlowCashViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    NSMutableArray *dataList;
    BOOL isWeight;//刚开始是按克重

    NSString *glodPrice;//金价
    NSString *totalGlodAmount;//总金价
    NSString *totalGlodWeight;//总重量
    
    TitleScrollView *priceScrollView;

    BOOL isUserAgree;//用户协议同意
}

@property(nonatomic,strong)UITableView *table;

@end

@implementation FlowCashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title=self.detailModel.productName;
    //刚开始是按克重
    
    if (self.detailModel.toPayPageID.integerValue==2)
    {
        //按金额购买
        isWeight=NO;
        
    }
    else
    {
        //按克重购买
        isWeight=YES;
    }

    

    totalGlodAmount=@"0";
    totalGlodWeight=@"0";
    isUserAgree=YES;//默认同意协议
    
    
    
    //bigProductType  0：招财金；1：生财金；2：特价黄金；3：新手特权金；4：零钱罐；5：存钱罐；6：新手专享；7：存钱罐365天升级版；8：保障金；9：黄金及时赚；10：黄金看涨；11：黄金看跌；
    //到期日
    if (self.detailModel.bigProductType.integerValue==2||self.detailModel.bigProductType.integerValue==3)
    {
        //新手特权和特价黄金
        glodPrice=[NSString stringWithFormat:@"%@",self.detailModel.discountPrice];

        
    }
    else
    {
        glodPrice=[NSString stringWithFormat:@"%@",self.detailModel.goldPrice];

        
    }

    
    
    //id 5：特价黄金；6：新手特权金
    
    NSString *interestTimeStr=nil;
    if (self.detailModel.interestTime.length<=0)
    {
        interestTimeStr=@"";
    }
    else
    {
        interestTimeStr=self.detailModel.interestTime;
    }
  
    if (self.detailModel.expirationDate.length>0)
    {
    
        if (self.detailModel.bigProductType.integerValue==2)//修改前：(self.detailModel.ID.integerValue==5
        {
            
            //新手特权和特价黄金需要显示黄金价格
                
            dataList=[NSMutableArray arrayWithArray:@[@{@"计息日":interestTimeStr},@{@"到期日":self.detailModel.expirationDate}]];
            
        }
        else
        {
            
            NSString *sybomlStr=@"%";
        
            if (![self.detailModel.oneDayAnnualizedRate containsString:@"%"]&&![self.detailModel.oneDayAnnualizedRate containsString:@"%"]) {
                
                dataList=[NSMutableArray arrayWithArray:@[@{@"年化利率":[NSString stringWithFormat:@"%@%@",self.detailModel.oneDayAnnualizedRate,sybomlStr]},@{@"计息日":interestTimeStr},@{@"到期日":self.detailModel.expirationDate}]];

            }
            else
            {
                dataList=[NSMutableArray arrayWithArray:@[@{@"年化利率":[NSString stringWithFormat:@"%@",self.detailModel.oneDayAnnualizedRate]},@{@"计息日":interestTimeStr},@{@"到期日":self.detailModel.expirationDate}]];

            }
            
            
        }

    }
    else
    {
        
       if (self.detailModel.bigProductType.integerValue==2)//修改前：(self.detailModel.ID.integerValue==5
        {
            
              dataList=[NSMutableArray arrayWithArray:@[@{@"计息日":interestTimeStr}]];

        }
        else
        {
          
            NSString *sybomlStr=@"%";
                
             if (![self.detailModel.oneDayAnnualizedRate containsString:@"%"]&&![self.detailModel.oneDayAnnualizedRate containsString:@"%"]) {

                 dataList=[NSMutableArray arrayWithArray:@[@{@"年化利率":[NSString stringWithFormat:@"%@%@",self.detailModel.oneDayAnnualizedRate,sybomlStr]},@{@"计息日":interestTimeStr}]];
                 

             }
            else
            {
                dataList=[NSMutableArray arrayWithArray:@[@{@"年化利率":[NSString stringWithFormat:@"%@",self.detailModel.oneDayAnnualizedRate]},@{@"计息日":interestTimeStr}]];
                

            }
           
        }
}

    [self.view addSubview:self.table];
    [self setTableHeadViewAndFootView];
    
}

-(void)setTableHeadViewAndFootView
{

    //bigProductType  0：招财金；1：生财金；2：特价黄金；3：新手特权金；4：零钱罐；5：存钱罐；6：新手专享；7：存钱罐365天升级版；8：保障金；9：黄金及时赚；10：黄金看涨；11：黄金看跌；

    if (self.detailModel.bigProductType.integerValue==2||self.detailModel.bigProductType.integerValue==3)
    {
        //新手特权金和
            self.table.tableHeaderView=[self sepcialTableViewHeadView];
    }
    else
    {
           self.table.tableHeaderView=[self CommonTableViewHeadView];
    }

    
    //表尾
    CMBorderView  *footView=[[CMBorderView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 300)];
    footView.backgroundColor=COLOR_TBBACK;
    footView.borderWidth=CMViewBorderWidthMake(0.5, 0, 0, 0);
    
    UIButton *tipBtn=[[UIButton alloc]initWithFrame:CGRectMake(10, 10, 120, 25)];

    [tipBtn setTitle:@"温馨提示" forState:UIControlStateNormal];
    [tipBtn setTitleColor:COLOR_GLODTEXT forState:UIControlStateNormal];
    [tipBtn setImage:[UIImage imageNamed:@"FlowCash_largeTipIcon"] forState:UIControlStateNormal];
    [tipBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
    tipBtn.titleLabel.font=FONT(14);
    [tipBtn addTarget:self action:@selector(tipBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [footView addSubview:tipBtn];
    
    //确定下单
    UIButton *orderBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    orderBtn.frame=CGRectMake(0, 0, SCREEN_WIDTH-30, 50);
    orderBtn.center=CGPointMake(SCREEN_WIDTH/2.0, tipBtn.ct_bottom+50+50/2.0);
    orderBtn.layer.masksToBounds=YES;
    orderBtn.layer.cornerRadius=8;
    orderBtn.backgroundColor=COLOR_GLODBACKGROUD;
    [orderBtn setTitle:@"确定下单" forState:UIControlStateNormal];
    [orderBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [orderBtn addTarget:self action:@selector(orderBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [footView addSubview:orderBtn];
   
    //同意说明
    NSString *agreeStr=nil;
    
    
    NSString *productName=self.detailModel.productName;
    agreeStr=[NSString stringWithFormat:@"我已阅读并同意%@ 综合服务协议",productName];

    NSMutableAttributedString *agreeAttri=[[NSMutableAttributedString alloc]initWithString:agreeStr];
    [agreeAttri addAttribute:NSForegroundColorAttributeName value:COLOR_TEXT range:NSMakeRange(0, 7)];
    [agreeAttri addAttribute:NSForegroundColorAttributeName value:COLOR_GLODBACKGROUD range:NSMakeRange(7, agreeStr.length-7)];
    [agreeAttri addAttribute:NSFontAttributeName value:FONT(13) range:NSMakeRange(0, agreeStr.length)];

    UIButton *agreeeBtn=[[UIButton alloc]initWithFrame:CGRectMake(0,0,SCREEN_WIDTH, 30)];
    agreeeBtn.center=CGPointMake(SCREEN_WIDTH/2.0, orderBtn.ct_bottom+20);
    [agreeeBtn setAttributedTitle:agreeAttri forState:UIControlStateNormal];
    [agreeeBtn setTitle:agreeStr forState:UIControlStateNormal];
    [agreeeBtn setImage:[UIImage imageNamed:@"FlowCash_selectIcon"] forState:UIControlStateNormal];
    [agreeeBtn setImage:[UIImage imageNamed:@"FlowCash_noSelectIcon"] forState:UIControlStateSelected];
    [agreeeBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    [agreeeBtn addTarget:self action:@selector(greeeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [footView addSubview:agreeeBtn];
    
    //透明button实现查看协议点击
    UIButton *clearCheckBtn=[[UIButton alloc]initWithFrame:CGRectMake(agreeeBtn.ct_width/2.0-10,0,agreeeBtn.ct_width/2.0+10,agreeeBtn.ct_height)];
    clearCheckBtn.backgroundColor=[UIColor clearColor];
    [clearCheckBtn addTarget:self action:@selector(clearCheckBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [agreeeBtn addSubview:clearCheckBtn];
//
//    
//    UILabel *bottomLabel=[[UILabel alloc]initWithFrame:CGRectMake(agreeeBtn.ct_left,agreeeBtn.ct_bottom+5,agreeeBtn.ct_width, 15)];
//    bottomLabel.text=@"中华联合财险承保用户黄金财产安全，请放心交易";
//    bottomLabel.textAlignment=NSTextAlignmentCenter;
//    bottomLabel.textColor=COLOR_SUBTEXT;
//    bottomLabel.font=FONT(13);
//    [footView addSubview:bottomLabel];
    self.table.tableFooterView=footView;
    
    
}

-(CMBorderView *)CommonTableViewHeadView
{
    
    CMBorderView *headview=[[CMBorderView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 120)];
    headview.borderWidth=CMViewBorderWidthMake(0.5, 0, 0.5, 0);
    headview.backgroundColor=COLOR_TBBACK;
    
    
    UILabel *nowTimeLab=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 30)];
    nowTimeLab.textAlignment=NSTextAlignmentCenter;
    nowTimeLab.textColor=COLOR_SUBTEXT;
    nowTimeLab.text=[DateTool getNowTimeString];
    nowTimeLab.font=FONT(12);
    [headview addSubview:nowTimeLab];
    
    priceScrollView=[TitleScrollView cycleScrollViewWithFrame:CGRectMake(0,nowTimeLab.ct_bottom+20, SCREEN_WIDTH, 25)];
    priceScrollView.center=CGPointMake(SCREEN_WIDTH/2.0,headview.ct_height/2.0-15);
    priceScrollView.title=glodPrice;
    priceScrollView.autoScrollTimeInterval=10;
    priceScrollView.titleFont=FONT(30);
    priceScrollView.titleColor=UIColorFromHex(0xf74d3c);
    [headview addSubview:priceScrollView];
    
    
    UILabel *titleLab=[[UILabel alloc]initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, 20)];
    titleLab.center=CGPointMake(SCREEN_WIDTH/2.0,headview.ct_height/2.0+10);
    titleLab.textAlignment=NSTextAlignmentCenter;
    titleLab.text=@"实时金价（元／克）";
    titleLab.font=FONT(14);
    titleLab.textColor=UIColorFromHex(0x999999);
    [headview addSubview:titleLab];
    
    return headview;
    
}


-(CMBorderView *)sepcialTableViewHeadView
{
    
    CMBorderView *headview=[[CMBorderView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 120)];
    headview.borderWidth=CMViewBorderWidthMake(0.5, 0, 0.5, 0);
    headview.backgroundColor=COLOR_TBBACK;
    
    
    UILabel *nowTimeLab=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 30)];
    nowTimeLab.textAlignment=NSTextAlignmentCenter;
    nowTimeLab.textColor=COLOR_SUBTEXT;
    nowTimeLab.text=[DateTool getNowTimeString];
    nowTimeLab.font=FONT(12);
    [headview addSubview:nowTimeLab];
    
    priceScrollView=[TitleScrollView cycleScrollViewWithFrame:CGRectMake(0,nowTimeLab.ct_bottom+20, SCREEN_WIDTH, 25)];
    priceScrollView.center=CGPointMake(SCREEN_WIDTH/2.0,headview.ct_height/2.0-15);
    priceScrollView.title=[NSString stringWithFormat:@"%@",self.detailModel.discountPrice];
    priceScrollView.autoScrollTimeInterval=10;
    priceScrollView.titleFont=FONT(30);
    priceScrollView.titleColor=UIColorFromHex(0xf74d3c);
    [headview addSubview:priceScrollView];
    
    
    UILabel *titleLab=[[UILabel alloc]initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, 20)];
    titleLab.center=CGPointMake(SCREEN_WIDTH/2.0,headview.ct_height/2.0+10);
    titleLab.textAlignment=NSTextAlignmentCenter;
    titleLab.text=@"优惠价（元／克）";
    titleLab.font=FONT(14);
    titleLab.textColor=UIColorFromHex(0x999999);
    [headview addSubview:titleLab];
    
    NSString *glodString=[NSString stringWithFormat:@"实时金价%@元/克",self.detailModel.goldPrice];
    NSMutableAttributedString *att=[[NSMutableAttributedString alloc]initWithString:glodString];

    [att addAttribute:NSForegroundColorAttributeName value:COLOR_Red range:NSMakeRange(glodString.length-3-glodPrice.length, glodPrice.length)];
    
    UILabel *glodPriceLab=[[UILabel alloc]initWithFrame:CGRectMake(0,titleLab.ct_bottom, SCREEN_WIDTH, 20)];
    glodPriceLab.textAlignment=NSTextAlignmentCenter;
    glodPriceLab.font=FONT(14);
    glodPriceLab.textColor=UIColorFromHex(0x999999);
    glodPriceLab.attributedText=att;
    [headview addSubview:glodPriceLab];
    
    
    return headview;
    
}



-(UITableView *)table
{
    if (!_table)
    {
        _table=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-NavBarHeight) style:UITableViewStylePlain];
        _table.delegate=self;
        _table.dataSource=self;
        _table.backgroundColor=COLOR_TBBACK;
        _table.showsVerticalScrollIndicator=NO;
        _table.separatorColor=COLOR_LINE;
    }
    
    return _table;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    if (section==0)
    {
        return 2;
    }
    
    return dataList.count;
    

    
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
   return 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section==1)
    {
        return 10;
    }
    
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section==1)
    {
        CMBorderView *view=[[CMBorderView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 10)];
        view.backgroundColor=COLOR_TBBACK;
        view.borderWidth=CMViewBorderWidthMake(0.5, 0, 0.5, 0);
        return view;

    }
    
    return nil;
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0)
    {
     
        if (indexPath.row==0)
        {
            static NSString *cell0ID=@"cellRow0ID";
            UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cell0ID];
            if (!cell)
            {
                cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell0ID];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
                cell.textLabel.font=FONT(15);
                cell.textLabel.textColor=UIColorFromHex(0x000000);
                
                UITextField *txf=[[UITextField alloc]initWithFrame:CGRectMake(100,0, SCREEN_WIDTH/3.0+10, 44)];
                txf.font=FONT(15);
                txf.delegate=self;
                txf.keyboardType=UIKeyboardTypeDecimalPad;
                txf.textColor=cell.textLabel.textColor;
                
                txf.tag=InputTxfTag;
                [cell.contentView addSubview:txf];
                
                   //bigProductType  0：招财金；1：生财金；2：特价黄金；3：新手特权金；4：零钱罐；5：存钱罐；6：新手专享；7：存钱罐365天升级版；8：保障金；9：黄金及时赚；10：黄金看涨；11：黄金看跌；
                if (self.detailModel.toPayPageID.intValue==0)
                {
                    //按克重购买
                    
                    txf.placeholder=@"请输入购买克重";
                    if (self.detailModel.bigProductType.integerValue==3)
                    {
                        if (self.detailModel.purchaseStartGold.floatValue>0)
                        {
                            if(self.detailModel.purchaseStartGold.floatValue==self.detailModel.purchaseEndGold.floatValue)
                            {
                                txf.placeholder=[NSString stringWithFormat:@"仅可购买%@克",self.detailModel.purchaseStartGold];
                            }
                            else
                            {
                                if (self.detailModel.purchaseEndGold.floatValue>0)
                                {
                                     txf.placeholder=[NSString stringWithFormat:@"最多可购买%@克",self.detailModel.purchaseEndGold];
                                }
                                else
                                {
                                     txf.placeholder=[NSString stringWithFormat:@"%@克起购",self.detailModel.purchaseStartGold];
                                }
                               
                            }
                            
                        }
                        
                        //新手特
                    }
                    else if (self.detailModel.bigProductType.integerValue==2)
                    {
                        //特价黄金
                        if (self.detailModel.purchaseStartGold.floatValue>0)
                        {
                            if(self.detailModel.purchaseStartGold.floatValue==self.detailModel.purchaseEndGold.floatValue)
                            {
                                txf.placeholder=[NSString stringWithFormat:@"仅可购买%@克",self.detailModel.purchaseStartGold];
                            }
                            else
                            {
                                txf.placeholder=[NSString stringWithFormat:@"%@克起购",self.detailModel.purchaseStartGold];
                            }
                            
                            
                        }
                       
                        
                    }
                }
                else if (self.detailModel.toPayPageID.intValue==1)
                {
                    
                    //按克重／金额购买
                    txf.placeholder=@"请输入购买克重";
                    
                    UIView *line=[[UIView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-100,8,0.5,30)];
                    line.backgroundColor=COLOR_GLODBACKGROUD;
                    [cell.contentView addSubview:line];
                    
                    UIButton *swichBtn=[UIButton buttonWithType:UIButtonTypeCustom];
                    swichBtn.frame=CGRectMake(line.ct_right, 0, 80, 44);
                    [swichBtn setTitle:@"切换为\n按金额买" forState:UIControlStateNormal];
                    [swichBtn setTitle:@"切换为\n按克重买" forState:UIControlStateSelected];
                    [swichBtn setTitleColor:COLOR_GLODBACKGROUD forState:UIControlStateNormal];
                    [swichBtn setTitleColor:COLOR_GLODBACKGROUD forState:UIControlStateSelected];
                    swichBtn.titleLabel.font=FONT(13);
                    swichBtn.titleLabel.numberOfLines=0;
                    [swichBtn addTarget:self action:@selector(swichBtnCLick:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.contentView addSubview:swichBtn];
            
                    
                }
                else
                {
                   //self.detailModel.toPayPageID.intValue==2
                    //按金额购买
                     txf.placeholder=@"请输入购买金额";

                }
                
            }
            
            
            if (self.detailModel.toPayPageID.integerValue==2)
            {
                //按金额购买
                cell.textLabel.text=@"金额";
            }
            else
            {
                cell.textLabel.text=@"克重";
            }
            
            
            return cell;

        }
        else
        {
            static NSString *cell0D=@"cellRow1ID";
            UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cell0D];
            if (!cell)
            {
                cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cell0D];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
                cell.textLabel.font=FONT(15);
                cell.textLabel.textColor=UIColorFromHex(0x000000);

                CMHeaderContentLable *lable=[[CMHeaderContentLable alloc] initWithFrame:CGRectMake(15, 0,SCREEN_WIDTH-30, 44)];
                lable.tag=ContentLabTag;
                if (self.detailModel.toPayPageID.integerValue==2)
                {
                    lable.header=@"预计克重";
                }
                else
                {
                    lable.header=@"预计金额";
                }

                
                lable.headerFont=FONT(15);
                lable.contentFont=FONT(15);
                lable.headerTextColor=UIColorFromHex(0x000000);
                lable.contentTextColor=UIColorFromHex(0x000000);
                lable.contentSide=headerContentLable_contentSide_left;
                lable.contentMargin=85;
                
                [cell.contentView addSubview:lable];
   
            }
            
            return cell;

            
        }
        
    }
    else
    {
        if (dataList.count>indexPath.row)
        {
            static NSString *cell1ID=@"cell1ID";
            UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cell1ID];
            if (!cell)
            {
                cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cell1ID];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
                cell.textLabel.font=FONT(15);
                cell.textLabel.textColor=UIColorFromHex(0x000000);
                cell.detailTextLabel.font=FONT(15);
                cell.detailTextLabel.textColor=UIColorFromHex(0x000000);

                
            }
            
            NSDictionary *dic=[dataList objectAtIndex:indexPath.row];
            cell.textLabel.text=[[dic allKeys]lastObject];
            cell.detailTextLabel.text=[[dic allValues]lastObject];
            return cell;

        }
       
        
        
    }
    
    return nil;
    
}



#pragma mark--温馨提示
-(void)tipBtnClick:(UIButton *)btn
{
    
    
    [TipsTool showTipsWithTitle:@"提示" andContent:self.detailModel.tips];
}

#pragma mark--下单按钮
-(void)orderBtnClick:(UIButton *)btn
{
    
    [self.view endEditing:YES];
    if (!isUserAgree)
    {
        [WSProgressHUD showErrorWithStatus:@"请先阅读用户协议并同意后购买！"];
        
        return;
        
    }

    
      
    [self goToBuyWithDetailModel:self.detailModel totalGlodWeight:totalGlodWeight totalGlodAmount:totalGlodAmount isBuyByAmount:!isWeight];
    


}



#pragma mark--同意用户协议
-(void)greeeBtnClick:(UIButton *)btn{
    
    btn.selected=!btn.selected;
    isUserAgree=!isUserAgree;
}

#pragma mark--查看流动金协议
-(void)clearCheckBtnClick:(UIButton *)btn
{
    CBWebViewController *webViewVC=[[CBWebViewController alloc]init];
    webViewVC.urlString=self.detailModel.protocolUrl;
    [self.navigationController pushViewController:webViewVC animated:YES];
    
}

#pragma mark--切换为金额／克重买
-(void)swichBtnCLick:(UIButton *)btn
{
    btn.selected=!btn.selected;
    
    NSIndexPath *index0=[NSIndexPath indexPathForRow:0 inSection:0];
    
    UITableViewCell *cell0=[self.table cellForRowAtIndexPath:index0];
    UITextField *txf=(UITextField *)[cell0 viewWithTag:InputTxfTag];
    
    
    NSIndexPath *index1=[NSIndexPath indexPathForRow:1 inSection:0];
    
    UITableViewCell *cell1=[self.table cellForRowAtIndexPath:index1];
    CMHeaderContentLable *lable=(CMHeaderContentLable *)[cell1 viewWithTag:ContentLabTag];

    if (btn.selected==YES)
    {
        //按金额
        isWeight=NO;
        
        cell0.textLabel.text=@"金额";
        txf.placeholder=@"请输入购买金额";
        txf.text=@"";
        lable.header=@"预计克重";
        lable.content=@"";
    }
    else
    {
        //按克重
        isWeight=YES;
        cell0.textLabel.text=@"克重";
        txf.placeholder=@"请输入购买克重";
        txf.text=@"";
        lable.header=@"预计金额";
        lable.content=@"";
    }
    
    
}

#pragma mark--通知时间
//-(void)glodPriceChangeAction:(NSNotification *)notify
//{
//    NSDictionary *dic=notify.userInfo;
//    
//    NSString *priceStr=[dic objectForKey:Notify_GlodPriceChange];
//    priceScrollView.title=priceStr;
//    
//    [dataList replaceObjectAtIndex:0 withObject:@{@"价格":[NSString stringWithFormat:@"%@元/克",priceStr]}];
//    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:0 inSection:1];
//    
//    [self.table reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
//    
//}

#pragma mark--textFieldDelegate


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
  
    NSString *displayString=nil;
    
    static NSInteger pointNum=0;
    
    if (textField.text.length<=0)
    {
        pointNum=0;  //此处针对输入过小数，然后又消去小数的处理
        //首位不能输入.
        if ([string isEqualToString:@"."])
        {
            return NO;
        }
        
        displayString=string;
    }
    else
    {
        
        
        if (range.location==0)
        {
            //针对输入很多位，返回第一位输入。的情况
            if ([string isEqualToString:@"."])
            {
                return NO;
            }
        }
        
        
        if (![textField.text containsString:@"."]) {
            //此处针对输入过小数，然后又消去小数的处理
            pointNum=0;
        }
        
        if ([string isEqualToString:@"."]) {
            
            //针对返回头部输入小数点 （重量只能输入小数点后三位小数，金额小数点后两位）
            NSString *subText=[textField.text substringFromIndex:range.location];
            
            if (isWeight)
            {
                //按重量
                if ((range.location<textField.text.length)&&(subText.length>3)) {
                    
                    return NO;
                    
                }
                
            }
            else
            {
                //按价格
                if ((range.location<textField.text.length)&&(subText.length>2)) {
                    
                    return NO;
                    
                }
                
            }
            
            //不能输入多个.
            if (pointNum==1)
            {
                return NO;
            }
            
            pointNum++;
            
        }
        else
        {
            //已经有一个.，但此次输入的不是.
            if (pointNum==1)
            {
                NSRange pointRange=[textField.text rangeOfString:@"."];
                if (isWeight)
                {
                    //按重量
                    if (range.location-pointRange.location>3) {
                        
                        return NO;
                    }
                    
                }
                else
                {
                    //按价格
                    
                    if (range.location-pointRange.location>2) {
                        
                        return NO;
                    }
                    
                }
                
                
            }
            
        }
        
        
        if (range.length>0)
        {
            NSMutableString *contentStr=[[NSMutableString alloc]initWithString:textField.text];
            [contentStr deleteCharactersInRange:range];
            displayString=contentStr;
        }
        else
        {
            NSMutableString *contentStr=[[NSMutableString alloc]initWithString:textField.text];
            [contentStr insertString:string atIndex:range.location];
            displayString=contentStr;
        }
        
       
        
    }
    
    
    if (isWeight)
    {
        //按克重
        if (self.detailModel.purchaseEndGold.integerValue>0)
        {
            if (displayString.floatValue>self.detailModel.purchaseEndGold.floatValue)
            {
                textField.text=[NSString stringWithFormat:@"%@",self.detailModel.purchaseEndGold];
                
                displayString=textField.text;
                [self computeWithdisplayString:displayString];
                
                return NO;
            }
            
            
        }
        
    }
    else
    {
        //按金额
        if (self.detailModel.purchaseEndAmount.integerValue>0)
        {
            if (displayString.floatValue>self.detailModel.purchaseEndAmount.floatValue)
            {
                textField.text=[NSString stringWithFormat:@"%@",self.detailModel.purchaseEndAmount];
                
                displayString=textField.text;
                [self computeWithdisplayString:displayString];
                return NO;
            }
            
            
        }
        
    }

    
    
    
    [self computeWithdisplayString:displayString];
    
     return YES;
}



-(void)computeWithdisplayString:(NSString *)displayString
{
    
    //合法输入后
    NSIndexPath *index=[NSIndexPath indexPathForRow:1 inSection:0];
    
    UITableViewCell *cell=[self.table cellForRowAtIndexPath:index];
    CMHeaderContentLable *lable=(CMHeaderContentLable *)[cell viewWithTag:ContentLabTag];
    
    NSString *row0String=displayString;//克数或金额
    
    NSDecimalNumber *displayNum=[NSDecimalNumber decimalNumberWithString:displayString];
    
    if (row0String.length>0)
    {
        if (isWeight==YES)
        {
            //按克重
            //得出金额保留两位小数
            
            NSString *amount=[PriceTool numberA:glodPrice MultiplyNumberB:row0String withRoundingStyle:NSRoundDown decimalNum:2];
            lable.content=[NSString stringWithFormat:@"%@元",amount];
            
            totalGlodAmount=amount;
            
            totalGlodWeight=[displayNum stringValue];
            
        }
        else
        {
            //按金额
            //得出克数保留三位小数
            lable.content=[NSString stringWithFormat:@"%@克",[PriceTool numberA:row0String DivideNumberB:glodPrice withRoundingStyle:NSRoundDown decimalNum:3]];
            
            totalGlodWeight=[PriceTool numberA:row0String DivideNumberB:glodPrice withRoundingStyle:NSRoundDown decimalNum:3];
            totalGlodAmount=[displayNum stringValue];
        }
    }
    else
    {
        if (isWeight==YES)
        {
            //按克重
            //得出金额保留两位小数
  
            lable.content=[NSString stringWithFormat:@"%@元",@"0.00"];
            
            totalGlodAmount=@"0.00";
            
            totalGlodWeight=@"0.000";
            
        }
        else
        {
            //按金额
            //得出克数保留三位小数
            lable.content=[NSString stringWithFormat:@"%@克",@"0.000"];
            
            totalGlodWeight=@"0.000";
            totalGlodAmount=@"0.00";
        }

        
    }
    

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [priceScrollView start];
}


-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [priceScrollView suspend];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
