//
//  ProductDetailWebViewVC.m
//  GoldCatBank
//
//  Created by 高丹丹 on 2017/8/25.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "ProductDetailWebViewVC.h"
#import "FlowCashViewController.h"//招财金
#import "SaveCashViewController.h"//存钱罐
#import "ProductDetailModel.h"
#import "AllNetWorkRquest.h"
#import <WebKit/WebKit.h>
#import "ApplyBuyOrderViewController.h"
#import "UIBarButtonItem+item.h"
#import "CBWebViewController.h"
#import "ONEShareTool.h"
#import "GoldUpAndDownViewController.h"
#import "SavePotViewController.h"
#import "GoBuyTool.h"
@interface ProductDetailWebViewVC ()<WKScriptMessageHandler,WKNavigationDelegate,WKUIDelegate>
//webView
@property(nonatomic,strong)WKWebView *webView;
//进度条
@property(nonatomic,strong)UIProgressView *progressView;

@property(nonatomic,strong)ProductDetailModel *detailModel;
@end

@implementation ProductDetailWebViewVC


-(void)dealloc
{
    //移除监听(在viewWilldisapear会崩溃)
    [self.webView removeObserver:self forKeyPath:@"loading"];
    [self.webView removeObserver:self forKeyPath:@"title"];
    [self.webView removeObserver:self forKeyPath:@"estimatedProgress"];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 请求详情页接口
     [self requestProductDetailDataWithbutton:nil];
    
    [self.view addSubview:self.webView];
    [self.view addSubview:self.progressView];
    //添加KVO监听
    [self.webView addObserver:self
                   forKeyPath:@"loading"
                      options:NSKeyValueObservingOptionNew
                      context:nil];
    [self.webView addObserver:self
                   forKeyPath:@"title"
                      options:NSKeyValueObservingOptionNew
                      context:nil];
    [self.webView addObserver:self
                   forKeyPath:@"estimatedProgress"
                      options:NSKeyValueObservingOptionNew
                      context:nil];

    
//    self.navigationItem.leftBarButtonItem = [UIBarButtonItem backItemWithimage:[UIImage imageNamed:@"navigationButtonReturn"] highImage:[UIImage imageNamed:@"navigationButtonReturnClick"]  target:self action:@selector(back) title:@""];
    UIButton *backBarButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    [backBarButton setImage:[UIImage imageNamed:@"navigationButtonReturn"] forState:UIControlStateNormal];
    [backBarButton setImage:[UIImage imageNamed:@"navigationButtonReturnClick"] forState:UIControlStateHighlighted];
    backBarButton.jk_touchAreaInsets = UIEdgeInsetsMake(20, 20, 20, 20);
    backBarButton.imageEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 25);
    [backBarButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backBarButton];


}

- (void)back {
    if (self.webView.canGoBack) {
        [self.webView goBack];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


-(WKWebView *)webView
{
    if (!_webView)
    {
        WKWebViewConfiguration *config = [WKWebViewConfiguration new];
        //初始化偏好设置属性：preferences
        config.preferences = [WKPreferences new];
        //The minimum font size in points default is 0;
        config.preferences.minimumFontSize = 10;
        //是否支持JavaScript
        config.preferences.javaScriptEnabled = YES;
        //不通过用户交互，是否可以打开窗口
        config.preferences.javaScriptCanOpenWindowsAutomatically = NO;
        //通过JS与webView内容交互
        config.userContentController = [WKUserContentController new];
        // 注入JS对象名称senderModel，当JS通过senderModel来调用时，我们可以在WKScriptMessageHandler代理中接收到
        //    [config.userContentController addScriptMessageHandler:self name:@"senderModel"];
        _webView = [[WKWebView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-NavBarHeight) configuration:config];
        [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.productURL]]];
        
        _webView.navigationDelegate = self;
    }
    
    return _webView;
}

-(UIProgressView *)progressView
{
    if (!_progressView)
    {
        _progressView=[[UIProgressView alloc]initWithFrame:self.view.bounds];
        _progressView.backgroundColor = [UIColor blackColor];
        _progressView.progressTintColor = [UIColor greenColor];
    }
    return _progressView;
}



#pragma mark - KVO监听函数
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    if ([keyPath isEqualToString:@"title"]) {
        self.title = self.webView.title;
    }else if([keyPath isEqualToString:@"loading"]){
        NSLog(@"loading");
    }
    else if ([keyPath isEqualToString:@"estimatedProgress"]){
        //estimatedProgress取值范围是0-1;
        self.progressView.progress = self.webView.estimatedProgress;
    }
    
    if (!self.webView.loading) {
        [UIView animateWithDuration:0.5 animations:^{
            self.progressView.alpha = 0;
        }];
    }
    
}


//js给oc传参
#pragma mark - WKScriptMessageHandler
-(void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message{
    //这里可以通过name处理多组交互
    if ([message.name isEqualToString:@"senderModel"]) {
        //body只支持NSNumber, NSString, NSDate, NSArray,NSDictionary 和 NSNull类型
        NSLog(@"message.body===%@",message.body);
    }
    
}

#pragma mark = WKNavigationDelegate
//在发送请求之前，决定是否跳转
-(void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler{
    
    if ([navigationAction.request.URL.absoluteString isEqualToString:self.productURL]) {
        decisionHandler(WKNavigationActionPolicyAllow);
    }
    else
    {
        
        if (self.productID.integerValue==6)
        {
            //新手特权金
            decisionHandler(WKNavigationActionPolicyCancel);
            CBWebViewController *webViewController = [[CBWebViewController alloc] init];
            webViewController.showShareButton = YES;
            webViewController.urlString = navigationAction.request.URL.absoluteString;
            [self.navigationController pushViewController:webViewController animated:YES];
       
        }
        else
        {
            decisionHandler(WKNavigationActionPolicyAllow);
        }
    }
//
//    NSString *hostname = navigationAction.request.URL.host.lowercaseString;
//    NSLog(@"%@",hostname);
//    if (navigationAction.navigationType == WKNavigationTypeLinkActivated
//        && ![hostname containsString:@".baidu.com"]) {
//        // 对于跨域，需要手动跳转
////        [[UIApplication sharedApplication] openURL:navigationAction.request.URL];
//        
//        // 不允许web内跳转
//        decisionHandler(WKNavigationActionPolicyAllow);
//    } else {
//        self.progressView.alpha = 1.0;
//        decisionHandler(WKNavigationActionPolicyAllow);
//    }
//    
    
}
//在响应完成时，调用的方法。如果设置为不允许响应，web内容就不会传过来

-(void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler{
    decisionHandler(WKNavigationResponsePolicyAllow);
}
//接收到服务器跳转请求之后调用
-(void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(WKNavigation *)navigation{
    
}

//开始加载时调用
-(void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation{
    
}
//当内容开始返回时调用
-(void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation{
    
}
//页面加载完成之后调用
-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    
    NSLog(@"title:%@",webView.title);
}
// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation
{
    
}



-(void)setNewBuyBtn{
    
    
    UIView *bottomView=[[UIView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT-NavBarHeight-50, SCREEN_WIDTH, 50)];
    bottomView.backgroundColor=COLOR_TBBACK;
    [self.view addSubview:bottomView];
    
    UIButton *buyBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    buyBtn.frame=CGRectMake(10,7, SCREEN_WIDTH/2.0-30, 35);
    buyBtn.backgroundColor=COLOR_GLODBACKGROUD;
    [buyBtn setTitle:@"买!" forState:UIControlStateNormal];
    buyBtn.titleLabel.font=FONT(15);
    buyBtn.layer.masksToBounds=YES;
    buyBtn.layer.cornerRadius=10;
    [buyBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [buyBtn addTarget:self action:@selector(buyBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:buyBtn];


    
    
    //新手特权
    UIButton *shareBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    shareBtn.frame=CGRectMake(SCREEN_WIDTH/2.0+20,buyBtn.ct_top, buyBtn.ct_width, 35);
    shareBtn.backgroundColor=COLOR_GLODBACKGROUD;
    [shareBtn setTitle:@"分享给朋友" forState:UIControlStateNormal];
    shareBtn.titleLabel.font=FONT(15);
    shareBtn.layer.masksToBounds=YES;
    shareBtn.layer.cornerRadius=10;
    [shareBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [shareBtn addTarget:self action:@selector(shareBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:shareBtn];
    
}
-(void)setBuyBtn:(NSString *)isApplyStr{
    //买入按钮
    UIButton *buyBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    buyBtn.frame=CGRectMake(0,SCREEN_HEIGHT-NavBarHeight-50, SCREEN_WIDTH, 50);
    buyBtn.backgroundColor=COLOR_MAIN;
    if (isApplyStr.integerValue>0)
    {
        [buyBtn setTitle:@"立即申购" forState:UIControlStateNormal];
    }
    else
    {
        [buyBtn setTitle:@"立即买入" forState:UIControlStateNormal];
    }
    
    [buyBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [buyBtn addTarget:self action:@selector(buyBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:buyBtn];
    
}

#pragma mark--立即买入
-(void)buyBtnClick:(UIButton *)btn{
    
    if (!isLogin())
    {
        [LoginTool loginAction];
        return;
    }
    
    
    if (self.detailModel)
    {
        UIViewController *vc=[GoBuyTool goBuyChoosePageWithDetailModel:self.detailModel];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        btn.userInteractionEnabled=NO;
        [self requestProductDetailDataWithbutton:btn];
        
    }
    
   
}

#pragma mark--分享
-(void)shareBtnClick:(UIButton *)btn
{
    if (isLogin()) {
        [[ONEShareTool sharedInstance] showShareViewWithShareDict:@{
                                                                        @"url" : @"https://www.zhaojinmao.cn/testwebapp/index/Inviting/Inviting1.html",
                                                                        @"title" : @"新手专享",
                                                                        @"content" : @"邀请好友"
                                                                    }];
    } else {
        [LoginTool loginAction];
    }
}
#pragma mark--goback按钮
-(void)goback{
    if ([self.webView canGoBack]) {
        [self.webView goBack];
        NSLog(@"back");
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


-(void)requestProductDetailDataWithbutton:(UIButton *)btn
{
    //产品大类 0：招财金；1：生财金；2：特价黄金；3：新手特权金；4：零钱罐；5：存钱罐；6：新手专享；7：存钱罐365天升级版；8：保障金；9：黄金及时赚；10：黄金看涨；11：黄金看跌；
    
     __weak typeof(self) weakSelf = self;
    
    [AllNetWorkRquest productDetailWithProductId:self.productID response:^(id responseBody) {
        
        //NSLog(@"responseBody======%@",responseBody);
         weakSelf.detailModel=[ProductDetailModel mj_objectWithKeyValues:responseBody];
        
        //bigProductType:3：新手特权金
            if (weakSelf.detailModel.bigProductType.integerValue==3)
            {
                   [self setNewBuyBtn];
            }
            else
            {
                [self setBuyBtn:weakSelf.detailModel.isApply];
            }

        
        if (btn)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIViewController *vc=[GoBuyTool goBuyChoosePageWithDetailModel:weakSelf.detailModel];
                [self.navigationController pushViewController:vc animated:YES];
                btn.userInteractionEnabled=YES;
                
            });
 
        }
       
    } failureBlock:^(NSString *error) {
        
        if (btn)
        {
            btn.userInteractionEnabled=YES;
        }
        
         [WSProgressHUD showErrorWithStatus:error];
        
    }];
    
}



//- (void)dealloc{
//    //这里需要注意，前面增加过的方法一定要remove掉。
//    [userContentController removeScriptMessageHandlerForName:@"showMobile"];
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
