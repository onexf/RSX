//
//  ProductDetailWebViewVC.h
//  GoldCatBank
//
//  Created by 高丹丹 on 2017/8/25.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "BaseViewController.h"

@interface ProductDetailWebViewVC : BaseViewController
@property(nonatomic,copy)NSString *productURL;
@property(nonatomic,copy)NSString *productID;
@property(nonatomic,copy)NSString *isApply;

@end
