//
//  SubAllListViewController.h
//  GoldCatBank
//
//  Created by 高丹丹 on 2017/8/25.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "BaseViewController.h"
#import "AllNetWorkRquest.h"
@interface SubAllListViewController : BaseViewController
@property(nonatomic,assign)NSInteger productType;
@property(nonatomic,copy)NSString *productName;
@end
