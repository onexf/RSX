//
//  BaseTableViewController.h
//  GoldCatBank
//
//  Created by Sunny on 2017/11/1.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BlankPageView.h"
@interface BaseTableViewController : UITableViewController

@property(nonatomic,strong)BlankPageView *blankView;


-(void)showBlankPageWithType:(EaseBlankPageType)type withFrame:(CGRect)frame;
-(void)showBlankPageWithType:(EaseBlankPageType)type withFrame:(CGRect)frame switchView:(UIView *)switchView;
-(void)showBlankPageWithType:(EaseBlankPageType)type withFrame:(CGRect)frame reloadButtonBlock:(void (^)(id))block;
@end
