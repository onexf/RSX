//
//  BaseViewController.m
//  GoldCatBank
//
//  Created by 高丹丹 on 2017/8/25.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = COLOR_TBBACK;
    // Do any additional setup after loading the view.
}

-(void)showBlankPageWithType:(EaseBlankPageType)type withFrame:(CGRect)frame{
    [self dismissBlankPage];
    _blankView=[[BlankPageView alloc] initWithFrame:frame];
    
    [_blankView configWithType:type reloadButtonBlock:nil];
    [self.view addSubview:_blankView];
}

-(void)showBlankPageWithType:(EaseBlankPageType)type withFrame:(CGRect)frame switchView:(UIView *)switchView{
    [self dismissBlankPage];
    BlankPageView * blankView=[[BlankPageView alloc] initWithFrame:frame withSwitchView:switchView];
    [blankView configWithType:type reloadButtonBlock:nil];
    [self.view addSubview:blankView];
    self.blankView=blankView;
    self.blankView.hidden=NO;
}
-(void)showBlankPageWithType:(EaseBlankPageType)type withFrame:(CGRect)frame withSuperView:(UIView *)superView{
    [self dismissBlankPage];
    _blankView=[[BlankPageView alloc] initWithFrame:frame];
    
    [_blankView configWithType:type reloadButtonBlock:nil];
    [superView addSubview:_blankView];
}
-(void)showBlankPageWithType:(EaseBlankPageType)type withFrame:(CGRect)frame reloadButtonBlock:(void (^)(id))block{
    [self dismissBlankPage];
    _blankView=[[BlankPageView alloc] initWithFrame:frame];
    
    [_blankView configWithType:type reloadButtonBlock:block];
    [self.view addSubview:_blankView];
    
}
-(void)dismissBlankPage{
    if (_blankView) {
        [_blankView removeFromSuperview];
        _blankView=nil;
    }
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
