//
//  BaseTableViewController.m
//  GoldCatBank
//
//  Created by Sunny on 2017/11/1.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "BaseTableViewController.h"

@interface BaseTableViewController ()

@end

@implementation BaseTableViewController


-(void)showBlankPageWithType:(EaseBlankPageType)type withFrame:(CGRect)frame{
    [self dismissBlankPage];
    _blankView=[[BlankPageView alloc] initWithFrame:frame];
    
    [_blankView configWithType:type reloadButtonBlock:nil];
    [self.view addSubview:_blankView];
}

-(void)showBlankPageWithType:(EaseBlankPageType)type withFrame:(CGRect)frame switchView:(UIView *)switchView{
    [self dismissBlankPage];
    BlankPageView * blankView=[[BlankPageView alloc] initWithFrame:frame withSwitchView:switchView];
    [blankView configWithType:type reloadButtonBlock:nil];
    [self.view addSubview:blankView];
    self.blankView=blankView;
    self.blankView.hidden=NO;
}
-(void)showBlankPageWithType:(EaseBlankPageType)type withFrame:(CGRect)frame withSuperView:(UIView *)superView{
    [self dismissBlankPage];
    _blankView=[[BlankPageView alloc] initWithFrame:frame];
    
    [_blankView configWithType:type reloadButtonBlock:nil];
    [superView addSubview:_blankView];
}
-(void)showBlankPageWithType:(EaseBlankPageType)type withFrame:(CGRect)frame reloadButtonBlock:(void (^)(id))block{
    [self dismissBlankPage];
    _blankView=[[BlankPageView alloc] initWithFrame:frame];
    
    [_blankView configWithType:type reloadButtonBlock:block];
    [self.view addSubview:_blankView];
    
}
-(void)dismissBlankPage{
    if (_blankView) {
        [_blankView removeFromSuperview];
        _blankView=nil;
    }
}



@end
