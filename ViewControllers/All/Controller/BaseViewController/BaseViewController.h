//
//  BaseViewController.h
//  GoldCatBank
//
//  Created by 高丹丹 on 2017/8/25.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BlankPageView.h"
@interface BaseViewController : UIViewController

@property(nonatomic,strong)BlankPageView *blankView;

-(void)showBlankPageWithType:(EaseBlankPageType)type withFrame:(CGRect)frame;
-(void)showBlankPageWithType:(EaseBlankPageType)type withFrame:(CGRect)frame switchView:(UIView *)switchView;
-(void)showBlankPageWithType:(EaseBlankPageType)type withFrame:(CGRect)frame reloadButtonBlock:(void (^)(id))block;
@end
