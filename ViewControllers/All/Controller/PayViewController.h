//
//  PayViewController.h
//  GoldCatBank
//
//  Created by Sunny on 2017/8/30.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "BaseViewController.h"
#import "ProductDetailModel.h"

@interface PayViewController : BaseViewController
@property(nonatomic,strong)ProductDetailModel *detailModel;
@property(nonatomic,copy)NSString *weight;
@property(nonatomic,copy)NSString *amount;
@property(nonatomic,assign)BOOL isBuyByAmount;
@end
