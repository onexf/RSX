//
//  SetChargePasswordViewController.m
//  GoldCatBank
//
//  Created by 高丹丹 on 2017/9/8.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "SetChargePasswordViewController.h"
#import "AllNetWorkRquest.h"
#import "UserAuthData.h"
#import "payPasswordBoxView.h"
#import "UIBarButtonItem+item.h"
@interface SetChargePasswordViewController ()
{
    
    payPasswordBoxView *newPswBoxView;
    payPasswordBoxView *aginNewPswBoxView;
}

@end

@implementation SetChargePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title=@"设置交易密码";
//    self.navigationItem.leftBarButtonItem = [UIBarButtonItem backItemWithimage:[UIImage imageNamed:@"navigationButtonReturn"] highImage:[UIImage imageNamed:@"navigationButtonReturnClick"]  target:self action:@selector(back) title:@""];
    UIButton *backBarButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    [backBarButton setImage:[UIImage imageNamed:@"navigationButtonReturn"] forState:UIControlStateNormal];
    [backBarButton setImage:[UIImage imageNamed:@"navigationButtonReturnClick"] forState:UIControlStateHighlighted];
    backBarButton.jk_touchAreaInsets = UIEdgeInsetsMake(20, 20, 20, 20);
    backBarButton.imageEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 25);
    [backBarButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backBarButton];


    self.view.backgroundColor=COLOR_TBBACK;
    [self initContentView];
}



-(void)initContentView{

    newPswBoxView=[[payPasswordBoxView alloc]initWithFrame:CGRectMake(0,20,SCREEN_WIDTH,100)];
    newPswBoxView.title=@"请设置支付密码";
    newPswBoxView.titleFont=FONT(16);
    newPswBoxView.titleColor=COLOR_SUBTEXT;
    newPswBoxView.backgroundColor=[UIColor clearColor];
    [self.view addSubview:newPswBoxView];
    
    aginNewPswBoxView=[[payPasswordBoxView alloc]initWithFrame:CGRectMake(newPswBoxView.ct_left,newPswBoxView.ct_bottom+10,newPswBoxView.ct_width, newPswBoxView.ct_height)];
    aginNewPswBoxView.title=@"请再次确认输入";
    aginNewPswBoxView.titleFont=FONT(16);
    aginNewPswBoxView.titleColor=COLOR_SUBTEXT;
    aginNewPswBoxView.backgroundColor=[UIColor clearColor];
    [self.view addSubview:aginNewPswBoxView];
    
    
    UILabel *titleLab=[[UILabel alloc]initWithFrame:CGRectMake(aginNewPswBoxView.ct_left+20, aginNewPswBoxView.ct_bottom+20, aginNewPswBoxView.ct_width-40, 20)];
    titleLab.text=@"设置密码提示";
    titleLab.textColor=COLOR_SUBTEXT;
    titleLab.font=FONT(14);
    [self.view addSubview:titleLab];
    
    
    NSString *tipString=@"1、支付密码是您在进行交易时的身份证，交易过程中输入交易密码，我方即认为您是本人操作。请妥善保管您的密码信息。\n2、密码设定为6位数字，如果您在交易时，输入密码错误次数超过3次，我们将冻结您的交易活动，您可以在第二天进行密码更改或找回。";
    UILabel *tipContentLab=[[UILabel alloc]initWithFrame:CGRectMake(titleLab.ct_left, titleLab.ct_bottom, titleLab.ct_width, 100)];
    tipContentLab.text=tipString;
    tipContentLab.font=FONT(12);
    tipContentLab.textColor=COLOR_SUBTEXT;
    tipContentLab.numberOfLines=0;
    [self.view addSubview:tipContentLab];
 
    
    UIButton *ensureBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    ensureBtn.frame=CGRectMake(0,SCREEN_HEIGHT-NavBarHeight-50, SCREEN_WIDTH, 50);
    ensureBtn.titleLabel.font=FONT(16);
    ensureBtn.backgroundColor=COLOR_GLODBACKGROUD;
    [ensureBtn setTitle:@"确定" forState:UIControlStateNormal];
    [ensureBtn setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
    [self.view addSubview:ensureBtn];
    [ensureBtn addTarget:self action:@selector(ensureBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    

  
}





-(void)ensureBtnClick:(UIButton *)btn
{
    
    if (newPswBoxView.inputTxf.text.length<6)
    {
        [WSProgressHUD showErrorWithStatus:@"请输入六位的密码"];
        return;
    }
    
    if (![newPswBoxView.inputTxf.text isEqualToString:aginNewPswBoxView.inputTxf.text]) {
        
        [WSProgressHUD showErrorWithStatus:@"两次密码请输入一致"];
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    
    NSDictionary *dic=@{@"pwd":newPswBoxView.inputTxf.text};
    
    [WSProgressHUD showWithMaskType:WSProgressHUDMaskTypeBlack];
    
    [AllNetWorkRquest setTradePassword:dic successBlock:^(id responseBody) {
        [WSProgressHUD dismiss];
        
        if (responseBody[@"results"])
        {
            if ([responseBody[@"results"] integerValue]>0)
            {
                [WSProgressHUD showSuccessWithStatus:@"设置密码成功"];
                
                if (self.isBind)
                {
                    NSArray *vcArr=weakSelf.navigationController.viewControllers;
                    
                    UIViewController *presentVC=(UIViewController *)vcArr[vcArr.count-3];
                    [weakSelf.navigationController popToViewController:presentVC animated:YES];
                    
                  
                }
                else
                {
                      [self.navigationController popViewControllerAnimated:YES];
                }
           
                [[UserAuthData shareInstance] refreshUserInfoResponse:^(UserAuthDataModel *userAuthData, NSString *errMsg) {

                    
                }];
            }
            else
            {
                [WSProgressHUD showErrorWithStatus:responseBody[@"errMsg"]] ;
            }
        }
        
        
    } failureBlock:^(NSString *error) {
        
        [WSProgressHUD dismiss];
    }];

    
}


-(void)back
{

    [WSProgressHUD showErrorWithStatus:@"为了您的账户安全，请设置支付密码！"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
