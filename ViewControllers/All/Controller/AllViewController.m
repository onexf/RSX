//
//  AllViewController.m
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/8/21.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "AllViewController.h"

#import "AllHomeCell.h"
#import "AllListModel.h"
#import "ProductDetailWebViewVC.h"
#import "SubAllListViewController.h"
#import "MJRefresh.h"
#import "AllNetWorkRquest.h"
#import "CBWebViewController.h"
#import <FDFullscreenPopGesture/UINavigationController+FDFullscreenPopGesture.h>
#import "SetChargePasswordViewController.h"
#import "JX_GCDTimerManager.h"
#import "CWCalendarLabel.h"
#import "UIView+Extension.h"
#import "NewAllHomeCell.h"

static NSString * const myTimer = @"AllMyTimer";

@interface AllViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong)UITableView *table;

@property(nonatomic,strong)NSMutableArray *dateList;
/** titleView */
@property(nonatomic, strong) UIView *titleView;
/** 金价 */
@property(nonatomic, strong) CWCalendarLabel *goldPrice;
/** 刷新 */
@property(nonatomic, strong) UIButton *refImageView;
/** 实时金价(元/克) */
@property(nonatomic, strong) UILabel *unitLabel;

@end

@implementation AllViewController

- (void)requestData {
    
    __weak typeof(self) weakSelf = self;
    NSInteger bigProductType=-1;//  代表全部
    [AllNetWorkRquest allListWithBigProductType:bigProductType successResponse:^(NSArray<AllListModel *> *list, NSString *errMsg) {
        if (errMsg) {
            [weakSelf showErrorView];
        } else {
            [weakSelf hideErrorView];
            
             [self.dateList removeAllObjects];

            if (list.count > 0) {
                
                NSMutableArray *section1Arr=[NSMutableArray array];
                NSMutableArray *section2Arr=[NSMutableArray array];
                for (AllListModel *model in list) {
                    
                    if ([model.isGoldProduct integerValue]>0) {
                        
                        [section1Arr addObject:model];
                    }
                    else
                    {
                        [section2Arr addObject:model];
                    }
                    
                }
                
                if (section1Arr.count>0)
                {
                    [self.dateList addObject:section1Arr];
                }
                
                if (section2Arr.count>0)
                {
                    [self.dateList addObject:section2Arr];
                    
                }
                
                [weakSelf.table reloadData];
                [weakSelf.table.mj_header endRefreshing];
                
            }
        }

    } failureBlock:^(NSString *error) {

        [weakSelf.table.mj_header endRefreshing];
    }];
   
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"全部";
    self.fd_prefersNavigationBarHidden = YES;
    
    [self setNavTitle];
    [self.view addSubview:self.table];
    [self requestData];
}
-(NSMutableArray *)dateList
{
    if (!_dateList)
    {
        _dateList=[NSMutableArray array];
    }
    
    return _dateList;
}

-(UITableView *)table
{
    if (!_table)
    {
        _table=[[UITableView alloc]initWithFrame:CGRectMake(0, NavBarH + 24, SCREEN_WIDTH, SCREEN_HEIGHT - TabBarHeight - NavBarH - 24) style:UITableViewStylePlain];
        _table.mj_header = [MJDIYHeader headerWithRefreshingBlock:^{
            [self requestData];
        }];
        _table.backgroundColor = COLOR_TBBACK;
        _table.delegate=self;
        _table.dataSource=self;
        _table.showsVerticalScrollIndicator=NO;
        _table.tableFooterView=[UIView new];
        _table.rowHeight = 103;
        _table.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    
    return _table;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.dateList.count>section)
    {
        NSArray *arr=(NSArray *)self.dateList[section];
        return arr.count;
    }
    
    return 0;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dateList.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.dateList.count<=indexPath.section)
    {
        return nil;
    }
    
    NSArray *array=self.dateList[indexPath.section];
    
    if (array.count<=indexPath.row)
    {
        return nil;
    }
    NewAllHomeCell *cell = [NewAllHomeCell cellWithTableview:tableView];
    AllListModel *model = [array objectAtIndex:indexPath.row];
    cell.model = model;
//    [cell fillCellWithModel:model];
    return cell;
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CMBorderView*view=[[CMBorderView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 40)];
    view.backgroundColor=UIColorFromHex(0xf5f5f5);
    
    UILabel *titleLab=[[UILabel alloc]initWithFrame:CGRectMake(16, 0, SCREEN_WIDTH-20, 40)];
    titleLab.textColor=UIColorFromHex(0x333333);

    if (section==0)
    {
        titleLab.text=@"黄金产品";
    }
    else
    {
        titleLab.text=@"其他产品";
    }
    
    titleLab.font=FONT(15);

    [view addSubview:titleLab];
    return view;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];

    
    NSArray *array=self.dateList[indexPath.section];
    AllListModel *model=[array objectAtIndex:indexPath.row];
    
    if ([model.hasLeaf integerValue]>0)
    {
        
        SubAllListViewController *vc=[[SubAllListViewController alloc]init];
        vc.productName=model.productName;
        vc.productType=model.bigProductType.intValue;

        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        CBWebViewController *vc=[[CBWebViewController alloc]init];
        vc.urlString=model.h5URL;
        vc.productID=model.ID;
        if ([model.riskLevel isEqualToString:@"不显示"]) {
            vc.isDisplayNativeBtn=NO;
        } else {
            vc.isDisplayNativeBtn=YES;
        }
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    
   
}

- (void)setNavTitle {
    [self.view addSubview:self.titleView];
    [self.goldPrice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleView).offset(16);
        if (IS_IPHONEX) {
            make.bottom.equalTo(self.titleView).offset(-21);
        } else {
            make.bottom.equalTo(self.titleView).offset(-17);
        }
    }];
    [self.refImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.goldPrice);
        make.left.equalTo(self.goldPrice.mas_right).offset(8);
    }];
    [self.unitLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.goldPrice);
        make.right.equalTo(self.titleView).offset(-30);
    }];
}
/**
 懒加载
 
 @return 导航栏
 */
- (UIView *)titleView {
    if (!_titleView) {
        _titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, NavBarH + 24)];
        _titleView.backgroundColor = COLOR_MAIN;
        [_titleView addSubview:self.goldPrice];
        [_titleView addSubview:self.refImageView];
        [_titleView addSubview:self.unitLabel];
    }
    return _titleView;
}

#pragma warn timer处理不完美，稍后完善
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self refreshGoldPriceRequestData];
    __weak typeof(self) weakSelf = self;
    [[JX_GCDTimerManager sharedInstance] scheduledDispatchTimerWithName:myTimer
                                                           timeInterval:7.0
                                                                  queue:nil
                                                                repeats:YES
                                                           actionOption:AbandonPreviousAction
                                                                 action:^{
                                                                     [weakSelf refreshGoldPriceRequestData];
                                                                 }];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[JX_GCDTimerManager sharedInstance] cancelTimerWithName:myTimer];
}

#pragma mark--刷新金价接口
- (void)refreshGoldPriceRequestData {
    __weak typeof(self) weakSelf = self;
    [AllNetWorkRquest allListRefreshGoldPriceWithResponse:^(NSString *goldPrice, NSString *errMsg) {
        if (goldPrice.length > 0) {
            [weakSelf.goldPrice showNextText:goldPrice withDirection:CWCalendarLabelScrollToTop];
        }
    }];
}
- (CWCalendarLabel *)goldPrice {
    if (!_goldPrice) {
        _goldPrice = [[CWCalendarLabel alloc] init];
        _goldPrice.font = FONT_BOLD(28);
        _goldPrice.textColor = COLOR_WHITE;
        _goldPrice.text = @"000.00";
    }
    return _goldPrice;
}
/**
 懒加载
 
 @return unitLabel
 */
- (UILabel *)unitLabel {
    if (!_unitLabel) {
        _unitLabel = [[UILabel alloc] init];
        _unitLabel.text = @"实时金价(元/克)";
        _unitLabel.font = FONT_TITLE;
        _unitLabel.textColor = COLOR_WHITE;
    }
    return _unitLabel;
}
/**
 懒加载
 
 @return 刷新
 */
- (UIButton *)refImageView {
    if (!_refImageView) {
        _refImageView = [[UIButton alloc] init];
        _refImageView.timeInterval = 0.1;
        _refImageView.jk_touchAreaInsets = UIEdgeInsetsMake(20, 20, 20, 20);
        [_refImageView setImage:[UIImage imageNamed:@"All_refreshIcon"] forState:UIControlStateNormal];
        [_refImageView addTarget:self action:@selector(refImageViewDidTap) forControlEvents:UIControlEventTouchUpInside];
    }
    return _refImageView;
}
- (void)refImageViewDidTap {
    [self.refImageView jk_rotateByAngle:360 duration:0.77 autoreverse:NO repeatCount:1 timingFunction:[CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionLinear]];
    [self refreshGoldPriceRequestData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
