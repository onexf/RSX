//
//  ApplyBuyOrderViewController.m
//  GoldCatBank
//
//  Created by Sunny on 2017/9/11.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "ApplyBuyOrderViewController.h"
#import "BindBankCarViewController.h"
#import "PayViewController.h"
#import "UserAuthData.h"
#import "TitleScrollView.h"
#import "SetChargePasswordViewController.h"
#import "CBWebViewController.h"
#define InputTxfTag 10  //第一行输入UITextField的tag
#define ContentLabTag 100  //第二行变化CMHeadContentLab的tag


@interface ApplyBuyOrderViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    NSMutableArray *dataList;
    
    NSString *glodPrice;//金价
    NSString *totalGlodAmount;//总金价
    NSString *totalGlodWeight;//总重量
    
    TitleScrollView *priceScrollView;
    
    BOOL isUserAgree;//用户协议同意
}

@property(nonatomic,strong)UITableView *table;

@end

@implementation ApplyBuyOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title=self.detailModel.productName;
    //刚开始是按克重
    
    totalGlodAmount=@"0";
    totalGlodWeight=@"0";
    isUserAgree=YES;//默认同意协议
    glodPrice=self.detailModel.goldPrice;
    
    NSString *annualizedIncomeTxt=@"";
    if (self.detailModel.oneDayAnnualizedRate.length>0)
    {
        NSString *sybomlStr=@"%";
        if (![self.detailModel.oneDayAnnualizedRate containsString:@"%"]||![self.detailModel.oneDayAnnualizedRate containsString:@"%"]) {
            annualizedIncomeTxt=[NSString stringWithFormat:@"%@%@",self.detailModel.oneDayAnnualizedRate,sybomlStr];
            
        }
        else
        {
            annualizedIncomeTxt=[NSString stringWithFormat:@"%@",self.detailModel.oneDayAnnualizedRate];

        }
    }
    
    NSString *guaranteeStartTime=nil;
    if (self.detailModel.guaranteeStartTime.length>0)
    {
        guaranteeStartTime=[[self.detailModel.guaranteeStartTime componentsSeparatedByString:@" "] firstObject];
    }
        
    NSString *guaranteeEndTime=nil;
    
    if (self.detailModel.guaranteeEndTime.length>0)
    {
        guaranteeEndTime=[[self.detailModel.guaranteeEndTime componentsSeparatedByString:@" "] firstObject];
    }
    
    
   // bigProductType (integer, optional): 产品大类 0：招财金；1：生财金；2：特价黄金；3：新手特权金；4：零钱罐；5：存钱罐；6：新手专享；7：存钱罐365天升级版；8：保障金；9：黄金及时赚；10：黄金看涨；11：黄金看跌； ,
    if (self.detailModel.bigProductType.integerValue==9)//修改前self.detailModel.ID.integerValue==15
    {
       //黄金及时赚
    
        if (annualizedIncomeTxt.length>0)
        {
            dataList=[NSMutableArray arrayWithArray:@[@{@"年化利率":annualizedIncomeTxt},@{@"正式购买日":guaranteeStartTime},@{@"到期日":guaranteeEndTime}]];
        }
        else
        {
            dataList=[NSMutableArray arrayWithArray:@[@{@"正式购买日":guaranteeStartTime},@{@"到期日":guaranteeEndTime}]];
        }
        

        
    }
    else
    {
         //保价金
       // self.detailModel.ID.integerValue==14
         dataList=[NSMutableArray arrayWithArray:@[@{@"正式购买日":guaranteeStartTime},@{@"到期日":guaranteeEndTime}]];
    
    }
    [self.view addSubview:self.table];
    [self setTableHeadViewAndFootView];
    
    
}

-(void)setTableHeadViewAndFootView
{
    CMBorderView *headview=[[CMBorderView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 120)];
    headview.borderWidth=CMViewBorderWidthMake(0.5, 0, 0.5, 0);
    headview.backgroundColor=COLOR_TBBACK;
    
    UILabel *nowTimeLab=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 30)];
    nowTimeLab.textAlignment=NSTextAlignmentCenter;
    nowTimeLab.textColor=COLOR_SUBTEXT;
    nowTimeLab.text=[DateTool getNowTimeString];
    nowTimeLab.font=FONT(12);
    [headview addSubview:nowTimeLab];
    
    priceScrollView=[TitleScrollView cycleScrollViewWithFrame:CGRectMake(0,nowTimeLab.ct_bottom+20, SCREEN_WIDTH, 25)];
    priceScrollView.center=CGPointMake(SCREEN_WIDTH/2.0,headview.ct_height/2.0-15);
    priceScrollView.title=glodPrice;
    priceScrollView.autoScrollTimeInterval=10;
    priceScrollView.titleFont=FONT(30);
    priceScrollView.titleColor=UIColorFromHex(0xf74d3c);
    [headview addSubview:priceScrollView];
    
    
    UILabel *titleLab=[[UILabel alloc]initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, 20)];
    titleLab.center=CGPointMake(SCREEN_WIDTH/2.0,headview.ct_height/2.0+10);
    titleLab.textAlignment=NSTextAlignmentCenter;
    titleLab.text=@"实时金价（元／克）";
    titleLab.font=FONT(14);
    titleLab.textColor=UIColorFromHex(0x999999);
    [headview addSubview:titleLab];
    
    self.table.tableHeaderView=headview;
    
    //表尾
    CMBorderView  *footView=[[CMBorderView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 300)];
    footView.backgroundColor=COLOR_TBBACK;
    footView.borderWidth=CMViewBorderWidthMake(0.5, 0, 0, 0);
    
    UIButton *tipBtn=[[UIButton alloc]initWithFrame:CGRectMake(10, 10, 120, 25)];
    
    [tipBtn setTitle:@"温馨提示" forState:UIControlStateNormal];
    [tipBtn setTitleColor:COLOR_GLODTEXT forState:UIControlStateNormal];
    [tipBtn setImage:[UIImage imageNamed:@"FlowCash_largeTipIcon"] forState:UIControlStateNormal];
    [tipBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
    tipBtn.titleLabel.font=FONT(14);
    [tipBtn addTarget:self action:@selector(tipBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [footView addSubview:tipBtn];
    
    //确定下单
    UIButton *orderBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    orderBtn.frame=CGRectMake(0, 0, SCREEN_WIDTH-30, 50);
    orderBtn.center=CGPointMake(SCREEN_WIDTH/2.0, tipBtn.ct_bottom+50+50/2.0);
    orderBtn.layer.masksToBounds=YES;
    orderBtn.layer.cornerRadius=8;
    orderBtn.backgroundColor=COLOR_GLODBACKGROUD;
    [orderBtn setTitle:@"确定下单" forState:UIControlStateNormal];
    [orderBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [orderBtn addTarget:self action:@selector(orderBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [footView addSubview:orderBtn];
    
    //同意说明
    NSString *agreeStr=nil;
    
    
    NSString *productName=self.detailModel.productName;
    agreeStr=[NSString stringWithFormat:@"我已阅读并同意%@ 综合服务协议",productName];
    
    NSMutableAttributedString *agreeAttri=[[NSMutableAttributedString alloc]initWithString:agreeStr];
    [agreeAttri addAttribute:NSForegroundColorAttributeName value:COLOR_TEXT range:NSMakeRange(0, 7)];
    [agreeAttri addAttribute:NSForegroundColorAttributeName value:COLOR_GLODBACKGROUD range:NSMakeRange(7, agreeStr.length-7)];
    [agreeAttri addAttribute:NSFontAttributeName value:FONT(13) range:NSMakeRange(0, agreeStr.length)];
    
    UIButton *agreeeBtn=[[UIButton alloc]initWithFrame:CGRectMake(0,0,SCREEN_WIDTH, 30)];
    agreeeBtn.center=CGPointMake(SCREEN_WIDTH/2.0, orderBtn.ct_bottom+20);
    [agreeeBtn setAttributedTitle:agreeAttri forState:UIControlStateNormal];
    [agreeeBtn setTitle:agreeStr forState:UIControlStateNormal];
    [agreeeBtn setImage:[UIImage imageNamed:@"FlowCash_selectIcon"] forState:UIControlStateNormal];
    [agreeeBtn setImage:[UIImage imageNamed:@"FlowCash_noSelectIcon"] forState:UIControlStateSelected];
    [agreeeBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    [agreeeBtn addTarget:self action:@selector(greeeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [footView addSubview:agreeeBtn];
    
    //透明button实现查看协议点击
    UIButton *clearCheckBtn=[[UIButton alloc]initWithFrame:CGRectMake(agreeeBtn.ct_width/2.0-10,0,agreeeBtn.ct_width/2.0+10,agreeeBtn.ct_height)];
    clearCheckBtn.backgroundColor=[UIColor clearColor];
    [clearCheckBtn addTarget:self action:@selector(clearCheckBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [agreeeBtn addSubview:clearCheckBtn];
    //
    //
    //    UILabel *bottomLabel=[[UILabel alloc]initWithFrame:CGRectMake(agreeeBtn.ct_left,agreeeBtn.ct_bottom+5,agreeeBtn.ct_width, 15)];
    //    bottomLabel.text=@"中华联合财险承保用户黄金财产安全，请放心交易";
    //    bottomLabel.textAlignment=NSTextAlignmentCenter;
    //    bottomLabel.textColor=COLOR_SUBTEXT;
    //    bottomLabel.font=FONT(13);
    //    [footView addSubview:bottomLabel];
    self.table.tableFooterView=footView;
    
    
    
    
}

-(UITableView *)table
{
    if (!_table)
    {
        _table=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-NavBarHeight) style:UITableViewStylePlain];
        _table.delegate=self;
        _table.dataSource=self;
        _table.backgroundColor=COLOR_TBBACK;
        _table.showsVerticalScrollIndicator=NO;
        _table.separatorColor=COLOR_LINE;
    }
    
    return _table;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0)
    {
        return 2;
    }
  
     return dataList.count;
    
   
    
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section==1)
    {
        return 10;
    }
    
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section==1)
    {
        CMBorderView *view=[[CMBorderView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 10)];
        view.backgroundColor=COLOR_TBBACK;
        view.borderWidth=CMViewBorderWidthMake(0.5, 0, 0.5, 0);
        return view;
        
    }
    
    return nil;
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0)
    {
        
        if (indexPath.row==0)
        {
            static NSString *cell0ID=@"cellRow0ID";
            UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cell0ID];
            if (!cell)
            {
                cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell0ID];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
                cell.textLabel.font=FONT(15);
                cell.textLabel.textColor=UIColorFromHex(0x000000);
                
                UITextField *txf=[[UITextField alloc]initWithFrame:CGRectMake(100,0, SCREEN_WIDTH/3.0+10, 44)];
                txf.font=FONT(15);
                txf.delegate=self;
                txf.keyboardType=UIKeyboardTypeDecimalPad;
                txf.textColor=cell.textLabel.textColor;
                txf.placeholder=[NSString stringWithFormat:@"%@起购",self.detailModel.purchaseStartAmount];
                txf.tag=InputTxfTag;
                [cell.contentView addSubview:txf];
                
                
            }
            
            cell.textLabel.text=@"金额";
            
            return cell;
            
        }
        else
        {
            static NSString *cell0D=@"cellRow1ID";
            UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cell0D];
            if (!cell)
            {
                cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cell0D];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
                cell.textLabel.font=FONT(15);
                cell.textLabel.textColor=UIColorFromHex(0x000000);
                
                CMHeaderContentLable *lable=[[CMHeaderContentLable alloc] initWithFrame:CGRectMake(15, 0,SCREEN_WIDTH-30, 44)];
                lable.tag=ContentLabTag;
                lable.header=@"预计克重";
                lable.headerFont=FONT(15);
                lable.contentFont=FONT(15);
                lable.headerTextColor=UIColorFromHex(0x000000);
                lable.contentTextColor=UIColorFromHex(0x000000);
                lable.contentSide=headerContentLable_contentSide_left;
                lable.contentMargin=85;
                [cell.contentView addSubview:lable];
                
            }
            
            return cell;
            
            
        }
        
    }
    else
    {
        if (dataList.count>indexPath.row)
        {
            static NSString *cell1ID=@"cell1ID";
            UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cell1ID];
            if (!cell)
            {
                cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cell1ID];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
                cell.textLabel.font=FONT(15);
                cell.textLabel.textColor=UIColorFromHex(0x000000);
                cell.detailTextLabel.font=FONT(15);
                cell.detailTextLabel.textColor=UIColorFromHex(0x000000);
                
                
            }
            
            NSDictionary *dic=[dataList objectAtIndex:indexPath.row];
            cell.textLabel.text=[[dic allKeys]lastObject];
            cell.detailTextLabel.text=[[dic allValues]lastObject];
            return cell;
            
        }
        
        
        
    }
    
    return nil;
    
}



#pragma mark--温馨提示
-(void)tipBtnClick:(UIButton *)btn
{
    
    [TipsTool showTipsWithTitle:@"提示" andContent:self.detailModel.tips];
}

#pragma mark--下单按钮
-(void)orderBtnClick:(UIButton *)btn
{
    
    [self.view endEditing:YES];
    if (!isUserAgree)
    {
        [WSProgressHUD showErrorWithStatus:@"请先阅读用户协议并同意后购买！"];
        
        return;
        
    }
    
    
    
    [self goToBuyWithDetailModel:self.detailModel totalGlodWeight:totalGlodWeight totalGlodAmount:totalGlodAmount isBuyByAmount:YES];
    
    
}



#pragma mark--同意用户协议
-(void)greeeBtnClick:(UIButton *)btn{
    
    btn.selected=!btn.selected;
    isUserAgree=!isUserAgree;
}

#pragma mark--查看流动金协议
-(void)clearCheckBtnClick:(UIButton *)btn
{
    CBWebViewController *webViewVC=[[CBWebViewController alloc]init];
    webViewVC.urlString=self.detailModel.protocolUrl;
    [self.navigationController pushViewController:webViewVC animated:YES];
    
}


#pragma mark--通知时间
//-(void)glodPriceChangeAction:(NSNotification *)notify
//{
//    NSDictionary *dic=notify.userInfo;
//
//    NSString *priceStr=[dic objectForKey:Notify_GlodPriceChange];
//    priceScrollView.title=priceStr;
//
//    [dataList replaceObjectAtIndex:0 withObject:@{@"价格":[NSString stringWithFormat:@"%@元/克",priceStr]}];
//    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:0 inSection:1];
//
//    [self.table reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
//
//}

#pragma mark--textFieldDelegate


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    
    
    NSString *displayString=nil;
    
    static NSInteger pointNum=0;
    
    if (textField.text.length<=0)
    {
        pointNum=0;  //此处针对输入过小数，然后又消去小数的处理
        //首位不能输入.
        if ([string isEqualToString:@"."])
        {
            return NO;
        }
        
        displayString=string;
    }
    else
    {
        
        
        if (range.location==0)
        {
            //针对输入很多位，返回第一位输入。的情况
            if ([string isEqualToString:@"."])
            {
                return NO;
            }
        }
        
        
        if (![textField.text containsString:@"."]) {
            //此处针对输入过小数，然后又消去小数的处理
            pointNum=0;
        }
        
        if ([string isEqualToString:@"."]) {
            
            //针对返回头部输入小数点 （重量只能输入小数点后三位小数，金额小数点后两位）
            NSString *subText=[textField.text substringFromIndex:range.location];
            
            //按价格
            if ((range.location<textField.text.length)&&(subText.length>2)) {
                
                return NO;
                
            }
            
            //不能输入多个.
            if (pointNum==1)
            {
                return NO;
            }
            
            pointNum++;
            
        }
        else
        {
            //已经有一个.，但此次输入的不是.
            if (pointNum==1)
            {
                NSRange pointRange=[textField.text rangeOfString:@"."];
                //按价格
                
                if (range.location-pointRange.location>2) {
                    
                    return NO;
                }
                
                
            }
            
        }
        
        
        if (range.length>0)
        {
            NSMutableString *contentStr=[[NSMutableString alloc]initWithString:textField.text];
            [contentStr deleteCharactersInRange:range];
            displayString=contentStr;
        }
        else
        {
            NSMutableString *contentStr=[[NSMutableString alloc]initWithString:textField.text];
            [contentStr insertString:string atIndex:range.location];
            displayString=contentStr;
        }
        
        
    }
    
     //购买上限判断
    
    if (self.detailModel.purchaseEndAmount.integerValue>0)
    {
        if (displayString.floatValue>self.detailModel.purchaseEndAmount.floatValue)
        {
            textField.text=[NSString stringWithFormat:@"%@",self.detailModel.purchaseEndAmount];
            displayString=textField.text;
           [self computeWithdisplayString:displayString];
            
            return NO;
        }
        
        
    }

    
    
    [self computeWithdisplayString:displayString];
    
    return YES;
}



-(void)computeWithdisplayString:(NSString *)displayString
{
    
    //合法输入后
    NSIndexPath *index=[NSIndexPath indexPathForRow:1 inSection:0];
    
    UITableViewCell *cell=[self.table cellForRowAtIndexPath:index];
    CMHeaderContentLable *lable=(CMHeaderContentLable *)[cell viewWithTag:ContentLabTag];
    
    NSString *row0String=displayString;//克数或金额
    
    NSDecimalNumber *displayNum=[NSDecimalNumber decimalNumberWithString:displayString];
    
    if (row0String.length>0)
    {
        //按金额
        //得出克数保留三位小数
        lable.content=[NSString stringWithFormat:@"%@克",[PriceTool numberA:row0String DivideNumberB:glodPrice withRoundingStyle:NSRoundDown decimalNum:3]];
        
        totalGlodWeight=[PriceTool numberA:row0String DivideNumberB:glodPrice withRoundingStyle:NSRoundDown decimalNum:3];
        totalGlodAmount=[displayNum stringValue];
    }
    else
    {
        //按金额
        //得出克数保留三位小数
        lable.content=[NSString stringWithFormat:@"%@克",@"0.000"];
        
        totalGlodWeight=@"0.000";
        totalGlodAmount=@"0.00";
        
    }

    
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [priceScrollView start];
}


-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [priceScrollView suspend];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
