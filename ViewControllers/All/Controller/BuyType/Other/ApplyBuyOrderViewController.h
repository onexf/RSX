//
//  ApplyBuyOrderViewController.h
//  GoldCatBank
//
//  Created by Sunny on 2017/9/11.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//


#import "ProductDetailModel.h"
#import "GoOrderBaseViewController.h"

@interface ApplyBuyOrderViewController : GoOrderBaseViewController
@property(nonatomic,strong)ProductDetailModel *detailModel;
@end
