//
//  OtherPaySuccessViewController.h
//  GoldCatBank
//
//  Created by Sunny on 2017/9/14.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "BaseViewController.h"
#import "ProductDetailModel.h"
#import "PayModel.h"

@interface OtherPaySuccessViewController : BaseViewController
@property(nonatomic,strong)ProductDetailModel *detailModel;
@property(nonatomic,strong)PayModel *payModel;
@property(nonatomic,copy)NSString *discountStr;

@end
