//
//  GoldUpAndDownViewController.h
//  GoldCatBank
//
//  Created by Sunny on 2017/9/20.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "GoOrderBaseViewController.h"
#import "ProductDetailModel.h"
@interface GoldUpAndDownViewController : GoOrderBaseViewController
@property(nonatomic,strong)ProductDetailModel *detailModel;

@end
