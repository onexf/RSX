//
//  SaveCashViewController.m
//  GoldCatBank
//
//  Created by 高丹丹 on 2017/8/25.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "SaveCashViewController.h"
#import "CBWebViewController.h"
#import "PayViewController.h"
#import "PayViewController.h"
@interface SaveCashViewController ()<UITextFieldDelegate>
{
    NSInteger tag;
    NSString *glodPrice;
    UITextField *amountTxf;
    BOOL isUserAgree;
    
}

@end

@implementation SaveCashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title=self.detailModel.productName;
    glodPrice=self.detailModel.profitRatio;
    self.view.backgroundColor=COLOR_TBBACK;
    isUserAgree=YES;//用户同意协议
    [self initView];
}

-(void)initView
{
    CMBorderView *headview=[[CMBorderView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 120)];
    headview.borderWidth=CMViewBorderWidthMake(0.5, 0, 0.5, 0);
    headview.backgroundColor=COLOR_TBBACK;
    
    
    UILabel *nowTimeLab=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 30)];
    nowTimeLab.textAlignment=NSTextAlignmentCenter;
    nowTimeLab.textColor=COLOR_SUBTEXT;
    nowTimeLab.text=[DateTool getNowTimeString];
    nowTimeLab.font=FONT(12);
    [headview addSubview:nowTimeLab];
    
    NSString *title=[NSString stringWithFormat:@"%@\n%@",glodPrice,self.detailModel.smallTitle2];
    
    NSInteger profitshowTxtlength=self.detailModel.smallTitle2.length;
    
    NSMutableAttributedString *titleAttri=[[NSMutableAttributedString alloc]initWithString:title];
    [titleAttri addAttribute:NSFontAttributeName value:FONT(30) range:NSMakeRange(0, title.length-profitshowTxtlength)];
    [titleAttri addAttribute:NSFontAttributeName value:FONT(14) range:NSMakeRange(title.length-profitshowTxtlength, profitshowTxtlength)];
    [titleAttri addAttribute:NSForegroundColorAttributeName value:UIColorFromHex(0xf74d3c) range:NSMakeRange(0, title.length-profitshowTxtlength)];
    [titleAttri addAttribute:NSForegroundColorAttributeName value:UIColorFromHex(0x999999) range:NSMakeRange(title.length-profitshowTxtlength, profitshowTxtlength)];
    
    UILabel *titleLab=[[UILabel alloc]initWithFrame:CGRectMake(0,nowTimeLab.ct_bottom+15, SCREEN_WIDTH, 70)];
    titleLab.center=CGPointMake(SCREEN_WIDTH/2.0,headview.ct_height/2.0);
    titleLab.numberOfLines=0;
    titleLab.textAlignment=NSTextAlignmentCenter;
    titleLab.attributedText=titleAttri;
    [headview addSubview:titleLab];
    
    [self.view addSubview:headview];
    
    
    
    //金额和计息日
    
    UIView *backGroudView=nil;
    
    if (self.detailModel.expirationDate.length>0)
    {
        
        backGroudView=[[UIView alloc]initWithFrame:CGRectMake(0, headview.ct_bottom, SCREEN_WIDTH, 44*3)];
    }
    else
    {
        backGroudView=[[UIView alloc]initWithFrame:CGRectMake(0, headview.ct_bottom, SCREEN_WIDTH, 88)];
    }
    backGroudView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:backGroudView];

    
    CMBorderView *cashView=[[CMBorderView alloc]initWithFrame:CGRectMake(15,0, SCREEN_WIDTH, 44)];
    cashView.borderWidth=CMViewBorderWidthMake(0, 0,0.5, 0);
    [backGroudView addSubview:cashView];
    
    
    UILabel*cashTitleLab=[[UILabel alloc]initWithFrame:CGRectMake(0,0,70, 44)];
    cashTitleLab.textColor=COLOR_TEXT;
    cashTitleLab.font=FONT(15);
    cashTitleLab.text=@"金额";
    [cashView addSubview:cashTitleLab];
    
    amountTxf=[[UITextField alloc]initWithFrame:CGRectMake(80,0, SCREEN_WIDTH/2.0, 44)];
    amountTxf.font=FONT(15);
    amountTxf.delegate=self;
    amountTxf.keyboardType=UIKeyboardTypeDecimalPad;
    amountTxf.textColor=cashTitleLab.textColor;
    amountTxf.placeholder=[NSString stringWithFormat:@"%@元起购",self.detailModel.purchaseStartAmount];
    [cashView addSubview:amountTxf];
    
    
    CMHeaderContentLable *dateLabel=[[CMHeaderContentLable alloc]initWithFrame:CGRectMake(15, cashView.ct_bottom, SCREEN_WIDTH-15, 44)];
    dateLabel.header=@"计息日";
    dateLabel.content=self.detailModel.interestTime;
    dateLabel.headerFont=FONT(15);
    dateLabel.contentFont=FONT(15);
    dateLabel.headerTextColor=COLOR_TEXT;
    dateLabel.contentTextColor=COLOR_TEXT;
    dateLabel.contentSide=headerContentLable_contentSide_left;
    dateLabel.contentMargin=SCREEN_WIDTH-115;
    [backGroudView addSubview:dateLabel];
    
    if (self.detailModel.expirationDate.length>0)
    {
        
        UIView *line=[[UIView alloc]initWithFrame:CGRectMake(15,dateLabel.ct_bottom, SCREEN_WIDTH-15, 0.5)];
        line.backgroundColor=COLOR_LINE;
        [backGroudView addSubview:line];

        CMHeaderContentLable *endDateLabel=[[CMHeaderContentLable alloc]initWithFrame:CGRectMake(15, dateLabel.ct_bottom, SCREEN_WIDTH-15, 44)];
        endDateLabel.header=@"到期日";
        endDateLabel.content=self.detailModel.expirationDate;
        endDateLabel.headerFont=FONT(15);
        endDateLabel.contentFont=FONT(15);
        endDateLabel.headerTextColor=COLOR_TEXT;
        endDateLabel.contentTextColor=COLOR_TEXT;
        endDateLabel.contentSide=headerContentLable_contentSide_left;
        endDateLabel.contentMargin=SCREEN_WIDTH-115;
        [backGroudView addSubview:endDateLabel];
        

    }
    
    
    
    //表尾
    CMBorderView  *footView=nil;

    footView=[[CMBorderView alloc]initWithFrame:CGRectMake(0,backGroudView.ct_bottom, SCREEN_WIDTH, 300)];
    footView.backgroundColor=COLOR_TBBACK;
    footView.borderWidth=CMViewBorderWidthMake(0.5, 0, 0, 0);
    [self.view addSubview:footView];
    
    UIButton *tipBtn=[[UIButton alloc]initWithFrame:CGRectMake(10, 10, 120, 25)];
    //    tipBtn.backgroundColor=[UIColor redColor];
    [tipBtn setTitle:@"温馨提示" forState:UIControlStateNormal];
    [tipBtn setTitleColor:COLOR_GLODTEXT forState:UIControlStateNormal];
    [tipBtn setImage:[UIImage imageNamed:@"FlowCash_largeTipIcon"] forState:UIControlStateNormal];
    [tipBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
    tipBtn.titleLabel.font=FONT(14);
    [tipBtn addTarget:self action:@selector(tipBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [footView addSubview:tipBtn];
    
    //确定下单
    UIButton *orderBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    orderBtn.frame=CGRectMake(0, 0, SCREEN_WIDTH-30, 50);
    orderBtn.center=CGPointMake(SCREEN_WIDTH/2.0, tipBtn.ct_bottom+50+50/2.0);
    orderBtn.layer.masksToBounds=YES;
    orderBtn.layer.cornerRadius=8;
    orderBtn.backgroundColor=COLOR_GLODBACKGROUD;
    [orderBtn setTitle:@"确定下单" forState:UIControlStateNormal];
    [orderBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [orderBtn addTarget:self action:@selector(orderBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [footView addSubview:orderBtn];
    
    //同意说明
    NSString *productName=self.detailModel.productName;
    NSString *agreeStr=agreeStr=[NSString stringWithFormat:@"我已阅读并同意%@ 综合服务协议",productName];
 
    NSMutableAttributedString *agreeAttri=[[NSMutableAttributedString alloc]initWithString:agreeStr];
    [agreeAttri addAttribute:NSForegroundColorAttributeName value:COLOR_TEXT range:NSMakeRange(0, 7)];
    [agreeAttri addAttribute:NSForegroundColorAttributeName value:COLOR_GLODBACKGROUD range:NSMakeRange(7, agreeStr.length-7)];
    [agreeAttri addAttribute:NSFontAttributeName value:FONT(13) range:NSMakeRange(0, agreeStr.length)];
    
    UIButton *agreeeBtn=[[UIButton alloc]initWithFrame:CGRectMake(0,0,300, 30)];
    agreeeBtn.center=CGPointMake(SCREEN_WIDTH/2.0, orderBtn.ct_bottom+20);
    [agreeeBtn setAttributedTitle:agreeAttri forState:UIControlStateNormal];
    [agreeeBtn setTitle:agreeStr forState:UIControlStateNormal];
    [agreeeBtn setImage:[UIImage imageNamed:@"FlowCash_selectIcon"] forState:UIControlStateNormal];
    [agreeeBtn setImage:[UIImage imageNamed:@"FlowCash_noSelectIcon"] forState:UIControlStateSelected];
    [agreeeBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    [agreeeBtn addTarget:self action:@selector(greeeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [footView addSubview:agreeeBtn];
    
    //透明button实现查看协议点击
    UIButton *clearCheckBtn=[[UIButton alloc]initWithFrame:CGRectMake(agreeeBtn.ct_width/2.0-10,0,agreeeBtn.ct_width/2.0+10,agreeeBtn.ct_height)];
    clearCheckBtn.backgroundColor=[UIColor clearColor];
    [clearCheckBtn addTarget:self action:@selector(clearCheckBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [agreeeBtn addSubview:clearCheckBtn];
     
}


#pragma mark--查看买入提示
-(void)tipBtnClick:(UIButton *)btn
{
    
    [TipsTool showTipsWithTitle:@"提示" andContent:self.detailModel.tips];
}

#pragma mark--下单按钮
-(void)orderBtnClick:(UIButton *)btn
{
    
    [self.view endEditing:YES];
    if (!isUserAgree)
    {
        [WSProgressHUD showErrorWithStatus:@"请先阅读用户协议并同意后购买！"];
        
        return;

    }
    
    
    [self goToBuyWithDetailModel:self.detailModel totalGlodWeight:@"0" totalGlodAmount:amountTxf.text isBuyByAmount:YES];
    

}

#pragma mark--同意用户协议
-(void)greeeBtnClick:(UIButton *)btn{
    
    btn.selected=!btn.selected;
    
    isUserAgree=!isUserAgree;
}

#pragma mark--查看流动金协议
-(void)clearCheckBtnClick:(UIButton *)btn
{
    CBWebViewController *webViewVC=[[CBWebViewController alloc]init];
    webViewVC.urlString=self.detailModel.protocolUrl;
    [self.navigationController pushViewController:webViewVC animated:YES];
}


#pragma mark--textFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
//    NSString *displayString=nil;
    
    static NSInteger pointNum=0;
    
    if (textField.text.length<=0) 
    {
        pointNum=0;  //此处针对输入过小数，然后又消去小数的处理
        //首位不能输入.
        if ([string isEqualToString:@"."])
        {
            return NO;
        }
        
//        displayString=string;
    }
    else
    {
        
        
        if (range.location==0)
        {
            //针对输入很多位，返回第一位输入。的情况
            if ([string isEqualToString:@"."])
            {
                return NO;
            }
        }
        
        
        if (![textField.text containsString:@"."]) {
            //此处针对输入过小数，然后又消去小数的处理
            pointNum=0;
        }
        
        if ([string isEqualToString:@"."]) {
            
            //针对返回头部输入小数点 （重量只能输入小数点后三位小数，金额小数点后两位）
            NSString *subText=[textField.text substringFromIndex:range.location];
            
            //按价格
            if ((range.location<textField.text.length)&&(subText.length>2)) {
                
                return NO;
                
            }
            
            //不能输入多个.
            if (pointNum==1)
            {
                return NO;
            }
            
            pointNum++;
            
        }
        else
        {
            //已经有一个.，但此次输入的不是.
            if (pointNum==1)
            {
                NSRange pointRange=[textField.text rangeOfString:@"."];
                //按价格
                
                if (range.location-pointRange.location>2) {
                    
                    return NO;
                }
                
                
            }
            
        }
        

        
        
    }
    
    
    NSString *displayString=nil;
    
    if (range.length>0)
    {
        NSMutableString *contentStr=[[NSMutableString alloc]initWithString:textField.text];
        [contentStr deleteCharactersInRange:range];
        displayString=contentStr;
    }
    else
    {
        NSMutableString *contentStr=[[NSMutableString alloc]initWithString:textField.text];
        [contentStr insertString:string atIndex:range.location];
        displayString=contentStr;
    }
    

    
     //购买上限判断
    
    if (self.detailModel.purchaseEndAmount.integerValue>0)
    {
        if (displayString.floatValue>self.detailModel.purchaseEndAmount.floatValue)
        {
            textField.text=[NSString stringWithFormat:@"%@",self.detailModel.purchaseEndAmount];
            return NO;
        }
        
        
    }
    

    
    
    
    
    return YES;
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
