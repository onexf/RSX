//
//  PaySuccessViewController.m
//  GoldCatBank
//
//  Created by 高丹丹 on 2017/9/8.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "PaySuccessViewController.h"
#import <FDFullscreenPopGesture/UINavigationController+FDFullscreenPopGesture.h>
#import "RecordListViewController.h"
@interface PaySuccessViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSArray *dataArr;
}
@property(nonatomic,strong)UITableView *table;

@end

@implementation PaySuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.fd_prefersNavigationBarHidden = YES;
    self.fd_interactivePopDisabled=YES;
    
    UIView *maincolor = [[UIView alloc] initWithFrame:CGRectMake(0, -20 - (NavBarH - 64), SCREEN_WIDTH, 300)];
    maincolor.backgroundColor = COLOR_MAIN;
    [self.view insertSubview:maincolor atIndex:0];
    // bigProductType (integer, optional): 产品大类 0：招财金；1：生财金；2：特价黄金；3：新手特权金；4：零钱罐；5：存钱罐；6：新手专享；7：存钱罐365天升级版；8：保障金；9：黄金及时赚；10：黄金看涨；11：黄金看跌； ,
    
    NSString *couponStr=nil;
    if (self.discountStr.length>0)
    {
        couponStr=self.discountStr;
    }
    else
    {
        couponStr=[NSString stringWithFormat:@"-¥%@",self.payModel.couponAmount];
    }
    if (self.detailModel.bigProductType.integerValue==9)//修改前self.detailModel.ID.integerValue==15
    {
        //黄金及时赚
        if (self.payModel.expirationDate.length>0)
        {
            
            dataArr=@[@{@"优惠券":couponStr},@{@"余额支付":[NSString stringWithFormat:@"¥%@",self.payModel.useAccountAmount]},@{@"在线支付":[NSString stringWithFormat:@"¥%@",self.payModel.bankAmount]},@{@"到期日":self.payModel.expirationDate}];
            
        }
        else
        {
            dataArr=@[@{@"优惠券":couponStr},@{@"余额支付":[NSString stringWithFormat:@"¥%@",self.payModel.useAccountAmount]},@{@"在线支付":[NSString stringWithFormat:@"¥%@",self.payModel.bankAmount]}];
            
        }
        

    }
    else
    {
        if (self.payModel.expirationDate.length>0)
        {
            dataArr=@[@{@"优惠券":couponStr},@{@"余额支付":[NSString stringWithFormat:@"¥%@",self.payModel.useAccountAmount]},@{@"在线支付":[NSString stringWithFormat:@"¥%@",self.payModel.bankAmount]},@{@"计息日":self.payModel.interestTime},@{@"到期日":self.payModel.expirationDate}];
            
        }
        else
        {
            dataArr=@[@{@"优惠券":couponStr},@{@"余额支付":[NSString stringWithFormat:@"¥%@",self.payModel.useAccountAmount]},@{@"在线支付":[NSString stringWithFormat:@"¥%@",self.payModel.bankAmount]},@{@"计息日":self.payModel.interestTime}];
            
        }
        

    }

    

    [self.view addSubview:self.table];
    [self setTableHeadView];
}

-(UITableView *)table
{
    if (!_table)
    {
        _table=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) style:UITableViewStylePlain];
        _table.delegate=self;
        _table.dataSource=self;
        _table.separatorColor=COLOR_LINE;
        //_table.separatorStyle=UITableViewCellSelectionStyleNone;
        _table.backgroundColor = [UIColor clearColor];
    }
    return _table;
}


-(void)setTableHeadView
{
    
    UIView *headView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 260)];
    headView.backgroundColor=COLOR_GLODBACKGROUD;
    
    UIButton *topTipBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 150, 25)];
    topTipBtn.center=CGPointMake(SCREEN_WIDTH/2.0, 20+15);
    [topTipBtn setTitle:@"成功买入黄金" forState:UIControlStateNormal];
    [topTipBtn setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
    [topTipBtn setImage:[UIImage imageNamed:@"paySucccess_selecctIcon"] forState:UIControlStateNormal];
    [topTipBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    topTipBtn.titleLabel.font=FONT(16);
    topTipBtn.userInteractionEnabled=NO;
    [headView addSubview:topTipBtn];
    
    
    UIButton *finishBtn=[[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-50,topTipBtn.ct_top-5,40, 30)];
    
    [finishBtn setTitle:@"完成" forState:UIControlStateNormal];
    [finishBtn setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
    finishBtn.titleLabel.font=FONT(16);
    [finishBtn addTarget:self action:@selector(finishBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [headView addSubview:finishBtn];
    
    UILabel *weightLab=[[UILabel alloc]initWithFrame:CGRectMake(0, topTipBtn.ct_bottom+10, SCREEN_WIDTH, 30)];
    weightLab.textAlignment=NSTextAlignmentCenter;
    weightLab.font=FONT(30);
    weightLab.textColor=COLOR_WHITE;
    NSString *buyweight=[NSString stringWithFormat:@"%.3f",self.weight.floatValue];
    
    weightLab.text=[NSString stringWithFormat:@"%@克",buyweight];
    [headView addSubview:weightLab];
    
    UILabel *nameLab=[[UILabel alloc]initWithFrame:CGRectMake(0, weightLab.ct_bottom+15, SCREEN_WIDTH, 20)];
    nameLab.text=self.detailModel.productName;
    nameLab.textColor=COLOR_Red;
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.font=FONT(15);
    [headView addSubview:nameLab];
    
    UILabel *priceLab=[[UILabel alloc]initWithFrame:CGRectMake(0, nameLab.ct_bottom+10, SCREEN_WIDTH, 20)];
    priceLab.text=[NSString stringWithFormat:@"%@元/克",self.payModel.goldPrice];
    priceLab.textColor=COLOR_Red;
    priceLab.font=FONT(15);
    priceLab.textAlignment=NSTextAlignmentCenter;
    [headView addSubview:priceLab];
    
    CMBorderView *borderView=[[CMBorderView alloc]initWithFrame:CGRectMake(0,headView.ct_bottom-88,SCREEN_WIDTH, 44)];
    borderView.backgroundColor=COLOR_TBBACK;
    borderView.borderWidth=CMViewBorderWidthMake(0.5, 0, 0.5, 0);
    [headView addSubview:borderView];
    
    UILabel *priceTipLab=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, borderView.ct_height)];
    priceTipLab.text=@"由于金价实时变动，实际购买克重可能与预计克重有所偏差。";
    priceTipLab.textColor=COLOR_SUBTEXT;
    priceTipLab.font=FONT(12);
    priceTipLab.textAlignment=NSTextAlignmentCenter;
    [borderView addSubview:priceTipLab];
    
    CMBorderView *amountView=[[CMBorderView alloc]initWithFrame:CGRectMake(0,headView.ct_bottom-44,SCREEN_WIDTH, 44)];
    amountView.backgroundColor=COLOR_WHITE;
    amountView.borderWidth=CMViewBorderWidthMake(0.5, 0, 0.5, 0);
    [headView addSubview:amountView];
    
    UILabel *amountLab=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 44)];
    amountLab.text=[NSString stringWithFormat:@"支付%@元",self.payModel.payAmount];
    amountLab.textColor=COLOR_Red;
    amountLab.font=FONT(18);
    
    amountLab.textAlignment=NSTextAlignmentCenter;
    [amountView addSubview:amountLab];
    
    
    self.table.tableHeaderView=headView;
    
    UIView *footView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    footView.backgroundColor=[UIColor clearColor];

    UIView *line=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0.5)];
    line.backgroundColor=COLOR_LINE;
    [footView addSubview:line];
    
    
    NSDate *nowsDate=[NSDate date];
    
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *nowString=[dateFormat stringFromDate:nowsDate];
    

    UILabel *trdeTime=[[UILabel alloc]initWithFrame:CGRectMake(10, 10, SCREEN_WIDTH-20, 20)];
    trdeTime.text=nowString;
    trdeTime.textColor=COLOR_SUBTEXT;
    trdeTime.font=FONT(12);
    trdeTime.text=[NSString stringWithFormat:@"交易时间：%@",self.payModel.transactionTime];
    trdeTime.textAlignment=NSTextAlignmentRight;
    [footView addSubview:trdeTime];

    self.table.tableFooterView=footView;

    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId=@"cellId";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (!cell)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellId];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    if (indexPath.row<=2)
    {
        cell.detailTextLabel.textColor=COLOR_Red;
    }
    else
    {
        cell.detailTextLabel.textColor=COLOR_TEXT;
    }
    NSDictionary *dic=dataArr[indexPath.row];
    cell.textLabel.text=[[dic allKeys] lastObject];
    cell.detailTextLabel.text=[[dic allValues]lastObject];
    
    return cell;
}

-(void)finishBtnClick:(UIButton *)btn
{
    
    RecordListViewController *allGoldList = [[RecordListViewController alloc] initWithType:RecordTypeAllGold];
    allGoldList.isPaySuccess=YES;
    [self.navigationController pushViewController:allGoldList animated:YES];
//    BalanceTradeVC *vc=[[BalanceTradeVC alloc]init];
//    vc.isPaySuccess=YES;
//    [self.navigationController pushViewController:vc animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
