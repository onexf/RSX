//
//  GoOrderBaseViewController.m
//  GoldCatBank
//
//  Created by Sunny on 2017/9/13.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "GoOrderBaseViewController.h"
#import "PayViewController.h"
#import "UserAuthData.h"
#import "BindBankCarViewController.h"
#import "SetChargePasswordViewController.h"
@interface GoOrderBaseViewController ()

@end

@implementation GoOrderBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


-(void)goToBuyWithDetailModel:(ProductDetailModel *)detailModel totalGlodWeight:(NSString *)totalGlodWeight totalGlodAmount:(NSString *)totalGlodAmount isBuyByAmount:(BOOL)isBuyByAmount{
    
    
   // bigProductType (integer, optional): 产品大类 0：招财金；1：生财金；2：特价黄金；3：新手特权金；4：零钱罐；5：存钱罐；6：新手专享；7：存钱罐365天升级版；8：保障金；9：黄金及时赚；10：黄金看涨；11：黄金看跌； ,
    
  //  toPayPageID (integer, optional): 跳转的支付页面ID,0 只能用g 买， 1可切换，2只能用金额买 ,
    if (!isLogin())
    {
        [LoginTool loginAction];
        return;
    }
    
    if (isBuyByAmount)
    {
        //按金额购买
        if (totalGlodAmount.length<=0||totalGlodAmount.floatValue<=0)
        {
            [WSProgressHUD showErrorWithStatus:@"购买金额必须大于0"];
             return;
        }
    }
    else
    {
       //按克重购买
        if (totalGlodWeight.length<=0||totalGlodWeight.floatValue<=0)
        {
            [WSProgressHUD showErrorWithStatus:@"购买克重必须大于0"];
             return;
        }
    }
    
    
    
    if (detailModel.toPayPageID.integerValue==0)
    {

        
        if (totalGlodWeight.floatValue<detailModel.purchaseStartGold.floatValue)
        {
            [WSProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"购买克重不能小于%@克",detailModel.purchaseStartGold]];
            
            return;
        }
        
        if (detailModel.purchaseEndGold.floatValue>0)
        {
            if (totalGlodWeight.floatValue>detailModel.purchaseEndGold.floatValue) {
                
                [WSProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"购买克重不能大于%@克",detailModel.purchaseEndGold]];
                
                return;
            }
        }
        
       

    }
    else if(detailModel.toPayPageID.integerValue==1)
    {
        
        if (isBuyByAmount)
        {
            if (totalGlodAmount.floatValue<detailModel.purchaseStartAmount.floatValue)
            {
                [WSProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"购买金额不能小于%@元",detailModel.purchaseStartAmount]];
                
                return;
            }
            
            
            if (detailModel.purchaseEndAmount.floatValue>0)
            {
                if (totalGlodAmount.floatValue>detailModel.purchaseEndAmount.floatValue)
                {
                    [WSProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"购买金额不能大于%@元",detailModel.purchaseEndAmount]];
                    
                    return;
                }
            }
            

       }
        else
        {
            if (totalGlodWeight.floatValue<detailModel.purchaseStartGold.floatValue)
            {
                [WSProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"购买克重不能小于%@克",detailModel.purchaseStartGold]];
                
                return;
            }
            
            if (detailModel.purchaseEndGold.floatValue>0)
            {
                if (totalGlodWeight.floatValue>detailModel.purchaseEndGold.floatValue) {
                    
                    [WSProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"购买克重不能大于%@克",detailModel.purchaseEndGold]];
                    
                    return;
                }
            }

            
        }
        
    }
    else
    {
        if (totalGlodAmount.floatValue<detailModel.purchaseStartAmount.floatValue)
        {
            [WSProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"购买金额不能小于%@元",detailModel.purchaseStartAmount]];
            
            return;
        }
        
        if (detailModel.purchaseEndAmount.floatValue>0)
        {
            if (totalGlodAmount.floatValue>detailModel.purchaseEndAmount.floatValue)
            {
                [WSProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"购买金额不能大于%@元",detailModel.purchaseEndAmount]];
                
                return;
            }
        }
    }
   
    
    [WSProgressHUD showWithMaskType:WSProgressHUDMaskTypeBlack];

    [[UserAuthData shareInstance]refreshUserInfoResponse:^(UserAuthDataModel *userAuthData, NSString *errMsg) {
        
        [WSProgressHUD dismiss];

        if ([userAuthData.isAuthentication boolValue]>0)
        {
            if (userAuthData.hasTradePwd.integerValue>0)
            {
                PayViewController *vc=[[PayViewController alloc]init];
                vc.detailModel=detailModel;
                
                //按金额
                vc.weight=totalGlodWeight;
                vc.amount=totalGlodAmount;
                vc.isBuyByAmount=isBuyByAmount;
                
                [self.navigationController pushViewController:vc animated:YES];

            }
            else
            {
                SetChargePasswordViewController*vc=[[SetChargePasswordViewController alloc]init];
                [self.navigationController pushViewController:vc animated:YES];
            }
            
            
        }
        else
        {
            
            BindBankCarViewController *vc=[[BindBankCarViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
            
        }

    }];
    

   
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
