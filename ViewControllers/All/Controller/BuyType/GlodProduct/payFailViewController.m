//
//  payFailViewController.m
//  GoldCatBank
//
//  Created by 高丹丹 on 2017/9/8.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "payFailViewController.h"
#import <FDFullscreenPopGesture/UINavigationController+FDFullscreenPopGesture.h>


@interface payFailViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSArray *dataArr;
}
@property(nonatomic,strong)UITableView *table;



@end

@implementation payFailViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.fd_prefersNavigationBarHidden = YES;
    self.fd_interactivePopDisabled=YES;
    UIView *maincolor = [[UIView alloc] initWithFrame:CGRectMake(0, -20 - (NavBarH - 64), SCREEN_WIDTH, 300)];
    maincolor.backgroundColor = COLOR_MAIN;
    [self.view insertSubview:maincolor atIndex:0];

    dataArr=@[@{@"优惠券":@"¥0.00"},@{@"余额支付":@"¥0.00"},@{@"在线支付":@"¥0.00"}];

    
    [self.view addSubview:self.table];
    [self setTableHeadView];
}

-(UITableView *)table
{
    if (!_table)
    {
        _table=[[UITableView alloc]initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, SCREEN_HEIGHT) style:UITableViewStylePlain];
        _table.delegate=self;
        _table.dataSource=self;
        //_table.separatorStyle=UITableViewCellSelectionStyleNone;
        _table.backgroundColor = [UIColor clearColor];
    }
    return _table;
}

-(void)setTableHeadView
{
    
    UIView *headView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 220)];
    headView.backgroundColor=COLOR_GLODBACKGROUD;
    
    UIButton *topTipBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 150, 25)];
    topTipBtn.center=CGPointMake(SCREEN_WIDTH/2.0, 70+15);
    [topTipBtn setTitle:@"购买未成功" forState:UIControlStateNormal];
    [topTipBtn setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
    [topTipBtn setImage:[UIImage imageNamed:@"payFailIcon"] forState:UIControlStateNormal];
    [topTipBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    topTipBtn.titleLabel.font=FONT(16);
    topTipBtn.userInteractionEnabled=NO;
    [headView addSubview:topTipBtn];
    

    
    CMBorderView *borderView=[[CMBorderView alloc]initWithFrame:CGRectMake(0,headView.ct_bottom-43,SCREEN_WIDTH, 43)];
    borderView.backgroundColor=COLOR_TBBACK;
    borderView.borderWidth=CMViewBorderWidthMake(0.5, 0, 0.5, 0);
    [headView addSubview:borderView];
    
    UILabel *priceTipLab=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, borderView.ct_height)];
    priceTipLab.text=@"由于金价实时变动，实际购买克重可能与预计克重有所偏差。";
    priceTipLab.textColor=COLOR_SUBTEXT;
    priceTipLab.font=FONT(12);
    priceTipLab.textAlignment=NSTextAlignmentCenter;
    [borderView addSubview:priceTipLab];
    
    
    self.table.tableHeaderView=headView;
    
    CMBorderView *footView=[[CMBorderView alloc]initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,150)];
    borderView.backgroundColor=COLOR_TBBACK;
    borderView.borderWidth=CMViewBorderWidthMake(0.5, 0,0, 0);

    UIView *line=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0.5)];
    line.backgroundColor=COLOR_LINE;
    [footView addSubview:line];
    
    
    NSDate *nowsDate=[NSDate date];
    
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *nowString=[dateFormat stringFromDate:nowsDate];
    
    UILabel *trdeTime=[[UILabel alloc]initWithFrame:CGRectMake(10, 10, SCREEN_WIDTH-20, 20)];
    trdeTime.text=nowString;
    trdeTime.textColor=COLOR_SUBTEXT;
    trdeTime.font=FONT(12);
    trdeTime.textAlignment=NSTextAlignmentRight;
    [footView addSubview:trdeTime];
    
    //重新购买
    UIButton *aginBuyBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    aginBuyBtn.frame=CGRectMake(0, 0, SCREEN_WIDTH-40, 50);
    aginBuyBtn.center=CGPointMake(SCREEN_WIDTH/2.0, 60+50/2.0);
    aginBuyBtn.layer.masksToBounds=YES;
    aginBuyBtn.layer.cornerRadius=8;
    aginBuyBtn.backgroundColor=COLOR_GLODBACKGROUD;
    [aginBuyBtn setTitle:@"重新购买" forState:UIControlStateNormal];
    [aginBuyBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [aginBuyBtn addTarget:self action:@selector(aginBuyBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [footView addSubview:aginBuyBtn];

    
    
    self.table.tableFooterView=footView;
    
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId=@"cellId";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (!cell)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellId];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    if (indexPath.row<=2)
    {
        cell.detailTextLabel.textColor=COLOR_Red;
    }
    else
    {
        cell.detailTextLabel.textColor=COLOR_TEXT;
    }
    NSDictionary *dic=dataArr[indexPath.row];
    cell.textLabel.text=[[dic allKeys] lastObject];
    cell.detailTextLabel.text=[[dic allValues]lastObject];
    
    return cell;
}


#pragma mark--a再次购买

-(void)aginBuyBtnClick:(UIButton *)btn{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
