//
//  PaySuccessViewController.h
//  GoldCatBank
//
//  Created by 高丹丹 on 2017/9/8.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "BaseViewController.h"
#import "ProductDetailModel.h"
#import "PayModel.h"
@interface PaySuccessViewController : BaseViewController
@property(nonatomic,strong)ProductDetailModel *detailModel;
@property(nonatomic,copy)NSString *weight;
@property(nonatomic,strong)PayModel *payModel;
@property(nonatomic,copy)NSString *discountStr;

@end
