//
//  GoOrderBaseViewController.h
//  GoldCatBank
//
//  Created by Sunny on 2017/9/13.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductDetailModel.h"
@interface GoOrderBaseViewController : UIViewController

-(void)goToBuyWithDetailModel:(ProductDetailModel *)detailModel totalGlodWeight:(NSString *)totalGlodWeight totalGlodAmount:(NSString *)totalGlodAmount isBuyByAmount:(BOOL)isBuyByAmount;

@end
