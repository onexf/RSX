//
//  PayViewController.m
//  GoldCatBank
//
//  Created by Sunny on 2017/8/30.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "PayViewController.h"
#import "AllNetWorkRquest.h"
#import "UserAuthData.h"
#import "BindBankCarViewController.h"
#import "BankListModel.h"
#import "PayHandle.h"
#import "payCodeInputView.h"
#import "YYKeyboardManager.h"
#import "IQKeyboardManager.h"
#import "PayPassWordBounceView.h"
#import "PayPacketAndCouponViewController.h"
#import "PaySuccessViewController.h"
#import "payFailViewController.h"
#import "SetChargePasswordViewController.h"
#import "NetworkSingleton+UserData.h"
#import "MineHomePageModel.h"
#import "OtherPaySuccessViewController.h"
#import "ResetTradePWDViewController.h"
#import "PayModel.h"

#import "VerifyTool.h"


#define NewUserTip @"newUserSpecialTip" //新手特权金（针对没有资格购买的和已经抢完的提示存储，不能再次购买）

@interface PayViewController ()
{
    BOOL isBalance; //是否用余额

//    BOOL isBounceBankCodeView;//弹出框是否是银行校验码
    
    CMBorderView *bankView;//银行rowVIew
    
    NSDictionary *orderDataDic;
    
    
    
}

@property(nonatomic,copy)NSString * couponSubAmount;//优惠券减免金额
@property(nonatomic,copy)NSString * totalAmount;
@property(nonatomic,strong)NSString *payByBalanceAmount;//余额支付的余额
@property (nonatomic,copy)NSString *orderId;
@property(nonatomic,copy)NSString *balanceString;//账户所剩余额
@property(nonatomic,strong)NSString  *couponIdStr;//优惠券ID
@property(nonatomic,assign)BOOL isCanUseCoupon;//是否有可用优惠券；

@property(nonatomic,strong)UILabel *disPayBalanceLab;//余额lab
//@property(nonatomic,strong)payCodeInputView *payCodeView;//银行卡支付校验码校验
//@property(nonatomic,strong)PayPassWordBounceView *payChargePaswView;//交易密码
@property(nonatomic,strong)UILabel *counponTiplab;//优惠券提示语：暂无可用优惠券或扣除金额
@property(nonatomic,strong)UILabel *bankPayAmountLab;//银行卡最后所付金额
@property(nonatomic,strong)UILabel *ensurePayLab;//页面底部显示的银行卡需要支付的费用
@property(nonatomic,strong)UILabel *bankTipLabel;//银行提示语
@property(nonatomic,strong)PayModel *payModel;
/** 支付类型 */
@property(assign) VerifyType payType;
/** 验证密码显示的支付数值 */
@property(nonatomic, copy) NSString *payAmount;
@end

@implementation PayViewController

- (void)dealloc {
    // 注销
   [UD removeObjectForKey:NewUserTip];
   [UD synchronize];
}

#pragma mark---余额
-(void)requestBalanceData{
    
    // __weak typeof(self) weakSelf = self;
    __weak typeof (self)  weakSelf =self;
    //请求余额
    [NetworkSingleton userData_getMinePageDataResponse:^(MineHomePageModel *userAssetsData, NSString *errMsg) {
        
        if (!errMsg)
        {
            weakSelf.balanceString=userAssetsData.accountBalance;
            weakSelf.disPayBalanceLab.text=[NSString stringWithFormat:@"余额：%@元",userAssetsData.accountBalance];
            
        }
        
    }];
}

#pragma mark---优惠券
-(void)requestCouponList{
    
    __weak typeof (self)  weakSelf =self;
    
    NSDictionary *dic=nil;
    
    if (self.detailModel.isGoldProduct.integerValue>0)
    {
        dic=@{@"productId":self.detailModel.ID,@"buyGold":self.weight,@"pageSize":[NSNumber numberWithInteger:10000],@"pageIndex":[NSNumber numberWithInteger:1]};
    }
    else
    {
        dic=@{@"productId":self.detailModel.ID,@"buyAmount":self.amount,@"pageSize":[NSNumber numberWithInteger:10000],@"pageIndex":[NSNumber numberWithInteger:1]};
    }
    
    [AllNetWorkRquest getUseCouponListByProductId:dic successBlock:^(id responseBody) {
        
        if (responseBody[@"totalItems"]) {
            
            if ([responseBody[@"totalItems"]integerValue]<=0)
            {
                
                weakSelf.counponTiplab.text=@"暂无可用优惠券";
             
            }
            else
            {
                weakSelf.counponTiplab.text=[NSString stringWithFormat:@"%@张可用",responseBody[@"totalItems"]];
                weakSelf.isCanUseCoupon=YES;
            }
            

        }
        
        
    } failureBlock:^(NSString *error) {
        
        
    }];
}

#pragma mark--银行信息

-(void)requstBankInfo{
    
    __weak typeof(self) weakSelf = self;
    
    UserAuthDataModel *userModel=[[UserAuthData shareInstance] userAuthData];

    NSDictionary *dic=@{@"bankName":userModel.bankName};
    [AllNetWorkRquest getBankInfo:dic successBlock:^(id responseBody) {
        
        NSArray *arr=[BankListModel mj_objectArrayWithKeyValuesArray:(NSArray *)responseBody];
        
        BankListModel *model=[arr firstObject];
        
        weakSelf.bankTipLabel.text=[NSString stringWithFormat:@"单笔限额%@ 单日限额%@",model.onceLimit,model.dailyLimt];
        
        
    } failureBlock:^(NSString *error) {
        
        
        
    }];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.payType = VerifyTypeUnionPayBuy;
    
    
    self.title=@"确认订单";
    isBalance=NO;// 不用余额
    self.couponSubAmount=@"0.00";
    self.balanceString=@"0.00";
    self.couponIdStr=@"";
    self.totalAmount=self.amount;
    self.payByBalanceAmount=@"0.00";
    self.isCanUseCoupon=NO;
    self.view.backgroundColor=COLOR_TBBACK;
    
   
    self.payAmount = self.amount;
    

    [self initView];
    
    [self setContentView];

    
    
    [self requestBalanceData];
    [self requestCouponList];
    [self requstBankInfo];
}

-(void)initView{

    
    UIView *bottomView=[[UIView alloc]initWithFrame:CGRectMake(0,SCREEN_HEIGHT-NavBarH-50,SCREEN_WIDTH,50)];
    bottomView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:bottomView];
    
    UILabel *priceLab=[[UILabel alloc]initWithFrame:CGRectMake(10, 0, SCREEN_WIDTH-10-100, bottomView.ct_height)];
    priceLab.font=FONT(16);
    priceLab.textColor=COLOR_TEXT;
    priceLab.attributedText=[self pricFormater:self.totalAmount];
    [bottomView addSubview:priceLab];
    self.ensurePayLab=priceLab;
    
    UIButton *payBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    payBtn.frame=CGRectMake(priceLab.ct_right, 0,100, priceLab.ct_height);
    [payBtn setTitle:@"确认支付" forState:UIControlStateNormal];
    [payBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    payBtn.backgroundColor=COLOR_GLODBACKGROUD;
    payBtn.titleLabel.font=FONT(18);
    [payBtn addTarget:self action:@selector(payBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:payBtn];
    
}


-(void)setContentView
{

    UserAuthDataModel *userModel=[[UserAuthData shareInstance] userAuthData];
    
    CGFloat top_y=0.0;
    if ([self.detailModel.isGoldProduct integerValue]>0)
    {
        //黄金产品
        UILabel *currentPrice=[[UILabel alloc]initWithFrame:CGRectMake(0, 0,SCREEN_WIDTH ,40)];
        currentPrice.backgroundColor=COLOR_GLODBACKGROUD;
        currentPrice.textColor=COLOR_WHITE;
        currentPrice.text=[NSString stringWithFormat:@"实时金价 %@元/克",self.detailModel.goldPrice];
        currentPrice.textAlignment=NSTextAlignmentCenter;
        [self.view addSubview:currentPrice];

        
        CMBorderView* firstView=[[CMBorderView alloc]initWithFrame:CGRectMake(0,currentPrice.ct_bottom,SCREEN_WIDTH, 60)];
        firstView.backgroundColor=COLOR_WHITE;
        firstView.borderWidth=CMViewBorderWidthMake(0.5, 0, 0.5, 0);
        [self.view addSubview:firstView];
        
        UILabel *productNameLab=[[UILabel alloc]initWithFrame:CGRectMake(10, 0, SCREEN_WIDTH-20, 44)];
        productNameLab.text=self.detailModel.productName;
        productNameLab.font=FONT(15);
        productNameLab.textColor=COLOR_TEXT;
        [firstView addSubview:productNameLab];
        
        
        UILabel *weightlab=[[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2.0-10,5,SCREEN_WIDTH/2.0 , 20)];
        weightlab.text=[NSString stringWithFormat:@"%@克",self.weight];
        weightlab.textAlignment=NSTextAlignmentRight;
        weightlab.textColor=COLOR_TEXT;
        weightlab.font=FONT(15);
        [firstView addSubview:weightlab];
        
        UILabel *predictWeightTip=[[UILabel alloc]initWithFrame:CGRectMake(weightlab.ct_left, weightlab.ct_bottom, weightlab.ct_width, 20)];
        predictWeightTip.text=@"预计克重";
        predictWeightTip.textAlignment=NSTextAlignmentRight;
        predictWeightTip.textColor=COLOR_SUBTEXT;
        predictWeightTip.font=FONT(15);
        [firstView addSubview:predictWeightTip];
        
        UILabel *priceTipLab=[[UILabel alloc]initWithFrame:CGRectMake(0,firstView.ct_bottom+10, SCREEN_WIDTH,20)];
        priceTipLab.text=@"由于金价实时变动，实际购买克重可能与预计克重有所偏差。";
        priceTipLab.textColor=COLOR_SUBTEXT;
        priceTipLab.font=FONT(12);
        priceTipLab.textAlignment=NSTextAlignmentCenter;
        [self.view addSubview:priceTipLab];
        
        top_y=priceTipLab.ct_bottom;
        
        
    }
    else
    {
        CMBorderView* firstView=[[CMBorderView alloc]initWithFrame:CGRectMake(0,0,SCREEN_WIDTH, 44)];
        firstView.backgroundColor=COLOR_WHITE;
        firstView.borderWidth=CMViewBorderWidthMake(0.5, 0, 0.5, 0);
        [self.view addSubview:firstView];
        
        UILabel *productNameLab=[[UILabel alloc]initWithFrame:CGRectMake(10, 0, SCREEN_WIDTH-20, 44)];
        productNameLab.text=self.detailModel.productName;
        productNameLab.font=FONT(15);
        productNameLab.textColor=COLOR_TEXT;
        [firstView addSubview:productNameLab];
        
        top_y=firstView.ct_bottom;


    }
    
    //
    
    CMBorderView *contentView=[[CMBorderView alloc]initWithFrame:CGRectMake(0, top_y+10, SCREEN_WIDTH, 44*2)];
    contentView.backgroundColor=COLOR_WHITE;
    contentView.borderWidth=CMViewBorderWidthMake(0.5, 0, 0.5, 0);
    [self.view addSubview:contentView];
    
    
    UILabel *titleLab=[[UILabel alloc]initWithFrame:CGRectMake(10, 0, 100,44)];
    titleLab.text=@"订单总额";
    titleLab.textColor=COLOR_TEXT;
    titleLab.font=FONT(15);
    [contentView addSubview:titleLab];
    
    UILabel *amountLab=[[UILabel alloc]initWithFrame:CGRectMake(titleLab.ct_right+10,0 ,SCREEN_WIDTH-titleLab.ct_right-20, 44)];
    amountLab.textAlignment=NSTextAlignmentRight;
    amountLab.textColor=COLOR_TEXT;
    amountLab.font=FONT(15);
    amountLab.text=[NSString stringWithFormat:@"%.2f元",self.amount.floatValue];
    [contentView addSubview:amountLab];
    
    UIView *line=[[UIView alloc]initWithFrame:CGRectMake(0, amountLab.ct_bottom, SCREEN_WIDTH, 0.5)];
    line.backgroundColor=COLOR_LINE;
    [contentView addSubview:line];
    
    
    UILabel *discountTitlab=[[UILabel alloc]initWithFrame:CGRectMake(10,line.ct_bottom, 100, 43)];
    discountTitlab.textColor=COLOR_TEXT;
    discountTitlab.font=FONT(15);
    discountTitlab.text=@"优惠券";
    [contentView addSubview:discountTitlab];
    
    
    UILabel *discountTipLab=[[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-150, line.ct_bottom, 125, 43)];
    
    discountTipLab.textAlignment=NSTextAlignmentRight;
    discountTipLab.textColor=COLOR_TEXT;
    discountTipLab.font=FONT(15);
    discountTipLab.text=@"暂无可用优惠券";
    [contentView addSubview:discountTipLab];
    
    self.counponTiplab=discountTipLab;
    
    
    UIImage *pintImage=[UIImage imageNamed:@"pay_pointIcon"];
    UIImageView *pointImageVIew=[[UIImageView alloc]initWithFrame:CGRectMake(discountTipLab.ct_right, line.ct_bottom+(43-pintImage.size.height)/2.0, pintImage.size.width,pintImage.size.height)];
    pointImageVIew.image=pintImage;
    [contentView addSubview:pointImageVIew];
    
    UIButton *clearBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    clearBtn.frame=CGRectMake(0,line.ct_bottom, SCREEN_WIDTH, 40);
    clearBtn.backgroundColor=[UIColor clearColor];
    [clearBtn addTarget:self action:@selector(enterCouponList:) forControlEvents:UIControlEventTouchUpInside];
    [contentView addSubview:clearBtn];
    
    
    // 余额
    CMBorderView *balanceView=[[CMBorderView alloc]initWithFrame:CGRectMake(0, contentView.ct_bottom+10, SCREEN_WIDTH, 44)];
    balanceView.backgroundColor=COLOR_WHITE;
    balanceView.borderWidth=CMViewBorderWidthMake(0.5, 0, 0.5, 0);
    [self.view addSubview:balanceView];
    
    UILabel *balanceLab=[[UILabel alloc]initWithFrame:CGRectMake(10, 0, SCREEN_WIDTH-100, 44)];
    balanceLab.textColor=COLOR_TEXT;
    balanceLab.font=FONT(15);
    [balanceView addSubview:balanceLab];
    balanceLab.text=[NSString stringWithFormat:@"余额：%@元",@"0.00"];
    self.disPayBalanceLab=balanceLab;
    
    
    UISwitch *switchView=[[UISwitch alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-70, 5, 60, 35)];
    switchView.onTintColor = COLOR_MAIN;
    [switchView addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];   // 开关事件切换通知
    [balanceView addSubview: switchView];
    
    
    bankView=[[CMBorderView alloc]initWithFrame:CGRectMake(0, balanceView.ct_bottom+10, SCREEN_WIDTH, 60)];
    bankView.backgroundColor=COLOR_WHITE;
    bankView.borderWidth=CMViewBorderWidthMake(0.5, 0, 0.5, 0);
    [self.view addSubview:bankView];
    
    UILabel *bankNameLab=[[UILabel alloc]initWithFrame:CGRectMake(10,10,SCREEN_WIDTH/2.0+25, 20)];
    bankNameLab.textColor=COLOR_TEXT;
    bankNameLab.font=FONT(14);
    
    NSString *bankLastNum=[userModel.bankCard substringWithRange:NSMakeRange(userModel.bankCard.length-4, 4)];
    bankNameLab.text=[NSString stringWithFormat:@"%@ 尾号%@",userModel.bankName,bankLastNum];
    [bankView addSubview:bankNameLab];
    
    UILabel *bankTipLab=[[UILabel alloc]initWithFrame:CGRectMake(bankNameLab.ct_left, bankNameLab.ct_bottom+5, bankNameLab.ct_width, 20)];
    bankTipLab.text=@"单笔限额5万 单日限额10万元";
    bankTipLab.font=FONT(12);
    bankTipLab.textColor=COLOR_SUBTEXT;
    [bankView addSubview:bankTipLab];
    self.bankTipLabel=bankTipLab;
    
    

    UILabel*payAmountLab=[[UILabel alloc]initWithFrame:CGRectMake(bankNameLab.ct_right, 0, SCREEN_WIDTH/2.0-45, 60)];
    payAmountLab.textColor=COLOR_Red;
    payAmountLab.text=[NSString stringWithFormat:@"%@元",self.totalAmount];
    
    payAmountLab.textAlignment=NSTextAlignmentRight;
    payAmountLab.font=FONT(22);
    [bankView addSubview:payAmountLab];
    self.bankPayAmountLab=payAmountLab;
    
 
    
}

-(NSMutableAttributedString *)pricFormater:(NSString *)amount{


    NSString *priceStr=[NSString stringWithFormat:@"在线支付：¥%@", amount];
    NSMutableAttributedString *attri=[[NSMutableAttributedString alloc]initWithString:priceStr];
    [attri addAttribute:NSForegroundColorAttributeName value:COLOR_Red range:NSMakeRange(5, priceStr.length-5)];

    return attri;
}

#pragma mark--点击确认支付
-(void)payBtnClick:(UIButton *)btn
{
    //3:新手特权金
    if (self.detailModel.bigProductType.integerValue==3) {
        
        if (UDGET(NewUserTip))
        {
            [WSProgressHUD showErrorWithStatus:UDGET(NewUserTip)];
            return;
        }
    }
    
    
  
    NSNumber *amount=[NSNumber numberWithFloat:[self.amount floatValue]];;
    NSNumber *weight=[NSNumber numberWithFloat:[self.weight floatValue]];
    NSNumber *productId=[NSNumber numberWithInteger:self.detailModel.ID.integerValue];
    NSNumber *isOpenAccountPay=[NSNumber numberWithBool:isBalance];
    
    NSDictionary *dic=nil;
    if (self.isBuyByAmount)
    {
        
        dic=@{@"amount":amount,@"productId":productId,@"isOpenAccountPay":isOpenAccountPay,@"couponId":self.couponIdStr};
        
    }
    else
    {
        
        dic=@{@"gold":weight,@"productId":productId,@"isOpenAccountPay":isOpenAccountPay,@"couponId":self.couponIdStr};
    }
    __weak typeof(self) weakSelf = self;
    
    [VerifyTool verifyTradeNumWithType:self.payType amount:self.payAmount parameter:dic response:^(BOOL result, PayModel *payModel, NSString *errMsg) {
        if (result) {
            weakSelf.payModel = payModel;
            [weakSelf paySuccess];
        } else {
            [WSProgressHUD showErrorWithStatus:errMsg];
            
            NSString *newUserTip1=@"太遗憾了，购买5克249元的资格已被抢完";
            NSString *newUserTip2=@"活动期间您已购买过此产品，不可重复参与";
            
            //3:新手特权金
            if (self.detailModel.bigProductType.integerValue==3) {
                
                if ([errMsg containsString:newUserTip1]||[errMsg containsString:newUserTip2])
                {
                    UDSave(errMsg,NewUserTip);
                }
            }
    
            payFailViewController *vc = [[payFailViewController alloc] init];
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }
    }];
    
}
#pragma mark---支付成功
-(void)paySuccess{
    
    if (self.detailModel.isGoldProduct.integerValue>0)
    { 
       //黄金产品
       
        PaySuccessViewController *paySuccess=[[PaySuccessViewController alloc]init];
        paySuccess.detailModel=self.detailModel;
        paySuccess.weight=self.weight;

        if (self.couponSubAmount.floatValue<=0)
        {
            if (self.couponIdStr.length>0)
            {
                paySuccess.discountStr=self.counponTiplab.text;
            }
            
        }
        paySuccess.payModel=self.payModel;

        
        [self.navigationController pushViewController:paySuccess animated:YES];

    }
    else
    {
        //非黄金产品
        OtherPaySuccessViewController *paySuccess=[[OtherPaySuccessViewController alloc]init];
        paySuccess.detailModel=self.detailModel;
        paySuccess.payModel=self.payModel;
        if (self.couponSubAmount.floatValue<=0)
        {
            if (self.couponIdStr.length>0)
            {
                paySuccess.discountStr=self.counponTiplab.text;
            }
            
        }
        [self.navigationController pushViewController:paySuccess animated:YES];

    }
    
}
#pragma mark--是否用余额
-(void)switchAction:(UISwitch *)swich
{
   
    
    if (self.balanceString.floatValue<=0)
    {
        
        swich.on=NO;
        return;
    }
    
    isBalance= !isBalance;
    
    if (isBalance) {
        
        //用余额
        
        if (self.balanceString.floatValue>=self.totalAmount.floatValue)
        {
            //余额满足
            NSString *userAmount=self.totalAmount;
            
            self.bankPayAmountLab.text=@"0.00元";
            self.payByBalanceAmount=userAmount;
            self.ensurePayLab.attributedText=[self pricFormater:@"0.00"];
            
            self.totalAmount=@"0.00";
            
            self.payType = VerifyTypeBalancePay;//余额支付
            self.payAmount = userAmount;
        }
        else
        {
            
            NSString *bankPayAmountStr=[PriceTool numberA:self.totalAmount SubtractNumberB:self.balanceString withRoundingStyle:NSRoundDown decimalNum:2];
            //剩余要支付的金额
            self.totalAmount=bankPayAmountStr;
            
            self.bankPayAmountLab.text=[NSString stringWithFormat:@"%@元",bankPayAmountStr];
            self.ensurePayLab.attributedText=[self pricFormater:bankPayAmountStr];

            self.payByBalanceAmount=self.balanceString;
            
//            self.payCodeView.payAmountStr=bankPayAmountStr;
//            self.payChargePaswView.payAmountStr=bankPayAmountStr;
            
            self.payType = VerifyTypeUnionPayBuy;//银行卡支付
            self.payAmount = bankPayAmountStr;

        }

    
        
    }else {
     
        //用银行卡
        self.totalAmount=[PriceTool numberA:self.amount SubtractNumberB:self.couponSubAmount withRoundingStyle:NSRoundDown decimalNum:2];
        self.bankPayAmountLab.text=[NSString stringWithFormat:@"%@元",self.totalAmount];
        self.ensurePayLab.attributedText=[self pricFormater:self.totalAmount];
        
        
        self.payByBalanceAmount=@"0.00";
        
//        self.payCodeView.payAmountStr=self.totalAmount;
//        self.payChargePaswView.payAmountStr=self.totalAmount;
        
        self.payType = VerifyTypeUnionPayBuy;//银行卡支付
        self.payAmount = self.totalAmount;
       


    }
    
   
}

#pragma mark--进入优惠券列表
-(void)enterCouponList:(UIButton *)btn
{
    
    if (!self.isCanUseCoupon)
    {
        return;
    }
    
     __weak typeof(self) weakSelf = self;
    
    NSDictionary *dic=nil;
  
    
    if (self.detailModel.isGoldProduct.integerValue>0)
    {
        dic=@{@"productId":self.detailModel.ID,@"buyGold":self.weight};
    }
    else
    {
        dic=@{@"productId":self.detailModel.ID,@"buyAmount":self.amount};
    }

    //PayPacketAndCouponViewController
    PayPacketAndCouponViewController *vc=[[PayPacketAndCouponViewController alloc]init];
//    vc.isUseCoupon=YES;
    vc.dataDic=dic;
    vc.userCoupon = ^(DiscountCouponModel *model) {
    
        if (model.couponType.integerValue==2)
        {
            //使用加息券
            weakSelf.payAmount=weakSelf.amount;
            weakSelf.totalAmount=weakSelf.amount;
            weakSelf.couponSubAmount=@"0";
            
            if (model.increaseValue.floatValue<=0)
            {
                weakSelf.counponTiplab.text=[NSString stringWithFormat:@"%@%@加息券%@天", @"0",@"%",model.increaseDay];
                
            }
            else
            {
               NSString *increaseHandleStr=[PriceTool numberPointHandle:[NSString stringWithFormat:@"%@",model.increaseValue] withRoundingStyle:NSRoundDown decimalNum:2];
            
                weakSelf.counponTiplab.text=[NSString stringWithFormat:@"%@%@加息券%@天", increaseHandleStr,@"%",model.increaseDay];
                
            }
           
        }
        else
        {
            //使用现金红包
            //当前使用优惠券后需要支付的金额
            NSString *currentAount=[PriceTool numberA:self.amount SubtractNumberB:model.couponAmount withRoundingStyle:NSRoundDown decimalNum:2];
            
            weakSelf.totalAmount=currentAount;
            weakSelf.payAmount = currentAount;

            weakSelf.couponSubAmount=model.couponAmount;
            
            weakSelf.counponTiplab.text=[NSString stringWithFormat:@"-%@元",model.couponAmount];
            
        }

        if (isBalance)
        {
            //使用余额
            if (weakSelf.balanceString.floatValue>=weakSelf.totalAmount.floatValue)
            {
                //余额足够
                weakSelf.payAmount = weakSelf.totalAmount;
                weakSelf.totalAmount=@"0.00";
            }
            else
            {
                //余额不够
                weakSelf.totalAmount=[PriceTool numberA:weakSelf.totalAmount SubtractNumberB:weakSelf.balanceString withRoundingStyle:NSRoundDown decimalNum:2];
                weakSelf.payAmount = weakSelf.totalAmount;
            }
            
            
        }
       
        weakSelf.couponIdStr=model.ID;

        weakSelf.bankPayAmountLab.text=[NSString stringWithFormat:@"%@元",weakSelf.totalAmount];
        weakSelf.ensurePayLab.attributedText=[weakSelf pricFormater:weakSelf.totalAmount];

    };
   
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
