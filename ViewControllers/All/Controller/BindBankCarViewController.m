//
//  BindBankCarViewController.m
//  GoldCatBank
//
//  Created by Sunny on 2017/8/30.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "BindBankCarViewController.h"
#import "CMBorderView.h"
#import "AllNetWorkRquest.h"
#import "SetChargePasswordViewController.h"
#import "UIBarButtonItem+item.h"
#import "BankListView.h"
#import "BankListModel.h"
#import "UserData.h"
#import "CBWebViewController.h"
#import "UserAuthData.h"
@interface BindBankCarViewController ()<UITextFieldDelegate>
{
    UITextField *nameTxf;
    UITextField *IDCarTxf;
    
    UITextField *bankCarTxf;
    UITextField *phoneTxf;
    BOOL isSupport;

    CGRect boxFrame;
}
@property(nonatomic,strong)UILabel *bankNameLab;
@property(nonatomic,strong)BankListView *bankListView;
@property(nonatomic,strong)UIImageView *pointImageView;

/** uibutton */
@property(nonatomic, weak) IBOutlet UIButton *clearBtn;
@end

@implementation BindBankCarViewController


-(void)requstBankList{
    
     __weak typeof(self) weakSelf = self;
    [AllNetWorkRquest getBankInfo:nil successBlock:^(id responseBody) {
        
        NSArray *arr=[BankListModel mj_objectArrayWithKeyValuesArray:(NSArray *)responseBody];
        
        weakSelf.bankListView.bankListArr=arr;
        
        
    } failureBlock:^(NSString *error) {
        
   
        
    }];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=COLOR_TBBACK;
    
//    self.navigationItem.leftBarButtonItem = [UIBarButtonItem backItemWithimage:[UIImage imageNamed:@"navigationButtonReturn"] highImage:[UIImage imageNamed:@"navigationButtonReturnClick"]  target:self action:@selector(back) title:@""];
    UIButton *backBarButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    [backBarButton setImage:[UIImage imageNamed:@"navigationButtonReturn"] forState:UIControlStateNormal];
    [backBarButton setImage:[UIImage imageNamed:@"navigationButtonReturnClick"] forState:UIControlStateHighlighted];
    backBarButton.jk_touchAreaInsets = UIEdgeInsetsMake(20, 20, 20, 20);
    backBarButton.imageEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 25);
    [backBarButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backBarButton];

    

    self.title=@"绑定信息";
    [self initContentView];
    [self.view addSubview:self.bankListView];
    [self.bankListView setHidden:YES];
    
    __weak typeof(self) weakSelf=self;
    
    self.bankListView.viewHidden = ^{
        
//        weakSelf.pointImageView.transform=CGAffineTransformIdentity;
        [weakSelf clearBtnClick:weakSelf.clearBtn];
    };
    [self requstBankList];
    
    
}

-(BankListView *)bankListView
{
    if (!_bankListView) {
        
        _bankListView=[[BankListView alloc]initWithFrame:CGRectMake(0,0, SCREEN_WIDTH,SCREEN_HEIGHT-NavBarHeight)];
        
    }
    
    return _bankListView;
}

-(void)initContentView
{
    CMBorderView *topView=[[CMBorderView alloc]initWithFrame:CGRectMake(0,10,SCREEN_WIDTH,100)];
    topView.backgroundColor=COLOR_WHITE;
    topView.borderWidth=CMViewBorderWidthMake(0.5, 0, 0.5, 0);
    [self.view addSubview:topView];
    
    nameTxf=[[UITextField alloc]initWithFrame:CGRectMake(10, 0, SCREEN_WIDTH-20,49)];
    nameTxf.placeholder=@"请输入您的真实姓名";
    nameTxf.textColor=COLOR_SUBTEXT;
    nameTxf.font=FONT(16);
    nameTxf.delegate=self;
    nameTxf.jk_maxLength = 4;
    [topView addSubview:nameTxf];
    
    UIView *line=[[UIView alloc]initWithFrame:CGRectMake(nameTxf.ct_left, nameTxf.ct_bottom, SCREEN_WIDTH-10, 0.5)];
    line.backgroundColor=COLOR_LINE;
    [topView addSubview:line];
    
    IDCarTxf=[[UITextField alloc]initWithFrame:CGRectMake(nameTxf.ct_left, line.ct_bottom, nameTxf.ct_width, nameTxf.ct_height)];
    IDCarTxf.placeholder=@"请输入您的身份证号";
    IDCarTxf.textColor=COLOR_SUBTEXT;
    IDCarTxf.jk_maxLength = 18;
    IDCarTxf.font=FONT(16);
    IDCarTxf.delegate=self;
    [topView addSubview:IDCarTxf];
    
    
    CMBorderView *bankView=[[CMBorderView alloc]initWithFrame:CGRectMake(0,topView.ct_bottom+10,SCREEN_WIDTH,100)];
    bankView.backgroundColor=COLOR_WHITE;
    bankView.borderWidth=CMViewBorderWidthMake(0.5, 0, 0.5, 0);
    [self.view addSubview:bankView];
    
    bankCarTxf=[[UITextField alloc]initWithFrame:CGRectMake(10, 0,nameTxf.ct_width-100,49)];
    bankCarTxf.placeholder=@"请输入银行卡号";
    bankCarTxf.textColor=COLOR_SUBTEXT;
    bankCarTxf.font=FONT(16);
    bankCarTxf.delegate=self;
    bankCarTxf.keyboardType=UIKeyboardTypePhonePad;
    bankCarTxf.jk_maxLength=20;
    [bankView addSubview:bankCarTxf];
    
    UILabel *bankNameLab=[[UILabel alloc]initWithFrame:CGRectMake(bankCarTxf.ct_right, bankCarTxf.ct_top,SCREEN_WIDTH-bankCarTxf.ct_right-10, bankCarTxf.ct_height)];
    bankNameLab.textAlignment=NSTextAlignmentRight;
    bankNameLab.font=FONT(16);
    bankNameLab.textColor=COLOR_SUBTEXT;
    [bankView addSubview:bankNameLab];
    
    self.bankNameLab=bankNameLab;
    
    UIView *line2=[[UIView alloc]initWithFrame:CGRectMake(bankCarTxf.ct_left, bankCarTxf.ct_bottom, SCREEN_WIDTH-10, 0.5)];
    line2.backgroundColor=COLOR_LINE;
    [bankView addSubview:line2];
    
    phoneTxf=[[UITextField alloc]initWithFrame:CGRectMake(bankCarTxf.ct_left, line2.ct_bottom, SCREEN_WIDTH-20, bankCarTxf.ct_height)];
    phoneTxf.placeholder=@"请输入银行预留手机号";
    phoneTxf.textColor=COLOR_SUBTEXT;
    phoneTxf.font=FONT(16);
    phoneTxf.keyboardType=UIKeyboardTypePhonePad;
    phoneTxf.jk_maxLength=11;
    phoneTxf.delegate=self;
    [bankView addSubview:phoneTxf];
    
    UILabel *banlistLab=[[UILabel alloc]initWithFrame:CGRectMake(10, bankView.ct_bottom+10, 160, 20)];
    banlistLab.font=FONT(15);
    banlistLab.textColor=COLOR_SUBTEXT;
    banlistLab.text=@"点击查看支持银行列表";
    [self.view addSubview:banlistLab];
    
    UIImage *pointImage=[UIImage imageNamed:@"pay_pointIcon"];
    UIImageView *pointImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0,0, pointImage.size.width, pointImage.size.height)];
    pointImageView.center=CGPointMake(banlistLab.ct_right+pointImage.size.width/2.0, banlistLab.center.y);
    pointImageView.image=pointImage;
    pointImageView.userInteractionEnabled=NO;
    [self.view addSubview:pointImageView];
    
    self.pointImageView=pointImageView;
    
    UIButton *clearBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    clearBtn.frame=CGRectMake(0,bankView.ct_bottom, banlistLab.ct_width+30, 40);
    clearBtn.backgroundColor=[UIColor clearColor];
    [clearBtn addTarget:self action:@selector(clearBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:clearBtn];
    
    clearBtn.timeInterval = 0.3;
    
    self.clearBtn = clearBtn;
    
    self.bankListView.boxOri_y=clearBtn.ct_bottom+10;
    
    //提交按钮
    UIButton *commitBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    commitBtn.frame=CGRectMake(20,banlistLab.ct_bottom+50, SCREEN_WIDTH-40, 50);
    commitBtn.backgroundColor=COLOR_GLODBACKGROUD;
    commitBtn.layer.masksToBounds=YES;
    commitBtn.layer.cornerRadius=8;
    [commitBtn setTitle:@"确定绑定" forState:UIControlStateNormal];
    [commitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [commitBtn addTarget:self action:@selector(commitBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:commitBtn];
    
    
    NSString *userProctolStr=@"确认提交即表示同意《用户授权协议》";
    NSMutableAttributedString *userAgreeAtt=[[NSMutableAttributedString alloc]initWithString:userProctolStr];
    [userAgreeAtt addAttribute:NSForegroundColorAttributeName value:UIColorFromHex(0x404df1) range:NSMakeRange(userProctolStr.length-8, 8)];
    [userAgreeAtt addAttribute:NSForegroundColorAttributeName value:COLOR_SUBTEXT range:NSMakeRange(0,userProctolStr.length-8)];
    [userAgreeAtt addAttribute:NSFontAttributeName value:FONT(12) range:NSMakeRange(0, userProctolStr.length)];
    
    UIButton *userAgreeBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    userAgreeBtn.frame=CGRectMake(0, 0,220, 20);
    userAgreeBtn.center=CGPointMake(SCREEN_WIDTH/2.0, commitBtn.ct_bottom+20);
    [userAgreeBtn setAttributedTitle:userAgreeAtt forState:UIControlStateNormal];
    [userAgreeBtn addTarget:self action:@selector(userAgreeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    userAgreeBtn.titleLabel.font=FONT(12);
    userAgreeBtn.titleLabel.textAlignment=NSTextAlignmentLeft;
    [self.view addSubview:userAgreeBtn];
    
    
    NSString *tipStr=@"信息认证将您的的资产与个人信息绑定，保障您的账户资产安全。认证成功后将为您申请与个人身份证绑定的电子签名证书，确保您和招金猫签订的投资协议合法有效。";
    UILabel *tipContentLab=[[UILabel alloc]initWithFrame:CGRectMake(0, 0,SCREEN_WIDTH-80, 80)];
    tipContentLab.text=tipStr;
    tipContentLab.center=CGPointMake(SCREEN_WIDTH/2.0, userAgreeBtn.ct_bottom+40);
    tipContentLab.textColor=COLOR_SUBTEXT;
    tipContentLab.font=FONT(12);
    tipContentLab.numberOfLines=0;
    [self.view addSubview:tipContentLab];

}


#pragma mark--提交按钮

-(void)commitBtnClick:(UIButton *)btn{
    
    [self.view endEditing:YES];
    if (nameTxf.text.length==0)
    {
        [WSProgressHUD showErrorWithStatus:nameTxf.placeholder];
        return;
    }
    if (![NSString jk_accurateVerifyIDCardNumber:IDCarTxf.text])
    {
        [WSProgressHUD showErrorWithStatus:@"请输入正确的身份证号"];
        return;
    }
    if (bankCarTxf.text.length==0)
    {
        [WSProgressHUD showErrorWithStatus:bankCarTxf.placeholder];
        return;
    }
    if (!validateMobile(phoneTxf.text))
    {
        [WSProgressHUD showErrorWithStatus:@"请输入正确的手机号"];
        return;
    }
    
    if (!isSupport) {
        
        [WSProgressHUD showErrorWithStatus:@"该银行卡不支持"];
        return;
    }

    
    UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"" message:@"请再次核对您的姓名和身份信息" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction=[UIAlertAction actionWithTitle:@"修改" style:UIAlertActionStyleCancel handler:nil];

    UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"无误" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self sendRequest];
       
        
    }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
    
    
    
}


-(void)sendRequest{
    
  __weak typeof(self) weakSelf = self;
    
    UserInfoModel *model=[[UserData instance]getUserInfo];
    
    NSDictionary *dic=@{@"userName":nameTxf.text,@"userIdCard":IDCarTxf.text,@"bankIdNumber":bankCarTxf.text,@"cellphone":phoneTxf.text,@"userId":model.userId};
    [WSProgressHUD showWithMaskType:WSProgressHUDMaskTypeBlack];
    [AllNetWorkRquest bankIcarBindWithData:dic successBlock:^(id responseBody) {
        [WSProgressHUD dismiss];
        if (responseBody[@"results"])
        {
            if ([responseBody[@"results"] integerValue]>0)
            {
                [WSProgressHUD showSuccessWithStatus:@"认证成功!"];
                
                UserAuthDataModel *userModel=[[UserAuthData shareInstance]userAuthData];
                
                if (userModel.hasTradePwd.integerValue>0)
                {
                    //有交易密码
                    [self.navigationController popViewControllerAnimated:YES];
                }
                else
                {
                    //无交易密码
                    SetChargePasswordViewController*vc=[[SetChargePasswordViewController alloc]init];
                    vc.isBind=YES;
                    [weakSelf.navigationController pushViewController:vc animated:YES];
                }
                
            }
            else
            {
//                [WSProgressHUD showErrorWithStatus:responseBody[@"errMsg"]] ;
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"绑定失败" message:responseBody[@"errMsg"] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
                [alert addAction:action];
                [weakSelf.navigationController presentViewController:alert animated:YES completion:nil];
                
            }
        }
        
        
    } failureBlock:^(NSString *error) {
        [WSProgressHUD dismiss];
        
    }];
    

}


-(void)clearBtnClick:(UIButton *)btn
{
    
    static BOOL isSelect=NO;
    
    isSelect=!isSelect;

    
    if (isSelect)
    {
        [UIView animateWithDuration:0.3 animations:^{
            
            self.pointImageView.transform=CGAffineTransformMakeRotation(M_PI/2.0);
            
            [self.bankListView setHidden:NO];
        }];
    }
    else
    {
        [UIView animateWithDuration:0.3 animations:^{
            
            self.pointImageView.transform = CGAffineTransformIdentity;
            
             [self.bankListView setHidden:YES];
            

        }];
        
    }
    
    
}

-(void)userAgreeBtnClick:(UIButton *)btn{
    
    
    CBWebViewController *vc=[[CBWebViewController alloc]init];
    vc.urlString=@"https://www.zhaojinmao.cn/testwebapp/me/User_agreement/index1.html";
    [self.navigationController pushViewController:vc animated:YES];
    
    
}


#pragma mark--textFieldDelegate


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    
    if ([string isEqualToString:@" "])
    {
        
        return NO;
    }
//    NSString *displayString=nil;
//    
//    if (range.length>0)
//    {
//        NSMutableString *contentStr=[[NSMutableString alloc]initWithString:textField.text];
//        [contentStr deleteCharactersInRange:range];
//        displayString=contentStr;
//    }
//    else
//    {
//        NSMutableString *contentStr=[[NSMutableString alloc]initWithString:textField.text];
//        [contentStr insertString:string atIndex:range.location];
//        displayString=contentStr;
//    }
    
    
    return YES;
}



- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==bankCarTxf)
    {
        
        if (bankCarTxf.text.length>0)
        {
           
            __weak typeof(self) weakSelf = self;
            NSDictionary *dic=@{@"bankCardNum":bankCarTxf.text};
            [AllNetWorkRquest searchBankIcarIDIsSupport:dic successBlock:^(id responseBody) {
                
                
                if (responseBody[@"isSupport"])
                {
                    if ([responseBody[@"isSupport"] integerValue]>0)
                    {
                        weakSelf.bankNameLab.text=responseBody[@"bankName"];
                        isSupport=YES;
                    }
                    else
                    {
                        [WSProgressHUD showErrorWithStatus:@"此银行卡不支持"];
                        isSupport=NO;
                    }

                }

            
                
            } failureBlock:^(NSString *error) {
                
                isSupport=NO;
            }];
        }
   
        
    }
}


-(void)back
{
 
    UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"" message:@"您的信息尚未完善\n是否放弃" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction=[UIAlertAction actionWithTitle:@"放弃" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"继续完善" style:UIAlertActionStyleDefault handler:nil];

    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
