//
//  PayModel.h
//  GoldCatBank
//
//  Created by Sunny on 2017/8/30.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "BaseModel.h"
//银行卡支付Model

@interface PayModel : BaseModel
@property(nonatomic,copy)NSString * userId;
@property(nonatomic,copy)NSString * orderNo;
@property(nonatomic,copy)NSString * payWay;
@property(nonatomic,copy)NSString * payableAmount;//应付金额
@property(nonatomic,copy)NSString * payAmount;//实付金额
@property(nonatomic,copy)NSString * couponAmount;//优惠券金额
@property(nonatomic,strong)NSDictionary* purchaseUserInfo;
@property(nonatomic,copy)NSString * useAccountAmount;//使用的账户余额
@property(nonatomic,copy)NSString * bankAmount;//使用银行卡支付的金额
@property(nonatomic,copy)NSString * mchntcd;
@property(nonatomic,copy)NSString * fuyouSecketKey;
@property(nonatomic,copy)NSString * isAuthentication;
@property(nonatomic,copy)NSString * expirationDate;
@property(nonatomic,copy)NSString *interestTime;
@property(nonatomic,copy)NSString *transactionTime;
@property(nonatomic,copy)NSString *goldPrice;
@property(nonatomic,copy)NSString * errCode;
@property(nonatomic,copy)NSString * errMsg;

@end
