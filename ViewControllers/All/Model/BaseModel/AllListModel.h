//
//  AllListModel.h
//  GoldCatBank
//
//  Created by 高丹丹 on 2017/8/25.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "BaseModel.h"

@interface AllListModel : BaseModel
@property(nonatomic,copy)NSString *ID;

@property(nonatomic,copy)NSString *productName;

@property(nonatomic,copy)NSString *profitRatio;

@property(nonatomic,copy)NSString *h5URL;

@property(nonatomic,copy)NSString *smallTitle1;

@property(nonatomic,copy)NSString *smallTitle2;

@property(nonatomic,copy)NSString *isGoldProduct;

@property(nonatomic,copy)NSString *isVip;

@property(nonatomic,copy)NSString *realTimeGoldPrice;

@property(nonatomic,copy)NSString *sort;

@property(nonatomic,copy)NSString *bigProductType;

@property(nonatomic,copy)NSString *hasLeaf;
/** 已售份数/克数 */
@property(nonatomic, copy) NSString *sellCount;

@property(nonatomic, copy) NSString *isApply;
/** 稳健型 */
@property(nonatomic, copy) NSString *riskLevel;
/** 5克起购 */
@property(nonatomic, copy) NSString *purchaseStartAmount;
/** 天数 */
@property(nonatomic, copy) NSString *termOfInvestment;

@end
