//
//  PayModel.m
//  GoldCatBank
//
//  Created by Sunny on 2017/8/30.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "PayModel.h"

@implementation PayModel


- (id)mj_newValueFromOldValue:(id)oldValue property:(MJProperty *)property
{
    if ([property.name isEqualToString:@"payableAmount"]||[property.name isEqualToString:@"payAmount"]||[property.name isEqualToString:@"couponAmount"]||[property.name isEqualToString:@"useAccountAmount"]||[property.name isEqualToString:@"bankAmount"])
    {
        
        NSString *amount=[PriceTool numberPointHandle:[NSString stringWithFormat:@"%@",oldValue] withRoundingStyle:NSRoundDown decimalNum:2];
        
        return amount;
        
    }

    return oldValue;
}

@end
