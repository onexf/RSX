//
//  ProductDetailModel.h
//  GoldCatBank
//
//  Created by 高丹丹 on 2017/8/28.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "BaseModel.h"

@interface ProductDetailModel : BaseModel

@property(nonatomic,copy)NSString* goldType;
@property(nonatomic,copy)NSString* h5URL;
@property(nonatomic,copy)NSString* ID;
@property(nonatomic,copy)NSString* purchasePoundage;
@property(nonatomic,copy)NSString* totalSale;
@property(nonatomic,copy)NSString* isOffShelf;
@property(nonatomic,copy)NSString* sellCount;
@property(nonatomic,copy)NSString* oneDayAnnualizedRate;
@property(nonatomic,copy)NSString* endEarningsDay;
@property(nonatomic,copy)NSString* redeemLimit;
@property(nonatomic,copy)NSString* sort;
@property(nonatomic,copy)NSString* goldRoseEarnings;
@property(nonatomic,copy)NSString* purchaseStartGold;
@property(nonatomic,copy)NSString* purchaseEndGold;
@property(nonatomic,copy)NSString* realTimeGoldPrice;
@property(nonatomic,copy)NSString* interestCalculationExpression;
@property(nonatomic,copy)NSString* interestPayment;
@property(nonatomic,copy)NSString* purchaseStartAmount;
@property(nonatomic,copy)NSString* yearAnnualizedRate;
@property(nonatomic,copy)NSString* annualizedIncomeTxt;
@property(nonatomic,copy)NSString* goldPrice;
@property(nonatomic,copy)NSString* automaticExpiration;
@property(nonatomic,copy)NSString* protocolUrl;
@property(nonatomic,copy)NSString* smallTitle2;
@property(nonatomic,copy)NSString* redeemExplain;
@property(nonatomic,copy)NSString* interestCalculationDetails;
@property(nonatomic,copy)NSString* grantPostion;
@property(nonatomic,copy)NSString* redeemMode;
@property(nonatomic,copy)NSString* createTime;

@property(nonatomic,copy)NSString* termOfInvestment;
@property(nonatomic,copy)NSString* earningsReleaseDay;
@property(nonatomic,copy)NSString* productName;
@property(nonatomic,copy)NSString* purchaseEndAmount;
@property(nonatomic,copy)NSString* riskLevel;
@property(nonatomic,copy)NSString* isGoldProduct;
@property(nonatomic,copy)NSString* smallTitle1;
@property(nonatomic,copy)NSString* tips;
@property(nonatomic,copy)NSString* incomeStructure;
@property(nonatomic,copy)NSString* startEarningsDay;
@property(nonatomic,copy)NSString* profitRatio;
@property(nonatomic,copy)NSString* productDetails;
@property(nonatomic,copy)NSString* interestTime;//记息日
@property(nonatomic,copy)NSString* toPayPageID;//0 1 2跳三种不同买入页面
@property(nonatomic,copy)NSString* guaranteeStartTime;//保障开始时间
@property(nonatomic,copy)NSString* guaranteeEndTime;//保障结束时间
@property(nonatomic,copy)NSString* couponCount;//保障结束时间
@property(nonatomic,copy)NSString* profitShowTxt;//年化收益率文字
@property(nonatomic,copy)NSString *expirationDate;//到期日
@property(nonatomic,copy)NSString *bigProductType;

@property(nonatomic,copy)NSString *ProfitRatio;
@property(nonatomic,copy)NSString *RealTimeGoldPrice;
@property(nonatomic,copy)NSString *discountPrice;
@property(nonatomic,copy)NSString *isApply;
@end
