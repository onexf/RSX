//
//  BankListModel.h
//  GoldCatBank
//
//  Created by Sunny on 2017/9/13.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "BaseModel.h"

@interface BankListModel : BaseModel
@property(nonatomic,strong)NSString *ID;
@property(nonatomic,strong)NSString *bankName;
@property(nonatomic,strong)NSString *dailyLimt;
@property(nonatomic,strong)NSString *monthLimt;
@property(nonatomic,strong)NSString *logo;
@property(nonatomic,strong)NSString *onceLimit;

@end
