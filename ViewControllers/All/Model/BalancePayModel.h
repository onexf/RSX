//
//  BalancePayModel.h
//  GoldCatBank
//
//  Created by Sunny on 2017/8/31.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "BaseModel.h"

@interface BalancePayModel : BaseModel
@property(nonatomic,copy)NSString *orderNo;
@property(nonatomic,copy)NSString *amount;
@property(nonatomic,copy)NSString *errCode;
@end
