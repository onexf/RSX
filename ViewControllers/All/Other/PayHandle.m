//
//  PayHandle.m
//  GoldCatBank
//
//  Created by Sunny on 2017/8/30.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "PayHandle.h"

@implementation PayHandle

+(NSDictionary *)payInfomationWithUserInfo:(NSDictionary *)dict
{
    NSString * mchntCd = dict[@"mchntcd"];
    NSString * bankCard = dict[@"bankCard"];
    NSString * amt = [NSString stringWithFormat:@"%ld",lroundf([dict[@"amount"] floatValue] * 100)];//  NSString *orderAmount = [NSString stringWithFormat:@"%ld",lroundf(orderID.floatValue * 100)];
    
    NSString * mchntOrdId = dict[@"transSerialNumber"];
    NSString * idNo = dict[@"idCard"];
    NSString * name = dict[@"realName"];
    NSString * userId = dict[@"userId"];
    NSString * idType = @"0";
    
    
    NSString * myVERSION = [NSString stringWithFormat:@"2.0"] ;     //SDK 接口版本参数  定值
    NSString * myMCHNTCD = mchntCd ;                      // 商户号
    NSString * myMCHNTORDERID = mchntOrdId ;              // 商户订单号
    NSString * myUSERID = userId ;                        // 用户编号
    NSString * myAMT = amt ;                              // 金额
    NSString * myBANKCARD = bankCard ;                    // 银行卡号
    NSString * myBACKURL = [NSString stringWithFormat:@"http://api.zhaojinmao.cn/api/money/UpdateChangeFromMobile"] ;                                // 回调地址
    
    NSString * myNAME = name ;                            // 姓名
    NSString * myIDNO = idNo ;                            // 证件编号
    NSString * myIDTYPE = idType ;                        // 证件类型(目前只支持身份证)
    NSString * myTYPE = [NSString stringWithFormat:@"02"] ;
    NSString * mySIGNTP = [NSString stringWithFormat:@"MD5"] ;
    NSString * myMCHNTCDKEY = [NSString stringWithFormat:@"5c4mcpqvgslhv84cmgpazqixxwwprbie"] ;//商户秘钥5c4mcpqvgslhv84cmgpazqixxwwprbie  富友 5old71wihg2tqjug9kkpxnhx9hiujoqj
    
    
    NSString * mySIGN = [NSString stringWithFormat:@"%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@" , myTYPE,myVERSION,myMCHNTCD,myMCHNTORDERID,myUSERID,myAMT,myBANKCARD,myBACKURL,myNAME,myIDNO,myIDTYPE,myMCHNTCDKEY] ;
    NSLog(@"sign = %@",mySIGN) ;
    mySIGN = [mySIGN jk_md5String] ;                         //签名字段
    
    //添加环境参数  BOOL 变量 @"TEST"   YES 是测试  NO 是生产。  使用生产环境时 ，请传入真实信息，并替换生产商户号和秘钥
    
    //富友提供测试用的生产环境商户号和秘钥 ： mchntcd : 0002900F0280321       key : fx45crzkmo8akn24plwvrv8ywd10zjuy
    //测试商户号和秘钥                    mchntcd : 0002900F0096235       key : 5old71wihg2tqjug9kkpxnhx9hiujoqj
    BOOL test = NO;
    NSNumber * testNumber = [NSNumber numberWithBool:test] ;
    NSDictionary * dicD = @{@"TYPE":myTYPE,@"VERSION":myVERSION,@"MCHNTCD":myMCHNTCD,@"MCHNTORDERID":myMCHNTORDERID,@"USERID":myUSERID,@"AMT":myAMT,@"BANKCARD":myBANKCARD,@"BACKURL":myBACKURL,@"NAME":myNAME,@"IDNO":myIDNO,@"IDTYPE":myIDTYPE,@"SIGNTP":mySIGNTP,@"SIGN":mySIGN , @"TEST" : testNumber} ;
    NSLog(@"😄dicD =%@ " , dicD) ;
    
    return dicD;
    
}
@end
