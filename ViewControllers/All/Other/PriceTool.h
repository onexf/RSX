//
//  PriceTool.h
//  GoldCatBank
//
//  Created by 高丹丹 on 2017/8/25.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface PriceTool : NSObject

#pragma mark---对小数点位数处理
+(NSString *)numberPointHandle:(NSString *)num withRoundingStyle:(NSRoundingMode)roundMode decimalNum:(NSInteger)decimalNum;

+(NSString *)numberA:(NSString *)numberA addNumberB:(NSString *)numberB withRoundingStyle:(NSRoundingMode)roundMode decimalNum:(NSInteger)decimalNum;

//a-b
+(NSString *)numberA:(NSString *)numberA SubtractNumberB:(NSString *)numberB withRoundingStyle:(NSRoundingMode)roundMode decimalNum:(NSInteger)decimalNum;
//a*b
+(NSString *)numberA:(NSString *)numberA MultiplyNumberB:(NSString *)numberB withRoundingStyle:(NSRoundingMode)roundMode decimalNum:(NSInteger)decimalNum;
//a/b
+(NSString *)numberA:(NSString *)numberA DivideNumberB:(NSString *)numberB withRoundingStyle:(NSRoundingMode)roundMode decimalNum:(NSInteger)decimalNum;

@end
