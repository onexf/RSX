//
//  PayHandle.h
//  GoldCatBank
//
//  Created by Sunny on 2017/8/30.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PayHandle : NSObject

+(NSDictionary *)payInfomationWithUserInfo:(NSDictionary *)dict;

@end
