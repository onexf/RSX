//
//  PriceTool.m
//  GoldCatBank
//
//  Created by 高丹丹 on 2017/8/25.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "PriceTool.h"

@implementation PriceTool

//RoundPlain,//四舍五入
//RoundDown,//（向下取整）相当于直接截取，不做操作
//RoundUp,//向上取整

//a+b

+(NSString *)numberPointHandle:(NSString *)num withRoundingStyle:(NSRoundingMode)roundMode decimalNum:(NSInteger)decimalNum

{
    NSDecimalNumber *numA=[NSDecimalNumber decimalNumberWithString:@"0"];
    
    NSDecimalNumber *number=[NSDecimalNumber decimalNumberWithString:num];
    
    NSDecimalNumberHandler *handel = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:roundMode scale:decimalNum raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:NO];
    
    NSDecimalNumber *numC=[number decimalNumberByAdding:numA withBehavior:handel];
    
    return [numC stringValue];
}
+(NSString *)numberA:(NSString *)numberA addNumberB:(NSString *)numberB withRoundingStyle:(NSRoundingMode)roundMode decimalNum:(NSInteger)decimalNum
{
    
    NSDecimalNumber *numA=[NSDecimalNumber decimalNumberWithString:numberA];
    NSDecimalNumber *numB=[NSDecimalNumber decimalNumberWithString:numberB];
    
    NSDecimalNumberHandler *handel = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:roundMode scale:decimalNum raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:NO];
    
    NSDecimalNumber *numC=[numA decimalNumberByAdding:numB withBehavior:handel];
    
    return [numC stringValue];
}


//a-b
+(NSString *)numberA:(NSString *)numberA SubtractNumberB:(NSString *)numberB withRoundingStyle:(NSRoundingMode)roundMode decimalNum:(NSInteger)decimalNum
{
    NSDecimalNumber *numA=[NSDecimalNumber decimalNumberWithString:numberA];
    NSDecimalNumber *numB=[NSDecimalNumber decimalNumberWithString:numberB];

    NSDecimalNumberHandler *handel = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:roundMode scale:decimalNum raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:NO];
    
    NSDecimalNumber *numC=[numA decimalNumberBySubtracting:numB withBehavior:handel];
  
    
    return [numC stringValue];
}
//a*b
+(NSString *)numberA:(NSString *)numberA MultiplyNumberB:(NSString *)numberB withRoundingStyle:(NSRoundingMode)roundMode decimalNum:(NSInteger)decimalNum
{
    NSDecimalNumber *numA=[NSDecimalNumber decimalNumberWithString:numberA];
    NSDecimalNumber *numB=[NSDecimalNumber decimalNumberWithString:numberB];
    
    NSDecimalNumberHandler *handel = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:roundMode scale:decimalNum raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:NO];
    
    NSDecimalNumber *numC=[numA decimalNumberByMultiplyingBy:numB withBehavior:handel];
    return [numC stringValue];
}
//a/b
+(NSString *)numberA:(NSString *)numberA DivideNumberB:(NSString *)numberB withRoundingStyle:(NSRoundingMode)roundMode decimalNum:(NSInteger)decimalNum
{
    NSDecimalNumber *numA=[NSDecimalNumber decimalNumberWithString:numberA];
    NSDecimalNumber *numB=[NSDecimalNumber decimalNumberWithString:numberB];
    
    NSDecimalNumberHandler *handel = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:roundMode scale:decimalNum raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:NO];
    
    NSDecimalNumber *numC=[numA decimalNumberByDividingBy:numB withBehavior:handel];

    return [numC stringValue];
    
}


@end
