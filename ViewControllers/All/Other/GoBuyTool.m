//
//  GoBuyTool.m
//  GoldCatBank
//
//  Created by Sunny on 2017/10/31.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "GoBuyTool.h"
#import "GoldUpAndDownViewController.h"
#import "SavePotViewController.h"
#import "ApplyBuyOrderViewController.h"
#import "FlowCashViewController.h"
#import "SaveCashViewController.h"


@implementation GoBuyTool

+(UIViewController *)goBuyChoosePageWithDetailModel:(ProductDetailModel *)model{
    
    
    if (model.bigProductType.integerValue==11||model.bigProductType.integerValue==10)
    {
        //10黄金看涨、11黄金看跌
        GoldUpAndDownViewController *vc=[[GoldUpAndDownViewController alloc]init];
        vc.detailModel=model;
        return vc;
        //[self.navigationController pushViewController:vc animated:YES];
        
    }
    else if (model.bigProductType.integerValue==5||model.bigProductType.integerValue==6||model.bigProductType.integerValue==7)
    {
        //5：存钱罐；6：新手专享；7：存钱罐365天升级版
        SavePotViewController *vc=[[SavePotViewController alloc]init];
        vc.detailModel=model;
        return vc;
        //[self.navigationController pushViewController:vc animated:YES];
    }
    else if (model.bigProductType.integerValue==8||model.bigProductType.integerValue==9)
    {
        //8：保障金 9：黄金及时赚
        
        //立即申购
        ApplyBuyOrderViewController *vc=[[ApplyBuyOrderViewController alloc]init];
        vc.detailModel=model;
        return vc;
        //[self.navigationController pushViewController:vc animated:YES];
        
    }
    else
    {
        
        
        if (model.isGoldProduct.integerValue>0)
        {
            //黄金产品出了保价金和黄金及时赚都跳这个页面
            FlowCashViewController *vc=[[FlowCashViewController alloc]init];
            vc.detailModel=model;
            return vc;
            //[self.navigationController pushViewController:vc animated:YES];
            
        }
        else
        {
            
            SaveCashViewController *vc=[[SaveCashViewController alloc]init];
            vc.detailModel=model;
            return vc;
            //[self.navigationController pushViewController:vc animated:YES];
            
        }
        
    }
    
}
@end
