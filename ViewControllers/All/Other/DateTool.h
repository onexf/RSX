//
//  DateTool.h
//  GoldCatBank
//
//  Created by Sunny on 2017/9/14.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateTool : NSObject

#pragma mark--计算几天后的日期或几天前的日期
+(NSString *)computeWithDays:(NSInteger)daysNum;

#pragma mark--获取现在的时间 年月日 时分秒
+(NSString *)getNowTimeString;

#pragma mark--获取两个日期的时间间隔
+(NSInteger)getDaysNumberWithDateA:(NSString *)DateA dateB:(NSString *)DateB;
@end
