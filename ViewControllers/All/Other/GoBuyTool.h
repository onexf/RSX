//
//  GoBuyTool.h
//  GoldCatBank
//
//  Created by Sunny on 2017/10/31.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProductDetailModel.h"

@interface GoBuyTool : NSObject
+(UIViewController *)goBuyChoosePageWithDetailModel:(ProductDetailModel *)model;
@end
