//
//  DateTool.m
//  GoldCatBank
//
//  Created by Sunny on 2017/9/14.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "DateTool.h"

@implementation DateTool

+(NSString *)computeWithDays:(NSInteger)daysNum
{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *myDate = [NSDate date];
    
    NSDate *newDate = [myDate dateByAddingTimeInterval:60 * 60 * 24 * daysNum];
    
    return [dateFormatter stringFromDate:newDate];
    
}

+(NSString *)getNowTimeString{
   
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *nowDate = [NSDate date];

    return [dateFormatter stringFromDate:nowDate];

}

//默认dateA的格式是1898-12-30
+(NSInteger)getDaysNumberWithDateA:(NSString *)DateA dateB:(NSString *)DateB
{
    //实例化一个NSDateFormatter对象
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设定时间格式
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *oldDate = [dateFormatter dateFromString:DateA];
    NSDate *newDate=[dateFormatter dateFromString:DateB];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    unsigned int unitFlags = NSCalendarUnitDay;
    NSDateComponents *comps = [gregorian components:unitFlags fromDate:oldDate  toDate:newDate  options:0];
    return [comps day];

}
@end
