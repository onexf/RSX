//
//  NetworkSingleton+Shop.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/11/29.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "NetworkSingleton.h"
@class GoldMarketModel;

@interface NetworkSingleton (Shop)

/**
 获取金店列表

 @param dict 请求参数
 @param response 返回结果
 */
+ (void)shop_getAllListWithDict:(NSDictionary *)dict Response:(void(^)(NSArray <GoldMarketModel *>*list, NSString *errMsg))response;


/**
 获取城市列表

 @param response 返回
 */
+ (void)shop_getAllCityResponse:(void(^)(NSArray <NSString *>*list, NSString *errMsg))response;


@end
