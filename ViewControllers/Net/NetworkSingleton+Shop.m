//
//  NetworkSingleton+Shop.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/11/29.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "NetworkSingleton+Shop.h"
#import "GoldMarketModel.h"

#define UrlShopList           URLString(@"/api/shops/List")
#define UrlCityList           URLString(@"/api/shops/Cities")

@implementation NetworkSingleton (Shop)

+ (void)shop_getAllListWithDict:(NSDictionary *)dict Response:(void(^)(NSArray <GoldMarketModel *>*list, NSString *errMsg))response {
    [[NetworkSingleton sharedManager] postResultWithParameter:dict url:UrlShopList successBlock:^(id responseBody) {
        NSArray<GoldMarketModel *> *list = [GoldMarketModel mj_objectArrayWithKeyValuesArray:responseBody];
        response(list, nil);
    } failureBlock:^(NSString *error) {
        response(nil, error);
    }];
}

+ (void)shop_getAllCityResponse:(void(^)(NSArray <NSString *>*list, NSString *errMsg))response {
    [[NetworkSingleton sharedManager] postResultWithParameter:nil url:UrlCityList successBlock:^(id responseBody) {
        NSMutableArray *cities = [NSMutableArray new];
        for (NSDictionary *dict in responseBody) {
            [cities addObject:dict[@"city"]];
        }
        response(cities, nil);
    } failureBlock:^(NSString *error) {
        response(nil, error);
    }];
}
@end
