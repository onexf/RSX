//
//  MineGoldAssetsView.m
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/9/4.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "MineGoldAssetsView.h"
#import "AssetsCell.h"
#import "AssetsTitleCell.h"
#import "GoldAssetsModel.h"
#import "SellGoldViewController.h"
#import "UserAuthData.h"
#import "SetChargePasswordViewController.h"
#import "BindBankCarViewController.h"
@interface MineGoldAssetsView ()

/** 黄金 资产*/
@property(nonatomic, strong) AssetsTitleCell *goldAssetsTitleCell;
/** 黄金现值 */
@property(nonatomic, strong) AssetsCell *goldBalanceCell;
/** 历史盈亏 */
@property(nonatomic, strong) AssetsCell *profitCell;

/** 招财金 */
@property(nonatomic, strong) AssetsTitleCell *zhaocaiTitleCell;
/** 买入金价 */
@property(nonatomic, strong) AssetsCell *zhaocaiBuyPriceCell;
/** 浮动盈亏 */
@property(nonatomic, strong) AssetsCell *zhaocaiProfitCell;
/** 累计收益 */
@property(nonatomic, strong) AssetsCell *zhaocaiTotalIncomeCell;

/** 招财金 */
@property(nonatomic, strong) AssetsTitleCell *shengcaiTitleCell;
/** 买入金价 */
@property(nonatomic, strong) AssetsCell *shengcaBuyPriceCell;
/** 浮动盈亏 */
@property(nonatomic, strong) AssetsCell *shengcaProfitCell;
/** 累计收益 */
@property(nonatomic, strong) AssetsCell *shengcaTotalIncomeCell;
/** 卖出 */
@property(nonatomic, strong) UIButton *sellOutButton;

@end


@implementation MineGoldAssetsView

- (void)setGoldAssets:(GoldAssetsModel *)goldAssets {
    _goldAssets = goldAssets;
    self.goldAssetsTitleCell.subText = goldAssets.goldAssets;
    self.goldBalanceCell.subText = goldAssets.goldValue;
    self.profitCell.subText = goldAssets.historyProfitLoss;
    
    self.zhaocaiTitleCell.subText = goldAssets.recruitFinance;
    self.zhaocaiBuyPriceCell.subText = goldAssets.rAvePurchasePrice;
    self.zhaocaiProfitCell.subText = goldAssets.rFloatProfitLoss;
    self.zhaocaiTotalIncomeCell.subText = goldAssets.rTotalProfit;
    
    self.shengcaiTitleCell.subText = goldAssets.makingMoney;
    self.shengcaBuyPriceCell.subText = goldAssets.mAvePurchasePrice;
    self.shengcaProfitCell.subText = goldAssets.mFloatProfitLoss;
    self.shengcaTotalIncomeCell.subText = goldAssets.mTotalProfit;
}

- (void)sellOutButtonDidTap {
    [[UserAuthData shareInstance] refreshUserInfoResponse:^(UserAuthDataModel *authData, NSString *errMsg) {
        if ([authData.isAuthentication isEqualToString:@"1"] && authData.bankCard.length > 0) {
            if (authData.hasTradePwd.integerValue <= 0) {//设置密码
                SetChargePasswordViewController *setVC=[[SetChargePasswordViewController alloc] init];
                [self.jk_viewController.navigationController pushViewController:setVC animated:YES];
            } else {//更改密码
                SellGoldViewController *sellGoldViewController = [[SellGoldViewController alloc] init];
                [self.jk_viewController.navigationController pushViewController:sellGoldViewController animated:YES];
            }
        } else {//绑定银行卡
            BindBankCarViewController *bindBankCardVC=[[BindBankCarViewController alloc] init];
            [self.jk_viewController.navigationController pushViewController:bindBankCardVC animated:YES];
        }
    }];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.goldAssetsTitleCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(10);
        make.left.right.equalTo(self);
        make.height.equalTo(@(NavBarHeight));
    }];
    [self.goldBalanceCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goldAssetsTitleCell.mas_bottom);
        make.left.right.equalTo(self);
        make.height.equalTo(@(NORMAL_CELL_HEIGHT));
    }];
    [self.profitCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goldBalanceCell.mas_bottom);
        make.left.right.height.equalTo(self.goldBalanceCell);
    }];
    
    [self.zhaocaiTitleCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.profitCell.mas_bottom).offset(10);
        make.left.right.height.equalTo(self.goldAssetsTitleCell);
    }];
    [self.zhaocaiBuyPriceCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.zhaocaiTitleCell.mas_bottom);
        make.left.right.height.equalTo(self.goldBalanceCell);
    }];
    [self.zhaocaiProfitCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.zhaocaiBuyPriceCell.mas_bottom);
        make.left.right.height.equalTo(self.zhaocaiBuyPriceCell);
    }];
    [self.zhaocaiTotalIncomeCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.zhaocaiProfitCell.mas_bottom);
        make.left.right.height.equalTo(self.zhaocaiProfitCell);
    }];
    
    [self.shengcaiTitleCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.zhaocaiTotalIncomeCell.mas_bottom).offset(10);
        make.left.right.height.equalTo(self.zhaocaiTitleCell);
    }];
    [self.shengcaBuyPriceCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.shengcaiTitleCell.mas_bottom);
        make.left.right.height.equalTo(self.zhaocaiTotalIncomeCell);
    }];
    [self.shengcaProfitCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.shengcaBuyPriceCell.mas_bottom);
        make.left.right.height.equalTo(self.shengcaBuyPriceCell);
    }];
    [self.shengcaTotalIncomeCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.shengcaProfitCell.mas_bottom);
        make.left.right.height.equalTo(self.shengcaProfitCell);
    }];
    [self.sellOutButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.shengcaTotalIncomeCell.mas_bottom).offset(10);
        make.left.right.equalTo(self.shengcaTotalIncomeCell);
        make.height.equalTo(@(NORMAL_BUTTON_HEIGHT));
    }];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 283)]) {
        self.backgroundColor = COLOR_TBBACK;
        [self addSubview:self.goldAssetsTitleCell];
        [self addSubview:self.goldBalanceCell];
        [self addSubview:self.profitCell];
        
        [self addSubview:self.zhaocaiTitleCell];
        [self addSubview:self.zhaocaiBuyPriceCell];
        [self addSubview:self.zhaocaiProfitCell];
        [self addSubview:self.zhaocaiTotalIncomeCell];
        
        [self addSubview:self.shengcaiTitleCell];
        [self addSubview:self.shengcaBuyPriceCell];
        [self addSubview:self.shengcaProfitCell];
        [self addSubview:self.shengcaTotalIncomeCell];
        [self addSubview:self.sellOutButton];
    }
    return self;
}
/**
 懒加载
 
 @return 卖出
 */
- (UIButton *)sellOutButton {
    if (!_sellOutButton) {
        _sellOutButton = [[UIButton alloc] init];
        [_sellOutButton setTitle:@"卖出" forState:UIControlStateNormal];
        [_sellOutButton setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
        _sellOutButton.backgroundColor = COLOR_MAIN;
        [_sellOutButton addTarget:self action:@selector(sellOutButtonDidTap) forControlEvents:UIControlEventTouchUpInside];
    }
    return _sellOutButton;
}

/**
 懒加载
 
 @return totalincome
 */
- (AssetsCell *)shengcaTotalIncomeCell {
    if (!_shengcaTotalIncomeCell) {
        _shengcaTotalIncomeCell = [[AssetsCell alloc] init];
        _shengcaTotalIncomeCell.cellType = AssetsCellTypeIncomeShengCai;
    }
    return _shengcaTotalIncomeCell;
}

/**
 懒加载
 
 @return zhaocaiProfitCell
 */
- (AssetsCell *)shengcaProfitCell {
    if (!_shengcaProfitCell) {
        _shengcaProfitCell = [[AssetsCell alloc] init];
        _shengcaProfitCell.cellType = AssetsCellTypeProfitShengCai;
    }
    return _shengcaProfitCell;
}

/**
 懒加载
 
 @return zhaocaibuyprice
 */
- (AssetsCell *)shengcaBuyPriceCell {
    if (!_shengcaBuyPriceCell) {
        _shengcaBuyPriceCell = [[AssetsCell alloc] init];
        _shengcaBuyPriceCell.cellType = AssetsCellTypeBuyPriceShengCai;
    }
    return _shengcaBuyPriceCell;
}

/**
 懒加载
 
 @return zhaocaititle
 */
- (AssetsTitleCell *)shengcaiTitleCell {
    if (!_shengcaiTitleCell) {
        _shengcaiTitleCell = [[AssetsTitleCell alloc] init];
        _shengcaiTitleCell.cellType = AssetsTitleCellTypeShengCaiJin;
    }
    return _shengcaiTitleCell;
}

/**
 懒加载
 
 @return totalincome
 */
- (AssetsCell *)zhaocaiTotalIncomeCell {
    if (!_zhaocaiTotalIncomeCell) {
        _zhaocaiTotalIncomeCell = [[AssetsCell alloc] init];
        _zhaocaiTotalIncomeCell.cellType = AssetsCellTypeIncomeZhaoCai;
    }
    return _zhaocaiTotalIncomeCell;
}

/**
 懒加载
 
 @return zhaocaiProfitCell
 */
- (AssetsCell *)zhaocaiProfitCell {
    if (!_zhaocaiProfitCell) {
        _zhaocaiProfitCell = [[AssetsCell alloc] init];
        _zhaocaiProfitCell.cellType = AssetsCellTypeProfitZhaoCai;
    }
    return _zhaocaiProfitCell;
}

/**
 懒加载
 
 @return zhaocaibuyprice
 */
- (AssetsCell *)zhaocaiBuyPriceCell {
    if (!_zhaocaiBuyPriceCell) {
        _zhaocaiBuyPriceCell = [[AssetsCell alloc] init];
        _zhaocaiBuyPriceCell.cellType = AssetsCellTypeBuyPriceZhaoCai;
    }
    return _zhaocaiBuyPriceCell;
}

/**
 懒加载
 
 @return zhaocaititle
 */
- (AssetsTitleCell *)zhaocaiTitleCell {
    if (!_zhaocaiTitleCell) {
        _zhaocaiTitleCell = [[AssetsTitleCell alloc] init];
        _zhaocaiTitleCell.cellType = AssetsTitleCellTypeZhaoCaiJin;
    }
    return _zhaocaiTitleCell;
}

/**
 懒加载
 
 @return profit
 */
- (AssetsCell *)profitCell {
    if (!_profitCell) {
        _profitCell = [[AssetsCell alloc] init];
        _profitCell.cellType = AssetsCellTypeProfit;
    }
    return _profitCell;
}

/**
 懒加载
 
 @return balance
 */
- (AssetsCell *)goldBalanceCell {
    if (!_goldBalanceCell) {
        _goldBalanceCell = [[AssetsCell alloc] init];
        _goldBalanceCell.cellType = AssetsCellTypeGoldAmount;
    }
    return _goldBalanceCell;
}

/**
 懒加载
 
 @return 黄金资产
 */
- (AssetsTitleCell *)goldAssetsTitleCell {
    if (!_goldAssetsTitleCell) {
        _goldAssetsTitleCell = [[AssetsTitleCell alloc] init];
        _goldAssetsTitleCell.cellType = AssetsTitleCellTypeGold;
    }
    return _goldAssetsTitleCell;
}

@end
