//
//  AssetsCell.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/5.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 *  资产cell类型
 */
typedef NS_ENUM(NSInteger, AssetsCellType) {
    AssetsCellTypeGoldAmount = 0,//黄金现值
    AssetsCellTypeProfit,//历史盈亏
    
    AssetsCellTypeBuyPriceZhaoCai,//买入金价 招财金
    AssetsCellTypeProfitZhaoCai,//浮动盈亏 招财金
    AssetsCellTypeIncomeZhaoCai,//累计收一次 招财金
    
    AssetsCellTypeBuyPriceShengCai,//买入金价 生财金
    AssetsCellTypeProfitShengCai,//浮动盈亏 生财金
    AssetsCellTypeIncomeShengCai,//累计收一次 生财金
    
    AssetsCellTypeIncomeLingQian,//累计收益 零钱罐
    AssetsCellTypeIncomeCunQian//累计收益 存钱罐
};


@interface AssetsCell : UIView

/** 类型 */
@property(nonatomic ,assign) AssetsCellType cellType;
/** subText */
@property(nonatomic, copy) NSString *subText;

@end
