//
//  SYPasswordView.m
//  PasswordDemo
//
//  Created by aDu on 2017/2/6.
//  Copyright © 2017年 DuKaiShun. All rights reserved.
//

#import "SYPasswordView.h"
#import "UIView+Extension.h"
#define kDotSize CGSizeMake (10, 10) //密码点的大小
#define kDotCount 6  //密码个数
#define K_Field_Height self.frame.size.height  //每一个输入框的高度等于当前view的高度
@interface SYPasswordView ()

@property (nonatomic, strong) NSMutableArray *dotArray; //用于存放黑色的点点

@end

@implementation SYPasswordView

- (NSString *)passWord {
    return _textField.text;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.layer.borderColor = COLOR_MAIN_BLUE.CGColor;
        self.layer.borderWidth = 1.0f;
        [self initPwdTextField];
    }
    return self;
}

- (void)initPwdTextField
{
    //每个密码输入框的宽度
    CGFloat width = self.frame.size.width / kDotCount;
    
    //生成分割线
    for (int i = 0; i < kDotCount - 1; i++) {
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.frame) + (i + 1) * width, 0, 1, K_Field_Height)];
        lineView.backgroundColor = COLOR_LINE;
        [self addSubview:lineView];
    }
    
    self.dotArray = [[NSMutableArray alloc] init];
    //生成中间的点
    for (int i = 0; i < kDotCount; i++) {
//        UIView *dotView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.textField.frame) + (width - kDotCount) / 2 + i * width, CGRectGetMinY(self.textField.frame) + (K_Field_Height - kDotSize.height) / 2, kDotSize.width, kDotSize.height)];
////        dotView.backgroundColor = [UIColor yellowColor];
//        
//        dotView.layer.cornerRadius = kDotSize.width / 2.0f;
//        dotView.clipsToBounds = YES;
//        dotView.hidden = YES; //先隐藏
//        [self addSubview:dotView];
        
        
        UIImageView *starImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.frame) + (width - kDotCount) / 2 + i * width, CGRectGetMinY(self.frame) + (K_Field_Height - kDotSize.height) / 2, kDotSize.width, kDotSize.height)];;
        starImageView.image = [UIImage imageNamed:@"icon_pwd"];
        
        starImageView.hidden = YES; //先隐藏
        [self addSubview:starImageView];
        //把创建的黑色点加入到数组中
        [self.dotArray addObject:starImageView];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
//    NSLog(@"变化%@", string);
    if([string isEqualToString:@"\n"]) {
        //按回车关闭键盘
        [textField resignFirstResponder];
        return NO;
    } else if(string.length == 0) {
        //判断是不是删除键
        return YES;
    }
    else if(textField.text.length >= kDotCount) {
        //输入的字符个数大于6，则无法继续输入，返回NO表示禁止输入
        if (self.delegate && [self.delegate respondsToSelector:@selector(passwordView:inputPassword:)]) {
            [self.delegate passwordView:self inputPassword:textField.text];
        }
//        NSLog(@"输入的字符个数大于6，忽略输入");
        return NO;
    } else {
        return YES;
    }
}

/**
 *  清除密码
 */
- (void)clearUpPassword
{
    self.textField.text = @"";
    [self textFieldDidChange:self.textField];
}

/**
 *  重置显示的点
 */
- (void)textFieldDidChange:(UITextField *)textField
{
//    NSLog(@"当前密码：%@", textField.text);
    if (self.delegate && [self.delegate respondsToSelector:@selector(passwordView:inputPassword:)]) {
        [self.delegate passwordView:self inputPassword:textField.text];
    }
    for (UIView *dotView in self.dotArray) {
        dotView.hidden = YES;
    }
    for (int i = 0; i < textField.text.length; i++) {
        ((UIView *)[self.dotArray objectAtIndex:i]).hidden = NO;
    }
    if (textField.text.length == kDotCount) {
        NSLog(@"输入完毕");
//        self.layer.borderColor = COLOR_MAIN_BLUE.CGColor;
        if (self.delegate && [self.delegate respondsToSelector:@selector(inputDone:)]) {
            [self.delegate inputDone:self];
        }
    }
}

#pragma mark - init

- (UITextField *)textField
{
    if (!_textField) {
        _textField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
        _textField.backgroundColor = [UIColor whiteColor];
        //输入的文字颜色为白色
        _textField.textColor = [UIColor whiteColor];
        //输入框光标的颜色为白色
        _textField.tintColor = [UIColor whiteColor];
        _textField.delegate = self;
        _textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _textField.keyboardType = UIKeyboardTypeNumberPad;
        _textField.layer.borderColor = [[UIColor grayColor] CGColor];
        _textField.layer.borderWidth = 1;
        [_textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        [self addSubview:_textField];
    }
    return _textField;
}

@end
