//
//  MIneBottomView.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/1.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//


#import <UIKit/UIKit.h>

@protocol MineOverageViewDelegate <NSObject>

- (void)didSelectRowAtIndex:(NSInteger)index;

@end

@interface MIneBottomView : UIView

/** 代理 */
@property(nonatomic, weak) id <MineOverageViewDelegate>viewDelegate;
/** 优惠券 */
@property(nonatomic, copy) NSString *couponCount;


@end
