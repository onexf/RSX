//
//  BuyOtherTopView.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/10/30.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "BuyOtherTopView.h"
#import "RecordDetailModel.h"

@interface BuyOtherTopView ()

/** 状态 */
@property(nonatomic, strong) UILabel *buyStatusLabel;
/** typeLabel */
@property(nonatomic, strong) UILabel *buyTypeLabel;
/** 数量 */
@property(nonatomic, strong) UILabel *buyCountLabel;
/** 分割线 */
@property(nonatomic, strong) UIView *buySepLine;
/** 订单总额 */
@property(nonatomic, strong) UILabel *buyTotal;
/** 总额 */
@property(nonatomic, strong) UILabel *buyTotalValue;

@end


@implementation BuyOtherTopView

- (void)setModel:(RecordDetailModel *)model {
    _model = model;
    self.buyStatusLabel.text = model.orderStatusString;
    self.buyTypeLabel.text = model.logTypeString;
    self.buyCountLabel.text = model.productName;
    self.buyTotalValue.text = [NSString stringWithFormat:@"%@元", model.orderAmount];
    if (model.logType == 3) {//赎回
        self.buyTotal.text = @"赎回总额";
    }
//    self.buyStatusLabel.textColor = model.statusColor;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = COLOR_WHITE;
        [self addSubview:self.buyStatusLabel];
        [self addSubview:self.buyTypeLabel];
        [self addSubview:self.buyCountLabel];
        [self addSubview:self.buySepLine];
        [self addSubview:self.buyTotal];
        [self addSubview:self.buyTotalValue];
    }
    return self;
}
- (void)layoutSubviews {
    [super layoutSubviews];
    [self.buyStatusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(10);
        make.centerX.equalTo(self);
    }];
    [self.buyTypeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.buyStatusLabel.mas_bottom).offset(10);
        make.left.equalTo(self).offset(LEFT_RIGHT_MARGIN);
    }];
    [self.buyCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.buyTypeLabel);
        make.right.equalTo(self).offset(-LEFT_RIGHT_MARGIN);
    }];
    [self.buySepLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.buyCountLabel.mas_bottom).offset(10);
        make.left.equalTo(self.buyTypeLabel);
        make.right.equalTo(self.buyCountLabel);
        make.height.equalTo(@1);
    }];
    [self.buyTotal mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.buySepLine.mas_bottom).offset(10);
        make.left.equalTo(self.buySepLine);
    }];
    [self.buyTotalValue mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.buyTotal);
        make.right.equalTo(self.buySepLine);
        make.bottom.equalTo(self).offset(-10);
    }];
}
/**
 懒加载
 
 @return buyTotalValue
 */
- (UILabel *)buyTotalValue {
    if (!_buyTotalValue) {
        _buyTotalValue = [[UILabel alloc] init];
        _buyTotalValue.font = FONT_TEXT;
        _buyTotalValue.textColor = COLOR_TEXT;
    }
    return _buyTotalValue;
}
/**
 懒加载
 
 @return buyTotal
 */
- (UILabel *)buyTotal {
    if (!_buyTotal) {
        _buyTotal = [[UILabel alloc] init];
        _buyTotal.font = FONT_TEXT;
        _buyTotal.textColor = COLOR_TEXT;
        _buyTotal.text = @"订单总额";
    }
    return _buyTotal;
}
/**
 懒加载
 
 @return buySepLine
 */
- (UIView *)buySepLine {
    if (!_buySepLine) {
        _buySepLine = [[UIView alloc] init];
        _buySepLine.backgroundColor = COLOR_LINE;
    }
    return _buySepLine;
}
/**
 懒加载
 
 @return buyCountLabel
 */
- (UILabel *)buyCountLabel {
    if (!_buyCountLabel) {
        _buyCountLabel = [[UILabel alloc] init];
        _buyCountLabel.textColor = COLOR_TEXT;
        _buyCountLabel.font = FONT_TEXT;
    }
    return _buyCountLabel;
}
/**
 懒加载
 
 @return buyTypeLabel
 */
- (UILabel *)buyTypeLabel {
    if (!_buyTypeLabel) {
        _buyTypeLabel = [[UILabel alloc] init];
        _buyTypeLabel.textColor = COLOR_TEXT;
        _buyTypeLabel.font = FONT_TEXT;
    }
    return _buyTypeLabel;
}
/**
 懒加载
 
 @return buyStatusLabel
 */
- (UILabel *)buyStatusLabel {
    if (!_buyStatusLabel) {
        _buyStatusLabel = [[UILabel alloc] init];
        _buyStatusLabel.textColor = COLOR_Red;
        _buyStatusLabel.font = FONT_TITLE;
    }
    return _buyStatusLabel;
}

@end
