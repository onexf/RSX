//
//  TradeBounceView.h
//  GoldCatBank
//
//  Created by Sunny on 2017/9/5.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum{
    
    TypeAll =-1,
    TypeInvest =0,
    TypeBuyGold =1,
    TypeSaleGold =2,
    TypeRedeem =3,
    TypeCharge =4,
    TypeWithdraw =5,
    TypeExpire =6,
    TypeOther =7
    
}OrderType;

@protocol TradeDelegate <NSObject>

-(void)selectItemWithIndex:(NSInteger)index;

@end

@interface TradeBounceView : UIView
@property(nonatomic,strong)NSArray *titleArray;
@property(nonatomic,assign)CGFloat showTop;
@property(nonatomic,weak)id <TradeDelegate>delegate;

-(id)initWithFrame:(CGRect)frame withSelect:(OrderType)type;

-(void)show;

-(void)hidden;

@end
