//
//  RechargeAndWithdrawView.m
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/9/5.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "RechargeAndWithdrawView.h"
#import "HYBImageCliped.h"
#import "WithdrawalsViewController.h"
#import "RechargeViewController.h"
#import "MineHomePageModel.h"
#import "IncomeViewController.h"
#import "BindBankCarViewController.h"
#import "UserAuthData.h"
#import "UIView+Extension.h"
#import "AllAssetsViewController.h"

@interface RechargeAndWithdrawView ()
/** top */
@property(nonatomic, strong) UIView *topView;
/** 充值 */
@property(nonatomic, strong) UIButton *rechargeButton;
/** 提现 */
@property(nonatomic, strong) UIButton *withDrawButton;
/** bottom */
@property(nonatomic, strong) UIView *bottomView;
/** line */
@property(nonatomic, strong) UIView *hLine;
/** line */
@property(nonatomic, strong) UIView *vLine;
/** 黄金 */
@property(nonatomic, strong) UILabel *goldLabel;
/** 黄金资产 */
@property(nonatomic, strong) UILabel *goldValue;
/** 其他 */
@property(nonatomic, strong) UILabel *otherLabel;
/** 其他资产 */
@property(nonatomic, strong) UILabel *otherValue;
/** 昨日收益 */
@property(nonatomic, strong) UIView *yesterdayView;
/** zrsy */
@property(nonatomic, strong) UILabel *yesterdayTitleLabel;
/** zrsy */
@property(nonatomic, strong) UILabel *yesterdayAmountLabel;
/** 累计收益 */
@property(nonatomic, strong) UIView *totalView;
/** zrsy */
@property(nonatomic, strong) UILabel *totalTitleLabel;
/** zrsy */
@property(nonatomic, strong) UILabel *totalAmountLabel;

@end

@implementation RechargeAndWithdrawView

- (void)setEarnData:(MineHomePageModel *)earnData {
    _earnData = earnData;
    self.goldValue.text = [earnData.goldAssets jk_containsaString:@"*"] ? earnData.goldAssets : earnData.goldAssets;
    self.otherValue.text = [earnData.otherAssets jk_containsaString:@"*"] ? earnData.otherAssets : earnData.otherAssets;
    
    self.yesterdayAmountLabel.text = earnData.ydTotalAmount;
    self.totalAmountLabel.text = earnData.earnTotalAmount;
}
- (void)rechargeButtonDidTap:(UIButton *)button {
    [WSProgressHUD showWithMaskType:WSProgressHUDMaskTypeBlack];
    __weak typeof(self) weakSelf = self;
    [[UserAuthData shareInstance] refreshUserInfoResponse:^(UserAuthDataModel *userAuthData, NSString *errMsg) {
        [WSProgressHUD dismiss];
        if ([userAuthData.isAuthentication boolValue]>0) {
            RechargeViewController *vc = [[RechargeViewController alloc] init];
            [weakSelf.jk_viewController.navigationController pushViewController:vc animated:YES];
        } else {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"您尚未绑定银行卡\n是否绑定" message:nil preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *actionQ = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                BindBankCarViewController *vc=[[BindBankCarViewController alloc]init];
                [weakSelf.jk_viewController.navigationController pushViewController:vc animated:YES];
            }];
            UIAlertAction *actionC = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
            [alert addAction:actionC];
            [alert addAction:actionQ];
            [weakSelf.jk_viewController.navigationController presentViewController:alert animated:YES completion:nil];
        }
    }];
}

- (void)withDrawButtonClick:(UIButton *)btn {
    [WSProgressHUD showWithMaskType:WSProgressHUDMaskTypeBlack];
    __weak typeof(self) weakSelf = self;
    [[UserAuthData shareInstance] refreshUserInfoResponse:^(UserAuthDataModel *userAuthData, NSString *errMsg) {
        [WSProgressHUD dismiss];
        if ([userAuthData.isAuthentication boolValue]>0) {
            WithdrawalsViewController *vc=[[WithdrawalsViewController alloc]init];
            [weakSelf.jk_viewController.navigationController pushViewController:vc animated:YES];
        } else {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"您尚未绑定银行卡\n是否绑定" message:nil preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *actionQ = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                BindBankCarViewController *vc=[[BindBankCarViewController alloc]init];
                [weakSelf.jk_viewController.navigationController pushViewController:vc animated:YES];
            }];
            UIAlertAction *actionC = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
            [alert addAction:actionC];
            [alert addAction:actionQ];
            [weakSelf.jk_viewController.navigationController presentViewController:alert animated:YES completion:nil];
        }
    }];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self);
        make.bottom.equalTo(self.rechargeButton.mas_bottom).offset(21);
    }];
    [self.rechargeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.topView).offset(21);
        make.left.equalTo(self.topView).offset(40);
        make.height.equalTo(@40);
    }];
    [self.withDrawButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.width.equalTo(self.rechargeButton);
        make.left.equalTo(self.rechargeButton.mas_right).offset(29);
        make.right.equalTo(self.topView).offset(-40);
    }];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.topView.mas_bottom).offset(10);
        make.left.right.equalTo(self);
        make.height.equalTo(@167);
    }];
    [self.hLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.left.right.equalTo(self.bottomView);
        make.height.equalTo(@1);
    }];
    [self.vLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.top.bottom.equalTo(self.bottomView);
        make.width.equalTo(@1);
    }];
    [self.goldLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bottomView);
        make.left.equalTo(self.bottomView);
        make.right.equalTo(self.vLine.mas_left);
        make.height.equalTo(@60);
    }];
    [self.goldValue mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.goldLabel);
        make.height.equalTo(@47);
        make.bottom.equalTo(self.hLine.mas_top);
    }];
    [self.otherLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.height.equalTo(self.goldLabel);
        make.left.equalTo(self.vLine.mas_right);
        make.right.equalTo(self.bottomView);
    }];
    [self.otherValue mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.height.equalTo(self.goldValue);
        make.left.right.equalTo(self.otherLabel);
    }];
    [self.yesterdayView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.hLine.mas_bottom);
        make.left.bottom.equalTo(self.bottomView);
        make.right.equalTo(self.vLine.mas_left);
    }];
    [self.yesterdayTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.yesterdayView);
        make.left.equalTo(self.yesterdayView);
        make.right.equalTo(self.vLine.mas_left);
        make.height.equalTo(@60);
    }];
    [self.yesterdayAmountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.yesterdayTitleLabel);
        make.height.equalTo(@47);
        make.bottom.equalTo(self.yesterdayView);
    }];
    [self.totalView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.yesterdayView);
        make.left.equalTo(self.vLine.mas_right);
        make.right.equalTo(self.bottomView);
    }];
    [self.totalTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.totalView);
        make.left.equalTo(self.vLine.mas_right);
        make.right.equalTo(self.totalView);
        make.height.equalTo(@60);
    }];
    [self.totalAmountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.totalTitleLabel);
        make.height.equalTo(@47);
        make.bottom.equalTo(self.totalView);
    }];
}
/**
 懒加载
 
 @return 收益多少
 */
- (UILabel *)totalAmountLabel {
    if (!_totalAmountLabel) {
        _totalAmountLabel = [[UILabel alloc] init];
        _totalAmountLabel.textColor = COLOR_MAIN;
        _totalAmountLabel.font = FONT_TITLE;
        _totalAmountLabel.text = @"00.00";
        _totalAmountLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _totalAmountLabel;
}
/**
 懒加载
 
 @return zrsy
 */
- (UILabel *)totalTitleLabel {
    if (!_totalTitleLabel) {
        _totalTitleLabel = [[UILabel alloc] init];
        _totalTitleLabel.text = @"累计收益(元)";
        _totalTitleLabel.font = FONT_TEXT;
        _totalTitleLabel.textColor = COLOR_TEXT;
        _totalTitleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _totalTitleLabel;
}
/**
 懒加载
 
 @return 昨日收益
 */
- (UIView *)totalView {
    if (!_totalView) {
        _totalView = [[UIView alloc] init];
        [_totalView addSubview:self.totalTitleLabel];
        [_totalView addSubview:self.totalAmountLabel];
        _totalView.backgroundColor = COLOR_WHITE;
        __weak typeof(self) weakSelf = self;
        [_totalView jk_addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
            IncomeViewController *incomeVC = [[IncomeViewController alloc] init];
            incomeVC.typeString = @"1";
            [weakSelf.jk_viewController.navigationController pushViewController:incomeVC animated:YES];
        }];
    }
    return _totalView;
}
/**
 懒加载
 
 @return 收益多少
 */
- (UILabel *)yesterdayAmountLabel {
    if (!_yesterdayAmountLabel) {
        _yesterdayAmountLabel = [[UILabel alloc] init];
        _yesterdayAmountLabel.textColor = COLOR_MAIN;
        _yesterdayAmountLabel.font = FONT_TITLE;
        _yesterdayAmountLabel.text = @"00.00";
        _yesterdayAmountLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _yesterdayAmountLabel;
}
/**
 懒加载
 
 @return zrsy
 */
- (UILabel *)yesterdayTitleLabel {
    if (!_yesterdayTitleLabel) {
        _yesterdayTitleLabel = [[UILabel alloc] init];
        _yesterdayTitleLabel.text = @"昨日收益(元)";
        _yesterdayTitleLabel.font = FONT_TEXT;
        _yesterdayTitleLabel.textColor = COLOR_TEXT;
        _yesterdayTitleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _yesterdayTitleLabel;
}
/**
 懒加载
 
 @return 昨日收益
 */
- (UIView *)yesterdayView {
    if (!_yesterdayView) {
        _yesterdayView = [[UIView alloc] init];
        [_yesterdayView addSubview:self.yesterdayTitleLabel];
        [_yesterdayView addSubview:self.yesterdayAmountLabel];
        _yesterdayView.backgroundColor = COLOR_WHITE;
        __weak typeof(self) weakSelf = self;
        [_yesterdayView jk_addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
            IncomeViewController *incomeVC = [[IncomeViewController alloc] init];
            incomeVC.typeString = @"0";
            [weakSelf.jk_viewController.navigationController pushViewController:incomeVC animated:YES];
        }];
    }
    return _yesterdayView;
}
/**
 懒加载
 
 @return 提现
 */
- (UIButton *)withDrawButton {
    if (!_withDrawButton) {
        _withDrawButton = [[UIButton alloc] init];
        [_withDrawButton setTitle:@"提现" forState:UIControlStateNormal];
        [_withDrawButton setTitleColor:COLOR_MAIN forState:UIControlStateNormal];
        _withDrawButton.titleLabel.font = FONT_BUTTON_TEXT;
        _withDrawButton.backgroundColor = COLOR_WHITE;
        _withDrawButton.tag=100;
        [_withDrawButton addTarget:self action:@selector(withDrawButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        _withDrawButton.layer.cornerRadius = 20;
        _withDrawButton.layer.borderWidth = 1;
        _withDrawButton.layer.borderColor = COLOR_MAIN.CGColor;

    }
    return _withDrawButton;
}

/**
 懒加载
 
 @return 充值
 */
- (UIButton *)rechargeButton {
    if (!_rechargeButton) {
        _rechargeButton = [[UIButton alloc] init];
        [_rechargeButton setTitle:@"充值" forState:UIControlStateNormal];
        [_rechargeButton setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
        _rechargeButton.titleLabel.font = FONT_BUTTON_TEXT;
        _rechargeButton.backgroundColor = COLOR_MAIN;
        [_rechargeButton addTarget:self action:@selector(rechargeButtonDidTap:) forControlEvents:UIControlEventTouchUpInside];
        _rechargeButton.layer.cornerRadius = 20;
    }
    return _rechargeButton;
}
/**
 懒加载
 
 @return top
 */
- (UIView *)topView {
    if (!_topView) {
        _topView = [[UIView alloc] init];
        _topView.backgroundColor = COLOR_WHITE;
        [_topView addSubview:self.rechargeButton];
        [_topView addSubview:self.withDrawButton];
    }
    return _topView;
}
/**
 懒加载
 
 @return line
 */
- (UIView *)vLine {
    if (!_vLine) {
        _vLine = [[UIView alloc] init];
        _vLine.backgroundColor = COLOR_LINE;
    }
    return _vLine;
}
/**
 懒加载
 
 @return line
 */
- (UIView *)hLine {
    if (!_hLine) {
        _hLine = [[UIView alloc] init];
        _hLine.backgroundColor = COLOR_LINE;
    }
    return _hLine;
}
- (void)otherAssetsAction {
    AllAssetsViewController *allAssetsViewController = [[AllAssetsViewController alloc] init];
    allAssetsViewController.assetsType = @"2";
    [self.jk_viewController.navigationController pushViewController:allAssetsViewController animated:YES];
}
/**
 懒加载
 
 @return 其他资产
 */
- (UILabel *)otherValue {
    if (!_otherValue) {
        _otherValue = [[UILabel alloc] init];
        _otherValue.textColor = UIColorFromHex(0x007eff);
        _otherValue.font = FONT_TITLE;
        _otherValue.textAlignment = NSTextAlignmentCenter;
        [_otherValue addTapAction:@selector(otherAssetsAction) target:self];
    }
    return _otherValue;
}
/**
 懒加载
 
 @return huangjin
 */
- (UILabel *)otherLabel {
    if (!_otherLabel) {
        _otherLabel = [[UILabel alloc] init];
        _otherLabel.text = @"其他资产(元)";
        _otherLabel.font = FONT_TEXT;
        _otherLabel.textColor = COLOR_TEXT;
        _otherLabel.textAlignment = NSTextAlignmentCenter;
        [_otherLabel addTapAction:@selector(otherAssetsAction) target:self];
    }
    return _otherLabel;
}
- (void)goldAssetsAction {
    AllAssetsViewController *allAssetsViewController = [[AllAssetsViewController alloc] init];
    allAssetsViewController.assetsType = @"1";
    [self.jk_viewController.navigationController pushViewController:allAssetsViewController animated:YES];

}
/**
 懒加载
 
 @return 黄金资产
 */
- (UILabel *)goldValue {
    if (!_goldValue) {
        _goldValue = [[UILabel alloc] init];
        _goldValue.textColor = UIColorFromHex(0XC574);
        _goldValue.font = FONT_TITLE;
        _goldValue.textAlignment = NSTextAlignmentCenter;
        [_goldValue addTapAction:@selector(goldAssetsAction) target:self];
    }
    return _goldValue;
}
/**
 懒加载
 
 @return huangjin
 */
- (UILabel *)goldLabel {
    if (!_goldLabel) {
        _goldLabel = [[UILabel alloc] init];
        _goldLabel.text = @"黄金资产(克)";
        _goldLabel.font = FONT_TEXT;
        _goldLabel.textColor = COLOR_TEXT;
        _goldLabel.textAlignment = NSTextAlignmentCenter;
        [_goldLabel addTapAction:@selector(goldAssetsAction) target:self];
    }
    return _goldLabel;
}
/**
 懒加载
 
 @return bottom
 */
- (UIView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[UIView alloc] init];
        _bottomView.backgroundColor = COLOR_WHITE;
        [_bottomView addSubview:self.hLine];
        [_bottomView addSubview:self.vLine];
        [_bottomView addSubview:self.goldLabel];
        [_bottomView addSubview:self.goldValue];
        [_bottomView addSubview:self.otherLabel];
        [_bottomView addSubview:self.otherValue];
        [_bottomView addSubview:self.yesterdayView];
        [_bottomView addSubview:self.totalView];
    }
    return _bottomView;
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 125)]) {
        self.backgroundColor = COLOR_TBBACK;
        [self addSubview:self.topView];
        [self addSubview:self.bottomView];
    }
    return self;
}

@end
