//
//  RecordListCell.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/10/26.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "RecordListCell.h"
#import "GlodTradeModel.h"
@interface RecordListCell ()
/** 交易时间 */
@property(nonatomic, strong) UILabel *timeLabel;
/** 交易状态 */
@property(nonatomic, strong) UILabel *statusLabel;
/** 分割线 */
@property(nonatomic, strong) UIView *line;
/** 交易类型 */
@property(nonatomic, strong) UILabel *typeLabel;
/** 交易数量 */
@property(nonatomic, strong) UILabel *countLabel;
/** 箭头 */
@property(nonatomic, strong) UIImageView *arrowImageView;
/** 所交易产品名 */
@property(nonatomic, strong) UILabel *nameLabel;
/** 交易金额 */
@property(nonatomic, strong) UILabel *amountLabel;

@end

@implementation RecordListCell


- (void)setModel:(GlodTradeModel *)model {
    _model = model;
    self.timeLabel.text = model.createTime;
    self.statusLabel.text = model.statusString;
    self.typeLabel.text = model.logTypeString;
    self.countLabel.text = model.buyCountString;
    self.nameLabel.text = model.logTitle;
    self.amountLabel.text = model.goldAmountString;
    self.statusLabel.textColor = model.statusColor;
}

- (void)setFrame:(CGRect)frame {
    frame.size.height = 107;
    frame.origin.y += 0;
    frame.size.height -= 10;
    [super setFrame:frame];
}

+ (instancetype)cellWithTableview:(UITableView *)tableView
{
    static NSString *identifier = @"RecordListCell";
    RecordListCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[RecordListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = COLOR_WHITE;
        [self.contentView addSubview:self.timeLabel];
        [self.contentView addSubview:self.statusLabel];
        [self.contentView addSubview:self.line];
        [self.contentView addSubview:self.typeLabel];
        [self.contentView addSubview:self.countLabel];
        [self.contentView addSubview:self.arrowImageView];
        [self.contentView addSubview:self.nameLabel];
        [self.contentView addSubview:self.amountLabel];
    }
    return self;
}
- (void)layoutSubviews {
    [super layoutSubviews];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(10);
        make.left.equalTo(self.contentView).offset(LEFT_RIGHT_MARGIN);
    }];
    [self.statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.timeLabel);
        make.right.equalTo(self.contentView).offset(-LEFT_RIGHT_MARGIN);
    }];
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.statusLabel.mas_bottom).offset(10);
        make.left.equalTo(self.timeLabel);
        make.right.equalTo(self.statusLabel);
        make.height.equalTo(@1);
    }];
    [self.typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.line.mas_bottom).offset(10);
        make.left.equalTo(self.timeLabel);
    }];
    [self.arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.typeLabel);
        make.right.equalTo(self.line);
    }];
    [self.countLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.typeLabel);
        make.right.equalTo(self.arrowImageView.mas_left).offset(-10);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.typeLabel.mas_bottom).offset(10);
        make.left.equalTo(self.line);
    }];
    [self.amountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.nameLabel);
        make.right.equalTo(self.line);
    }];
}
/**
 懒加载
 
 @return 交易金额
 */
- (UILabel *)amountLabel {
    if (!_amountLabel) {
        _amountLabel = [[UILabel alloc] init];
        _amountLabel.textColor = COLOR_SUBTEXT;
        _amountLabel.font = FONT_TEXT;
    }
    return _amountLabel;
}
/**
 懒加载
 
 @return 产品名
 */
- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.textColor = COLOR_SUBTEXT;
        _nameLabel.font = FONT_TEXT;
    }
    return _nameLabel;
}
/**
 懒加载
 
 @return 箭头
 */
- (UIImageView *)arrowImageView {
    if (!_arrowImageView) {
        _arrowImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_nextArrow"]];
    }
    return _arrowImageView;
}
/**
 懒加载
 
 @return 交易数量
 */
- (UILabel *)countLabel {
    if (!_countLabel) {
        _countLabel = [[UILabel alloc] init];
        _countLabel.textColor = COLOR_TEXT;
        _countLabel.font = FONT_TEXT;
    }
    return _countLabel;
}
/**
 懒加载
 
 @return 交易类型
 */
- (UILabel *)typeLabel {
    if (!_typeLabel) {
        _typeLabel = [[UILabel alloc] init];
        _typeLabel.textColor = COLOR_TEXT;
        _typeLabel.font = FONT_TEXT;
    }
    return _typeLabel;
}
/**
 懒加载
 
 @return 分割线
 */
- (UIView *)line {
    if (!_line) {
        _line = [[UIView alloc] init];
        _line.backgroundColor = COLOR_LINE;
    }
    return _line;
}
/**
 懒加载
 
 @return 状态
 */
- (UILabel *)statusLabel {
    if (!_statusLabel) {
        _statusLabel = [[UILabel alloc] init];
        _statusLabel.textColor = COLOR_Red;
        _statusLabel.font = FONT_TEXT;
    }
    return _statusLabel;
}
/**
 懒加载
 
 @return 时间
 */
- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.font = FONT_SUBTEXT;
        _timeLabel.textColor = COLOR_SUBTEXT;
    }
    return _timeLabel;
}
@end
