//
//  MineGoldAssetsView.h
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/9/4.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GoldAssetsModel;
@interface MineGoldAssetsView : UIView
/** 黄金资产 */
@property(nonatomic, strong) GoldAssetsModel *goldAssets;

@end
