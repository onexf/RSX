//
//  MineHeaderView.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/2.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MineHomePageModel;
@protocol PieChartViewDelegate <NSObject>

- (void)didTapPieChartViewCenter;
- (void)didTapHideViewWithStatus:(BOOL)isHide;
@end
@interface MineHeaderView : UIView

/** 资产数据 */
@property(nonatomic, strong) MineHomePageModel *userAssets;
/** 点击代理 */
@property(nonatomic, weak) id <PieChartViewDelegate> tapDelegate;

/** 是否在首页 */
@property(nonatomic, assign) BOOL isMineHomeChart;

/** assetsShow */
@property(nonatomic, assign) BOOL assetsIsShow;

@end
