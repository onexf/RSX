//
//  SYPasswordView.h
//  PasswordDemo
//
//  Created by aDu on 2017/2/6.
//  Copyright © 2017年 DuKaiShun. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SYPasswordView;
@protocol PassWordInputViewDelegate <NSObject>

- (void)passwordView:(SYPasswordView *)passwordView inputPassword:(NSString *)password;

/**
 输入完毕

 @param passwordView 完成的是哪个
 */
- (void)inputDone:(SYPasswordView *)passwordView;


@end

@interface SYPasswordView : UIView<UITextFieldDelegate>

@property (nonatomic, weak) id<PassWordInputViewDelegate> delegate;

@property (nonatomic, strong) UITextField *textField;

/**
 *  清除密码
 */
- (void)clearUpPassword;

/** 获取密码 */
- (NSString *)passWord;

@end
