//
//  BlankPageView.m
//  GoldCatBank
//
//  Created by Sunny on 2017/10/25.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "BlankPageView.h"

@implementation BlankPageView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame withSwitchView:(UIView *)switchView
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor =COLOR_TBBACK;
        _switchView=switchView;
    }
    return self;
    
}

-(void)setHidden:(BOOL)hidden
{
     [super setHidden:hidden];
    if (hidden==YES)
    {
        self.switchView.hidden=NO;
    }
    else
    {
        self.switchView.hidden=YES;
    }

}
- (void)configWithType:(EaseBlankPageType)blankPageType reloadButtonBlock:(void (^)(id))block{
    
    self.alpha = 1.0;
    
    UIImage *image;
    NSString*tipStr;
    switch (blankPageType) {
        case BlankPageTypeNoData:
        {
            image = [UIImage imageNamed:@"Counpon_NodataIcon"];
            tipStr =@"您当前没有现金红包";
        }
            break;
        case BlankPageTypeNetError:
        {
            image = [UIImage imageNamed:@"Counpon_NodataIcon"];
            tipStr =@"您当前没有现金红包";
        }
            break;
            
        case BlankPageRedPacketNOData:
        {
            image = [UIImage imageNamed:@"Counpon_NodataIcon"];
            tipStr =@"您当前没有现金红包";
        }
            break;
        case BlankPageCouponNOData:
        {
            image = [UIImage imageNamed:@"Counpon_NodataIcon"];
            tipStr =@"您当前没有优惠券";
        }
            break;
        case BlankPageOrderListNOData:
        {
            image = [UIImage imageNamed:@"Counpon_NodataIcon"];
            tipStr =@"暂无当前状态下订单记录";
        }
            break;
       
            
        default://其它页面（这里没有提到的页面，都属于其它）
        {
           
        }
            break;
    }
    
    // 图片
   UIImageView  *monkeyView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
    [monkeyView setImage:image];
    monkeyView.center=self.center;
    [self addSubview:monkeyView];
    self.monkeyView=monkeyView;
    //文字
     UILabel*tipLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, _monkeyView.ct_bottom, self.ct_width, 30)];
    tipLabel.backgroundColor = [UIColor clearColor];
    tipLabel.numberOfLines = 0;
    tipLabel.font = [UIFont systemFontOfSize:15];
    tipLabel.textColor = COLOR_SUBTEXT;
    tipLabel.text = tipStr;
    tipLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:tipLabel];
    self.tipLabel=tipLabel;
    
    // 空白数据
    if (block) {
        _reloadButtonBlock  =[block copy];
        
        [_monkeyView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(reloadButtonClicked:)]];
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(reloadButtonClicked:)]];
        
    }
    
    
    
}
- (void)reloadButtonClicked:(id)sender{
    self.hidden = YES;
    [self removeFromSuperview];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (_reloadButtonBlock) {
            _reloadButtonBlock(sender);
        }
    });
}


@end
