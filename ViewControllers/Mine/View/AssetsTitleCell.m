//
//  AssetsTitleCell.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/5.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "AssetsTitleCell.h"
#import "UIView+Extension.h"
#import "BalanceTradeVC.h"
#import "SellOtherViewController.h"
#import "UserAuthData.h"
#import "SetChargePasswordViewController.h"
#import "BindBankCarViewController.h"
#import "RecordListViewController.h"
@interface AssetsTitleCell ()
/** name */
@property(nonatomic, strong) UILabel *nameLabel;
/** 数值 */
@property(nonatomic, strong) UILabel *accountLabel;
/** his */
@property(nonatomic, strong) UILabel *recordLabel;
/** 箭头 */
@property(nonatomic, strong) UIImageView *arrowImageView;
/** 赎回 */
@property(nonatomic, strong) UIButton *ransomButton;


@end

@implementation AssetsTitleCell

- (void)setSubText:(NSString *)subText {
    _subText = subText;
    self.accountLabel.text = subText;
}
//赎回
- (void)ransomButtonDidTap {
    switch (self.cellType) {
        case AssetsTitleCellTypeLingQianGuan:
        {
            [[UserAuthData shareInstance] refreshUserInfoResponse:^(UserAuthDataModel *authData, NSString *errMsg) {
                if ([authData.isAuthentication isEqualToString:@"1"] && authData.bankCard.length > 0) {
                    if (authData.hasTradePwd.integerValue <= 0) {//设置密码
                        SetChargePasswordViewController *setVC=[[SetChargePasswordViewController alloc] init];
                        [self.jk_viewController.navigationController pushViewController:setVC animated:YES];
                    } else {
                        SellOtherViewController *sellOtherVC = [[SellOtherViewController alloc] init];
                        [self.jk_viewController.navigationController pushViewController:sellOtherVC animated:YES];
                    }
                } else {//绑定银行卡
                    BindBankCarViewController *bindBankCardVC=[[BindBankCarViewController alloc] init];
                    [self.jk_viewController.navigationController pushViewController:bindBankCardVC animated:YES];
                }
            }];
        }
            break;
        default:
            break;
    }
}
//记录
- (void)recordLabelDidTap {
    switch (self.cellType) {
        case AssetsTitleCellTypeGold:
        {
            RecordListViewController *allGoldList = [[RecordListViewController alloc] initWithType:RecordTypeAllGold];
            [self.jk_viewController.navigationController pushViewController:allGoldList animated:YES];
        }
            break;
        case AssetsTitleCellTypeZhaoCaiJin:
        {
            RecordListViewController *zcjRecordList = [[RecordListViewController alloc] initWithType:RecordTypeZhaoCaiJin];
            [self.jk_viewController.navigationController pushViewController:zcjRecordList animated:YES];
        }
            break;
        case AssetsTitleCellTypeShengCaiJin:
        {
            RecordListViewController *scjRecord = [[RecordListViewController alloc] initWithType:RecordTypeShengCaiJin];
            [self.jk_viewController.navigationController pushViewController:scjRecord animated:YES];
        }
            break;
        case AssetsTitleCellTypeOtherAssets:
        {
            RecordListViewController *otherRecord = [[RecordListViewController alloc] initWithType:RecordTypeOtherAssets];
            [self.jk_viewController.navigationController pushViewController:otherRecord animated:YES];
        }
            break;
        case AssetsTitleCellTypeCunQianGuan:
        {
            RecordListViewController *cqgRecord = [[RecordListViewController alloc] initWithType:RecordTypeCunQianGuan];
            [self.jk_viewController.navigationController pushViewController:cqgRecord animated:YES];
        }
            break;
        case AssetsTitleCellTypeLingQianGuan:
        {
            RecordListViewController *lqgRecord = [[RecordListViewController alloc] initWithType:RecordTypeLingQianGuan];
            [self.jk_viewController.navigationController pushViewController:lqgRecord animated:YES];
        }
            break;
        default:
            break;
    }
}

- (void)setCellType:(AssetsTitleCellType)cellType {
    _cellType = cellType;
    switch (cellType) {
        case AssetsTitleCellTypeGold:
        {
            self.accountLabel.text = @"0";
        }
            break;
        case AssetsTitleCellTypeZhaoCaiJin:
        {
            self.nameLabel.text = @"招财金（克）";
            self.accountLabel.text = @"0";
        }
            break;
        case AssetsTitleCellTypeShengCaiJin:
        {
            self.nameLabel.text = @"生财金（克）";
            self.accountLabel.text = @"0";
        }
            break;
        case AssetsTitleCellTypeOtherAssets:
        {
            self.nameLabel.text = @"其他资产(元)";
            self.accountLabel.text = @"0";
        }
            break;
        case AssetsTitleCellTypeLingQianGuan:
        {
            self.nameLabel.text = @"零钱罐(元)";
            self.accountLabel.text = @"0";
            self.recordLabel.hidden = NO;
            self.arrowImageView.hidden = NO;
            self.ransomButton.hidden = NO;
        }
            break;
        default://AssetsTitleCellTypeCunQianGuan
        {
            self.nameLabel.text = @"存钱罐(元)";
            self.accountLabel.text = @"0";
//            self.recordLabel.hidden = YES;
//            self.arrowImageView.hidden = YES;
//            self.ransomButton.hidden = NO;
        }
            break;
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(10);
        make.left.equalTo(self).offset(LEFT_RIGHT_MARGIN);
    }];
    [self.accountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLabel);
        make.bottom.equalTo(self).offset(-10);
    }];
    if (self.cellType == AssetsTitleCellTypeLingQianGuan) {
        [self.recordLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.right.equalTo(self.arrowImageView.mas_left).offset(-7);
        }];
        [self.arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.recordLabel);
            make.right.equalTo(self).offset(-LEFT_RIGHT_MARGIN);
        }];
        [self.ransomButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.right.equalTo(self.recordLabel.mas_left).offset(-LEFT_RIGHT_MARGIN);
            make.width.equalTo(@77);
        }];
    } else {
        [self.recordLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.nameLabel);
            make.right.equalTo(self.arrowImageView.mas_left).offset(-7);
        }];
        [self.arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.recordLabel);
            make.right.equalTo(self).offset(-LEFT_RIGHT_MARGIN);
        }];
        [self.ransomButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.recordLabel.mas_bottom).offset(8);
            make.right.equalTo(self.arrowImageView);
            make.width.equalTo(@77);
        }];
    }
    
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, NavBarHeight)]) {
        self.backgroundColor = COLOR_WHITE;
        [self addSubview:self.nameLabel];
        [self addSubview:self.accountLabel];
        [self addSubview:self.recordLabel];
        [self addSubview:self.arrowImageView];
        [self addSubview:self.ransomButton];
    }
    return self;
}

/**
 懒加载
 
 @return 赎回
 */
- (UIButton *)ransomButton {
    if (!_ransomButton) {
        _ransomButton = [[UIButton alloc] init];
        [_ransomButton setTitle:@"赎回" forState:UIControlStateNormal];
        [_ransomButton setTitleColor:COLOR_MAIN forState:UIControlStateNormal];
        _ransomButton.titleLabel.font = FONT_NAV;
        _ransomButton.layer.cornerRadius = 7.0f;
        _ransomButton.layer.borderColor = COLOR_MAIN.CGColor;
        _ransomButton.layer.borderWidth = 1.0f;
        _ransomButton.hidden = YES;
        [_ransomButton addTarget:self action:@selector(ransomButtonDidTap) forControlEvents:UIControlEventTouchUpInside];
    }
    return _ransomButton;
}

/**
 懒加载
 
 @return 箭头
 */
- (UIImageView *)arrowImageView {
    if (!_arrowImageView) {
        _arrowImageView = [[UIImageView alloc] init];
        _arrowImageView.image = [UIImage imageNamed:@"icon_nextArrow"];
    }
    return _arrowImageView;
}
/**
 懒加载
 
 @return 交易记录
 */
- (UILabel *)recordLabel {
    if (!_recordLabel) {
        _recordLabel = [[UILabel alloc] init];
        _recordLabel.font = FONT_TEXT;
        _recordLabel.textColor = COLOR_SUBTEXT;
        _recordLabel.text = @"交易记录";
        [_recordLabel addTapAction:@selector(recordLabelDidTap) target:self];
    }
    return _recordLabel;
}
/**
 懒加载
 
 @return 产品额
 */
- (UILabel *)accountLabel {
    if (!_accountLabel) {
        _accountLabel = [[UILabel alloc] init];
        _accountLabel.font = FONT_TEXT;
        _accountLabel.textColor = COLOR_Red;
        _accountLabel.text = @"0";
    }
    return _accountLabel;
}
/**
 懒加载
 
 @return 产品名称
 */
- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.font = FONT_TITLE;
        _nameLabel.textColor = COLOR_TEXT;
        _nameLabel.text = @"黄金资产（克）";
    }
    return _nameLabel;
}

@end
