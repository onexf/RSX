//
//  BalanceBottomeView.m
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/9/7.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "BalanceBottomeView.h"
#import "UIView+Extension.h"
#import "BalanceTradeVC.h"
#import "WithdrawalsViewController.h"
#import "RechargeViewController.h"
#import "MineHomePageModel.h"
#import "UserAuthData.h"
#import "BindBankCarViewController.h"
#import "SetChargePasswordViewController.h"
#import "RecordListViewController.h"
@interface BalanceBottomeView ()
/** 可用余额 */
@property(nonatomic, strong) UILabel *canuseBanlance;
/** 问号 */
@property(nonatomic, strong) UIImageView *questionImageView;
/** 可用View */
@property(nonatomic, strong) UILabel *canuseValue;
/** 交易明细 */
@property(nonatomic, strong) UILabel *detailsLabel;
/** 箭头 */
@property(nonatomic, strong) UIImageView *arrowImageView;

/** 冻结资金 */
@property(nonatomic, strong) UILabel *freezeMoney;
/** 冻结数值 */
@property(nonatomic, strong) UILabel *freezeValue;
/** 充值 */
@property(nonatomic, strong) UIButton *recharge;
/** 提现 */
@property(nonatomic, strong) UIButton *withdraw;
/** wenhao */
@property(nonatomic, strong) UIImageView *freezeImageView;

@end

@implementation BalanceBottomeView

- (void)freezeImageViewDidTap {
    [TipsTool showTipsWithTitle:@"冻结资金" andContent:@"当您发起提现申请后，招金猫平台冻结并审核该笔资金。审核通过，该笔资金解冻并进入提现流程；审核不通过，冻结金额将返还至您的帐户余额。三个工作日内完成解冻提现。"];
}
- (void)setAssetsData:(MineHomePageModel *)assetsData {
    _assetsData = assetsData;
    self.canuseValue.text = assetsData.accountBalance;
    self.freezeValue.text = assetsData.frozenAmount;
}

- (void)details {
    RecordListViewController *balanceList = [[RecordListViewController alloc] initWithType:RecordTypeBalance];
    [self.jk_viewController.navigationController pushViewController:balanceList animated:YES];
//    BalanceTradeVC *vc=[[BalanceTradeVC alloc] init];
//    [self.jk_viewController.navigationController pushViewController:vc animated:YES];
}
- (void)buttonDidTap:(UIButton *)button {
    
    [[UserAuthData shareInstance]refreshUserInfoResponse:^(UserAuthDataModel *userAuthData, NSString *errMsg) {
        
        if ([userAuthData.isAuthentication boolValue]>0)
        {
            if (userAuthData.hasTradePwd.integerValue>0)
            {
                if ([button.titleLabel.text isEqualToString:@"充值"]) {
                    RechargeViewController *vc=[[RechargeViewController alloc] init];
                    [self.jk_viewController.navigationController pushViewController:vc animated:YES];
                } else {
                    WithdrawalsViewController *vc=[[WithdrawalsViewController alloc] init];
                    [self.jk_viewController.navigationController pushViewController:vc animated:YES];
                }
                
            }
            else
            {
                SetChargePasswordViewController*vc=[[SetChargePasswordViewController alloc]init];
                [self.jk_viewController.navigationController pushViewController:vc animated:YES];
            }

            
        }
        else
        {
            
            BindBankCarViewController *vc=[[BindBankCarViewController alloc]init];
            [self.jk_viewController.navigationController pushViewController:vc animated:YES];
            
        }
        
    }];

}
- (void)layoutSubviews {
    [super layoutSubviews];
    [self.canuseBanlance mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(10);
        make.centerX.equalTo(self);
    }];
    [self.questionImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.canuseBanlance);
        make.left.equalTo(self.mas_right).offset(7);
    }];
    [self.canuseValue mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.canuseBanlance.mas_bottom).offset(10);
        make.left.equalTo(self).offset(LEFT_RIGHT_MARGIN);
        make.height.equalTo(@50);
        make.right.equalTo(self).offset(-LEFT_RIGHT_MARGIN);
    }];
    [self.arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.canuseValue);
        make.right.equalTo(self.canuseValue).offset(-LEFT_RIGHT_MARGIN);
    }];
    [self.detailsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.arrowImageView);
        make.right.equalTo(self.arrowImageView.mas_left).offset(-7);
    }];
    [self.freezeMoney mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.canuseValue.mas_bottom).offset(10);
        make.centerX.equalTo(self);
    }];
    [self.freezeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.freezeMoney);
        make.left.equalTo(self.freezeMoney.mas_right);
    }];
    [self.freezeValue mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.freezeMoney.mas_bottom).offset(10);
        make.left.right.height.equalTo(self.canuseValue);
    }];
    [self.recharge mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.equalTo(self);
        make.height.equalTo(@49);
    }];
    [self.withdraw mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.width.height.equalTo(self.recharge);
        make.right.equalTo(self);
        make.left.equalTo(self.recharge.mas_right);
    }];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 360)]) {
        self.backgroundColor = COLOR_TBBACK;
        [self addSubview:self.canuseBanlance];
        [self addSubview:self.questionImageView];
        [self addSubview:self.canuseValue];
        [self addSubview:self.detailsLabel];
        [self addSubview:self.arrowImageView];
        [self addSubview:self.freezeMoney];
        [self addSubview:self.freezeValue];
        [self addSubview:self.recharge];
        [self addSubview:self.withdraw];
        [self addSubview:self.freezeImageView];
    }
    return self;
}
/**
 懒加载
 
 @return 问好
 */
- (UIImageView *)freezeImageView {
    if (!_freezeImageView) {
        _freezeImageView = [[UIImageView alloc] init];
        _freezeImageView.image = [UIImage imageNamed:@"icon_question"];
        [_freezeImageView addTapAction:@selector(freezeImageViewDidTap) target:self];
    }
    return _freezeImageView;
}
/**
 懒加载
 
 @return 提现
 */
- (UIButton *)withdraw {
    if (!_withdraw) {
        _withdraw = [[UIButton alloc] init];
        _withdraw.backgroundColor = COLOR_Red;
        _withdraw.titleLabel.font = FONT_BUTTON_TEXT;
        [_withdraw setTitle:@"提现" forState:UIControlStateNormal];
        [_withdraw addTarget:self action:@selector(buttonDidTap:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _withdraw;
}

/**
 懒加载
 
 @return 充值
 */
- (UIButton *)recharge {
    if (!_recharge) {
        _recharge = [[UIButton alloc] init];
        _recharge.backgroundColor = COLOR_MAIN;
        _recharge.titleLabel.font = FONT_BUTTON_TEXT;
        [_recharge setTitle:@"充值" forState:UIControlStateNormal];
        [_recharge addTarget:self action:@selector(buttonDidTap:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _recharge;
}

/**
 懒加载
 
 @return 冻结数额
 */
- (UILabel *)freezeValue {
    if (!_freezeValue) {
        _freezeValue = [[UILabel alloc] init];
        _freezeValue.backgroundColor = COLOR_WHITE;
        _freezeValue.layer.cornerRadius = 10;
        _freezeValue.clipsToBounds = YES;
        _freezeValue.textAlignment = NSTextAlignmentCenter;
        _freezeValue.font = FONT(25);
        _freezeValue.textColor = COLOR_Red;
        _freezeValue.text = @"0000.00";

    }
    return _freezeValue;
}
/**
 懒加载
 
 @return 冻结资金
 */
- (UILabel *)freezeMoney {
    if (!_freezeMoney) {
        _freezeMoney = [[UILabel alloc] init];
        _freezeMoney.text = @"冻结资金(元)";
        _freezeMoney.textColor = COLOR_SUBTEXT;
        _freezeMoney.font = FONT_TEXT;

    }
    return _freezeMoney;
}
/**
 懒加载
 
 @return 箭头
 */
- (UIImageView *)arrowImageView {
    if (!_arrowImageView) {
        _arrowImageView = [[UIImageView alloc] init];
        _arrowImageView.image = [UIImage imageNamed:@"icon_nextArrow"];
        [_arrowImageView addTapAction:@selector(details) target:self];
    }
    return _arrowImageView;
}
/**
 懒加载
 
 @return 交易明细
 */
- (UILabel *)detailsLabel {
    if (!_detailsLabel) {
        _detailsLabel = [[UILabel alloc] init];
        _detailsLabel.text = @"交易记录";
        _detailsLabel.font = FONT_SUBTEXT;
        _detailsLabel.textColor = COLOR_SUBTEXT;
        [_detailsLabel addTapAction:@selector(details) target:self];
    }
    return _detailsLabel;
}
/**
 懒加载
 
 @return 可用余额
 */
- (UILabel *)canuseValue {
    if (!_canuseValue) {
        _canuseValue = [[UILabel alloc] init];
        _canuseValue.backgroundColor = COLOR_WHITE;
        _canuseValue.layer.cornerRadius = 10;
        _canuseValue.clipsToBounds = YES;
        _canuseValue.textAlignment = NSTextAlignmentCenter;
        _canuseValue.font = FONT(25);
        _canuseValue.textColor = COLOR_Red;
        _canuseValue.text = @"0.00";
    }
    return _canuseValue;
}
/**
 懒加载
 
 @return 问号
 */
- (UIImageView *)questionImageView {
    if (!_questionImageView) {
        _questionImageView = [[UIImageView alloc] init];
        _questionImageView.image = [UIImage imageNamed:@"icon_question"];
    }
    return _questionImageView;
}
/**
 懒加载
 
 @return 可用余额(元)
 */
- (UILabel *)canuseBanlance {
    if (!_canuseBanlance) {
        _canuseBanlance = [[UILabel alloc] init];
        _canuseBanlance.text = @"可用余额(元)";
        _canuseBanlance.textColor = COLOR_SUBTEXT;
        _canuseBanlance.font = FONT_TEXT;
    }
    return _canuseBanlance;
}

@end
