//
//  RechargeAndWithdrawView.h
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/9/5.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MineHomePageModel;
@interface RechargeAndWithdrawView : UIView
/** 收益 */
@property(nonatomic, strong) MineHomePageModel *earnData;
@end
