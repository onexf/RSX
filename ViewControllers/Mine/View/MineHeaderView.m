//
//  MineHeaderView.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/2.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "MineHeaderView.h"
#import "MineHomePageModel.h"
#import "PNChart.h"
#import "UIView+Extension.h"

@interface MineHeaderView ()<PNChartDelegate>
/** 图 */
@property(nonatomic, strong) PNPieChart *pieChart;
/** 块数据 */
@property(nonatomic, strong) NSArray *pieItems;

/** totalAssetsView */
@property(nonatomic, strong) UIView *totalAssetsView;
/** 总资产约 */
@property(nonatomic, strong) UILabel *totalAssetsLabel;
/** 总资产数值 */
@property(nonatomic, strong) UILabel *totalAssetsAccountLabel;
/** 眼睛 */
@property(nonatomic, strong) UIButton *hideAssetsButton;

@end

@implementation MineHeaderView

- (void)setIsMineHomeChart:(BOOL)isMineHomeChart {
    _isMineHomeChart = isMineHomeChart;
    if (isMineHomeChart) {
        [self.hideAssetsButton setImage:[UIImage imageNamed:@"icon_show_assets"] forState:UIControlStateSelected];
        [self.hideAssetsButton setImage:[UIImage imageNamed:@"icon_hide_assets"] forState:UIControlStateNormal];
    } else {
        [self.hideAssetsButton setImage:[UIImage imageNamed:@"icon_question"] forState:UIControlStateSelected];
        [self.hideAssetsButton setImage:[UIImage imageNamed:@"icon_question"] forState:UIControlStateNormal];
    }
}

- (void)userClickedOnPieIndexItem:(NSInteger)pieIndex {
    NSLog(@"点击了：%ld", (long)pieIndex);
}

- (void)didUnselectPieItem {
    if (self.tapDelegate && [self.tapDelegate respondsToSelector:@selector(didTapPieChartViewCenter)]) {
        [self.tapDelegate didTapPieChartViewCenter];
    }
}

- (void)hideAssetsButtonDidTap:(UIButton *)button {
    if (self.isMineHomeChart) {
        self.hideAssetsButton.selected = !button.selected;
        self.assetsIsShow = self.hideAssetsButton.selected;
        if (self.hideAssetsButton.selected) {
            self.totalAssetsAccountLabel.text = self.userAssets.totalAssetValue;
        } else {
            self.totalAssetsAccountLabel.text = @"*******";
        }
        if (self.tapDelegate && [self.tapDelegate respondsToSelector:@selector(didTapHideViewWithStatus:)]) {
            [self.tapDelegate didTapHideViewWithStatus:!self.hideAssetsButton.selected];
        }
    } else {
        [TipsTool showTipsWithTitle:@"资产总价值" andContent:@"1.总资产 = 持仓黄金产品总价值 + 其他产品总价值 + 账户余额\n\n2.黄金价格实时波动，总资产存在刷新变动情况。"];
    }
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 283)]) {
        self.backgroundColor = COLOR_TBBACK;
        [self addSubview:self.pieChart];
        [self addSubview:self.totalAssetsView];
        [self addSubview:self.hideAssetsButton];
    }
    return self;
}
- (void)layoutSubviews {
    [super layoutSubviews];
    [self.pieChart mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self);
        make.width.height.equalTo(@223);
    }];
    [self.totalAssetsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self);
    }];
    [self.totalAssetsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.totalAssetsView);
        make.top.equalTo(self.totalAssetsView);
    }];
    [self.totalAssetsAccountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.totalAssetsLabel.mas_bottom).offset(10);
        make.centerX.equalTo(self.totalAssetsView);
        make.bottom.equalTo(self.totalAssetsView);
    }];
    [self.hideAssetsButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(20);
        make.right.equalTo(self).offset(-20);
    }];
}

- (void)setUserAssets:(MineHomePageModel *)userAssets {
    _userAssets = userAssets;
    if (userAssets.accountBalance.floatValue <= 0 && userAssets.goldAssets.floatValue <= 0 && userAssets.otherAssets.floatValue <= 0) {
        self.pieItems = @[
                              [PNPieChartDataItem dataItemWithValue:0 color:COLOR_SUBTEXT description:@"账户余额"],
                              [PNPieChartDataItem dataItemWithValue:0 color:COLOR_SUBTEXT description:@"其他资产"],
                              [PNPieChartDataItem dataItemWithValue:0 color:COLOR_SUBTEXT description:@"黄金资产"],
                          ];
    } else {
        self.pieItems = @[
                              [PNPieChartDataItem dataItemWithValue:userAssets.accountBalance.floatValue < 0 ? 0 : userAssets.accountBalance.floatValue color:COLOR_Red description:@"账户余额"],
                              [PNPieChartDataItem dataItemWithValue:userAssets.otherAssets.floatValue < 0 ? 0 : userAssets.otherAssets.floatValue color:UIColorFromHex(0x007eff) description:@"其他资产"],
                              [PNPieChartDataItem dataItemWithValue:userAssets.goldAssets.floatValue * [UDGET(@"currentGoldPrice") floatValue] < 0 ? 0 : userAssets.goldAssets.floatValue * [UDGET(@"currentGoldPrice") floatValue] color:UIColorFromHex(0XC574) description:@"黄金资产"]
                              ];
    }
    [self.pieChart updateChartData:self.pieItems];
    [self.pieChart strokeChart];
    if (self.assetsIsShow) {
        self.totalAssetsAccountLabel.text = userAssets.totalAssetValue;
    } else {
        self.totalAssetsAccountLabel.text = @"*******";
    }
}

/**
 懒加载
 
 @return hideAssetsButton
 */
- (UIButton *)hideAssetsButton {
    if (!_hideAssetsButton) {
        _hideAssetsButton = [[UIButton alloc] init];
        [_hideAssetsButton setImage:[UIImage imageNamed:@"icon_show_assets"] forState:UIControlStateSelected];
        [_hideAssetsButton setImage:[UIImage imageNamed:@"icon_hide_assets"] forState:UIControlStateNormal];
        [_hideAssetsButton sizeToFit];
        _hideAssetsButton.selected = YES;
        [_hideAssetsButton addTarget:self action:@selector(hideAssetsButtonDidTap:) forControlEvents:UIControlEventTouchUpInside];
        _hideAssetsButton.jk_touchAreaInsets = UIEdgeInsetsMake(20, 20, 20, 20);
        _hideAssetsButton.timeInterval = 0.7;
    }
    return _hideAssetsButton;
}

/**
 懒加载
 
 @return 资产值
 */
- (UILabel *)totalAssetsAccountLabel {
    if (!_totalAssetsAccountLabel) {
        _totalAssetsAccountLabel = [[UILabel alloc] init];
        _totalAssetsAccountLabel.textColor = COLOR_TEXT;
        _totalAssetsAccountLabel.font = FONT_TEXT;
    }
    return _totalAssetsAccountLabel;
}

/**
 懒加载
 
 @return 总资产
 */
- (UILabel *)totalAssetsLabel {
    if (!_totalAssetsLabel) {
        _totalAssetsLabel = [[UILabel alloc] init];
        _totalAssetsLabel.text = @"总资产约";
        _totalAssetsLabel.font = FONT_TEXT;
        _totalAssetsLabel.textColor = COLOR_SUBTEXT;
    }
    return _totalAssetsLabel;
}

/**
 懒加载
 
 @return 总资产
 */
- (UIView *)totalAssetsView {
    if (!_totalAssetsView) {
        _totalAssetsView = [[UIView alloc] init];
        [_totalAssetsView addSubview:self.totalAssetsLabel];
        [_totalAssetsView addSubview:self.totalAssetsAccountLabel];
        _totalAssetsView.backgroundColor = COLOR_MAIN;
    }
    return _totalAssetsView;
}

/**
 懒加载
 
 @return 图
 */
- (PNPieChart *)pieChart {
    if (!_pieChart) {
        _pieChart = [[PNPieChart alloc] initWithFrame:CGRectZero items:nil];
        _pieChart.descriptionTextColor = [UIColor whiteColor];
        _pieChart.descriptionTextFont = [UIFont fontWithName:@"Avenir-Medium" size:8.0];
        _pieChart.descriptionTextShadowColor = [UIColor clearColor];
        _pieChart.delegate = self;
    }
    return _pieChart;
}

/**
 懒加载
 
 @return 数据数组
 */
- (NSArray *)pieItems {
    if (!_pieItems) {
        _pieItems = @[
                          [PNPieChartDataItem dataItemWithValue:0 color:COLOR_Red description:@"账户余额"],
                          [PNPieChartDataItem dataItemWithValue:0 color:UIColorFromHex(0x007eff) description:@"其他产品"],
                          [PNPieChartDataItem dataItemWithValue:0 color:UIColorFromHex(0x00c574) description:@"黄金产品"]
                      ];
    }
    return _pieItems;
}

@end
