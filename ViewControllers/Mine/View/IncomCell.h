//
//  IncomCell.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/12.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>
@class IncomeCellModel;
@interface IncomCell : UITableViewCell
/** 数据 */
@property(nonatomic, strong) IncomeCellModel *incomeModel;

+ (instancetype)cellWithTableview:(UITableView *)tableView;

@end
