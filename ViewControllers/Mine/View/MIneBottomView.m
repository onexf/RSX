//
//  MIneBottomView.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/1.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "MIneBottomView.h"
#import "MineBottomViewCell.h"

@interface MIneBottomView ()
/** 交易记录 */
@property(nonatomic, strong) MineBottomViewCell *myOrderCell;
/** 邀请奖励 */
@property(nonatomic, strong) MineBottomViewCell *inviteCell;
/** 卡券 */
@property(nonatomic, strong) MineBottomViewCell *saveGoldCell;
/** 我要提金 */
@property(nonatomic, strong) MineBottomViewCell *takeGoldCell;
/** 帮助中心 */
@property(nonatomic, strong) MineBottomViewCell *helpCenter;
/** 帮助中心 */
@property(nonatomic, strong) MineBottomViewCell *qrCodeCell;
/** 货币正在贬值  */
@property(nonatomic, strong) UILabel *textLabel;
/** 持有黄金是避免资产缩水的有效方法 */
@property(nonatomic, strong) UILabel *subTextLabel;

@end

@implementation MIneBottomView

- (void)setCouponCount:(NSString *)couponCount {
    _couponCount = couponCount;
    self.saveGoldCell.subText = couponCount;
}
- (void)layoutSubviews {
    [super layoutSubviews];
    [self.myOrderCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self);
        make.height.equalTo(@(NORMAL_CELL_HEIGHT));
    }];
    [self.saveGoldCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.myOrderCell.mas_bottom);
        make.left.right.equalTo(self);
        make.height.equalTo(@(NORMAL_CELL_HEIGHT));
    }];
    [self.inviteCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.saveGoldCell.mas_bottom);
        make.left.right.equalTo(self);
        make.height.equalTo(@(NORMAL_CELL_HEIGHT));
    }];
    [self.takeGoldCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.inviteCell.mas_bottom);
        make.left.right.equalTo(self);
        make.height.equalTo(@(NORMAL_CELL_HEIGHT));
    }];
    [self.helpCenter mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.takeGoldCell.mas_bottom);
        make.left.right.equalTo(self);
        make.height.equalTo(@(NORMAL_CELL_HEIGHT));
    }];
    [self.qrCodeCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.helpCenter.mas_bottom);
        make.left.right.equalTo(self);
        make.height.equalTo(@(NORMAL_CELL_HEIGHT));
    }];
    
    [self.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.qrCodeCell.mas_bottom).offset(20);
        make.centerX.equalTo(self);
    }];
    [self.subTextLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textLabel.mas_bottom).offset(5);
        make.centerX.equalTo(self);
    }];
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 284)]) {
        self.backgroundColor = COLOR_TBBACK;
        [self addSubview:self.myOrderCell];
        [self addSubview:self.inviteCell];
        [self addSubview:self.saveGoldCell];
        [self addSubview:self.takeGoldCell];
        [self addSubview:self.helpCenter];
        [self addSubview:self.qrCodeCell];
        [self addSubview:self.textLabel];
        [self addSubview:self.subTextLabel];
    }
    return self;
}
/**
 懒加载
 
 @return 持有黄金是避免资产缩水的有效方法
 */
- (UILabel *)subTextLabel {
    if (!_subTextLabel) {
        _subTextLabel = [[UILabel alloc] init];
        _subTextLabel.font = FONT_SUBTEXT;
        _subTextLabel.textColor = COLOR_SUBTEXT;
        _subTextLabel.text = @"持有黄金是避免资产缩水的有效方法";
    }
    return _subTextLabel;
}

/**
 懒加载
 
 @return text
 */
- (UILabel *)textLabel {
    if (!_textLabel) {
        _textLabel = [[UILabel alloc] init];
        _textLabel.font = FONT_SUBTEXT;
        _textLabel.textColor = COLOR_SUBTEXT;
        _textLabel.text = @"";
    }
    return _textLabel;
}
/**
 懒加载
 
 @return 二维码
 */
- (MineBottomViewCell *)qrCodeCell {
    if (!_qrCodeCell) {
        _qrCodeCell = [[MineBottomViewCell alloc] init];
        _qrCodeCell.cellData = @{
                                 @"image" : @"icon_server",
                                 @"title" : @"我的客服"
                                 };
        __weak typeof(self) weakSelf = self;
        [_qrCodeCell jk_addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
            if (weakSelf.viewDelegate && [weakSelf.viewDelegate respondsToSelector:@selector(didSelectRowAtIndex:)]) {
                [weakSelf.viewDelegate didSelectRowAtIndex:5];
            }
        }];
    }
    return _qrCodeCell;
}
/**
 懒加载
 
 @return 邀请
 */
- (MineBottomViewCell *)helpCenter {
    if (!_helpCenter) {
        _helpCenter = [[MineBottomViewCell alloc] init];
        _helpCenter.cellData = @{
                                 @"image" : @"icon_help_center",
                                 @"title" : @"我的二维码"
                                 };
        __weak typeof(self) weakSelf = self;
        [_helpCenter jk_addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
            if (weakSelf.viewDelegate && [weakSelf.viewDelegate respondsToSelector:@selector(didSelectRowAtIndex:)]) {
                [weakSelf.viewDelegate didSelectRowAtIndex:4];
            }
        }];
    }
    return _helpCenter;
}

/**
 懒加载
 
 @return 提金
 */
- (MineBottomViewCell *)takeGoldCell {
    if (!_takeGoldCell) {
        _takeGoldCell = [[MineBottomViewCell alloc] init];
        _takeGoldCell.cellData = @{
                                 @"image" : @"icon_out_gold",
                                 @"title" : @"我的金库"
                                 };
        __weak typeof(self) weakSelf = self;
        [_takeGoldCell jk_addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
            if (weakSelf.viewDelegate && [weakSelf.viewDelegate respondsToSelector:@selector(didSelectRowAtIndex:)]) {
                [weakSelf.viewDelegate didSelectRowAtIndex:3];
            }
        }];
    }
    return _takeGoldCell;
}

/**
 懒加载
 
 @return 邀请
 */
- (MineBottomViewCell *)inviteCell {
    if (!_inviteCell) {
        _inviteCell = [[MineBottomViewCell alloc] init];
        _inviteCell.cellData = @{
                                     @"image" : @"icon_invite",
                                     @"title" : @"我的邀请"
                                 };
        __weak typeof(self) weakSelf = self;
        [_inviteCell jk_addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
            if (weakSelf.viewDelegate && [weakSelf.viewDelegate respondsToSelector:@selector(didSelectRowAtIndex:)]) {
                [weakSelf.viewDelegate didSelectRowAtIndex:2];
            }
        }];
    }
    return _inviteCell;
}

/**
 懒加载
 
 @return 存金
 */
- (MineBottomViewCell *)saveGoldCell {
    if (!_saveGoldCell) {
        _saveGoldCell = [[MineBottomViewCell alloc] init];
        _saveGoldCell.cellData =@{
                                      @"image" : @"icon_in_gold",
                                      @"title" : @"卡券中心"
                                  };
        __weak typeof(self) weakSelf = self;
        [_saveGoldCell jk_addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
            if (weakSelf.viewDelegate && [weakSelf.viewDelegate respondsToSelector:@selector(didSelectRowAtIndex:)]) {
                [weakSelf.viewDelegate didSelectRowAtIndex:1];
            }
        }];
    }
    return _saveGoldCell;
}

/**
 懒加载
 
 @return 我的订单
 */
- (MineBottomViewCell *)myOrderCell {
    if (!_myOrderCell) {
        _myOrderCell = [[MineBottomViewCell alloc] init];
        _myOrderCell.cellData = @{
                                      @"image" : @"icon_my_order",
                                      @"title" : @"我的存折"
                                  };
        __weak typeof(self) weakSelf = self;
        [_myOrderCell jk_addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
            if (weakSelf.viewDelegate && [weakSelf.viewDelegate respondsToSelector:@selector(didSelectRowAtIndex:)]) {
                [weakSelf.viewDelegate didSelectRowAtIndex:0];
            }
        }];
    }
    return _myOrderCell;
}


@end
