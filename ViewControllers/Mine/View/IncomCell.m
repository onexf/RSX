//
//  IncomCell.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/12.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "IncomCell.h"
#import "IncomeCellModel.h"
@interface IncomCell ()
/** back */
@property(nonatomic, strong) UIView *backView;
/** name */
@property(nonatomic, strong) UILabel *nameLabel;
/** income */
@property(nonatomic, strong) UILabel *incomeLabel;

@end


@implementation IncomCell
- (void)setIncomeModel:(IncomeCellModel *)incomeModel {
    _incomeModel = incomeModel;
    self.nameLabel.text = incomeModel.productName;
    if (incomeModel.floatProfit.floatValue == 0) {
        self.incomeLabel.textColor = COLOR_SUBTEXT;
    } else if (incomeModel.floatProfit.floatValue > 0) {
        self.incomeLabel.textColor = COLOR_Red;
    } else {
        self.incomeLabel.textColor = UIColorFromHex(0x16a83f);
    }
    self.incomeLabel.text = [NSString stringWithFormat:@"%@ 元", incomeModel.floatProfit];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.right.equalTo(self.contentView);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.backView);
        make.left.equalTo(self.backView).offset(LEFT_RIGHT_MARGIN);
    }];
    [self.incomeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.backView);
        make.right.equalTo(self.backView).offset(-LEFT_RIGHT_MARGIN);
    }];
    
}

+ (instancetype)cellWithTableview:(UITableView *)tableView {
    static NSString *identifier = @"IncomCell";
    IncomCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[IncomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.contentView.backgroundColor = COLOR_WHITE;
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.backView];
    }
    return self;
}
/**
 懒加载
 
 @return income
 */
- (UILabel *)incomeLabel {
    if (!_incomeLabel) {
        _incomeLabel = [[UILabel alloc] init];
        _incomeLabel.textColor = COLOR_SUBTEXT;
        _incomeLabel.font = FONT_TEXT;
    }
    return _incomeLabel;
}
/**
 懒加载
 
 @return name
 */
- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.textColor = COLOR_TEXT;
        _nameLabel.font = FONT_TEXT;
    }
    return _nameLabel;
}
/**
 懒加载
 
 @return 背景
 */
- (UIView *)backView {
    if (!_backView) {
        _backView = [[UIView alloc] init];
        _backView.backgroundColor = COLOR_WHITE;
        [_backView addSubview:self.nameLabel];
        [_backView addSubview:self.incomeLabel];
    }
    return _backView;
}
- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
