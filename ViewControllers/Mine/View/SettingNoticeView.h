//
//  SettingNoticeView.h
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/9/14.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingNoticeView : UIView
/** high */
@property(nonatomic, strong) UITextField *lowTextField;
/** high */
@property(nonatomic, strong) UITextField *highTextField;

@end
