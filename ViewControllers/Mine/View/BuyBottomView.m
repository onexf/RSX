//
//  BuyBottomView.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/10/28.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "BuyBottomView.h"
#import "RecordDetailModel.h"
@interface BuyBottomView ()
/** 计息日 */
@property(nonatomic, strong) UILabel *buyStartDateLabel;
/** 计息日值 */
@property(nonatomic, strong) UILabel *buyStartDateValue;
/** buyLine4 */
@property(nonatomic, strong) UIView *buyLine4;
/** 到期日 */
@property(nonatomic, strong) UILabel *buyEndDateLabel;
/** 到期日值 */
@property(nonatomic, strong) UILabel *buyEndDateValue;

@end
@implementation BuyBottomView

- (void)setModel:(RecordDetailModel *)model {
    _model = model;
    self.buyStartDateValue.text = model.interestTime;
    self.buyEndDateValue.text = model.dueDate;
    if (model.termOfInvestment == -1 || model.logType == 3) {
        self.buyLine4.hidden = YES;
        self.buyEndDateLabel.hidden = YES;
        self.buyEndDateValue.hidden = YES;
    }
    if (model.logType == 3) {
        self.buyStartDateLabel.text = (model.orderStatus == 1) ? @"到账时间" : @"预计到账";
        NSDate *date = [NSDate jk_offsetDays:2 fromDate:[NSDate jk_dateWithString:model.payTime format:@"yyyy-MM-dd"]];
        self.buyStartDateValue.text = (model.orderStatus == 1) ? [[NSDate jk_dateWithString:model.payTime format:@"yyyy-MM-dd"] jk_stringWithFormat:@"yyyy-MM-dd"] : [date jk_stringWithFormat:@"yyyy-MM-dd"];
    }
    if (model.logType == 2) {
        self.buyLine4.hidden = NO;
        self.buyEndDateLabel.hidden = NO;
        self.buyEndDateValue.hidden = NO;
        self.buyStartDateLabel.text = @"手续费";
        self.buyStartDateValue.text = [NSString stringWithFormat:@"%@元", model.sellPoundage];
        self.buyEndDateLabel.text = @"卖出收入";
        self.buyEndDateValue.text = [NSString stringWithFormat:@"%@元", model.sellIncome];
    }
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = COLOR_WHITE;
        [self addSubview:self.buyStartDateLabel];
        [self addSubview:self.buyStartDateValue];
        [self addSubview:self.buyLine4];
        [self addSubview:self.buyEndDateLabel];
        [self addSubview:self.buyEndDateValue];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if (self.buyLine4.hidden) {
        [self.buyStartDateLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).offset(15);
            make.left.equalTo(self).offset(LEFT_RIGHT_MARGIN);
        }];
        [self.buyStartDateValue mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.buyStartDateLabel);
            make.right.equalTo(self).offset(-LEFT_RIGHT_MARGIN);
        }];
        [self.buyLine4 mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.buyStartDateValue.mas_bottom).offset(15);
            make.left.equalTo(self.buyStartDateLabel);
            make.right.equalTo(self.buyStartDateValue);
            make.height.equalTo(@1);
        }];
        [self.buyEndDateLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.buyStartDateLabel);
            make.left.equalTo(self.buyLine4);
        }];
        [self.buyEndDateValue mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.buyEndDateLabel);
            make.right.equalTo(self.buyLine4);
            make.bottom.equalTo(self).offset(-20);
        }];
    } else {
        [self.buyStartDateLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).offset(15);
            make.left.equalTo(self).offset(LEFT_RIGHT_MARGIN);
        }];
        [self.buyStartDateValue mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.buyStartDateLabel);
            make.right.equalTo(self).offset(-LEFT_RIGHT_MARGIN);
        }];
        [self.buyLine4 mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.buyStartDateValue.mas_bottom).offset(15);
            make.left.equalTo(self.buyStartDateLabel);
            make.right.equalTo(self.buyStartDateValue);
            make.height.equalTo(@1);
        }];
        [self.buyEndDateLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.buyLine4.mas_bottom).offset(15);
            make.left.equalTo(self.buyLine4);
        }];
        [self.buyEndDateValue mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.buyEndDateLabel);
            make.right.equalTo(self.buyLine4);
            make.bottom.equalTo(self).offset(-15);
        }];
    }
}
/**
 懒加载
 
 @return buyEndDateValue
 */
- (UILabel *)buyEndDateValue {
    if (!_buyEndDateValue) {
        _buyEndDateValue = [[UILabel alloc] init];
        _buyEndDateValue.font = FONT_TEXT;
        _buyEndDateValue.textColor = COLOR_TEXT;
    }
    return _buyEndDateValue;
}
/**
 懒加载
 
 @return buyEndDateLabel
 */
- (UILabel *)buyEndDateLabel {
    if (!_buyEndDateLabel) {
        _buyEndDateLabel = [[UILabel alloc] init];
        _buyEndDateLabel.font = FONT_TEXT;
        _buyEndDateLabel.textColor = COLOR_TEXT;
        _buyEndDateLabel.text = @"到期日";
    }
    return _buyEndDateLabel;
}
/**
 懒加载
 
 @return buyLine4
 */
- (UIView *)buyLine4 {
    if (!_buyLine4) {
        _buyLine4 = [[UIView alloc] init];
        _buyLine4.backgroundColor = COLOR_LINE;
    }
    return _buyLine4;
}
/**
 懒加载
 
 @return buyStartDateValue
 */
- (UILabel *)buyStartDateValue {
    if (!_buyStartDateValue) {
        _buyStartDateValue = [[UILabel alloc] init];
        _buyStartDateValue.font = FONT_TEXT;
        _buyStartDateValue.textColor = COLOR_TEXT;
    }
    return _buyStartDateValue;
}
/**
 懒加载
 
 @return buyStartDateLabel
 */
- (UILabel *)buyStartDateLabel {
    if (!_buyStartDateLabel) {
        _buyStartDateLabel = [[UILabel alloc] init];
        _buyStartDateLabel.font = FONT_TEXT;
        _buyStartDateLabel.textColor = COLOR_TEXT;
        _buyStartDateLabel.text = @"计息日";
    }
    return _buyStartDateLabel;
}

@end
