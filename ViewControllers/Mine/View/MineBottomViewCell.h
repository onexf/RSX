//
//  MineBottomViewCell.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/1.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MineBottomViewCell : UIView

/** 参数 */
@property(nonatomic, strong) NSDictionary *cellData;

/** subText */
@property(nonatomic, copy) NSString *subText;

@end
