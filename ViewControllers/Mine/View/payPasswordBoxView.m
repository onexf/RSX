//
//  payPasswordBoxView.m
//  GoldCatBank
//
//  Created by Sunny on 2017/9/12.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "payPasswordBoxView.h"
#define kDotCount 6.0
#define kDotSize CGSizeMake (15, 15) //密码点的大小
@interface payPasswordBoxView()<UITextFieldDelegate>

@property(nonatomic,strong)UILabel *titleLab;
@property(nonatomic,strong)UIImageView *bottomImageView;
@property(nonatomic,strong)NSMutableArray *dotArray;

@end

@implementation payPasswordBoxView

-(id)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    
    if (self)
    {
       
        [self addSubview:self.titleLab];
        [self addSubview:self.bottomImageView];
        [self addSubview:self.inputTxf];
        
        
        [self initPwdTextField];

    }
    
    return self;
}

//
//+(id)initPasswordViewWithFrame:(CGRect)frame
//{
//    payPasswordBoxView *passwordView=[[self alloc]initWithFrame:frame];
//    
//    return passwordView;
//}
- (void)initPwdTextField
{
    //每个密码输入框的宽度
    CGFloat width=self.bottomImageView.ct_width/kDotCount;
    CGFloat height=self.bottomImageView.ct_height;
    
    //生成中间的点
    for (int i = 0; i < kDotCount; i++) {
        UILabel *dotView = [[UILabel alloc] initWithFrame:CGRectMake((width/2.0-kDotSize.width/2.0)+width*i,height/2.0-kDotSize.height/2.0+5,kDotSize.width,kDotSize.height)];
//        dotView.backgroundColor = [UIColor blackColor];
//        dotView.layer.cornerRadius = kDotSize.width / 2.0f;
//        dotView.clipsToBounds = YES;
        dotView.text=@"*";
        dotView.font=[UIFont boldSystemFontOfSize:20];
        dotView.textColor=COLOR_SUBTEXT;
        dotView.hidden = YES; //先隐藏
        [self.bottomImageView addSubview:dotView];
        //把创建的黑色点加入到数组中
        [self.dotArray addObject:dotView];
    }
    
}

-(void)setTitleFont:(UIFont *)titleFont
{
    _titleFont=titleFont;
    self.titleLab.font=titleFont;
}

-(void)setTitleColor:(UIColor *)titleColor
{
    _titleColor=titleColor;
    self.titleLab.textColor=titleColor;
    
}

-(void)setTitle:(NSString *)title
{
    _title=title;
    self.titleLab.text=title;
}

-(NSMutableArray *)dotArray
{
    if (!_dotArray)
    {
        _dotArray=[NSMutableArray array];
    }
    
    return _dotArray;
}

-(UILabel *)titleLab
{
    if (!_titleLab)
    {
    
        _titleLab=[[UILabel alloc]initWithFrame:CGRectMake(10, 0, self.ct_width-20, 30)];
    
    }
    
    return _titleLab;
}


-(UIImageView *)bottomImageView
{
    if (!_bottomImageView)
    {
        UIImage *image=[UIImage imageNamed:@"password_boxIcon"];
        _bottomImageView=[[UIImageView alloc]initWithFrame:CGRectMake(_titleLab.ct_left, _titleLab.ct_bottom, _titleLab.ct_width,_titleLab.ct_width*(image.size.height/image.size.width))];
        _bottomImageView.image=image;
    
        
    }
    
    return _bottomImageView;
}

-(UITextField *)inputTxf
{
    if (!_inputTxf)
    {
        UIImage *image=[UIImage imageNamed:@"password_boxIcon"];
        _inputTxf=[[UITextField alloc]initWithFrame:CGRectMake(_bottomImageView.ct_left, _titleLab.ct_bottom, _titleLab.ct_width,_titleLab.ct_width*(image.size.height/image.size.width))];
        _inputTxf.backgroundColor = [UIColor whiteColor];
        //输入的文字颜色为白色
        _inputTxf.textColor = [UIColor clearColor];
        //输入框光标的颜色为白色
        _inputTxf.tintColor = [UIColor clearColor];
        _inputTxf.font=FONT(16);
        _inputTxf.backgroundColor=[UIColor clearColor];
        _inputTxf.delegate = self;
        _inputTxf.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _inputTxf.keyboardType = UIKeyboardTypeNumberPad;
        [_inputTxf addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];

    }
    
    return _inputTxf;
}


#pragma mark--UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSLog(@"变化%@", string);
    if([string isEqualToString:@"\n"]) {
        //按回车关闭键盘
        [textField resignFirstResponder];
        return NO;
    } else if(string.length == 0) {
        //判断是不是删除键
        return YES;
    }
    else if(textField.text.length >= kDotCount) {
        //输入的字符个数大于6，则无法继续输入，返回NO表示禁止输入
        NSLog(@"输入的字符个数大于6，忽略输入");
        return NO;
    } else {
        return YES;
    }
}

/**
 *  清除密码
 */
//- (void)clearUpPassword
//{
//    self.inputTxf.text = @"";
//    [self textFieldDidChange:self.inputTxf];
//}

/**
 *  重置显示的点
 */
- (void)textFieldDidChange:(UITextField *)textField
{
  
    NSLog(@"%@", textField.text);
    for (UIView *dotView in self.dotArray) {
        dotView.hidden = YES;
    }
    for (int i = 0; i < textField.text.length; i++) {
        ((UIView *)[self.dotArray objectAtIndex:i]).hidden = NO;
    }
    if (textField.text.length == kDotCount) {
        NSLog(@"输入完毕");
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{

    self.bottomImageView.image=[UIImage imageNamed:@"Password_redBoxIcon"];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
     self.bottomImageView.image=[UIImage imageNamed:@"password_boxIcon"];
}
@end
