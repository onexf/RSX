//
//  BuyTopView.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/10/28.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "BuyTopView.h"
#import "RecordDetailModel.h"

@interface BuyTopView ()
/** 状态 */
@property(nonatomic, strong) UILabel *buyStatusLabel;
/** typeLabel */
@property(nonatomic, strong) UILabel *buyTypeLabel;
/** 数量 */
@property(nonatomic, strong) UILabel *buyCountLabel;
/** 名称 */
@property(nonatomic, strong) UILabel *buyNameLabel;
/** 价格 */
@property(nonatomic, strong) UILabel *buyPriceLabel;
/** 分割线 */
@property(nonatomic, strong) UIView *buySepLine;
/** 订单总额 */
@property(nonatomic, strong) UILabel *buyTotal;
/** 总额 */
@property(nonatomic, strong) UILabel *buyTotalValue;

@end

@implementation BuyTopView


- (void)setModel:(RecordDetailModel *)model {
    _model = model;
    self.buyStatusLabel.text = model.orderStatusString;
    self.buyTypeLabel.text = model.logTypeString;
    NSString *countString = [NSString stringWithFormat:@"克重：%@克", model.orderGold];
    NSMutableAttributedString *attrS = [[NSMutableAttributedString alloc] initWithString:countString];
    [attrS addAttribute:NSForegroundColorAttributeName value:COLOR_TEXT range:NSMakeRange(0, 3)];
    self.buyCountLabel.attributedText = attrS;
    self.buyNameLabel.text = model.productName;
    self.buyPriceLabel.text = [NSString stringWithFormat:@"交易金价：%@元/克", model.rtgp];
    self.buyTotalValue.text = [NSString stringWithFormat:@"%@元", model.orderAmount];
    if (model.logType == 2) {
        self.buyTotal.text = @"卖出总额";
    }
    if (model.logType == 7) {
        self.buyPriceLabel.hidden = YES;
        self.buyNameLabel.text = model.productName;
        self.buyTotal.text = @"订单总额";//(model.isGoldProduct == 0) ?  : @"订单总量";
        self.buyTotalValue.text = [NSString stringWithFormat:@"%@元", model.orderAmount];
    }
    
//    self.buyStatusLabel.textColor = model.statusColor;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = COLOR_WHITE;
        [self addSubview:self.buyStatusLabel];
        [self addSubview:self.buyTypeLabel];
        [self addSubview:self.buyCountLabel];
        [self addSubview:self.buyNameLabel];
        [self addSubview:self.buyPriceLabel];
        [self addSubview:self.buySepLine];
        [self addSubview:self.buyTotal];
        [self addSubview:self.buyTotalValue];
    }
    return self;
}
- (void)layoutSubviews {
    [super layoutSubviews];
    [self.buyStatusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(10);
        make.centerX.equalTo(self);
    }];
    [self.buyTypeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.buyStatusLabel.mas_bottom).offset(10);
        make.left.equalTo(self).offset(LEFT_RIGHT_MARGIN);
    }];
    [self.buyCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.buyTypeLabel);
        make.right.equalTo(self).offset(-LEFT_RIGHT_MARGIN);
    }];
    [self.buyNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.buyTypeLabel.mas_bottom).offset(10);
        make.left.equalTo(self.buyTypeLabel);
    }];
    [self.buyPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.buyNameLabel);
        make.right.equalTo(self.buyCountLabel);
    }];
    [self.buySepLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.buyNameLabel.mas_bottom).offset(10);
        make.left.equalTo(self.buyNameLabel);
        make.right.equalTo(self.buyPriceLabel);
        make.height.equalTo(@1);
    }];
    [self.buyTotal mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.buySepLine.mas_bottom).offset(10);
        make.left.equalTo(self.buySepLine);
    }];
    [self.buyTotalValue mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.buyTotal);
        make.right.equalTo(self.buySepLine);
        make.bottom.equalTo(self).offset(-10);
    }];
}
/**
 懒加载
 
 @return buyTotalValue
 */
- (UILabel *)buyTotalValue {
    if (!_buyTotalValue) {
        _buyTotalValue = [[UILabel alloc] init];
        _buyTotalValue.font = FONT_TEXT;
        _buyTotalValue.textColor = COLOR_SUBTEXT;
    }
    return _buyTotalValue;
}
/**
 懒加载
 
 @return buyTotal
 */
- (UILabel *)buyTotal {
    if (!_buyTotal) {
        _buyTotal = [[UILabel alloc] init];
        _buyTotal.font = FONT_TEXT;
        _buyTotal.textColor = COLOR_SUBTEXT;
        _buyTotal.text = @"订单总额";
    }
    return _buyTotal;
}
/**
 懒加载
 
 @return buySepLine
 */
- (UIView *)buySepLine {
    if (!_buySepLine) {
        _buySepLine = [[UIView alloc] init];
        _buySepLine.backgroundColor = COLOR_LINE;
    }
    return _buySepLine;
}
/**
 懒加载
 
 @return buyPriceLabel
 */
- (UILabel *)buyPriceLabel {
    if (!_buyPriceLabel) {
        _buyPriceLabel = [[UILabel alloc] init];
        _buyPriceLabel.textColor = COLOR_SUBTEXT;
        _buyPriceLabel.font = FONT_TEXT;
    }
    return _buyPriceLabel;
}
/**
 懒加载
 
 @return buyNameLabel
 */
- (UILabel *)buyNameLabel {
    if (!_buyNameLabel) {
        _buyNameLabel = [[UILabel alloc] init];
        _buyNameLabel.textColor = COLOR_SUBTEXT;
        _buyNameLabel.font = FONT_TEXT;
    }
    return _buyNameLabel;
}
/**
 懒加载
 
 @return buyCountLabel
 */
- (UILabel *)buyCountLabel {
    if (!_buyCountLabel) {
        _buyCountLabel = [[UILabel alloc] init];
        _buyCountLabel.textColor = COLOR_Red;
        _buyCountLabel.font = FONT_TEXT;
    }
    return _buyCountLabel;
}
/**
 懒加载
 
 @return buyTypeLabel
 */
- (UILabel *)buyTypeLabel {
    if (!_buyTypeLabel) {
        _buyTypeLabel = [[UILabel alloc] init];
        _buyTypeLabel.textColor = COLOR_TEXT;
        _buyTypeLabel.font = FONT_TEXT;
    }
    return _buyTypeLabel;
}
/**
 懒加载
 
 @return buyStatusLabel
 */
- (UILabel *)buyStatusLabel {
    if (!_buyStatusLabel) {
        _buyStatusLabel = [[UILabel alloc] init];
        _buyStatusLabel.textColor = COLOR_Red;
        _buyStatusLabel.font = FONT_TITLE;
    }
    return _buyStatusLabel;
}

@end
