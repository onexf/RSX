//
//  MineOtherAssetsView.m
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/9/4.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "MineOtherAssetsView.h"
#import "AssetsCell.h"
#import "AssetsTitleCell.h"
#import "OtherAssetsModel.h"
@interface MineOtherAssetsView ()
/** 其他资产 */
@property(nonatomic, strong) AssetsTitleCell *otherAssetsTitleCell;
/** 零钱罐 */
@property(nonatomic, strong) AssetsTitleCell *lingqianguanTitleCell;
/** 收益 */
@property(nonatomic, strong) AssetsCell *lingqianguanIncomeCell;

/** 存钱罐 */
@property(nonatomic, strong) AssetsTitleCell *cunqianguanTitleCell;
/** shouyi */
@property(nonatomic, strong) AssetsCell *cunqianguanIncomeCell;

@end

@implementation MineOtherAssetsView


- (void)setOtherAssetsData:(OtherAssetsModel *)otherAssetsData {
    _otherAssetsData = otherAssetsData;
    self.otherAssetsTitleCell.subText = otherAssetsData.otherAssets;
    self.lingqianguanTitleCell.subText = otherAssetsData.piggyBankValue;
    self.lingqianguanIncomeCell.subText = otherAssetsData.totalPBProfit;
    self.cunqianguanTitleCell.subText = otherAssetsData.saveBankValue;
    self.cunqianguanIncomeCell.subText = otherAssetsData.totalSBProfit;
}
- (void)layoutSubviews {
    [super layoutSubviews];
    [self.otherAssetsTitleCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(10);
        make.left.right.equalTo(self);
        make.height.equalTo(@(NavBarHeight));
    }];
    [self.lingqianguanTitleCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.otherAssetsTitleCell.mas_bottom).offset(10);
        make.left.right.height.equalTo(self.otherAssetsTitleCell);
    }];
    [self.lingqianguanIncomeCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lingqianguanTitleCell.mas_bottom);
        make.left.right.equalTo(self.lingqianguanTitleCell);
        make.height.equalTo(@(NORMAL_CELL_HEIGHT));
    }];
    [self.cunqianguanTitleCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lingqianguanIncomeCell.mas_bottom).offset(10);
        make.left.right.height.equalTo(self.lingqianguanTitleCell);
    }];
    [self.cunqianguanIncomeCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.cunqianguanTitleCell.mas_bottom);
        make.left.right.height.equalTo(self.lingqianguanIncomeCell);
    }];
    
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 283)]) {
        self.backgroundColor = COLOR_TBBACK;
        [self addSubview:self.otherAssetsTitleCell];
        [self addSubview:self.lingqianguanTitleCell];
        [self addSubview:self.lingqianguanIncomeCell];
        [self addSubview:self.cunqianguanTitleCell];
        [self addSubview:self.cunqianguanIncomeCell];
    }
    return self;
}
/**
 懒加载
 
 @return 收益
 */
- (AssetsCell *)cunqianguanIncomeCell {
    if (!_cunqianguanIncomeCell) {
        _cunqianguanIncomeCell = [[AssetsCell alloc] init];
        _cunqianguanIncomeCell.cellType = AssetsCellTypeIncomeCunQian;
    }
    return _cunqianguanIncomeCell;
}

/**
 懒加载
 
 @return 存钱罐
 */
- (AssetsTitleCell *)cunqianguanTitleCell {
    if (!_cunqianguanTitleCell) {
        _cunqianguanTitleCell = [[AssetsTitleCell alloc] init];
        _cunqianguanTitleCell.cellType = AssetsTitleCellTypeCunQianGuan;
    }
    return _cunqianguanTitleCell;
}

/**
 懒加载
 
 @return 收益
 */
- (AssetsCell *)lingqianguanIncomeCell {
    if (!_lingqianguanIncomeCell) {
        _lingqianguanIncomeCell = [[AssetsCell alloc] init];
        _lingqianguanIncomeCell.cellType = AssetsCellTypeIncomeLingQian;
    }
    return _lingqianguanIncomeCell;
}

/**
 懒加载
 
 @return 零钱罐
 */
- (AssetsTitleCell *)lingqianguanTitleCell {
    if (!_lingqianguanTitleCell) {
        _lingqianguanTitleCell = [[AssetsTitleCell alloc] init];
        _lingqianguanTitleCell.cellType = AssetsTitleCellTypeLingQianGuan;
    }
    return _lingqianguanTitleCell;
}

/**
 懒加载
 
 @return 其他资产
 */
- (AssetsTitleCell *)otherAssetsTitleCell {
    if (!_otherAssetsTitleCell) {
        _otherAssetsTitleCell = [[AssetsTitleCell alloc] init];
        _otherAssetsTitleCell.cellType = AssetsTitleCellTypeOtherAssets;
    }
    return _otherAssetsTitleCell;
}

@end
