//
//  BuyTopView.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/10/28.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RecordDetailModel;
@interface BuyTopView : UIView

/** 数据 */
@property(nonatomic, strong) RecordDetailModel *model;
@end
