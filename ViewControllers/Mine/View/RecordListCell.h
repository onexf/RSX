//
//  RecordListCell.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/10/26.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>
@class GlodTradeModel;
@interface RecordListCell : UITableViewCell

/** model */
@property(nonatomic, strong) GlodTradeModel *model;

+ (instancetype)cellWithTableview:(UITableView *)tableView;

@end
