//
//  BuyDaoQiBottomView.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/10/30.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "BuyDaoQiBottomView.h"
#import "RecordDetailModel.h"
@interface BuyDaoQiBottomView ()

/** 优惠卡券 */
@property(nonatomic, strong) UILabel *buyCouponLabel;
/** 优惠卡券值 */
@property(nonatomic, strong) UILabel *buyCouponValue;
/** buyLine */
@property(nonatomic, strong) UIView *buyLine1;
/** 实际支付 */
@property(nonatomic, strong) UILabel *buyTruePayLabel;
/** 实际支付值 */
@property(nonatomic, strong) UILabel *buyTruePayValue;
/** buyLine */
@property(nonatomic, strong) UIView *buyLine2;
/** 余额支付 */
@property(nonatomic, strong) UILabel *buyBalanceLabel;
/** 余额支付值 */
@property(nonatomic, strong) UILabel *buyBalanceValue;


@end

@implementation BuyDaoQiBottomView


- (void)setModel:(RecordDetailModel *)model {
    _model = model;
    if (model.logType == 5) {
        self.buyCouponLabel.text = @"提现金额";
        self.buyTruePayLabel.text = @"手续费";
        self.buyBalanceLabel.text = @"到账金额";
        self.buyCouponValue.text = [NSString stringWithFormat:@"%@元", model.orderAmount];
        if (model.orderStatus == 2 || model.orderStatus == 4 || model.orderStatus == 6) {
            self.buyTruePayValue.text = [NSString stringWithFormat:@"%@元", @"0.00"];
            self.buyBalanceValue.text = [NSString stringWithFormat:@"%@元", @"0.00"];
        } else {
            self.buyTruePayValue.text = [NSString stringWithFormat:@"%@元", model.poundage];
            self.buyBalanceValue.text = [NSString stringWithFormat:@"%@元", model.payAmount];
        }
    } else {
        self.buyCouponValue.text = [NSString stringWithFormat:@"%@元", model.orderAmount];
        self.buyTruePayValue.text = [NSString stringWithFormat:@"%@元", model.totalProfit];
        self.buyBalanceValue.text = [NSString stringWithFormat:@"%.2f元", (model.orderAmount.floatValue + model.totalProfit.floatValue)];
    }
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = COLOR_WHITE;
        [self addSubview:self.buyCouponLabel];
        [self addSubview:self.buyCouponValue];
        [self addSubview:self.buyLine1];
        [self addSubview:self.buyTruePayLabel];
        [self addSubview:self.buyTruePayValue];
        [self addSubview:self.buyLine2];
        [self addSubview:self.buyBalanceLabel];
        [self addSubview:self.buyBalanceValue];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.buyCouponLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(15);
        make.left.equalTo(self).offset(LEFT_RIGHT_MARGIN);
    }];
    [self.buyCouponValue mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.buyCouponLabel);
        make.right.equalTo(self).offset(-LEFT_RIGHT_MARGIN);
    }];
    [self.buyLine1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.buyCouponValue.mas_bottom).offset(15);
        make.left.equalTo(self.buyCouponLabel);
        make.right.equalTo(self.buyCouponValue);
        make.height.equalTo(@1);
    }];
    [self.buyTruePayLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.buyLine1.mas_bottom).offset(15);
        make.left.equalTo(self.buyLine1);
    }];
    [self.buyTruePayValue mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.buyTruePayLabel);
        make.right.equalTo(self.buyLine1);
    }];
    [self.buyLine2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.buyTruePayValue.mas_bottom).offset(15);
        make.left.right.height.equalTo(self.buyLine1);
    }];
    [self.buyBalanceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.buyLine2.mas_bottom).offset(15);
        make.left.equalTo(self.buyLine2);
    }];
    [self.buyBalanceValue mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.buyBalanceLabel);
        make.right.equalTo(self.buyLine2);
        make.bottom.equalTo(self).offset(-15);
    }];
}

/**
 懒加载
 
 @return buyBalanceValue
 */
- (UILabel *)buyBalanceValue {
    if (!_buyBalanceValue) {
        _buyBalanceValue = [[UILabel alloc] init];
        _buyBalanceValue.font = FONT_TEXT;
        _buyBalanceValue.textColor = COLOR_TEXT;
    }
    return _buyBalanceValue;
}
/**
 懒加载
 
 @return buyBalanceLabel
 */
- (UILabel *)buyBalanceLabel {
    if (!_buyBalanceLabel) {
        _buyBalanceLabel = [[UILabel alloc] init];
        _buyBalanceLabel.textColor = COLOR_TEXT;
        _buyBalanceLabel.font = FONT_TEXT;
        _buyBalanceLabel.text = @"实际到账";
    }
    return _buyBalanceLabel;
}
/**
 懒加载
 
 @return buyLine2
 */
- (UIView *)buyLine2 {
    if (!_buyLine2) {
        _buyLine2 = [[UIView alloc] init];
        _buyLine2.backgroundColor = COLOR_LINE;
    }
    return _buyLine2;
}
/**
 懒加载
 
 @return buyTruePayValue
 */
- (UILabel *)buyTruePayValue {
    if (!_buyTruePayValue) {
        _buyTruePayValue = [[UILabel alloc] init];
        _buyTruePayValue.textColor = COLOR_TEXT;
        _buyTruePayValue.font = FONT_TEXT;
    }
    return _buyTruePayValue;
}
/**
 懒加载
 
 @return buyTruePayLabel
 */
- (UILabel *)buyTruePayLabel {
    if (!_buyTruePayLabel) {
        _buyTruePayLabel = [[UILabel alloc] init];
        _buyTruePayLabel.font = FONT_TEXT;
        _buyTruePayLabel.textColor = COLOR_TEXT;
        _buyTruePayLabel.text = @"到期收益";
    }
    return _buyTruePayLabel;
}
/**
 懒加载
 
 @return buyLine1
 */
- (UIView *)buyLine1 {
    if (!_buyLine1) {
        _buyLine1 = [[UIView alloc] init];
        _buyLine1.backgroundColor = COLOR_LINE;
    }
    return _buyLine1;
}
/**
 懒加载
 
 @return buyCouponValue
 */
- (UILabel *)buyCouponValue {
    if (!_buyCouponValue) {
        _buyCouponValue = [[UILabel alloc] init];
        _buyCouponValue.font = FONT_TEXT;
        _buyCouponValue.textColor = COLOR_TEXT;
    }
    return _buyCouponValue;
}
/**
 懒加载
 
 @return buyCouponLabel
 */
- (UILabel *)buyCouponLabel {
    if (!_buyCouponLabel) {
        _buyCouponLabel = [[UILabel alloc] init];
        _buyCouponLabel.font = FONT_TEXT;
        _buyCouponLabel.textColor = COLOR_TEXT;
        _buyCouponLabel.text = @"到期总额";
    }
    return _buyCouponLabel;
}

@end
