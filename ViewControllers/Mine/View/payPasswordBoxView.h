//
//  payPasswordBoxView.h
//  GoldCatBank
//
//  Created by Sunny on 2017/9/12.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface payPasswordBoxView : UIView
@property(nonatomic,strong)NSString *title;
@property(nonatomic,strong)UIFont *titleFont;
@property(nonatomic,strong)UIColor *titleColor;
@property(nonatomic,strong)UITextField *inputTxf;

@end
