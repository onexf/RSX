//
//  SettingNoticeView.m
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/9/14.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "SettingNoticeView.h"
#import "UIView+Extension.h"
#import "AllNetWorkRquest.h"
#import "CWCalendarLabel.h"

@interface SettingNoticeView ()<UITextFieldDelegate>
/** warn */
@property(nonatomic, strong) UILabel *warnLabel;
/** white */
@property(nonatomic, strong) UIView *whiteBackView;
/** label1 */
@property(nonatomic, strong) UILabel *leftLabel;
/** 金价 */
@property(nonatomic, strong) CWCalendarLabel *priceLabel;
/** label1 */
@property(nonatomic, strong) UILabel *rightLabel;
/** 刷新 */
@property(nonatomic, strong) UIImageView *refImageView;
/** highLabel */
@property(nonatomic, strong) UILabel *highLabel;
/** highSwitch */
@property(nonatomic, strong) UISwitch *highSwitch;
/** highLabel */
@property(nonatomic, strong) UILabel *lowLabel;

/** highSwitch */
@property(nonatomic, strong) UISwitch *lowSwitch;

@end


@implementation SettingNoticeView
#pragma mark - 事件处理
- (void)switchAction:(UISwitch *)sender {
    if (sender == self.highSwitch) {
        [[NSUserDefaults standardUserDefaults] setBool:sender.isOn forKey:@"highSwitch"];
        UDSave(self.highTextField.text, @"highTextField");
        if (self.highTextField.text.length <= 0) {
            
        } else {
            
        }
    } else {
        [[NSUserDefaults standardUserDefaults] setBool:sender.isOn forKey:@"lowSwitch"];
        UDSave(self.highTextField.text, @"lowTextField");
        if (self.lowTextField.text.length <= 0) {
            
        } else {
            
        }
    }
}
- (void)refImageViewDidTap {
    [self.refImageView jk_rotateByAngle:180 duration:0.77 autoreverse:NO repeatCount:1 timingFunction:[CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionLinear]];
    [self getCurrentGoldPrice];
}
#pragma mark - 数据请求
- (void)getCurrentGoldPrice {
    __weak typeof(self) weakSelf = self;
    [AllNetWorkRquest allListRefreshGoldPriceWithResponse:^(NSString *goldPrice, NSString *errMsg) {
        if (!errMsg) {
            [weakSelf.priceLabel showNextText:goldPrice withDirection:CWCalendarLabelScrollToTop];
        }
    }];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.warnLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.centerX.equalTo(self);
    }];
    [self.whiteBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.warnLabel.mas_bottom).offset(17);
        make.left.right.equalTo(self);
        make.bottom.equalTo(self).offset(-10);
    }];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.whiteBackView).offset(10);
        make.centerX.equalTo(self.whiteBackView).offset(-10);
    }];
    [self.leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.priceLabel);
        make.right.equalTo(self.priceLabel.mas_left).offset(-3);
    }];
    [self.rightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.priceLabel);
        make.left.equalTo(self.priceLabel.mas_right).offset(4);
    }];
    [self.refImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.priceLabel);
        make.left.equalTo(self.rightLabel.mas_right).offset(7);
    }];
    [self.highLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.leftLabel.mas_bottom).offset(10);
        make.left.equalTo(self.whiteBackView).offset(40);
    }];
    [self.highTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.highLabel.mas_bottom);
        make.left.equalTo(self.highLabel);
        make.right.equalTo(self.highSwitch.mas_left);
        make.height.equalTo(@(NORMAL_CELL_HEIGHT));
    }];
    [self.highSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.highTextField);
        make.right.equalTo(self.whiteBackView).offset(-LEFT_RIGHT_MARGIN);
    }];
    [self.lowLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.highTextField.mas_bottom);
        make.left.equalTo(self.highLabel);
    }];
    [self.lowTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lowLabel.mas_bottom);
        make.left.right.equalTo(self.highTextField);
        make.height.equalTo(self.highTextField);
    }];
    [self.lowSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.lowTextField);
        make.right.equalTo(self.highSwitch);
    }];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.warnLabel];
        [self addSubview:self.whiteBackView];
        [self getCurrentGoldPrice];
    }
    return self;
}
/**
 懒加载
 
 @return 白色背景
 */
- (UIView *)whiteBackView {
    if (!_whiteBackView) {
        _whiteBackView = [[UIView alloc] init];
        _whiteBackView.backgroundColor = COLOR_WHITE;
        [_whiteBackView addSubview:self.leftLabel];
        [_whiteBackView addSubview:self.priceLabel];
        [_whiteBackView addSubview:self.rightLabel];
        [_whiteBackView addSubview:self.refImageView];
        [_whiteBackView addSubview:self.highLabel];
        [_whiteBackView addSubview:self.highTextField];
        [_whiteBackView addSubview:self.highSwitch];
        [_whiteBackView addSubview:self.lowLabel];
        [_whiteBackView addSubview:self.lowSwitch];
        [_whiteBackView addSubview:self.lowTextField];
    }
    return _whiteBackView;
}

/**
 懒加载
 
 @return highSwitch
 */
- (UISwitch *)lowSwitch {
    if (!_lowSwitch) {
        _lowSwitch = [[UISwitch alloc] init];
        [_lowSwitch addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
        _lowSwitch.on = [[NSUserDefaults standardUserDefaults] boolForKey:@"lowSwitch"];
        _lowSwitch.onTintColor = COLOR_MAIN;
    }
    return _lowSwitch;
}
/**
 懒加载
 
 @return highText
 */
- (UITextField *)lowTextField {
    if (!_lowTextField) {
        _lowTextField = [[UITextField alloc] init];
        _lowTextField.font = FONT_TEXT;
        _lowTextField.textColor = COLOR_Red;
        _lowTextField.placeholder = @"金价低于或等于";
        _lowTextField.keyboardType = UIKeyboardTypeDecimalPad;
        _lowTextField.jk_maxLength = 6;
        _lowTextField.delegate = self;
        _lowTextField.text = UDGET(@"lowTextField");
    }
    return _lowTextField;
}
/**
 懒加载
 
 @return 高价位提醒
 */
- (UILabel *)lowLabel {
    if (!_lowLabel) {
        _lowLabel = [[UILabel alloc] init];
        _lowLabel.text = @"低价位提醒";
        _lowLabel.textColor = COLOR_TEXT;
        _lowLabel.font = FONT_TEXT;
    }
    return _lowLabel;
}

/**
 懒加载
 
 @return highSwitch
 */
- (UISwitch *)highSwitch {
    if (!_highSwitch) {
        _highSwitch = [[UISwitch alloc] init];
        [_highSwitch addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
        _highSwitch.on = [[NSUserDefaults standardUserDefaults] boolForKey:@"highSwitch"];
        _highSwitch.onTintColor = COLOR_MAIN;
    }
    return _highSwitch;
}
/**
 懒加载
 
 @return highText
 */
- (UITextField *)highTextField {
    if (!_highTextField) {
        _highTextField = [[UITextField alloc] init];
        _highTextField.font = FONT_TEXT;
        _highTextField.textColor = COLOR_Red;
        _highTextField.placeholder = @"金价高于或等于";
        _highTextField.keyboardType = UIKeyboardTypeDecimalPad;
        _highTextField.jk_maxLength = 6;
        _highTextField.delegate = self;
        _highTextField.text = UDGET(@"highTextField");
    }
    return _highTextField;
}
/**
 懒加载
 
 @return 高价位提醒
 */
- (UILabel *)highLabel {
    if (!_highLabel) {
        _highLabel = [[UILabel alloc] init];
        _highLabel.text = @"高价位提醒";
        _highLabel.textColor = COLOR_TEXT;
        _highLabel.font = FONT_TEXT;
    }
    return _highLabel;
}
/**
 懒加载
 
 @return 刷新
 */
- (UIImageView *)refImageView {
    if (!_refImageView) {
        _refImageView = [[UIImageView alloc] init];
        _refImageView.image = [UIImage imageNamed:@"icon_shuanxin"];
        [_refImageView addTapAction:@selector(refImageViewDidTap) target:self];
    }
    return _refImageView;
}
/**
 懒加载
 
 @return right
 */
- (UILabel *)rightLabel {
    if (!_rightLabel) {
        _rightLabel = [[UILabel alloc] init];
        _rightLabel.text = @"元/克";
        _rightLabel.textColor = COLOR_SUBTEXT;
        _rightLabel.font = FONT_TEXT;
    }
    return _rightLabel;
}
/**
 懒加载
 
 @return price
 */
- (CWCalendarLabel *)priceLabel {
    if (!_priceLabel) {
        _priceLabel = [[CWCalendarLabel alloc] init];
        _priceLabel.text = @"";
        _priceLabel.textColor = COLOR_Red;
        _priceLabel.font = FONT_TEXT;
    }
    return _priceLabel;
}
/**
 懒加载
 
 @return leftLabel
 */
- (UILabel *)leftLabel {
    if (!_leftLabel) {
        _leftLabel = [[UILabel alloc] init];
        _leftLabel.textColor = COLOR_SUBTEXT;
        _leftLabel.text = @"当前金价";
        _leftLabel.font = FONT_TEXT;
    }
    return _leftLabel;
}
/**
 懒加载
 
 @return warn
 */
- (UILabel *)warnLabel {
    if (!_warnLabel) {
        _warnLabel = [[UILabel alloc] init];
        _warnLabel.text = @"当金价到达您设置的点位时，招金猫为您提醒消息";
        _warnLabel.textColor = COLOR_SUBTEXT;
        _warnLabel.font = FONT_SUBTEXT;
    }
    return _warnLabel;
}
@end
