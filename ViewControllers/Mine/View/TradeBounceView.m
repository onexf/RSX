//
//  TradeBounceView.m
//  GoldCatBank
//
//  Created by Sunny on 2017/9/5.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "TradeBounceView.h"
@interface TradeBounceView()

@property(nonatomic,strong)UIButton *selectBtn;
@property(nonatomic,assign)OrderType type;

@end

@implementation TradeBounceView

-(id)initWithFrame:(CGRect)frame withSelect:(OrderType)type
{
    self=[super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
        _type=type;
        
    }
    return self;
}

-(void)setTitleArray:(NSArray *)titleArray
{
    _titleArray=titleArray;
    [self initViewsWithTitleArray:titleArray];
    
}

-(void)initViewsWithTitleArray:(NSArray *)titleArray{
    
    UIView *backView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0)];
    backView.backgroundColor=[UIColor whiteColor];
    [self addSubview:backView];
    
    CGFloat btnWidth=(SCREEN_WIDTH-20*4)/3.0;
    CGFloat btnHeight=30;
    CGFloat btnBottom=0.0;
    
    for (int i=0; i<titleArray.count; i++)
    {
        UIButton *titleBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        
        if (i==0)
        {
            titleBtn.frame=CGRectMake(20,30, btnWidth, btnHeight);
        }
        else
        {
            titleBtn.frame=CGRectMake(20+btnWidth+20+(btnWidth+20)*((i-1)%2),30+(btnHeight+20)*((i-1)/2), btnWidth, btnHeight);
        }
        
        [titleBtn setTitle:titleArray[i] forState:UIControlStateNormal];
        titleBtn.tag=100+i;
        UIImage *selectImage=[UIImage imageNamed:@"trade_selectedIcon"];
        UIImage *image=[UIImage imageNamed:@"trade_noSelectedIcon"];
        [titleBtn setBackgroundImage:image forState:UIControlStateNormal];
        [titleBtn setBackgroundImage:selectImage forState:UIControlStateSelected];
        
        [titleBtn setTitleColor:COLOR_WHITE forState:UIControlStateSelected];
        [titleBtn setTitleColor:COLOR_GLODTEXT forState:UIControlStateNormal];
        titleBtn.titleLabel.font=FONT(14);
        
        [titleBtn addTarget:self action:@selector(titleBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [backView addSubview:titleBtn];
        
        if (i==self.type+1)
        {
            titleBtn.selected=YES;
            self.selectBtn=titleBtn;
        }
        
        if (i==titleArray.count-1)
        {
            btnBottom=titleBtn.ct_bottom;
        }
    }
    
    backView.frame=CGRectMake(0, 0, SCREEN_WIDTH, btnBottom+30);
    
    
}

-(void)show{
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.frame=CGRectMake(0,self.showTop, self.frame.size.width,  self.frame.size.height);
        
    } completion:^(BOOL finished) {
        
        
        
    }];
    
}

-(void)hidden{
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.frame=CGRectMake(0,-SCREEN_HEIGHT, self.frame.size.width, self.frame.size.height);
        
    } completion:^(BOOL finished) {
        
        
        
    }];
    

}


-(void)titleBtnClick:(UIButton *)btn
{
    
    self.selectBtn.selected=NO;
    
    btn.selected=YES;
    self.selectBtn=btn;

    
    if (self.delegate && [self.delegate respondsToSelector:@selector(selectItemWithIndex:)]) {
        //代理存在且有这个transButIndex:方法
        [self.delegate selectItemWithIndex:btn.tag - 100-1];
    }
    
    
}
@end
