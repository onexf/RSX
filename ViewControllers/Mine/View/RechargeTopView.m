//
//  RechargeTopView.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/10/30.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "RechargeTopView.h"
#import "RecordDetailModel.h"
@interface RechargeTopView ()

/** topBg */
@property(nonatomic, strong) UIView *backgroundView;
/** 状态 */
@property(nonatomic, strong) UILabel *buyStatusLabel;
/** typeLabel */
@property(nonatomic, strong) UILabel *buyTypeLabel;
/** 数量 */
@property(nonatomic, strong) UILabel *buyCountLabel;
/** server */
@property(nonatomic, strong) UILabel *serverPhoneLabel;


@end

@implementation RechargeTopView

- (void)setModel:(RecordDetailModel *)model {
    _model = model;
    self.buyStatusLabel.text = model.orderStatusString;
    self.buyTypeLabel.text = model.logTypeString;
    self.buyCountLabel.text = [NSString stringWithFormat:@"金额：%@元", model.orderAmount];
    
//    self.buyStatusLabel.textColor = model.statusColor;

    if (model.logType == 5) {
        self.serverPhoneLabel.hidden = YES;
    }
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = COLOR_TBBACK;
        [self addSubview:self.backgroundView];
        [self addSubview:self.serverPhoneLabel];
    }
    return self;
}
- (void)layoutSubviews {
    [super layoutSubviews];
    if (self.serverPhoneLabel.hidden) {
        [self.backgroundView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        [self.buyStatusLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.backgroundView).offset(10);
            make.centerX.equalTo(self.backgroundView);
        }];
        [self.buyTypeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.buyStatusLabel.mas_bottom).offset(10);
            make.left.equalTo(self.backgroundView).offset(LEFT_RIGHT_MARGIN);
        }];
        [self.buyCountLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.buyTypeLabel);
            make.right.equalTo(self.backgroundView).offset(-LEFT_RIGHT_MARGIN);
            make.bottom.equalTo(self.backgroundView).offset(-15);
        }];
    } else {
        [self.backgroundView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.equalTo(self);
        }];
        [self.buyStatusLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.backgroundView).offset(10);
            make.centerX.equalTo(self.backgroundView);
        }];
        [self.buyTypeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.buyStatusLabel.mas_bottom).offset(10);
            make.left.equalTo(self.backgroundView).offset(LEFT_RIGHT_MARGIN);
        }];
        [self.buyCountLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.buyTypeLabel);
            make.right.equalTo(self.backgroundView).offset(-LEFT_RIGHT_MARGIN);
            make.bottom.equalTo(self.backgroundView).offset(-15);
        }];
        [self.serverPhoneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.backgroundView.mas_bottom).offset(20);
            make.left.right.equalTo(self);
            make.bottom.equalTo(self).offset(-15);
        }];
    }
}
/**
 懒加载
 
 @return 电话
 */
- (UILabel *)serverPhoneLabel {
    if (!_serverPhoneLabel) {
        _serverPhoneLabel = [[UILabel alloc] init];
        _serverPhoneLabel.textColor = COLOR_SUBTEXT;
        _serverPhoneLabel.font = FONT_TEXT;
        _serverPhoneLabel.numberOfLines = 0;
        _serverPhoneLabel.text = @"充值过程出现任何疑问，请致电客服\n400-801-7862\n工作时间 9:00-18:00";
        _serverPhoneLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _serverPhoneLabel;
}
/**
 懒加载
 
 @return buyCountLabel
 */
- (UILabel *)buyCountLabel {
    if (!_buyCountLabel) {
        _buyCountLabel = [[UILabel alloc] init];
        _buyCountLabel.textColor = COLOR_TEXT;
        _buyCountLabel.font = FONT_TEXT;
    }
    return _buyCountLabel;
}
/**
 懒加载
 
 @return buyTypeLabel
 */
- (UILabel *)buyTypeLabel {
    if (!_buyTypeLabel) {
        _buyTypeLabel = [[UILabel alloc] init];
        _buyTypeLabel.textColor = COLOR_TEXT;
        _buyTypeLabel.font = FONT_TEXT;
    }
    return _buyTypeLabel;
}
/**
 懒加载
 
 @return buyStatusLabel
 */
- (UILabel *)buyStatusLabel {
    if (!_buyStatusLabel) {
        _buyStatusLabel = [[UILabel alloc] init];
        _buyStatusLabel.textColor = COLOR_Red;
        _buyStatusLabel.font = FONT_TITLE;
    }
    return _buyStatusLabel;
}
/**
 懒加载
 
 @return 背景
 */
- (UIView *)backgroundView {
    if (!_backgroundView) {
        _backgroundView = [[UIView alloc] init];
        _backgroundView.backgroundColor = COLOR_WHITE;
        [_backgroundView addSubview:self.buyStatusLabel];
        [_backgroundView addSubview:self.buyTypeLabel];
        [_backgroundView addSubview:self.buyCountLabel];
    }
    return _backgroundView;
}

@end
