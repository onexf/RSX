//
//  AssetsTitleCell.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/5.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  资产cell类型
 */
typedef NS_ENUM(NSInteger, AssetsTitleCellType) {
    AssetsTitleCellTypeGold = 0,//黄金资产
    
    AssetsTitleCellTypeZhaoCaiJin,//招财金
    AssetsTitleCellTypeShengCaiJin,//生财金
    
    AssetsTitleCellTypeOtherAssets,//其他资产
    AssetsTitleCellTypeLingQianGuan,//零钱罐
    AssetsTitleCellTypeCunQianGuan//存钱罐
};


@interface AssetsTitleCell : UIView

/** 类型 */
@property(nonatomic ,assign) AssetsTitleCellType cellType;

/** 数据 */
@property(nonatomic, strong) NSString *subText;


@end
