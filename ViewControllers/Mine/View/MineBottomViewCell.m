//
//  MineBottomViewCell.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/1.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "MineBottomViewCell.h"

@interface MineBottomViewCell ()
/** icon */
@property(nonatomic, strong) UIImageView *iconView;
/** title */
@property(nonatomic, strong) UILabel *titleLabel;
/** 箭头 */
@property(nonatomic, strong) UIImageView *arrowImageView;
/** line */
@property(nonatomic, strong) UIView *sepLine;
/** uilabel */
@property(nonatomic, strong) UILabel *countLabel;


@end

@implementation MineBottomViewCell

- (void)setSubText:(NSString *)subText {
    _subText = subText;
    self.countLabel.text = subText;
}

- (void)setCellData:(NSDictionary *)cellData {
    _cellData = cellData;
    self.iconView.image = [UIImage imageNamed:cellData[@"image"]];
    self.titleLabel.text = cellData[@"title"];
    if ([cellData[@"title"] isEqualToString:@"帮助中心"]) {
        self.sepLine.hidden = YES;
    }
    if (isLogin() && [cellData[@"title"] isEqualToString:@"卡券中心"]) {
        self.countLabel.hidden = NO;
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if (isLogin() && [self.titleLabel.text isEqualToString:@"卡券中心"]) {
        self.countLabel.hidden = NO;
    } else {
        self.countLabel.hidden = YES;
    }
    [self.iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(self).offset(LEFT_RIGHT_MARGIN);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(self.iconView.mas_right).offset(10);
    }];
    [self.arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.right.equalTo(self).offset(-LEFT_RIGHT_MARGIN);
    }];
    [self.countLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.right.equalTo(self.arrowImageView.mas_left).offset(-7);
    }];
    [self.sepLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLabel);
        make.bottom.right.equalTo(self);
        make.height.equalTo(@1);
    }];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, NORMAL_CELL_HEIGHT)]) {
        self.backgroundColor = COLOR_WHITE;
        [self addSubview:self.iconView];
        [self addSubview:self.titleLabel];
        [self addSubview:self.arrowImageView];
        [self addSubview:self.sepLine];
        [self addSubview:self.countLabel];
    }
    return self;
}

/**
 懒加载
 
 @return 卡券数量
 */
- (UILabel *)countLabel {
    if (!_countLabel) {
        _countLabel = [[UILabel alloc] init];
        _countLabel.textColor = COLOR_TEXT;
        _countLabel.font = FONT_TEXT;
        _countLabel.text = @"5";
        _countLabel.hidden = YES;
    }
    return _countLabel;
}
/**
 懒加载
 
 @return 分割线
 */
- (UIView *)sepLine {
    if (!_sepLine) {
        _sepLine = [[UIView alloc] init];
        _sepLine.backgroundColor = COLOR_LINE;
    }
    return _sepLine;
}

/**
 懒加载
 
 @return 箭头
 */
- (UIImageView *)arrowImageView {
    if (!_arrowImageView) {
        _arrowImageView = [[UIImageView alloc] init];
        _arrowImageView.image = [UIImage imageNamed:@"icon_nextArrow"];
    }
    return _arrowImageView;
}

/**
 懒加载
 
 @return 标题
 */
- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = COLOR_TEXT;
        _titleLabel.font = FONT_TEXT;
    }
    return _titleLabel;
}

/**
 懒加载
 
 @return icon
 */
- (UIImageView *)iconView {
    if (!_iconView) {
        _iconView = [[UIImageView alloc] init];
    }
    return _iconView;
}


@end
