//
//  WithdrawalsCell.h
//  GoldCatBank
//
//  Created by Sunny on 2017/9/7.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WithdrawalsListModel.h"
@interface WithdrawalsCell : UITableViewCell


-(void)fillCellWithModel:(WithdrawalsListModel *)model;

@end
