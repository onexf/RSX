//
//  GlodTradeCell.h
//  GoldCatBank
//
//  Created by Sunny on 2017/9/5.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlodTradeModel.h"
@interface GlodTradeCell : UITableViewCell
-(void)fillCellWithModel:(GlodTradeModel *)model;
@end
