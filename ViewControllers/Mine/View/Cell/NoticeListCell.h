//
//  NoticeListCell.h
//  GoldCatBank
//
//  Created by Sunny on 2017/9/4.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NoticeListModel.h"

@interface NoticeListCell : UITableViewCell

-(void)fillCellWithModel:(NoticeListModel *)model;

@end
