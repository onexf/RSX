//
//  DiscountCouponCell.m
//  GoldCatBank
//
//  Created by Sunny on 2017/9/7.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "DiscountCouponCell.h"

#define CellContentheight 80
@interface DiscountCouponCell()

@property(nonatomic,strong)UIView *backView;

@property(nonatomic,strong)UIImageView *stateImageView;

@property(nonatomic,strong)UILabel *amountLab;

@property(nonatomic,strong)UILabel *nameLab;

@property(nonatomic,strong)UILabel *desLabel;

@property(nonatomic,strong)UIButton *viableTimeBtn;

@property(nonatomic,strong)UILabel *statelab;

@property(nonatomic,strong)UIView *leftBackView;
@property(nonatomic,strong)UIImageView *rightImageVIew;
@property(nonatomic,strong)UIImageView *signalImageView;
@end

//@property(nonatomic,strong)

@implementation DiscountCouponCell


//Counpon_EmptyLine   Counpon_rightIcon

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self.contentView addSubview:self.backView];
        
        self.contentView.backgroundColor=COLOR_TBBACK;
        
  
        [self.backView addSubview:self.rightImageVIew];
        [self.backView addSubview:self.leftBackView];
        
        [self.leftBackView addSubview:self.stateImageView];
        [self.leftBackView addSubview:self.statelab];
        [self.leftBackView addSubview:self.amountLab];
        [self.leftBackView addSubview:self.nameLab];
        [self.leftBackView addSubview:self.desLabel];
        [self.leftBackView addSubview:self.viableTimeBtn];
        
        [self.rightImageVIew addSubview:self.statelab];
        
        [self.backView addSubview:self.signalImageView];
        
        
        
    }
    
    return self;
}


-(UIImageView *)rightImageVIew
{
    if (!_rightImageVIew)
    {
        UIImage *rightImage=[UIImage imageNamed:@"Coupon_YellowBg"];
        
        CGFloat widht=CellContentheight*(rightImage.size.width/rightImage.size.height);
        _rightImageVIew=[[UIImageView alloc]initWithFrame:CGRectMake(self.backView.ct_width-widht, 0, widht, CellContentheight)];
        _rightImageVIew.image=rightImage;
    }
    
    return _rightImageVIew;
}

-(UIView *)leftBackView
{
    if (!_leftBackView)
    {
        
        _leftBackView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.rightImageVIew.ct_left, CellContentheight)];
        _leftBackView.backgroundColor=COLOR_WHITE;
        _leftBackView.layer.borderColor=COLOR_GLODBACKGROUD.CGColor;
        _leftBackView.layer.borderWidth=0.5;
       
    }
    
    return _leftBackView;
}

-(UIView *)backView
{
    if (!_backView)
    {
        _backView=[[UIView alloc]initWithFrame:CGRectMake(10, 10, SCREEN_WIDTH-20, 75)];
        _backView.backgroundColor=COLOR_WHITE;
        
    }
    
    return _backView;
}


-(UIImageView *)stateImageView
{
    if (!_stateImageView)
    {
        UIImage *image=[UIImage imageNamed:@"Counpon_addFlag"];
        _stateImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
        _stateImageView.image=image;
        
    }
    
    return _stateImageView;
}

-(UILabel *)amountLab
{
    if (!_amountLab)
    {
        _amountLab=[[UILabel alloc]initWithFrame:CGRectMake(0,30, 80, 35)];
        _amountLab.textAlignment=NSTextAlignmentCenter;
        _amountLab.textColor=COLOR_Red;
        _amountLab.font=FONT(45);
        
    }
    
    return _amountLab;
}

- (UILabel *)desLabel {
    if (!_desLabel) {
        _desLabel = [[UILabel alloc]initWithFrame:CGRectMake(_amountLab.ct_right,_nameLab.ct_bottom,_rightImageVIew.ct_left-_amountLab.ct_right-10, 20)];
        _desLabel.textAlignment=NSTextAlignmentCenter;
        _desLabel.textColor=COLOR_TEXT;
        _desLabel.font=FONT(13);
    }
    return _desLabel;
}
-(UILabel *)nameLab
{
    if (!_nameLab)
    {
        _nameLab=[[UILabel alloc]initWithFrame:CGRectMake(_amountLab.ct_right,2,_rightImageVIew.ct_left-_amountLab.ct_right-10, 20)];
//        _nameLab.numberOfLines=0;
        _nameLab.textAlignment=NSTextAlignmentCenter;
        _nameLab.textColor=COLOR_TEXT;
        _nameLab.font=FONT(13);
        
    }
    
    return _nameLab;
}

-(UIButton *)viableTimeBtn
{
    if (!_viableTimeBtn)
    {
        _viableTimeBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [_viableTimeBtn setBackgroundImage:[UIImage imageNamed:@"Counpon_EmptyLine"] forState:UIControlStateNormal];
        _viableTimeBtn.titleLabel.font=FONT(11);
        _viableTimeBtn.frame=CGRectMake(_nameLab.ct_left, _desLabel.ct_bottom+5, _nameLab.ct_width, 25);
        [_viableTimeBtn setTitleColor:COLOR_SUBTEXT forState:UIControlStateNormal];
        _viableTimeBtn.userInteractionEnabled=NO;
        
        
    }
    
    return _viableTimeBtn;
}


-(UILabel *)statelab
{
    if (!_statelab)
    {
        _statelab=[[UILabel alloc]initWithFrame:CGRectMake(10,0,_rightImageVIew.ct_width-20, _rightImageVIew.ct_height)];
        _statelab.textAlignment=NSTextAlignmentCenter;
        _statelab.textColor=COLOR_WHITE;
        _statelab.numberOfLines=0;
        _statelab.font=FONT(18);
        
    }
    
    return _statelab;
}

-(UIImageView *)signalImageView
{
    if (!_signalImageView)
    {
        UIImage *signalImage=[UIImage imageNamed:@"Coupon_grayUsed"];
        _signalImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0,0, signalImage.size.width, signalImage.size.height)];
        _signalImageView.center=CGPointMake(self.rightImageVIew.ct_left+10-signalImage.size.width/2.0, self.backView.center.y-5);
        
        _signalImageView.image=signalImage;
   
    }
    return _signalImageView;
}
-(void)fillCellWithModel:(DiscountCouponModel *)model
{
    self.stateImageView.hidden=YES;
    self.amountLab.text=model.couponAmount;
    self.nameLab.text=model.couponName;
    NSString *dateStr=[[model.endTime componentsSeparatedByString:@" "] firstObject];
//    NSDateFormatter *format=[[NSDateFormatter alloc]init];
//    [format setDateFormat:@"yyyy-mm-dd"];
//    NSDate *currentDate=[format dateFromString:dateStr];
//    [format setDateFormat:@"yyyy年mm月dd日"];
    [self.viableTimeBtn setTitle:[NSString stringWithFormat:@"到期时间%@",dateStr] forState:UIControlStateNormal];
    self.statelab.text=@"立即\n使用";

}



-(void)fillCellWithNewRedPacketsModel:(DiscountCouponModel *)model
{
    
    //couponType (integer, optional): 优惠券类型 0：满减红包；1：普通红包；2：加息券
    switch (model.couponType.integerValue)
    {
        case 0:
        {
            //满减
            self.stateImageView.image=[UIImage imageNamed:@"Counpon_subFlag"];
            self.rightImageVIew.image=[UIImage imageNamed:@"Coupon_RedBg"];
            [self.viableTimeBtn setBackgroundImage:[UIImage imageNamed:@"Counpon_RedEmptyLine"] forState:UIControlStateNormal];
            self.amountLab.textColor=COLOR_Red;
            self.amountLab.text=[NSString stringWithFormat:@"%@%@",@"¥",model.couponAmount];
//            self.nameLab.text=[NSString stringWithFormat:@"%@%@",model.couponName,model.couponDesc];
            self.nameLab.text = model.couponName;
            self.desLabel.text = model.couponDesc;

            
        }
            break;
        case 1:
        { //普通红包
            self.stateImageView.image=[UIImage imageNamed:@"Counpon_commonFlag"];
            self.rightImageVIew.image=[UIImage imageNamed:@"Coupon_YellowBg"];
            [self.viableTimeBtn setBackgroundImage:[UIImage imageNamed:@"Counpon_YellowEmptyLine"] forState:UIControlStateNormal];
            self.amountLab.textColor=COLOR_GLODBACKGROUD;
            self.amountLab.text=[NSString stringWithFormat:@"%@%@",@"¥",model.couponAmount];
//            self.nameLab.text=[NSString stringWithFormat:@"%@\n%@",model.couponName,model.couponDesc];
            self.nameLab.text = model.couponName;
            self.desLabel.text = model.couponDesc;

        }
            break;
        case 2:
        { //加息券
            self.stateImageView.image=[UIImage imageNamed:@"Counpon_addFlag"];
            self.rightImageVIew.image=[UIImage imageNamed:@"Coupon_RedBg"];
            [self.viableTimeBtn setBackgroundImage:[UIImage imageNamed:@"Counpon_RedEmptyLine"] forState:UIControlStateNormal];
            self.amountLab.textColor=COLOR_Red;
            
            if (model.increaseValue.floatValue<=0)
            {
                self.amountLab.text=[NSString stringWithFormat:@"%@%@",@"0",@"%"];
            }
            else
            {
                NSString *increaseHandleStr=[PriceTool numberPointHandle:[NSString stringWithFormat:@"%@",model.increaseValue] withRoundingStyle:NSRoundDown decimalNum:2];
                self.amountLab.text=[NSString stringWithFormat:@"%@%@",increaseHandleStr,@"%"];
            }
//            self.nameLab.text=[NSString stringWithFormat:@"%@\n加息%@天",model.couponName,model.increaseDay];
            self.nameLab.text = model.couponName;
            self.desLabel.text = [NSString stringWithFormat:@"加息%@天", model.couponDesc];

            
        }
            break;
            
        default:
            break;
    }
    
    self.amountLab.font=FONT(26);
    self.leftBackView.layer.borderColor=[UIColor clearColor].CGColor;
    self.stateImageView.hidden=NO;
    self.signalImageView.hidden=YES;
    
    
    
    NSString *dateStr=[[model.endTime componentsSeparatedByString:@" "] firstObject];
    [self.viableTimeBtn setTitle:[NSString stringWithFormat:@"到期时间%@",dateStr] forState:UIControlStateNormal];
    self.statelab.text=@"立即\n使用";
    
}


-(void)fillCellWithNewOutOfDateModel:(DiscountCouponModel *)model
{
    
    //couponType (integer, optional): 优惠券类型 0：满减红包；1：普通红包；2：加息券
    switch (model.couponType.integerValue)
    {
        case 0:
        {
            //满减
            self.stateImageView.image=[UIImage imageNamed:@"Counpon_past_subFlag"];
            self.amountLab.text=[NSString stringWithFormat:@"%@%@",@"¥",model.couponAmount];
            self.nameLab.text=[NSString stringWithFormat:@"%@\n%@",model.couponName,model.couponDesc];
            
        }
            break;
        case 1:
        { //普通红包
            self.stateImageView.image=[UIImage imageNamed:@"Counpon_past_commonFlag"];
            self.amountLab.text=[NSString stringWithFormat:@"%@%@",@"¥",model.couponAmount];
            self.nameLab.text=[NSString stringWithFormat:@"%@\n%@",model.couponName,model.couponDesc];
        }
            break;
        case 2:
        { //加息券
            self.stateImageView.image=[UIImage imageNamed:@"Counpon_past_addFlag"];
            if (model.increaseValue.floatValue<=0)
            {
                self.amountLab.text=[NSString stringWithFormat:@"%@%@",@"0",@"%"];
            }
            else
            {
                self.amountLab.text=[NSString stringWithFormat:@"%@%@",model.increaseValue,@"%"];
            }
            
            self.nameLab.text=[NSString stringWithFormat:@"%@\n加息%@天",model.couponName,model.increaseDay];

        }
            break;
            
        default:
            break;
    }
    
    
    if (model.isExpire.integerValue==1)
    {
        //已过期
        self.signalImageView.hidden=NO;
        self.signalImageView.image=[UIImage imageNamed:@"Coupon_grayOutOfDate"];
        
    }
    
    if (model.isUse.integerValue==1)
    {
        //已使用
        self.signalImageView.hidden=NO;
        self.signalImageView.image=[UIImage imageNamed:@"Coupon_grayUsed"];
        
        
    }
    
    self.amountLab.textColor=UIColorFromHex(0xc2c2c2);
    self.rightImageVIew.image=[UIImage imageNamed:@"Coupon_grayBg"];
    self.stateImageView.hidden=NO;
    [self.viableTimeBtn setBackgroundImage:[UIImage imageNamed:@"Counpon_GrayEmptyLine"] forState:UIControlStateNormal];
    self.amountLab.textColor=COLOR_LINE;
    self.amountLab.font=FONT(26);

    self.leftBackView.layer.borderColor=[UIColor clearColor].CGColor;

    NSString *dateStr=[[model.endTime componentsSeparatedByString:@" "] firstObject];
    [self.viableTimeBtn setTitle:[NSString stringWithFormat:@"到期时间%@",dateStr] forState:UIControlStateNormal];
    self.statelab.text=@"立即\n使用";

}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
