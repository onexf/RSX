//
//  DiscountCouponCell.h
//  GoldCatBank
//
//  Created by Sunny on 2017/9/7.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DiscountCouponModel.h"

@interface DiscountCouponCell : UITableViewCell

-(void)fillCellWithModel:(DiscountCouponModel *)model;

-(void)fillCellWithNewRedPacketsModel:(DiscountCouponModel *)model;

-(void)fillCellWithNewOutOfDateModel:(DiscountCouponModel *)model;
@end
