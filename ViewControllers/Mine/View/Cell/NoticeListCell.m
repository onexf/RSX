//
//  NoticeListCell.m
//  GoldCatBank
//
//  Created by Sunny on 2017/9/4.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "NoticeListCell.h"

@interface NoticeListCell()
@property(nonatomic,strong)UIImageView *topicImageView;
@property(nonatomic,strong)UILabel *titleLab;
@property(nonatomic,strong)UILabel *contentlab;

@end
@implementation NoticeListCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
//        [self.contentView addSubview:self.topicImageView];
        [self.contentView addSubview:self.titleLab];
        [self.contentView addSubview:self.contentlab];
        
    }
    
    return self;
}

-(UIImageView *)topicImageView
{
    if (!_topicImageView)
    {
       UIImage *image=[UIImage imageNamed:@"icon_xiaoxi"];
        _topicImageView=[[UIImageView alloc]initWithFrame:CGRectMake(10, 10,image.size.width,image.size.height)];
        _topicImageView.image=image;
    }
    return _topicImageView;
}

-(UILabel *)titleLab
{
    if (!_titleLab)
    {
        _titleLab=[[UILabel alloc]initWithFrame:CGRectMake(LEFT_RIGHT_MARGIN, 8,SCREEN_WIDTH-2 * LEFT_RIGHT_MARGIN, 15)];
        _titleLab.textAlignment = NSTextAlignmentCenter;
        _titleLab.font=FONT(12);
        _titleLab.textColor=COLOR_SUBTEXT;
    }
    return _titleLab;
}

-(UILabel *)contentlab
{
    if (!_contentlab)
    {
        _contentlab=[[UILabel alloc]initWithFrame:CGRectMake(LEFT_RIGHT_MARGIN, _titleLab.ct_bottom+10, SCREEN_WIDTH - 2 * LEFT_RIGHT_MARGIN, 20)];
        _contentlab.font=FONT(14);
        _contentlab.textColor=COLOR_TEXT;
    }
    
    return _contentlab;
}

-(void)fillCellWithModel:(NoticeListModel *)model
{
    self.titleLab.text = model.createTime;
    self.contentlab.text=model.title;
    
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
