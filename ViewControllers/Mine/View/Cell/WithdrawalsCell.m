//
//  WithdrawalsCell.m
//  GoldCatBank
//
//  Created by Sunny on 2017/9/7.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "WithdrawalsCell.h"
@interface WithdrawalsCell()

@property (nonatomic,strong)UILabel *timeLab;
@property(nonatomic,strong)UILabel *dateLab;

@property(nonatomic,strong)UILabel *amountLab;
@property(nonatomic,strong)UILabel *stateLab;

@end

@implementation WithdrawalsCell


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        
        CMBorderView * backVIew=[[CMBorderView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
        backVIew.backgroundColor=COLOR_WHITE;
        backVIew.borderWidth=CMViewBorderWidthMake(0, 0, 0.5, 0);
        [self.contentView addSubview:backVIew];
        
        [backVIew addSubview:self.timeLab];
        [backVIew addSubview:self.dateLab];
        [backVIew addSubview:self.amountLab];
        [backVIew addSubview:self.stateLab];
        
    }
    
    return self;
}

-(UILabel *)timeLab
{
    if (!_timeLab)
    {
        _timeLab=[[UILabel alloc]initWithFrame:CGRectMake(10, 5, 100, 20)];
        _timeLab.font=FONT(15);
        _timeLab.textColor=COLOR_TEXT;
        
    }
    
    return _timeLab;
    
}

-(UILabel *)dateLab
{
    if (!_dateLab)
    {
        _dateLab=[[UILabel alloc]initWithFrame:CGRectMake(_timeLab.ct_left, _timeLab.ct_bottom, _timeLab.ct_width, 20)];
        _dateLab.font=FONT(12);
        _dateLab.textColor=COLOR_SUBTEXT;
        
    }
    
    return _dateLab;
    
}

-(UILabel *)amountLab
{
    if (!_amountLab)
    {
        _amountLab=[[UILabel alloc]initWithFrame:CGRectMake(_dateLab.ct_right+10, 15, SCREEN_WIDTH-_dateLab.ct_right-10-90, 20)];
        _amountLab.textAlignment=NSTextAlignmentCenter;
        _amountLab.font=FONT(15);
        _amountLab.textColor=COLOR_TEXT;
    }
    
    return _amountLab;
}

-(UILabel *)stateLab
{
    if (!_stateLab)
    {
        _stateLab=[[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-120, _amountLab.ct_top,110, 20)];
        _stateLab.textColor=COLOR_SUBTEXT;
        _stateLab.font=FONT(15);
        _stateLab.textAlignment=NSTextAlignmentRight;
    }
    
    return _stateLab;
}

-(void)fillCellWithModel:(WithdrawalsListModel *)model
{

    
    self.timeLab.text=model.time2;
    self.dateLab.text=model.time1;
    self.amountLab.text=model.amount;

    
    switch (model.rechargeStatus.integerValue)
    {
        case 0:
        case 1:
        case 3:
        {
          //处理中
            self.stateLab.text=@"处理中";
            self.stateLab.textColor=COLOR_SUBTEXT;

        }
            break;
        case 2:
        case 4:
        case 6:
        {
            //审核拒绝
            self.stateLab.text=@"审核拒绝";
            self.stateLab.textColor=COLOR_Red;
        }
            break;
        case 5:
        {
          //已提交银行
            self.stateLab.text=@"已提交至银行";
            self.stateLab.textColor=COLOR_GLODTEXT;
        }
            break;
        case 7:
        {
           //已到账
            self.stateLab.text=@"已完成";
            self.stateLab.textColor=COLOR_GLODTEXT;
        }
            break;
            
        default:
        {
            //异常情况
            self.stateLab.text=@"处理中";
            self.stateLab.textColor=COLOR_SUBTEXT;
        }
            break;
    }
    

}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
