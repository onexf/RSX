//
//  GlodTradeCell.m
//  GoldCatBank
//
//  Created by Sunny on 2017/9/5.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "GlodTradeCell.h"

@interface GlodTradeCell ()

@property(nonatomic,strong)UILabel *typeLab;
@property(nonatomic,strong)UILabel *productNameLab;
@property(nonatomic,strong)UILabel *timelab;
@property(nonatomic,strong)UILabel *weightLab;
@property(nonatomic,strong)UILabel *amountLab;
@property(nonatomic,strong)UILabel *stateLab;

@end
@implementation GlodTradeCell


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        
        self.contentView.backgroundColor=COLOR_TBBACK;
        CMBorderView *borderView=[[CMBorderView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 100)];
        borderView.borderWidth=CMViewBorderWidthMake(0, 0, 0.5, 0);
        [self.contentView addSubview:borderView];
        
        [borderView addSubview:self.timelab];
        [borderView addSubview:self.stateLab];
        UIView *line=[[UIView alloc]initWithFrame:CGRectMake(0, self.timelab.ct_bottom, SCREEN_WIDTH, 0.5)];
        line.backgroundColor=COLOR_LINE;
        [borderView addSubview:line];
        [borderView addSubview:self.typeLab];
        [borderView addSubview:self.productNameLab];
        [borderView addSubview:self.weightLab];
        
        UIImage *image=[UIImage imageNamed:@"pay_pointIcon"];
        UIImageView *imageView=[[UIImageView alloc]initWithFrame:CGRectMake(self.weightLab.ct_right+5, self.weightLab.ct_top+2, 8, 8*(image.size.height/image.size.width))];
        imageView.image=image;
        [borderView addSubview:imageView];
        
        [borderView addSubview:self.amountLab];
    }
    
    return self;
}


-(UILabel *)timelab{
    
    if (!_timelab)
    {
        _timelab=[[UILabel alloc]initWithFrame:CGRectMake(10, 0,SCREEN_WIDTH/2.0, 30)];
        _timelab.textColor=COLOR_SUBTEXT;
        _timelab.font=FONT(12);
        
        
    }
    
    return _timelab;
}

-(UILabel *)stateLab{
    
    if (!_stateLab)
    {
        _stateLab=[[UILabel alloc]initWithFrame:CGRectMake(_timelab.ct_right+10,_timelab.ct_top,SCREEN_WIDTH-_timelab.ct_right-20, 30)];
        _stateLab.textAlignment=NSTextAlignmentRight;
        _stateLab.font=FONT(16);
        
    }
    
    return _stateLab;
}

-(UILabel *)typeLab
{
    if(!_typeLab){
        
        _typeLab=[[UILabel alloc]initWithFrame:CGRectMake(_timelab.ct_left,_timelab.ct_bottom+10,SCREEN_WIDTH/2.0-20, 20)];
        _typeLab.textColor=COLOR_TEXT;
        _typeLab.font=FONT(15);
    }
    
    return _typeLab;
}

-(UILabel *)productNameLab
{
    if (!_productNameLab)
    {
        _productNameLab=[[UILabel alloc]initWithFrame:CGRectMake(_typeLab.ct_left, _typeLab.ct_bottom+5, _typeLab.ct_width,_typeLab.ct_height)];
        _productNameLab.textColor=COLOR_SUBTEXT;
        _productNameLab.font=FONT(14);
    }
    
    return _productNameLab;
}

-(UILabel *)weightLab
{
    if (!_weightLab)
    {
        _weightLab=[[UILabel alloc]initWithFrame:CGRectMake(_typeLab.ct_right,_typeLab.ct_top, SCREEN_WIDTH-_typeLab.ct_right-25, _typeLab.ct_height)];
        _weightLab.textColor=COLOR_TEXT;
        _weightLab.font=FONT(15);
        _weightLab.textAlignment=NSTextAlignmentRight;
    }
    
    return _weightLab;
}

-(UILabel *)amountLab
{
    if (!_amountLab)
    {
        _amountLab=[[UILabel alloc]initWithFrame:CGRectMake(_productNameLab.ct_right,_productNameLab.ct_top, SCREEN_WIDTH-_productNameLab.ct_right-10, _productNameLab.ct_height)];
        _amountLab.textColor=COLOR_SUBTEXT;
        _amountLab.font=FONT(14);
        _amountLab.textAlignment=NSTextAlignmentRight;
    }
    
    return _amountLab;
}

-(void)fillCellWithModel:(GlodTradeModel *)model
{
    
    NSArray *typeArr=@[@"投资",@"买金",@"卖金",@"赎回",@"充值",@"提现",@"到期",@"其他"];
    
    if (model.logType.integerValue<typeArr.count)
    {
        self.typeLab.text=[typeArr objectAtIndex:model.logType.integerValue];
    }
    
    self.productNameLab.text=model.logTitle;
    self.timelab.text=model.createTime;
    
    NSString *goldCount=nil;
    if (model.goldCount.floatValue>0)
    {
        goldCount=[PriceTool numberPointHandle:model.goldCount withRoundingStyle:NSRoundDown decimalNum:3];
    }
    else
    {
        goldCount=@"0.000";
    }
    
    NSString *goldAmount=nil;
    
    if (model.goldAmount.floatValue>0)
    {
        goldAmount=[PriceTool numberPointHandle:model.goldAmount withRoundingStyle:NSRoundDown decimalNum:2];
    }
    else
    {
        goldAmount=@"0.00";
    }
    

    //交易记录类型（0：投资；1：买金；2：卖金；3：赎回；4：充值；5：提现；6：到期；7：其他） ,
    switch (model.logType.integerValue) {
        case 0:
        {
            //投资
            self.weightLab.text=[NSString stringWithFormat:@"%@",model.buyCount];
            [self setCommonStateView:model.orderStatus];
            
        }
            break;
        case 4:
        {
            //充值
            self.weightLab.text=[NSString stringWithFormat:@"%@",model.buyCount];
            [self setRechargeStateView:model.orderStatus];
        }
            break;
        case 5:
        {
            //提现
            self.weightLab.text=[NSString stringWithFormat:@"%@",model.buyCount];
            
            [self setWithdrawStateView:model.orderStatus];

        }
            break;
        case 1:
        {
            //买金
            self.weightLab.text=[NSString stringWithFormat:@"+%@克",goldCount];
            [self setCommonStateView:model.orderStatus];
        }
            break;
        case 7:
        {
            //其他
            self.weightLab.text=[NSString stringWithFormat:@"+%@克",goldCount];
            self.stateLab.text=@"已完成";
            self.stateLab.textColor=COLOR_GLODBACKGROUD;
           
        }
            break;
        case 2:
        {
            //卖金
            self.weightLab.text=[NSString stringWithFormat:@"-%@克",goldCount];
            self.stateLab.text=@"已完成";
            self.stateLab.textColor=COLOR_GLODBACKGROUD;
          
        }
            break;
        case 3:
        {
            //赎回

            self.weightLab.text = [NSString stringWithFormat:@"+%@元", goldAmount];
            if (model.orderStatus.integerValue==1)
            {
                self.stateLab.text=@"赎回完成";
                self.stateLab.textColor=COLOR_GLODBACKGROUD;
            }
            else
            {
                self.stateLab.text=@"处理中";
                self.stateLab.textColor=COLOR_SUBTEXT;
            }
           
                
        }
            break;
        case 6:
        {
             //到期
            if (model.isGoldProduct.integerValue>0)
            {
                self.weightLab.text=[NSString stringWithFormat:@"%@克",goldCount];
            }
            else
            {
                self.weightLab.text=[NSString stringWithFormat:@"%@元",goldAmount];
            }
            
            self.stateLab.text=@"";
        }
            break;
            
        default:
        {
            if (model.isGoldProduct.integerValue>0)
            {
                self.weightLab.text=[NSString stringWithFormat:@"%@克",goldCount];
            }
            else
            {
                self.weightLab.text=[NSString stringWithFormat:@"%@元",goldAmount];
            }
        }
            break;
    }
    

    if ([self.typeLab.text isEqualToString:@"卖金"]||[self.typeLab.text isEqualToString:@"赎回"]||[self.typeLab.text isEqualToString:@"提现"]) {
        
        self.amountLab.text=[NSString stringWithFormat:@"%@金额：¥%.2f",self.typeLab.text,model.goldAmount.floatValue];
        //提现金额、卖出金额、赎回金额、
    }
    else
    {
        if (model.logType.integerValue == 7) {
            //其他
            self.amountLab.text=[NSString stringWithFormat:@"实际支付：¥%@",@"0.00"];
        }
        else if (model.logType.integerValue ==6)
        {
            //到期
            
        NSString *totalAmount=[PriceTool numberA:[NSString stringWithFormat:@"%@",model.goldAmount] addNumberB:[NSString stringWithFormat:@"%@",model.totalProfit] withRoundingStyle:NSRoundDown decimalNum:2];
            self.amountLab.text=[NSString stringWithFormat:@"实际到账：¥%.2f",totalAmount.floatValue];
            
        }
        else
        {
         
            if (model.logType.integerValue == 4)
            {
                //、充值陈功
                
                if (model.orderStatus.integerValue==1)
                {
                    self.amountLab.text=[NSString stringWithFormat:@"实际支付：¥%.2f",model.payAmount.floatValue];
                }
                
            }
            else
            {
                   //投资、买金
                
                self.amountLab.text=[NSString stringWithFormat:@"实际支付：¥%.2f",model.payAmount.floatValue];
            }
           
        }

    }
    
}

//提现
-(void)setWithdrawStateView:(NSString *)stateStr
{
    switch (stateStr.integerValue)
    {
        case 0:
        case 1:
        case 3:
        {
            //处理中
            self.stateLab.text=@"审核中";
            self.stateLab.textColor=COLOR_SUBTEXT;
            
        }
            break;
        case 2:
        case 4:
        case 6:
        {
            //审核拒绝
            self.stateLab.text=@"审核拒绝";
            self.stateLab.textColor=COLOR_Red;
        }
            break;
        case 5:
        {
            //已提交银行
            self.stateLab.text=@"提交至银行";
            self.stateLab.textColor=COLOR_GLODBACKGROUD;
        }
            break;
        case 7:
        {
            //已到账
            self.stateLab.text=@"已到账";
            self.stateLab.textColor=COLOR_GLODBACKGROUD;
        }
            break;
            
        default:
        {
            //异常情况
            self.stateLab.text=@"审核中";
            self.stateLab.textColor=COLOR_SUBTEXT;
        }
            break;
    }
}

//充值
-(void)setRechargeStateView:(NSString *)stateStr
{
    if (stateStr.integerValue==0)
    {
        self.stateLab.text=@"待充值";
        self.stateLab.textColor=COLOR_SUBTEXT;
        self.amountLab.text=[NSString stringWithFormat:@"实际支付：¥%@",@"0.00"];

    }
    else if (stateStr.integerValue==1)
    {
        
        self.stateLab.text=@"充值成功";
        self.stateLab.textColor=COLOR_GLODBACKGROUD;
    }
    else
    {
        self.stateLab.text=@"充值失败";
        self.stateLab.textColor=COLOR_Red;
        self.amountLab.text=[NSString stringWithFormat:@"实际支付：¥%@",@"0.00"];
    }
}
//买金、投资
-(void)setCommonStateView:(NSString *)stateStr
{
    if (stateStr.integerValue==0)
    {
        self.stateLab.text=@"待支付";
        self.stateLab.textColor=COLOR_SUBTEXT;
    }
    else if (stateStr.integerValue==1)
    {
        
        self.stateLab.text=@"已完成";
        self.stateLab.textColor=COLOR_GLODBACKGROUD;
    }
    else
    {
        self.stateLab.text=@"支付失败";
        self.stateLab.textColor=COLOR_Red;
        
    }
    
 }

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end

