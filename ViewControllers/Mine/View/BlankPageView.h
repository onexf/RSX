//
//  BlankPageView.h
//  GoldCatBank
//
//  Created by Sunny on 2017/10/25.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, EaseBlankPageType)
{
    /*
     到时候根据需要再改正
     */
    BlankPageTypeNoData = 0,//无数据
    BlankPageTypeNetError,//网络错误
    BlankPageRedPacketNOData,//无现金红包
    BlankPageCouponNOData,//无优惠券
    BlankPageOrderListNOData,//订单记录
    
};

@interface BlankPageView : UIView
@property (strong, nonatomic) UIImageView *monkeyView;
@property (strong, nonatomic) UILabel *tipLabel;
@property (strong,nonatomic)UIView *switchView;
@property (copy, nonatomic) void(^reloadButtonBlock)(id sender);
-(instancetype)initWithFrame:(CGRect)frame withSwitchView:(UIView *)switchView;
- (void)configWithType:(EaseBlankPageType)blankPageType reloadButtonBlock:(void (^)(id))block;
@end
