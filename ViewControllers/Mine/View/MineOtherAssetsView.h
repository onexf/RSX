//
//  MineOtherAssetsView.h
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/9/4.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>
@class OtherAssetsModel;
@interface MineOtherAssetsView : UIView

/** 数据 */
@property(nonatomic, strong) OtherAssetsModel *otherAssetsData;
@end
