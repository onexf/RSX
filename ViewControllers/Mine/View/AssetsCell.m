//
//  AssetsCell.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/5.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "AssetsCell.h"
#import "UIView+Extension.h"
@interface AssetsCell ()
/** sepLine */
@property(nonatomic, strong) UIView *sepLine;
/** title */
@property(nonatomic, strong) UILabel *titleLabel;
/** ? */
@property(nonatomic, strong) UIImageView *tipsButton;
/** subTitle */
@property(nonatomic, strong) UILabel *subTitleLabel;

@end

@implementation AssetsCell

- (void)setSubText:(NSString *)subText {
    _subText = subText;
    self.subTitleLabel.text = subText;
}

- (void)tipsButtonDidTap {
    switch (self.cellType) {
        case AssetsCellTypeGoldAmount:
        {
            [TipsTool showTipsWithTitle:@"黄金现值" andContent:@"您的当前账户可计价黄金资产基于当前的黄金价格。\n\n计算公式\n\n黄金现值 = 可计价黄金克数 × 实时金价"];
        }
            break;
        case AssetsCellTypeProfit:
        {
            [TipsTool showTipsWithTitle:@"历史盈亏" andContent:@"历史盈亏包括您在招金猫使用期间内的历史黄金收益或亏损（不包括持有的部分）\n\n计算公式\n\n历史盈亏 = 每一次卖出的盈亏之和\n\n某一次卖出的盈亏 =（实时金价-当时的持仓成本）x 卖出克重"];
        }
            break;
        case AssetsCellTypeProfitZhaoCai:
        {
            [TipsTool showTipsWithTitle:@"【招财金】" andContent:@"浮动盈亏：\n\n可计价招财金（含已持仓黄金）在当前金价的条件下的盈亏\n\n计算公式：\n\n盈亏 =（实时金价 － 买入均价）× 持仓招财金克数"];
        }
            break;
        case AssetsCellTypeProfitShengCai:
        {
            [TipsTool showTipsWithTitle:@"【生财金】" andContent:@"浮动盈亏：\n\n可计价生财金（含已持仓黄金）在当前金价的条件下的盈亏\n\n计算公式：\n\n盈亏 =（实时金价 － 买入均价）× 持仓招财金克数"];
        }
            break;
//        case AssetsCellTypeIncomeLingQian:
//        {
//            [TipsTool showTipsWithTitle:@"【零钱罐】" andContent:@"1、预计收益开始日为买入后第二天\n\n2、投资和赎回无任何手续费。\n\n3、手机在线支付限额以下单页为准，若购买金额大于手机支付限额，请到官方网站：www.zhaojinmao .cn进行大额充值"];
//        }
//            break;
//        case AssetsCellTypeIncomeCunQian:
//        {
//            [TipsTool showTipsWithTitle:@"【存钱罐】" andContent:@"手机在线支付限额以下单页为准，若购买金额大于手机支付限额，请到官方网站：www.zhaojinmao .cn进行大额充值"];
//        }
//            break;
        default:
            break;
    }
}

- (void)setCellType:(AssetsCellType)cellType {
    _cellType = cellType;
    switch (cellType) {
        case AssetsCellTypeGoldAmount:
        {
            self.tipsButton.hidden = NO;
            self.titleLabel.text = @"黄金现值(元)";
            self.titleLabel.font = FONT_TITLE;
            self.titleLabel.textColor = COLOR_TEXT;
            self.subTitleLabel.text = @"0";
        }
            break;
        case AssetsCellTypeProfit:
        {
            self.tipsButton.hidden = NO;
            self.titleLabel.text = @"历史盈亏(元)";
            self.titleLabel.font = FONT_TITLE;
            self.titleLabel.textColor = COLOR_TEXT;
            self.subTitleLabel.text = @"0";
        }
            break;
        case AssetsCellTypeBuyPriceZhaoCai:
        {
            self.subTitleLabel.text = @"0";
        }
            break;
        case AssetsCellTypeProfitZhaoCai:
        {
            self.tipsButton.hidden = NO;
            self.titleLabel.text = @"浮动盈亏(元)";
            self.subTitleLabel.text = @"0";
        }
            break;
        case AssetsCellTypeIncomeZhaoCai:
        {
            self.titleLabel.text = @"累计收益(元)";
            self.subTitleLabel.text = @"0";
        }
            break;
        case AssetsCellTypeBuyPriceShengCai:
        {
            self.subTitleLabel.text = @"0";
        }
            break;
        case AssetsCellTypeProfitShengCai:
        {
            self.tipsButton.hidden = NO;
            self.titleLabel.text = @"浮动盈亏(元)";
            self.subTitleLabel.text = @"0";
        }
            break;
        case AssetsCellTypeIncomeShengCai:
        {
            self.titleLabel.text = @"累计收益(元)";
            self.subTitleLabel.text = @"0";
        }
            break;
        case AssetsCellTypeIncomeLingQian:
        {
//            self.tipsButton.hidden = NO;
            self.titleLabel.text = @"累计收益(元)";
            self.titleLabel.font = FONT_TITLE;
            self.titleLabel.textColor = COLOR_TEXT;
            self.subTitleLabel.text = @"0";
        }
            break;
        default: //AssetsCellTypeIncomeCunQian
        {
//            self.tipsButton.hidden = NO;
            self.titleLabel.text = @"累计收益(元)";
            self.titleLabel.font = FONT_TITLE;
            self.titleLabel.textColor = COLOR_TEXT;
            self.subTitleLabel.text = @"0";
        }
            break;
    }
}
- (void)layoutSubviews {
    [super layoutSubviews];
    [self.sepLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.left.equalTo(self).offset(LEFT_RIGHT_MARGIN);
        make.right.equalTo(self).offset(-LEFT_RIGHT_MARGIN);
        make.height.equalTo(@1);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(self.sepLine);
    }];
    [self.subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.right.equalTo(self.sepLine);
    }];
    [self.tipsButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(self.titleLabel.mas_right).offset(10);
    }];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, NORMAL_CELL_HEIGHT)]) {
        self.backgroundColor = COLOR_WHITE;
        [self addSubview:self.sepLine];
        [self addSubview:self.titleLabel];
        [self addSubview:self.tipsButton];
        [self addSubview:self.subTitleLabel];
    }
    return self;
}

/**
 懒加载
 
 @return sub
 */
- (UILabel *)subTitleLabel {
    if (!_subTitleLabel) {
        _subTitleLabel = [[UILabel alloc] init];
        _subTitleLabel.textColor = COLOR_TEXT;
        _subTitleLabel.font = FONT_TEXT;
        _subTitleLabel.text = @"0";
    }
    return _subTitleLabel;
}

/**
 懒加载
 
 @return 问号
 */
- (UIImageView *)tipsButton {
    if (!_tipsButton) {
        _tipsButton = [[UIImageView alloc] init];
        _tipsButton.image = [UIImage imageNamed:@"icon_question"];
        [_tipsButton addTapAction:@selector(tipsButtonDidTap) target:self];
        _tipsButton.hidden = YES;
    }
    return _tipsButton;
}

/**
 懒加载
 
 @return 标题
 */
- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = FONT_TEXT;
        _titleLabel.textColor = COLOR_SUBTEXT;
        _titleLabel.text = @"买入均价（元/克）";
    }
    return _titleLabel;
}

/**
 懒加载
 
 @return 分割线
 */
- (UIView *)sepLine {
    if (!_sepLine) {
        _sepLine = [[UIView alloc] init];
        _sepLine.backgroundColor = COLOR_LINE;
    }
    return _sepLine;
}

@end
