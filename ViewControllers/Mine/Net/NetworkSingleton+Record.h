//
//  NetworkSingleton+Record.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/10/26.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "NetworkSingleton.h"
@class GlodTradeModel, RecordDetailModel;
@interface NetworkSingleton (Record)

/**
 获取交易记录

 @param dict 参数
 @param response 结果
 */
+ (void)getRecordListWithDict:(NSDictionary *)dict Response:(void(^)(NSArray<GlodTradeModel *> *result, NSString *errMsg))response;

/**
 获取记录详情

 @param dict 参数
 @param response 详情
 */
+ (void)getRecordDetailWithDict:(NSDictionary *)dict Response:(void(^)(RecordDetailModel *detail, NSString *errMsg))response;

/**
 获取金店交易

 @param dict 参数
 @param response 结果
 */
+ (void)getShopOrderListWithDict:(NSDictionary *)dict Response:(void(^)(NSArray<GlodTradeModel *> *result, NSString *errMsg))response;


@end
