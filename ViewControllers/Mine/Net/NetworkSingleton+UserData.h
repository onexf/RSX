//
//  NetworkSingleton+UserData.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/4.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "NetworkSingleton.h"
@class MineHomePageModel, GoldAssetsModel, OtherAssetsModel, IncomeCellModel, IncomePageModel, SellOutModel;
@interface NetworkSingleton (UserData)


/**
 获取资产数据

 @param response 返回结果
 */
+ (void)userData_getMinePageDataResponse:(void(^)(MineHomePageModel *userAssetsData, NSString *errMsg))response;

/**
 获取黄金资产

 @param response 黄金资产
 */
+ (void)mine_getGoldAssetsDataResponse:(void(^)(GoldAssetsModel *goldAssets, NSString *errMsg))response;

/**
 获取其他资产
 
 @param response 其他资产
 */
+ (void)mine_getOtherAssetsDataResponse:(void(^)(OtherAssetsModel *otherAssets, NSString *errMsg))response;

/**
 卖出或赎回

 @param dict 入参
 @param response 结果
 */
+ (void)mine_sellAssetWithDict:(NSDictionary *)dict Response:(void(^)(SellOutModel *result, NSString *errMsg))response;


/**
 修改交易密码

 @param dict 入参
 @param response 结果
 */
+ (void)mine_changeTradePwd:(NSDictionary *)dict Response:(void(^)(BOOL result, NSString *errMsg))response;

/**
 重置交易密码

 @param dict 入参
 @param response 结果
 */
+ (void)mine_resetTradePwd:(NSDictionary *)dict Response:(void(^)(BOOL result, NSString *errMsg))response;

/**
 获取收益列表

 @param dict 入参昨日，累计
 @param response 结果
 */
+ (void)mine_dayFloatProfitList:(NSDictionary *)dict Response:(void(^)(NSArray <IncomeCellModel *> *incomeList, IncomePageModel *incomeData, NSString *errMsg))response;



@end
