//
//  NetworkSingleton+Login.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/8/28.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "NetworkSingleton.h"

@interface NetworkSingleton (Login)


/**
 获取验证码

 @param phoneNum 手机号
 @param response 返回结果
 */
+ (void)login_getCmsCodeWithPhoneNum:(NSString *)phoneNum response:(void(^)(BOOL result, NSString *errMsg))response;

/**
 使用验证码登录

 @param dict 入参
 @param response 返回结果
 */
+ (void)login_verifyCmsCodeWithPhoneNum:(NSDictionary *)dict response:(void(^)(BOOL result, NSString *errMsg))response;

/**
 获取验证码

 @param dict 手机号和type
 @param response 结果
 */
+ (void)login_getCmsCodeWithDict:(NSDictionary *)dict response:(void(^)(BOOL result, NSString *errMsg))response;
@end
