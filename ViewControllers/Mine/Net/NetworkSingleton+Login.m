//
//  NetworkSingleton+Login.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/8/28.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "NetworkSingleton+Login.h"
#import "UserData.h"
#import "UserAuthData.h"
#import "JPUSHService.h"
#import "NetworkSingleton+Coupon.h"
#import "CouponModel.h"

//接口拼接
#define UrlGetCmsCode               URLString(@"/api/cms/Send")

#define UrlVerifyCmsCode            URLString(@"/api/cms/Verify")

@implementation NetworkSingleton (Login)

+ (void)login_getCmsCodeWithPhoneNum:(NSString *)phoneNum response:(void(^)(BOOL result, NSString *errMsg))response {
    NSDictionary *dict = @{@"cellphone" : phoneNum};
    [[NetworkSingleton sharedManager] postResultWithParameter:dict url:UrlGetCmsCode successBlock:^(id responseBody) {
        if ([[responseBody valueForKey:@"results"] integerValue] == 1) {
            response(YES, nil);
        } else {
            response(NO, @"获取验证码失败");
        }
    } failureBlock:^(NSString *error) {
        response(NO, error);
    }];
}

+ (void)login_verifyCmsCodeWithPhoneNum:(NSDictionary *)dict response:(void(^)(BOOL result, NSString *errMsg))response {
    [[NetworkSingleton sharedManager] postResultWithParameter:dict url:UrlVerifyCmsCode successBlock:^(id responseBody) {
        if ([[responseBody valueForKey:@"results"] integerValue] == 1) {
            UserInfoModel *userInfoModel = [UserInfoModel mj_objectWithKeyValues:responseBody];
            userInfoModel.cellphone = (userInfoModel.cellphone.length == 11) ? userInfoModel.cellphone : @"13333333333";
            userInfoModel.cellphoneHide = [userInfoModel.cellphone stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
            [[UserData instance] updateUserInfo:userInfoModel];
            [[UserData instance] setLoginState:YES];
            [[UserAuthData shareInstance] refreshUserAuthData];
            [JPUSHService setAlias:userInfoModel.userId completion:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
                //        NSLog(@"%s:\n------%ld\n------%@\n-------%ld", __func__, (long)iResCode, iAlias, (long)seq);
            } seq:0];            
            response(YES, nil);
        } else {
            response(NO, @"登录失败");
        }
    } failureBlock:^(NSString *error) {
        response(NO, error);
    }];
}
+ (void)login_getCmsCodeWithDict:(NSDictionary *)dict response:(void(^)(BOOL result, NSString *errMsg))response {
    [[NetworkSingleton sharedManager] postResultWithParameter:dict url:UrlGetCmsCode successBlock:^(id responseBody) {
        if ([[responseBody valueForKey:@"results"] integerValue] == 1) {
            response(YES, nil);
        } else {
            response(NO, @"获取验证码失败");
        }
    } failureBlock:^(NSString *error) {
        response(NO, error);
    }];
}
@end
