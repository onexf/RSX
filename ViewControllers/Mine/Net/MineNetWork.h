//
//  MineNetWork.h
//  GoldCatBank
//
//  Created by Sunny on 2017/9/4.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NoticeListModel.h"

typedef void(^SuccessResponeBlock)(id responseBody);

typedef void(^FailureResponeBlock)(NSString *error);

//交易列表接口专用
typedef enum{
    Default_ProductType  =-1,//代表不传此参数
    ZhaoCaiGold = 0,//招财金
    ShengCaiGold30 = 1,//生财金30
    ShengCaiGold90 = 2,//生财金90
    ShengCaiGold180 = 3,//生财金180
    ShengCaiGold365 = 4,//生财金365
    TeJiaGold = 5,//特价黄金
    XinShouTeQuanGold = 6,//新手特权金
    LingQianGold= 7,//零钱罐
    CunQianGuan30 = 8,//存钱罐30
    CunQianGuan90 = 9,//存钱罐90
    CunQianGuan180 = 10,//存钱罐180
    CunQianGuan365 = 11,//存钱罐365
    XianShouZhuanXiang = 12,//新手专享
    CunQianGuan365ShengJiBan =13//存钱罐365天升级版
}ProductType;

//交易列表接口专用
typedef enum{
    Default_TradeType  =-1,//代表不传此参数
    Invest = 0,//投资
    BuyGlod = 1,//买金
    SellGlod = 2,//卖金
    Redemption = 3,//赎回
}TradeType;


@interface MineNetWork : NSObject

#pragma mark---消息列表
+ (void)noticeListWithDataDic:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock;


#pragma mark---交易列表
+ (void)tradeListWithDataDic:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock;
#pragma mark---新的交易／订单列表
+ (void)NewOrderListWithDataDic:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock;

#pragma mark---提现
+ (void)WithdrawalsWithDataDic:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock;

#pragma mark---提现列表
+ (void)WithdrawalsListWithDataDic:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock;

#pragma mark---优惠券列表
+ (void)discountCouponListWithDataDic:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock;

#pragma mark---获取订单详情
+ (void)GetOrderDetailWithDataDic:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock;

#pragma mark---意见反馈
+ (void)userFeedBackWithDataDic:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock;

#pragma mark---获取我的优惠券／红包
+ (void)getMyCouponListWithDataDic:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock;
@end
