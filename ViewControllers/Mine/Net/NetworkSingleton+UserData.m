//
//  NetworkSingleton+UserData.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/4.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "NetworkSingleton+UserData.h"
#import "MJExtension.h"
#import "MineHomePageModel.h"
#import "GoldAssetsModel.h"
#import "OtherAssetsModel.h"
#import "IncomeCellModel.h"
#import "IncomePageModel.h"
#import "SellOutModel.h"
#define UrlUserAssetsData            URLString(@"/api/Users/MyIndex")

#define UrlUserGoldAssets            URLString(@"/api/Users/GoldAsserts")

#define UrlUserOtherAssets           URLString(@"/api/Users/OtherAsserts")

#define UrlUSellAssets               URLString(@"/api/product/SellOut")

#define UrlChangeTradePwd            URLString(@"/api/Users/ChangeTradePwd")

#define UrlResetTradePwd             URLString(@"/api/Users/RestTradePwd")

#define UrlDayFloatProfitList        URLString(@"/api/Users/DayFloatProfitList")



@implementation NetworkSingleton (UserData)


+ (void)userData_getMinePageDataResponse:(void(^)(MineHomePageModel *userAssetsData, NSString *errMsg))response {
    [[NetworkSingleton sharedManager] postResultWithParameter:nil url:UrlUserAssetsData successBlock:^(id responseBody) {
        MineHomePageModel *userAssets = [MineHomePageModel mj_objectWithKeyValues:responseBody];
        response(userAssets, nil);
    } failureBlock:^(NSString *error) {
        response(nil, error);
    }];
}

+ (void)mine_getGoldAssetsDataResponse:(void(^)(GoldAssetsModel *goldAssets, NSString *errMsg))response {
    [[NetworkSingleton sharedManager] postResultWithParameter:nil url:UrlUserGoldAssets successBlock:^(id responseBody) {
        GoldAssetsModel *userAssets = [GoldAssetsModel mj_objectWithKeyValues:responseBody];
        response(userAssets, nil);
    } failureBlock:^(NSString *error) {
        response(nil, error);
    }];
}

+ (void)mine_getOtherAssetsDataResponse:(void(^)(OtherAssetsModel *otherAssets, NSString *errMsg))response {
    [[NetworkSingleton sharedManager] postResultWithParameter:nil url:UrlUserOtherAssets successBlock:^(id responseBody) {
        OtherAssetsModel *userAssets = [OtherAssetsModel mj_objectWithKeyValues:responseBody];
        response(userAssets, nil);
    } failureBlock:^(NSString *error) {
        response(nil, error);
    }];
}

+ (void)mine_sellAssetWithDict:(NSDictionary *)dict Response:(void(^)(SellOutModel *result, NSString *errMsg))response {
    [[NetworkSingleton sharedManager] postResultWithParameter:dict url:UrlUSellAssets successBlock:^(id responseBody) {
        if ([responseBody[@"errCode"] integerValue] == 0) {
            SellOutModel *model = [SellOutModel mj_objectWithKeyValues:responseBody];
            response(model, nil);
        } else {
            response(nil, responseBody[@"errMsg"]);
        }
    } failureBlock:^(NSString *error) {
        response(nil, error);
    }];
}

+ (void)mine_changeTradePwd:(NSDictionary *)dict Response:(void(^)(BOOL result, NSString *errMsg))response {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:dict];
    params[@"id"] = [[UserData instance] getUserInfo].userId;
    [[NetworkSingleton sharedManager] postResultWithParameter:params url:UrlChangeTradePwd successBlock:^(id responseBody) {
        if ([responseBody[@"results"] integerValue] == 1) {
            response(YES, nil);
        } else {
            response(NO, responseBody[@"errMsg"]);
        }
    } failureBlock:^(NSString *error) {
        response(NO, error);
    }];
}

+ (void)mine_resetTradePwd:(NSDictionary *)dict Response:(void(^)(BOOL result, NSString *errMsg))response {
    [[NetworkSingleton sharedManager] postResultWithParameter:dict url:UrlResetTradePwd successBlock:^(id responseBody) {
        if ([responseBody[@"results"] integerValue] == 1) {
            response(YES, nil);
        } else {
            response(NO, responseBody[@"errMsg"]);
        }
    } failureBlock:^(NSString *error) {
        response(NO, error);
    }];
}

+ (void)mine_dayFloatProfitList:(NSDictionary *)dict Response:(void(^)(NSArray <IncomeCellModel *> *incomeList, IncomePageModel *incomdeData, NSString *errMsg))response {
    [[NetworkSingleton sharedManager] postResultWithParameter:dict url:UrlDayFloatProfitList successBlock:^(id responseBody) {
        NSArray <IncomeCellModel *> *list = [IncomeCellModel mj_objectArrayWithKeyValuesArray:responseBody[@"floatProfitList"]];
        IncomePageModel *data = [IncomePageModel mj_objectWithKeyValues:responseBody];
        response(list, data, nil);
//        if ([responseBody[@"code"] integerValue] == 200) {
//            
//        } else {
//            response(nil, responseBody[@"msg"]);
//        }
    } failureBlock:^(NSString *error) {
        response(nil, nil, error);
    }];
}
@end
