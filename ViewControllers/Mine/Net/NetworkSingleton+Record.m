//
//  NetworkSingleton+Record.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/10/26.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "NetworkSingleton+Record.h"
#import "GlodTradeModel.h"
#import "RecordDetailModel.h"
#define UrlNewTransactionLogListRequest            URLString(@"/api/Users/NewTransactionLogList")
#define UrlNewGetOrderDetail                       URLString(@"/api/orders/NewGetOrderDetail")

#define UrlGetShopOrderList                        URLString(@"/api/shops/ShopOrdersList")


@implementation NetworkSingleton (Record)

+ (void)getShopOrderListWithDict:(NSDictionary *)dict Response:(void(^)(NSArray<GlodTradeModel *> *result, NSString *errMsg))response {
    [[NetworkSingleton sharedManager] postResultWithParameter:dict url:UrlGetShopOrderList successBlock:^(id responseBody) {
        if ([responseBody[@"totalItems"] integerValue] > 0) {
            if ([responseBody[@"currentPage"] integerValue] > [responseBody[@"totalPages"] integerValue]) {
                [WSProgressHUD showSuccessWithStatus:@"已经全部加载完毕"];
            }
        } else {
            [WSProgressHUD showSuccessWithStatus:@"暂无该项交易记录"];
        }
        NSArray<GlodTradeModel *> *list = [GlodTradeModel mj_objectArrayWithKeyValuesArray:responseBody[@"items"]];
        response(list, nil);
    } failureBlock:^(NSString *error) {
        response(nil, error);
    }];
}

+ (void)getRecordListWithDict:(NSDictionary *)dict Response:(void(^)(NSArray<GlodTradeModel *> *result, NSString *errMsg))response {
    [[NetworkSingleton sharedManager] postResultWithParameter:dict url:UrlNewTransactionLogListRequest successBlock:^(id responseBody) {
        if ([responseBody[@"totalItems"] integerValue] > 0) {
            if ([responseBody[@"currentPage"] integerValue] > [responseBody[@"totalPages"] integerValue]) {
                [WSProgressHUD showSuccessWithStatus:@"已经全部加载完毕"];
            }
        } else {
            [WSProgressHUD showSuccessWithStatus:@"暂无该项交易记录"];
        }
        NSArray<GlodTradeModel *> *list = [GlodTradeModel mj_objectArrayWithKeyValuesArray:responseBody[@"items"]];
        response(list, nil);
    } failureBlock:^(NSString *error) {
        response(nil, error);
    }];
}

+ (void)getRecordDetailWithDict:(NSDictionary *)dict Response:(void(^)(RecordDetailModel *detail, NSString *errMsg))response {
    [[NetworkSingleton sharedManager] postResultWithParameter:dict url:UrlNewGetOrderDetail successBlock:^(id responseBody) {
        RecordDetailModel *model = [RecordDetailModel mj_objectWithKeyValues:responseBody];
        response(model, nil);
    } failureBlock:^(NSString *error) {
        response(nil, error);
    }];

}
@end
