//
//  NetworkSingleton+Coupon.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/28.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "NetworkSingleton.h"
@class CouponModel;
@interface NetworkSingleton (Coupon)

+ (void)coupon_withSendWay:(NSString *)sendWay Response:(void(^)(CouponModel *result, NSString *errMsg))response;

@end
