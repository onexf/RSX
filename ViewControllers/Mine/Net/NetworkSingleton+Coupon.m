//
//  NetworkSingleton+Coupon.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/28.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "NetworkSingleton+Coupon.h"
#import "CouponModel.h"
#define UrlReceiveCouponWithChance            URLString(@"/api/Coupon/ReceiveCouponWithChance")

@implementation NetworkSingleton (Coupon)

+ (void)coupon_withSendWay:(NSString *)sendWay Response:(void(^)(CouponModel *result, NSString *errMsg))response {
    NSDictionary *dict = @{
                               @"sendWay" : sendWay
                           };
    [[NetworkSingleton sharedManager] postResultWithParameter:dict url:UrlReceiveCouponWithChance successBlock:^(id responseBody) {
//        CouponModel *model = [CouponModel mj_setKeyValues:[responseBody mj_keyValues]];
        NSString *name = responseBody[@"couponName"];
        if (name.length > 0) {
            [WSProgressHUD showSuccessWithStatus:[NSString stringWithFormat:@"恭喜您获得%@", name]];
        }
        response(nil, nil);
    } failureBlock:^(NSString *error) {
        response(nil, error);
    }];
}
@end
