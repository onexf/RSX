//
//  MineNetWork.m
//  GoldCatBank
//
//  Created by Sunny on 2017/9/4.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "MineNetWork.h"
#import "NetworkSingleton.h"
#define UrlNoticesList           URLString(@"/api/notices/list")
#define UrlTransactionLogList           URLString(@"/api/Users/TransactionLogList")
#define UrlMoneyPreChange          URLString(@"/api/money/PreChange")
#define UrlMoneyList          URLString(@"/api/money/List")
#define UrlMyCouponList          URLString(@"/api/Coupon/MyCouponList")
#define UrlFeedBack          URLString(@"/api/Users/FeedBack")
#define UrlGetOrderDetail         URLString(@"/api/orders/GetOrderDetail")
#define UrlMyCouponList         URLString(@"/api/Coupon/MyCouponList")
#define UrlNewTransactionLogList         URLString(@"/api/Users/NewTransactionLogList")
@implementation MineNetWork

+ (void)noticeListWithDataDic:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock{
    
    
    [[NetworkSingleton sharedManager] postResultWithParameter:dic url:UrlNoticesList successBlock:^(id responseBody) {
   
        successBlock(responseBody);
        
    } failureBlock:^(NSString *error) {
        
        failureBlock(error);
    }];
}

+ (void)tradeListWithDataDic:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock{
    
    
    [[NetworkSingleton sharedManager] postResultWithParameter:dic url:UrlTransactionLogList successBlock:^(id responseBody) {
        
        successBlock(responseBody);
        
    } failureBlock:^(NSString *error) {
        
        failureBlock(error);
    }];
}

+ (void)NewOrderListWithDataDic:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock{
    
    
    [[NetworkSingleton sharedManager] postResultWithParameter:dic url:UrlNewTransactionLogList successBlock:^(id responseBody) {
        
        successBlock(responseBody);
        
    } failureBlock:^(NSString *error) {
        
        failureBlock(error);
    }];
}


+ (void)WithdrawalsWithDataDic:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock{
    
    
    [[NetworkSingleton sharedManager] postResultWithParameter:dic url:UrlMoneyPreChange successBlock:^(id responseBody) {
        
        successBlock(responseBody);
        
    } failureBlock:^(NSString *error) {
        
        failureBlock(error);
    }];
}



+ (void)WithdrawalsListWithDataDic:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock{
    
    
    [[NetworkSingleton sharedManager] postResultWithParameter:dic url:UrlMoneyList successBlock:^(id responseBody) {
        
        successBlock(responseBody);
        
    } failureBlock:^(NSString *error) {
        
        failureBlock(error);
    }];
}


+ (void)discountCouponListWithDataDic:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock{
    
    
    [[NetworkSingleton sharedManager] postResultWithParameter:dic url:UrlMyCouponList successBlock:^(id responseBody) {
        
        successBlock(responseBody);
        
    } failureBlock:^(NSString *error) {
        
        failureBlock(error);
    }];
}


+ (void)GetOrderDetailWithDataDic:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock{
    
    
    [[NetworkSingleton sharedManager] postResultWithParameter:dic url:UrlGetOrderDetail successBlock:^(id responseBody) {
        
        successBlock(responseBody);
        
    } failureBlock:^(NSString *error) {
        
        failureBlock(error);
    }];
}

+ (void)userFeedBackWithDataDic:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock{
    
    
    [[NetworkSingleton sharedManager] postResultWithParameter:dic url:UrlFeedBack successBlock:^(id responseBody) {
        
        successBlock(responseBody);
        
    } failureBlock:^(NSString *error) {
        
        failureBlock(error);
    }];
}

+ (void)getMyCouponListWithDataDic:(NSDictionary *)dic successBlock:(SuccessResponeBlock)successBlock failureBlock:(FailureResponeBlock )failureBlock{
    
    
    [[NetworkSingleton sharedManager] postResultWithParameter:dic url:UrlMyCouponList successBlock:^(id responseBody) {
        
        successBlock(responseBody);
        
    } failureBlock:^(NSString *error) {
        
        failureBlock(error);
    }];
}



@end
