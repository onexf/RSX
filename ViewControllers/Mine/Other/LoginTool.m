//
//  LoginTool.m
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/8/25.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "LoginTool.h"
#import "LoginViewController.h"
#import "GoldCatBankNavController.h"
@interface LoginTool ()<LoginDelegate>

@end

@implementation LoginTool

+ (void)loginAction {
    LoginViewController *loginViewController = [[LoginViewController alloc] init];
    GoldCatBankNavController *loginNavController = [[GoldCatBankNavController alloc] initWithRootViewController:loginViewController];
    [[[UIApplication sharedApplication] keyWindow].rootViewController presentViewController:loginNavController animated:YES completion:nil];
}

- (void)loginAction {
    LoginViewController *loginViewController = [[LoginViewController alloc] init];
    loginViewController.loginDelegate = self;
    GoldCatBankNavController *loginNavController = [[GoldCatBankNavController alloc] initWithRootViewController:loginViewController];
    [[[UIApplication sharedApplication] keyWindow].rootViewController presentViewController:loginNavController animated:YES completion:nil];
}

- (void)loginSuccess {
    if (self.loginSuccessDelegate && [self.loginSuccessDelegate respondsToSelector:@selector(successLogin)]) {
        [self.loginSuccessDelegate successLogin];
    }
}


@end
