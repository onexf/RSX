//
//  VerifyTradePwdViewController.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/16.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VerifyTradePwdViewController : UIViewController


@property (nonatomic, copy) void (^response)(BOOL result, NSString *errMsg);


@end
