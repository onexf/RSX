//
//  LoginTool.h
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/8/25.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LoginToolDelegate <NSObject>

- (void)successLogin;

@end
@interface LoginTool : NSObject

/** 登录成功 */
@property(nonatomic, weak) id<LoginToolDelegate> loginSuccessDelegate;

+ (void)loginAction;

- (void)loginAction;

@end
