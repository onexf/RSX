//
//  VerifyTradePwdViewController.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/16.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "VerifyTradePwdViewController.h"
#import "UIView+Extension.h"
#import "UIView+YSTextInputKeyboard.h"
#import "AllNetWorkRquest.h"
#import <FDFullscreenPopGesture/UINavigationController+FDFullscreenPopGesture.h>
#import "ResetTradePWDViewController.h"
@interface VerifyTradePwdViewController ()<UITextFieldDelegate>
/** topView */
@property(nonatomic, strong) UIView *topView;
/** bottomView */
@property(nonatomic, strong) UIView *bottomView;
/** close */
@property(nonatomic, strong) UIImageView *closeImage;
/** 验证密码 */
@property(nonatomic, strong) UILabel *titleLabel;
/** line */
@property(nonatomic, strong) UIView *line;
/** 交易密码 */
@property(nonatomic, strong) UITextField *pwdField;
/** 按钮 */
@property(nonatomic, strong) UIButton *comitBtn;

@end

@implementation VerifyTradePwdViewController
#pragma mark - 事件处理
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == self.pwdField) {
        if (textField.secureTextEntry) {
            [textField insertText:self.pwdField.text];
        }
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField.text.length == 5 && ![string isEqualToString:@""]){
        self.comitBtn.enabled = YES;
        return YES;
    } else if (textField.text.length == 6) {
        if ([string isEqualToString:@""]) {
            self.comitBtn.enabled = NO;
            return YES;
        } else {
            return NO;
        }
        return YES;
    }
    return YES;
}

- (void)viewDismissAction {
    [self.pwdField cancelFirstResponse];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)comitPwd {
    [self.view endEditing:YES];
    NSDictionary *dic = @{@"pwd" : self.pwdField.text};
    __weak typeof(self) weakSelf = self;
    [WSProgressHUD showWithMaskType:WSProgressHUDMaskTypeBlack];
    [AllNetWorkRquest VerifytradePassword:dic successBlock:^(id responseBody) {
        [WSProgressHUD dismiss];
        if ([responseBody[@"results"] integerValue] == 1) {
            [weakSelf viewDismissAction];
            if (weakSelf.response) {
                weakSelf.response(YES, responseBody[@"errMsg"]);
            }
        } else {
            weakSelf.pwdField.text = @"";
            [weakSelf verVerifytradePasswordFail:responseBody];
        }
    } failureBlock:^(NSString *error) {
        [WSProgressHUD dismiss];
        [WSProgressHUD showErrorWithStatus:error];
    }];
}
- (void)verVerifytradePasswordFail:(NSDictionary *)dic {
    if ([dic[@"leftCount"] integerValue] <= 0) {
        [WSProgressHUD showErrorWithStatus:dic[@"errMsg"]];
        return;
    }
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"交易密码错误，还可以再输入 %@次",dic[@"leftCount"]]
                                                                             message:@""
                                                                      preferredStyle:UIAlertControllerStyleAlert ];
    //添加取消到UIAlertController
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"忘记密码" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        ResetTradePWDViewController *resetPWD = [[ResetTradePWDViewController alloc] init];
        //resetPWD.shouldPopToRoot = YES;
        [self.navigationController pushViewController:resetPWD animated:YES];
    }];
    [alertController addAction:cancelAction];
    UIAlertAction *OKAction = [UIAlertAction actionWithTitle:@"重新输入" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.pwdField becomeFirstResponder];
    }];
    [alertController addAction:OKAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - 初始化，布局
- (void)viewDidLoad {
    [super viewDidLoad];
    self.fd_prefersNavigationBarHidden = YES;
    self.view.backgroundColor = RGBA(255, 255, 255, 0.5);
    [self.view addSubview:self.topView];
    [self.view addSubview:self.bottomView];
    
    [self layOut];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.pwdField becomeFirstResponder];
}
- (void)layOut {
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.height.equalTo(@210);
    }];
    [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.bottom.equalTo(self.bottomView.mas_top);
    }];
    [self.closeImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(self.bottomView).offset(LEFT_RIGHT_MARGIN);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.closeImage);
        make.centerX.equalTo(self.bottomView);
    }];
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(10);
        make.left.right.equalTo(self.bottomView);
        make.height.equalTo(@1);
    }];
    [self.pwdField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.line.mas_bottom).offset(25);
        make.left.equalTo(self.bottomView).offset(LEFT_RIGHT_MARGIN);
        make.right.equalTo(self.bottomView).offset(-LEFT_RIGHT_MARGIN);
        make.height.equalTo(@(NORMAL_CELL_HEIGHT));
    }];
    [self.comitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.bottomView).offset(LEFT_RIGHT_MARGIN);
        make.right.bottom.equalTo(self.bottomView).offset(-LEFT_RIGHT_MARGIN);
        make.height.equalTo(@(NORMAL_BUTTON_HEIGHT));
    }];
}
/**
 懒加载
 
 @return 确认
 */
- (UIButton *)comitBtn {
    if (!_comitBtn) {
        _comitBtn = [[UIButton alloc] init];
        [_comitBtn setBackgroundImage:[UIImage jk_imageWithColor:COLOR_MAIN] forState:UIControlStateNormal];
        [_comitBtn setBackgroundImage:[UIImage jk_imageWithColor:COLOR_BUTTON_DISABLE] forState:UIControlStateDisabled];
        _comitBtn.layer.cornerRadius = 7;
        _comitBtn.clipsToBounds = YES;
        [_comitBtn setTitle:@"确认" forState:UIControlStateNormal];
        [_comitBtn setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
        [_comitBtn addTarget:self action:@selector(comitPwd) forControlEvents:UIControlEventTouchUpInside];
        _comitBtn.enabled = NO;
    }
    return _comitBtn;
}
/**
 懒加载
 
 @return 密码
 */
- (UITextField *)pwdField {
    if (!_pwdField) {
        _pwdField = [[UITextField alloc] init];
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 77, 44)];
        UILabel *code = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, 65, 44)];
        code.text = @"交易密码";
        code.font = FONT_TEXT;
        code.textColor = COLOR_TEXT;
        [leftView addSubview:code];
        _pwdField.leftViewMode = UITextFieldViewModeAlways;
        _pwdField.placeholder = @"请输入交易密码";
        _pwdField.leftView = leftView;
        _pwdField.backgroundColor = COLOR_WHITE;
        _pwdField.font = FONT_TEXT;
        _pwdField.jk_maxLength = 6;
        _pwdField.keyboardType = UIKeyboardTypeNumberPad;
        _pwdField.secureTextEntry = YES;
        _pwdField.layer.borderColor = COLOR_LINE.CGColor;
        _pwdField.layer.borderWidth = 1;
        _pwdField.kbMoving.offset = 71;
        _pwdField.delegate = self;
    }
    return _pwdField;
}
/**
 懒加载
 
 @return line
 */
- (UIView *)line {
    if (!_line) {
        _line = [[UIView alloc] init];
        _line.backgroundColor = COLOR_LINE;
        _line.hidden = YES;
    }
    return _line;
}
/**
 懒加载
 
 @return 验证密码
 */
- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = @"请输入交易密码";
        _titleLabel.font = FONT_TITLE;
        _titleLabel.textColor = COLOR_TEXT;
    }
    return _titleLabel;
}
/**
 懒加载
 
 @return closeImage
 */
- (UIImageView *)closeImage {
    if (!_closeImage) {
        _closeImage = [[UIImageView alloc] init];
        _closeImage.image = [UIImage imageNamed:@"close_gray"];
        [_closeImage addTapAction:@selector(viewDismissAction) target:self];

    }
    return _closeImage;
}
/**
 懒加载
 
 @return topView
 */
- (UIView *)topView {
    if (!_topView) {
        _topView = [[UIView alloc] init];
        _topView.backgroundColor = RGBA(77, 77, 77, 0.7);
        [_topView addTapAction:@selector(viewDismissAction) target:self];
    }
    return _topView;
}
/**
 懒加载
 
 @return bottomView
 */
- (UIView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[UIView alloc] init];
        _bottomView.backgroundColor = COLOR_WHITE;
        [_bottomView addSubview:self.closeImage];
        [_bottomView addSubview:self.titleLabel];
        [_bottomView addSubview:self.line];
        [_bottomView addSubview:self.pwdField];
        [_bottomView addSubview:self.comitBtn];
    }
    return _bottomView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
