//
//  VerifyTradePWDTool.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/16.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VerifyTradePWDTool : NSObject

@property (nonatomic, copy) void (^response)(BOOL result, NSString *errMsg);


+ (void)verifyTradePwdOnViewController:(UIViewController *)viewController response:(void(^)(BOOL result, NSString *errMsg))response;


@end
