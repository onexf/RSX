//
//  NSMutableAttributedString+Attributes.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/8/28.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSMutableAttributedString (Attributes)

/**
 *  返回带样式的字符串(颜色)
 *
 *  @param range 范围
 *  @param color 字体颜色
 *
 *  @return 字符串
 */
-(NSMutableAttributedString *)attributeInRange:(NSRange)range textColor:(UIColor*)color;
/**
 *  返回带样式的字符串 (font)
 *
 *  @param range 范围
 *  @param font  字体
 *
 *  @return 字符串(字体)
 */
-(NSMutableAttributedString *)attributeInRange:(NSRange)range textFont:(UIFont *)font;
/**
 *  返回带样式的字符串(行间距 对齐方式)
 *
 *  @param range 范围
 *  @param space 行间距
 *  @param align 对其方式
 *
 *  @return 字符串
 */
-(NSMutableAttributedString *)attributeInRange:(NSRange)range textLineSpace:(CGFloat)space textAlign:(NSTextAlignment)align;
@end

@interface NSMutableAttributedString (textHeight)

-(CGFloat)heightInSize:(CGSize)size;

@end
