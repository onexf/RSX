//
//  VerifyTradePWDTool.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/16.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "VerifyTradePWDTool.h"
#import "VerifyTradePwdViewController.h"
#import "GoldCatBankNavController.h"
@interface VerifyTradePWDTool ()


@end


@implementation VerifyTradePWDTool

+ (void)verifyTradePwdOnViewController:(UIViewController *)viewController response:(void(^)(BOOL result, NSString *errMsg))response {
    VerifyTradePwdViewController *verVC = [[VerifyTradePwdViewController alloc] init];
    verVC.response = ^(BOOL result, NSString *errMsg) {
        response(YES , errMsg);
    };
    GoldCatBankNavController *nav = [[GoldCatBankNavController alloc] initWithRootViewController:verVC];
    nav.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [viewController presentViewController:nav animated:YES completion:nil];
}
@end
