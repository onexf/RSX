//
//  NSMutableAttributedString+Attributes.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/8/28.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "NSMutableAttributedString+Attributes.h"

@implementation NSMutableAttributedString (Attributes)

-(NSMutableAttributedString *)attributeInRange:(NSRange)range textColor:(UIColor*)color{
    if (!color) {
    }
    [self addAttribute:NSForegroundColorAttributeName value:color range:range];
    return self;
}


-(NSMutableAttributedString *)attributeInRange:(NSRange)range textFont:(UIFont *)font{
    if (!font) {
    }
    [self addAttribute:NSFontAttributeName value:font range:range];
    return self;
}

-(NSMutableAttributedString *)attributeInRange:(NSRange)range textLineSpace:(CGFloat)space textAlign:(NSTextAlignment)align{
    NSMutableParagraphStyle *parag = [[NSMutableParagraphStyle alloc]init];
    parag.lineSpacing = space;
    parag.alignment = align;
    [self addAttribute:NSParagraphStyleAttributeName value:parag range:range];
    return self;
}

@end

@implementation NSMutableAttributedString (textHeight)

-(CGFloat)heightInSize:(CGSize)size{
    CGRect rect=[self boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    return rect.size.height;
}

@end
