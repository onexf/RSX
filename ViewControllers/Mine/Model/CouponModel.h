//
//  CouponModel.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/28.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CouponModel : NSObject

/** 券面值 */
@property(nonatomic, copy) NSString *couponAmount;
/** ID */
@property(nonatomic, copy) NSString *couponId;
/** 券名 */
@property(nonatomic, copy) NSString *couponName;
/** 是否领取到优惠券 */
@property(nonatomic, copy) NSString *isGet;



@end
