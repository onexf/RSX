//
//  DiscountCouponModel.h
//  GoldCatBank
//
//  Created by Sunny on 2017/9/7.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "BaseModel.h"

@interface DiscountCouponModel : BaseModel

@property(nonatomic,copy)NSString *ID;
@property(nonatomic,copy)NSString *bigProductTypeIds;
@property(nonatomic,copy)NSString *couponAmount;
@property(nonatomic,copy)NSString *couponId;
@property(nonatomic,copy)NSString *couponName;
@property(nonatomic,copy)NSString *createTime;
@property(nonatomic,copy)NSString *endTime;
@property(nonatomic,copy)NSString *isUse;
@property(nonatomic,copy)NSString *startTime;
@property(nonatomic,copy)NSString *totalDays;
@property(nonatomic,copy)NSString *useRangeDesc;
@property(nonatomic,copy)NSString *useTime;
@property(nonatomic,copy)NSString *userName;
@property(nonatomic,copy)NSString *userRealName;
@property(nonatomic,copy)NSString *couponType;
@property(nonatomic,copy)NSString *isExpire;
@property(nonatomic,copy)NSString *increaseValue;
@property(nonatomic,copy)NSString *increaseDay;
@property(nonatomic,copy)NSString *increaseFormula;
@property(nonatomic,copy)NSString *isApply;
@property(nonatomic,copy)NSString *h5URL;
@property(nonatomic,copy)NSString *productIds;
@property(nonatomic,copy)NSString *couponDesc;
@end
