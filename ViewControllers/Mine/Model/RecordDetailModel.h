//
//  RecordDetailModel.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/10/27.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecordDetailModel : NSObject

///** 产品ID */
@property(nonatomic, assign) NSInteger productId;
/** 定投时间 */
@property(nonatomic, assign) NSInteger termOfInvestment;
/** 订单是否过期（0：未过期；1：已过期；） */
@property(nonatomic, assign) NSUInteger isExpire;
/** 交易记录类型（0：投资；1：买金；2：卖金；3：赎回；4：充值；5：提现；6：到期；7：其他 */
@property(nonatomic, assign) NSUInteger logType;
/** 到期本金自动转入到（-1：无；0：余额；1：招财金；） */
@property(nonatomic, assign) NSInteger automaticExpiration;
/** 订单状态 充值或提现状态 0：支付待更新或提现待审核；1：充值成功或提现审核通过；2：充值失败或提现审核失败；3：提现复审通过；4：提现复审失败；5：已提交至银行；6：银行拒绝；7：银行处理成功； = ['0', '1', '2', '3', '4', '5', '6', '7'] */
@property(nonatomic, assign) NSUInteger orderStatus;
/** 是否为黄金类产品 0：不是；1：是 */
@property(nonatomic, assign) NSUInteger isGoldProduct;

/** 订单号 */
@property(nonatomic, copy) NSString *orderNo;
/** 交易时间 */
@property(nonatomic, copy) NSString *payTime;
/** 产品ID */
//@property(nonatomic, copy) NSString *productId;
/** 产品名称 , */
@property(nonatomic, copy) NSString *productName;
/** 买金  */
@property(nonatomic, copy) NSString *orderGold;
/** 应付金额（也是卖出金额，赎回金额，提现金额，充值金额） */
@property(nonatomic, copy) NSString *orderAmount;
/** 实付金额  */
@property(nonatomic, copy) NSString *payAmount;
/** 优惠券金额  */
@property(nonatomic, copy) NSString *couponAmount;
/** 使用的余额  */
@property(nonatomic, copy) NSString *useAccountAmount;
/** 使用银行卡支付金额  */
@property(nonatomic, copy) NSString *bankAmount;
/** 定投时间 */
//@property(nonatomic, copy) NSString *termOfInvestment;
///** 订单是否过期（0：未过期；1：已过期；） */
//@property(nonatomic, copy) NSString *isExpire;
/** 购买时的的实时金价（元/克） */
@property(nonatomic, copy) NSString *rtgp;
/** 交易时金价（元/克） */
@property(nonatomic, copy) NSString *tranRTGP;
/** 金价涨跌幅度（带百分号） */
@property(nonatomic, copy) NSString *goldPriceRange;
/** 计息开始日 */
@property(nonatomic, copy) NSString *interestTime;
/** 到期日  */
@property(nonatomic, copy) NSString *dueDate;
/** 收益发放日  */
@property(nonatomic, copy) NSString *profitSendTime;
/** 交易记录类型（0：投资；1：买金；2：卖金；3：赎回；4：充值；5：提现；6：到期；7：其他 */
//@property(nonatomic, copy) NSString *logType;
/** 卖出收入 */
@property(nonatomic, copy) NSString *sellIncome;
/** 账户余额 */
@property(nonatomic, copy) NSString *userAccount;
/** 卖出手续费  */
@property(nonatomic, copy) NSString *sellPoundage;
/** 提现手续费 */
@property(nonatomic, copy) NSString *poundage;
/** 到期本金自动转入到（-1：无；0：余额；1：招财金；） */
//@property(nonatomic, copy) NSString *automaticExpiration;
/** 累计发放收益 */
@property(nonatomic, copy) NSString *totalProfit;
/** 剩余金额(赎回)  */
@property(nonatomic, copy) NSString *redeemSurplus;
/** 订单状态 充值或提现状态 0：支付待更新或提现待审核；1：充值成功或提现审核通过；2：充值失败或提现审核失败；3：提现复审通过；4：提现复审失败；5：已提交至银行；6：银行拒绝；7：银行处理成功； = ['0', '1', '2', '3', '4', '5', '6', '7'] */
//@property(nonatomic, copy) NSString *orderStatus;
///** 是否为黄金类产品 0：不是；1：是 */
//@property(nonatomic, copy) NSString *isGoldProduct;
/** 购买的数量（前端显示，该字段包括单位） */
@property(nonatomic, copy) NSString *buyCount;
/** errCode (integer, optional): 错误小状态;0表示正确；否则表示错误；可以根据该字段做错误的相应处理点击查看详细 */
@property(nonatomic, copy) NSString *errCode;
/** 错误信息 */
@property(nonatomic, copy) NSString *errMsg;
/** 链接 */
@property(nonatomic, copy) NSString *protocolUrl;

/** 订单状态描述 */
@property(nonatomic, copy) NSString *orderStatusString;
/** typeString */
@property(nonatomic, copy) NSString *logTypeString;


/** 优惠券ID 0表示不存在，否则表示对应的优惠券ID  */
@property(nonatomic, assign) NSInteger couponId;
/** 优惠券名称 */
@property(nonatomic, copy) NSString *couponName;
/** 优惠券类型 0：满减红包；1：普通红包；2：加息券； , */
@property(nonatomic, copy) NSString *couponType;
/** 加息天数  */
@property(nonatomic, assign) NSUInteger increaseDay;
/** 加息值 , */
@property(nonatomic, assign) CGFloat increaseValue;


/** 状态颜色 */
@property(nonatomic, strong) UIColor *statusColor;

@end
