//
//  MineHomePageModel.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/4.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MineHomePageModel : NSObject

/** 账户余额 */
@property(nonatomic, copy) NSString *accountBalance;

/** 我在招金猫赚了（元 */
@property(nonatomic, copy) NSString *earnTotalAmount;
/** 错误小状态;0表示正确；否则表示错误；可以根据该字段做错误的相应处理 */
@property(nonatomic, copy) NSString *errCode;
/** 错误信息 */
@property(nonatomic, copy) NSString *errMsg;

/** 黄金资产 */
@property(nonatomic, copy) NSString *goldAssets;
/** 其他资产 */
@property(nonatomic, copy) NSString *otherAssets;
/** 资产总价值 黄金资产*浮动金价+其他资产+账户余额 */
@property(nonatomic, copy) NSString *totalAssetValue;
/** 昨天总收益 */
@property(nonatomic, copy) NSString *ydTotalAmount;
/** 冻结余额 */
@property(nonatomic, copy) NSString *frozenAmount;
/** 总余额 */
@property(nonatomic, copy) NSString *totalAmount;
/** 优惠券 */
@property(nonatomic, copy) NSString *couponCount;


@end
