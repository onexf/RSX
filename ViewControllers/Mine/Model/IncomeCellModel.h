//
//  IncomeCellModel.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/13.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IncomeCellModel : NSObject
/** 收益 */
@property(nonatomic, copy) NSString *floatProfit;
/** 收益来源 */
@property(nonatomic, copy) NSString *productName;


@end
