//
//  GlodTradeModel.h
//  GoldCatBank
//
//  Created by Sunny on 2017/9/5.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "BaseModel.h"

@interface GlodTradeModel : BaseModel
@property(nonatomic,copy)NSString *ID;
/** <#des#> */
@property(nonatomic, copy) NSString *orderId;

@property(nonatomic,copy)NSString *productId;
@property(nonatomic,copy)NSString *logType;
@property(nonatomic,copy)NSString *logTitle;
@property(nonatomic,copy)NSString *goldCount;
@property(nonatomic,copy)NSString *goldAmount;
@property(nonatomic,copy)NSString *createTime;
@property(nonatomic,copy)NSString *isGoldProduct;
@property(nonatomic,copy)NSString *buyCount;
/** <#des#> */
@property(nonatomic, copy) NSString *isExpire;

@property(nonatomic,copy)NSString *orderStatus;
@property(nonatomic,copy)NSString *payAmount;
/** 收益 */
@property(nonatomic, copy) NSString *totalProfit;


/** logTypeString */
@property(nonatomic, copy) NSString *logTypeString;
/** statusString */
@property(nonatomic, copy) NSString *statusString;
/** buyCountString */
@property(nonatomic, copy) NSString *buyCountString;
/** goldAmountString */
@property(nonatomic, copy) NSString *goldAmountString;

/** statusColor */
@property(nonatomic, strong) UIColor *statusColor;
@end
