//
//  GlodTradeModel.m
//  GoldCatBank
//
//  Created by Sunny on 2017/9/5.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "GlodTradeModel.h"

@implementation GlodTradeModel


- (UIColor *)statusColor {
    UIColor *color;
    switch (self.logType.integerValue) {
        case 0://投资
        case 1:
        case 2:
        case 3:
        case 4://充值
        {
            if (self.orderStatus.integerValue == 0) {
                color = COLOR_SUBTEXT;
            } else if (self.orderStatus.integerValue == 1) {
                color = COLOR_MAIN;
            } else {
                color = COLOR_Red;
            }
        }
            break;
        case 5://提现
        {
            switch (self.orderStatus.integerValue) {
                case 2:
                case 4:
                case 6:
                    color = COLOR_Red;
                    break;
                case 7:
                    color = COLOR_MAIN;
                    break;
                default:
                    color = COLOR_SUBTEXT;
                    break;
                    
            }
            break;
        case 6://到期
            {
                color = COLOR_MAIN;
            }
            break;
        case 7://其他
            {
                color = COLOR_MAIN;
            }
            break;
        }
    }
    return color;
}

- (NSString *)logTypeString {
    switch (self.logType.integerValue) {
        case 0:
            return @"投资";
            break;
        case 1:
            return @"买金";
            break;
        case 2:
            return @"卖金";
            break;
        case 3:
            return @"赎回";
            break;
        case 4:
            return @"充值";
            break;
        case 5:
            return @"提现";
            break;
        case 6:
            return @"到期";
            break;
        case 7:
            return @"其他";
            break;
        default:
            return @"";
            break;
    }
}

- (NSString *)statusString {
    switch (self.logType.integerValue) {
        case 0:
            return @"已完成";
            break;
        case 1:
            return @"已完成";
            break;
        case 2:
            return @"已完成";
            break;
        case 3:
            return @"赎回完成";
            break;
        case 4:
            return @"充值成功";
            break;
        case 5:
            return @"提现成功";
            break;
        case 6:
            return @"已到期";//到期
            break;
        case 7:
            return @"";//其他
            break;
        default:
            return @"";
            break;
    }
}

- (NSString *)buyCountString {
    switch (self.logType.integerValue) {
        case 0:
            return [NSString stringWithFormat:@"%@", self.buyCount];
            break;
        case 1:
            return [NSString stringWithFormat:@"+%@", self.buyCount];
            break;
        case 2:
            return [NSString stringWithFormat:@"-%@", self.buyCount];
            break;
        case 3:
            return [NSString stringWithFormat:@"+%@", self.buyCount];
            break;
        case 4:
            return [NSString stringWithFormat:@"+%@", self.buyCount];
            break;
        case 5:
            return [NSString stringWithFormat:@"-%@", self.buyCount];
            break;
        case 6:
            return [NSString stringWithFormat:@"+%@", self.buyCount];//到期
            break;
        case 7:
            return [NSString stringWithFormat:@"+%@", self.buyCount];
            break;
        default:
            return @"";
            break;
    }
}

- (NSString *)goldAmountString {
    switch (self.logType.integerValue) {
        case 0:
            return [NSString stringWithFormat:@"实际支付：￥%.2f", self.payAmount.floatValue];
            break;
        case 1:
            return [NSString stringWithFormat:@"实际支付：￥%.2f", self.payAmount.floatValue];
            break;
        case 2:
            return [NSString stringWithFormat:@"卖金金额：￥%.2f", self.goldAmount.floatValue];
            break;
        case 3:
            return [NSString stringWithFormat:@"赎回金额：￥%.2f", self.goldAmount.floatValue];
            break;
        case 4:
            return [NSString stringWithFormat:@"实际支付：￥%.2f", self.payAmount.floatValue];
            break;
        case 5:
            return [NSString stringWithFormat:@"提现金额：￥%.2f", self.goldAmount.floatValue];
            break;
        case 6:
            return [NSString stringWithFormat:@"实际到账：￥%.2f", self.payAmount.floatValue + self.totalProfit.floatValue];
            break;
        case 7:
            return [NSString stringWithFormat:@"%@金额：￥%.2f", self.logTitle, self.payAmount.floatValue];
            break;
        default:
            return [NSString stringWithFormat:@"%@金额：￥%.2f", self.logTitle, self.payAmount.floatValue];
            break;
    }

}
@end
