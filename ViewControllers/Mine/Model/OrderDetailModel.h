//
//  OrderDetailModel.h
//  GoldCatBank
//
//  Created by Sunny on 2017/9/15.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderDetailModel : NSObject
@property(nonatomic,copy)NSString * orderNo;
@property(nonatomic,copy)NSString * payTime;
@property(nonatomic,copy)NSString * productId;
@property(nonatomic,copy)NSString * productName;
@property(nonatomic,copy)NSString * orderGold;
@property(nonatomic,copy)NSString * orderAmount;
@property(nonatomic,copy)NSString * payAmount;
@property(nonatomic,copy)NSString * couponAmount;
@property(nonatomic,copy)NSString * useAccountAmount;
@property(nonatomic,copy)NSString * bankAmount;
@property(nonatomic,copy)NSString * termOfInvestment;
@property(nonatomic,copy)NSString * isExpire;
@property(nonatomic,copy)NSString * rtgp;
@property(nonatomic,copy)NSString * tranRTGP;
@property(nonatomic,copy)NSString * goldPriceRange;
@property(nonatomic,copy)NSString * interestTime;
@property(nonatomic,copy)NSString * dueDate;
@property(nonatomic,copy)NSString * profitSendTime;
@property(nonatomic,copy)NSString * logType;
@property(nonatomic,copy)NSString * sellIncome;
@property(nonatomic,copy)NSString * userAccount;
@property(nonatomic,copy)NSString * sellPoundage;
@property(nonatomic,copy)NSString * automaticExpiration;

@property(nonatomic,copy)NSString * totalProfit;
@property(nonatomic,copy)NSString * redeemSurplus;

@property(nonatomic,copy)NSString * errCode;
@property(nonatomic,copy)NSString * errMsg;




@end
