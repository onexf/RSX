//
//  OtherAssetsModel.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/7.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OtherAssetsModel : NSObject
/** 错误小状态;0表示正确；否则表示错误；可以根据该字段做错误的相应处理 */
@property(nonatomic, copy) NSString *errCode;
/** errMsg */
@property(nonatomic, copy) NSString *errMsg;
/** 其他资产(元)  */
@property(nonatomic, copy) NSString *otherAssets;
/** 零钱罐(元) */
@property(nonatomic, copy) NSString *piggyBankValue;
/** 存钱罐(元) */
@property(nonatomic, copy) NSString *saveBankValue;
/** 总零钱罐收益(元) */
@property(nonatomic, copy) NSString *totalPBProfit;
/** 总存钱罐收益(元)*/
@property(nonatomic, copy) NSString *totalSBProfit;

@end
