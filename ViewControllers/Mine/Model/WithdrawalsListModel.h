//
//  WithdrawalsListModel.h
//  GoldCatBank
//
//  Created by Sunny on 2017/9/7.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "BaseModel.h"

@interface WithdrawalsListModel : BaseModel

@property(nonatomic,copy)NSString *ID;
@property(nonatomic,copy)NSString *changeType;
@property(nonatomic,copy)NSString *createTime;
@property(nonatomic,copy)NSString *amount;
@property(nonatomic,copy)NSString *poundage;
@property(nonatomic,copy)NSString *rechargeStatus;
@property(nonatomic,copy)NSString *time1;
@property(nonatomic,copy)NSString *time2;
@property(nonatomic,copy)NSString *transSerialNumber;
@property(nonatomic,copy)NSString *updateTime;


@end
