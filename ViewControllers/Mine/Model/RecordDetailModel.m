//
//  RecordDetailModel.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/10/27.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "RecordDetailModel.h"

@implementation RecordDetailModel

- (UIColor *)statusColor {
    UIColor *color;
    switch (self.logType) {
        case 0://投资
        case 1:
        case 4://充值
        {
            if (self.orderStatus == 0) {
                color = COLOR_SUBTEXT;
            } else if (self.orderStatus == 1) {
                color = COLOR_MAIN;
            } else {
                color = COLOR_Red;
            }
        }
            break;
        case 2:
        case 3:
        {
            if (self.orderStatus == 0) {
                color = COLOR_SUBTEXT;
            } else if (self.orderStatus == 1) {
                color = COLOR_MAIN;
            } else {
                color = COLOR_Red;
            }
        }
            break;
        case 5://提现
        {
            switch (self.orderStatus) {
                case 2:
                case 4:
                case 6:
                    color = COLOR_Red;
                    break;
                case 7:
                    color = COLOR_MAIN;
                    break;
                default:
                    color = COLOR_SUBTEXT;
                    break;
                    
            }
            break;
        case 6://到期
            {
                color = COLOR_MAIN;
            }
            break;
        case 7://其他
            {
                color = COLOR_MAIN;
            }
            break;
        }
    }
    return color;
}
- (NSString *)orderStatusString {
    NSString *result;
    switch (self.logType) {
        case 0://投资
        {
            if (self.orderStatus == 0) {
                result = @"待支付";
            } else if (self.orderStatus == 1) {
                result = @"已完成";
            } else if (self.orderStatus == 2) {
                result = @"支付失败";
            } else {
                result = @"";
            }
        }
            break;
        case 1://买金
        {
            if (self.orderStatus == 0) {
                result = @"待支付";
            } else if (self.orderStatus == 1) {
                result = @"已完成";
            } else if (self.orderStatus == 2) {
                result = @"支付失败";
            } else {
                result = @"";
            }
        }
            break;
        case 2://卖金
        {
            if (self.orderStatus == 0) {
                result = @"处理中";
            } else if (self.orderStatus == 1) {
                result = @"已完成";//实际只有这一种情况
            } else {
                result = @"卖出失败";
            }
        }
            break;
        case 3://赎回
        {
            if (self.orderStatus == 0) {
                result = @"处理中";
            } else if (self.orderStatus == 1) {
                result = @"已完成";//实际只有这一种情况
            } else {
                result = @"赎回失败";
            }
        }
            break;
        case 4://充值
        {
            if (self.orderStatus == 0) {
                result = @"待充值";
            } else if (self.orderStatus == 1) {
                result = @"已完成";
            } else if (self.orderStatus == 2) {
                result = @"充值失败";
            } else {
                result = @"";
            }
        }
            break;
        case 5://提现
        {
            switch (self.orderStatus) {
                case 2:
                case 4:
                case 6:
                    result = @"审核拒绝";
                    break;
                case 5:
                    result = @"已提交至银行";
                    break;
                case 7:
                    result = @"已完成";
                    break;
                default:
                    result = @"审核中";
                    break;
            }
        }
            break;
        case 6://到期
        {
            result = @"到期";
        }
            break;
        case 7://其他
        {
            result = @"已完成";

        }
            break;
    }
    return result;
}

- (NSString *)logTypeString {
    NSString *result;
    switch (self.logType) {
        case 0://投资
        {
            result = @"投资";
        }
            break;
        case 1://买金
        {
            result = @"买金";
        }
            break;
        case 2://卖金
        {
            result = @"卖金";
        }
            break;
        case 3://赎回
        {
            result = @"赎回";
        }
            break;
        case 4://充值
        {
            result = @"充值";
        }
            break;
        case 5://提现
        {
            result = @"提现";
        }
            break;
        case 6://到期
        {
            if (self.isGoldProduct == 1) {
                result = @"买金";
            } else {
                result = @"投资";
            }
        }
            break;
        case 7://其他
        {
            result = @"其他";
        }
            break;
    }
    return result;
}
@end
