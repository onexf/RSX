//
//  SellOutModel.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/14.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SellOutModel : NSObject
/** 生成的订单号 （前缀：UP代表用户买入的订单；US代表用户卖出的订单；S代表系统订单（暂定）） */
@property(nonatomic, copy) NSString *orderNo;
/** 卖出的克重 */
@property(nonatomic, copy) NSString *sellGoldWieght;
/**  卖出的金额 , */
@property(nonatomic, copy) NSString *sellAmount;
/** 手续费 , */
@property(nonatomic, copy) NSString *serviceCharge;
/** 卖出后招财金购买总计 */
@property(nonatomic, copy) NSString *totalRecruitFinance;
/** 赎回后零钱罐购买总计（元) */
@property(nonatomic, copy) NSString *totalPiggyBank;




@end
