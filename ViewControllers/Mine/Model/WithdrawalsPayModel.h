//
//  WithdrawalsPayModel.h
//  GoldCatBank
//
//  Created by Sunny on 2017/9/21.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "BaseModel.h"

@interface WithdrawalsPayModel : BaseModel
@property(nonatomic,copy)NSString * userId;
@property(nonatomic,copy)NSString * transSerialNumber;

@property(nonatomic,copy)NSString * mchntcd;

@property(nonatomic,copy)NSString * fuyouSecketKey;

@property(nonatomic,copy)NSString * isAuthentication;

@property(nonatomic,copy)NSString * amount;

@property(nonatomic,copy)NSString * rechargeUserInfo;

@property(nonatomic,copy)NSString * errCode;

@property(nonatomic,copy)NSString * errMsg;


@end
