//
//  IncomePageModel.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/13.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IncomePageModel : NSObject

/** 累计收益 */
@property(nonatomic, copy) NSString *cumulativeProfit;
/** 黄金及时赚  */
@property(nonatomic, copy) NSString *goldPaysInTime;
/** 黄金看跌 */
@property(nonatomic, copy) NSString *goldSeeDown;
/** 黄金看涨 */
@property(nonatomic, copy) NSString *goldSeeRise;
/** 保价金 */
@property(nonatomic, copy) NSString *guaranteeFund;
/** 邀请其他好友奖励 */
@property(nonatomic, copy) NSString *inviteOtherReward;
/** 生财金利息180天  */
@property(nonatomic, copy) NSString *makingGoldProfit180day;
/** 生财金利息30天 */
@property(nonatomic, copy) NSString *makingGoldProfit30day;
/** 生财金利息365天 */
@property(nonatomic, copy) NSString *makingGoldProfit365day;
/** 生财金利息90天 */
@property(nonatomic, copy) NSString *makingGoldProfit90day;
/** 新手专享 */
@property(nonatomic, copy) NSString *VIPnew;
/** 新手特权金 */
@property(nonatomic, copy) NSString *newbiePrivilege;
/** 零钱罐 */
@property(nonatomic, copy) NSString *piggyBank;
/** 收益构成 */
@property(nonatomic, copy) NSString *profitConstitute;
/** 招财金 */
@property(nonatomic, copy) NSString *recruitFinance;
/** 存钱罐180天 */
@property(nonatomic, copy) NSString *saveMoneyBank180day;
/** 存钱罐30天 */
@property(nonatomic, copy) NSString *saveMoneyBank30day;
/** 存钱罐365天 */
@property(nonatomic, copy) NSString *saveMoneyBank365day;
/** 存钱罐365天升级版 */
@property(nonatomic, copy) NSString *saveMoneyBank365dayUpgrade;
/** 存钱罐90天 */
@property(nonatomic, copy) NSString *saveMoneyBank90day;
/** 更新日期 */
@property(nonatomic, copy) NSString *updateTime;
/** 昨日收益 */
@property(nonatomic, copy) NSString *yesDayFloatProfit;

@end
