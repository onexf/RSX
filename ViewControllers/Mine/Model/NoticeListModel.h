//
//  NoticeListModel.h
//  GoldCatBank
//
//  Created by Sunny on 2017/9/4.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "BaseModel.h"

@interface NoticeListModel : BaseModel

@property(nonatomic,copy)NSString *ID;
@property(nonatomic,copy)NSString *title;
@property(nonatomic,copy)NSString *href;
@property(nonatomic,copy)NSString *details;
@property(nonatomic,copy)NSString *desc;
@property(nonatomic,copy)NSString *createTime;

@end
