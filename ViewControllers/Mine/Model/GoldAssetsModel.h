//
//  GoldAssetsModel.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/7.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GoldAssetsModel : NSObject
/** 错误小状态;0表示正确；否则表示错误；可以根据该字段做错误的相应处理 */
@property(nonatomic, copy) NSString *errCode;
/** errMsg */
@property(nonatomic, copy) NSString *errMsg;
/** 黄金资产（克） */
@property(nonatomic, copy) NSString *goldAssets;
/** 黄金现值(元) */
@property(nonatomic, copy) NSString *goldValue;
/** 历史盈亏（克） */
@property(nonatomic, copy) NSString *historyProfitLoss;
/** 生财金买入均价（元/克） */
@property(nonatomic, copy) NSString *mAvePurchasePrice;
/** 生财金浮动盈亏(元) */
@property(nonatomic, copy) NSString *mFloatProfitLoss;
/** 生财金累计收益(元) */
@property(nonatomic, copy) NSString *mTotalProfit;
/** 生财金（克） */
@property(nonatomic, copy) NSString *makingMoney;
/** 生财金买入均价（元/克） */
@property(nonatomic, copy) NSString *rAvePurchasePrice;
/** 生财金浮动盈亏(元) */
@property(nonatomic, copy) NSString *rFloatProfitLoss;
/** 生财金累计收益(元) */
@property(nonatomic, copy) NSString *rTotalProfit;
/** 招财金（克）  */
@property(nonatomic, copy) NSString *recruitFinance;

@end
