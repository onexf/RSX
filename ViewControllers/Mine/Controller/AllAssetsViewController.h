//
//  AllAssetsViewController.h
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/9/7.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AllAssetsViewController : UIViewController
/** assetsType */
@property(nonatomic, copy) NSString *assetsType;

@end
