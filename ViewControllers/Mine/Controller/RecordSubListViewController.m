//
//  RecordSubListViewController.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/10/26.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "RecordSubListViewController.h"
#import "RecordListCell.h"
#import "MJRefresh.h"
#import "NetworkSingleton+Record.h"
#import "GlodTradeModel.h"
#import "RecordDetailsViewController.h"
@interface RecordSubListViewController ()<UITableViewDelegate, UITableViewDataSource>
/** UItableView */
@property(nonatomic, strong) UITableView *tableView;
/** 数据 */
@property(nonatomic, strong) NSMutableArray *dataSource;
/** 当前页 */
@property (nonatomic,assign)NSInteger currentPage;

@end

@implementation RecordSubListViewController
#pragma mark - 数据请求
- (void)getDataFromNet {
    __weak typeof(self) weakSelf = self;
    NSDictionary *dict = @{
                               @"isExpire" : self.isExpire,
                               @"listType" : self.listType,
                               @"pageIndex" : @(self.currentPage)
                           };
    [NetworkSingleton getRecordListWithDict:dict Response:^(NSArray<GlodTradeModel *> *result, NSString *errMsg) {
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        if (result.count > 0) {
            [weakSelf.dataSource addObjectsFromArray:result];
        } else {
            if (errMsg.length > 0) {
                [WSProgressHUD showErrorWithStatus:errMsg];
            }
            weakSelf.currentPage -= 1;
        }
        [weakSelf.tableView reloadData];
    }];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = COLOR_TBBACK;
    [self.view addSubview:self.tableView];
    [self.tableView.mj_header beginRefreshing];
}

#pragma mark - UItableView
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.dataSource.count > indexPath.row) {
        GlodTradeModel *model = self.dataSource[indexPath.row];
        RecordDetailsViewController *detailsVC = [[RecordDetailsViewController alloc] initWithData:model];
        [self.navigationController pushViewController:detailsVC animated:YES];
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RecordListCell *cell = [RecordListCell cellWithTableview:tableView];
    cell.model = self.dataSource[indexPath.row];
    return cell;
}
- (NSMutableArray *)dataSource {
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    return _dataSource;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 107) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.rowHeight = 107;
        _tableView.backgroundColor = COLOR_TBBACK;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        __weak typeof(self) weakSelf = self;
        _tableView.mj_header = [MJDIYHeader headerWithRefreshingBlock:^{
            weakSelf.currentPage = 1;
            [weakSelf.dataSource removeAllObjects];
            [weakSelf getDataFromNet];
        }];
        _tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            weakSelf.currentPage += 1;
            [weakSelf getDataFromNet];
        }];
    }
    return _tableView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end
