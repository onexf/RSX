//
//  ResetTradePWDViewController.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/12.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "ResetTradePWDViewController.h"
#import "NetworkSingleton+Login.h"
#import "NetworkSingleton+UserData.h"
#import "JKCountDownButton.h"

@interface ResetTradePWDViewController ()<UITextFieldDelegate, UIScrollViewDelegate>
/** scrollow */
@property(nonatomic, strong) UIScrollView *backGroundView;
/** phoneLabel */
@property(nonatomic, strong) UILabel *phoneLabel;
/** 验证码 */
@property(nonatomic, strong) UITextField *codeTextField;
/** countDownButton */
@property(nonatomic, strong) JKCountDownButton *countDownButton;
/** 姓名 */
@property(nonatomic, strong) UITextField *nameTextField;
/** ID */
@property(nonatomic, strong) UITextField *IDCardTextField;
/** pwd1 */
@property(nonatomic, strong) UITextField *pwd1TextField;
/** pwd2 */
@property(nonatomic, strong) UITextField *pwd2TextField;
/** 确定 */
@property(nonatomic, strong) UIButton *doneButton;


@end

@implementation ResetTradePWDViewController

#pragma mark - 事件处理
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    return YES;
}
- (void)doneButtonDidTap {
    NSString *errMsg = nil;
    if (self.pwd2TextField.text.length < 6) {
        errMsg = @"确认密码输入有误";
    }
    if (self.pwd1TextField.text.length < 6) {
        errMsg = @"密码输入有误";
    }
    if (self.IDCardTextField.text.length < 18) {
        errMsg = @"身份证号输入有误";
    }
    if (self.nameTextField.text.length <= 0) {
        errMsg = @"姓名输入有误";
    }
    if (self.codeTextField.text.length < 4) {
        errMsg = @"验证码输入有误";
    }
    if (![self.pwd1TextField.text isEqualToString:self.pwd2TextField.text]) {
        errMsg = @"两次输入的密码不一致";
    }
    if (errMsg) {
        [WSProgressHUD showErrorWithStatus:errMsg];
        return;
    }
    NSDictionary *dict = @{
                               @"realName" : self.nameTextField.text,
                               @"idNo" : self.IDCardTextField.text,
                               @"verCd" : self.codeTextField.text,
                               @"pwd" : self.pwd2TextField.text
                           };
    __weak typeof(self) weakSelf = self;
    [NetworkSingleton mine_resetTradePwd:dict Response:^(BOOL result, NSString *errMsg) {
        if (errMsg) {
            [WSProgressHUD showErrorWithStatus:errMsg];
        } else {
            [WSProgressHUD showSuccessWithStatus:@"修改成功"];
            if (weakSelf.shouldPopToRoot) {
                [weakSelf.navigationController popToRootViewControllerAnimated:YES];
            } else {
                [weakSelf.navigationController popViewControllerAnimated:YES];
            }
        }
    }];
}
#pragma mark - 初始化，布局
- (void)layOut {
    [self.backGroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    [self.phoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backGroundView);
        make.centerX.equalTo(self.view);
        make.height.equalTo(@(NORMAL_CELL_HEIGHT));
    }];
    [self.codeTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.phoneLabel.mas_bottom);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@(NORMAL_CELL_HEIGHT));
    }];
    [self.nameTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.codeTextField.mas_bottom).offset(10);
        make.left.right.height.equalTo(self.codeTextField);
    }];
    [self.IDCardTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.nameTextField.mas_bottom).offset(1);
        make.left.right.height.equalTo(self.codeTextField);
    }];
    [self.pwd1TextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.IDCardTextField.mas_bottom).offset(10);
        make.left.right.height.equalTo(self.codeTextField);
    }];
    [self.pwd2TextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.pwd1TextField.mas_bottom).offset(1);
        make.left.right.height.equalTo(self.codeTextField);
    }];
    [self.doneButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.height.equalTo(@49);
        if (@available(iOS 11.0, *)) {
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        } else {
            make.bottom.equalTo(self.view);
        }
    }];
    [self.countDownButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.codeTextField);
        make.right.equalTo(self.codeTextField).offset(-LEFT_RIGHT_MARGIN);
    }];
    self.backGroundView.contentSize = CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT);
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"重置交易密码";
    self.view.backgroundColor = COLOR_TBBACK;
    [self.view addSubview:self.backGroundView];
    
    [self layOut];
    [self.countDownButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    self.doneButton.enabled = YES;
}
/**
 懒加载
 
 @return uiscrollow
 */
- (UIScrollView *)backGroundView {
    if (!_backGroundView) {
        _backGroundView = [[UIScrollView alloc] init];
        _backGroundView.backgroundColor = COLOR_TBBACK;
        [_backGroundView addSubview:self.phoneLabel];
        [_backGroundView addSubview:self.codeTextField];
        [_backGroundView addSubview:self.nameTextField];
        [_backGroundView addSubview:self.IDCardTextField];
        [_backGroundView addSubview:self.pwd1TextField];
        [_backGroundView addSubview:self.pwd2TextField];
        [_backGroundView addSubview:self.doneButton];
        [_backGroundView addSubview:self.countDownButton];
    }
    return _backGroundView;
}
/**
 懒加载
 
 @return 获取验证码按钮
 */
- (JKCountDownButton *)countDownButton
{
    if (!_countDownButton) {
        _countDownButton = [JKCountDownButton buttonWithType:UIButtonTypeCustom];
        _countDownButton.titleLabel.font = FONT_TEXT;
        [_countDownButton setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_countDownButton setTitleColor:COLOR_MAIN forState:UIControlStateNormal];
        [_countDownButton sizeToFit];
        [_countDownButton setTitleColor:COLOR_SUBTEXT forState:UIControlStateDisabled];
        _countDownButton.jk_touchAreaInsets = UIEdgeInsetsMake(20, 20, 20, 20);
        [_countDownButton countDownButtonHandler:^(JKCountDownButton*sender, NSInteger tag) {
            sender.enabled = NO;
            [NetworkSingleton login_getCmsCodeWithDict:@{@"cellphone" : [[UserData instance] getUserInfo].cellphone, @"type" : @1} response:^(BOOL result, NSString *errMsg) {
                if (result) {
                    [sender startCountDownWithSecond:59];
                    
                    [sender countDownChanging:^NSString *(JKCountDownButton *countDownButton,NSUInteger second) {
                        NSString *title = [NSString stringWithFormat:@"%zds",second];
                        return title;
                    }];
                    [sender countDownFinished:^NSString *(JKCountDownButton *countDownButton, NSUInteger second) {
                        countDownButton.enabled = YES;
                        return @"重新获取";
                    }];
                } else {
                    [WSProgressHUD showErrorWithStatus:errMsg];
                    sender.enabled = YES;
                }
            }];
        }];
    }
    return _countDownButton;
}
/**
 懒加载
 
 @return 确定
 */
- (UIButton *)doneButton {
    if (!_doneButton) {
        _doneButton = [[UIButton alloc] init];
        [_doneButton setTitle:@"确定" forState:UIControlStateNormal];
        [_doneButton setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
        [_doneButton setBackgroundImage:[UIImage jk_imageWithColor:COLOR_MAIN] forState:UIControlStateNormal];
        [_doneButton setBackgroundImage:[UIImage jk_imageWithColor:COLOR_BUTTON_DISABLE] forState:UIControlStateDisabled];
        _doneButton.enabled = NO;
        [_doneButton addTarget:self action:@selector(doneButtonDidTap) forControlEvents:UIControlEventTouchUpInside];
    }
    return _doneButton;
}
/**
 懒加载
 
 @return 姓名
 */
- (UITextField *)IDCardTextField {
    if (!_IDCardTextField) {
        _IDCardTextField = [[UITextField alloc] init];
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 77, 44)];
        UILabel *code = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, 65, 44)];
        code.text = @"身份证";
        code.font = FONT_TEXT;
        code.textColor = COLOR_TEXT;
        [leftView addSubview:code];
        _IDCardTextField.leftViewMode = UITextFieldViewModeAlways;
        _IDCardTextField.placeholder = @"请输入身份证号码";
        _IDCardTextField.leftView = leftView;
        _IDCardTextField.backgroundColor = COLOR_WHITE;
        _IDCardTextField.font = FONT_TEXT;
        _IDCardTextField.jk_maxLength = 18;
        _IDCardTextField.delegate = self;
    }
    return _IDCardTextField;
}
/**
 懒加载
 
 @return 密码1
 */
- (UITextField *)pwd1TextField {
    if (!_pwd1TextField) {
        _pwd1TextField = [[UITextField alloc] init];
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 77, 44)];
        UIImageView *code = [[UIImageView alloc] initWithFrame:CGRectMake(12, 10, 24, 24)];
        code.image = [UIImage imageNamed:@"xiugaimima"];
        [leftView addSubview:code];
        _pwd1TextField.leftViewMode = UITextFieldViewModeAlways;
        _pwd1TextField.placeholder = @"请输入6位密码";
        _pwd1TextField.leftView = leftView;
        _pwd1TextField.keyboardType = UIKeyboardTypeNumberPad;
        _pwd1TextField.backgroundColor = COLOR_WHITE;
        _pwd1TextField.font = FONT_TEXT;
        _pwd1TextField.jk_maxLength = 6;
        _pwd1TextField.delegate = self;
        _pwd1TextField.secureTextEntry = YES;
    }
    return _pwd1TextField;
}
/**
 懒加载
 
 @return 密码2
 */
- (UITextField *)pwd2TextField {
    if (!_pwd2TextField) {
        _pwd2TextField = [[UITextField alloc] init];
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 77, 44)];
        UIImageView *code = [[UIImageView alloc] initWithFrame:CGRectMake(12, 10, 24, 24)];
        code.image = [UIImage imageNamed:@"xiugaimima"];
        [leftView addSubview:code];
        _pwd2TextField.leftViewMode = UITextFieldViewModeAlways;
        _pwd2TextField.placeholder = @"请确认新密码";
        _pwd2TextField.leftView = leftView;
        _pwd2TextField.backgroundColor = COLOR_WHITE;
        _pwd2TextField.font = FONT_TEXT;
        _pwd2TextField.keyboardType = UIKeyboardTypeNumberPad;
        _pwd2TextField.jk_maxLength = 6;
        _pwd2TextField.delegate = self;
        _pwd2TextField.secureTextEntry = YES;
    }
    return _pwd2TextField;
}/**
 懒加载
 
 @return 姓名
 */
- (UITextField *)nameTextField {
    if (!_nameTextField) {
        _nameTextField = [[UITextField alloc] init];
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 77, 44)];
        UILabel *code = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, 65, 44)];
        code.text = @"姓名";
        code.font = FONT_TEXT;
        code.textColor = COLOR_TEXT;
        [leftView addSubview:code];
        _nameTextField.leftViewMode = UITextFieldViewModeAlways;
        _nameTextField.placeholder = @"请输入真实姓名";
        _nameTextField.leftView = leftView;
        _nameTextField.backgroundColor = COLOR_WHITE;
        _nameTextField.font = FONT_TEXT;
        _nameTextField.delegate = self;
    }
    return _nameTextField;
}
/**
 懒加载
 
 @return 验证码
 */
- (UITextField *)codeTextField {
    if (!_codeTextField) {
        _codeTextField = [[UITextField alloc] init];
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 77, 44)];
        UILabel *code = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, 65, 44)];
        code.text = @"验证码";
        code.font = FONT_TEXT;
        code.textColor = COLOR_TEXT;
        [leftView addSubview:code];
        _codeTextField.leftViewMode = UITextFieldViewModeAlways;
        _codeTextField.placeholder = @"请输入验证码";
        _codeTextField.keyboardType = UIKeyboardTypeNumberPad;
        _codeTextField.leftView = leftView;
        _codeTextField.backgroundColor = COLOR_WHITE;
        _codeTextField.font = FONT_TEXT;
        _codeTextField.jk_maxLength = 4;
        _codeTextField.delegate = self;
    }
    return _codeTextField;
}
/**
 懒加载
 
 @return phoneLabel
 */
- (UILabel *)phoneLabel {
    if (!_phoneLabel) {
        _phoneLabel = [[UILabel alloc] init];
        _phoneLabel.font = FONT_TEXT;
        _phoneLabel.textColor = COLOR_TEXT;
        _phoneLabel.text = [NSString stringWithFormat:@"您的手机%@将会收到一条短信", [[UserData instance] getUserInfo].cellphoneHide];
    }
    return _phoneLabel;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
