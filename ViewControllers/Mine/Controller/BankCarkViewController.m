//
//  BankCarkViewController.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/8/31.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "BankCarkViewController.h"
#import "UserAuthData.h"
#import "CBWebViewController.h"
#import "BindBankCarViewController.h"
@interface BankCarkViewController ()
/** 图标 */
@property(nonatomic, strong) UIImageView *warnImageView;
/** 认证提示 */
@property(nonatomic, strong) UILabel *warnTitleLabel;
/** 提示内容 */
@property(nonatomic, strong) UILabel *warnDetails;
/** 姓名 */
@property(nonatomic, strong) UILabel *nameLabel;
/** 银行卡号 */
@property(nonatomic, strong) UILabel *bankCardLabel;
/** 更换银行卡 */
@property(nonatomic, strong) UIButton *changBankCard;

@end

@implementation BankCarkViewController
#pragma mark - 事件处理 
//更换银行卡
- (void)changeBankCanr:(UIButton *)button {
    if ([button.titleLabel.text isEqualToString:@"去绑定"]) {
        BindBankCarViewController *bindBankCardVC=[[BindBankCarViewController alloc] init];
        [self.navigationController pushViewController:bindBankCardVC animated:YES];
    } else {
        CBWebViewController *webViewController = [[CBWebViewController alloc] init];
        webViewController.urlString = @"https://www.zhaojinmao.cn/testwebapp/me/bankcards/bankcards.html";
        webViewController.showShareButton = NO;
        [self.navigationController pushViewController:webViewController animated:YES];
    }
    
}
#pragma mark - 初始化，布局
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = COLOR_TBBACK;
    self.title = @"银行卡认证";
    [self.view addSubview:self.warnImageView];
    [self.view addSubview:self.warnTitleLabel];
    [self.view addSubview:self.warnDetails];
    [self.view addSubview:self.nameLabel];
    [self.view addSubview:self.bankCardLabel];
    [self.view addSubview:self.changBankCard];
}
- (void)layout {
    [self.warnImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(25);
        make.left.equalTo(self.view).offset(LEFT_RIGHT_MARGIN);
    }];
    [self.warnTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.warnImageView.mas_right).offset(10);
        make.centerY.equalTo(self.warnImageView);
    }];
    [self.warnDetails mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.warnTitleLabel.mas_bottom).offset(20);
        make.centerX.equalTo(self.view);
        make.width.equalTo(@(SCREEN_WIDTH - 56));
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.warnDetails.mas_bottom).offset(30);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@44);
    }];
    [self.bankCardLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.nameLabel.mas_bottom).offset(1);
        make.left.height.equalTo(self.nameLabel);
        make.right.equalTo(self.changBankCard.mas_left);
    }];
    [self.changBankCard mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.height.equalTo(self.bankCardLabel);
        make.right.equalTo(self.view);
        make.width.mas_lessThanOrEqualTo(@90);
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UserAuthData shareInstance] refreshUserInfoResponse:^(UserAuthDataModel *authData, NSString *errMsg) {
        if (authData && [authData.isAuthentication isEqualToString:@"1"] && authData.bankCard.length > 0) {
            NSString *nameString = [authData.realName stringByReplacingCharactersInRange:NSMakeRange(1, authData.realName.length - 1) withString:@" * * "];
            NSString *cardString = [authData.bankCard stringByReplacingCharactersInRange:NSMakeRange(4, authData.bankCard.length - 7) withString:@"  ****  ****  ****  "];
            self.nameLabel.text = [NSString stringWithFormat:@"    姓名：%@", nameString];
            self.bankCardLabel.text = [NSString stringWithFormat:@"    银行卡号：%@", cardString];
            [self.changBankCard setTitle:@"更换此卡" forState:UIControlStateNormal];
        } else {
            self.nameLabel.text = [NSString stringWithFormat:@"    姓名：%@", @""];
            self.bankCardLabel.text = [NSString stringWithFormat:@"    银行卡号：%@", @"尚未绑定"];
            [self.changBankCard setTitle:@"去绑定" forState:UIControlStateNormal];
        }
    }];
    [self layout];
}
/**
 懒加载
 
 @return 按钮
 */
- (UIButton *)changBankCard {
    if (!_changBankCard) {
        _changBankCard = [[UIButton alloc] init];
        [_changBankCard setTitle:@"去绑定" forState:UIControlStateNormal];
        _changBankCard.backgroundColor = COLOR_MAIN;
        [_changBankCard setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
        [_changBankCard addTarget:self action:@selector(changeBankCanr:) forControlEvents:UIControlEventTouchUpInside];
        _changBankCard.titleLabel.font = FONT_TITLE;
        [_changBankCard sizeToFit];
    }
    return _changBankCard;
}
/**
 懒加载
 
 @return 银行卡号
 */
- (UILabel *)bankCardLabel {
    if (!_bankCardLabel) {
        _bankCardLabel = [[UILabel alloc] init];
        _bankCardLabel.backgroundColor = COLOR_WHITE;
        _bankCardLabel.font = FONT_TEXT;
        _bankCardLabel.textColor = COLOR_SUBTEXT;
    }
    return _bankCardLabel;
}
/**
 懒加载
 
 @return 姓名
 */
- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.backgroundColor = COLOR_WHITE;
        _nameLabel.font = FONT_TEXT;
        _nameLabel.textColor = COLOR_SUBTEXT;
    }
    return _nameLabel;
}
/**
 懒加载
 
 @return 提示内容
 */
- (UILabel *)warnDetails {
    if (!_warnDetails) {
        _warnDetails = [[UILabel alloc] init];
        _warnDetails.textColor = COLOR_SUBTEXT;
        _warnDetails.font = FONT_SUBTEXT;
        NSString *string = @"实名认证将您的资产与个人绑定，保障账户资金安全。认证成功后将为您申请与个人身份绑定的电子签名证书，确保您和招金猫签订的投资协议合法有效。";
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:5];
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, string.length)];
        _warnDetails.attributedText = attributedString;
        _warnDetails.numberOfLines = 0;
    }
    return _warnDetails;
}
/**
 懒加载
 
 @return 认证提示
 */
- (UILabel *)warnTitleLabel {
    if (!_warnTitleLabel) {
        _warnTitleLabel = [[UILabel alloc] init];
        _warnTitleLabel.font = FONT_TEXT;
        _warnTitleLabel.textColor = COLOR_GLODTEXT;
        _warnTitleLabel.text = @"认证提示";
    }
    return _warnTitleLabel;
}
/**
 懒加载
 
 @return 图标
 */
- (UIImageView *)warnImageView {
    if (!_warnImageView) {
        _warnImageView = [[UIImageView alloc] init];
        _warnImageView.image = [UIImage imageNamed:@"icon_warn"];
    }
    return _warnImageView;
}
- (void)dealloc {
//    NSLog(@"%s", __func__);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
