//
//  MessageHomeViewController.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/10/19.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "MessageHomeViewController.h"
#import "UIView+Extension.h"
#import "MessageViewController.h"
@interface MessageHomeViewController ()
/** uiview */
@property(nonatomic, strong) UIView *systemMessageView;
/** uiimage */
@property(nonatomic, strong) UIImageView *systemMessageImageView;
/** uilabel */
@property(nonatomic, strong) UILabel *systemMessageLabel;
/** arrow */
@property(nonatomic, strong) UIImageView *arrowImageView;
@end

@implementation MessageHomeViewController
- (void)systemMessages {
    MessageViewController *systemMessage = [[MessageViewController alloc] init];
    [self.navigationController pushViewController:systemMessage animated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"消息中心";
    self.view.backgroundColor = COLOR_TBBACK;
    [self.view addSubview:self.systemMessageView];
    
    [self.systemMessageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(10);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@49);
    }];
    [self.systemMessageImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.systemMessageView);
        make.left.equalTo(self.systemMessageView).offset(LEFT_RIGHT_MARGIN);
    }];
    [self.systemMessageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.systemMessageView);
        make.left.equalTo(self.systemMessageImageView.mas_right).offset(10);
    }];
    [self.arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.systemMessageView);
        make.right.equalTo(self.systemMessageView).offset(-LEFT_RIGHT_MARGIN);
    }];
}
/**
 懒加载
 
 @return arrow
 */
- (UIImageView *)arrowImageView {
    if (!_arrowImageView) {
        _arrowImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home_More"]];
    }
    return _arrowImageView;
}
/**
 懒加载
 
 @return 系统公告
 */
- (UILabel *)systemMessageLabel {
    if (!_systemMessageLabel) {
        _systemMessageLabel = [[UILabel alloc] init];
        _systemMessageLabel.text = @"系统公告";
        _systemMessageLabel.font = FONT_TITLE;
        _systemMessageLabel.textColor = COLOR_TEXT;
    }
    return _systemMessageLabel;
}
/**
 懒加载
 
 @return image
 */
- (UIImageView *)systemMessageImageView {
    if (!_systemMessageImageView) {
        _systemMessageImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_xiaoxi"]];
    }
    return _systemMessageImageView;
}
/**
 懒加载
 
 @return 系统公告
 */
- (UIView *)systemMessageView {
    if (!_systemMessageView) {
        _systemMessageView = [[UIView alloc] init];
        _systemMessageView.backgroundColor = COLOR_WHITE;
        [_systemMessageView addSubview:self.systemMessageImageView];
        [_systemMessageView addSubview:self.systemMessageLabel];
        [_systemMessageView addSubview:self.arrowImageView];
        [_systemMessageView addTapAction:@selector(systemMessages) target:self];
    }
    return _systemMessageView;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
