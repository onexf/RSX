//
//  LoginViewController.m
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/8/25.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "LoginViewController.h"
#import "NSMutableAttributedString+Attributes.h"
#import "UIView+Extension.h"
#import "CBWebViewController.h"
#import <FDFullscreenPopGesture/UINavigationController+FDFullscreenPopGesture.h>
#import "NetworkSingleton+Login.h"
#import "JKCountDownButton.h"

@interface LoginViewController ()<UITextFieldDelegate>

/** 关闭 */
@property(nonatomic, strong) UIButton *dismissButton;
/** 标题 */
@property(nonatomic, strong) UILabel *titleLabel;
/** 图片 */
@property(nonatomic, strong) UIImageView *logoImageView;
/** text */
@property(nonatomic, strong) UILabel *textLabel;
/** 账号 */
@property(nonatomic, strong) UITextField *phoneTextField;
/** 验证码 */
@property(nonatomic, strong) UITextField *codeTextField;
/** 倒计时按钮 */
@property(nonatomic, strong) JKCountDownButton *countDownButton;
/** 协议按钮 */
@property(nonatomic, strong) UIButton *pactButton;
/** 协议 */
@property(nonatomic, strong) UILabel *pactLabel;
/** 登录按钮 */
@property(nonatomic, strong) UIButton *loginButoon;
/** 阿里云图片 */
@property(nonatomic, strong) UIImageView *aliyunImageView;
/** 阿里云des */
@property(nonatomic, strong) UILabel *aliyunLabel;

@end

@implementation LoginViewController

#pragma mark - 事件处理
//招金猫服务协议
- (void)goldCatBankPact {
    CBWebViewController *webViewController = [[CBWebViewController alloc] init];
    webViewController.urlString = @"https://www.zhaojinmao.cn/webapp/User_agreement/index1.html";
    [self.navigationController pushViewController:webViewController animated:YES];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == self.phoneTextField) {//手机号
        if (textField.text.length == 10 && ![string isEqualToString:@""]){
            self.loginButoon.enabled = (self.codeTextField.text.length == 4 && self.pactButton.selected == YES);
            self.countDownButton.enabled = YES;
            return YES;
        } else if (textField.text.length == 11) {
            if ([string isEqualToString:@""]) {
                self.loginButoon.enabled = NO;
                self.countDownButton.enabled = NO;
                return YES;
            } else {
                return NO;
            }
            return YES;
        }
        return YES;
    } else {//验证码
        if (textField.text.length == 3 && ![string isEqualToString:@""]){
            self.loginButoon.enabled = (self.phoneTextField.text.length == 11 && self.pactButton.selected == YES);
            return YES;
        } else if (textField.text.length == 4) {
            if ([string isEqualToString:@""]) {
                self.loginButoon.enabled = NO;
                return YES;
            } else {
                return NO;
            }
            return YES;
        }
        return YES;
    }
}
//取消登录
- (void)dismissLoginView {
    [self dismissViewControllerAnimated:YES completion:nil];
    if (self.loginDelegate && [self.loginDelegate respondsToSelector:@selector(loginSuccess)]) {
        [self.loginDelegate loginSuccess];
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}
//登录
- (void)loginButoonDidTap:(UIButton *)button {
    [self.view endEditing:YES];
    if (!(self.codeTextField.text.length == 4)) {
        [WSProgressHUD showErrorWithStatus:@"验证码输入错误"];
        return;
    }
    if (!(self.phoneTextField.text.length == 11)) {
        [WSProgressHUD showErrorWithStatus:@"手机号输入错误"];
        return;
    }
    __weak typeof(self) weakSelf = self;
    NSDictionary *dict = @{@"cellphone" : self.phoneTextField.text, @"code" : self.codeTextField.text};
    [WSProgressHUD showWithMaskType:WSProgressHUDMaskTypeBlack];
    [NetworkSingleton login_verifyCmsCodeWithPhoneNum:dict response:^(BOOL result, NSString *errMsg) {
        [WSProgressHUD dismiss];
        if (result) {
            [weakSelf dismissViewControllerAnimated:YES completion:nil];
            [WSProgressHUD showSuccessWithStatus:@"登录成功"];
            if (weakSelf.loginDelegate && [weakSelf.loginDelegate respondsToSelector:@selector(loginSuccess)]) {
                [weakSelf.loginDelegate loginSuccess];
            }
        } else {
            [WSProgressHUD showErrorWithStatus:errMsg];
        }
    }];
    
}
//用户协议
- (void)pactButtonDidTap:(UIButton *)button {
    self.pactButton.selected = !button.selected;
    self.loginButoon.enabled = button.selected && self.phoneTextField.text.length == 11 && self.codeTextField.text.length == 4;
}
#pragma mark - 初始化，布局
- (void)viewDidLoad {
    [super viewDidLoad];
    self.fd_prefersNavigationBarHidden = YES;
    self.view.backgroundColor = COLOR_TBBACK;

    [self.view addSubview:self.dismissButton];
    [self.view addSubview:self.titleLabel];
    [self.view addSubview:self.logoImageView];
    [self.view addSubview:self.textLabel];
    [self.view addSubview:self.phoneTextField];
    [self.view addSubview:self.codeTextField];
    [self.view addSubview:self.pactButton];
    [self.view addSubview:self.pactLabel];
    [self.view addSubview:self.loginButoon];
    [self.view addSubview:self.aliyunImageView];
    [self.view addSubview:self.aliyunLabel];
    [self.view addSubview:self.countDownButton];
    
    [self layout];
}

- (void)layout {
    [self.dismissButton mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0, *)){
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop).offset(30);
        } else {
            make.top.equalTo(self.view).offset(30);
        }
        make.left.equalTo(self.view).offset(LEFT_RIGHT_MARGIN);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.dismissButton);
        make.centerX.equalTo(self.view);
        make.height.equalTo(@44);
    }];
    [self.logoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(20);
        make.centerX.equalTo(self.view);
    }];
    [self.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.logoImageView.mas_bottom).offset(30);
        make.centerX.equalTo(self.view);
    }];
    [self.phoneTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textLabel.mas_bottom).offset(15);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@44);
    }];
    [self.codeTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.phoneTextField.mas_bottom).offset(SEP_LINE_HEIGHT);
        make.left.right.height.equalTo(self.phoneTextField);
    }];
    [self.countDownButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.phoneTextField);
        make.right.equalTo(self.phoneTextField).offset(-LEFT_RIGHT_MARGIN);
    }];
    [self.pactButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.codeTextField.mas_bottom).offset(24);
        make.left.equalTo(self.view).offset(LEFT_RIGHT_MARGIN);
    }];
    [self.pactLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.pactButton);
        make.left.equalTo(self.pactButton.mas_right).offset(7);
    }];
    [self.loginButoon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.pactLabel.mas_bottom).offset(15);
        make.left.equalTo(self.pactButton);
        make.right.equalTo(self.view).offset(-LEFT_RIGHT_MARGIN);
        make.height.equalTo(@(NORMAL_BUTTON_HEIGHT));
    }];
    
    [self.aliyunLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0, *)){
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom).offset(-30);
        } else {
            make.bottom.equalTo(self.view).offset(-30);
        }
        make.centerX.equalTo(self.view);
    }];
    [self.aliyunImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.aliyunLabel.mas_top).offset(-15);
        make.centerX.equalTo(self.aliyunLabel);
    }];
}

/**
 懒加载
 
 @return 阿里云文字
 */
- (UILabel *)aliyunLabel
{
    if (!_aliyunLabel) {
        _aliyunLabel = [[UILabel alloc] init];
        _aliyunLabel.font = FONT_SUBTEXT;
        _aliyunLabel.textColor = COLOR_SUBTEXT;
        _aliyunLabel.text = @"数据安全由阿里云提供保障您的隐私";
    }
    return _aliyunLabel;
}

/**
 懒加载
 
 @return 阿里云图片
 */
- (UIImageView *)aliyunImageView
{
    if (!_aliyunImageView) {
        _aliyunImageView = [[UIImageView alloc] init];
        _aliyunImageView.image = [UIImage imageNamed:@"mine_aliyun"];
    }
    return _aliyunImageView;
}

/**
 懒加载
 
 @return 登录按钮
 */
- (UIButton *)loginButoon
{
    if (!_loginButoon) {
        _loginButoon = [[UIButton alloc] init];
        [_loginButoon setTitle:@"登录" forState:UIControlStateNormal];
        _loginButoon.titleLabel.font = FONT_BUTTON_TEXT;
        [_loginButoon setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
        [_loginButoon jk_setBackgroundColor:COLOR_MAIN forState:UIControlStateNormal];
        [_loginButoon jk_setBackgroundColor:COLOR_BUTTON_DISABLE forState:UIControlStateDisabled];
        [_loginButoon addTarget:self action:@selector(loginButoonDidTap:) forControlEvents:UIControlEventTouchUpInside];
        _loginButoon.layer.cornerRadius = 10.0f;
        _loginButoon.clipsToBounds = YES;
        _loginButoon.enabled = NO;
    }
    return _loginButoon;
}

/**
 懒加载
 
 @return 协议
 */
- (UILabel *)pactLabel
{
    if (!_pactLabel) {
        _pactLabel = [[UILabel alloc] init];
        _pactLabel.font = FONT_SUBTEXT;
        NSString *string = @"我已阅读并同意招金猫服务协议";
        NSMutableAttributedString *returnStr = [[NSMutableAttributedString alloc] initWithString:string];
        NSRange strRange = NSMakeRange(7, 7);
        [returnStr attributeInRange:strRange textColor:COLOR_MAIN];
        _pactLabel.attributedText = returnStr;
        [_pactLabel addTapAction:@selector(goldCatBankPact) target:self];
    }
    return _pactLabel;
}

/**
 懒加载
 
 @return 协议按钮
 */
- (UIButton *)pactButton
{
    if (!_pactButton) {
        _pactButton = [[UIButton alloc] init];
        [_pactButton setImage:[UIImage imageNamed:@"mine_button_selected"] forState:UIControlStateSelected];
        [_pactButton setImage:[UIImage imageNamed:@"mine_button_default"] forState:UIControlStateNormal];
        [_pactButton sizeToFit];
        _pactButton.selected = YES;
        [_pactButton addTarget:self action:@selector(pactButtonDidTap:) forControlEvents:UIControlEventTouchUpInside];
        _pactButton.timeInterval = 0.3;
        _pactButton.jk_touchAreaInsets = UIEdgeInsetsMake(20, 20, 20, 20);
    }
    return _pactButton;
}

/**
 懒加载
 
 @return 获取验证码按钮
 */
- (JKCountDownButton *)countDownButton
{
    if (!_countDownButton) {
        _countDownButton = [JKCountDownButton buttonWithType:UIButtonTypeCustom];
        _countDownButton.titleLabel.font = FONT_TEXT;
        [_countDownButton setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_countDownButton setTitleColor:COLOR_MAIN forState:UIControlStateNormal];
        [_countDownButton sizeToFit];
        [_countDownButton setTitleColor:COLOR_SUBTEXT forState:UIControlStateDisabled];
        _countDownButton.enabled = NO;
        _countDownButton.jk_touchAreaInsets = UIEdgeInsetsMake(20, 20, 20, 20);
        __weak typeof(self) weakSelf = self;
        [_countDownButton countDownButtonHandler:^(JKCountDownButton*sender, NSInteger tag) {
            sender.enabled = NO;
            
            if ([weakSelf.phoneTextField.text isEqualToString:@"13333333333"]) {
                [sender startCountDownWithSecond:59];
                [sender countDownChanging:^NSString *(JKCountDownButton *countDownButton,NSUInteger second) {
                    NSString *title = [NSString stringWithFormat:@"%zds",second];
                    if (weakSelf.phoneTextField.text.length < 11) {
                        [weakSelf.countDownButton stopCountDown];
                    }
                    return title;
                }];
                [sender countDownFinished:^NSString *(JKCountDownButton *countDownButton, NSUInteger second) {
                    countDownButton.enabled = YES;
                    return @"重新获取";
                }];
            } else {
                if (!validateMobile(weakSelf.phoneTextField.text)) {
                    [WSProgressHUD showErrorWithStatus:@"手机号输入错误"];
                    return;
                }
                [weakSelf.codeTextField becomeFirstResponder];
                [NetworkSingleton login_getCmsCodeWithDict:@{@"cellphone" : weakSelf.phoneTextField.text, @"type" : @0} response:^(BOOL result, NSString *errMsg) {
                    if (result) {
                        [sender startCountDownWithSecond:59];
                        
                        [sender countDownChanging:^NSString *(JKCountDownButton *countDownButton,NSUInteger second) {
                            NSString *title = [NSString stringWithFormat:@"%zds",second];
                            if (weakSelf.phoneTextField.text.length < 11) {
                                [weakSelf.countDownButton stopCountDown];
                            }
                            return title;
                        }];
                        [sender countDownFinished:^NSString *(JKCountDownButton *countDownButton, NSUInteger second) {
                            countDownButton.enabled = YES;
                            return @"重新获取";
                        }];
                    } else {
                        [WSProgressHUD showErrorWithStatus:errMsg];
                    }
                }];
            }
        }];
    }
    return _countDownButton;
}

/**
 懒加载
 
 @return 验证码
 */
- (UITextField *)codeTextField
{
    if (!_codeTextField) {
        _codeTextField = [[UITextField alloc] init];
        UIButton *leftView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [leftView setImage:[UIImage imageNamed:@"mine_login_password"] forState:UIControlStateNormal];
        leftView.userInteractionEnabled = NO;
        _codeTextField.leftView = leftView;
        _codeTextField.leftViewMode = UITextFieldViewModeAlways;
        _codeTextField.backgroundColor = COLOR_WHITE;
        _codeTextField.placeholder = @"请输入验证码";
        _codeTextField.font = FONT_TEXT;
        _codeTextField.jk_maxLength = 4;
        _codeTextField.keyboardType = UIKeyboardTypeNumberPad;
        _codeTextField.delegate = self;
    }
    return _codeTextField;
}

/**
 懒加载
 
 @return 输入手机号
 */
- (UITextField *)phoneTextField
{
    if (!_phoneTextField) {
        _phoneTextField = [[UITextField alloc] init];
        UIButton *leftView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [leftView setImage:[UIImage imageNamed:@"mine_login_account"] forState:UIControlStateNormal];
        leftView.userInteractionEnabled = NO;
        _phoneTextField.leftView = leftView;
        _phoneTextField.leftViewMode = UITextFieldViewModeAlways;
        _phoneTextField.backgroundColor = COLOR_WHITE;
        _phoneTextField.placeholder = @"请输入手机号码";
        _phoneTextField.font = FONT_TEXT;
        _phoneTextField.jk_maxLength = 11;
        _phoneTextField.keyboardType = UIKeyboardTypeNumberPad;
        _phoneTextField.delegate = self;
    }
    return _phoneTextField;
}
/**
 懒加载
 
 @return text
 */
- (UILabel *)textLabel {
    if (!_textLabel) {
        _textLabel = [[UILabel alloc] init];
        _textLabel.text = @"请输入手机号码登录";
        _textLabel.font = FONT_SUBTEXT;
        _textLabel.textColor = COLOR_SUBTEXT;
    }
    return _textLabel;
}
/**
 懒加载
 
 @return logo图片
 */
- (UIImageView *)logoImageView {
    if (!_logoImageView) {
        _logoImageView = [[UIImageView alloc] init];
        _logoImageView.image = [UIImage imageNamed:@"mine_login_cat"];
    }
    return _logoImageView;
}

/**
 懒加载
 
 @return title
 */
- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = @"登录";
        _titleLabel.textColor = COLOR_TEXT;
        _titleLabel.font = FONT_NAV;
    }
    return _titleLabel;
}
/**
 懒加载
 
 @return 关闭
 */
- (UIButton *)dismissButton {
    if (!_dismissButton) {
        _dismissButton = [[UIButton alloc] init];
        [_dismissButton setImage:[UIImage imageNamed:@"close_gray"] forState:UIControlStateNormal];
        [_dismissButton addTarget:self action:@selector(dismissLoginView) forControlEvents:UIControlEventTouchUpInside];
        _dismissButton.jk_touchAreaInsets = UIEdgeInsetsMake(20, 20, 20, 20);
    }
    return _dismissButton;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
//    NSLog(@"%s", __func__);
}


@end
