//
//  CashBoxSubViewController.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/12/7.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "CashBoxSubViewController.h"

#import "RecordListCell.h"
#import "MJRefresh.h"
#import "NetworkSingleton+Record.h"
#import "GlodTradeModel.h"
#import "RecordDetailsViewController.h"

#import "IncomCell.h"
#import "NetworkSingleton+UserData.h"
#import "IncomeCellModel.h"
#import "IncomePageModel.h"

@interface CashBoxSubViewController ()<UITableViewDelegate, UITableViewDataSource>
/** liebiao */
@property(nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataSource;
/** 当前页 */
@property (nonatomic,assign)NSInteger currentPage;


@end

@implementation CashBoxSubViewController


#pragma mark - 数据请求
- (void)getDataFromNet {
    __weak typeof(self) weakSelf = self;
    NSDictionary *dict = @{
                               @"isBuySell" : [self.type isEqualToString:@"0"] ? @0 : @1,
                               @"pageIndex" : @(self.currentPage)
                           };
    [NetworkSingleton getShopOrderListWithDict:dict Response:^(NSArray<GlodTradeModel *> *result, NSString *errMsg) {
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        if (result.count > 0) {
            [weakSelf.dataSource addObjectsFromArray:result];
        } else {
            if (errMsg.length > 0) {
                [WSProgressHUD showErrorWithStatus:errMsg];
            }
            weakSelf.currentPage -= 1;
        }
        [weakSelf.tableView reloadData];
    }];
}
#pragma mark - 初始化，布局
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = COLOR_TBBACK;
    [self.view addSubview:self.tableView];
    [self.tableView.mj_header beginRefreshing];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RecordListCell  *cell = [RecordListCell cellWithTableview:tableView];
    cell.model = self.dataSource[indexPath.row];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 49 * 2 - NavBarH)];//self.view.bounds
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.rowHeight = 107;
        _tableView.backgroundColor = COLOR_TBBACK;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        __weak typeof(self) weakSelf = self;
        _tableView.mj_header = [MJDIYHeader headerWithRefreshingBlock:^{
            weakSelf.currentPage = 1;
            [weakSelf.dataSource removeAllObjects];
            [weakSelf getDataFromNet];
        }];
        _tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            weakSelf.currentPage += 1;
            [weakSelf getDataFromNet];
        }];
    }
    return _tableView;
}


- (NSMutableArray *)dataSource
{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    return _dataSource;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
