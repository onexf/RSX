//
//  Security center Security center Security center SecurityCenterViewController.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/8/31.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "SecurityCenterViewController.h"
#import "IDVerifyViewController.h"
@interface SecurityCenterViewController ()
/** 实名认证 */
@property(nonatomic, strong) UIView *idVerifiedView;
/** label */
@property(nonatomic, strong) UILabel *idVerifiedLabel;
/** arrow */
@property(nonatomic, strong) UIImageView *arrowImageView;

@end

@implementation SecurityCenterViewController
#pragma mark - 初始化，布局
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = COLOR_TBBACK;
    self.title = @"安全中心";
    [self.view addSubview:self.idVerifiedView];
    [self layout];
}
- (void)layout {
    [self.idVerifiedView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(27);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@(NORMAL_CELL_HEIGHT));
    }];
    [self.idVerifiedLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.idVerifiedView);
        make.left.equalTo(self.idVerifiedView).offset(LEFT_RIGHT_MARGIN);
    }];
    [self.arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.idVerifiedView);
        make.right.equalTo(self.idVerifiedView).offset(-LEFT_RIGHT_MARGIN);
    }];
}
/**
 懒加载
 
 @return 箭头
 */
- (UIImageView *)arrowImageView
{
    if (!_arrowImageView) {
        _arrowImageView = [[UIImageView alloc] init];
        _arrowImageView.image = [UIImage imageNamed:@"icon_nextArrow"];
    }
    return _arrowImageView;
}

/**
 懒加载
 
 @return 实名认证
 */
- (UILabel *)idVerifiedLabel
{
    if (!_idVerifiedLabel) {
        _idVerifiedLabel = [[UILabel alloc] init];
        _idVerifiedLabel.font = FONT_TEXT;
        _idVerifiedLabel.text = @"实名认证";
    }
    return _idVerifiedLabel;
}

/**
 懒加载
 
 @return 白色背景
 */
- (UIView *)idVerifiedView
{
    if (!_idVerifiedView) {
        _idVerifiedView = [[UIView alloc] init];
        _idVerifiedView.backgroundColor = COLOR_WHITE;
        [_idVerifiedView addSubview:self.idVerifiedLabel];
        [_idVerifiedView addSubview:self.arrowImageView];
        __weak typeof(self) weakSelf = self;
        [_idVerifiedView jk_addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
            IDVerifyViewController *idVerifyVC = [[IDVerifyViewController alloc] init];
            [weakSelf.navigationController pushViewController:idVerifyVC animated:YES];
        }];
    }
    return _idVerifiedView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
