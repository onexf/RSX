//
//  RecordListViewController.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/10/26.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>
//不同交易类型
typedef NS_ENUM(NSInteger, RecordType) {
    RecordTypeBalance = 0,//余额变动
    RecordTypeAllGold,//黄金资产
    RecordTypeZhaoCaiJin,//招财金
    RecordTypeShengCaiJin,//生财金
    RecordTypeOtherAssets,//其他资产
    RecordTypeLingQianGuan,//零钱罐
    RecordTypeCunQianGuan,//存钱罐
//    RecordTypeAll//订单记录
};


//此页面只展示交易成功的订单
@interface RecordListViewController : UIViewController
/** 交易类型 */
@property(nonatomic, assign, readonly) RecordType recordType;
@property(nonatomic,assign)BOOL isPaySuccess;

/**
 初始化页面

 @param recordType 交易类型
 @return 页面
 */
- (instancetype)initWithType:(RecordType)recordType;
@end
