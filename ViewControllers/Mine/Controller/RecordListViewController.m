//
//  RecordListViewController.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/10/26.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "RecordListViewController.h"
#import "RecordListCell.h"
#import "RecordSubListViewController.h"
#import "MJRefresh.h"
#import "NetworkSingleton+Record.h"
#import "GlodTradeModel.h"
#import "RecordDetailsViewController.h"
#import <FDFullscreenPopGesture/UINavigationController+FDFullscreenPopGesture.h>
#import "TypeButton.h"
@interface RecordListViewController ()<UITableViewDelegate, UITableViewDataSource>
/** UItableView */
@property(nonatomic, strong) UITableView *tableView;
/** 数据 */
@property(nonatomic, strong) NSMutableArray *dataSource;
/** scrollow */
@property(nonatomic, strong) UIScrollView *contentView;
/** 未过期 */
@property (nonatomic, strong) TypeButton *walfareBtn;
/** 已过期 */
@property (nonatomic, strong) TypeButton *activityBtn;
/** 当前选中的按钮 */
@property (nonatomic, weak) UIButton *selectedButton;
/** 当前页 */
@property (nonatomic, assign) NSInteger currentPage;
/** -1 全部、 0 未到期、 1 已到期 */
@property (nonatomic, strong) NSNumber *isExpire;
/** 列表类型 0：余额变动列表；1：黄金资产交易记录；2：招财金交易列表；3：生财金交易记录；4：其他资产交易记录；5：零钱罐交易记录；6：存钱罐交易记录；7：订单记录； */
@property(nonatomic, strong) NSNumber *listType;

@end

@implementation RecordListViewController
//初始化
- (instancetype)initWithType:(RecordType)recordType {
    if (self = [super init]) {
        _recordType = recordType;
    }
    return self;
}
#pragma mark - 数据请求
- (void)getDataFromNet {
    __weak typeof(self) weakSelf = self;
    NSDictionary *dict = @{
                               @"isExpire" : self.isExpire,
                               @"listType" : self.listType,
                               @"pageIndex" : @(self.currentPage)
                           };
    [NetworkSingleton getRecordListWithDict:dict Response:^(NSArray<GlodTradeModel *> *result, NSString *errMsg) {
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        if (result.count > 0) {
            [weakSelf.dataSource addObjectsFromArray:result];
        } else {
            if (errMsg.length > 0) {
                [WSProgressHUD showErrorWithStatus:errMsg];
            }
            weakSelf.currentPage -= 1;
        }
        [weakSelf.tableView reloadData];
    }];
}
#pragma mark - 页面加载
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = COLOR_TBBACK;
    
    //购买成功进入需要特殊处理
    if (self.isPaySuccess)
    {
        UIButton *backBarButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [backBarButton setImage:[UIImage imageNamed:@"navigationButtonReturn"] forState:UIControlStateNormal];
        [backBarButton setImage:[UIImage imageNamed:@"navigationButtonReturnClick"] forState:UIControlStateHighlighted];
        backBarButton.jk_touchAreaInsets = UIEdgeInsetsMake(20, 20, 20, 20);
        backBarButton.imageEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 25);
        [backBarButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backBarButton];
        
        self.fd_interactivePopDisabled = YES;
        
    }
    
    switch (self.recordType) {
        case RecordTypeBalance:
        {
            self.title = @"余额变动记录";
            self.listType = @0;
        }
            break;
        case RecordTypeAllGold:
        {
            self.title = @"黄金资产交易记录";
            self.listType = @1;
        }
            break;
        case RecordTypeZhaoCaiJin:
        {
            self.title = @"招财金交易记录";
            self.listType = @2;
        }
            break;
        case RecordTypeShengCaiJin:
        {
            self.title = @"生财金交易记录";
            self.listType = @3;
        }
            break;
        case RecordTypeOtherAssets:
        {
            self.title = @"其他资产交易记录";
            self.listType = @4;
        }
            break;
        case RecordTypeLingQianGuan:
        {
            self.title = @"零钱罐交易记录";
            self.listType = @5;
        }
            break;
        case RecordTypeCunQianGuan:
        {
            self.title = @"存钱罐交易记录";
            self.listType = @6;
        }
            break;
    }
    //区分页面
    if (self.recordType == RecordTypeShengCaiJin || self.recordType == RecordTypeCunQianGuan) {
        [self setChildViewControllers];
        [self setContent];
    } else {
        self.isExpire = @(-1);
        [self.view addSubview:self.tableView];
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.equalTo(self.view);
            if (@available(iOS 11.0, *)) {
                make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
            } else {
                make.bottom.equalTo(self.view);
            }
        }];
        [self.tableView.mj_header beginRefreshing];
    }
}

#pragma mark - 生财金、存钱罐处理
- (void)titleClick:(UIButton *)button {
    // 修改按钮状态
    self.selectedButton.enabled = YES;
    button.enabled = NO;
    self.selectedButton = button;
    //    滚动
    CGPoint offset = self.contentView.contentOffset;
    if ([button.titleLabel.text isEqualToString:@"未到期"]) {
        offset.x = 0;
        self.isExpire = @0;
    }
    if ([button.titleLabel.text isEqualToString:@"已到期"]) {
        offset.x = self.contentView.ct_width;
        self.isExpire = @1;
    }
    [self.contentView setContentOffset:offset animated:YES];
}
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    if (scrollView == self.contentView) {
        // 当前的索引
        NSInteger index = scrollView.contentOffset.x / scrollView.ct_width;
        // 取出子控制器
        UIViewController *vc = self.childViewControllers[index];
        vc.view.ct_x = scrollView.contentOffset.x;
        vc.view.ct_y = 0; // 设置控制器view的y值为0(默认是20)
        vc.view.ct_height = scrollView.ct_height; // 设置控制器view的height值为整个屏幕的高度(默认是比屏幕高度少个20)
        [scrollView addSubview:vc.view];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self scrollViewDidEndScrollingAnimation:scrollView];
    // 点击按钮
    NSInteger index = scrollView.contentOffset.x / scrollView.ct_width;
    if (index == 0) {
        [self titleClick:self.walfareBtn];
    }
    if (index == 1) {
        [self titleClick:self.activityBtn];
    }
}

- (void)setChildViewControllers {
    RecordSubListViewController *walfareViewController = [[RecordSubListViewController alloc] init];
    walfareViewController.isExpire = @0;
    walfareViewController.listType = self.listType;
    [self addChildViewController:walfareViewController];

    RecordSubListViewController *activityViewController = [[RecordSubListViewController alloc] init];
    activityViewController.isExpire = @1;
    activityViewController.listType = self.listType;
    [self addChildViewController:activityViewController];
    

}
- (void)setContent {
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.view insertSubview:self.contentView atIndex:0];
    [self.view addSubview:self.walfareBtn];
    [self.view addSubview:self.activityBtn];
    // 添加第一个控制器的view
    [self scrollViewDidEndScrollingAnimation:self.contentView];
    
    CGFloat btnWidth = SCREEN_WIDTH / 2.0;
    [self.walfareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(self.view);
        make.height.equalTo(@46);
        make.width.equalTo(@(btnWidth));
    }];
    [self.activityBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view);
        make.top.width.height.equalTo(self.walfareBtn);
    }];
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.walfareBtn.mas_bottom);
        make.left.right.bottom.equalTo(self.view);
    }];
    
    self.isExpire = @0;
    
    self.selectedButton = self.walfareBtn;
    [self titleClick:self.walfareBtn];
}
- (TypeButton *)walfareBtn {
    if (!_walfareBtn) {
        _walfareBtn = [[TypeButton alloc] init];
        _walfareBtn.backgroundColor = COLOR_WHITE;
        [_walfareBtn setTitle:@"未到期" forState:UIControlStateNormal];
        [_walfareBtn setTitle:@"未到期" forState:UIControlStateDisabled];
        [_walfareBtn setTitleColor:COLOR_TEXT forState:UIControlStateNormal];
        [_walfareBtn setTitleColor:COLOR_Red forState:UIControlStateDisabled];
        _walfareBtn.titleLabel.font = FONT_TITLE;
        [_walfareBtn addTarget:self action:@selector(titleClick:) forControlEvents:UIControlEventTouchUpInside];
        _walfareBtn.enabled = NO;
    }
    return _walfareBtn;
}

- (TypeButton *)activityBtn {
    if (!_activityBtn) {
        _activityBtn = [[TypeButton alloc] init];
        _activityBtn.backgroundColor = COLOR_WHITE;
        [_activityBtn setTitle:@"已到期" forState:UIControlStateNormal];
        [_activityBtn setTitle:@"已到期" forState:UIControlStateDisabled];
        [_activityBtn setTitleColor:COLOR_TEXT forState:UIControlStateNormal];
        [_activityBtn setTitleColor:COLOR_Red forState:UIControlStateDisabled];
        _activityBtn.titleLabel.font = FONT_TITLE;
        [_activityBtn addTarget:self action:@selector(titleClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _activityBtn;
}

- (UIScrollView *)contentView {
    if (!_contentView) {
        _contentView = [[UIScrollView alloc] init];
        _contentView.backgroundColor = COLOR_TBBACK;
        _contentView.frame = CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT - 64);
        _contentView.delegate = self;
        _contentView.pagingEnabled = YES;
        _contentView.contentSize = CGSizeMake(_contentView.ct_width * 2, 0);
        _contentView.showsHorizontalScrollIndicator = NO;
    }
    return _contentView;
}

#pragma mark - UItableView
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.dataSource.count > indexPath.row) {
        GlodTradeModel *model = self.dataSource[indexPath.row];
        RecordDetailsViewController *detailsVC = [[RecordDetailsViewController alloc] initWithData:model];
        [self.navigationController pushViewController:detailsVC animated:YES];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RecordListCell *cell = [RecordListCell cellWithTableview:tableView];
    cell.model = self.dataSource[indexPath.row];
    return cell;
}

- (NSMutableArray *)dataSource {
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    return _dataSource;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.rowHeight = 107;
        _tableView.backgroundColor = COLOR_TBBACK;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        __weak typeof(self) weakSelf = self;
        _tableView.mj_header = [MJDIYHeader headerWithRefreshingBlock:^{
            weakSelf.currentPage = 1;
            [weakSelf.dataSource removeAllObjects];
            [weakSelf getDataFromNet];
        }];
        _tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            weakSelf.currentPage += 1;
            [weakSelf getDataFromNet];
        }];


    }
    return _tableView;
}

#pragma mark---购买成功页面进入返回事件
- (void)goBack{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end
