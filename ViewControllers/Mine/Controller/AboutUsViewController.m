//
//  AboutUsViewController.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/8/31.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "AboutUsViewController.h"
#import "CBWebViewController.h"
@interface AboutUsViewController ()<UITableViewDelegate, UITableViewDataSource>

/** tableView */
@property(nonatomic, strong) UITableView *tableView;
/** 列表数据 */
@property(nonatomic, strong) NSArray *dataSource;
/** headerView */
@property(nonatomic, strong) UIView *headerView;
/** 招金猫 */
@property(nonatomic, strong) UIImageView *logoImageView;
/** 版本号 */
@property(nonatomic, strong) UILabel *version;

@end

@implementation AboutUsViewController
#pragma mark - UItableView
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case 0:
        {
            NSDictionary *dict = self.dataSource[indexPath.section][indexPath.row];
            if ([dict[@"title"] isEqualToString:@"客服电话："]) {
                NSMutableString *str=[[NSMutableString alloc] initWithFormat:@"tel:%@", dict[@"subTitle"]];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
            } else {
                UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                pasteboard.string = dict[@"subTitle"];
                NSString *toust = [NSString stringWithFormat:@"%@已复制", dict[@"title"]];
                [WSProgressHUD showSuccessWithStatus:toust];
            }
        }
            break;
            
        default:
        {
            NSDictionary *dict = self.dataSource[indexPath.section][indexPath.row];
            if ([dict[@"title"] isEqualToString:@"用户协议"]) {
                CBWebViewController *webView = [[CBWebViewController alloc] init];
                webView.urlString = @"https://www.zhaojinmao.cn/testwebapp/me/User_agreement/index.html";
                webView.showShareButton = NO;
                [self.navigationController pushViewController:webView animated:YES];
            } else {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/cn/app/id1254137501?mt=8"]];
            }
        }
            break;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataSource[section] count];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *ID = @"aboutus";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:ID];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.font = FONT_TEXT;
        cell.detailTextLabel.font = FONT_TEXT;
    }
    NSDictionary *dict = self.dataSource[indexPath.section][indexPath.row];
    cell.textLabel.text = dict[@"title"];
    cell.detailTextLabel.text = dict[@"subTitle"];
    return cell;
}
#pragma mark - 初始化，布局
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = COLOR_TBBACK;
    self.title = @"关于我们";
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    [self.logoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.headerView);
    }];
    [self.version mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.logoImageView);
        make.top.equalTo(self.logoImageView.mas_bottom).offset(20);
    }];
}
/**
 懒加载
 
 @return 版本
 */
- (UILabel *)version {
    if (!_version) {
        _version = [[UILabel alloc] init];
        _version.textColor = COLOR_TEXT;
        _version.font = FONT_TEXT;
        _version.text = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    }
    return _version;
}
/**
 懒加载
 
 @return logo
 */
- (UIImageView *)logoImageView
{
    if (!_logoImageView) {
        _logoImageView = [[UIImageView alloc] init];
        _logoImageView.image = [UIImage imageNamed:@"mine_login_cat"];
    }
    return _logoImageView;
}
/**
 懒加载
 
 @return 头
 */
- (UIView *)headerView
{
    if (!_headerView) {
        _headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 170)];
        [_headerView addSubview:self.logoImageView];
        [_headerView addSubview:self.version];
    }
    return _headerView;
}
/**
 懒加载
 
 @return 数据
 */
- (NSArray *)dataSource {
    if (!_dataSource) {
        NSArray *array = @[
                           @[
                               @{@"title" : @"客服电话：", @"subTitle" : @"4008-017-862"},
                               @{@"title" : @"微信公众号", @"subTitle" : @"zhaojinmao01"},
                               @{@"title" : @"客服邮箱", @"subTitle" : @"service@zhaojinmao.cn"},
                               @{@"title" : @"官方网站", @"subTitle" : @"www.zhaojinmao.cn"},
                               @{@"title" : @"加入我们", @"subTitle" : @"hr@zhaojinmao.cn"}
                           ],
                           @[
                               @{@"title" : @"App Store评分", @"subTitle" : @""},
                               @{@"title" : @"用户协议", @"subTitle" : @""}
                               ]
                           ];
        _dataSource = [[NSArray alloc] initWithArray:array];
    }
    return _dataSource;
}
/**
 懒加载
 
 @return 列表
 */
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.tableHeaderView = self.headerView;
    }
    return _tableView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
