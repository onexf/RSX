//
//  AllAssetsViewController.m
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/9/7.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "AllAssetsViewController.h"
#import "MineHeaderView.h"
#import "NetworkSingleton+UserData.h"
#import "MineHomePageModel.h"
#import "BalanceBottomeView.h"
#import "MineGoldAssetsView.h"
#import "MineOtherAssetsView.h"
#import "MJRefresh.h"

@interface AllAssetsViewController ()
/** 头部 */
@property(nonatomic, strong) MineHeaderView *headerView;
/** 背景 */
@property(nonatomic, strong) UIScrollView *backGroundView;

/** tabbarView */
@property(nonatomic, strong) UIView *titlesView;
/** 指示器 */
@property (nonatomic, strong) UIView *indicatorView;
/** 黄金 */
@property (nonatomic, strong) UIButton *walfareBtn;
/** 黄金资产 */
@property(nonatomic, strong) UILabel *walfareValue;
/** 余额 */
@property (nonatomic, strong) UIButton *activityBtn;
/** 余额资产 */
@property(nonatomic, strong) UILabel *activityValue;
/** 其他资产 */
@property (nonatomic, strong) UIButton *dryBtn;
/** 其他资产 */
@property(nonatomic, strong) UILabel *dryValue;
/** 选中的按钮 */
@property (nonatomic, weak) UIButton *selectedButton;
/** 余额 */
@property(nonatomic, strong) BalanceBottomeView *balanceView;
/*********************************************************************登录黄金资产******************************************************************/
/** 黄金资产 */
@property(nonatomic, strong) MineGoldAssetsView *goldAssetsView;
/*********************************************************************登录其他资产******************************************************************/
/** 其他资产 */
@property(nonatomic, strong) MineOtherAssetsView *otherAssetsView;

@end

@implementation AllAssetsViewController

#pragma mark - 事件处理
- (void)titleClick:(UIButton *)button {
    // 修改按钮状态
    self.selectedButton.enabled = YES;
    button.enabled = NO;
    self.selectedButton = button;
    // 动画
    [UIView animateWithDuration:0.25 animations:^{
        self.indicatorView.ct_centerX = button.ct_centerX;
    }];
    if ([button.titleLabel.text isEqualToString:@"账户余额"]) {
        //余额
        [self.balanceView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.titlesView.mas_bottom).offset(1);
            make.left.width.equalTo(self.headerView);
            make.height.equalTo(@350);
        }];
        //黄金资产
        [self.goldAssetsView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.width.equalTo(self.balanceView);
            make.left.equalTo(self.balanceView.mas_right);
            make.height.equalTo(@624);
        }];
        //其他资产
        [self.otherAssetsView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.width.equalTo(self.goldAssetsView);
            make.left.equalTo(self.goldAssetsView.mas_right);
            make.height.equalTo(@320);
        }];
        [self.view layoutIfNeeded];
        self.backGroundView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetMaxY(self.balanceView.frame));
    } else if ([button.titleLabel.text isEqualToString:@"黄金资产"]) {
        //余额
        [self.balanceView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.titlesView.mas_bottom).offset(1);
            make.left.equalTo(self.backGroundView).offset(-SCREEN_WIDTH);
            make.width.equalTo(@(SCREEN_WIDTH));
            make.height.equalTo(@350);
        }];
        //黄金资产
        [self.goldAssetsView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.width.equalTo(self.balanceView);
            make.left.equalTo(self.balanceView.mas_right);
            make.height.equalTo(@624);
        }];
        //其他资产
        [self.otherAssetsView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.width.equalTo(self.goldAssetsView);
            make.left.equalTo(self.goldAssetsView.mas_right);
            make.height.equalTo(@320);
        }];
        [self.view layoutIfNeeded];
        self.backGroundView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetMaxY(self.goldAssetsView.frame) + 5);
    } else {
        //余额
        [self.balanceView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.titlesView.mas_bottom).offset(1);
            make.left.equalTo(self.backGroundView).offset(- 2 * SCREEN_WIDTH);
            make.width.equalTo(@(SCREEN_WIDTH));
            make.height.equalTo(@350);
        }];
        //黄金资产
        [self.goldAssetsView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.width.equalTo(self.balanceView);
            make.left.equalTo(self.balanceView.mas_right);
            make.height.equalTo(@624);
        }];
        //其他资产
        [self.otherAssetsView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.width.equalTo(self.goldAssetsView);
            make.left.equalTo(self.goldAssetsView.mas_right);
            make.height.equalTo(@320);
        }];
        [self.view layoutIfNeeded];
        self.backGroundView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetMaxY(self.otherAssetsView.frame));
    }
}
#pragma mark - 数据请求

- (void)getUserInfoData {
    __weak typeof(self) weakSelf = self;
    [NetworkSingleton userData_getMinePageDataResponse:^(MineHomePageModel *userAssetsData, NSString *errMsg) {
        [weakSelf.backGroundView.mj_header endRefreshing];
        if (!errMsg) {
            weakSelf.headerView.userAssets = userAssetsData;
            weakSelf.activityValue.text = [NSString stringWithFormat:@"%@元", userAssetsData.accountBalance];
            weakSelf.walfareValue.text = [NSString stringWithFormat:@"%@克", userAssetsData.goldAssets];
            weakSelf.dryValue.text = [NSString stringWithFormat:@"%@元", userAssetsData.otherAssets];
            weakSelf.balanceView.assetsData = userAssetsData;
        }
    }];
}
- (void)getGoldAssetsData {
    __weak typeof(self) weakSelf = self;
    [NetworkSingleton mine_getGoldAssetsDataResponse:^(GoldAssetsModel *goldAssets, NSString *errMsg) {
        [weakSelf.backGroundView.mj_header endRefreshing];
        weakSelf.goldAssetsView.goldAssets = goldAssets;
    }];
}
- (void)getOtherAssetsData {
    __weak typeof(self) weakSelf = self;
    [NetworkSingleton mine_getOtherAssetsDataResponse:^(OtherAssetsModel *otherAssets, NSString *errMsg) {
        [weakSelf.backGroundView.mj_header endRefreshing];
        weakSelf.otherAssetsView.otherAssetsData = otherAssets;
    }];
}
#pragma mark - 初始化，布局

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"总资产";
    self.view.backgroundColor = COLOR_TBBACK;
    [self.view addSubview:self.backGroundView];
    [self layout];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getUserInfoData];
    [self getGoldAssetsData];
    [self getOtherAssetsData];
}


- (void)layout {
    self.activityBtn.enabled = YES;
    self.walfareBtn.enabled = YES;
    self.dryBtn.enabled = YES;

    [self.backGroundView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    [self.headerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(self.backGroundView);
        make.width.equalTo(@(SCREEN_WIDTH));
        make.height.equalTo(@(283));
    }];
    [self.titlesView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headerView.mas_bottom).offset(-20);
        make.left.width.equalTo(self.headerView);
        make.height.equalTo(@(60));
    }];
    
    CGFloat btnWidth = SCREEN_WIDTH / 3.0;
    [self.activityBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titlesView);
        make.left.equalTo(self.titlesView);
        make.width.equalTo(@(btnWidth));
    }];
    [self.activityValue mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.activityBtn.mas_bottom);
        make.centerX.equalTo(self.activityBtn);
    }];
    [self.walfareBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.activityBtn.mas_right);
        make.top.bottom.width.equalTo(self.activityBtn);
    }];
    [self.walfareValue mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.walfareBtn.mas_bottom);
        make.centerX.equalTo(self.walfareBtn);
    }];
    [self.dryBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.activityBtn);
        make.left.equalTo(self.walfareBtn.mas_right);
        make.width.equalTo(self.walfareBtn);
    }];
    [self.dryValue mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.dryBtn.mas_bottom);
        make.centerX.equalTo(self.dryBtn);
    }];
    [self.indicatorView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@1);
        make.bottom.equalTo(self.titlesView).offset(1);
        if ([self.assetsType isEqualToString:@"1"]) {
            make.left.right.equalTo(self.walfareBtn);
        } else if ([self.assetsType isEqualToString:@"2"]) {
            make.left.right.equalTo(self.dryBtn);
        } else {
            make.left.right.equalTo(self.activityBtn);
        }
    }];
    if ([self.assetsType isEqualToString:@"1"]) {
        self.selectedButton = self.walfareBtn;
        [self titleClick:self.walfareBtn];
    } else if ([self.assetsType isEqualToString:@"2"]) {
        self.selectedButton = self.dryBtn;
        [self titleClick:self.dryBtn];
    } else {
        self.selectedButton = self.activityBtn;
        [self titleClick:self.activityBtn];
    }
}
/**
 懒加载
 
 @return 其他资产
 */
- (MineOtherAssetsView *)otherAssetsView {
    if (!_otherAssetsView) {
        _otherAssetsView = [[MineOtherAssetsView alloc] init];
    }
    return _otherAssetsView;
}
/**
 懒加载
 
 @return 黄金资产
 */
- (MineGoldAssetsView *)goldAssetsView {
    if (!_goldAssetsView) {
        _goldAssetsView = [[MineGoldAssetsView alloc] init];
    }
    return _goldAssetsView;
}
/**
 懒加载
 
 @return 余额
 */
- (BalanceBottomeView *)balanceView {
    if (!_balanceView) {
        _balanceView = [[BalanceBottomeView alloc] init];
    }
    return _balanceView;
}
- (UIView *)indicatorView {
    if (!_indicatorView) {
        _indicatorView = [[UIView alloc] init];
        _indicatorView.backgroundColor = COLOR_Red;
    }
    return _indicatorView;
}
/**
 懒加载
 
 @return 黄金资产
 */
- (UILabel *)walfareValue {
    if (!_walfareValue) {
        _walfareValue = [[UILabel alloc] init];
        _walfareValue.textColor = UIColorFromHex(0XC574);
        _walfareValue.font = FONT_TITLE;
    }
    return _walfareValue;
}
- (UIButton *)walfareBtn {
    if (!_walfareBtn) {
        _walfareBtn = [[UIButton alloc] init];
        [_walfareBtn setTitle:@"黄金资产" forState:UIControlStateNormal];
        [_walfareBtn setTitleColor:COLOR_SUBTEXT forState:UIControlStateNormal];
        [_walfareBtn setTitleColor:UIColorFromHex(0XC574) forState:UIControlStateDisabled];
        _walfareBtn.titleLabel.font = FONT_TITLE;
        [_walfareBtn addTarget:self action:@selector(titleClick:) forControlEvents:UIControlEventTouchUpInside];
        _walfareBtn.jk_touchAreaInsets = UIEdgeInsetsMake(10, 0, 30, 0);

    }
    return _walfareBtn;
}
/**
 懒加载
 
 @return 其他资产
 */
- (UILabel *)dryValue {
    if (!_dryValue) {
        _dryValue = [[UILabel alloc] init];
        _dryValue.font = FONT_TITLE;
        _dryValue.textColor = UIColorFromHex(0x007eff);
    }
    return _dryValue;
}
- (UIButton *)dryBtn {
    if (!_dryBtn) {
        _dryBtn = [[UIButton alloc] init];
        [_dryBtn setTitle:@"其他资产" forState:UIControlStateNormal];
        [_dryBtn setTitleColor:COLOR_SUBTEXT forState:UIControlStateNormal];
        [_dryBtn setTitleColor:UIColorFromHex(0x007eff) forState:UIControlStateDisabled];
        _dryBtn.titleLabel.font = FONT_TITLE;
        [_dryBtn addTarget:self action:@selector(titleClick:) forControlEvents:UIControlEventTouchUpInside];
        _dryBtn.jk_touchAreaInsets = UIEdgeInsetsMake(10, 0, 30, 0);

    }
    return _dryBtn;
}
/**
 懒加载
 
 @return 余额
 */
- (UILabel *)activityValue {
    if (!_activityValue) {
        _activityValue = [[UILabel alloc] init];
        _activityValue.font = FONT_TITLE;
        _activityValue.textColor = COLOR_Red;
    }
    return _activityValue;
}
- (UIButton *)activityBtn {
    if (!_activityBtn) {
        _activityBtn = [[UIButton alloc] init];
        [_activityBtn setTitle:@"账户余额" forState:UIControlStateNormal];
        [_activityBtn setTitleColor:COLOR_SUBTEXT forState:UIControlStateNormal];
        [_activityBtn setTitleColor:COLOR_Red forState:UIControlStateDisabled];
        _activityBtn.titleLabel.font = FONT_TITLE;
        [_activityBtn addTarget:self action:@selector(titleClick:) forControlEvents:UIControlEventTouchUpInside];
        _activityBtn.jk_touchAreaInsets = UIEdgeInsetsMake(10, 0, 30, 0);
    }
    return _activityBtn;
}
- (UIView *)titlesView {
    if (!_titlesView) {
        _titlesView = [[UIView alloc] init];
        _titlesView.backgroundColor = COLOR_WHITE;
        [_titlesView addSubview:self.walfareBtn];
        [_titlesView addSubview:self.walfareValue];
        [_titlesView addSubview:self.activityBtn];
        [_titlesView addSubview:self.activityValue];
        [_titlesView addSubview:self.dryBtn];
        [_titlesView addSubview:self.dryValue];
        [_titlesView addSubview:self.indicatorView];
    }
    return _titlesView;
}
/**
 懒加载
 
 @return 头部
 */
- (MineHeaderView *)headerView {
    if (!_headerView) {
        _headerView = [[MineHeaderView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 283)];
//        _headerView.tapDelegate = self;
        _headerView.isMineHomeChart = NO;
        _headerView.assetsIsShow = YES;
    }
    return _headerView;
}
/**
 懒加载
 
 @return 背景
 */
- (UIScrollView *)backGroundView {
    if (!_backGroundView) {
        _backGroundView = [[UIScrollView alloc] init];
        _backGroundView.backgroundColor = COLOR_TBBACK;
        [_backGroundView addSubview:self.headerView];
        [_backGroundView addSubview:self.titlesView];
        [_backGroundView addSubview:self.balanceView];
        [_backGroundView addSubview:self.goldAssetsView];
        [_backGroundView addSubview:self.otherAssetsView];
        __weak typeof(self) weakSelf = self;
        _backGroundView.mj_header = [MJDIYHeader headerWithRefreshingBlock:^{
            [weakSelf getUserInfoData];
            [weakSelf getGoldAssetsData];
            [weakSelf getOtherAssetsData];
        }];
    }
    return _backGroundView;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
