//
//  SellOtherSuccessViewController.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/8.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SellOtherSuccessViewController : UIViewController
/** 赎回金额 */
@property(nonatomic, copy) NSString *sellAmountValue;

@end
