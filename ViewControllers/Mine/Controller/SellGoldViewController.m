//
//  SellGoldViewController.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/7.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "SellGoldViewController.h"
#import "UIView+Extension.h"
#import "NetworkSingleton+Home.h"
#import "NetworkSingleton+UserData.h"
#import "GoldAssetsModel.h"
#import "SellGoldSuccessViewController.h"
#import "SellOutModel.h"
#import "VerifyTradePWDTool.h"

@interface SellGoldViewController ()<UITextFieldDelegate>
/** 金价 */
@property(nonatomic, strong) UIView *goldPriceView;
/** 黄金价格 */
@property(nonatomic, strong) UILabel *goldPriceLabel;
/** 实时金价 */
@property(nonatomic, strong) UILabel *currentText;
/** userGold */
@property(nonatomic, strong) UILabel *userTotalGold;
/** sellview */
@property(nonatomic, strong) UIView *sellView;
/** sellweight */
@property(nonatomic, strong) UITextField *sellWeight;
/** 分割线 */
@property(nonatomic, strong) UIView *sepLine;
/** sellAmount */
@property(nonatomic, strong) UITextField *sellAmount;
/** 警告图 */
@property(nonatomic, strong) UIImageView *warnImageView;
/** 查看卖出提示 */
@property(nonatomic, strong) UILabel *alertTips;
/** 确定卖出 */
@property(nonatomic, strong) UIButton *confirmButton;
/** 用户总克重 */
@property(assign) CGFloat totalWeight;
/** 金价 */
@property(nonatomic, copy) NSString *currentPrice;


@end

@implementation SellGoldViewController
#pragma mark - 事件处理
- (void)computeWithdisplayString:(NSString *)displayString {
    if (displayString.floatValue > self.totalWeight || displayString.floatValue < 0.001) {
        self.confirmButton.enabled = NO;
    } else {
        self.confirmButton.enabled = YES;
    }
    if (displayString.length <=0) {
        displayString = @"0";
    }
    NSString *amount=[PriceTool numberA:self.currentPrice MultiplyNumberB:displayString withRoundingStyle:NSRoundDown decimalNum:2];
    self.sellAmount.text = amount;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *displayString=nil;
    static NSInteger pointNum=0;
    if (textField.text.length<=0)
    {
        pointNum=0;  //此处针对输入过小数，然后又消去小数的处理
        //首位不能输入.
        if ([string isEqualToString:@"."])
        {
            return NO;
        }
        displayString=string;
    }
    else
    {
        if (range.location==0)
        {
            //针对输入很多位，返回第一位输入。的情况
            if ([string isEqualToString:@"."])
            {
                return NO;
            }
        }
        if (![textField.text containsString:@"."]) {
            //此处针对输入过小数，然后又消去小数的处理
            pointNum=0;
        }
        if ([string isEqualToString:@"."]) {
            //针对返回头部输入小数点 （重量只能输入小数点后三位小数，金额小数点后两位）
            NSString *subText=[textField.text substringFromIndex:range.location];
            //按重量
            if ((range.location<textField.text.length)&&(subText.length>3)) {
                
                return NO;
            }
            //不能输入多个.
            if (pointNum==1)
            {
                return NO;
            }
            pointNum++;
        }
        else
        {
            //已经有一个.，但此次输入的不是.
            if (pointNum==1)
            {
                NSRange pointRange=[textField.text rangeOfString:@"."];
                //按重量
                if (range.location-pointRange.location>3) {
                    return NO;
                }
            }
        }
        if (range.length>0)
        {
            NSMutableString *contentStr=[[NSMutableString alloc]initWithString:textField.text];
            [contentStr deleteCharactersInRange:range];
            displayString=contentStr;
        }
        else
        {
            NSMutableString *contentStr=[[NSMutableString alloc]initWithString:textField.text];
            [contentStr insertString:string atIndex:range.location];
            displayString=contentStr;
        }
    }
    [self computeWithdisplayString:displayString];
    return YES;
}
- (void)sellGoldOut {
    [self.view endEditing:YES];
    NSString *errMsg = nil;
    if (self.sellWeight.text.floatValue > self.totalWeight) {
        errMsg = @"可卖出黄金不足";
    }
    if (self.sellWeight.text.floatValue < 0.001) {
        errMsg = @"卖出黄金克数过低";
    }
    if (errMsg) {
        [WSProgressHUD showErrorWithStatus:errMsg];
        return;
    }
    
    [VerifyTradePWDTool verifyTradePwdOnViewController:self response:^(BOOL result, NSString *errMsg) {
        if (result) {
            NSDictionary *dict = @{
                                   @"gold" : @(self.sellWeight.text.floatValue)
                                   };
            __weak typeof(self) weakSelf = self;
            [WSProgressHUD showWithMaskType:WSProgressHUDMaskTypeBlack];
            [NetworkSingleton mine_sellAssetWithDict:dict Response:^(SellOutModel *result, NSString *errMsg) {
                [WSProgressHUD dismiss];
                if (errMsg) {
                    [WSProgressHUD showErrorWithStatus:errMsg];
                } else {
                    [WSProgressHUD showSuccessWithStatus:@"提交成功"];
                    SellGoldSuccessViewController *sellGoldSuccessVC = [[SellGoldSuccessViewController alloc] init];
                    sellGoldSuccessVC.sellOutData = result;
                    [weakSelf.navigationController pushViewController:sellGoldSuccessVC animated:YES];
                }
            }];
        }
    }];

}
- (void)showTips {
    [TipsTool showTipsWithTitle:@"招财金卖出" andContent:@"1.买入无任何手续费，卖出手续费每笔为1元/克。\n\n2.招金猫买卖金线上交易时间段：每周周一至周五早9:00-2:30（次日）。\n\n3.由于金价实时变动，实际卖出收入会与预计卖出收入有所偏差，卖出金价以实际卖出时的实时金价为准。\n\n4.点击下一步后，请在60秒内完成交易。"];
}

#pragma mark - 数据请求
- (void)getCurrentGoldPrice {
    __weak typeof(self) weakSelf = self;
    [NetworkSingleton home_allListRefreshGoldPriceWithResponse:^(NSString *goldPrice, NSString *total, NSString *errMsg) {
        if (!errMsg) {
            weakSelf.goldPriceLabel.text = goldPrice;
            weakSelf.currentPrice = goldPrice;
        }
    }];
    [NetworkSingleton mine_getGoldAssetsDataResponse:^(GoldAssetsModel *goldAssets, NSString *errMsg) {
        self.totalWeight = goldAssets.recruitFinance.floatValue;
        NSString *string = [NSString stringWithFormat:@"可卖出黄金%@克", goldAssets.recruitFinance];
        NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:string];
        [text addAttributes:@{NSForegroundColorAttributeName:COLOR_Red} range:NSMakeRange(5, goldAssets.recruitFinance.length)];
        self.userTotalGold.attributedText = text;
    }];
}
#pragma mark - 初始化、布局
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getCurrentGoldPrice];
}
- (void)layout {
    [self.goldPriceView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(10);
        make.left.equalTo(self.view).offset(LEFT_RIGHT_MARGIN);
        make.right.equalTo(self.view).offset(-LEFT_RIGHT_MARGIN);
        make.height.equalTo(@140);
    }];
    [self.goldPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.goldPriceView);
        make.bottom.equalTo(self.goldPriceView.mas_centerY);
    }];
    [self.currentText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.goldPriceLabel);
        make.top.equalTo(self.goldPriceLabel.mas_bottom).offset(5);
    }];
    [self.userTotalGold mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goldPriceView.mas_bottom).offset(10);
        make.left.equalTo(self.view).offset(LEFT_RIGHT_MARGIN);
    }];
    [self.sellView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userTotalGold.mas_bottom).offset(10);
        make.left.right.equalTo(self.goldPriceView);
        make.height.equalTo(@101);
    }];
    [self.sellWeight mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.equalTo(self.sellView);
        make.left.equalTo(self.sellView).offset(LEFT_RIGHT_MARGIN);
        make.height.equalTo(@50);
    }];
    [self.sepLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.sellWeight.mas_bottom);
        make.left.right.equalTo(self.sellView);
        make.height.equalTo(@1);
    }];
    [self.sellAmount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.sepLine.mas_bottom);
        make.left.height.right.equalTo(self.sellWeight);
    }];
    [self.warnImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.sellView.mas_bottom).offset(15);
        make.left.equalTo(self.sellView);
    }];
    [self.alertTips mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.warnImageView);
        make.left.equalTo(self.warnImageView.mas_right).offset(10);
    }];
    [self.confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.alertTips.mas_bottom).offset(55);
        make.left.right.equalTo(self.sellView);
        make.height.equalTo(@50);
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = COLOR_TBBACK;
    self.title = @"卖出招财金";
    
    [self.view addSubview:self.goldPriceView];
    [self.view addSubview:self.userTotalGold];
    [self.view addSubview:self.sellView];
    [self.view addSubview:self.warnImageView];
    [self.view addSubview:self.alertTips];
    [self.view addSubview:self.confirmButton];
    [self layout];
}
/**
 懒加载
 
 @return 确认卖出
 */
- (UIButton *)confirmButton {
    if (!_confirmButton) {
        _confirmButton = [[UIButton alloc] init];
        [_confirmButton setBackgroundImage:[UIImage jk_imageWithColor:COLOR_MAIN] forState:UIControlStateNormal];
        [_confirmButton setBackgroundImage:[UIImage jk_imageWithColor:COLOR_BUTTON_DISABLE] forState:UIControlStateDisabled];
        [_confirmButton setTitle:@"确定卖出" forState:UIControlStateNormal];
        [_confirmButton setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
        _confirmButton.titleLabel.font = FONT_BUTTON_TEXT;
        _confirmButton.layer.cornerRadius = 10.0f;
        _confirmButton.clipsToBounds = YES;
        _confirmButton.enabled = NO;
        [_confirmButton addTarget:self action:@selector(sellGoldOut) forControlEvents:UIControlEventTouchUpInside];
    }
    return _confirmButton;
}
/**
 懒加载
 
 @return 查看
 */
- (UILabel *)alertTips {
    if (!_alertTips) {
        _alertTips = [[UILabel alloc] init];
        _alertTips.font = FONT_SUBTEXT;
        _alertTips.textColor = COLOR_MAIN;
        _alertTips.text = @"查看卖出提示";
        [_alertTips addTapAction:@selector(showTips) target:self];
    }
    return _alertTips;
}
/**
 懒加载
 
 @return warnImage
 */
- (UIImageView *)warnImageView {
    if (!_warnImageView) {
        _warnImageView = [[UIImageView alloc] init];
        _warnImageView.image = [UIImage imageNamed:@"icon_warn"];
        [_warnImageView addTapAction:@selector(showTips) target:self];
    }
    return _warnImageView;
}
/**
 懒加载
 
 @return 预计金额
 */
- (UITextField *)sellAmount {
    if (!_sellAmount) {
        _sellAmount = [[UITextField alloc] init];
        UILabel *name = [[UILabel alloc] init];
        name.textColor = COLOR_TEXT;
        name.text = @"预计金额    ";
        name.font = FONT_TEXT;
        [name jk_adjustLabel];
        _sellAmount.leftViewMode = UITextFieldViewModeAlways;
        _sellAmount.leftView = name;
        _sellAmount.placeholder = @"0.00元";
        _sellAmount.font = FONT_TEXT;
        _sellAmount.enabled = NO;
        
    }
    return _sellAmount;
}

/**
 懒加载
 
 @return sep
 */
- (UIView *)sepLine {
    if (!_sepLine) {
        _sepLine = [[UIView alloc] init];
        _sepLine.backgroundColor = COLOR_LINE;
    }
    return _sepLine;
}

/**
 懒加载
 
 @return 卖出的克重
 */
- (UITextField *)sellWeight {
    if (!_sellWeight) {
        _sellWeight = [[UITextField alloc] init];
        UILabel *name = [[UILabel alloc] init];
        name.textColor = COLOR_TEXT;
        name.text = @"卖出克重    ";
        name.font = FONT_TEXT;
        [name jk_adjustLabel];
        _sellWeight.leftViewMode = UITextFieldViewModeAlways;
        _sellWeight.leftView = name;
        _sellWeight.placeholder = @"请输入卖出克重";
        _sellWeight.font = FONT_TEXT;
        _sellWeight.keyboardType = UIKeyboardTypeDecimalPad;
        _sellWeight.delegate = self;
    }
    return _sellWeight;
}

/**
 懒加载
 
 @return 卖金
 */
- (UIView *)sellView {
    if (!_sellView) {
        _sellView = [[UIView alloc] init];
        _sellView.backgroundColor = COLOR_WHITE;
        _sellView.layer.cornerRadius = 10;
        _sellView.clipsToBounds = YES;
        [_sellView addSubview:self.sellWeight];
        [_sellView addSubview:self.sepLine];
        [_sellView addSubview:self.sellAmount];
    }
    return _sellView;
}

/**
 懒加载
 
 @return 可卖出的黄金
 */
- (UILabel *)userTotalGold {
    if (!_userTotalGold) {
        _userTotalGold = [[UILabel alloc] init];
        _userTotalGold.font = FONT_SUBTEXT;
        _userTotalGold.text = @"可卖出黄金0.00克";
        _userTotalGold.textColor = COLOR_SUBTEXT;
    }
    return _userTotalGold;
}

/**
 懒加载
 
 @return text
 */
- (UILabel *)currentText {
    if (!_currentText) {
        _currentText = [[UILabel alloc] init];
        _currentText.font = FONT_SUBTEXT;
        _currentText.textColor = COLOR_SUBTEXT;
        _currentText.text = @"实时金价（元/克）";
    }
    return _currentText;
}

/**
 懒加载
 
 @return 金价
 */
- (UILabel *)goldPriceLabel {
    if (!_goldPriceLabel) {
        _goldPriceLabel = [[UILabel alloc] init];
        _goldPriceLabel.textColor = COLOR_Red;
        _goldPriceLabel.font = FONT(30);
        _goldPriceLabel.text = @"";
    }
    return _goldPriceLabel;
}

/**
 懒加载
 
 @return 金价背景
 */
- (UIView *)goldPriceView {
    if (!_goldPriceView) {//150
        _goldPriceView = [[UIView alloc] init];
        _goldPriceView.layer.cornerRadius = 10;
        _goldPriceView.clipsToBounds = YES;
        _goldPriceView.backgroundColor = COLOR_WHITE;
        [_goldPriceView addSubview:self.goldPriceLabel];
        [_goldPriceView addSubview:self.currentText];
    }
    return _goldPriceView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
