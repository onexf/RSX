//
//  ResetTransPwdViewController.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/11.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "ResetTransPwdViewController.h"
#import "SYPasswordView.h"
#import "NetworkSingleton+UserData.h"
#import "ResetTradePWDViewController.h"
#import "UIView+Extension.h"
#import "UIView+YSTextInputKeyboard.h"

@interface ResetTransPwdViewController ()<PassWordInputViewDelegate, UIScrollViewDelegate>

/** scrollow */
@property(nonatomic, strong) UIScrollView *backgroundView;
/** topView */
@property(nonatomic, strong) UIView *topView;
/** 修改密码 */
@property(nonatomic, strong) UILabel *resetLabel;
/** 箭头 */
@property(nonatomic, strong) UIButton *arrowImage;
/** 底部 */
@property(nonatomic, strong) UIView *bottomView;
/** uilabel */
@property(nonatomic, strong) UILabel *passwordLabel1;
/** 原始密码 */
@property(nonatomic, strong) SYPasswordView *passwordView1;

@property(nonatomic, strong) UILabel *passwordLabel2;
@property(nonatomic, strong) SYPasswordView *passwordView2;

@property(nonatomic, strong) UILabel *passwordLabel3;
@property(nonatomic, strong) SYPasswordView *passwordView3;

/** 设置密码提示 */
@property(nonatomic, strong) UILabel *setPwdWarn;

@property(nonatomic, strong) UILabel *setPwdWarnDetails;
/** 确定 */
@property(nonatomic, strong) UIButton *doneButton;
/** 密码1 */
@property(nonatomic, copy) NSString *pwd1;
/** 密码1 */
@property(nonatomic, copy) NSString *pwd2;
/** 密码1 */
@property(nonatomic, copy) NSString *pwd3;

@end

@implementation ResetTransPwdViewController

- (void)pwd3ViewDidTap {
    [self.passwordView3.textField becomeFirstResponder];
    self.passwordView3.layer.borderColor = COLOR_Red.CGColor;
    self.passwordView1.layer.borderColor = COLOR_MAIN_BLUE.CGColor;
    self.passwordView2.layer.borderColor = COLOR_MAIN_BLUE.CGColor;
}
- (void)pwd2ViewDidTap {
    [self.passwordView2.textField becomeFirstResponder];
    self.passwordView2.layer.borderColor = COLOR_Red.CGColor;
    self.passwordView1.layer.borderColor = COLOR_MAIN_BLUE.CGColor;
    self.passwordView3.layer.borderColor = COLOR_MAIN_BLUE.CGColor;
}
- (void)pwd1ViewDidTap {
    [self.passwordView1.textField becomeFirstResponder];
    self.passwordView1.layer.borderColor = COLOR_Red.CGColor;
    self.passwordView2.layer.borderColor = COLOR_MAIN_BLUE.CGColor;
    self.passwordView3.layer.borderColor = COLOR_MAIN_BLUE.CGColor;
}

- (void)inputDone:(SYPasswordView *)passwordView {
    if (passwordView == self.passwordView1) {
        [self.passwordView2.textField becomeFirstResponder];
        self.passwordView2.layer.borderColor = COLOR_Red.CGColor;
        self.passwordView1.layer.borderColor = COLOR_MAIN_BLUE.CGColor;
        self.passwordView3.layer.borderColor = COLOR_MAIN_BLUE.CGColor;
    }
    if (passwordView == self.passwordView2) {
        [self.passwordView3.textField becomeFirstResponder];
        self.passwordView3.layer.borderColor = COLOR_Red.CGColor;
        self.passwordView1.layer.borderColor = COLOR_MAIN_BLUE.CGColor;
        self.passwordView2.layer.borderColor = COLOR_MAIN_BLUE.CGColor;
    }
    if (passwordView == self.passwordView3) {
        [self.passwordView3.textField resignFirstResponder];
        self.passwordView3.layer.borderColor = COLOR_MAIN_BLUE.CGColor;
    }
}

- (void)passwordView:(SYPasswordView *)passwordView inputPassword:(NSString *)password {
    if (passwordView == self.passwordView1) {
        self.pwd1 = password;
    } else if (passwordView == self.passwordView2) {
        self.pwd2 = password;
    } else {
        self.pwd3 = password;
    }
    self.doneButton.enabled = [self.pwd3 isEqualToString:self.pwd2] && self.pwd2.length == 6 && self.pwd1.length == 6;
}
#pragma mark - 事件处理
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}
- (void)doneButtonDidTap {
    NSDictionary *dict = @{
                               @"oldpwd" : self.pwd1,
                               @"pwd" : self.pwd2
                               };
    __weak typeof(self) weakSelf = self;
    [NetworkSingleton mine_changeTradePwd:dict Response:^(BOOL result, NSString *errMsg) {
        if (errMsg) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:errMsg message:nil preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *actionQ = [UIAlertAction actionWithTitle:@"重置密码" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                ResetTradePWDViewController *resetPWD = [[ResetTradePWDViewController alloc] init];
                resetPWD.shouldPopToRoot = YES;
                [self.navigationController pushViewController:resetPWD animated:YES];
            }];
            UIAlertAction *actionC = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
            [alert addAction:actionC];
            [alert addAction:actionQ];
            [self.navigationController presentViewController:alert animated:YES completion:nil];
        } else {
            [WSProgressHUD showSuccessWithStatus:@"修改成功"];
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
}
- (void)viewTapAction {
    [self pactButtonDidTap:self.arrowImage];
}
- (void)pactButtonDidTap:(UIButton *)button {
    [self.view endEditing:YES];
    self.arrowImage.selected = !button.selected;
    self.bottomView.hidden = button.selected;
    if (!button.selected) {
        [self.passwordView1.textField becomeFirstResponder];
        self.passwordView1.layer.borderColor = COLOR_Red.CGColor;
        self.passwordView2.layer.borderColor = COLOR_MAIN_BLUE.CGColor;
        self.passwordView3.layer.borderColor = COLOR_MAIN_BLUE.CGColor;
    }
}
#pragma mark - 接口请求


#pragma mark - 初始化，布局
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = COLOR_TBBACK;
    self.title = @"支付密码";
    [self.view addSubview:self.backgroundView];
    [self layOut];
}

- (void)layOut {
    [self.backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backgroundView).offset(10);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@64);
    }];
    [self.resetLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.topView);
        make.left.equalTo(self.topView).offset(LEFT_RIGHT_MARGIN);
    }];
    [self.arrowImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.topView);
        make.right.equalTo(self.topView).offset(-LEFT_RIGHT_MARGIN);
    }];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.topView.mas_bottom);
        make.left.bottom.right.equalTo(self.view);
    }];
    
    [self.passwordLabel1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bottomView).offset(10);
        make.left.equalTo(self.bottomView).offset(LEFT_RIGHT_MARGIN);
    }];
    [self.passwordView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.passwordLabel1.mas_bottom).offset(10);
        make.left.equalTo(self.passwordLabel1);
        make.right.equalTo(self.bottomView).offset(-LEFT_RIGHT_MARGIN);
        make.height.equalTo(@50);
    }];
    
    [self.passwordLabel2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.passwordView1.mas_bottom).offset(10);
        make.left.equalTo(self.passwordLabel1);
    }];
    [self.passwordView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.passwordLabel2.mas_bottom).offset(10);
        make.left.right.height.equalTo(self.passwordView1);
    }];

    [self.passwordLabel3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.passwordView2.mas_bottom).offset(10);
        make.left.equalTo(self.passwordLabel1);
    }];
    [self.passwordView3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.passwordLabel3.mas_bottom).offset(10);
        make.left.right.height.equalTo(self.passwordView1);
    }];
    [self.setPwdWarn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.passwordView3.mas_bottom).offset(20);
        make.left.equalTo(self.bottomView).offset(44);
    }];
    [self.setPwdWarnDetails mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.setPwdWarn.mas_bottom).offset(10);
        make.left.equalTo(self.setPwdWarn);
        make.right.equalTo(self.bottomView).offset(-44);
    }];
    [self.doneButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.height.equalTo(@49);
        if (@available(iOS 11.0, *)) {
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        } else {
            make.bottom.equalTo(self.view);
        }
    }];

    
    [self.view layoutIfNeeded];
//    NSLog(@"%f", CGRectGetMaxY(self.setPwdWarnDetails.frame));
    self.backgroundView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetMaxY(self.setPwdWarnDetails.frame) + 150);
}
/**
 懒加载
 
 @return 滚动背景
 */
- (UIScrollView *)backgroundView {
    if (!_backgroundView) {
        _backgroundView = [[UIScrollView alloc] init];
        _backgroundView.backgroundColor = COLOR_TBBACK;
        [_backgroundView addSubview:self.topView];
        [_backgroundView addSubview:self.bottomView];
    }
    return _backgroundView;
}
/**
 懒加载
 
 @return 确定
 */
- (UIButton *)doneButton {
    if (!_doneButton) {
        _doneButton = [[UIButton alloc] init];
        _doneButton.titleLabel.font = FONT_BUTTON_TEXT;
        [_doneButton setTitle:@"确定" forState:UIControlStateNormal];
        [_doneButton setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
        [_doneButton setBackgroundImage:[UIImage jk_imageWithColor:COLOR_BUTTON_DISABLE] forState:UIControlStateDisabled];
        [_doneButton setBackgroundImage:[UIImage jk_imageWithColor:COLOR_MAIN] forState:UIControlStateNormal];
        [_doneButton addTarget:self action:@selector(doneButtonDidTap) forControlEvents:UIControlEventTouchUpInside];
        _doneButton.enabled = NO;
    }
    return _doneButton;
}
/**
 懒加载
 
 @return 提示
 */
- (UILabel *)setPwdWarnDetails {
    if (!_setPwdWarnDetails) {
        _setPwdWarnDetails = [[UILabel alloc] init];
        _setPwdWarnDetails.font = FONT_SUBTEXT;
        _setPwdWarnDetails.textColor = COLOR_SUBTEXT;
        NSString *string = @"1、支付密码是您在交易时的身份凭证，交易过程中输入交易密码，我方即认为是您本人操作。请妥善保管您的密码信息。\n2、密码设定为6位数字，如果你在交易时，输入密码错误次数超过3次，我们将冻结您的交易活动，您可以在第二天进行密码修改或找回。";
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:4];
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, string.length)];
        _setPwdWarnDetails.attributedText = attributedString;
        _setPwdWarnDetails.numberOfLines = 0;
    }
    return _setPwdWarnDetails;
}
/**
 懒加载
 
 @return 设置密码提示
 */
- (UILabel *)setPwdWarn {
    if (!_setPwdWarn) {
        _setPwdWarn = [[UILabel alloc] init];
        _setPwdWarn.font = FONT_TEXT;
        _setPwdWarn.text = @"设置密码提示";
        _setPwdWarn.textColor = COLOR_SUBTEXT;
    }
    return _setPwdWarn;
}
/**
 懒加载
 
 @return 原始密码
 */
- (SYPasswordView *)passwordView3 {
    if (!_passwordView3) {
        _passwordView3 = [[SYPasswordView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 24, 50)];
        _passwordView3.delegate = self;
        [_passwordView3 addTapAction:@selector(pwd3ViewDidTap) target:self];
        _passwordView3.textField.kbMoving.kbMovingView = self.view;
        _passwordView3.textField.kbMoving.offset = 45;
    }
    return _passwordView3;
}
/**
 懒加载
 
 @return 请输入原密码
 */
- (UILabel *)passwordLabel3 {
    if (!_passwordLabel3) {
        _passwordLabel3 = [[UILabel alloc] init];
        _passwordLabel3.text = @"请再次输入新密码";
        _passwordLabel3.font = FONT_TEXT;
        _passwordLabel3.textColor = COLOR_SUBTEXT;
    }
    return _passwordLabel3;
}
/**
 懒加载
 
 @return 原始密码
 */
- (SYPasswordView *)passwordView2 {
    if (!_passwordView2) {
        _passwordView2 = [[SYPasswordView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 24, 50)];
        _passwordView2.delegate = self;
        [_passwordView2 addTapAction:@selector(pwd2ViewDidTap) target:self];
    }
    return _passwordView2;
}
/**
 懒加载
 
 @return 请输入原密码
 */
- (UILabel *)passwordLabel2 {
    if (!_passwordLabel2) {
        _passwordLabel2 = [[UILabel alloc] init];
        _passwordLabel2.text = @"输入新密码";
        _passwordLabel2.font = FONT_TEXT;
        _passwordLabel2.textColor = COLOR_SUBTEXT;
    }
    return _passwordLabel2;
}
/**
 懒加载
 
 @return 原始密码
 */
- (SYPasswordView *)passwordView1 {
    if (!_passwordView1) {
        _passwordView1 = [[SYPasswordView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 24, 50)];
        _passwordView1.delegate = self;
        [_passwordView1 addTapAction:@selector(pwd1ViewDidTap) target:self];
    }
    return _passwordView1;
}
/**
 懒加载
 
 @return 请输入原密码
 */
- (UILabel *)passwordLabel1 {
    if (!_passwordLabel1) {
        _passwordLabel1 = [[UILabel alloc] init];
        _passwordLabel1.text = @"请输入原密码";
        _passwordLabel1.font = FONT_TEXT;
        _passwordLabel1.textColor = COLOR_SUBTEXT;
    }
    return _passwordLabel1;
}
/**
 懒加载
 
 @return bottomView
 */
- (UIView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[UIView alloc] init];
        [_bottomView addSubview:self.passwordLabel1];
        [_bottomView addSubview:self.passwordView1];
        [_bottomView addSubview:self.passwordLabel2];
        [_bottomView addSubview:self.passwordView2];
        [_bottomView addSubview:self.passwordLabel3];
        [_bottomView addSubview:self.passwordView3];
        [_bottomView addSubview:self.setPwdWarn];
        [_bottomView addSubview:self.setPwdWarnDetails];
        [_bottomView addSubview:self.doneButton];
        _bottomView.hidden = YES;
    }
    return _bottomView;
}
/**
 懒加载
 
 @return 向下箭头
 */
- (UIButton *)arrowImage {
    if (!_arrowImage) {
        _arrowImage = [[UIButton alloc] init];
        _arrowImage.imageEdgeInsets = UIEdgeInsetsMake(3, 3, 3, 3);
        [_arrowImage setImage:[UIImage imageNamed:@"icon_sez_normal"] forState:UIControlStateSelected];
        [_arrowImage setImage:[UIImage imageNamed:@"icon_sez"] forState:UIControlStateNormal];
        [_arrowImage sizeToFit];
        _arrowImage.selected = YES;
        _arrowImage.timeInterval = 0.1;
        [_arrowImage addTarget:self action:@selector(pactButtonDidTap:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _arrowImage;
}

/**
 懒加载
 
 @return 修改密码
 */
- (UILabel *)resetLabel {
    if (!_resetLabel) {
        _resetLabel = [[UILabel alloc] init];
        _resetLabel.text = @"修改密码";
        _resetLabel.font = FONT_TEXT;
        _resetLabel.textColor = COLOR_TEXT;
    }
    return _resetLabel;
}

/**
 懒加载
 
 @return topView
 */
- (UIView *)topView {
    if (!_topView) {
        _topView = [[UIView alloc] init];
        _topView.backgroundColor = COLOR_WHITE;
        [_topView addSubview:self.resetLabel];
        [_topView addSubview:self.arrowImage];
        [_topView addTapAction:@selector(viewTapAction) target:self];
    }
    return _topView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
