//
//  SellOtherSuccessViewController.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/8.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "SellOtherSuccessViewController.h"
#import <FDFullscreenPopGesture/UINavigationController+FDFullscreenPopGesture.h>

@interface SellOtherSuccessViewController ()

/** topView */
@property(nonatomic, strong) UIView *topView;
/** title */
@property(nonatomic, strong) UILabel *titleLabel;
/** rightBar */
@property(nonatomic, strong) UIButton *rightBarButton;
/** 赎回金额 */
@property(nonatomic, strong) UILabel *sellAmount;
/** name  零钱罐 */
@property(nonatomic, strong) UILabel *nameLabel;
/** midView */
@property(nonatomic, strong) UIView *midView;
/** 赎回金额 */
@property(nonatomic, strong) UILabel *sellAmountRedLabel;
/** 预计到账时间 */
@property(nonatomic, strong) UILabel *timeLabel;
/** 到账时间 */
@property(nonatomic, strong) UILabel *time;
/** warnImage */
@property(nonatomic, strong) UIImageView *imageView;
/** 提示说明 */
@property(nonatomic, strong) UILabel *warnTitle;
/** warnText */
@property(nonatomic, strong) UILabel *warnDetails;
/** 赎回金额 */
@property(nonatomic, strong) NSString *amount;

@end

@implementation SellOtherSuccessViewController

#pragma mark - 事件处理
- (void)sellDone {
    [self.navigationController jk_popToViewControllerWithLevel:2 animated:YES];
}
#pragma mark - 初始化，布局
- (void)layOut {
    [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0, *)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
        } else {
            make.top.equalTo(self.view);
        }
        make.left.right.equalTo(self.view);
        make.height.equalTo(@200);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.topView.mas_top).offset(44);
        make.centerX.equalTo(self.topView);
    }];
    [self.rightBarButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.titleLabel);
        make.right.equalTo(self.topView).offset(-12);
    }];
    [self.sellAmount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.topView).offset(89);
        make.centerX.equalTo(self.topView);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.sellAmount.mas_bottom).offset(25);
        make.centerX.equalTo(self.topView);
    }];
    [self.midView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.topView.mas_bottom).offset(10);
        make.left.equalTo(self.view).offset(LEFT_RIGHT_MARGIN);
        make.right.equalTo(self.view).offset(-LEFT_RIGHT_MARGIN);
        make.height.equalTo(@120);
    }];
    [self.sellAmountRedLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.midView).offset(10);
        make.centerX.equalTo(self.midView);
    }];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.sellAmountRedLabel.mas_bottom).offset(35);
        make.left.equalTo(self.midView).offset(30);
    }];
    [self.time mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.timeLabel);
        make.right.equalTo(self.midView).offset(-30);
    }];
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.midView.mas_bottom).offset(10);
        make.left.equalTo(self.midView).offset(15);
    }];
    [self.warnTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.imageView);
        make.left.equalTo(self.imageView.mas_right).offset(10);
    }];
    [self.warnDetails mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.warnTitle);
        make.top.equalTo(self.warnTitle.mas_bottom).offset(10);
        make.right.equalTo(self.midView);
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.fd_prefersNavigationBarHidden = YES;
    self.fd_interactivePopDisabled = YES;
    self.view.backgroundColor = COLOR_TBBACK;
    
    [self.view addSubview:self.topView];
    [self.view addSubview:self.midView];
    [self.view addSubview:self.imageView];
    [self.view addSubview:self.warnTitle];
    [self.view addSubview:self.warnDetails];
    
    if (@available(iOS 11.0, *)) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 44)];
        view.backgroundColor = COLOR_MAIN;
        [self.view insertSubview:view atIndex:0];
    }

    [self layOut];
    
}

- (void)setSellAmountValue:(NSString *)sellAmountValue {
    _sellAmountValue = sellAmountValue;
    self.amount = [NSString stringWithFormat:@"%.2f", sellAmountValue.floatValue];
}

/**
 懒加载
 
 @return 详细说明
 */
- (UILabel *)warnDetails {
    if (!_warnDetails) {
        _warnDetails = [[UILabel alloc] init];
        _warnDetails.textColor = COLOR_TEXT;
        _warnDetails.font = FONT_SUBTEXT;
        NSString *string = @"根据零钱罐赎回流程，该笔赎回资金将会进入到冻结中，当其他用户购买零钱罐的累计金额大于或等您的赎回金额后，系统将该笔资金发放至您的账户余额中。";
        _warnDetails.numberOfLines = 0;
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:5];
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, string.length)];
        _warnDetails.attributedText = attributedString;
    }
    return _warnDetails;
}

/**
 懒加载
 
 @return 提示说明
 */
- (UILabel *)warnTitle {
    if (!_warnTitle) {
        _warnTitle = [[UILabel alloc] init];
        _warnTitle.font = FONT_SUBTEXT;
        _warnTitle.textColor = COLOR_MAIN;
        _warnTitle.text = @"提示说明";
    }
    return _warnTitle;
}

/**
 懒加载
 
 @return ima
 */
- (UIImageView *)imageView {
    if (!_imageView) {
        _imageView = [[UIImageView alloc] init];
        _imageView.image = [UIImage imageNamed:@"withdrawalsTipIcon"];
    }
    return _imageView;
}

/**
 懒加载
 
 @return 到账时间
 */
- (UILabel *)time {
    if (!_time) {
        _time = [[UILabel alloc] init];
        _time.textColor = COLOR_TEXT;
        _time.font = FONT_TEXT;
        NSDate *date = [NSDate jk_offsetDays:3 fromDate:[NSDate new]];
        _time.text = [date jk_stringWithFormat:@"yyyy年MM月dd日"];
    }
    return _time;
}

/**
 懒加载
 
 @return 预计到账时间
 */
- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.text = @"预计到账时间";
        _timeLabel.textColor = COLOR_TEXT;
        _timeLabel.font = FONT_TEXT;
    }
    return _timeLabel;
}
/**
 懒加载
 
 @return 赎回金额
 */
- (UILabel *)sellAmountRedLabel {
    if (!_sellAmountRedLabel) {
        _sellAmountRedLabel = [[UILabel alloc] init];
        _sellAmountRedLabel.font = FONT_TEXT;
        _sellAmountRedLabel.textColor = COLOR_TEXT;
        NSString *string = [NSString stringWithFormat:@"赎回金额%@元", self.amount];
        NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:string];
        [text addAttributes:@{NSForegroundColorAttributeName:COLOR_Red} range:NSMakeRange(4, self.amount.length)];
        _sellAmountRedLabel.attributedText = text;
    }
    return _sellAmountRedLabel;
}
/**
 懒加载
 
 @return 到账日
 */
- (UIView *)midView {
    if (!_midView) {
        _midView = [[UIView alloc] init];
        _midView.backgroundColor = COLOR_WHITE;
        _midView.layer.cornerRadius = 10.0f;
        _midView.clipsToBounds = YES;
        [_midView addSubview:self.sellAmountRedLabel];
        [_midView addSubview:self.timeLabel];
        [_midView addSubview:self.time];
    }
    return _midView;
}
/**
 懒加载
 
 @return 零钱罐
 */
- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.text = @"零钱罐";
        _nameLabel.textColor = COLOR_SUBTEXT;
        _nameLabel.font = FONT_TEXT;
    }
    return _nameLabel;
}
/**
 懒加载
 
 @return 赎回金额
 */
- (UILabel *)sellAmount {
    if (!_sellAmount) {
        _sellAmount = [[UILabel alloc] init];
        _sellAmount.font = FONT(30);
        _sellAmount.textColor = COLOR_WHITE;
        _sellAmount.text = [NSString stringWithFormat:@"%@元", self.amount];
    }
    return _sellAmount;
}
/**
 懒加载
 
 @return 完成
 */
- (UIButton *)rightBarButton {
    if (!_rightBarButton) {
        _rightBarButton = [[UIButton alloc] init];
        _rightBarButton.titleLabel.font = FONT_TITLE;
        [_rightBarButton setTitle:@"完成" forState:UIControlStateNormal];
        [_rightBarButton setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
        [_rightBarButton addTarget:self action:@selector(sellDone) forControlEvents:UIControlEventTouchUpInside];
        [_rightBarButton sizeToFit];
        _rightBarButton.jk_touchAreaInsets = UIEdgeInsetsMake(20, 20, 20, 20);
    }
    return _rightBarButton;
}
/**
 懒加载
 
 @return 赎回等待
 */
- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = @"赎回等待";
        _titleLabel.textColor = COLOR_WHITE;
        _titleLabel.font = FONT_NAV;
    }
    return _titleLabel;
}
/**
 懒加载
 
 @return 顶部
 */
- (UIView *)topView {
    if (!_topView) {
        _topView = [[UIView alloc] init];
        _topView.backgroundColor = COLOR_MAIN;
        [_topView addSubview:self.titleLabel];
        [_topView addSubview:self.rightBarButton];
        [_topView addSubview:self.sellAmount];
        [_topView addSubview:self.nameLabel];
    }
    return _topView;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
