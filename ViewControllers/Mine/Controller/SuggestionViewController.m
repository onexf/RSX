//
//  SuggestionViewController.m
//  GoldCatBank
//
//  Created by Sunny on 2017/9/6.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "SuggestionViewController.h"
#import "MineNetWork.h"
#define contentMaxCount 500

@interface SuggestionViewController ()<UITextViewDelegate>
{
    UILabel *countLab;
}

@property(nonatomic,strong)UITextView *contentView;
@property(nonatomic,strong)UITextField *nameTxf;
@end

@implementation SuggestionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"意见反馈";
    self.view.backgroundColor=COLOR_TBBACK;
    [self initContentView];
}


-(void)initContentView{
    
    UIView *topVIew=[[UIView alloc]initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH, 50)];
    topVIew.backgroundColor=COLOR_WHITE;
    [self.view addSubview:topVIew];
    
    UITextField *nameTxf=[[UITextField alloc]initWithFrame:CGRectMake(15, 7, SCREEN_WIDTH-20, 35)];
    nameTxf.placeholder=@"请输入手机号";
    nameTxf.textColor=COLOR_TEXT;
    nameTxf.font=FONT(13);
    
    UILabel *leftlab=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 50)];
    leftlab.font=FONT(13);
    leftlab.text=@"联系方式：";
    leftlab.textColor=COLOR_TEXT;
    nameTxf.leftView=leftlab;
    nameTxf.leftViewMode=UITextFieldViewModeAlways;
    
    if (isLogin())
    {
        UserInfoModel *model = [[UserData instance] getUserInfo];
        nameTxf.text = model.cellphoneHide;
    }
    [topVIew addSubview:nameTxf];
    
    
    self.nameTxf=nameTxf;
    
    UIView *textVIewBackVIew=[[UIView alloc]initWithFrame:CGRectMake(10, topVIew.ct_bottom+10, SCREEN_WIDTH-20, 150)];
    textVIewBackVIew.layer.borderWidth=0.5;
    textVIewBackVIew.layer.borderColor=COLOR_LINE.CGColor;
    textVIewBackVIew.layer.masksToBounds=YES;
    textVIewBackVIew.layer.cornerRadius=5;
    textVIewBackVIew.backgroundColor=COLOR_WHITE;
    [self.view addSubview:textVIewBackVIew];
    
    UITextView *contentView=[[UITextView alloc]initWithFrame:CGRectMake(5, 5, textVIewBackVIew.ct_width-10, 140)];
    contentView.backgroundColor=COLOR_WHITE;
    contentView.textColor=COLOR_SUBTEXT;
    contentView.font=FONT(12);
    contentView.delegate=self;
    contentView.bounces=NO;
    contentView.showsHorizontalScrollIndicator=NO;
    contentView.text=@"请输入您的意见和建议，我们会根据您的宝贵意见不断完善，为您的提供更优质服务";
    [textVIewBackVIew addSubview:contentView];
    
    self.contentView=contentView;
    
    countLab=[[UILabel alloc]initWithFrame:CGRectMake(contentView.ct_width-200, textVIewBackVIew.ct_bottom+5, 200, 20)];
    countLab.textColor=COLOR_SUBTEXT;
    countLab.font=FONT(14);
    countLab.text=@"0/500";
    countLab.textAlignment=NSTextAlignmentRight;
    [self.view addSubview:countLab];
    
    
    //二维码
    UIImage *codeImage=[UIImage imageNamed:@"mine_codeIcon"];
    UIImageView *codeImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, codeImage.size.width, codeImage.size.height)];
    codeImageView.center=CGPointMake(SCREEN_WIDTH/2.0, textVIewBackVIew.ct_bottom+40+30);
    codeImageView.image=codeImage;
    [self.view addSubview:codeImageView];
    
    
    UILabel *serviceTimeLab=[[UILabel alloc]initWithFrame:CGRectMake(0, codeImageView.ct_bottom+20, SCREEN_WIDTH,25)];
    serviceTimeLab.text=@"客服时间：9:00～18:00";
    serviceTimeLab.textAlignment=NSTextAlignmentCenter;
    serviceTimeLab.textColor=COLOR_SUBTEXT;
    serviceTimeLab.font=FONT(14);
    [self.view addSubview:serviceTimeLab];
    
    UIButton *phoneBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    phoneBtn.frame=CGRectMake(0, 0, 150, 50);
    phoneBtn.center=CGPointMake(SCREEN_WIDTH/2.0, serviceTimeLab.ct_bottom+20+50/2.0);
    [phoneBtn setTitle:@"电话：4008-017-862" forState:UIControlStateNormal];
    [phoneBtn setTitleColor:COLOR_SUBTEXT forState:UIControlStateNormal];
    phoneBtn.titleLabel.font=FONT(12);
    phoneBtn.layer.borderColor=COLOR_SUBTEXT.CGColor;
    phoneBtn.layer.borderWidth=0.5;
    phoneBtn.layer.masksToBounds=YES;
    phoneBtn.layer.cornerRadius=5;
    [self.view addSubview:phoneBtn];
    [phoneBtn addTarget:self action:@selector(phoneBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
  
    //底部提交按钮
    UIButton *commitBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    commitBtn.frame=CGRectMake(0, SCREEN_HEIGHT-NavBarH-50, SCREEN_WIDTH, 50);
    commitBtn.backgroundColor=COLOR_GLODBACKGROUD;
    [commitBtn setTitle:@"提交" forState:UIControlStateNormal];
    [commitBtn setTitleColor:COLOR_TEXT forState:UIControlStateNormal];
    commitBtn.titleLabel.font=FONT(16);
    [self.view addSubview:commitBtn];
    [commitBtn addTarget:self action:@selector(commitBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    if ([textView.text isEqualToString:@"请输入您的意见和建议，我们会根据您的宝贵意见不断完善，为您的提供更优质服务"])
    {
        textView.text=@"";
        textView.textColor=COLOR_TEXT;
    }
   
    return YES;
}


- (void)textViewDidEndEditing:(UITextView *)textView
{
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if (textView.text.length==contentMaxCount)
    {
        if (range.length==0)
        {
            [WSProgressHUD showErrorWithStatus:@"最多输入500字"];
            return NO;
        }
    }
    
    if (range.location>contentMaxCount-1) {
        
        [WSProgressHUD showErrorWithStatus:@"最多输入500字"];
        return NO;

    }
    

    if (range.length>0)
    {
        countLab.text=[NSString stringWithFormat:@"%ld/500",textView.text.length-1];
    }
    else
    {
        countLab.text=[NSString stringWithFormat:@"%ld/500",textView.text.length+text.length];

    }
   
    return YES;
}


#pragma mark--打电话
-(void)phoneBtnClick:(UIButton *)btn
{
    NSMutableString *str=[[NSMutableString alloc] initWithFormat:@"tel:%@",@"4008-017-862"];
    UIWebView *callWebview = [[UIWebView alloc] init];
    [callWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:str]]];
    [self.view addSubview:callWebview];
}


#pragma mark--提交按钮
-(void)commitBtnClick:(UIButton *)btn
{
   
    if (!isLogin())
    {
        if (self.nameTxf.text.length==0)
        {
         
            self.nameTxf.text=@"";
        }

    }
    else
    {
        if (self.nameTxf.text.length<=0)
        {
            [WSProgressHUD showErrorWithStatus:self.nameTxf.placeholder];
            return;
        }

    }
    
    if ([self.contentView.text isEqualToString:@"请输入您的意见和建议，我们会根据您的宝贵意见不断完善，为您的提供更优质服务"]||self.contentView.text.length<=0)
    {
        
        [WSProgressHUD showErrorWithStatus:@"请输入您的意见和建议，我们会根据您的宝贵意见不断完善，为您的提供更优质服务"];
        return;
        
    }
    
    btn.userInteractionEnabled=NO;
    NSDictionary *dic=@{@"userName":isLogin() ? [[UserData instance] getUserInfo].cellphone : self.nameTxf.text,@"contentDetails":self.contentView.text};
    
    [MineNetWork userFeedBackWithDataDic:dic successBlock:^(id responseBody) {
        
        
        if ([responseBody[@"results"] integerValue]>0)
        {
            self.contentView.text=@"";
            
            [WSProgressHUD showSuccessWithStatus:@"提交成功"];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            if (responseBody[@"errMsg"])
            {
                [WSProgressHUD showErrorWithStatus:responseBody[@"errMsg"]];
                
            }
        }
        
        btn.userInteractionEnabled=YES;
    } failureBlock:^(NSString *error) {
        
        btn.userInteractionEnabled=YES;
    }];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
