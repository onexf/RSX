//
//  CashBoxSubViewController.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/12/7.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CashBoxSubViewController : UIViewController
/** 类型 */
@property(nonatomic, copy) NSString *type;

@end
