//
//  MineViewController.m
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/8/21.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "MineViewController.h"
#import <FDFullscreenPopGesture/UINavigationController+FDFullscreenPopGesture.h>
#import "SettingViewController.h"
#import "UIView+Extension.h"
#import "MIneBottomView.h"
#import "MineHeaderView.h"
#import "NetworkSingleton+UserData.h"
#import "RechargeAndWithdrawView.h"
#import "BalanceTradeVC.h"
#import "SuggestionViewController.h"
#import "AllAssetsViewController.h"
#import "MineHomePageModel.h"
#import "MJRefresh.h"
#import "CBWebViewController.h"
#import "BindBankCarViewController.h"
#import "UserAuthData.h"
#import "MessageHomeViewController.h"
#import "SetChargePasswordViewController.h"

#import "CouponsViewController.h"
#import "CashBoxViewController.h"

@interface MineViewController ()<MineOverageViewDelegate, PieChartViewDelegate>
/*********************************************************************导航栏部分***************************************************************/
/** 导航栏 */
@property(nonatomic, strong) UIView *titleView;
/** 标题 */
@property(nonatomic, strong) UILabel *titleLabel;
/** 右按钮 */
@property(nonatomic, strong) UIButton *rightBarButton;
///** 左按钮 */
//@property(nonatomic, strong) UIButton *leftBarButton;
/** 标题View */
@property(nonatomic, strong) UIView *titleLogin;
/** 电话 */
@property(nonatomic, strong) UILabel *phoneLabel;
/** 图片 */
@property(nonatomic, strong) UILabel *loginImageView;
/** 背景 */
@property(nonatomic, strong) UIScrollView *backGroundView;
/*********************************************************************头部******************************************************************/
/** unloginImage */
@property(nonatomic, strong) UIImageView *unloginImageView;
/**  */
@property(nonatomic, strong) UIButton *unloginLabel;
/** 头部 */
@property(nonatomic, strong) MineHeaderView *headerView;
/*********************************************************************登录余额******************************************************************/
/** tabbarView */
@property(nonatomic, strong) UIView *titlesView;
/** 余额 */
@property(nonatomic, strong) UILabel *balanceLabel;
/** 充值、提现 */
@property(nonatomic, strong) RechargeAndWithdrawView *rechargeView;
/** bottom */
@property(nonatomic, strong) MIneBottomView *mineBottomView;
/** 数据 */
@property(nonatomic, strong) MineHomePageModel *userAssetsData;
/** ****** */
@property(nonatomic, strong) MineHomePageModel *starUserAssetsData;
@end

@implementation MineViewController
- (void)didTapHideViewWithStatus:(BOOL)isHide {
    if (isHide) {
        NSString *balanceStr = [NSString stringWithFormat:@"余额  %@", @"*******"];
        NSMutableAttributedString *balanceAttStr = [[NSMutableAttributedString alloc] initWithString:balanceStr];
        [balanceAttStr addAttribute:NSForegroundColorAttributeName
                              value:COLOR_TEXT
                              range:NSMakeRange(0, 2)];
        [balanceAttStr addAttribute:NSFontAttributeName
                              value:FONT(13)
                              range:NSMakeRange(0, 2)];
        [balanceAttStr addAttribute:NSBaselineOffsetAttributeName
                              value:@(0.36 * (24 - 13)) range:NSMakeRange(0, 2)];
        
        self.balanceLabel.attributedText = balanceAttStr;
        self.rechargeView.earnData = self.starUserAssetsData;
    } else {
        NSString *balanceStr = [NSString stringWithFormat:@"余额  %@元", self.userAssetsData.accountBalance];
        NSMutableAttributedString *balanceAttStr = [[NSMutableAttributedString alloc] initWithString:balanceStr];
        [balanceAttStr addAttribute:NSForegroundColorAttributeName
                              value:COLOR_TEXT
                              range:NSMakeRange(0, 2)];
        [balanceAttStr addAttribute:NSFontAttributeName
                              value:FONT(13)
                              range:NSMakeRange(0, 2)];
        [balanceAttStr addAttribute:NSBaselineOffsetAttributeName
                              value:@(0.36 * (17 - 13)) range:NSMakeRange(0, 2)];

        self.balanceLabel.attributedText = balanceAttStr;
        self.rechargeView.earnData = self.userAssetsData;
    }
}
#pragma mark - 事件处理
- (void)didTapPieChartViewCenter {
    AllAssetsViewController *allAssetsViewController = [[AllAssetsViewController alloc] init];
    [self.navigationController pushViewController:allAssetsViewController animated:YES];
}
//选中余额页面的某一行
- (void)didSelectRowAtIndex:(NSInteger)index {
    switch (index) {
        case 0://交易记录
        {
            if (!isLogin()) {
                [LoginTool loginAction];
                return;
            } else {
                
                BalanceTradeVC *vc=[[BalanceTradeVC alloc]init];
                [self.navigationController pushViewController:vc animated:YES];
            }
        }
            break;
        case 1://卡券中心
        {
            if (!isLogin()) {
                [LoginTool loginAction];
                return;
            } else {
                CouponsViewController *vc=[[CouponsViewController alloc]init];
                [self.navigationController pushViewController:vc animated:YES];
            }
        }
            break;
        case 2://我的邀请
        {
            CBWebViewController *webViewController = [[CBWebViewController alloc] init];
            webViewController.urlString = @"https://www.zhaojinmao.cn/testwebapp/index/Inviting/Inviting.html";
            webViewController.showShareButton = YES;
            [self.navigationController pushViewController:webViewController animated:YES];
        }
            break;
        case 3://意见反馈
        {
            CashBoxViewController *cashBox = [[CashBoxViewController alloc] init];
            [self.navigationController pushViewController:cashBox animated:YES];
        }
            break;
        default://帮助中心
        {
            CBWebViewController *vc=[[CBWebViewController alloc] init];
            vc.urlString = @"https://www.zhaojinmao.cn/testwebapp/me/help_center/help_center.html";
            vc.showShareButton = NO;
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
    }
}
- (void)leftBtnClick:(UIButton *)button {
    if (isLogin()) {
        MessageHomeViewController *messageViewController = [[MessageHomeViewController alloc] init];
        [self.navigationController pushViewController:messageViewController animated:YES];
    } else {
        [LoginTool loginAction];
    }
}

- (void)rightBtnClick:(UIButton *)button {
    if (isLogin()) {
        SettingViewController *settingViewController = [[SettingViewController alloc] init];
        [self.navigationController pushViewController:settingViewController animated:YES];
    } else {
        [LoginTool loginAction];
    }
}
//未登录
- (void)unloginImageViewDidTap {
    [LoginTool loginAction];
}
#pragma mark - 数据请求

- (void)getUserInfoData {
    __weak typeof(self) weakSelf = self;
    [NetworkSingleton userData_getMinePageDataResponse:^(MineHomePageModel *userAssetsData, NSString *errMsg) {
        [weakSelf.backGroundView.mj_header endRefreshing];
        if (!errMsg) {
            weakSelf.userAssetsData = userAssetsData;
            weakSelf.headerView.userAssets = userAssetsData;
            if (isLogin()) {
                weakSelf.mineBottomView.couponCount = userAssetsData.couponCount;
            } else {
                weakSelf.mineBottomView.couponCount = @"";
            }
            if (weakSelf.headerView.assetsIsShow) {
                [weakSelf didTapHideViewWithStatus:NO];
                weakSelf.rechargeView.earnData = userAssetsData;
            } else {
                [weakSelf didTapHideViewWithStatus:YES];
                weakSelf.rechargeView.earnData = weakSelf.starUserAssetsData;
            }
        }
    }];
}
/**
 懒加载
 
 @return ***
 */
- (MineHomePageModel *)starUserAssetsData {
    if (!_starUserAssetsData) {
        _starUserAssetsData = [[MineHomePageModel alloc] init];
        _starUserAssetsData.ydTotalAmount = @"*******";
        _starUserAssetsData.earnTotalAmount = @"*******";
        _starUserAssetsData.goldAssets = @"*******";
        _starUserAssetsData.otherAssets = @"********";
    }
    return _starUserAssetsData;
}
#pragma mark - 初始化，布局

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (isLogin()) {
        [self getUserInfoData];
        self.titleLabel.hidden = YES;
        self.titleLogin.hidden = NO;
        self.phoneLabel.text = [[UserData instance] getUserInfo].cellphoneHide;
    } else {
        self.titleLabel.hidden = NO;
        self.titleLogin.hidden = YES;
        self.mineBottomView.couponCount = @"";
    }
    [self layout];
}
- (void)layout {
    [self.backGroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-49);
    }];
    if (isLogin()) {
        self.unloginImageView.hidden = YES;
        self.headerView.hidden = NO;
        self.titlesView.hidden = NO;
        self.rechargeView.hidden = NO;
        __weak typeof(self) weakSelf = self;
        [[UserAuthData shareInstance] refreshUserInfoResponse:^(UserAuthDataModel *userAuthData, NSString *errMsg) {
            if (userAuthData.isAuthentication.integerValue > 0) {
                weakSelf.loginImageView.hidden = NO;
                [self.titleLogin mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.centerY.equalTo(self.titleLabel);
                    make.centerX.equalTo(self.titleLabel).offset(-25);
                    make.height.equalTo(@20);
                    make.width.equalTo(@250);
                }];
                [self.phoneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.center.equalTo(self.titleLogin);
                }];
                [self.loginImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.centerY.right.equalTo(self.titleLogin);
                    make.left.equalTo(self.phoneLabel.mas_right).offset(7);
                }];


            } else {
                weakSelf.loginImageView.hidden = YES;
                [self.titleLogin mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.center.equalTo(self.titleLabel);
                    make.height.equalTo(@20);
                    make.width.equalTo(@250);
                }];
                [self.phoneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.center.equalTo(self.titleLogin);
                }];
                [self.loginImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.centerY.right.equalTo(self.titleLogin);
                    make.left.equalTo(self.phoneLabel.mas_right).offset(7);
                }];
            }
        }];
        
        [self.headerView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.left.equalTo(self.backGroundView);
            make.width.equalTo(@(SCREEN_WIDTH));
            make.height.equalTo(@(283));
        }];
        [self.titlesView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.headerView.mas_bottom).offset(-10);
            make.left.width.equalTo(self.headerView);
            make.height.equalTo(@40);
        }];
        [self.balanceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.centerX.equalTo(self.titlesView);
        }];
        
        [self.rechargeView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.titlesView.mas_bottom).offset(1);
            make.left.equalTo(self.backGroundView);
            make.width.equalTo(@(SCREEN_WIDTH));
            make.height.equalTo(@259);
        }];
        [self.mineBottomView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.rechargeView.mas_bottom).offset(10);
            make.left.width.equalTo(self.rechargeView);
            make.height.equalTo(@328);
        }];
        [self.view layoutIfNeeded];
        self.backGroundView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetMaxY(self.mineBottomView.frame) + 10);
    } else {
        self.unloginImageView.hidden = NO;
        self.headerView.hidden = YES;
        self.titlesView.hidden = YES;
        self.rechargeView.hidden = YES;
        [self.unloginImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.backGroundView).offset(-1);
            make.left.equalTo(self.backGroundView);
            make.width.equalTo(@(SCREEN_WIDTH));
            make.height.equalTo(@(SCREEN_WIDTH / 2));
        }];
        [self.unloginLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self.unloginImageView);
        }];
        [self.mineBottomView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.unloginImageView.mas_bottom).offset(10);
            make.left.width.equalTo(self.unloginImageView);
            make.height.equalTo(@284);
        }];
        [self.view layoutIfNeeded];
        self.backGroundView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetMaxY(self.mineBottomView.frame) + 10);
    }
}
/**
 懒加载
 
 @return 充值
 */
- (RechargeAndWithdrawView *)rechargeView {
    if (!_rechargeView) {
        _rechargeView = [[RechargeAndWithdrawView alloc] init];
    }
    return _rechargeView;
}
/**
 懒加载
 
 @return balanceLabel
 */
- (UILabel *)balanceLabel {
    if (!_balanceLabel) {
        _balanceLabel = [[UILabel alloc] init];
        _balanceLabel.font = FONT(17);
        _balanceLabel.textColor = COLOR_Red;
    }
    return _balanceLabel;
}
- (UIView *)titlesView {
    if (!_titlesView) {
        _titlesView = [[UIView alloc] init];
        _titlesView.backgroundColor = COLOR_TBBACK;
        [_titlesView addSubview:self.balanceLabel];
    }
    return _titlesView;
}

/**
 懒加载
 
 @return 头部
 */
- (MineHeaderView *)headerView {
    if (!_headerView) {
        _headerView = [[MineHeaderView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 283)];
        _headerView.tapDelegate = self;
        _headerView.isMineHomeChart = YES;
        _headerView.assetsIsShow = YES;
    }
    return _headerView;
}

/**
 懒加载
 
 @return 底部
 */
- (MIneBottomView *)mineBottomView {
    if (!_mineBottomView) {
        _mineBottomView = [[MIneBottomView alloc] init];
        _mineBottomView.viewDelegate = self;
    }
    return _mineBottomView;
}

/**
 懒加载
 
 @return 马上登陆
 */
- (UIButton *)unloginLabel {
    if (!_unloginLabel) {
        _unloginLabel = [[UIButton alloc] init];
        _unloginLabel.backgroundColor = RGBA(77, 77, 77, 0.77);
        _unloginLabel.titleLabel.font = FONT_BUTTON_TEXT;
        [_unloginLabel setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
        [_unloginLabel setTitle:@"马上登录" forState:UIControlStateNormal];
        _unloginLabel.contentEdgeInsets = UIEdgeInsetsMake(10, 20, 10, 20);
        [_unloginLabel addTapAction:@selector(unloginImageViewDidTap) target:self];
        [_unloginLabel sizeToFit];
    }
    return _unloginLabel;
}
/**
 懒加载
 
 @return 未登录状图片
 */
- (UIImageView *)unloginImageView {
    if (!_unloginImageView) {
        _unloginImageView = [[UIImageView alloc] init];
        _unloginImageView.image = [UIImage imageNamed:@"loginImage"];
        [_unloginImageView addTapAction:@selector(unloginImageViewDidTap) target:self];
        [_unloginImageView addSubview:self.unloginLabel];
    }
    return _unloginImageView;
}

/**
 懒加载
 
 @return 背景
 */
- (UIScrollView *)backGroundView {
    if (!_backGroundView) {
        _backGroundView = [[UIScrollView alloc] init];
        _backGroundView.backgroundColor = COLOR_TBBACK;
        [_backGroundView addSubview:self.unloginImageView];
        [_backGroundView addSubview:self.headerView];
        [_backGroundView addSubview:self.mineBottomView];
        [_backGroundView addSubview:self.titlesView];
        [_backGroundView addSubview:self.rechargeView];
        __weak typeof(self) weakSelf = self;
        _backGroundView.mj_header = [MJDIYHeader headerWithRefreshingBlock:^{
            if (isLogin()) {
                [weakSelf getUserInfoData];
            } else {
                [weakSelf.backGroundView.mj_header endRefreshing];
            }
        }];
    }
    return _backGroundView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的";
    UIView *maincolor = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 300)];
    maincolor.backgroundColor = COLOR_MAIN;
    [self.view insertSubview:maincolor atIndex:0];
    self.view.backgroundColor = COLOR_TBBACK;
    self.fd_prefersNavigationBarHidden = YES;
    [self setNavTitle];
    [self.view addSubview:self.backGroundView];
}
- (void)setNavTitle {
    [self.view addSubview:self.titleView];
    
    [self.titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0, *)){
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.height.equalTo(@44);
        } else {
            make.top.equalTo(self.view);
            make.height.equalTo(@64);
        }
        make.left.right.equalTo(self.view);

    }];
//    [self.leftBarButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        if (@available(iOS 11.0, *)) {
//            make.centerY.equalTo(self.titleView);
//        } else {
//            make.centerY.equalTo(self.titleView).offset(10);
//        }
//        make.left.equalTo(self.titleView).offset(LEFT_RIGHT_MARGIN);
//    }];
    [self.rightBarButton mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0, *)) {
            make.centerY.equalTo(self.titleView);
        } else {
            make.centerY.equalTo(self.titleView).offset(10);
        }
        make.right.equalTo(self.titleView).offset(-LEFT_RIGHT_MARGIN);
    }];
    //未登录
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.titleView);
        make.centerY.equalTo(self.rightBarButton);
    }];
}

/**
 懒加载
 
 @return 已认证图片
 */
- (UILabel *)loginImageView
{
    if (!_loginImageView) {
        _loginImageView = [[UILabel alloc] init];
//        _loginImageView.image = [UIImage imageNamed:@"mine_authed"];
        _loginImageView.textColor = COLOR_WHITE;
        _loginImageView.text = @"(已认证)";
        _loginImageView.font = FONT_BOLD(12);
    }
    return _loginImageView;
}

/**
 懒加载
 
 @return 登录的手机号
 */
- (UILabel *)phoneLabel
{
    if (!_phoneLabel) {
        _phoneLabel = [[UILabel alloc] init];
        _phoneLabel.text = @"***********";
        _phoneLabel.textColor = COLOR_WHITE;
        _phoneLabel.font = FONT_NAV;
    }
    return _phoneLabel;
}

/**
 懒加载
 
 @return 登录的标题
 */
- (UIView *)titleLogin
{
    if (!_titleLogin) {
        _titleLogin = [[UIView alloc] init];
        [_titleLogin addSubview:self.phoneLabel];
        [_titleLogin addSubview:self.loginImageView];
    }
    return _titleLogin;
}

/**
 懒加载
 
 @return 消息
 */
//- (UIButton *)leftBarButton {
//    if (!_leftBarButton) {
//        _leftBarButton = [[UIButton alloc] init];
//        [_leftBarButton addTarget:self action:@selector(leftBtnClick:) forControlEvents:UIControlEventTouchUpInside];
//        [_leftBarButton setImage:[UIImage imageNamed:@"mine_message"] forState:UIControlStateNormal];
//        _leftBarButton.jk_touchAreaInsets = UIEdgeInsetsMake(10, 10, 10, 10);
//    }
//    return _leftBarButton;
//}
/**
 懒加载
 
 @return 设置
 */
- (UIButton *)rightBarButton {
    if (!_rightBarButton) {
        _rightBarButton = [[UIButton alloc] init];
        [_rightBarButton addTarget:self action:@selector(rightBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_rightBarButton setImage:[UIImage imageNamed:@"mine_setting"] forState:UIControlStateNormal];
        _rightBarButton.jk_touchAreaInsets = UIEdgeInsetsMake(10, 10, 10, 10);
    }
    return _rightBarButton;
}
/**
 懒加载
 
 @return 标题
 */
- (UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = FONT_NAV;
        _titleLabel.text = @"我的";
        _titleLabel.textColor = COLOR_WHITE;
    }
    return _titleLabel;
}

/**
 懒加载
 
 @return 导航栏
 */
- (UIView *)titleView {
    if (!_titleView) {
        _titleView = [[UIView alloc] init];
        _titleView.backgroundColor = COLOR_MAIN;
        [_titleView addSubview:self.titleLabel];
//        [_titleView addSubview:self.leftBarButton];
        [_titleView addSubview:self.rightBarButton];
        [_titleView addSubview:self.titleLogin];
    }
    return _titleView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
