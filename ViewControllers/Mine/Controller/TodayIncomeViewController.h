//
//  TodayIncomeViewController.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/12.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>
@class IncomePageModel;
@protocol IncomeDataDelegate <NSObject>

- (void)incomeDataDidChange:(IncomePageModel *)incomData;

@end
@interface TodayIncomeViewController : UIViewController

/** 类型 */
@property(nonatomic, copy) NSString *incomType;
/** 代理 */
@property(nonatomic, weak) id<IncomeDataDelegate> incomeDataDelegate;


@end
