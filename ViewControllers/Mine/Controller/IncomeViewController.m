//
//  IncomeViewController.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/12.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "IncomeViewController.h"
#import "TodayIncomeViewController.h"
#import "IncomePageModel.h"
@interface IncomeViewController ()<UIScrollViewDelegate, IncomeDataDelegate>
/** titleView */
@property(nonatomic, strong) UIView *titlesView;
/** 指示器 */
@property (nonatomic, strong) UIView *indicatorView;
/** 累计收益 */
@property (nonatomic, strong) UIButton *walfareBtn;
/** leiji */
@property(nonatomic, strong) UILabel *walfareValue;
/** 昨日 */
@property (nonatomic, strong) UIButton *activityBtn;
/** 昨日收益 */
@property(nonatomic, strong) UILabel *activityValue;
/** 选中的按钮 */
@property (nonatomic, weak) UIButton *selectedButton;
/** 收益更新日期 */
@property(nonatomic, strong) UILabel *dateLabel;
/** 滚动 */
@property (nonatomic, strong) UIScrollView *contentView;
/** 规则 */
@property(nonatomic, strong) UIButton *rightBarButton;

@end

@implementation IncomeViewController
#pragma mark - 事件处理
- (void)incomeRules:(UIButton *)button {
    [TipsTool showTipsWithTitle:@"收益及发放规则" andContent:@"1、 招金猫平台每日收益结算时间是每日03:00。\n\n2、当日投资次日开始进行收益计算的产品（招财金、零钱罐、新手特权金、挖金矿等活动），第三日可在“昨日收益”中显示每日投资收益。\n\n3、用户投资的定期产品（生财金系列、存钱罐系列、特价黄金、新手专享、黄金看涨、看跌），产品到期后的第二日显示定期产品期间内收益。"];
}
- (void)incomeDataDidChange:(IncomePageModel *)incomData {
    if (incomData.yesDayFloatProfit.floatValue == 0) {
        self.activityValue.textColor = COLOR_SUBTEXT;
    } else if (incomData.yesDayFloatProfit.floatValue > 0) {
        self.activityValue.textColor = COLOR_Red;
    } else {
        self.activityValue.textColor = UIColorFromHex(0x16a83f);
    }
    if (incomData.cumulativeProfit.floatValue == 0) {
        self.walfareValue.textColor = COLOR_SUBTEXT;
    } else if (incomData.cumulativeProfit.floatValue > 0) {
        self.walfareValue.textColor = COLOR_Red;
    } else {
        self.walfareValue.textColor = UIColorFromHex(0x16a83f);
    }
    self.activityValue.text = incomData.yesDayFloatProfit;
    self.walfareValue.text = incomData.cumulativeProfit;
    self.dateLabel.text = [NSString stringWithFormat:@"收益更新日期：%@", incomData.updateTime];
}
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    // 当前的索引
    NSInteger index = scrollView.contentOffset.x / scrollView.ct_width;
    // 取出子控制器
    UIViewController *vc = self.childViewControllers[index];
    vc.view.ct_x = scrollView.contentOffset.x;
    vc.view.ct_y = 0; // 设置控制器view的y值为0(默认是20)
    vc.view.ct_height = scrollView.ct_height; // 设置控制器view的height值为整个屏幕的高度(默认是比屏幕高度少个20)
    [scrollView addSubview:vc.view];
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self scrollViewDidEndScrollingAnimation:scrollView];
    // 点击按钮
    NSInteger index = scrollView.contentOffset.x / scrollView.ct_width;
    if (index == 0) {
        [self titleClick:self.activityBtn];
    }
    if (index == 1) {
        [self titleClick:self.walfareBtn];
    }
}

- (void)titleClick:(UIButton *)button {
    self.title = button.titleLabel.text;
    // 修改按钮状态
    self.selectedButton.enabled = YES;
    button.enabled = NO;
    self.selectedButton = button;
    // 动画
    [UIView animateWithDuration:0.25 animations:^{
        self.indicatorView.ct_centerX = button.ct_centerX;
    }];
    // 滚动
    CGPoint offset = self.contentView.contentOffset;
    if ([button.titleLabel.text isEqualToString:@"昨日收益(元)"]) {
        offset.x = 0;
    }
    if ([button.titleLabel.text isEqualToString:@"累计收益(元)"]) {
        offset.x = self.contentView.ct_width;
    }
    [self.contentView setContentOffset:offset animated:YES];
}
#pragma mark - 初始化，布局

- (void)tapTitleButton {
    if ([self.typeString isEqualToString:@"0"]) {
        [self titleClick:self.activityBtn];
    } else {
        [self titleClick:self.walfareBtn];
    }
}

- (void)layOut {
    [self.titlesView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(10);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@75);
    }];
    [self.rightBarButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.titlesView);
        make.centerX.equalTo(self.titlesView).offset(-35);
    }];
    CGFloat btnWidth = SCREEN_WIDTH / 2.0;
    [self.activityBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titlesView).offset(15);
        make.left.equalTo(self.titlesView);
        make.width.equalTo(@(btnWidth));
    }];
    [self.activityValue mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.activityBtn.mas_bottom);
        make.centerX.equalTo(self.activityBtn);
    }];
    [self.walfareBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.activityBtn.mas_right);
        make.top.bottom.width.equalTo(self.activityBtn);
    }];
    [self.walfareValue mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.walfareBtn.mas_bottom);
        make.centerX.equalTo(self.walfareBtn);
    }];
    if ([self.typeString isEqualToString:@"0"]) {
        [self.indicatorView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@1);
            make.bottom.equalTo(self.titlesView).offset(1);
            make.left.right.equalTo(self.activityBtn);
        }];
    } else {
        [self.indicatorView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@1);
            make.bottom.equalTo(self.titlesView).offset(1);
            make.left.right.equalTo(self.walfareBtn);
        }];
    }
    [self.dateLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titlesView.mas_bottom).offset(20);
        make.centerX.equalTo(self.view);
    }];
    
    [self.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.dateLabel.mas_bottom).offset(15);
        make.left.width.equalTo(self.view);
        if (@available(iOS 11.0, *)) {
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        } else {
            make.bottom.equalTo(self.view);
        }
    }];
}
- (void)setChildViewControllers {
    TodayIncomeViewController *todayIncom = [[TodayIncomeViewController alloc] init];
    todayIncom.incomType = @"0";
    todayIncom.incomeDataDelegate = self;
    [self addChildViewController:todayIncom];
    
    TodayIncomeViewController *totalIncom = [[TodayIncomeViewController alloc] init];
    totalIncom.incomType = @"1";
    totalIncom.incomeDataDelegate = self;
    [self addChildViewController:totalIncom];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self.view insertSubview:self.contentView atIndex:0];
    // 添加第一个控制器的view
    [self scrollViewDidEndScrollingAnimation:self.contentView];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.typeString.integerValue == 0) {
        self.title = @"昨日收益";
    } else {
        self.title = @"累计收益";
    }
    self.view.backgroundColor = COLOR_TBBACK;
    
    [self setChildViewControllers];
    
    [self.view addSubview:self.titlesView];
    [self.view addSubview:self.dateLabel];
    [self layOut];
    [self performSelector:@selector(tapTitleButton) withObject:nil afterDelay:0.3];
    
}

- (UIButton *)rightBarButton {
    if (!_rightBarButton) {
        _rightBarButton = [[UIButton alloc] init];
        [_rightBarButton addTarget:self action:@selector(incomeRules:) forControlEvents:UIControlEventTouchUpInside];
        [_rightBarButton setImage:[UIImage imageNamed:@"icon_question"] forState:UIControlStateNormal];
        _rightBarButton.jk_touchAreaInsets = UIEdgeInsetsMake(10, 10, 10, 10);
    }
    return _rightBarButton;
}

- (UIScrollView *)contentView
{
    if (!_contentView) {
        _contentView = [[UIScrollView alloc] init];
        _contentView.delegate = self;
        _contentView.pagingEnabled = YES;
        _contentView.contentSize = CGSizeMake(SCREEN_WIDTH * 2, 0);
        _contentView.showsHorizontalScrollIndicator = NO;
    }
    return _contentView;
}
/**
 懒加载
 
 @return 收益日期
 */
- (UILabel *)dateLabel {
    if (!_dateLabel) {
        _dateLabel = [[UILabel alloc] init];
        _dateLabel.font = FONT_TEXT;
        _dateLabel.textColor = COLOR_TEXT;
//        _dateLabel.text = [NSString stringWithFormat:@"收益更新日期：%@", [NSDate jk_formatYMD:[NSDate new]]];
    }
    return _dateLabel;
}
- (UIView *)indicatorView {
    if (!_indicatorView) {
        _indicatorView = [[UIView alloc] init];
        _indicatorView.backgroundColor = COLOR_Red;
    }
    return _indicatorView;
}
/**
 懒加载
 
 @return 黄金资产
 */
- (UILabel *)walfareValue {
    if (!_walfareValue) {
        _walfareValue = [[UILabel alloc] init];
        _walfareValue.textColor = COLOR_SUBTEXT;
        _walfareValue.font = FONT_TITLE;
        _walfareValue.text = @"0.00(元)";
    }
    return _walfareValue;
}
- (UIButton *)walfareBtn {
    if (!_walfareBtn) {
        _walfareBtn = [[UIButton alloc] init];
        [_walfareBtn setTitle:@"累计收益(元)" forState:UIControlStateNormal];
        [_walfareBtn setTitleColor:COLOR_SUBTEXT forState:UIControlStateNormal];
        [_walfareBtn setTitleColor:COLOR_Red forState:UIControlStateDisabled];
        _walfareBtn.titleLabel.font = FONT_TITLE;
        [_walfareBtn addTarget:self action:@selector(titleClick:) forControlEvents:UIControlEventTouchUpInside];
        _walfareBtn.jk_touchAreaInsets = UIEdgeInsetsMake(0, 0, 20, 0);
    }
    return _walfareBtn;
}
/**
 懒加载
 
 @return 余额
 */
- (UILabel *)activityValue {
    if (!_activityValue) {
        _activityValue = [[UILabel alloc] init];
        _activityValue.font = FONT_TITLE;
        _activityValue.textColor = COLOR_SUBTEXT;
        _activityValue.text = @"0.00(元)";
    }
    return _activityValue;
}
- (UIButton *)activityBtn {
    if (!_activityBtn) {
        _activityBtn = [[UIButton alloc] init];
        [_activityBtn setTitle:@"昨日收益(元)" forState:UIControlStateNormal];
        [_activityBtn setTitleColor:COLOR_SUBTEXT forState:UIControlStateNormal];
        [_activityBtn setTitleColor:COLOR_Red forState:UIControlStateDisabled];
        _activityBtn.titleLabel.font = FONT_TITLE;
        [_activityBtn addTarget:self action:@selector(titleClick:) forControlEvents:UIControlEventTouchUpInside];
        _activityBtn.jk_touchAreaInsets = UIEdgeInsetsMake(0, 0, 20, 0);
    }
    return _activityBtn;
}
- (UIView *)titlesView {
    if (!_titlesView) {
        _titlesView = [[UIView alloc] init];
        _titlesView.backgroundColor = COLOR_WHITE;
        [_titlesView addSubview:self.walfareBtn];
        [_titlesView addSubview:self.walfareValue];
        [_titlesView addSubview:self.activityBtn];
        [_titlesView addSubview:self.activityValue];
        [_titlesView addSubview:self.indicatorView];
        [_titlesView addSubview:self.rightBarButton];
    }
    return _titlesView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
