//
//  IncomeViewController.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/12.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IncomeViewController : UIViewController

/** type */
@property(nonatomic, copy) NSString *typeString;

@end
