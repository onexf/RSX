//
//  GoldPriceRemindViewController.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/8/31.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "GoldPriceRemindViewController.h"
#import "AllNetWorkRquest.h"

@interface GoldPriceRemindViewController ()
/** scrollowView */
@property(nonatomic, strong) UIScrollView *backGroundView;
/** 实时金价背景 */
@property(nonatomic, strong) UIView *currentGoldPriceView;
/** 居中view */
@property(nonatomic, strong) UIView *centerXView;
/** 图片 */
@property(nonatomic, strong) UIImageView *clockImageView;
/** 实时金价 */
@property(nonatomic, strong) UILabel *currentGoldPriceLabel;
/** 中间图 */
@property(nonatomic, strong) UIView *midView;
/** 铃铛 */
@property(nonatomic, strong) UIImageView *bellImageView;
/** * 当金价到达您设置的点位时招金猫会为您推送提醒消息； */
@property(nonatomic, strong) UILabel *label1;
/** * 您可自由选择同时开启两个点位提醒或只提醒单个点位； */
@property(nonatomic, strong) UILabel *label2;
/** * 高位值与低位值分别高于或低于实时金价； */
@property(nonatomic, strong) UILabel *label3;
/** 金价高于货币等于 */
@property(nonatomic, strong) UILabel *goldPriceH;
/** hview */
@property(nonatomic, strong) UIView *hView;
/** priceLabel */
@property(nonatomic, strong) UILabel *priceLabel;
/** hswitch */
@property(nonatomic, strong) UISwitch *hSwitch;
/** 金价低于或等于 */
@property(nonatomic, strong) UILabel *goldPriceL;
/** lowView */
@property(nonatomic, strong) UIView *lView;
/** 输入金额 */
@property(nonatomic, strong) UITextField *inputPrice;
/** lswitch */
@property(nonatomic, strong) UISwitch *lSwitch;

@property(nonatomic, strong) UIButton *shareButton;

@end

@implementation GoldPriceRemindViewController
#pragma mark - 数据请求
- (void)getCurrentGoldPrice {
    __weak typeof(self) weakSelf = self;
    [AllNetWorkRquest allListRefreshGoldPriceWithResponse:^(NSString *goldPrice, NSString *errMsg) {
        if (!errMsg) {
            weakSelf.currentGoldPriceLabel.text = [NSString stringWithFormat:@"实时金价 %@ 元/克", goldPrice];
            weakSelf.priceLabel.text = goldPrice;
        }
    }];

}
#pragma mark - 时间处理
- (void)shareButtonDidTap {
    [WSProgressHUD showSuccessWithStatus:@"设置成功"];
    [self jk_performAfter:1.0 block:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
}
#pragma mark - 初始化，布局
- (void)layout {
    [self.backGroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    [self.currentGoldPriceView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.backGroundView);
        make.height.equalTo(@(NORMAL_CELL_HEIGHT));
    }];
    [self.centerXView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.height.equalTo(self.currentGoldPriceView);
    }];
    [self.clockImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.centerY.equalTo(self.centerXView);
    }];
    [self.currentGoldPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.clockImageView.mas_right).offset(7);
        make.right.centerY.equalTo(self.centerXView);
    }];
    [self.midView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.currentGoldPriceView.mas_bottom);
        make.left.right.equalTo(self.backGroundView);
    }];
    [self.bellImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.midView).offset(47);
        make.left.equalTo(self.midView).offset(37);
        make.bottom.equalTo(self.midView).offset(-47);
    }];
    [self.view layoutIfNeeded];
    CGFloat width = SCREEN_WIDTH - 37 - self.bellImageView.ct_width - 20 - 20;
    [self.label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.midView);
        make.left.equalTo(self.bellImageView.mas_right).offset(20);
        make.width.equalTo(@(width));
    }];
    [self.label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.width.equalTo(self.label2);
        make.bottom.equalTo(self.label2.mas_top).offset(-20);
    }];
    [self.label3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.width.equalTo(self.label2);
        make.top.equalTo(self.label2.mas_bottom).offset(20);
    }];
    [self.goldPriceH mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.midView.mas_bottom).offset(10);
        make.left.equalTo(self.view).offset(LEFT_RIGHT_MARGIN);
    }];
    [self.hView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goldPriceH.mas_bottom).offset(20);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@(NORMAL_CELL_HEIGHT));
    }];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.hView);
        make.left.equalTo(self.hView).offset(LEFT_RIGHT_MARGIN);
    }];
    [self.hSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.hView);
        make.right.equalTo(self.hView).offset(-LEFT_RIGHT_MARGIN);
    }];
    [self.goldPriceL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.hView.mas_bottom).offset(20);
        make.left.equalTo(self.goldPriceH);
    }];
    [self.lView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goldPriceL.mas_bottom).offset(20);
        make.left.right.height.equalTo(self.hView);
    }];
    [self.inputPrice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.lView);
        make.left.equalTo(self.lView).offset(LEFT_RIGHT_MARGIN);
        make.right.equalTo(self.lSwitch.mas_left);
    }];
    [self.lSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.lView);
        make.right.equalTo(self.lView).offset(-LEFT_RIGHT_MARGIN);
    }];
    [self.view layoutIfNeeded];
    self.backGroundView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetMaxY(self.lView.frame) + 30);
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = COLOR_TBBACK;
    self.title = @"金价提醒";
    [self.view addSubview:self.backGroundView];
    [self.view addSubview:self.currentGoldPriceView];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.shareButton];

    [self layout];
    [self getCurrentGoldPrice];
}

/**i
 懒加载
 
 @return 分享按钮
 */
- (UIButton *)shareButton
{
    if (!_shareButton) {
        _shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _shareButton.frame = CGRectMake(0, 0, 44, 44);
        [_shareButton setTitle:@"完成" forState:UIControlStateNormal];
        [_shareButton setTitleColor:COLOR_TEXT forState:UIControlStateNormal];
        _shareButton.titleLabel.font = FONT_TITLE;
        [_shareButton addTarget:self action:@selector(shareButtonDidTap) forControlEvents:UIControlEventTouchUpInside];
        _shareButton.contentEdgeInsets = UIEdgeInsetsMake(0, 12, 0, -12);
    }
    return _shareButton;
}
/**
 懒加载
 
 @return lswitch
 */
- (UISwitch *)lSwitch {
    if (!_lSwitch) {
        _lSwitch = [[UISwitch alloc] init];
        _lSwitch.on = NO;
    }
    return _lSwitch;
}

/**
 懒加载
 
 @return 输入金额
 */
- (UITextField *)inputPrice {
    if (!_inputPrice) {
        _inputPrice = [[UITextField alloc] init];
        _inputPrice.font = FONT_TEXT;
        _inputPrice.placeholder = @"请输入";
    }
    return _inputPrice;
}

/**
 懒加载
 
 @return lview
 */
- (UIView *)lView {
    if (!_lView) {
        _lView = [[UIView alloc] init];
        _lView.backgroundColor = COLOR_WHITE;
        [_lView addSubview:self.inputPrice];
        [_lView addSubview:self.lSwitch];
    }
    return _lView;
}

/**
 懒加载
 
 @return goldPriceL
 */
- (UILabel *)goldPriceL {
    if (!_goldPriceL) {
        _goldPriceL = [[UILabel alloc] init];
        _goldPriceL.text = @"金价低于或等于";
        _goldPriceL.textColor = COLOR_TEXT;
        _goldPriceL.font = FONT_TITLE;
    }
    return _goldPriceL;
}
/**
 懒加载
 
 @return 低于开关
 */
- (UISwitch *)hSwitch {
    if (!_hSwitch) {
        _hSwitch = [[UISwitch alloc] init];
        _hSwitch.on = NO;
    }
    return _hSwitch;
}

/**
 懒加载
 
 @return priceLabel
 */
- (UILabel *)priceLabel {
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc] init];
        _priceLabel.font = FONT_TEXT;
        _priceLabel.textColor = COLOR_TEXT;
        _priceLabel.text = @"";
    }
    return _priceLabel;
}

/**
 懒加载
 
 @return hview
 */
- (UIView *)hView {
    if (!_hView) {
        _hView = [[UIView alloc] init];
        _hView.backgroundColor = COLOR_WHITE;
        [_hView addSubview:self.priceLabel];
        [_hView addSubview:self.hSwitch];
    }
    return _hView;
}

/**
 懒加载
 
 @return 高于
 */
- (UILabel *)goldPriceH {
    if (!_goldPriceH) {
        _goldPriceH = [[UILabel alloc] init];
        _goldPriceH.text = @"金价高于或等于";
        _goldPriceH.textColor = COLOR_TEXT;
        _goldPriceH.font = FONT_TITLE;
    }
    return _goldPriceH;
}

/**
 懒加载
 
 @return label3
 */
- (UILabel *)label3 {
    if (!_label3) {
        _label3 = [[UILabel alloc] init];_label1.font = FONT_SUBTEXT;
        _label3.textColor = COLOR_SUBTEXT;
        _label3.text = @"* 高位值与低位值分别高于或低于实时金价；";
        _label3.numberOfLines = 0;
        _label3.font = FONT_SUBTEXT;
    }
    return _label3;
}

/**
 懒加载
 
 @return label2
 */
- (UILabel *)label2 {
    if (!_label2) {
        _label2 = [[UILabel alloc] init];_label1.font = FONT_SUBTEXT;
        _label2.textColor = COLOR_SUBTEXT;
        _label2.font = FONT_SUBTEXT;
        _label2.text = @"* 您可自由选择同时开启两个点位提醒或只提醒单个点位；";
        _label2.numberOfLines = 0;
    }
    return _label2;
}

/**
 懒加载
 
 @return label1
 */
- (UILabel *)label1 {
    if (!_label1) {
        _label1 = [[UILabel alloc] init];
        _label1.font = FONT_SUBTEXT;
        _label1.textColor = COLOR_SUBTEXT;
        _label1.text = @"* 当金价到达您设置的点位时招金猫会为您推送提醒消息；";
        _label1.numberOfLines = 0;
    }
    return _label1;
}

/**
 懒加载
 
 @return 铃铛
 */
- (UIImageView *)bellImageView {
    if (!_bellImageView) {
        _bellImageView = [[UIImageView alloc] init];
        _bellImageView.image = [UIImage imageNamed:@"icon_gold_remind"];
    }
    return _bellImageView;
}

/**
 懒加载
 
 @return midView
 */
- (UIView *)midView {
    if (!_midView) {
        _midView = [[UIView alloc] init];
        _midView.backgroundColor = COLOR_MAIN;
        [_midView addSubview:self.bellImageView];
        [_midView addSubview:self.label1];
        [_midView addSubview:self.label2];
        [_midView addSubview:self.label3];
    }
    return _midView;
}

/**
 懒加载
 
 @return 实时金价
 */
- (UILabel *)currentGoldPriceLabel {
    if (!_currentGoldPriceLabel) {
        _currentGoldPriceLabel = [[UILabel alloc] init];
        _currentGoldPriceLabel.textColor = COLOR_WHITE;
        _currentGoldPriceLabel.font = FONT_BUTTON_TEXT;
        _currentGoldPriceLabel.text = @"";
    }
    return _currentGoldPriceLabel;
}

/**
 懒加载
 
 @return 时钟图
 */
- (UIImageView *)clockImageView {
    if (!_clockImageView) {
        _clockImageView = [[UIImageView alloc] init];
        _clockImageView.image = [UIImage imageNamed:@"icon_gold_pricemind"];
    }
    return _clockImageView;
}
/**
 懒加载
 
 @return 居中
 */
- (UIView *)centerXView {
    if (!_centerXView) {
        _centerXView = [[UIView alloc] init];
        [_centerXView addSubview:self.clockImageView];
        [_centerXView addSubview:self.currentGoldPriceLabel];
    }
    return _centerXView;
}

/**
 懒加载
 
 @return 金价背景
 */
- (UIView *)currentGoldPriceView {
    if (!_currentGoldPriceView) {
        _currentGoldPriceView = [[UIView alloc] init];
        _currentGoldPriceView.backgroundColor = COLOR_Red;
        [_currentGoldPriceView addSubview:self.centerXView];
    }
    return _currentGoldPriceView;
}

/**
 懒加载
 
 @return 背景
 */
- (UIScrollView *)backGroundView {
    if (!_backGroundView) {
        _backGroundView = [[UIScrollView alloc] init];
        _backGroundView.backgroundColor = COLOR_TBBACK;
        [_backGroundView addSubview:self.currentGoldPriceView];
        [_backGroundView addSubview:self.midView];
        [_backGroundView addSubview:self.goldPriceH];
        [_backGroundView addSubview:self.hView];
        [_backGroundView addSubview:self.goldPriceL];
        [_backGroundView addSubview:self.lView];
    }
    return _backGroundView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
