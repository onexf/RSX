//
//  RecordDetailsViewController.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/10/27.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "RecordDetailsViewController.h"
#import "GlodTradeModel.h"
#import "NetworkSingleton+Record.h"
#import "RecordDetailModel.h"
#import "BuyTopView.h"
#import "BuyMidView.h"
#import "BuyBottomView.h"
#import "UIView+Extension.h"
#import "CBWebViewController.h"
#import "FlowCashViewController.h"
#import "AllNetWorkRquest.h"
#import "BuyOtherTopView.h"
#import "BuyDaoQiBottomView.h"
#import "RechargeTopView.h"
#import "GoBuyTool.h"

@interface RecordDetailsViewController ()
/** 详情 */
@property(nonatomic, strong) RecordDetailModel *detail;
/** 背景 */
@property(nonatomic, strong) UIScrollView *bgView;
/** 交易时间 */
@property(nonatomic, strong) UILabel *payTimeLabel;

/** 下单头部 */
@property(nonatomic, strong) BuyTopView *buyTopView;
/** 提示 */
@property(nonatomic, strong) UILabel *goldOrderWarn;
/** 下单中间 */
@property(nonatomic, strong) BuyMidView *buyMidView;
/** 下单底部 */
@property(nonatomic, strong) BuyBottomView *buyBottomView;
/** 再下一单 */
@property(nonatomic, strong) UIButton *buyButton;
/** 协议 */
@property(nonatomic, strong) UILabel *protocolLabel;
/** 投资 */
@property(nonatomic, strong) BuyOtherTopView *buyOtherTopView;
/** 到期底部 */
@property(nonatomic, strong) BuyDaoQiBottomView *daoqiBottomView;
/** 充值 */
@property(nonatomic, strong) RechargeTopView *rechargeTopView;
/** server */
@property(nonatomic, strong) UILabel *serverPhone;
@end

@implementation RecordDetailsViewController
- (void)protocolDetail {
    CBWebViewController *webViewController = [[CBWebViewController alloc] init];
    webViewController.urlString = self.detail.protocolUrl;
    webViewController.showShareButton = NO;
    [self.navigationController pushViewController:webViewController animated:YES];
}
- (void)buyAgain {
    __weak typeof(self) weakSelf = self;
    [WSProgressHUD showWithMaskType:WSProgressHUDMaskTypeBlack];
    [AllNetWorkRquest productDetailWithProductId:@(self.detail.productId).stringValue response:^(ProductDetailModel *model, NSString *errMsg) {
        [WSProgressHUD dismiss];
        if (errMsg) {
            [WSProgressHUD showErrorWithStatus:errMsg];
        } else {
            UIViewController *vc=[GoBuyTool goBuyChoosePageWithDetailModel:model];
        
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }
    }];
}
- (instancetype)initWithData:(GlodTradeModel *)model {
    if (self = [super init]) {
        _model = model;
        _logType = model.logType.integerValue;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = COLOR_TBBACK;
    self.title = [NSString stringWithFormat:@"订单详情-%@", self.model.logTitle];
    [self getDetailData];
}
- (void)layOut {
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.right.equalTo(self.view);
    }];
    [self.payTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bgView);
        make.centerX.equalTo(self.view);
        make.height.equalTo(@44);
    }];
    switch (self.logType) {
        case RecordLogTypeTouZi:
        {
            [self.buyOtherTopView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.payTimeLabel.mas_bottom);
                make.left.right.equalTo(self.view);
            }];
            [self.buyMidView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.buyOtherTopView.mas_bottom).offset(15);
                make.left.right.equalTo(self.view);
            }];
            [self.buyBottomView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.buyMidView.mas_bottom).offset(10);
                make.left.right.equalTo(self.view);
            }];
            [self.buyButton mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.buyBottomView.mas_bottom).offset(40);
                make.centerX.equalTo(self.view);
            }];
            [self.protocolLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.buyButton.mas_bottom).offset(40);
                make.centerX.equalTo(self.view);
            }];
        }
            break;
        case RecordLogTypeGoldIn:
        {
            [self.buyTopView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.payTimeLabel.mas_bottom);
                make.left.right.equalTo(self.view);
            }];
            [self.goldOrderWarn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.buyTopView.mas_bottom).offset(15);
                make.left.equalTo(self.view).offset(LEFT_RIGHT_MARGIN);
                make.right.equalTo(self.view).offset(-LEFT_RIGHT_MARGIN);
            }];
            [self.buyMidView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.goldOrderWarn.mas_bottom).offset(15);
                make.left.right.equalTo(self.view);
            }];
            [self.buyBottomView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.buyMidView.mas_bottom).offset(10);
                make.left.right.equalTo(self.view);
            }];
            [self.buyButton mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.buyBottomView.mas_bottom).offset(40);
                make.centerX.equalTo(self.view);
            }];
            [self.protocolLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.buyButton.mas_bottom).offset(40);
                make.centerX.equalTo(self.view);
            }];
        }
            break;
        case RecordLogTypeGoldOut:
        {
            [self.buyTopView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.payTimeLabel.mas_bottom);
                make.left.right.equalTo(self.view);
            }];
            [self.buyBottomView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.buyTopView.mas_bottom).offset(10);
                make.left.right.equalTo(self.view);
            }];
        }
            break;
        case RecordLogTypeShuHui:
        {
            [self.buyOtherTopView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.payTimeLabel.mas_bottom);
                make.left.right.equalTo(self.view);
            }];
            [self.buyBottomView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.buyOtherTopView.mas_bottom).offset(10);
                make.left.right.equalTo(self.view);
            }];
        }
            break;
        case RecordLogTypeChongZhi:
        {
            [self.rechargeTopView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.payTimeLabel.mas_bottom);
                make.left.right.equalTo(self.view);
            }];
        }
            break;
        case RecordLogTypeTiXian:
        {
            [self.rechargeTopView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.payTimeLabel.mas_bottom);
                make.left.right.equalTo(self.view);
            }];
            [self.daoqiBottomView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.rechargeTopView.mas_bottom).offset(10);
                make.left.right.equalTo(self.view);
            }];
            [self.serverPhone mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.daoqiBottomView.mas_bottom).offset(40);
                make.left.right.equalTo(self.view);
            }];
        }
            break;
        case RecordLogTypeDaoQi:
        {
            if (self.detail.isGoldProduct == 0) {
                [self.buyOtherTopView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(self.payTimeLabel.mas_bottom);
                    make.left.right.equalTo(self.view);
                }];
                [self.buyMidView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(self.buyOtherTopView.mas_bottom).offset(15);
                    make.left.right.equalTo(self.view);
                }];
                [self.buyBottomView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(self.buyMidView.mas_bottom).offset(10);
                    make.left.right.equalTo(self.view);
                }];
                [self.daoqiBottomView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(self.buyBottomView.mas_bottom).offset(10);
                    make.left.right.equalTo(self.view);
                }];
            } else {
                [self.buyTopView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(self.payTimeLabel.mas_bottom);
                    make.left.right.equalTo(self.view);
                }];
                [self.goldOrderWarn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(self.buyTopView.mas_bottom).offset(15);
                    make.left.equalTo(self.view).offset(LEFT_RIGHT_MARGIN);
                    make.right.equalTo(self.view).offset(-LEFT_RIGHT_MARGIN);
                }];
                [self.buyMidView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(self.goldOrderWarn.mas_bottom).offset(15);
                    make.left.right.equalTo(self.view);
                }];
                [self.buyBottomView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(self.buyMidView.mas_bottom).offset(10);
                    make.left.right.equalTo(self.view);
                }];
                [self.daoqiBottomView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(self.buyBottomView.mas_bottom).offset(10);
                    make.left.right.equalTo(self.view);
                }];
            }
        }
            break;
        case RecordLogTypeOther:
        {
            [self.buyTopView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.payTimeLabel.mas_bottom);
                make.left.right.equalTo(self.view);
            }];
        }
            break;
    }
    
}
- (void)getDetailData {
    NSDictionary *dict = @{
                               @"tranId" : self.model.ID,
                               @"logType" : @(self.logType)
                           };
    __weak typeof(self) weakSelf = self;
    [NetworkSingleton getRecordDetailWithDict:dict Response:^(RecordDetailModel *detail, NSString *errMsg) {
        if (errMsg) {
            [WSProgressHUD showErrorWithStatus:errMsg];
        } else {
            weakSelf.detail = detail;
            [self.view addSubview:self.bgView];
            [self layOut];
            [weakSelf setData];
        }
    }];
}
- (void)setData {
    self.payTimeLabel.text = [NSString stringWithFormat:@"交易时间：%@", self.detail.payTime];
    switch (self.logType) {
        case RecordLogTypeTouZi:
        {
            self.buyOtherTopView.model = self.detail;
            self.buyMidView.model = self.detail;
            self.buyBottomView.model = self.detail;
            [self.view layoutIfNeeded];
            if (self.detail.orderStatus == 1) {
                self.bgView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetMaxY(self.protocolLabel.frame) + 40);
            } else {
                [self.buyButton removeFromSuperview];
                [self.protocolLabel removeFromSuperview];
                self.bgView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetMaxY(self.buyBottomView.frame) + 40);
            }
        }
            break;
        case RecordLogTypeGoldIn:
        {
            self.buyTopView.model = self.detail;
            self.buyMidView.model = self.detail;
            self.buyBottomView.model = self.detail;
            [self.view layoutIfNeeded];
            if (self.detail.orderStatus == 1) {
                self.bgView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetMaxY(self.protocolLabel.frame) + 40);
            } else {
                [self.buyButton removeFromSuperview];
                [self.protocolLabel removeFromSuperview];
                self.bgView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetMaxY(self.buyBottomView.frame) + 40);
            }
        }
            break;
        case RecordLogTypeGoldOut:
        {
            self.buyTopView.model = self.detail;
            self.buyBottomView.model = self.detail;
        }
            break;
        case RecordLogTypeShuHui:
        {
            self.buyOtherTopView.model = self.detail;
            self.buyBottomView.model = self.detail;
        }
            break;
        case RecordLogTypeChongZhi:
        {
            self.rechargeTopView.model = self.detail;
        }
            break;
        case RecordLogTypeTiXian:
        {
            self.rechargeTopView.model = self.detail;
            self.daoqiBottomView.model = self.detail;
            [self.view layoutIfNeeded];
            self.bgView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetMaxY(self.serverPhone.frame) + 40);
        }
            break;
        case RecordLogTypeDaoQi:
        {
            if (self.detail.isGoldProduct == 0) {
                self.buyOtherTopView.model = self.detail;
                self.buyMidView.model = self.detail;
                self.buyBottomView.model = self.detail;
                self.daoqiBottomView.model = self.detail;
                [self.view layoutIfNeeded];
                self.bgView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetMaxY(self.daoqiBottomView.frame) + 40);
            } else {
                self.buyTopView.model = self.detail;
                self.buyMidView.model = self.detail;
                self.buyBottomView.model = self.detail;
                self.daoqiBottomView.model = self.detail;
                [self.view layoutIfNeeded];
                self.bgView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetMaxY(self.daoqiBottomView.frame) + 40);
            }
        }
            break;
        case RecordLogTypeOther:
        {
            self.buyTopView.model = self.detail;
        }
            break;
    }

}
/**
 懒加载
 
 @return 再下一单
 */
- (UIButton *)buyButton {
    if (!_buyButton) {
        _buyButton = [[UIButton alloc] init];
        [_buyButton setTitle:@"再下一单" forState:UIControlStateNormal];
        [_buyButton jk_setBackgroundColor:COLOR_Red forState:UIControlStateNormal];
        [_buyButton setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
        _buyButton.titleLabel.font = FONT_TITLE;
        _buyButton.contentEdgeInsets = UIEdgeInsetsMake(15, 30, 15, 30);
        [_buyButton addTarget:self action:@selector(buyAgain) forControlEvents:UIControlEventTouchUpInside];
        _buyButton.layer.cornerRadius = 3;
        _buyButton.clipsToBounds = YES;
    }
    return _buyButton;
}
/**
 懒加载
 
 @return 协议
 */
- (UILabel *)protocolLabel {
    if (!_protocolLabel) {
        _protocolLabel = [[UILabel alloc] init];
        NSString *string = @"点击查看投资协议";
        _protocolLabel.textColor = COLOR_SUBTEXT;
        _protocolLabel.font = FONT_TEXT;
        NSDictionary *attribtDic = @{
                                         NSUnderlineStyleAttributeName : [NSNumber numberWithInteger:NSUnderlineStyleSingle]
                                     };
        NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc]initWithString:string attributes:attribtDic];
        _protocolLabel.attributedText = attribtStr;
        [_protocolLabel addTapAction:@selector(protocolDetail) target:self];
    }
    return _protocolLabel;
}
/**
 懒加载
 
 @return buyBottomView
 */
- (BuyBottomView *)buyBottomView {
    if (!_buyBottomView) {
        _buyBottomView = [[BuyBottomView alloc] init];
    }
    return _buyBottomView;
}
/**
 懒加载
 
 @return buyMid
 */
- (BuyMidView *)buyMidView {
    if (!_buyMidView) {
        _buyMidView = [[BuyMidView alloc] init];
    }
    return _buyMidView;
}
/**
 懒加载
 
 @return goldOrderWarn
 */
- (UILabel *)goldOrderWarn {
    if (!_goldOrderWarn) {
        _goldOrderWarn = [[UILabel alloc] init];
        _goldOrderWarn.textColor = COLOR_SUBTEXT;
        _goldOrderWarn.font = FONT_SUBTEXT;
        _goldOrderWarn.text = @"由于金价实时变动，交易完成时的金价可能与确认订单时金价不同。";
        _goldOrderWarn.numberOfLines = 0;
        _goldOrderWarn.textAlignment = NSTextAlignmentCenter;
    }
    return _goldOrderWarn;
}
/**
 懒加载
 
 @return 购买头部
 */
- (BuyTopView *)buyTopView {
    if (!_buyTopView) {
        _buyTopView = [[BuyTopView alloc] init];
    }
    return _buyTopView;
}

/**
 懒加载
 
 @return buyOtherTopView
 */
- (BuyOtherTopView *)buyOtherTopView {
    if (!_buyOtherTopView) {
        _buyOtherTopView = [[BuyOtherTopView alloc] init];
    }
    return _buyOtherTopView;
}
/**
 懒加载
 
 @return daoqiBottomView
 */
- (BuyDaoQiBottomView *)daoqiBottomView {
    if (!_daoqiBottomView) {
        _daoqiBottomView = [[BuyDaoQiBottomView alloc] init];
    }
    return _daoqiBottomView;
}
/**
 懒加载
 
 @return 充值
 */
- (RechargeTopView *)rechargeTopView {
    if (!_rechargeTopView) {
        _rechargeTopView = [[RechargeTopView alloc] init];
    }
    return _rechargeTopView;
}
/**
 懒加载
 
 @return 背景
 */
- (UIScrollView *)bgView {
    if (!_bgView) {
        _bgView = [[UIScrollView alloc] init];
        _bgView.backgroundColor = COLOR_TBBACK;
        [_bgView addSubview:self.payTimeLabel];
        switch (self.logType) {
            case RecordLogTypeTouZi:
            {
                [_bgView addSubview:self.buyOtherTopView];
                [_bgView addSubview:self.buyMidView];
                [_bgView addSubview:self.buyBottomView];
                [_bgView addSubview:self.buyButton];
                [_bgView addSubview:self.protocolLabel];
            }
                break;
            case RecordLogTypeGoldIn:
            {
                [_bgView addSubview:self.buyTopView];
                [_bgView addSubview:self.goldOrderWarn];
                [_bgView addSubview:self.buyMidView];
                [_bgView addSubview:self.buyBottomView];
                [_bgView addSubview:self.buyButton];
                [_bgView addSubview:self.protocolLabel];
            }
                break;
            case RecordLogTypeGoldOut:
            {
                [_bgView addSubview:self.buyTopView];
                [_bgView addSubview:self.buyBottomView];
            }
                break;
            case RecordLogTypeShuHui:
            {
                [_bgView addSubview:self.buyOtherTopView];
                [_bgView addSubview:self.buyBottomView];
            }
                break;
            case RecordLogTypeChongZhi:
            {
                [_bgView addSubview:self.rechargeTopView];
            }
                break;
            case RecordLogTypeTiXian:
            {
                [_bgView addSubview:self.rechargeTopView];
                [_bgView addSubview:self.daoqiBottomView];
                [_bgView addSubview:self.serverPhone];
            }
                break;
            case RecordLogTypeDaoQi:
            {
                if (self.detail.isGoldProduct == 0) {
                    [_bgView addSubview:self.buyOtherTopView];
                    [_bgView addSubview:self.buyMidView];
                    [_bgView addSubview:self.buyBottomView];
                    [_bgView addSubview:self.daoqiBottomView];
                } else {
                    [_bgView addSubview:self.buyTopView];
                    [_bgView addSubview:self.goldOrderWarn];
                    [_bgView addSubview:self.buyMidView];
                    [_bgView addSubview:self.buyBottomView];
                    [_bgView addSubview:self.daoqiBottomView];
                }
            }
                break;
            case RecordLogTypeOther:
            {
                [_bgView addSubview:self.buyTopView];
            }
                break;
        }
    }
    return _bgView;
}
/**
 懒加载
 
 @return 电话
 */
- (UILabel *)serverPhone {
    if (!_serverPhone) {
        _serverPhone = [[UILabel alloc] init];
        _serverPhone.textColor = COLOR_SUBTEXT;
        _serverPhone.font = FONT_TEXT;
        _serverPhone.numberOfLines = 0;
        _serverPhone.textAlignment = NSTextAlignmentCenter;
        switch (self.detail.orderStatus) {
            case 2:
            case 4:
            case 6:
                _serverPhone.text = @"您的提现请求已被拒绝\n\n若有疑问，致电客服\n400-801-7862\n工作时间：9:00-18:00";
                break;
            case 7:
                _serverPhone.text = @"持有黄金是避免资产缩水的有效方法\n\n若有疑问，致电客服\n400-801-7862\n工作时间：9:00-18:00";
                break;
            default:
                _serverPhone.text = @"提现到账时间为2-3个工作日\n\n若有疑问，致电客服\n400-801-7862\n工作时间：9:00-18:00";
                break;
        }
    }
    return _serverPhone;
}
/**
 懒加载
 
 @return 交易时间
 */
- (UILabel *)payTimeLabel {
    if (!_payTimeLabel) {
        _payTimeLabel = [[UILabel alloc] init];
        _payTimeLabel.textColor = COLOR_SUBTEXT;
        _payTimeLabel.font = FONT_SUBTEXT;
    }
    return _payTimeLabel;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
