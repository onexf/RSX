//
//  SellOtherViewController.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/7.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "SellOtherViewController.h"
#import "UIView+Extension.h"
#import "NetworkSingleton+UserData.h"
#import "OtherAssetsModel.h"
#import "NetworkSingleton+UserData.h"
#import "SellOtherSuccessViewController.h"
#import "SellOutModel.h"
#import "VerifyTradePWDTool.h"
@interface SellOtherViewController ()<UITextFieldDelegate>
/** 今日可赎回 */
@property(nonatomic, strong) UILabel *userTotalAmount;
/** 输入赎回金额 */
@property(nonatomic, strong) UITextField *inputView;
/** 警告图 */
@property(nonatomic, strong) UIImageView *warnImageView;
/** 查看卖出提示 */
@property(nonatomic, strong) UILabel *alertTips;
/** 确定卖出 */
@property(nonatomic, strong) UIButton *confirmButton;

@property(assign) CGFloat totalAmount;

@end

@implementation SellOtherViewController
#pragma mark - 事件处理
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *displayString=nil;
    static NSInteger pointNum=0;
    if (textField.text.length<=0)
    {
        pointNum=0;  //此处针对输入过小数，然后又消去小数的处理
        //首位不能输入.
        if ([string isEqualToString:@"."])
        {
            return NO;
        }
        displayString=string;
    }
    else
    {
        if (range.location==0)
        {
            //针对输入很多位，返回第一位输入。的情况
            if ([string isEqualToString:@"."])
            {
                return NO;
            }
        }
        if (![textField.text containsString:@"."]) {
            //此处针对输入过小数，然后又消去小数的处理
            pointNum=0;
        }
        if ([string isEqualToString:@"."]) {
            //针对返回头部输入小数点 （重量只能输入小数点后三位小数，金额小数点后两位）
            NSString *subText=[textField.text substringFromIndex:range.location];
            //按重量
            if ((range.location<textField.text.length)&&(subText.length>2)) {
                
                return NO;
            }
            //不能输入多个.
            if (pointNum==1)
            {
                return NO;
            }
            pointNum++;
        }
        else
        {
            //已经有一个.，但此次输入的不是.
            if (pointNum==1)
            {
                NSRange pointRange=[textField.text rangeOfString:@"."];
                //按重量
                if (range.location-pointRange.location>2) {
                    return NO;
                }
            }
        }
        if (range.length>0)
        {
            NSMutableString *contentStr=[[NSMutableString alloc]initWithString:textField.text];
            [contentStr deleteCharactersInRange:range];
            displayString=contentStr;
        }
        else
        {
            NSMutableString *contentStr=[[NSMutableString alloc]initWithString:textField.text];
            [contentStr insertString:string atIndex:range.location];
            displayString=contentStr;
        }
    }
    return YES;
}
- (void)confirmButtonDidTap {
    [self.view endEditing:YES];
    NSString *errMsg = nil;
    if (self.inputView.text.floatValue > self.totalAmount) {
        errMsg = @"可赎回金额不足";
    }
    if (self.inputView.text.floatValue <= 0) {
        errMsg = @"赎回金额必须大于0";
    }
    if (self.inputView.text.floatValue > 50000) {
        errMsg = @"单次赎回不能大于5万元";
    }

    if (errMsg) {
        [WSProgressHUD showErrorWithStatus:errMsg];
        return;
    }
    
    [VerifyTradePWDTool verifyTradePwdOnViewController:self response:^(BOOL result, NSString *errMsg) {
        if (result) {
            NSDictionary *dict = @{
                                       @"amount" : @(self.inputView.text.floatValue)
                                   };
            __weak typeof(self) weakSelf = self;
            [NetworkSingleton mine_sellAssetWithDict:dict Response:^(SellOutModel *result, NSString *errMsg) {
                if (errMsg) {
                    [WSProgressHUD showErrorWithStatus:errMsg];
                } else {
                    [WSProgressHUD showSuccessWithStatus:@"提交成功"];
                    SellOtherSuccessViewController *success = [[SellOtherSuccessViewController alloc] init];
                    success.sellAmountValue = weakSelf.inputView.text;
                    [weakSelf.navigationController pushViewController:success animated:YES];
                }
            }];
        }
    }];
}

- (void)showTips {
    [TipsTool showTipsWithTitle:@"零钱罐赎回" andContent:@"1.零钱罐计息结束日为赎回前一天， 赎回的当天不计息。 \n2.单人单日累计赎回3次上限为10万元。\n3.零钱罐赎回后，该笔资金将被冻结，当其他用户购买该产品的累计金额大于或等于您的赎回金额后，则赎回成功。\n4.法定节假日（除周末），赎回到账时间可能有所延迟。"];
}
#pragma mark - 数据请求
- (void)getOtherAssetsData {
    __weak typeof(self) weakSelf = self;
    [NetworkSingleton mine_getOtherAssetsDataResponse:^(OtherAssetsModel *otherAssets, NSString *errMsg) {
        weakSelf.totalAmount = otherAssets.piggyBankValue.floatValue;
        NSString *string = [NSString stringWithFormat:@"今日可赎回金额%@元", otherAssets.piggyBankValue];
        NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:string];
        [text addAttributes:@{NSForegroundColorAttributeName:COLOR_Red} range:NSMakeRange(7, otherAssets.piggyBankValue.length)];
        weakSelf.userTotalAmount.attributedText = text;
    }];
}
#pragma mark - 初始化，布局
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getOtherAssetsData];
}
- (void)layout {
    [self.userTotalAmount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(17);
        make.left.equalTo(self.view).offset(LEFT_RIGHT_MARGIN);
    }];
    [self.inputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userTotalAmount.mas_bottom).offset(17);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@(NORMAL_CELL_HEIGHT));
    }];
    [self.warnImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.inputView.mas_bottom).offset(15);
        make.left.equalTo(self.userTotalAmount);
    }];
    [self.alertTips mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.warnImageView);
        make.left.equalTo(self.warnImageView.mas_right).offset(10);
    }];
    [self.confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.alertTips.mas_bottom).offset(55);
        make.left.equalTo(self.userTotalAmount);
        make.right.equalTo(self.view).offset(-LEFT_RIGHT_MARGIN);
        make.height.equalTo(@50);
    }];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = COLOR_TBBACK;
    self.title = @"赎回零钱罐";
    
    [self.view addSubview:self.userTotalAmount];
    [self.view addSubview:self.inputView];
    [self.view addSubview:self.warnImageView];
    [self.view addSubview:self.alertTips];
    [self.view addSubview:self.confirmButton];
    [self layout];
}
/**
 懒加载
 
 @return 输入
 */
- (UITextField *)inputView {
    if (!_inputView) {
        _inputView = [[UITextField alloc] init];
        _inputView.backgroundColor = COLOR_WHITE;
        UILabel *name = [[UILabel alloc] init];
        name.textColor = COLOR_TEXT;
        name.text = @"    ";
        name.font = FONT_TEXT;
        [name jk_adjustLabel];
        _inputView.leftViewMode = UITextFieldViewModeAlways;
        _inputView.leftView = name;
        _inputView.placeholder = @"输入赎回金额";
        _inputView.font = FONT_TEXT;
        _inputView.keyboardType = UIKeyboardTypeDecimalPad;
        _inputView.delegate = self;
    }
    return _inputView;
}
/**
 懒加载
 
 @return 今日
 */
- (UILabel *)userTotalAmount {
    if (!_userTotalAmount) {
        _userTotalAmount = [[UILabel alloc] init];
        _userTotalAmount.font = FONT_SUBTEXT;
        _userTotalAmount.textColor = COLOR_SUBTEXT;
        _userTotalAmount.text = @"可赎回金额0.00元";
    }
    return _userTotalAmount;
}
/**
 懒加载
 
 @return 确认卖出
 */
- (UIButton *)confirmButton {
    if (!_confirmButton) {
        _confirmButton = [[UIButton alloc] init];
        [_confirmButton setBackgroundImage:[UIImage jk_imageWithColor:COLOR_MAIN] forState:UIControlStateNormal];
        [_confirmButton setBackgroundImage:[UIImage jk_imageWithColor:COLOR_BUTTON_DISABLE] forState:UIControlStateDisabled];
        [_confirmButton setTitle:@"确定赎回" forState:UIControlStateNormal];
        [_confirmButton setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
        _confirmButton.titleLabel.font = FONT_BUTTON_TEXT;
        _confirmButton.layer.cornerRadius = 10.0f;
        _confirmButton.clipsToBounds = YES;
        [_confirmButton addTarget:self action:@selector(confirmButtonDidTap) forControlEvents:UIControlEventTouchUpInside];
        //        _confirmButton.enabled = NO;
    }
    return _confirmButton;
}
/**
 懒加载
 
 @return 查看
 */
- (UILabel *)alertTips {
    if (!_alertTips) {
        _alertTips = [[UILabel alloc] init];
        _alertTips.font = FONT_SUBTEXT;
        _alertTips.textColor = COLOR_MAIN;
        _alertTips.text = @"查看赎回提示";
        [_alertTips addTapAction:@selector(showTips) target:self];
    }
    return _alertTips;
}
/**
 懒加载
 
 @return warnImage
 */
- (UIImageView *)warnImageView {
    if (!_warnImageView) {
        _warnImageView = [[UIImageView alloc] init];
        _warnImageView.image = [UIImage imageNamed:@"icon_warn"];
        [_warnImageView addTapAction:@selector(showTips) target:self];
    }
    return _warnImageView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
