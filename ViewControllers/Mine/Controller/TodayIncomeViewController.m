//
//  TodayIncomeViewController.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/12.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "TodayIncomeViewController.h"
#import "MJRefresh.h"
#import "IncomCell.h"
#import "NetworkSingleton+UserData.h"
#import "IncomeCellModel.h"
#import "IncomePageModel.h"
@interface TodayIncomeViewController ()<UITableViewDelegate, UITableViewDataSource>
/** liebiao */
@property(nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataSource;

@end

@implementation TodayIncomeViewController

#pragma mark - 数据请求
- (void)getDataFromNet {
    NSDictionary *dict = [self.incomType isEqualToString:@"0"] ? @{@"isYesdayFloatList" : @1} : @{@"isYesdayFloatList" : @0};
    __weak typeof(self) weakSelf = self;
    [NetworkSingleton mine_dayFloatProfitList:dict Response:^(NSArray<IncomeCellModel *> *incomeList, IncomePageModel *incomeData, NSString *errMsg) {
        [weakSelf.tableView.mj_header endRefreshing];
        if (errMsg) {
            [WSProgressHUD showErrorWithStatus:errMsg];
        } else {
            weakSelf.dataSource = (NSMutableArray *)incomeList;
            if (weakSelf.incomeDataDelegate && [weakSelf.incomeDataDelegate respondsToSelector:@selector(incomeDataDidChange:)]) {
                [weakSelf.incomeDataDelegate incomeDataDidChange:incomeData];
            }
        }
        [weakSelf.tableView reloadData];
    }];
}
#pragma mark - 初始化，布局
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = COLOR_TBBACK;
    [self.view addSubview:self.tableView];
    [self.tableView.mj_header beginRefreshing];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    IncomCell  *cell = [IncomCell cellWithTableview:tableView];
    cell.incomeModel = self.dataSource[indexPath.row];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, LEFT_RIGHT_MARGIN, 0, LEFT_RIGHT_MARGIN)];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, LEFT_RIGHT_MARGIN, 0, LEFT_RIGHT_MARGIN)];
    }
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(12, 0, SCREEN_WIDTH - 24, SCREEN_HEIGHT - 210 - (IS_IPHONEX ? 64 : 0)) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.rowHeight = NORMAL_CELL_HEIGHT;
        _tableView.backgroundColor = COLOR_WHITE;
        _tableView.layer.cornerRadius = 10;
        _tableView.clipsToBounds = YES;
        __weak typeof(self) weakSelf = self;
        _tableView.mj_header = [MJDIYHeader headerWithRefreshingBlock:^{
            [weakSelf getDataFromNet];
        }];
    }
    return _tableView;
}

- (NSMutableArray *)dataSource
{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    return _dataSource;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
