//
//  WithdrawalsViewController.m
//  GoldCatBank
//
//  Created by Sunny on 2017/9/6.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "WithdrawalsViewController.h"
#import "UserAuthData.h"
#import "NetworkSingleton+UserData.h"
#import "MineHomePageModel.h"
#import "WithdrawalsListViewController.h"
#import "WithdrawalsSuccessViewController.h"

#import "UserAuthData.h"
#import "PayPassWordBounceView.h"
#import "AllNetWorkRquest.h"
#import "BindBankCarViewController.h"
#import "BankListModel.h"

#import "VerifyTool.h"
#import "BalanceTradeVC.h"

@interface WithdrawalsViewController ()<UITextFieldDelegate>
{
    UserAuthDataModel *userModel;
}

@property(nonatomic,strong)NSString *orderId;

@property(nonatomic,strong)UILabel *tipLab;
@property(nonatomic,strong)UITextField *amountTxf;
@property(nonatomic,strong)BankListModel *model;

@property(nonatomic,strong)UIImageView *bankImageView;

@property(nonatomic,strong)NSString *balanceString;

@end

@implementation WithdrawalsViewController

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *displayString=nil;
    static NSInteger pointNum=0;
    if (textField.text.length<=0)
    {
        pointNum=0;  //此处针对输入过小数，然后又消去小数的处理
        //首位不能输入.
        if ([string isEqualToString:@"."])
        {
            return NO;
        }
        displayString=string;
    }
    else
    {
        if (range.location==0)
        {
            //针对输入很多位，返回第一位输入。的情况
            if ([string isEqualToString:@"."])
            {
                return NO;
            }
        }
        if (![textField.text containsString:@"."]) {
            //此处针对输入过小数，然后又消去小数的处理
            pointNum=0;
        }
        if ([string isEqualToString:@"."]) {
            //针对返回头部输入小数点 （重量只能输入小数点后三位小数，金额小数点后两位）
            NSString *subText=[textField.text substringFromIndex:range.location];
            //按重量
            if ((range.location<textField.text.length)&&(subText.length>2)) {
                
                return NO;
            }
            //不能输入多个.
            if (pointNum==1)
            {
                return NO;
            }
            pointNum++;
        }
        else
        {
            //已经有一个.，但此次输入的不是.
            if (pointNum==1)
            {
                NSRange pointRange=[textField.text rangeOfString:@"."];
                //按重量
                if (range.location-pointRange.location>2) {
                    return NO;
                }
            }
        }
        if (range.length>0)
        {
            NSMutableString *contentStr=[[NSMutableString alloc]initWithString:textField.text];
            [contentStr deleteCharactersInRange:range];
            displayString=contentStr;
        }
        else
        {
            NSMutableString *contentStr=[[NSMutableString alloc]initWithString:textField.text];
            [contentStr insertString:string atIndex:range.location];
            displayString=contentStr;
        }
    }
    return YES;
}


-(void)requstBankInfo{
    
    __weak typeof(self) weakSelf = self;
    
    NSDictionary *dic=@{@"bankName":userModel.bankName};
    [AllNetWorkRquest getBankInfo:dic successBlock:^(id responseBody) {
        
        NSArray *arr=[BankListModel mj_objectArrayWithKeyValuesArray:(NSArray *)responseBody];
       
        BankListModel *model=[arr firstObject];
        
        [weakSelf.bankImageView  sd_setImageWithURL:[NSURL URLWithString:model.logo ]];
        
        
    } failureBlock:^(NSString *error) {
        
      
        
    }];
}

-(void)requestData
{
    // __weak typeof(self) weakSelf = self;
    __weak typeof (self)  weakSelf =self;
    //请求余额
    [NetworkSingleton userData_getMinePageDataResponse:^(MineHomePageModel *userAssetsData, NSString *errMsg) {
        
        if (!errMsg)
        {
            NSString *maxBanlance = (userAssetsData.accountBalance.floatValue > 50000) ? @"50000" : userAssetsData.accountBalance;
            weakSelf.balanceString=[NSString stringWithFormat:@"%@",userAssetsData.accountBalance];
            weakSelf.tipLab.attributedText=[weakSelf pricFormater:maxBanlance];
        }
        
    }];
}

-(NSMutableAttributedString *)pricFormater:(NSString *)maxBanlance{
    
    
    NSString *string=[NSString stringWithFormat:@"本次最多可提现%@元", maxBanlance];
    NSString *amountStr=[NSString stringWithFormat:@"%@",maxBanlance];
    NSMutableAttributedString *attri=[[NSMutableAttributedString alloc]initWithString:string];
    [attri addAttribute:NSForegroundColorAttributeName value:COLOR_Red range:NSMakeRange(7, amountStr.length)];
    
    return attri;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"提现";
    self.balanceString=@"50000.00";
    self.view.backgroundColor=COLOR_TBBACK;
    
    userModel=[UserAuthData shareInstance].userAuthData;
    UIBarButtonItem *rightBtn=[[UIBarButtonItem alloc]initWithTitle:@"提现记录" style:UIBarButtonItemStylePlain target:self action:@selector(rightBtnClick:)];
    [rightBtn setTitleTextAttributes:@{NSForegroundColorAttributeName:COLOR_GLODBACKGROUD} forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem=rightBtn;
    
    
    [self initContentView];
    [self requestData];
    [self requstBankInfo];
    
}


-(void)initContentView{
   
    CMBorderView *topBackView=[[CMBorderView alloc]initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH, 80)];
    topBackView.backgroundColor=COLOR_WHITE;
    [self.view addSubview:topBackView];
    
    UIImageView *bankImageView=[[UIImageView alloc]initWithFrame:CGRectMake(20,15, 50, 50)];
    [topBackView addSubview:bankImageView];
    
    self.bankImageView=bankImageView;
    
    UILabel *titleLab=[[UILabel alloc]initWithFrame:CGRectMake(bankImageView.ct_right+10,bankImageView.ct_top,SCREEN_WIDTH-bankImageView.ct_right-20, 20)];
    titleLab.text=userModel.bankName;
    titleLab.textColor=COLOR_TEXT;
    titleLab.font=FONT(15);
    [topBackView addSubview:titleLab];
    
    UILabel *bankNumLab=[[UILabel alloc]initWithFrame:CGRectMake(titleLab.ct_left, titleLab.ct_bottom, titleLab.ct_width, titleLab.ct_height)];
    bankNumLab.textColor=COLOR_SUBTEXT;
    bankNumLab.font=FONT(14);
    NSString *num=[userModel.bankCard substringWithRange:NSMakeRange(userModel.bankCard.length-4, 4)];
    bankNumLab.text=[NSString stringWithFormat:@"尾号%@",num];
    [topBackView addSubview:bankNumLab];
    
    

    [self.view addSubview:self.tipLab];
    
    CMBorderView *amountView=[[CMBorderView alloc]initWithFrame:CGRectMake(0, self.tipLab.ct_bottom+5, SCREEN_WIDTH, 50)];
    amountView.backgroundColor=COLOR_WHITE;
    [self.view addSubview:amountView];
    

    
    [amountView addSubview:self.amountTxf];
    
    //提交按钮
    UIButton *commitBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH-40, 50)];
    commitBtn.center=CGPointMake(SCREEN_WIDTH/2.0, amountView.ct_bottom+42);
    commitBtn.backgroundColor=COLOR_GLODBACKGROUD;
    [commitBtn setTitle:@"提交" forState:UIControlStateNormal];
    [commitBtn setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
    commitBtn.layer.masksToBounds=YES;
    commitBtn.layer.cornerRadius=8;
    [self.view addSubview:commitBtn];
    [commitBtn addTarget:self action:@selector(commitBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    commitBtn.titleLabel.font=FONT(18);
    
    UIButton *withdrawalsTipBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    withdrawalsTipBtn.frame=CGRectMake(commitBtn.ct_left-10, commitBtn.ct_bottom+10, 90, 30);
    [withdrawalsTipBtn setTitle:@"提现说明" forState:UIControlStateNormal];
    [withdrawalsTipBtn setTitleColor:COLOR_GLODTEXT forState:UIControlStateNormal];
    [withdrawalsTipBtn setImage:[UIImage imageNamed:@"withdrawalsTipIcon"] forState:UIControlStateNormal];
    withdrawalsTipBtn.titleLabel.font=FONT(13);
    withdrawalsTipBtn.titleEdgeInsets=UIEdgeInsetsMake(0, 10, 0, 0);
    withdrawalsTipBtn.userInteractionEnabled=NO;
    [self.view addSubview:withdrawalsTipBtn];
    
    
    
    NSString *tipStr=@"提现手续费：每笔提现5元\n提现限额：单笔提现不能小于10元，不大于5万元\n提现到账时间：1～3个工作日到账，节假日顺延。";
    UILabel *tipContentlab=[[UILabel alloc]initWithFrame:CGRectMake(withdrawalsTipBtn.ct_left+30, withdrawalsTipBtn.ct_bottom,SCREEN_WIDTH-withdrawalsTipBtn.ct_left-20-15, 60)];
    tipContentlab.textColor=COLOR_SUBTEXT;
    tipContentlab.font=FONT(12);
    tipContentlab.numberOfLines=0;
    tipContentlab.text=tipStr;
    [self.view addSubview:tipContentlab];
    
    
}


-(UILabel *)tipLab
{
    if (!_tipLab)
    {
        _tipLab=[[UILabel alloc]initWithFrame:CGRectMake(20,10+80+5, SCREEN_WIDTH-50, 20)];
        _tipLab.textColor=COLOR_SUBTEXT;
        _tipLab.font=FONT(12);
        
    }
    
    return _tipLab;
}

-(UITextField *)amountTxf
{
    if (!_amountTxf)
    {
        _amountTxf=[[UITextField alloc]initWithFrame:CGRectMake(20, 10, SCREEN_WIDTH-40, 30)];
        _amountTxf.font=FONT(15);
        _amountTxf.placeholder=@"请输入提现金额";
        _amountTxf.textColor=COLOR_TEXT;
        _amountTxf.keyboardType=UIKeyboardTypeDecimalPad;
        _amountTxf.delegate = self;
        UILabel *rightLab=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 20, 30)];
        rightLab.font=FONT(15);
        rightLab.textColor=COLOR_TEXT;
        rightLab.text=@"元";
        
        _amountTxf.rightViewMode=UITextFieldViewModeAlways;
        _amountTxf.rightView=rightLab;

    }
    return _amountTxf;
}
#pragma mark---提交按钮
-(void)commitBtnClick:(UIButton *)btn
{
    [self.view endEditing:YES];
    
    if (self.amountTxf.text.floatValue<10)
    {
        [WSProgressHUD showErrorWithStatus:@"提现金额不能小于10元"];
        return;
    }
    
    if (self.amountTxf.text.floatValue>50000)
    {
        [WSProgressHUD showErrorWithStatus:@"每日提现不能多于5万元！"];
        return;
    }
    
    if (self.amountTxf.text.floatValue>self.balanceString.floatValue)
    {
        [WSProgressHUD showErrorWithStatus:@"提现金额不能大于余额！"];
        return;

    }


    __weak typeof(self) weakSelf = self;
    
    [[UserAuthData shareInstance] refreshUserInfoResponse:^(UserAuthDataModel *userAuthData, NSString *errMsg) {
        
        
        if ([userAuthData.isAuthentication boolValue]>0)
        {
            
            [weakSelf withdrawHandl:0];
            
        }
        else
        {
            
            BindBankCarViewController *vc=[[BindBankCarViewController alloc]init];
            [weakSelf.navigationController pushViewController:vc animated:YES];
            
        }
        
    }];
}


#pragma mark---

-(void)withdrawHandl:(CGFloat)keyBord_y{
    
       //UIAlertController风格：UIAlertControllerStyleAlert
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提现说明"
                                                                             message:@"每笔提现都会收取5元手续费，请您确认是否提现？"
                                                                      preferredStyle:UIAlertControllerStyleAlert ];
    
    //添加取消到UIAlertController
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
    }];
    
    
    [alertController addAction:cancelAction];
    
    __weak typeof(self) weakSelf = self;
    UIAlertAction *OKAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [VerifyTool verifyTradeNumWithType:VerifyTypeWithdraw amount:weakSelf.amountTxf.text response:^(BOOL result, NSString *errMsg) {
            if (result) {
                [WSProgressHUD showSuccessWithStatus:@"提现申请已提交"];
                WithdrawalsSuccessViewController *successVC=[[WithdrawalsSuccessViewController alloc]init];
                successVC.amount=weakSelf.amountTxf.text;
                [weakSelf.navigationController pushViewController:successVC animated:YES];
            } else {
                
            }
        }];
    }];
    
    [alertController addAction:OKAction];
    
    [self presentViewController:alertController animated:YES completion:nil];

}


#pragma mark---右 按钮

-(void)rightBtnClick:(UIBarButtonItem *)btn{
    
    BalanceTradeVC *vc=[[BalanceTradeVC alloc]initWithOrderType:TypeWithdraw];
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
