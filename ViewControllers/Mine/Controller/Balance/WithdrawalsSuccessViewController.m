//
//  WithdrawalsSuccessViewController.m
//  GoldCatBank
//
//  Created by Sunny on 2017/9/6.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "WithdrawalsSuccessViewController.h"
#import <FDFullscreenPopGesture/UINavigationController+FDFullscreenPopGesture.h>
#import "BalanceTradeVC.h"
@interface WithdrawalsSuccessViewController ()

@end

@implementation WithdrawalsSuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.fd_prefersNavigationBarHidden = YES;
    self.fd_interactivePopDisabled = YES;
    self.view.backgroundColor=COLOR_TBBACK;
    [self initContentView];
    
    
    UIButton *finishBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    finishBtn.frame=CGRectMake(SCREEN_WIDTH-60, 30 + NavBarH - 64, 50, 30);
    [finishBtn setTitle:@"完成" forState:UIControlStateNormal];
    finishBtn.titleLabel.font=FONT(15);
    [finishBtn addTarget:self action:@selector(finishedBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:finishBtn];
    

}


-(void)initContentView{
    
    UIView *topView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 214)];
    topView.backgroundColor=COLOR_GLODBACKGROUD;
    [self.view addSubview:topView];
    
    UILabel *titLab=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 20)];
    titLab.center=CGPointMake(SCREEN_WIDTH/2.0, 30+15 + NavBarH - 64);
    titLab.text=@"提现";
    titLab.font=FONT_NAV;
    titLab.textAlignment=NSTextAlignmentCenter;
    titLab.textColor=COLOR_WHITE;
    [topView addSubview:titLab];
    
    UILabel *amountLab=[[UILabel alloc]initWithFrame:CGRectMake(0, titLab.ct_bottom+30, SCREEN_WIDTH, 30)];
    
    amountLab.text=[NSString stringWithFormat:@"%.2f元",self.amount.floatValue];
    amountLab.textAlignment=NSTextAlignmentCenter;
    [topView addSubview:amountLab];
    
    
    UILabel *balanceLab=[[UILabel alloc]initWithFrame:CGRectMake(0, amountLab.ct_bottom+20, SCREEN_WIDTH, 20)];
    balanceLab.textAlignment=NSTextAlignmentCenter;
    balanceLab.font=FONT(13);
    balanceLab.text=@"余额提现";
    balanceLab.textColor=UIColorFromHex(0xaf862d);
    [topView addSubview:balanceLab];
    
    
    
    //提现详情
    
    UIView *detailView=[[UIView alloc]initWithFrame:CGRectMake(10, topView.ct_bottom+10, SCREEN_WIDTH-20, 150)];
    detailView.backgroundColor=COLOR_WHITE;
    detailView.layer.masksToBounds=YES;
    detailView.layer.cornerRadius=8;
    [self.view addSubview:detailView];
    
    
    UILabel *detailTitleLab=[[UILabel alloc]initWithFrame:CGRectMake(15,10,detailView.ct_width-30, 20)];
    detailTitleLab.textColor=COLOR_TEXT;
    detailTitleLab.font=FONT(13);
    detailTitleLab.textAlignment=NSTextAlignmentCenter;
    detailTitleLab.attributedText=[self amountFormater:[NSString stringWithFormat:@"提现%@元",self.amount] subStr:self.amount];
    [detailView addSubview:detailTitleLab];

    NSString *amountStr=[NSString stringWithFormat:@"%.2f元",self.amount.floatValue];
    NSString *cashStr=[NSString stringWithFormat:@"%.2f元",self.amount.floatValue-5];
    

    NSString *withdrawlsTime=[DateTool computeWithDays:3];
    NSArray *titleArray=@[@{@"提现金额":amountStr},@{@"手续费":@"5.00元"},@{@"到账金额":cashStr},@{@"预计到账时间":withdrawlsTime}];
    
    for (int i=0; i<titleArray.count; i++)
    {
        NSDictionary *dic=titleArray[i];
        
        UILabel *titleLab=[[UILabel alloc]initWithFrame:CGRectMake(20,detailTitleLab.ct_bottom+5+(5+20)*i, 100, 20)];
        titleLab.textColor=COLOR_SUBTEXT;
        titleLab.font=FONT(13);
        titleLab.text=[[dic allKeys] lastObject];
        [detailView addSubview:titleLab];
        
        UILabel *contentLab=[[UILabel alloc]initWithFrame:CGRectMake(titleLab.ct_right+10, titleLab.ct_top, detailView.ct_width-titleLab.ct_right-20, 20)];
        contentLab.textColor=COLOR_SUBTEXT;
        contentLab.font=FONT(13);
        contentLab.text=[[dic allValues]lastObject];
        contentLab.textAlignment=NSTextAlignmentRight;
        [detailView addSubview:contentLab];
    }
    
 
    
}

-(NSMutableAttributedString *)amountFormater:(NSString *)totalStr subStr:(NSString *)subStr
{
    NSMutableAttributedString *string=[[NSMutableAttributedString alloc]initWithString:totalStr];
    [string addAttribute:NSForegroundColorAttributeName value:COLOR_Red range:NSMakeRange(totalStr.length-1-subStr.length,subStr.length)];
    
    
    return string;
}


#pragma mark--
-(void)finishedBtnClick:(UIButton *)btn
{
    BalanceTradeVC *vc=[[BalanceTradeVC alloc]initWithOrderType:TypeWithdraw];
    vc.isSuccessFinish=YES;
    [self.navigationController pushViewController:vc animated:YES];
    
    
//    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
