//
//  OutOfDateCouponViewController.h
//  GoldCatBank
//
//  Created by Sunny on 2017/10/24.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "BaseViewController.h"

@interface OutOfDateCouponViewController : BaseViewController

@property(nonatomic,assign)NSInteger couponType;

@end
