//
//  TradeDetailViewController.m
//  GoldCatBank
//
//  Created by Sunny on 2017/9/15.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "TradeDetailViewController.h"
#import "MineNetWork.h"
#import "OrderDetailModel.h"
@interface TradeDetailViewController ()

@end

@implementation TradeDetailViewController


-(void)requestData{
    
    NSDictionary *dic=@{@"tranid":self.orderID};
    [MineNetWork GetOrderDetailWithDataDic:dic successBlock:^(id responseBody) {
     
        OrderDetailModel *model=[OrderDetailModel mj_objectWithKeyValues:responseBody];

//        if (model.logType.integerValue==1||model.logType.integerValue==2)
//        {
            [self initContentWithModel:model];

//        }
       
        
    } failureBlock:^(NSString *error) {
        
        
    }];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"订单详情";
    self.view.backgroundColor=COLOR_TBBACK;
    [self requestData];
    
}


    //买金／卖金。判那句哪个字段、字段对应需确认
-(void)initContentWithModel:(OrderDetailModel *)model{
    
    UILabel *topicLab=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 30)];
    topicLab.textAlignment=NSTextAlignmentCenter;
    topicLab.textColor=COLOR_SUBTEXT;
    topicLab.font=FONT(12);
    topicLab.text=[NSString stringWithFormat:@"交易时间  %@",model.payTime];
    [self.view addSubview:topicLab];
    
    UIView *bottomView=[[UIView alloc]initWithFrame:CGRectMake(10, topicLab.ct_bottom, SCREEN_WIDTH-20, 300)];
    bottomView.backgroundColor=COLOR_WHITE;
    bottomView.layer.masksToBounds=YES;
    bottomView.layer.cornerRadius=8;
    [self.view addSubview:bottomView];
    
    
    UILabel *bottomLab=[[UILabel alloc]initWithFrame:CGRectMake(bottomView.ct_left+10, bottomView.ct_bottom, SCREEN_WIDTH-100, 30)];
    bottomLab.textColor=COLOR_SUBTEXT;
    bottomLab.font=FONT(13);
    bottomLab.text=[NSString stringWithFormat:@"收益发放日：  %@",model.profitSendTime];
    [self.view addSubview:bottomLab];
    
    
    NSArray *payArr=@[@"投资",@"买金",@"卖金",@"赎回",@"充值",@"提现",@"到期",@"其他"];

    NSString *type=@"";
    if (model.logType.integerValue<payArr.count)
    {
        type=[payArr objectAtIndex:model.logType.integerValue];
        
    }

    NSArray *titleArr=@[@{type:[NSString stringWithFormat:@"克重： %@克",model.orderGold]},@{model.productName:[NSString stringWithFormat:@"交易金价： %@元/克",model.tranRTGP]},@{@"订单总额":[NSString stringWithFormat:@"%@元",model.orderAmount]},@{@"优惠减免":[NSString stringWithFormat:@"%@元",model.couponAmount]},@{@"实际支付":[NSString stringWithFormat:@"%@元",model.payAmount]},@{@"余额支付":[NSString stringWithFormat:@"%@元",model.useAccountAmount]},@{@"在线支付":[NSString stringWithFormat:@"%@元",model.bankAmount]}];
    
    for (int i=0; i<titleArr.count; i++)
    {
        NSDictionary *dic=titleArr[i];
        UILabel *titleLab=[[UILabel alloc]initWithFrame:CGRectMake(10,10+35*i, 100, 35)];
        titleLab.text=[[dic allKeys] lastObject];
        titleLab.font=FONT(15);
        titleLab.textColor=COLOR_TEXT;
        [bottomView addSubview:titleLab];
        
        if (i==2)
        {
            UIView *line=[[UIView alloc]initWithFrame:CGRectMake(0, titleLab.ct_bottom+5, bottomView.ct_width, 0.5)];
            line.backgroundColor=COLOR_LINE;
            [bottomView addSubview:line];
        }
        

        if (i>=3)
        {
            titleLab.frame=CGRectMake(10,10+10+35*i, 100, 35);
            
            titleLab.textColor=COLOR_SUBTEXT;
        }
        
        UILabel *contentLab=[[UILabel alloc]initWithFrame:CGRectMake(titleLab.ct_right+10,titleLab.ct_top,bottomView.ct_width-titleLab.ct_right-20, titleLab.ct_height)];
        contentLab.textAlignment=NSTextAlignmentRight;
        contentLab.font=FONT(14);
        contentLab.textColor=COLOR_SUBTEXT;
        NSString *contentStr=[[dic allValues]lastObject];
        if (i==0)
        {
            
            NSString *weightStr=[NSString stringWithFormat:@"%@克",model.orderGold];
            
            NSMutableAttributedString *string=[[NSMutableAttributedString alloc]initWithString:contentStr];
            [string addAttribute:NSForegroundColorAttributeName value:COLOR_Red range:NSMakeRange(contentStr.length-weightStr.length, weightStr.length)];
            contentLab.attributedText=string;
            
        }
        else if (i==1)
        {

            NSString *tradeStr=[NSString stringWithFormat:@"%@元/克",model.orderAmount];

            NSMutableAttributedString *string=[[NSMutableAttributedString alloc]initWithString:contentStr];
            [string addAttribute:NSForegroundColorAttributeName value:COLOR_Red range:NSMakeRange(contentStr.length-tradeStr.length, tradeStr.length)];
            contentLab.attributedText=string;
        }
        else if (i==2)
        {
            contentLab.textColor=COLOR_Red;
            contentLab.text=contentStr;
        }
        else
        {

            contentLab.text=contentStr;
        }
        
        [bottomView addSubview:contentLab];

        
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
