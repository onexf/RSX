//
//  RechargeViewController.m
//  GoldCatBank
//
//  Created by Sunny on 2017/9/7.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "RechargeViewController.h"
#import "UserAuthData.h"
#import "AllNetWorkRquest.h"

#import "BindBankCarViewController.h"
#import "BankListModel.h"
#import "BalanceTradeVC.h"
#import "WithdrawalsPayModel.h"
#import "VerifyTool.h"
@interface RechargeViewController ()<UITextFieldDelegate>
{
    UITextField *amountTxf;
    UserAuthDataModel *userModel;
    
}
@property(nonatomic,strong)UIImageView *bankImageView;

@property(nonatomic,strong)UILabel  *bankTiplab;

@property(nonatomic,copy)NSString * onceLimtAmount;
@end

@implementation RechargeViewController

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *displayString=nil;
    static NSInteger pointNum=0;
    if (textField.text.length<=0)
    {
        pointNum=0;  //此处针对输入过小数，然后又消去小数的处理
        //首位不能输入.
        if ([string isEqualToString:@"."])
        {
            return NO;
        }
        displayString=string;
    }
    else
    {
        if (range.location==0)
        {
            //针对输入很多位，返回第一位输入。的情况
            if ([string isEqualToString:@"."])
            {
                return NO;
            }
        }
        if (![textField.text containsString:@"."]) {
            //此处针对输入过小数，然后又消去小数的处理
            pointNum=0;
        }
        if ([string isEqualToString:@"."]) {
            //针对返回头部输入小数点 （重量只能输入小数点后三位小数，金额小数点后两位）
            NSString *subText=[textField.text substringFromIndex:range.location];
            //按重量
            if ((range.location<textField.text.length)&&(subText.length>2)) {
                
                return NO;
            }
            //不能输入多个.
            if (pointNum==1)
            {
                return NO;
            }
            pointNum++;
        }
        else
        {
            //已经有一个.，但此次输入的不是.
            if (pointNum==1)
            {
                NSRange pointRange=[textField.text rangeOfString:@"."];
                //按重量
                if (range.location-pointRange.location>2) {
                    return NO;
                }
            }
        }
        if (range.length>0)
        {
            NSMutableString *contentStr=[[NSMutableString alloc]initWithString:textField.text];
            [contentStr deleteCharactersInRange:range];
            displayString=contentStr;
        }
        else
        {
            NSMutableString *contentStr=[[NSMutableString alloc]initWithString:textField.text];
            [contentStr insertString:string atIndex:range.location];
            displayString=contentStr;
        }
    }
    return YES;
}
-(void)requstBankInfo{
    
    __weak typeof(self) weakSelf = self;
    
    NSDictionary *dic=@{@"bankName":userModel.bankName};
    [AllNetWorkRquest getBankInfo:dic successBlock:^(id responseBody) {
        
        NSArray *arr=[BankListModel mj_objectArrayWithKeyValuesArray:(NSArray *)responseBody];
        
        BankListModel *model=[arr firstObject];
        
        weakSelf.bankTiplab.text=[NSString stringWithFormat:@"单笔限额%@ 单日限额%@",model.onceLimit,model.dailyLimt];
        
        if (model.onceLimit.length>0)
        {
            NSString *unitStr=[model.onceLimit substringFromIndex:model.onceLimit.length-1];
            if ([unitStr isEqualToString:@"万"])
            {
                weakSelf.onceLimtAmount=[model.onceLimit stringByReplacingOccurrencesOfString:@"万" withString:@"0000"];
                
            }
            
        }
        
        [weakSelf.bankImageView  sd_setImageWithURL:[NSURL URLWithString:model.logo]];
        
    } failureBlock:^(NSString *error) {
        
       
        
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    userModel=[UserAuthData shareInstance].userAuthData;
    
    self.title=@"充值";
    self.view.backgroundColor=COLOR_TBBACK;
    
    [self setContentView];
    
    self.onceLimtAmount=@"";
    
    [self requstBankInfo];
    
//    [self.view addSubview:self.payCodeView];
   // [self.view addSubview:self.payChargePaswView];

    
}


//-(payCodeInputView *)payCodeView
//{
//    if (!_payCodeView)
//    {
//        _payCodeView=[[payCodeInputView alloc]initWithFrame:CGRectMake(0,SCREEN_HEIGHT-NavBarHeight, SCREEN_WIDTH,SCREEN_HEIGHT)];
//        _payCodeView.delegate=self;
//
//    }
//    return _payCodeView;
//}
-(void)setContentView
{
  
    CMBorderView *topciew=[[CMBorderView alloc]initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH, 80)];
    topciew.backgroundColor=COLOR_WHITE;
    [self.view addSubview:topciew];
    
    UIImageView *bankImageView=[[UIImageView alloc]initWithFrame:CGRectMake(20,15, 50, 50)];
    [topciew addSubview:bankImageView];
    
    self.bankImageView=bankImageView;
    
    UILabel *bankTitleLab=[[UILabel alloc]initWithFrame:CGRectMake(bankImageView.ct_right+10,bankImageView.ct_top,SCREEN_WIDTH-bankImageView.ct_right-20, 20)];
    bankTitleLab.text=userModel.bankName;
    bankTitleLab.textColor=COLOR_TEXT;
    bankTitleLab.font=FONT(15);
    [topciew addSubview:bankTitleLab];
    
    UILabel *bankNumLab=[[UILabel alloc]initWithFrame:CGRectMake(bankTitleLab.ct_left, bankTitleLab.ct_bottom, bankTitleLab.ct_width, bankTitleLab.ct_height)];
    bankNumLab.textColor=COLOR_SUBTEXT;
    bankNumLab.font=FONT(14);
//    NSString *num=[userModel.bankCard substringWithRange:NSMakeRange(userModel.bankCard.length-4, 4)];
//    bankNumLab.text=[NSString stringWithFormat:@"尾号%@",num];
    [topciew addSubview:bankNumLab];
    
    self.bankTiplab=bankNumLab;

    CMBorderView *contentView=[[CMBorderView alloc]initWithFrame:CGRectMake(0, topciew.ct_bottom+10, SCREEN_WIDTH, 50)];
    contentView.backgroundColor=COLOR_WHITE;
    contentView.borderWidth=CMViewBorderWidthMake(0.5, 0, 0.5, 0);
    [self.view addSubview:contentView];

    
    
    amountTxf=[[UITextField alloc]initWithFrame:CGRectMake(10, 10,SCREEN_WIDTH-25, 30)];
    amountTxf.delegate = self;
    amountTxf.placeholder=@"请输入充值金额";
    amountTxf.jk_maxLength = 6;
    amountTxf.font=FONT(15);
    UILabel *rightLab=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 20, 30)];
    rightLab.font=FONT(15);
    rightLab.textColor=COLOR_TEXT;
    rightLab.text=@"元";
    amountTxf.keyboardType=UIKeyboardTypeDecimalPad;
    amountTxf.rightViewMode=UITextFieldViewModeAlways;
    amountTxf.rightView=rightLab;

    amountTxf.textColor=COLOR_TEXT;
    [contentView addSubview:amountTxf];

    
    UIButton *rechargebtn=[UIButton buttonWithType:UIButtonTypeCustom];
    rechargebtn.frame=CGRectMake(0, 0, SCREEN_WIDTH-100, 50);
    rechargebtn.layer.masksToBounds=YES;
    rechargebtn.layer.cornerRadius=8;
    rechargebtn.backgroundColor=COLOR_Red;
    rechargebtn.center=CGPointMake(SCREEN_WIDTH/2.0, contentView.ct_bottom+20+50/2.0);
    [rechargebtn setTitle:@"立即充值" forState:UIControlStateNormal];
    [rechargebtn addTarget:self action:@selector(rechargeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    rechargebtn.titleLabel.font=FONT(16);
    [self.view addSubview:rechargebtn];
    
    
    
    UILabel *tipTitleLab=[[UILabel alloc]initWithFrame:CGRectMake(10, rechargebtn.ct_bottom+20, SCREEN_WIDTH/2.0, 20)];
    tipTitleLab.text=@"* 充值提示";
    tipTitleLab.textColor=COLOR_SUBTEXT;
    tipTitleLab.font=FONT(14);
    [self.view addSubview:tipTitleLab];
    
    
    NSString *tipStr=@"1、单笔充值金额不得小于2元。\n2、单笔充值最大金额、单日充值最大金额受您的银行卡额度影响。";
    UILabel *tipContentlab=[[UILabel alloc]initWithFrame:CGRectMake(tipTitleLab.ct_left, tipTitleLab.ct_bottom,SCREEN_WIDTH-20, 60)];
    tipContentlab.textColor=COLOR_SUBTEXT;
    tipContentlab.font=FONT(12);
    tipContentlab.numberOfLines=0;
    tipContentlab.text=tipStr;
    [self.view addSubview:tipContentlab];

    
}

#pragma mark--
-(void)rechargeBtnClick:(UIButton *)btn
{
    [self.view endEditing:YES];
    
    if (amountTxf.text.floatValue<2)
    {
        [WSProgressHUD showErrorWithStatus:@"充值金额不能小于2元"];
        return;
    }
    
    if (self.onceLimtAmount.length>0)
    {
        if (amountTxf.text.floatValue>self.onceLimtAmount.floatValue)
        {
            [WSProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"充值金额单笔不能超过%@元",self.onceLimtAmount]];
            return;
        }
    }
    
    __weak typeof(self) weakSelf = self;
    //验证交易密码
    [VerifyTool verifyTradeNumWithType:VerifyTypeUnionPayCharge amount:amountTxf.text response:^(BOOL result, NSString *errMsg) {
        if (result) {
            BalanceTradeVC *vc = [[BalanceTradeVC alloc] initWithOrderType:TypeCharge];
            [weakSelf.navigationController pushViewController:vc animated:YES];
            [WSProgressHUD showSuccessWithStatus:@"充值成功"];
        } else {//充值不存在
            
        }
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
