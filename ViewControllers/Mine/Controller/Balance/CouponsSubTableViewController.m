//
//  CouponsSubTableViewController.m
//  GoldCatBank
//
//  Created by Sunny on 2017/10/19.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "CouponsSubTableViewController.h"
#import "DiscountCouponCell.h"
#import "MineNetWork.h"
#import "MJRefresh.h"
#import "CouponsViewController.h"
#import "OutOfDateCouponViewController.h"
#import "AllViewController.h"
#import "CBWebViewController.h"
#import "BlankPageView.h"
#import "BlankPageView.h"
#define CellHeight 90
@interface CouponsSubTableViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *dataArray;
    NSInteger pageNum;
    NSInteger pageSize;
    NSInteger totalPageNum;
}
@property(nonatomic,strong)UITableView *table;

@end

@implementation CouponsSubTableViewController

#pragma mark--请求数据
-(void)requestData
{
    
    if (pageNum>1)
    {
        if (pageNum>totalPageNum)
        {
            [self.table.mj_footer endRefreshingWithNoMoreData];
            return;
        }
    }
    
    __weak typeof(self) weakSelf = self;
    
    NSDictionary *dic=nil;
    if (self.couponType==RedPacket)
    {
        //红包
        dic=@{@"isCanUse":[NSNumber numberWithInteger:1],@"couponTypes":@"0,1",@"isExpire":[NSNumber numberWithInteger:0],@"pageSize":[NSNumber numberWithInteger:pageSize],@"pageIndex":[NSNumber numberWithInteger:pageNum]};
    }
    else
    {
        //优惠券
        dic=@{@"isCanUse":[NSNumber numberWithInteger:1],@"couponTypes":@"2",@"isExpire":[NSNumber numberWithInteger:0],@"pageSize":[NSNumber numberWithInteger:pageSize],@"pageIndex":[NSNumber numberWithInteger:pageNum]};
        
    }
    
    [MineNetWork getMyCouponListWithDataDic:dic
                               successBlock:^(id responseBody) {
          
                                   if (![responseBody isKindOfClass:[NSDictionary class]]) {
                                       
                                       return;
                                   }
                             
                                   if (self.blankView)
                                   {
                                       self.blankView.hidden=YES;
                                   }
                                   
                                   
                                   NSArray *arr=[DiscountCouponModel mj_objectArrayWithKeyValuesArray:responseBody[@"items"]];
                                   
                                   totalPageNum=[responseBody[@"totalPages"] integerValue];
                                   
                                   if (pageNum<=1)
                                   {
                                       
                                       [dataArray removeAllObjects];
                                       
                                       if (arr.count>0)
                                       {
                                           [dataArray addObjectsFromArray:arr];
                                       }
                                       
                                      [self setTableFootView];
                                       if (totalPageNum<=1)
                                       {
                                           [weakSelf.table reloadData];
                                           [weakSelf.table.mj_header endRefreshing];
                                           [weakSelf.table.mj_footer endRefreshingWithNoMoreData];
                                           return;
                                           
                                       }
                                       
                                   }
                                   else
                                   {
                                       
                                       [dataArray addObjectsFromArray:arr];
                                       
                                       if (arr.count<pageSize)
                                       {
                                           [weakSelf.table reloadData];
                                           [weakSelf.table.mj_header endRefreshing];
                                           [weakSelf.table.mj_footer endRefreshingWithNoMoreData];
                                           
                                           return;
                                           
                                       }
                                       
                                   }
                                   
                                   [weakSelf.table reloadData];
 
                                   [weakSelf.table.mj_header endRefreshing];
                                   [weakSelf.table.mj_footer endRefreshing];
                                   
                                   
                               }
                               failureBlock:^(NSString *error) {

                                   [weakSelf setNoDataViewWithError:error];
                                   [weakSelf.table.mj_header endRefreshing];
                                   [weakSelf.table.mj_footer endRefreshing];
                                   
                                   
                               }];
    
 

    
}

#pragma mark--设置无数据时的查看过期。。
-(void)setNoDataViewWithError:(NSString *)error
{
    if ([error isEqualToString:@"查询成功无数据"])
    {
        if (self.couponType==RedPacket)
        {
            if (self.blankView==nil)
            {
                [self showBlankPageWithType:BlankPageRedPacketNOData withFrame:self.table.bounds switchView:self.table];
                UIButton *titleBtn=[UIButton buttonWithType:UIButtonTypeCustom];
                titleBtn.frame=CGRectMake(0, 0, 100, 30);
                titleBtn.center=CGPointMake(SCREEN_WIDTH/2.0, self.blankView.ct_bottom-80);
                [titleBtn setAttributedTitle:[self formatTitleWithTitle:@"查看过期红包"] forState:UIControlStateNormal];
                
                [titleBtn addTarget:self action:@selector(checkOutOfDateList:) forControlEvents:UIControlEventTouchUpInside];
                [self.blankView addSubview:titleBtn];
            }
            else
            {
                self.blankView.hidden=NO;
            }
            
            
            
        }
        else
        {
            if (self.blankView==nil)
            {
                
                [self showBlankPageWithType:BlankPageCouponNOData withFrame:self.table.bounds switchView:self.table];
                UIButton *titleBtn=[UIButton buttonWithType:UIButtonTypeCustom];
                titleBtn.frame=CGRectMake(0, 0, 100, 30);
                titleBtn.center=CGPointMake(SCREEN_WIDTH/2.0, self.blankView.ct_bottom-80);
                [titleBtn setAttributedTitle:[self formatTitleWithTitle:@"查看过期优惠券" ] forState:UIControlStateNormal];
                [titleBtn addTarget:self action:@selector(checkOutOfDateList:) forControlEvents:UIControlEventTouchUpInside];
                [self.blankView addSubview:titleBtn];
            }
            else
            {
                self.blankView.hidden=NO;
            }
            
        }
        
    }
}

-(void)setTableFootView
{
    CGFloat tableHeight=self.table.ct_height;
    
    CGFloat cellSHeight=CellHeight*dataArray.count;
    
    CGFloat intervalHeight=tableHeight-cellSHeight;
    
    UIView *footView=[[UIView alloc]init];
    footView.backgroundColor=COLOR_TBBACK;
    
    UIButton *titleBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    titleBtn.frame=CGRectMake(0, 0, 100, 30);

    if (intervalHeight>=80)
    {
        footView.frame=CGRectMake(0, 0, SCREEN_WIDTH, intervalHeight);
        titleBtn.center=CGPointMake(SCREEN_WIDTH/2.0, intervalHeight-30/2.0-20);
    }
    else
    {
        footView.frame=CGRectMake(0, 0, SCREEN_WIDTH, 80);
        titleBtn.center=CGPointMake(SCREEN_WIDTH/2.0, 30+30/2.0);
    }

    
    if (self.couponType==RedPacket)
    {
        [titleBtn setAttributedTitle:[self formatTitleWithTitle:@"查看过期红包"] forState:UIControlStateNormal];
    }
    else
    {
        [titleBtn setAttributedTitle:[self formatTitleWithTitle:@"查看过期优惠券" ] forState:UIControlStateNormal];
    }
    [titleBtn addTarget:self action:@selector(checkOutOfDateList:) forControlEvents:UIControlEventTouchUpInside];
    
    [footView addSubview:titleBtn];
    
    self.table.tableFooterView=footView;
    


}

-(NSMutableAttributedString *)formatTitleWithTitle:(NSString *)titleStr
{
    NSMutableAttributedString *attribute=[[NSMutableAttributedString alloc]initWithString:titleStr];
    [attribute addAttribute:NSForegroundColorAttributeName value:COLOR_GLODBACKGROUD range:NSMakeRange(0, titleStr.length)];
    [attribute addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, titleStr.length)];
    [attribute addAttribute:NSFontAttributeName value:FONT(14) range:NSMakeRange(0, titleStr.length)];
    
    return attribute;

}


-(instancetype)initWithTableFrame:(CGRect)tableFrame
{
    self=[super init];
    if (self)
    {
        _tableFrame=tableFrame;
    
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    pageNum=1;
    pageSize=10;
    totalPageNum=1;
    dataArray=[NSMutableArray array];
    [self.view addSubview:self.table];
    [self setUpTableView];
    
    
  
}

-(UITableView *)table
{
    if(!_table)
    {
        _table=[[UITableView alloc]initWithFrame:self.tableFrame style:UITableViewStylePlain];
        _table.delegate=self;
        _table.dataSource=self;
        _table.separatorStyle=UITableViewCellSeparatorStyleNone;
    }
    return _table;
}


-(void)setUpTableView{
    
    self.table.mj_header = [MJDIYHeader headerWithRefreshingBlock:^{
        pageNum=1;
        [self requestData];
    }];
    
    self.table.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        pageNum++;
        
        [self requestData];
        
    }];
    [self requestData];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArray.count;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CellHeight;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (dataArray.count<=indexPath.row) {
        return nil;
    }
    
    static NSString *cellId=@"cellId";
    DiscountCouponCell *cell=[tableView dequeueReusableCellWithIdentifier:cellId];
    if(!cell)
    {
        cell=[[DiscountCouponCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    DiscountCouponModel *model=[dataArray objectAtIndex:indexPath.row];
    [cell fillCellWithNewRedPacketsModel:model];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DiscountCouponModel *model=[dataArray objectAtIndex:indexPath.row];
    CBWebViewController*vc=[[CBWebViewController alloc]init];
    NSString *productID=model.productIds;
    if (productID.length>0)
    {

        productID=[[productID componentsSeparatedByString:@","] firstObject];
    }
    vc.productID=productID;
    vc.urlString=model.h5URL;
    vc.isDisplayNativeBtn=YES;
    [self.navigationController pushViewController:vc animated:YES];
    
}

#pragma mark---查看过期红包／优惠券
-(void)checkOutOfDateList:(UIButton *)btn
{

    OutOfDateCouponViewController *vc=[[OutOfDateCouponViewController alloc]init];
    if (self.couponType==RedPacket)
    {
         vc.couponType=RedPacket;
    }
    else
    {
         vc.couponType=DiscountCoupon;
    }
   
    [self.navigationController pushViewController:vc animated:YES];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
