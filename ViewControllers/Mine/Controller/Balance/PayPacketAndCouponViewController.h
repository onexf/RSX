//
//  PayPacketAndCouponViewController.h
//  GoldCatBank
//
//  Created by Sunny on 2017/10/24.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "BaseViewController.h"
#import "DiscountCouponCell.h"

typedef void(^UserCouponList)(DiscountCouponModel *model);

@interface PayPacketAndCouponViewController : BaseViewController

@property(nonatomic,strong)NSDictionary *dataDic;

@property(nonatomic,copy)UserCouponList userCoupon;
@end
