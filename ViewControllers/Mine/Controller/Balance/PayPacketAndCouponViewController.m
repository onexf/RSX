//
//  PayPacketAndCouponViewController.m
//  GoldCatBank
//
//  Created by Sunny on 2017/10/24.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "PayPacketAndCouponViewController.h"
#import "DiscountCouponCell.h"
#import "AllNetWorkRquest.h"


#import "MJRefresh.h"
@interface PayPacketAndCouponViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *dataList;
    NSInteger pageNum;
    NSInteger pageSize;
    NSInteger totalPageNum;
}

@property(nonatomic,strong)UITableView *table;


@end

@implementation PayPacketAndCouponViewController

-(void)requestData{
    
    if (pageNum>1)
    {
        if (pageNum>totalPageNum)
        {
            [self.table.mj_footer endRefreshingWithNoMoreData];
            return;
        }
    }
    
    __weak typeof(self) weakSelf = self;
    
    NSMutableDictionary *dataDic=[NSMutableDictionary dictionaryWithDictionary:self.dataDic];
     [dataDic setObject:[NSNumber numberWithInteger:0] forKey:@"isUse"];
    [dataDic setObject:[NSNumber numberWithInteger:-1] forKey:@"couponTypes"];
    [dataDic setObject:[NSNumber numberWithInteger:pageNum] forKey:@"pageIndex"];
    [dataDic setObject:[NSNumber numberWithInteger:pageSize] forKey:@"pageSize"];
    
    [AllNetWorkRquest getUseCouponListByProductId:dataDic
                                     successBlock:^(id responseBody){
        
        
        if (![responseBody isKindOfClass:[NSDictionary class]]) {
            
            return;
        }
        
        NSArray *arr=[DiscountCouponModel mj_objectArrayWithKeyValuesArray:responseBody[@"items"]];
        
        totalPageNum=[responseBody[@"totalPages"] integerValue];
        
        if (pageNum<=1)
        {
            
            [dataList removeAllObjects];
            
            if (arr.count>0)
            {
                [dataList addObjectsFromArray:arr];
            }
            
            if (totalPageNum<=1)
            {
                [weakSelf.table reloadData];
                [weakSelf.table.mj_header endRefreshing];
                [weakSelf.table.mj_footer endRefreshingWithNoMoreData];
                return;
                
            }
            
        }
        else
        {
            
            [dataList addObjectsFromArray:arr];
            
            if (arr.count<pageSize)
            {
                [weakSelf.table reloadData];
                [weakSelf.table.mj_header endRefreshing];
                [weakSelf.table.mj_footer endRefreshingWithNoMoreData];
                
                return;
                
            }
            
        }
        
        [weakSelf.table reloadData];
        
        
        [weakSelf.table.mj_header endRefreshing];
        [weakSelf.table.mj_footer endRefreshing];
        
        
        
    } failureBlock:^(NSString *error) {
        
        [weakSelf.table.mj_header endRefreshing];
        [weakSelf.table.mj_footer endRefreshing];
        
    }];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title=@"卡券中心";
    pageNum=1;
    pageSize=10000;
    totalPageNum=1;
    dataList=[NSMutableArray array];
    [self.view addSubview:self.table];
    [self setUpTableView];
    
    
}

-(UITableView *)table
{
    if (!_table)
    {
        _table=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-NavBarHeight)];
        _table.delegate=self;
        _table.dataSource=self;
        _table.backgroundColor=COLOR_WHITE;
        _table.separatorStyle=UITableViewCellSeparatorStyleNone;
        _table.tableFooterView=[UIView new];
    }
    
    return _table;
}


-(void)setUpTableView{
    
    self.table.mj_header = [MJDIYHeader headerWithRefreshingBlock:^{
        pageNum=1;
        [self requestData];
    }];
    self.table.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        pageNum++;
        
        [self requestData];
        
    }];
    //第一次请求数据
     [self requestData];

}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataList.count;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (dataList.count<=indexPath.row) {
        return nil;
        
    }
    static NSString *cellId=@"cellId";
    DiscountCouponCell *cell=[tableView dequeueReusableCellWithIdentifier:cellId];
    if(!cell)
    {
        cell=[[DiscountCouponCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    DiscountCouponModel *model=[dataList objectAtIndex:indexPath.row];
    [cell fillCellWithNewRedPacketsModel:model];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    DiscountCouponModel *model=[dataList objectAtIndex:indexPath.row];
    if (_userCoupon)
    {
        _userCoupon(model);
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
