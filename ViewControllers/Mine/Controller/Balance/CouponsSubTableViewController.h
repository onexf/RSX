//
//  CouponsSubTableViewController.h
//  GoldCatBank
//
//  Created by Sunny on 2017/10/19.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"


typedef enum{
    
    RedPacket  =1,
    DiscountCoupon  =2
    
}CouponType;

@interface CouponsSubTableViewController : BaseViewController
@property(nonatomic,assign)CGRect tableFrame;
@property(nonatomic,assign)CouponType couponType;
-(instancetype)initWithTableFrame:(CGRect)tableFrame;
@end
