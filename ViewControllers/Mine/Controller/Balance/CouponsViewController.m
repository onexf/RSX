//
//  CouponsViewController.m
//  GoldCatBank
//
//  Created by Sunny on 2017/10/18.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "CouponsViewController.h"
#import "DiscountCouponCell.h"
#import "MineNetWork.h"
#import "AllViewController.h"
#import "AllNetWorkRquest.h"
#import "MJRefresh.h"
#import "CouponsSubTableViewController.h"
#import "CBWebViewController.h"

@interface CouponsViewController ()<UIScrollViewDelegate>
{
    NSMutableArray *redPacketList;
    NSMutableArray *discountCouponList;
    NSInteger pageNum;
    NSInteger pageSize;
    NSInteger totalPageNum;
    BOOL isRedPacket;
}

@property(nonatomic,strong)UIView *moveView;
@property(nonatomic,strong)UIView *topView;
@property(nonatomic,strong)UIScrollView *contentScrollView;




@end

@implementation CouponsViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title=@"卡券中心";
    self.view.backgroundColor=COLOR_TBBACK;
    
    UIBarButtonItem *rightBtn=[[UIBarButtonItem alloc]initWithTitle:@"使用说明" style:UIBarButtonItemStylePlain target:self action:@selector(rightBtnClick)];
    [rightBtn setTitleTextAttributes:@{NSForegroundColorAttributeName:COLOR_GLODBACKGROUD} forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem=rightBtn;
    isRedPacket=YES;//现金红包
    pageNum=1;
    pageSize=10;
    totalPageNum=1;
    redPacketList=[NSMutableArray arrayWithArray:@[@"",@"",@""]];
    discountCouponList=[NSMutableArray array];
    self.automaticallyAdjustsScrollViewInsets=NO;
    [self.view addSubview:self.topView];
    [self.view addSubview:self.contentScrollView];

    
}


-(UIView *)topView
{
    if (!_topView)
    {
        _topView=[[UIView alloc]initWithFrame:CGRectMake(0,10, SCREEN_WIDTH, 46)];
        _topView.backgroundColor=COLOR_WHITE;
        
        //  现金红包
        UIButton *redPacketBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        redPacketBtn.frame=CGRectMake(0, 0, 150, _topView.ct_height-1);
        redPacketBtn.center=CGPointMake(SCREEN_WIDTH/4.0, _topView.ct_height/2.0-0.5);
        [redPacketBtn setTitle:@"现金红包" forState:UIControlStateNormal];
        [redPacketBtn setTitleColor:COLOR_Red forState:UIControlStateSelected];
        [redPacketBtn setTitleColor:COLOR_TEXT forState:UIControlStateNormal];
        redPacketBtn.tag=100;
        [redPacketBtn addTarget:self action:@selector(swichStateClick:) forControlEvents:UIControlEventTouchUpInside];
        redPacketBtn.titleLabel.font=FONT(15);
        [_topView addSubview:redPacketBtn];
        
        // 优惠券
        UIButton *discountCouponBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        discountCouponBtn.frame=CGRectMake(0, 0, redPacketBtn.ct_width, redPacketBtn.ct_height);
        discountCouponBtn.center=CGPointMake(SCREEN_WIDTH*3/4.0, redPacketBtn.center.y);
        [discountCouponBtn setTitle:@"优惠券" forState:UIControlStateNormal];
        [discountCouponBtn setTitleColor:COLOR_Red forState:UIControlStateSelected];
        [discountCouponBtn setTitleColor:COLOR_TEXT forState:UIControlStateNormal];
        discountCouponBtn.tag=200;
        [discountCouponBtn addTarget:self action:@selector(swichStateClick:) forControlEvents:UIControlEventTouchUpInside];
        discountCouponBtn.titleLabel.font=FONT(15);
        [_topView addSubview:discountCouponBtn];
        
        //非过期红包优惠券进来默认选中红包
        redPacketBtn.selected=YES;
        //  移动红条
        UIView *moveView=[[UIView alloc]initWithFrame:CGRectMake(0,redPacketBtn.ct_bottom, SCREEN_WIDTH/2.0, 1)];
        moveView.backgroundColor=COLOR_Red;
        [_topView addSubview:moveView];
        
        self.moveView=moveView;
        
    }
    
    return _topView;
}


-(UIScrollView *)contentScrollView
{
    if (!_contentScrollView)
    {
        _contentScrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, self.topView.ct_bottom+10, SCREEN_WIDTH, SCREEN_HEIGHT-self.topView.ct_bottom-NavBarH)];
        _contentScrollView.contentSize=CGSizeMake(SCREEN_WIDTH*2,SCREEN_HEIGHT-self.topView.ct_bottom-NavBarH);
        _contentScrollView.pagingEnabled=YES;
//        _contentScrollView.scrollEnabled=NO;
        _contentScrollView.delegate=self;
        _contentScrollView.showsVerticalScrollIndicator=NO;
        _contentScrollView.showsHorizontalScrollIndicator=NO;
        _contentScrollView.backgroundColor=COLOR_TBBACK;

        //红包
        CouponsSubTableViewController *redPacketTableVC=[[CouponsSubTableViewController alloc]initWithTableFrame:CGRectMake(0, 0, SCREEN_WIDTH, _contentScrollView.ct_height)];
        redPacketTableVC.couponType=RedPacket;
        redPacketTableVC.view.frame=CGRectMake(0, 0, SCREEN_WIDTH, _contentScrollView.ct_height);
        [_contentScrollView addSubview:redPacketTableVC.view];
        [self addChildViewController:redPacketTableVC];
        
        //优惠券
        CouponsSubTableViewController *discountTableVC=[[CouponsSubTableViewController alloc]initWithTableFrame:CGRectMake(0, 0, SCREEN_WIDTH, _contentScrollView.ct_height)];
        discountTableVC.couponType=DiscountCoupon;
        discountTableVC.view.frame=CGRectMake(SCREEN_WIDTH, 0, SCREEN_WIDTH, _contentScrollView.ct_height);
        [_contentScrollView addSubview:discountTableVC.view];
        [self addChildViewController:discountTableVC];
        
        
    }
    
    return _contentScrollView;

}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
    NSInteger a =scrollView.contentOffset.x/SCREEN_WIDTH;
    UIButton *redPacketBtn=(UIButton *)[self.topView viewWithTag:100];
    UIButton *discountBtn=(UIButton *)[self.topView viewWithTag:200];
    if (a>0)
    {//优惠券
       
        self.moveView.frame=CGRectMake(SCREEN_WIDTH/2.0, discountBtn.ct_bottom, SCREEN_WIDTH/2.0, 1);
       
        redPacketBtn.selected=NO;
        discountBtn.selected=YES;
        
    }
    else
    {//现金红包
        self.moveView.frame=CGRectMake(0, redPacketBtn.ct_bottom, SCREEN_WIDTH/2.0, 1);
        
        redPacketBtn.selected=YES;
        discountBtn.selected=NO;
        
    }
    
    
}

#pragma mark---切换事件
-(void)swichStateClick:(UIButton *)btn
{
   
    if (btn.tag==100)
    {
        //现金红包
        self.moveView.frame=CGRectMake(0, btn.ct_bottom, SCREEN_WIDTH/2.0, 1);
        UIButton *otherBtn=(UIButton *)[self.topView viewWithTag:200];
        otherBtn.selected=NO;
        btn.selected=YES;
        self.contentScrollView.contentOffset=CGPointMake(0, 0);
        
    }
    else
    {
        //优惠券 tag=200
        self.moveView.frame=CGRectMake(SCREEN_WIDTH/2.0, btn.ct_bottom, SCREEN_WIDTH/2.0, 1);
        UIButton *otherBtn=(UIButton *)[self.topView viewWithTag:100];
        otherBtn.selected=NO;
        btn.selected=YES;
        self.contentScrollView.contentOffset=CGPointMake(SCREEN_WIDTH, 0);
        
    }
    
    
}


#pragma mark---
-(void)rightBtnClick{
    
    CBWebViewController *webVC=[[CBWebViewController alloc]init];
    
    if (self.contentScrollView.contentOffset.x>0)
    {
       webVC.urlString=@"https://www.zhaojinmao.cn/testwebapp/me/usedescription/usedescription2.html";
    }
    else
    {    
       webVC.urlString=@"https://www.zhaojinmao.cn/testwebapp/me/usedescription/usedescription.html";
    }

    [self.navigationController pushViewController:webVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
