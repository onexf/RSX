//
//  BalanceTradeVC.m
//  GoldCatBank
//
//  Created by Sunny on 2017/9/5.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "BalanceTradeVC.h"
#import "GlodTradeCell.h"
#import "MineNetWork.h"
#import "UIBarButtonItem+item.h"
#import "TradeDetailViewController.h"
#import "MJRefresh.h"
#import "RecordDetailsViewController.h"
@interface BalanceTradeVC ()<UITableViewDelegate,UITableViewDataSource,TradeDelegate>
{
    NSInteger pageNum;
    NSInteger pageCount;
    NSMutableArray *dataList;
    NSInteger totalPageNumber;
    NSInteger productType;
}
@property(nonatomic,strong)UITableView *table;
@property(nonatomic,strong)TradeBounceView *bounceView;

@end

@implementation BalanceTradeVC

-(void)requestData:(NSInteger)type
{
    if (pageNum>1)
    {
        if (pageNum>totalPageNumber)
        {
            [self.table.mj_footer endRefreshingWithNoMoreData];
            return;
        }
    }

    NSNumber *typeNum=[NSNumber numberWithInteger:type];

    
    __weak typeof(self) weakSelf = self;
    NSDictionary *dic=@{@"logType":typeNum,@"isExpire":[NSNumber numberWithInteger:-1],@"listType":[NSNumber numberWithInteger:7],@"pageSize":[NSNumber numberWithInteger:pageCount],@"pageIndex":[NSNumber numberWithInteger:pageNum]};//

    [MineNetWork NewOrderListWithDataDic:dic successBlock:^(id responseBody) {

        if (![responseBody isKindOfClass:[NSDictionary class]]) {
            
            return;
        }

        NSArray *arr=[GlodTradeModel mj_objectArrayWithKeyValuesArray:responseBody[@"items"]];
        
        totalPageNumber=[responseBody[@"totalPages"] integerValue];
        
        if (pageNum<=1)
        {
            
            [dataList removeAllObjects];
            
            if (arr.count>0)
            {
                if (self.blankView)
                {
                    self.blankView.hidden=YES;
                }
                
                for (GlodTradeModel *model in arr)
                {
                    [dataList addObject:model];
                }
            }
            else
            {
                if (!self.blankView)
                {
                    [self showBlankPageWithType:BlankPageOrderListNOData withFrame:self.table.frame switchView:self.table];
                    
                }
                else
                {
                    self.blankView.hidden=NO;
                }
            }
            
            if (totalPageNumber<=1)
            {
                [weakSelf.table reloadData];
                [weakSelf.table.mj_header endRefreshing];
                [weakSelf.table.mj_footer endRefreshingWithNoMoreData];
                return;
                
            }
            
        }
        else
        {
            
            for (GlodTradeModel *model in arr)
            {
               [dataList addObject:model];
            }

            
            if (arr.count<pageCount)
            {
                [weakSelf.table reloadData];
                [weakSelf.table.mj_header endRefreshing];
                [weakSelf.table.mj_footer endRefreshingWithNoMoreData];
                
                return;

            }
            
        }
        
        [weakSelf.table reloadData];
        
        
        [weakSelf.table.mj_header endRefreshing];
        [weakSelf.table.mj_footer endRefreshing];

        
    } failureBlock:^(NSString *error) {
        
        
        if (pageNum==1)
        {
            [dataList removeAllObjects];
           [weakSelf.table reloadData];
        }
    
        
        [weakSelf.table.mj_header endRefreshing];
        [weakSelf.table.mj_footer endRefreshing];
        
    }];
}

-(instancetype)initWithOrderType:(OrderType)type
{
    self=[super init];
    if (self)
    {
        _type=type;
    }
    return self;
}

-(instancetype)init
{
    self=[super init];
    if (self)
    {
        _type=-1;
    }
    return self;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UIBarButtonItem *rightBtn=[[UIBarButtonItem alloc]initWithTitle:@"筛选" style:UIBarButtonItemStylePlain target:self action:@selector(rightBtnClick:)];
    [rightBtn setTitleTextAttributes:@{NSForegroundColorAttributeName:COLOR_GLODBACKGROUD} forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem=rightBtn;

    self.title = @"订单记录";
    
   
    UIButton *backBarButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    [backBarButton setImage:[UIImage imageNamed:@"navigationButtonReturn"] forState:UIControlStateNormal];
    [backBarButton setImage:[UIImage imageNamed:@"navigationButtonReturnClick"] forState:UIControlStateHighlighted];
    backBarButton.jk_touchAreaInsets = UIEdgeInsetsMake(20, 20, 20, 20);
    backBarButton.imageEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 25);
    [backBarButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backBarButton];


    dataList=[NSMutableArray array];
    pageCount=10;
    pageNum=1;
    totalPageNumber=0;
    productType=self.type;

    
    [self.view addSubview:self.table];
   [self showBlankPageWithType:BlankPageOrderListNOData withFrame:self.table.frame switchView:self.table];

    [self setUpTableView];
    [self.view addSubview:self.bounceView];
    self.bounceView.delegate=self;

}

-(TradeBounceView *)bounceView
{
    if (!_bounceView)
    {
        _bounceView=[[TradeBounceView alloc]initWithFrame:CGRectMake(0, -SCREEN_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT) withSelect:self.type];
       // 0：投资；1：买金；2：卖金；3：赎回；4：充值；5：提现；6：到期；7：其他
        _bounceView.titleArray=@[@"全部类型",@"投资",@"买金",@"卖金",@"赎回",@"充值",@"提现",@"到期",@"其他"];
        _bounceView.showTop=0;
        _bounceView.delegate=self;
    }
    
    return _bounceView;
}
-(UITableView *)table
{
    if(!_table)
    {
        _table=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-NavBarHeight) style:UITableViewStylePlain];
        _table.delegate=self;
        _table.dataSource=self;
        _table.separatorStyle=UITableViewCellSeparatorStyleNone;
    }
    return _table;
}


-(void)setUpTableView{
    
    self.table.mj_header = [MJDIYHeader headerWithRefreshingBlock:^{
        pageNum=1;
        [self requestData:productType];
    }];
    
    self.table.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            pageNum++;
            
            [self requestData:productType];
            
    }];
        //第一次请求数据
    [self requestData:productType];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 110;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID=@"cellID";
    GlodTradeCell *cell=[tableView dequeueReusableCellWithIdentifier:cellID];
   
    if (!cell)
    {
        cell=[[GlodTradeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    GlodTradeModel *mdel=[dataList objectAtIndex:indexPath.row];
    [cell fillCellWithModel:mdel];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    GlodTradeModel *model = dataList[indexPath.row];
    RecordDetailsViewController *detailsVC = [[RecordDetailsViewController alloc] initWithData:model];
    [self.navigationController pushViewController:detailsVC animated:YES];
}

#pragma mark--you 按钮
-(void)rightBtnClick:(UIBarButtonItem *)sender
{

    
    if ([sender.title isEqualToString:@"筛选"])
    {
        sender.title=@"收起";
   
        [self.bounceView show];
    }
    else
    {
        sender.title=@"筛选";
        
        [self.bounceView hidden];
    }
  
}

#pragma mark---分类代理
-(void)selectItemWithIndex:(NSInteger)index
{
    self.navigationItem.rightBarButtonItem.title=@"筛选";
    
    [self.bounceView hidden];
    pageNum=1;
    self.table.contentOffset=CGPointMake(0, 0);
    
    productType=index;

    [self requestData:productType];
    
    
}


-(void)back
{
    if (self.isSuccessFinish)
    {
         [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else
    {
         [self.navigationController popViewControllerAnimated:YES];
    }
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
