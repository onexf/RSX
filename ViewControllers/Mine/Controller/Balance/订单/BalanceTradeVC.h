//
//  BalanceTradeVC.h
//  GoldCatBank
//
//  Created by Sunny on 2017/9/5.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

//-1:全部类型  0：投资；1：买金；2：卖金；3：赎回；4：充值；5：提现；6：到期；7：其他

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "TradeBounceView.h"

@interface BalanceTradeVC : BaseViewController
@property(nonatomic,assign)OrderType type;
@property(nonatomic,assign)BOOL isSuccessFinish;//针对提现成功完成页 ：yes，返回到rootview
-(instancetype)initWithOrderType:(OrderType)type;
@end
