//
//  WithdrawalsListViewController.m
//  GoldCatBank
//
//  Created by Sunny on 2017/9/6.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "WithdrawalsListViewController.h"
#import "WithdrawalsCell.h"
#import "MineNetWork.h"
#import "MJRefresh.h"

@interface WithdrawalsListViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *dataList;
    NSInteger pageNum;
    NSInteger pageCount;
    NSInteger totalPageNum;
}

@property(nonatomic,strong)UITableView *table;

@end

@implementation WithdrawalsListViewController

-(void)requestData
{

    
    if (pageNum>1)
    {
        if (pageNum>totalPageNum)
        {
            [self.table.mj_footer endRefreshingWithNoMoreData];
            return;
        }
    }
    
    __weak typeof(self) weakSelf = self;
    NSDictionary *dic=@{@"type":[NSNumber numberWithInt:1],@"pageIndex":[NSString stringWithFormat:@"%ld",pageNum],@"pageSize":[NSString stringWithFormat:@"%ld",pageCount]};
    
    [MineNetWork WithdrawalsListWithDataDic:dic successBlock:^(id responseBody) {
        
        if (![responseBody isKindOfClass:[NSDictionary class]]) {
            
            return;
        }
        
        NSLog(@"responseBody====%@",responseBody);
        
        NSArray *arr=[WithdrawalsListModel mj_objectArrayWithKeyValuesArray:responseBody[@"items"]];
        
        totalPageNum=[responseBody[@"totalPages"] integerValue];
        
        if (pageNum<=1)
        {
            
            [dataList removeAllObjects];
            
            if (arr.count>0)
            {
                [dataList addObjectsFromArray:arr];
            }
            
            if (totalPageNum<=1)
            {
                [weakSelf.table reloadData];
                [weakSelf.table.mj_header endRefreshing];
                [weakSelf.table.mj_footer endRefreshingWithNoMoreData];
                return;
                
            }
            
        }
        else
        {
            
            [dataList addObjectsFromArray:arr];
            
            if (arr.count<pageCount)
            {
                [weakSelf.table reloadData];
                [weakSelf.table.mj_header endRefreshing];
                [weakSelf.table.mj_footer endRefreshingWithNoMoreData];
                
                return;
                
            }
            
        }
        
        [weakSelf.table reloadData];
        
        
        [weakSelf.table.mj_header endRefreshing];
        [weakSelf.table.mj_footer endRefreshing];
        


        
    } failureBlock:^(NSString *error) {
        
        if (pageNum==1)
        {
            [dataList removeAllObjects];
            [weakSelf.table reloadData];
        }
        
        [weakSelf.table.mj_header endRefreshing];
        [weakSelf.table.mj_footer endRefreshing];
        

        
    }];
    

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title=@"提现记录";
    pageNum=1;
    totalPageNum=1;
    pageCount=10;
    dataList=[NSMutableArray array];
    [self.view addSubview:self.table];
    [self setUpTableView];
    
}

-(UITableView *)table
{
    if (!_table)
    {
        _table=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-NavBarHeight)];
        _table.delegate=self;
        _table.dataSource=self;
        _table.backgroundColor=COLOR_WHITE;
        _table.separatorStyle=UITableViewCellSeparatorStyleNone;
        _table.tableHeaderView=[self setTableHeadView];
        _table.tableFooterView=[UIView new];
    }
    
    return _table;
}

-(void)setUpTableView{
    
    self.table.mj_header = [MJDIYHeader headerWithRefreshingBlock:^{
        pageNum=1;
        [self requestData];
    }];
    self.table.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        pageNum++;
        
        [self requestData];
        
    }];
    //第一次请求数据
    [self requestData];
}



-(UIView *)setTableHeadView
{
    CMBorderView *headView=[[CMBorderView alloc]initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, 50)];
    headView.borderWidth=CMViewBorderWidthMake(0.5, 0, 0.5, 0);
    headView.backgroundColor=COLOR_TBBACK;
    
    UILabel *timeLab=[[UILabel alloc]initWithFrame:CGRectMake(20, 15, 90, 20)];
    timeLab.text=@"时间";
    timeLab.textColor=COLOR_TEXT;
    timeLab.font=FONT(15);
    [headView addSubview:timeLab];
    
    UILabel *amountLab=[[UILabel alloc]initWithFrame:CGRectMake(timeLab.ct_right+10, timeLab.ct_top,SCREEN_WIDTH-timeLab.ct_right-10-70, 20)];
    amountLab.textColor=COLOR_TEXT;
    amountLab.font=FONT(15);
    amountLab.text=@"金额";
    amountLab.textAlignment=NSTextAlignmentCenter;
    [headView addSubview:amountLab];
    
    UILabel *statelab=[[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-60, amountLab.ct_top, 50, 20)];
    statelab.textAlignment=NSTextAlignmentCenter;
    statelab.font=FONT(15);
    statelab.text=@"状态";
    statelab.textColor=COLOR_TEXT;
    [headView addSubview:statelab];
    
    return headView;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataList.count;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId=@"cellId";
    WithdrawalsCell *cell=[tableView dequeueReusableCellWithIdentifier:cellId];
    if(!cell)
    {
        cell=[[WithdrawalsCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    WithdrawalsListModel *model=[dataList objectAtIndex:indexPath.row];
    [cell fillCellWithModel:model];
    return cell;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
