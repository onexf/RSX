//
//  OutOfDateCouponViewController.m
//  GoldCatBank
//
//  Created by Sunny on 2017/10/24.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "OutOfDateCouponViewController.h"
#import "DiscountCouponCell.h"
#import "MineNetWork.h"
#import "MJRefresh.h"

#define CellHeight 90

@interface OutOfDateCouponViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *dataArray;
    NSInteger pageNum;
    NSInteger pageSize;
    NSInteger totalPageNum;
}


@property(nonatomic,strong)UITableView *table;

@end

@implementation OutOfDateCouponViewController

-(void)requestData
{
    
    __weak typeof(self) weakSelf = self;
    
    NSDictionary *dic=nil;
    if (self.couponType==1)
    {
        //红包
        dic=@{@"isCanUse":[NSNumber numberWithInteger:0],@"couponTypes":@"0,1",@"isExpire":[NSNumber numberWithInteger:1],@"pageSize":[NSNumber numberWithInteger:pageSize],@"pageIndex":[NSNumber numberWithInteger:pageNum]};
    }
    else
    {
        //优惠券
        dic=@{@"isCanUse":[NSNumber numberWithInteger:0],@"couponTypes":@"2",@"isExpire":[NSNumber numberWithInteger:1],@"pageSize":[NSNumber numberWithInteger:pageSize],@"pageIndex":[NSNumber numberWithInteger:pageNum]};
        
    }
    
    [MineNetWork getMyCouponListWithDataDic:dic
                               successBlock:^(id responseBody) {
                                                                      
                                   if (![responseBody isKindOfClass:[NSDictionary class]]) {
                                       
                                       return;
                                   }
                                   
                                   
                                   if (self.blankView)
                                   {
                                       self.blankView.hidden=YES;
                                   }
                                   
                                   NSArray *arr=[DiscountCouponModel mj_objectArrayWithKeyValuesArray:responseBody[@"items"]];
                                   
                                   totalPageNum=[responseBody[@"totalPages"] integerValue];
                                   
                                   if (pageNum<=1)
                                   {
                                       
                                       [dataArray removeAllObjects];
                                       
                                       if (arr.count>0)
                                       {
                                           [dataArray addObjectsFromArray:arr];
                                       }
                                       
                                       [self setTableFootView];
                                       
                                       
                                       [weakSelf.table reloadData];
                                       [weakSelf.table.mj_header endRefreshing];
                                       [weakSelf.table.mj_footer endRefreshingWithNoMoreData];
                                       
                                     
                                       
                                   }
          
                  
                                   
                               }
                               failureBlock:^(NSString *error) {
                                   
                                   if ([error isEqualToString:@"查询成功无数据"])
                                   {
                                       if (self.couponType==1)
                                       {
                                           if (!self.blankView)
                                           {
                                               [self showBlankPageWithType:BlankPageRedPacketNOData withFrame:self.table.frame switchView:self.table];
                                           }
                                           else
                                           {
                                               self.blankView.hidden=NO;
                                           }
                                           
                                           
                                           
                                       }
                                       else
                                       {
                                           if (!self.blankView)
                                           {
                                               
                                               [self showBlankPageWithType:BlankPageCouponNOData withFrame:self.table.frame switchView:self.table];
                                           }
                                           else
                                           {
                                               self.blankView.hidden=NO;
                                           }
                                           
                                       }
                                       
                                   }
                                   [weakSelf.table.mj_header endRefreshing];
                                   [weakSelf.table.mj_footer endRefreshingWithNoMoreData];
                                   
                                   
                               }];
    
    
    
    
}


-(void)setTableFootView
{
    CGFloat tableHeight=self.table.ct_height;
    
    CGFloat cellSHeight=CellHeight*dataArray.count;
    
    CGFloat intervalHeight=tableHeight-cellSHeight;
    
    UIView *footView=[[UIView alloc]init];
    footView.backgroundColor=COLOR_TBBACK;
    
    UILabel *tipLab=[[UILabel alloc]init];
    tipLab.frame=CGRectMake(0, 0, 200, 30);
    tipLab.font=FONT(14);
    tipLab.textColor=COLOR_SUBTEXT;
    tipLab.textAlignment=NSTextAlignmentCenter;
    
    if (intervalHeight>=80)
    {
        footView.frame=CGRectMake(0, 0, SCREEN_WIDTH, intervalHeight);
        tipLab.center=CGPointMake(SCREEN_WIDTH/2.0, intervalHeight-30/2.0-20);
    }
    else
    {
        footView.frame=CGRectMake(0, 0, SCREEN_WIDTH, 80);
        tipLab.center=CGPointMake(SCREEN_WIDTH/2.0, 30+30/2.0);
    }
    
    if (self.couponType==1)
    {
        //红包
        tipLab.text=@"只显示近期10张过期红包";
    }
    else
    {
        //优惠券
        tipLab.text=@"只显示近期10张过期优惠券";
    }
    
    [footView addSubview:tipLab];
    
    self.table.tableFooterView=footView;
    
    
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"卡券中心";
    pageNum=1;
    pageSize=10;
    dataArray=[NSMutableArray array];
    
    [self.view addSubview:self.table];
    [self setUpTableView];
    
}


-(UITableView *)table
{
    if (!_table)
    {
        _table=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-NavBarHeight)];
        _table.delegate=self;
        _table.dataSource=self;
        _table.backgroundColor=COLOR_WHITE;
        _table.separatorStyle=UITableViewCellSeparatorStyleNone;
        _table.tableFooterView=[UIView new];
    }
    
    return _table;
}


-(void)setUpTableView{
    
    self.table.mj_header = [MJDIYHeader headerWithRefreshingBlock:^{
        pageNum=1;
        [self requestData];
    }];
    self.table.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//        pageNum++;
//
//         [self requestData];

    }];
    //第一次请求数据
   [self requestData];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArray.count;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CellHeight;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (dataArray.count<=indexPath.row) {
        return nil;
        
    }
    
    static NSString *cellId=@"cellId";
    DiscountCouponCell *cell=[tableView dequeueReusableCellWithIdentifier:cellId];
    if(!cell)
    {
        cell=[[DiscountCouponCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    DiscountCouponModel *model=[dataArray objectAtIndex:indexPath.row];
    [cell fillCellWithNewOutOfDateModel:model];
    return cell;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
