//
//  LoginViewController.h
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/8/25.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LoginDelegate <NSObject>

- (void)loginSuccess;

@end
@interface LoginViewController : UIViewController

/** 登录代理 */
@property(nonatomic, weak) id<LoginDelegate> loginDelegate;


@end
