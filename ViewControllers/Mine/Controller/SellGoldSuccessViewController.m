//
//  SellGoldSuccessViewController.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/14.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "SellGoldSuccessViewController.h"
#import <FDFullscreenPopGesture/UINavigationController+FDFullscreenPopGesture.h>
#import "SellOutModel.h"
@interface SellGoldSuccessViewController ()
/** topView */
@property(nonatomic, strong) UIView *topView;
/** title */
@property(nonatomic, strong) UILabel *titleLabel;
/** rightBar */
@property(nonatomic, strong) UIButton *rightBarButton;
/** 赎回金额 */
@property(nonatomic, strong) UILabel *sellAmount;
/** name  零钱罐 */
@property(nonatomic, strong) UILabel *nameLabel;
/** midView */
@property(nonatomic, strong) UIView *midView;
/** 赎回金额 */
@property(nonatomic, strong) UILabel *sellAmountRedLabel;
/** 预计到账时间 */
@property(nonatomic, strong) UILabel *timeLabel;
/** 到账时间 */
@property(nonatomic, strong) UILabel *time;
/** 手续费 */
@property(nonatomic, strong) UILabel *chargeLabel;
/**  */
@property(nonatomic, strong) UILabel *charge;
/** 交易金额 */
@property(nonatomic, strong) UILabel *amountLabel;
/**  */
@property(nonatomic, strong) UILabel *amount;
/** 剩余克重 */
@property(nonatomic, strong) UILabel *laveLabel;
/**  */
@property(nonatomic, strong) UILabel *lave;

@end

@implementation SellGoldSuccessViewController


#pragma mark - 事件处理
- (void)sellDone {
    [self.navigationController jk_popToViewControllerWithLevel:2 animated:YES];
}
#pragma mark - 初始化，布局
- (void)layOut {
    [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0, *)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
        } else {
            make.top.equalTo(self.view);
        }
        make.left.right.equalTo(self.view);
        make.height.equalTo(@200);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.topView.mas_top).offset(44);
        make.centerX.equalTo(self.topView);
    }];
    [self.rightBarButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.titleLabel);
        make.right.equalTo(self.topView).offset(-12);
    }];
    [self.sellAmount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.topView).offset(89);
        make.centerX.equalTo(self.topView);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.sellAmount.mas_bottom).offset(25);
        make.centerX.equalTo(self.topView);
    }];
    [self.midView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.topView.mas_bottom).offset(10);
        make.left.equalTo(self.view).offset(LEFT_RIGHT_MARGIN);
        make.right.equalTo(self.view).offset(-LEFT_RIGHT_MARGIN);
        make.bottom.equalTo(self.laveLabel.mas_bottom).offset(20);
    }];
    [self.sellAmountRedLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.midView).offset(10);
        make.centerX.equalTo(self.midView);
    }];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.sellAmountRedLabel.mas_bottom).offset(10);
        make.left.equalTo(self.midView).offset(30);
    }];
    [self.time mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.timeLabel);
        make.right.equalTo(self.midView).offset(-30);
    }];
    [self.chargeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.timeLabel.mas_bottom).offset(10);
        make.left.equalTo(self.timeLabel);
    }];
    [self.charge mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.chargeLabel);
        make.right.equalTo(self.time);
    }];
    [self.amountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.chargeLabel.mas_bottom).offset(10);
        make.left.equalTo(self.chargeLabel);
    }];
    [self.amount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.amountLabel);
        make.right.equalTo(self.charge);
    }];
    [self.laveLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.amountLabel.mas_bottom).offset(10);
        make.left.equalTo(self.amountLabel);
    }];
    [self.lave mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.laveLabel);
        make.right.equalTo(self.amount);
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.fd_prefersNavigationBarHidden = YES;
    self.fd_interactivePopDisabled = YES;
    self.view.backgroundColor = COLOR_TBBACK;
    
    [self.view addSubview:self.topView];
    [self.view addSubview:self.midView];
    
    if (@available(iOS 11.0, *)) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 44)];
        view.backgroundColor = COLOR_MAIN;
        [self.view insertSubview:view atIndex:0];
    }
    [self layOut];
}


/**
 懒加载
 
 @return 到账时间
 */
- (UILabel *)lave {
    if (!_lave) {
        _lave = [[UILabel alloc] init];
        _lave.textColor = COLOR_TEXT;
        _lave.font = FONT_TEXT;
        _lave.text = [NSString stringWithFormat:@"%@克", self.sellOutData.totalRecruitFinance];
    }
    return _lave;
}

/**
 懒加载
 
 @return 预计到账时间
 */
- (UILabel *)laveLabel {
    if (!_laveLabel) {
        _laveLabel = [[UILabel alloc] init];
        _laveLabel.text = @"剩余黄金";
        _laveLabel.textColor = COLOR_TEXT;
        _laveLabel.font = FONT_TEXT;
    }
    return _laveLabel;
}

/**
 懒加载
 
 @return 到账时间
 */
- (UILabel *)amount {
    if (!_amount) {
        _amount = [[UILabel alloc] init];
        _amount.textColor = COLOR_TEXT;
        _amount.font = FONT_TEXT;
        _amount.text = [NSString stringWithFormat:@"%@元", self.sellOutData.sellAmount];
    }
    return _amount;
}

/**
 懒加载
 
 @return 预计到账时间
 */
- (UILabel *)amountLabel {
    if (!_amountLabel) {
        _amountLabel = [[UILabel alloc] init];
        _amountLabel.text = @"交易金额";
        _amountLabel.textColor = COLOR_TEXT;
        _amountLabel.font = FONT_TEXT;
    }
    return _amountLabel;
}
/**
 懒加载
 
 @return 到账时间
 */
- (UILabel *)charge {
    if (!_charge) {
        _charge = [[UILabel alloc] init];
        _charge.textColor = COLOR_TEXT;
        _charge.font = FONT_TEXT;
        _charge.text = [NSString stringWithFormat:@"%@元", self.sellOutData.serviceCharge];
    }
    return _charge;
}

/**
 懒加载
 
 @return 预计到账时间
 */
- (UILabel *)chargeLabel {
    if (!_chargeLabel) {
        _chargeLabel = [[UILabel alloc] init];
        _chargeLabel.text = @"手续费";
        _chargeLabel.textColor = COLOR_TEXT;
        _chargeLabel.font = FONT_TEXT;
    }
    return _chargeLabel;
}
/**
 懒加载
 
 @return 到账时间
 */
- (UILabel *)time {
    if (!_time) {
        _time = [[UILabel alloc] init];
        _time.textColor = COLOR_TEXT;
        _time.font = FONT_TEXT;
        _time.text = [NSString stringWithFormat:@"%@克", self.sellOutData.sellGoldWieght];
    }
    return _time;
}

/**
 懒加载
 
 @return 预计到账时间
 */
- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.text = @"卖出克重";
        _timeLabel.textColor = COLOR_TEXT;
        _timeLabel.font = FONT_TEXT;
    }
    return _timeLabel;
}
/**
 懒加载
 
 @return 赎回金额
 */
- (UILabel *)sellAmountRedLabel {
    if (!_sellAmountRedLabel) {
        _sellAmountRedLabel = [[UILabel alloc] init];
        _sellAmountRedLabel.font = FONT_TEXT;
        _sellAmountRedLabel.textColor = COLOR_TEXT;
        NSString *string = [NSString stringWithFormat:@"售出黄金%@克", self.sellOutData.sellGoldWieght];
        NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:string];
        [text addAttributes:@{NSForegroundColorAttributeName:COLOR_Red} range:NSMakeRange(4, self.sellOutData.sellGoldWieght.length)];
        _sellAmountRedLabel.attributedText = text;
    }
    return _sellAmountRedLabel;
}
/**
 懒加载
 
 @return 到账日
 */
- (UIView *)midView {
    if (!_midView) {
        _midView = [[UIView alloc] init];
        _midView.backgroundColor = COLOR_WHITE;
        _midView.layer.cornerRadius = 10.0f;
        _midView.clipsToBounds = YES;
        [_midView addSubview:self.sellAmountRedLabel];
        [_midView addSubview:self.timeLabel];
        [_midView addSubview:self.time];
        [_midView addSubview:self.chargeLabel];
        [_midView addSubview:self.charge];
        [_midView addSubview:self.amountLabel];
        [_midView addSubview:self.amount];
        [_midView addSubview:self.laveLabel];
        [_midView addSubview:self.lave];
    }
    return _midView;
}
/**
 懒加载
 
 @return 零钱罐
 */
- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.text = @"招财金";
        _nameLabel.textColor = COLOR_SUBTEXT;
        _nameLabel.font = FONT_TEXT;
    }
    return _nameLabel;
}
/**
 懒加载
 
 @return 赎回金额
 */
- (UILabel *)sellAmount {
    if (!_sellAmount) {
        _sellAmount = [[UILabel alloc] init];
        _sellAmount.font = FONT(30);
        _sellAmount.textColor = COLOR_WHITE;
        _sellAmount.text = [NSString stringWithFormat:@"%@克", self.sellOutData.sellGoldWieght];
    }
    return _sellAmount;
}
/**
 懒加载
 
 @return 完成
 */
- (UIButton *)rightBarButton {
    if (!_rightBarButton) {
        _rightBarButton = [[UIButton alloc] init];
        _rightBarButton.titleLabel.font = FONT_TITLE;
        [_rightBarButton setTitle:@"完成" forState:UIControlStateNormal];
        [_rightBarButton setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
        [_rightBarButton addTarget:self action:@selector(sellDone) forControlEvents:UIControlEventTouchUpInside];
        [_rightBarButton sizeToFit];
        _rightBarButton.jk_touchAreaInsets = UIEdgeInsetsMake(20, 20, 20, 20);
    }
    return _rightBarButton;
}
/**
 懒加载
 
 @return 赎回等待
 */
- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = @"卖出成功";
        _titleLabel.textColor = COLOR_WHITE;
        _titleLabel.font = FONT_NAV;
    }
    return _titleLabel;
}
/**
 懒加载
 
 @return 顶部
 */
- (UIView *)topView {
    if (!_topView) {
        _topView = [[UIView alloc] init];
        _topView.backgroundColor = COLOR_MAIN;
        [_topView addSubview:self.titleLabel];
        [_topView addSubview:self.rightBarButton];
        [_topView addSubview:self.sellAmount];
        [_topView addSubview:self.nameLabel];
    }
    return _topView;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
