//
//  SettingViewController.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/8/29.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "SettingViewController.h"
#import "UserAuthData.h"
#import "BindBankCarViewController.h"
#import "SetChargePasswordViewController.h"
#import "ResetTransPwdViewController.h"
#import "SettingNoticeView.h"
#import "IQKeyboardManager.h"
#import "UIView+YSTextInputKeyboard.h"
#import "JPUSHService.h"
#import "UIView+TYAlertView.h"

@interface SettingViewController ()<UITableViewDelegate, UITableViewDataSource>

/** tableView */
@property(nonatomic, strong) UITableView *tableView;
/** 列表数据 */
@property(nonatomic, strong) NSArray *dataSource;
/** 退出登录 */
@property(nonatomic, strong) UIButton *logoutButton;
/** sectionheaderView */
@property(nonatomic, strong) UIView *headerView;
/** uiscitch */
@property(nonatomic, strong) UISwitch *cellSwitch;
@end

@implementation SettingViewController

#pragma mark - 事件处理
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}
- (void)notification:(NSNotification *)notification {
    [self.tableView reloadData];
}
- (void)switchAction:(UISwitch *)cellSwitch {
    NSString *title = ([[UIApplication sharedApplication] currentUserNotificationSettings].types  == UIUserNotificationTypeNone) ? @"您的推送功能尚未开启，不能使用推送功能" : @"进入系统设置关闭推送功能";
    NSString *subTitle = ([[UIApplication sharedApplication] currentUserNotificationSettings].types  == UIUserNotificationTypeNone) ? @"去开启" : @"去关闭";
    UIAlertController *openNotificationSettings = [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actionAgr = [UIAlertAction actionWithTitle:subTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        [self.tableView reloadData];
    }];
    UIAlertAction *actionRef = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self.tableView reloadData];
    }];
    [openNotificationSettings addAction:actionAgr];
    [openNotificationSettings addAction:actionRef];
    [self.navigationController presentViewController:openNotificationSettings animated:YES completion:nil];
}
/**
 退出登录
 */
- (void)loginoutAction {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"确定退出？" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actionQ = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self clearUserData];
    }];
    UIAlertAction *actionC = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:actionC];
    [alert addAction:actionQ];
    [self.navigationController presentViewController:alert animated:YES completion:nil];
}
/**
 清除数据
 */
- (void)clearUserData {
    UserInfoModel *model = [[UserInfoModel alloc] init];
    [[UserData instance] updateUserInfo:model];
    [[UserData instance] setLoginState:NO];
    [[UserAuthData shareInstance] clearUserAuthData];
    [JPUSHService setAlias:nil completion:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
        //                           NSLog(@"%s:\n------%ld\n------%@\n-------%ld", __func__, (long)iResCode, iAlias, (long)seq);
    } seq:0];
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - UItableView
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    __weak typeof(self) weakSelf = self;
    NSDictionary *dict = self.dataSource[indexPath.section][indexPath.row];
    if ([dict[@"title"] isEqualToString:@"支付密码"]) {
        [WSProgressHUD showWithMaskType:WSProgressHUDMaskTypeBlack];
        [[UserAuthData shareInstance] refreshUserInfoResponse:^(UserAuthDataModel *authData, NSString *errMsg) {
            [WSProgressHUD dismiss];
            if ([authData.isAuthentication isEqualToString:@"1"] && authData.bankCard.length > 0) {
                if (authData.hasTradePwd.integerValue <= 0) {//设置密码
                    SetChargePasswordViewController *setVC=[[SetChargePasswordViewController alloc] init];
                    [weakSelf.navigationController pushViewController:setVC animated:YES];
                } else {//更改密码
                    ResetTransPwdViewController *resetPwdVC = [[ResetTransPwdViewController alloc] init];
                    [weakSelf.navigationController pushViewController:resetPwdVC animated:YES];
                }
            } else {//绑定银行卡
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"您尚未绑定银行卡\n是否绑定" message:nil preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *actionQ = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    BindBankCarViewController *bindBankCardVC = [[BindBankCarViewController alloc] init];
                    [weakSelf.navigationController pushViewController:bindBankCardVC animated:YES];
                }];
                UIAlertAction *actionC = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
                [alert addAction:actionC];
                [alert addAction:actionQ];
                [weakSelf.navigationController presentViewController:alert animated:YES completion:nil];
            }
        }];
    } else if ([dict[@"title"] isEqualToString:@"我的银行卡"]) {
        [WSProgressHUD showWithMaskType:WSProgressHUDMaskTypeBlack];
        [[UserAuthData shareInstance] refreshUserInfoResponse:^(UserAuthDataModel *authData, NSString *errMsg) {
            [WSProgressHUD dismiss];
            if ([authData.isAuthentication isEqualToString:@"1"] && authData.bankCard.length > 0) {
                NSString *class = dict[@"viewController"];
                [weakSelf.navigationController pushViewController:[[NSClassFromString(class) alloc] init] animated:YES];
            } else {//绑定银行卡
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"您尚未绑定银行卡\n是否绑定" message:nil preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *actionQ = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    BindBankCarViewController *bindBankCardVC = [[BindBankCarViewController alloc] init];
                    [weakSelf.navigationController pushViewController:bindBankCardVC animated:YES];
                }];
                UIAlertAction *actionC = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
                [alert addAction:actionC];
                [alert addAction:actionQ];
                [weakSelf.navigationController presentViewController:alert animated:YES completion:nil];
            }
        }];
    } else if ([dict[@"title"] isEqualToString:@"实名认证"]) {
        [WSProgressHUD showWithMaskType:WSProgressHUDMaskTypeBlack];
        [[UserAuthData shareInstance] refreshUserInfoResponse:^(UserAuthDataModel *authData, NSString *errMsg) {
            [WSProgressHUD dismiss];
            if ([authData.isAuthentication isEqualToString:@"1"] && authData.bankCard.length > 0) {
                NSString *class = dict[@"viewController"];
                [weakSelf.navigationController pushViewController:[[NSClassFromString(class) alloc] init] animated:YES];
            } else {//绑定银行卡
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"您尚未绑定银行卡\n是否绑定" message:nil preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *actionQ = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    BindBankCarViewController *bindBankCardVC = [[BindBankCarViewController alloc] init];
                    [weakSelf.navigationController pushViewController:bindBankCardVC animated:YES];
                }];
                UIAlertAction *actionC = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
                [alert addAction:actionC];
                [alert addAction:actionQ];
                [weakSelf.navigationController presentViewController:alert animated:YES completion:nil];
            }
        }];
    } else if ([dict[@"title"] isEqualToString:@"清除缓存"]) {
        SDImageCache *cacheManager = [SDImageCache sharedImageCache];
        [cacheManager cleanDiskWithCompletionBlock:^{
            NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSString *cookiesFolderPath = [libraryPath stringByAppendingString:@"/Cookies"];
            NSError *errors;
            [[NSFileManager defaultManager] removeItemAtPath:cookiesFolderPath error:&errors];
            NSURLCache * cache = [NSURLCache sharedURLCache];
            [[SDImageCache sharedImageCache]clearDisk];
            [cache removeAllCachedResponses];
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }];
    } else {
        if ([dict[@"title"] isEqualToString:@"金价提醒"]) {
            
        } else {
            NSString *class = dict[@"viewController"];
            [self.navigationController pushViewController:[[NSClassFromString(class) alloc] init] animated:YES];
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataSource[section] count];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *ID = @"setting";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:ID];
        cell.textLabel.font = FONT_TEXT;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    self.cellSwitch.on = !([[UIApplication sharedApplication] currentUserNotificationSettings].types  == UIUserNotificationTypeNone);

    NSDictionary *dict = self.dataSource[indexPath.section][indexPath.row];
    cell.imageView.image = [UIImage imageNamed:dict[@"image"]];
    if ([dict[@"title"] isEqualToString:@"清除缓存"]) {
        SDImageCache *cacheManager = [SDImageCache sharedImageCache];
        NSInteger size = [cacheManager getSize];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%.2fM",size/1000000.00];
        cell.detailTextLabel.font = FONT_TEXT;
    }
    cell.textLabel.text = dict[@"title"];
    return cell;
}
#pragma mark - 初始化，布局
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [IQKeyboardManager sharedManager].enable = NO;
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = COLOR_TBBACK;
    self.title = @"设置";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notification:) name:@"First" object:nil];

    [self.view addSubview:self.tableView];
    [self.view addSubview:self.logoutButton];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.bottom.equalTo(self.logoutButton.mas_top);
    }];
    [self.logoutButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.height.equalTo(@49);
        if (@available(iOS 11.0, *)) {
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        } else {
            make.bottom.equalTo(self.view);
        }
    }];
}
/**
 懒加载
 
 @return headerView
 */
- (UIView *)headerView {
    if (!_headerView) {
        _headerView = [[UIView alloc] init];
    }
    return _headerView;
}
/**
 懒加载
 
 @return 数据
 */
- (NSArray *)dataSource {
    if (!_dataSource) {
        NSArray *array = @[
                              @[
                                  @{@"title" : @"我的银行卡", @"image" : @"icon_my_bankcard", @"viewController" : @"BankCarkViewController"},
                                  @{@"title" : @"支付密码", @"image" : @"icon_mima", @"viewController" : @""},
                                  @{@"title" : @"实名认证", @"image" : @"icon_safe_protect", @"viewController" : @"IDVerifyViewController"},
                                  @{@"title" : @"清除缓存", @"image" : @"icon_clear", @"viewController" : @""},
//                                  @{@"title" : @"金价提醒", @"image" : @"icon_goldPrice", @"viewController" : @"GoldPriceRemindViewController"}
                                  @{@"title" : @"关于招金猫", @"image" : @"icon_about_us", @"viewController" : @"AboutUsViewController"}

                              ],
                              @[
//                                  @{@"title" : @"关于招金猫", @"image" : @"icon_about_us", @"viewController" : @"AboutUsViewController"}
                                  
                              ]
                          ];
        _dataSource = [[NSArray alloc] initWithArray:array];
    }
    return _dataSource;
}
/**
 懒加载
 
 @return 退出登录按钮
 */
- (UIButton *)logoutButton
{
    if (!_logoutButton) {
        _logoutButton = [[UIButton alloc] init];
        [_logoutButton setTitle:@"退出登录" forState:UIControlStateNormal];
        [_logoutButton setTitleColor:COLOR_MAIN forState:UIControlStateNormal];
        _logoutButton.titleLabel.font = FONT_TITLE;
        _logoutButton.backgroundColor = COLOR_WHITE;
        [_logoutButton addTarget:self action:@selector(loginoutAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _logoutButton;
}
/**
 懒加载
 
 @return 列表
 */
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.dataSource = self;
        _tableView.delegate = self;
    }
    return _tableView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
