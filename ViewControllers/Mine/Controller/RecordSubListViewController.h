//
//  RecordSubListViewController.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/10/26.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecordSubListViewController : UIViewController
/** -1 全部、 0 未到期、 1 已到期 */
@property (nonatomic, strong) NSNumber *isExpire;
/** 列表类型 0：余额变动列表；1：黄金资产交易记录；2：招财金交易列表；3：生财金交易记录；4：其他资产交易记录；5：零钱罐交易记录；6：存钱罐交易记录；7：订单记录； */
@property(nonatomic, strong) NSNumber *listType;

@end
