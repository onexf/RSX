//
//  CashBoxViewController.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/12/7.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "CashBoxViewController.h"
#import "TypeButton.h"
#import "CashBoxSubViewController.h"

@interface CashBoxViewController ()<UIScrollViewDelegate>

/** titleView */
@property(nonatomic, strong) UIView *titlesView;
/** 指示器 */
@property (nonatomic, strong) UIView *indicatorView;
/** 累计收益 */
@property (nonatomic, strong) UIButton *walfareBtn;
/** 昨日 */
@property (nonatomic, strong) UIButton *activityBtn;
/** 选中的按钮 */
@property (nonatomic, weak) UIButton *selectedButton;
/** 滚动 */
@property (nonatomic, strong) UIScrollView *contentView;
/** line */
@property(nonatomic, strong) UIView *line;
/** <#name#> */
@property(nonatomic, strong) UIButton *bottomBtn;

@end


@implementation CashBoxViewController
#pragma mark - 事件处理
- (void)bottomBtnDidTap:(UIButton *)button {
    self.tabBarController.selectedIndex = 2;
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    // 当前的索引
    NSInteger index = scrollView.contentOffset.x / scrollView.ct_width;
    // 取出子控制器
    UIViewController *vc = self.childViewControllers[index];
    vc.view.ct_x = scrollView.contentOffset.x;
    vc.view.ct_y = 0; // 设置控制器view的y值为0(默认是20)
    vc.view.ct_height = scrollView.ct_height; // 设置控制器view的height值为整个屏幕的高度(默认是比屏幕高度少个20)
    [scrollView addSubview:vc.view];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self scrollViewDidEndScrollingAnimation:scrollView];
    // 点击按钮
    NSInteger index = scrollView.contentOffset.x / scrollView.ct_width;
    if (index == 0) {
        [self titleClick:self.activityBtn];
    }
    if (index == 1) {
        [self titleClick:self.walfareBtn];
    }
}

- (void)titleClick:(UIButton *)button {
    // 修改按钮状态
    self.selectedButton.enabled = YES;
    button.enabled = NO;
    self.selectedButton = button;
    
    [self.bottomBtn setTitle:[button.titleLabel.text isEqualToString:@"我的存金"] ? @"我要存金" : @"我要提金" forState:UIControlStateNormal];

    // 动画
    [UIView animateWithDuration:0.25 animations:^{
        self.indicatorView.ct_centerX = button.ct_centerX;
    }];
    // 滚动
    CGPoint offset = self.contentView.contentOffset;
    if ([button.titleLabel.text isEqualToString:@"我的存金"]) {
        offset.x = 0;
    }
    if ([button.titleLabel.text isEqualToString:@"我的提金"]) {
        offset.x = self.contentView.ct_width;
    }
    [self.contentView setContentOffset:offset animated:YES];
}
#pragma mark - 初始化，布局
- (void)layOut {
    [self.titlesView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@49);
    }];
    [self.bottomBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.height.equalTo(@49);
    }];
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.titlesView);
        make.height.equalTo(@29);
        make.width.equalTo(@1);
    }];
    CGFloat btnWidth = SCREEN_WIDTH / 2.0 - 0.5;
    [self.activityBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.titlesView);
        make.left.equalTo(self.titlesView);
        make.width.equalTo(@(btnWidth));
    }];
    [self.walfareBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.activityBtn.mas_right);
        make.top.bottom.width.equalTo(self.activityBtn);
    }];
    [self.indicatorView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@1);
        make.bottom.equalTo(self.titlesView).offset(1);
        make.left.right.equalTo(self.activityBtn);
    }];
    
    [self.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titlesView.mas_bottom);
        make.left.width.equalTo(self.view);
        make.bottom.equalTo(self.bottomBtn.mas_top);
    }];
}
- (void)setChildViewControllers {
    CashBoxSubViewController *todayIncom = [[CashBoxSubViewController alloc] init];
    todayIncom.type = @"0";
    [self addChildViewController:todayIncom];
    
    CashBoxSubViewController *totalIncom = [[CashBoxSubViewController alloc] init];
    totalIncom.type = @"1";
    [self addChildViewController:totalIncom];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self.view insertSubview:self.contentView atIndex:0];
    // 添加第一个控制器的view
    [self scrollViewDidEndScrollingAnimation:self.contentView];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的金库";
    
    self.view.backgroundColor = COLOR_TBBACK;
    
    [self setChildViewControllers];
    
    [self.view addSubview:self.titlesView];
    [self.view addSubview:self.bottomBtn];
    [self layOut];
    
    [self titleClick:self.activityBtn];
}

- (UIScrollView *)contentView
{
    if (!_contentView) {
        _contentView = [[UIScrollView alloc] init];
        _contentView.delegate = self;
        _contentView.pagingEnabled = YES;
        _contentView.contentSize = CGSizeMake(SCREEN_WIDTH * 2, 0);
        _contentView.showsHorizontalScrollIndicator = NO;
    }
    return _contentView;
}

- (UIView *)indicatorView {
    if (!_indicatorView) {
        _indicatorView = [[UIView alloc] init];
        _indicatorView.backgroundColor = COLOR_Red;
    }
    return _indicatorView;
}
- (UIButton *)walfareBtn {
    if (!_walfareBtn) {
        _walfareBtn = [[UIButton alloc] init];
        [_walfareBtn setTitle:@"我的提金" forState:UIControlStateNormal];
        [_walfareBtn setTitleColor:COLOR_SUBTEXT forState:UIControlStateNormal];
        [_walfareBtn setTitleColor:COLOR_Red forState:UIControlStateDisabled];
        _walfareBtn.titleLabel.font = FONT_TITLE;
        [_walfareBtn addTarget:self action:@selector(titleClick:) forControlEvents:UIControlEventTouchUpInside];
        _walfareBtn.jk_touchAreaInsets = UIEdgeInsetsMake(0, 0, 20, 0);
    }
    return _walfareBtn;
}

- (UIButton *)activityBtn {
    if (!_activityBtn) {
        _activityBtn = [[UIButton alloc] init];
        [_activityBtn setTitle:@"我的存金" forState:UIControlStateNormal];
        [_activityBtn setTitleColor:COLOR_SUBTEXT forState:UIControlStateNormal];
        [_activityBtn setTitleColor:COLOR_Red forState:UIControlStateDisabled];
        _activityBtn.titleLabel.font = FONT_TITLE;
        [_activityBtn addTarget:self action:@selector(titleClick:) forControlEvents:UIControlEventTouchUpInside];
        _activityBtn.jk_touchAreaInsets = UIEdgeInsetsMake(0, 0, 20, 0);
    }
    return _activityBtn;
}
/**
 懒加载
 
 @return line
 */
- (UIView *)line {
    if (!_line) {
        _line = [[UIView alloc] init];
        _line.backgroundColor = COLOR_SUBTEXT;
    }
    return _line;
}
- (UIView *)titlesView {
    if (!_titlesView) {
        _titlesView = [[UIView alloc] init];
        _titlesView.backgroundColor = COLOR_WHITE;
        [_titlesView addSubview:self.walfareBtn];
        [_titlesView addSubview:self.activityBtn];
        [_titlesView addSubview:self.indicatorView];
        [_titlesView addSubview:self.line];
    }
    return _titlesView;
}
/**
 懒加载
 
 @return bottomBtn
 */
- (UIButton *)bottomBtn {
    if (!_bottomBtn) {
        _bottomBtn = [[UIButton alloc] init];
        _bottomBtn.backgroundColor = COLOR_MAIN;
        [_bottomBtn setTitle:@"我要存金" forState:UIControlStateNormal];
        _bottomBtn.titleLabel.font = FONT_BUTTON_TEXT;
        [_bottomBtn addTarget:self action:@selector(bottomBtnDidTap:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _bottomBtn;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end


