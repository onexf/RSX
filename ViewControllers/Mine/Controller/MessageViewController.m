//
//  MessageViewController.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/8/29.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "MessageViewController.h"
#import "NoticeListCell.h"
#import "MineNetWork.h"
#import "CBWebViewController.h"
#import "MJRefresh.h"
@interface MessageViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSInteger pageNum;
    NSInteger pageCount;
    NSMutableArray *dataList;
    NSInteger totalPageNum;
}

@property(nonatomic,strong)UITableView *table;


@end

@implementation MessageViewController
-(void)requestData
{
    if (pageNum>1)
    {
        if (pageNum>totalPageNum)
        {
            [self.table.mj_footer endRefreshingWithNoMoreData];
            return;
        }
    }
    __weak typeof(self) weakSelf = self;
    
    NSDictionary *dic=@{@"id":@"0",@"pageIndex":[NSString stringWithFormat:@"%ld",(long)pageNum],@"pageSize":[NSString stringWithFormat:@"%ld",pageCount]};
    
    [MineNetWork noticeListWithDataDic:dic successBlock:^(id responseBody) {
        
        
        if (![responseBody isKindOfClass:[NSDictionary class]]) {
            
            return;
        }
        
        NSArray *arr=[NoticeListModel mj_objectArrayWithKeyValuesArray:responseBody[@"items"]];
        
        totalPageNum=[responseBody[@"totalPages"] integerValue];
        
        if (pageNum<=1)
        {
            
            [dataList removeAllObjects];
            
            if (arr.count>0)
            {
                [dataList addObjectsFromArray:arr];
            }
            
            if (totalPageNum<=1)
            {
                [weakSelf.table reloadData];
                [weakSelf.table.mj_header endRefreshing];
                [weakSelf.table.mj_footer endRefreshingWithNoMoreData];
                return;
                
            }
            
        }
        else
        {
            
            [dataList addObjectsFromArray:arr];
            
            if (arr.count<pageCount)
            {
                [weakSelf.table reloadData];
                [weakSelf.table.mj_header endRefreshing];
                [weakSelf.table.mj_footer endRefreshingWithNoMoreData];
                
                return;
                
            }
            
        }
        
        [weakSelf.table reloadData];
        
        
        [weakSelf.table.mj_header endRefreshing];
        [weakSelf.table.mj_footer endRefreshing];
        

        
    } failureBlock:^(NSString *error) {
        
        [weakSelf.table.mj_header endRefreshing];
        [weakSelf.table.mj_footer endRefreshing];
        
    }];
    

}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = COLOR_TBBACK;
    self.title = @"系统公告";
    
    dataList=[NSMutableArray array];
    pageCount=10;
    pageNum=1;
    totalPageNum=1;
    
    [self.view addSubview:self.table];
    [self setUpTableView];
    
}

-(UITableView *)table
{
    if (!_table)
    {
        _table=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-NavBarHeight) style:UITableViewStylePlain];
        _table.delegate=self;
        _table.dataSource=self;
        _table.tableFooterView=[UIView new];
        
    }
    
    return _table;
}


-(void)setUpTableView{
    
    self.table.mj_header = [MJDIYHeader headerWithRefreshingBlock:^{
        pageNum=1;
        [self requestData];
    }];
    self.table.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        pageNum++;
        
        [self requestData];
        
    }];
    //第一次请求数据
    [self requestData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataList.count;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row>=dataList.count)
    {
        return nil;
    }
    
    static NSString *cellId=@"cellId";
    NoticeListCell *cell=[tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (!cell)
    {
        cell=[[NoticeListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    NoticeListModel *model=[dataList objectAtIndex:indexPath.row];
    
    [cell fillCellWithModel:model];
    
    return cell;
    
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NoticeListModel *model=[dataList objectAtIndex:indexPath.row];
    
    CBWebViewController *vc=[[CBWebViewController alloc] init];
    vc.urlString =model.href;
    vc.showShareButton = NO;
    [self.navigationController pushViewController:vc animated:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
