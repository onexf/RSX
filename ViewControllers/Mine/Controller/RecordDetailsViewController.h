//
//  RecordDetailsViewController.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/10/27.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>
@class GlodTradeModel;
//交易记录类型（0：投资；1：买金；2：卖金；3：赎回；4：充值；5：提现；6：到期；7：其他） ,
typedef NS_ENUM(NSInteger, RecordLogType) {
    RecordLogTypeTouZi = 0,//投资
    RecordLogTypeGoldIn,//买金
    RecordLogTypeGoldOut,//卖金
    RecordLogTypeShuHui,//赎回
    RecordLogTypeChongZhi,//充值
    RecordLogTypeTiXian,//提现
    RecordLogTypeDaoQi,//到期
    RecordLogTypeOther//其他
};

@interface RecordDetailsViewController : UIViewController

/** 列表传进来的数据 */
@property(nonatomic, strong, readonly) GlodTradeModel *model;
/** 交易类型 */
@property(nonatomic, assign, readonly) RecordLogType logType;
/**
 初始化订单详情

 @param model 订单列表数据
 @return 订单详情
 */
- (instancetype)initWithData:(GlodTradeModel *)model;

@end
