//
//  NetworkSingleton+Home.h
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/8/23.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "NetworkSingleton.h"
@class BannerModel, HomePageMolde;

@interface NetworkSingleton (Home)


/**
 获取首页轮播图

 @param response 接口返回结果
 */
+ (void)home_queryBannersResponse:(void(^)(NSArray<BannerModel *> *list, NSString *errMsg))response;
/**
 获取实时金价

 @param response 金价，累计管理的黄金克数
 */
+ (void)home_allListRefreshGoldPriceWithResponse:(void(^)(NSString  *goldPrice, NSString *total, NSString *errMsg))response;

/**
 热卖所有数据

 @param response dict
 */
+ (void)home_allDataResponse:(void(^)(HomePageMolde *homePageData, NSString *errMsg))response;

/**
 获取活动信息

 @param response BannerModel
 */
+ (void)home_queryActivityImageResponse:(void(^)(BannerModel *activity, NSString *errMsg))response;

/**
 获取启动页
 
 @param response BannerModel
 */
+ (void)launch_queryLaunchImageResponse:(void(^)(BannerModel *activity, NSString *errMsg))response;
@end
