//
//  NetworkSingleton+Home.m
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/8/23.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "NetworkSingleton+Home.h"
#import "BannerModel.h"
#import "MJExtension.h"
#import "HomePageMolde.h"
#import "AllListModel.h"
#import "HomeNoticeModel.h"
#import "GoldMarketModel.h"
//接口拼接
#define UrlBannersList                    URLString(@"/api/banners/List")
#define UrlGetReatTimeGoldPrice           URLString(@"/api/home/GetReatTimeGoldPrice")
#define UrlHomeIndex                      URLString(@"/api/home/index")

@implementation NetworkSingleton (Home)

+ (void)launch_queryLaunchImageResponse:(void(^)(BannerModel *activity, NSString *errMsg))response {
    NSDictionary *dict = @{@"category" : @"3"};
    [[NetworkSingleton sharedManager] postResultWithParameter:dict url:UrlBannersList successBlock:^(id responseBody) {
        NSArray <BannerModel *>*result = [BannerModel mj_objectArrayWithKeyValuesArray:responseBody];
        if (result.count > 0) {
            response([result firstObject], nil);
        } else {
            response(nil, @"查询成功无数据");
        }
    } failureBlock:^(NSString *error) {
        response(nil, error);
    }];
}
+ (void)home_queryActivityImageResponse:(void(^)(BannerModel *activity, NSString *errMsg))response {
    NSDictionary *dict = @{@"category" : @"4"};
    [[NetworkSingleton sharedManager] postResultWithParameter:dict url:UrlBannersList successBlock:^(id responseBody) {
        NSArray <BannerModel *>*result = [BannerModel mj_objectArrayWithKeyValuesArray:responseBody];
        if (result.count > 0) {
            response([result firstObject], nil);
        } else {
            response(nil, @"查询成功无数据");
        }
    } failureBlock:^(NSString *error) {
        response(nil, error);
    }];
}
+ (void)home_queryBannersResponse:(void(^)(NSArray<BannerModel *> *list, NSString *errMsg))response {
    NSDictionary *dict = @{@"category" : @"1"};
    [[NetworkSingleton sharedManager] postResultWithParameter:dict url:UrlBannersList successBlock:^(id responseBody) {
        NSArray <BannerModel *>*result = [BannerModel mj_objectArrayWithKeyValuesArray:responseBody];
        response(result, nil);
    } failureBlock:^(NSString *error) {
        response(nil, error);
    }];
}

+ (void)home_allListRefreshGoldPriceWithResponse:(void(^)(NSString  *goldPrice, NSString *total, NSString *errMsg))response {
    [[NetworkSingleton sharedManager] postResultWithParameter:nil url:UrlGetReatTimeGoldPrice successBlock:^(id responseBody) {
        NSString *goldPrice = responseBody[@"realTimeGoldPrice"];
        NSString *total = responseBody[@"goldTotal"][@"total"];
        UDSave(goldPrice, @"currentGoldPrice");
        response(goldPrice, total, nil);
    } failureBlock:^(NSString *error) {
        response(nil, nil, error);
    }];
}

+ (void)home_allDataResponse:(void(^)(HomePageMolde *homePageData, NSString *errMsg))response {
    [[NetworkSingleton sharedManager] postResultWithParameter:nil url:UrlHomeIndex successBlock:^(id responseBody) {
        HomePageMolde *homePageData = [[HomePageMolde alloc] init];
        NSArray <BannerModel *>*result = [BannerModel mj_objectArrayWithKeyValuesArray:responseBody[@"banners"]];
        homePageData.banners = result;
        homePageData.isNew = responseBody[@"isNew"];
        NSArray <AllListModel *>*productListTiro = [AllListModel mj_objectArrayWithKeyValuesArray:responseBody[@"newProductList"]];
        homePageData.productListTiro = productListTiro;
        NSArray <AllListModel *>*productList = [AllListModel mj_objectArrayWithKeyValuesArray:responseBody[@"productList"]];
        homePageData.productList = productList;
        HomeNoticeModel *noticeModel = [HomeNoticeModel mj_objectWithKeyValues:responseBody[@"notice"]];
        homePageData.noticeModel = noticeModel;
        
        GoldMarketModel *shopModel = [GoldMarketModel mj_objectWithKeyValues:responseBody[@"shop"]];
        homePageData.shopModel = shopModel;
        
        response(homePageData, nil);
    } failureBlock:^(NSString *error) {
        response(nil, error);
    }];
}

@end
