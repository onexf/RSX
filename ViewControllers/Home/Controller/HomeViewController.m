//
//  HomeViewController.m
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/8/21.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "HomeViewController.h"
#import <FDFullscreenPopGesture/UINavigationController+FDFullscreenPopGesture.h>
#import "SDCycleScrollView.h"
#import "MJRefresh.h"
#import "NetworkSingleton+Home.h"
#import "CBWebViewController.h"
#import "HomeItemsView.h"
#import "SDMessageView.h"
#import "HomeGoldPriceView.h"
#import "HomeTiroView.h"
#import "HomeGoodsView.h"
#import "NewHomeGoodsView.h"
#import "UIView+Extension.h"
#import "HomePageMolde.h"
#import "BannerModel.h"
#import "AllListModel.h"
#import "HomeNoticeModel.h"

#import "TestViewController.h"
#import "JX_GCDTimerManager.h"
#import "JPUSHService.h"
#import "HomeShopView.h"
#import "BindBankCarViewController.h"
#import "SellGoldViewController.h"
#import "UserAuthData.h"


static NSString * const myTimer = @"MyTimer";

@interface HomeViewController ()<SDCycleScrollViewDelegate, HomeItemActionDelegate>

/** 背景scrollowView */
@property(nonatomic, strong) UIScrollView *scrollViewView;
/** 轮播图 */
@property(nonatomic, strong) SDCycleScrollView *sdcycleScrollView;
/** 轮播接口数据 */
@property (nonatomic, strong) NSArray *banberData;
/** 入口按钮 */
@property(nonatomic, strong) HomeItemsView *homeItemsView;
/** 金价 */
@property(nonatomic, strong) HomeGoldPriceView *goldPriceView;
/** 新手特权 */
@property(nonatomic, strong) HomeTiroView *tiroView;
/** 特价黄金 */
@property(nonatomic, strong) NewHomeGoodsView *specGoodsView;
/** 明星产品 */
@property(nonatomic, strong) HomeShopView *starGoodsView;


/** bottomViewImage */
@property(nonatomic, strong) UIView *booomViewImage;
/** 图片 */
@property(nonatomic, strong) UIImageView *activityImageView;
/** 数据 */
@property(nonatomic, strong) HomePageMolde *homePageData;
/** bottomLabel */
@property(nonatomic, strong) UILabel *bottomLabel;

@end

@implementation HomeViewController

#pragma mark - 事件处理

- (void)aboutUsImageViewDidTap {
    CBWebViewController *webViewController = [[CBWebViewController alloc] init];
    webViewController.urlString = @"https://www.zhaojinmao.cn/testwebapp/find/onemin/onmin.html";
    webViewController.showShareButton = YES;
    [self.navigationController pushViewController:webViewController animated:YES];
}

- (void)activityImageViewDidTap {
    CBWebViewController *webViewController = [[CBWebViewController alloc] init];
    webViewController.urlString = @"https://www.zhaojinmao.cn/testwebapp/find/activity/activity.html";
    webViewController.showShareButton = YES;
    [self.navigationController pushViewController:webViewController animated:YES];
}
/**
 商城 挖矿 邀请

 @param string 点击的按钮
 */
- (void)actionWithItemSting:(NSString *)string {
    if ([string isEqualToString:@"买入黄金"]) {
        self.tabBarController.selectedIndex = 1;
    }
    if ([string isEqualToString:@"卖出黄金"]) {
        if (isLogin()) {
            
            [WSProgressHUD showWithMaskType:WSProgressHUDMaskTypeBlack];
            __weak typeof(self) weakSelf = self;
            [[UserAuthData shareInstance] refreshUserInfoResponse:^(UserAuthDataModel *userAuthData, NSString *errMsg) {
                [WSProgressHUD dismiss];
                if ([userAuthData.isAuthentication boolValue]>0) {
                    SellGoldViewController *vc = [[SellGoldViewController alloc] init];
                    [weakSelf.navigationController pushViewController:vc animated:YES];
                } else {
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"您尚未绑定银行卡\n是否绑定" message:nil preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *actionQ = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        BindBankCarViewController *vc=[[BindBankCarViewController alloc]init];
                        [weakSelf.navigationController pushViewController:vc animated:YES];
                    }];
                    UIAlertAction *actionC = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
                    [alert addAction:actionC];
                    [alert addAction:actionQ];
                    [weakSelf.navigationController presentViewController:alert animated:YES completion:nil];
                }
            }];
        } else {
            [LoginTool loginAction];
        }
    }
    if ([string isEqualToString:@"存金生息"]) {
        self.tabBarController.selectedIndex = 2;
    }
    if ([string isEqualToString:@"实物提金"]) {
        self.tabBarController.selectedIndex = 2;
    }
}
/**
 轮播图delegate

 @param cycleScrollView 轮播
 @param index 下标
 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    //取出链接
    BannerModel *model = self.banberData[index];
    CBWebViewController *webViewController = [[CBWebViewController alloc] init];
    webViewController.urlString = model.href;
    webViewController.showShareButton = YES;
    [self.navigationController pushViewController:webViewController animated:YES];
}

- (void)reloadAllData {
    [self.scrollViewView.mj_header beginRefreshing];
}

#pragma mark - 数据请求
- (void)requestBannersList {
    __weak typeof(self) weakSelf = self;    
    [NetworkSingleton home_allDataResponse:^(HomePageMolde *homePageData, NSString *errMsg) {
        [weakSelf.scrollViewView.mj_header endRefreshing];
        if (errMsg) {
            [weakSelf showErrorView];
        } else {
            [weakSelf hideErrorView];
            weakSelf.homePageData = homePageData;
            weakSelf.banberData = homePageData.banners;
            if (homePageData.banners.count > 0) {
                NSMutableArray *imagesURLStrings = [NSMutableArray array];
                for (BannerModel *model in homePageData.banners) {
                    [imagesURLStrings addObject:model.picUrl];
                }
                weakSelf.sdcycleScrollView.imageURLStringsGroup = imagesURLStrings;
            }
            if (homePageData.productList.count > 0) {
                weakSelf.specGoodsView.goodsData = homePageData.productList[0];
            }
//            weakSelf.starGoodsView.goodsData = homePageData.productList[1];
            weakSelf.tiroView.productListTiro = homePageData.productListTiro;
            
            weakSelf.starGoodsView.model = homePageData.shopModel;
            [weakSelf layout];
        }
    }];
}

- (void)getGoldPrice {
    __weak typeof(self) weakSelf = self;
    [NetworkSingleton home_allListRefreshGoldPriceWithResponse:^(NSString *goldPrice, NSString *total, NSString *errMsg) {
        if (!errMsg) {
            weakSelf.goldPriceView.currentGoldPrice = goldPrice;
        }
    }];
}
#pragma mark - 初始化，布局
- (void)layout {
    [self.scrollViewView mas_remakeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0, *)){
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
        } else {
            make.top.equalTo(self.view);
        }
        make.left.bottom.right.equalTo(self.view);
    }];
    [self.homeItemsView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(self.sdcycleScrollView.mas_bottom);
        make.height.equalTo(@85);
    }];
    [self.goldPriceView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.homeItemsView.mas_bottom).offset(10);
        make.left.right.equalTo(self.homeItemsView);
        make.height.equalTo(@215);
    }];

    if (self.homePageData && self.homePageData.isNew.integerValue == 1) {//非新手
        self.tiroView.hidden = YES;
        [self.specGoodsView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.goldPriceView.mas_bottom).offset(10);
            make.left.right.equalTo(self.goldPriceView);
        }];
    } else {
        self.tiroView.hidden = NO;
        [self.tiroView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.goldPriceView.mas_bottom).offset(10);
            make.left.right.equalTo(self.goldPriceView);
        }];
        [self.specGoodsView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.tiroView.mas_bottom).offset(10);
            make.left.right.equalTo(self.goldPriceView);
        }];
    }
    
    [self.starGoodsView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.specGoodsView.mas_bottom).offset(10);
        make.left.right.height.equalTo(self.specGoodsView);
    }];
    [self.booomViewImage mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.starGoodsView.mas_bottom).offset(10);
        make.left.right.equalTo(self.starGoodsView);
    }];
    [self.activityImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(self.booomViewImage).offset(16);
        make.bottom.right.equalTo(self.booomViewImage).offset(-16);
        make.height.equalTo(@((SCREEN_WIDTH - 32) * 216 / 686));
    }];
    [self.bottomLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.booomViewImage.mas_bottom).offset(30);
        make.left.right.equalTo(self.booomViewImage);
    }];
    [self.view layoutIfNeeded];
    self.scrollViewView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetMaxY(self.bottomLabel.frame) + 30);
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[JX_GCDTimerManager sharedInstance] cancelTimerWithName:myTimer];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    __weak typeof(self) weakSelf = self;
    [self getGoldPrice];
    [[JX_GCDTimerManager sharedInstance] scheduledDispatchTimerWithName:myTimer
                                                           timeInterval:7.0
                                                                  queue:nil
                                                                repeats:YES
                                                           actionOption:AbandonPreviousAction
                                                                 action:^{
                                                                     [weakSelf getGoldPrice];
                                                                 }];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.sdcycleScrollView adjustWhenControllerViewWillAppera];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"首页";
    //隐藏导航栏
    self.fd_prefersNavigationBarHidden = YES;
    self.view.backgroundColor = COLOR_WHITE;
    //add控件
    [self.view addSubview:self.scrollViewView];
    UIView *statusBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 20)];
    statusBar.backgroundColor = COLOR_WHITE;
    [self.view addSubview:statusBar];

    [self.scrollViewView.mj_header beginRefreshing];
    [self layout];
    if (isLogin()) {
        [self performSelector:@selector(setJpushTag) withObject:nil afterDelay:2.0];
    }
}
- (void)setJpushTag {
    NSString *userID = [[UserData instance] getUserInfo].userId;
    [JPUSHService setAlias:userID completion:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
//        NSLog(@"%s:\n------%ld\n------%@\n-------%ld", __func__, (long)iResCode, iAlias, (long)seq);
    } seq:0];
}
/**
 懒加载
 
 @return bottomLabel
 */
- (UILabel *)bottomLabel {
    if (!_bottomLabel) {
        _bottomLabel = [[UILabel alloc] init];
        _bottomLabel.textColor = COLOR_SUBTEXT;
        _bottomLabel.font = FONT_TEXT;
        _bottomLabel.text = @"市场有风险    投资需谨慎";
        _bottomLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _bottomLabel;
}
/**
 懒加载
 
 @return 明星
 */
- (HomeShopView *)starGoodsView {
    if (!_starGoodsView) {
        _starGoodsView = [[HomeShopView alloc] init];
    }
    return _starGoodsView;
}
/**
 懒加载
 
 @return 特价
 */
- (NewHomeGoodsView *)specGoodsView {
    if (!_specGoodsView) {
        _specGoodsView = [[NewHomeGoodsView alloc] init];
    }
    return _specGoodsView;
}
/**
 懒加载
 
 @return 新手特权
 */
- (HomeTiroView *)tiroView {
    if (!_tiroView) {
        _tiroView = [[HomeTiroView alloc] init];
    }
    return _tiroView;
}
/**
 懒加载
 
 @return 金价
 */
- (HomeGoldPriceView *)goldPriceView {
    if (!_goldPriceView) {
        _goldPriceView = [[HomeGoldPriceView alloc] init];
    }
    return _goldPriceView;
}
- (NSArray *)banberData {
    if (!_banberData) {
        _banberData = [[NSArray alloc] init];
    }
    return _banberData;
}

/**
 懒加载
 
 @return 商城等入口
 */
- (HomeItemsView *)homeItemsView {
    if (!_homeItemsView) {
        _homeItemsView = [[HomeItemsView alloc] init];
        _homeItemsView.actionDelegate = self;
    }
    return _homeItemsView;
}
/**
 懒加载
 
 @return 背景
 */
- (UIScrollView *)scrollViewView {
    if (!_scrollViewView) {
        _scrollViewView = [[UIScrollView alloc] init];
        _scrollViewView.backgroundColor = COLOR_TBBACK;
        __weak typeof(self) weakSelf = self;
        _scrollViewView.mj_header.automaticallyChangeAlpha = YES;
        _scrollViewView.mj_header = [MJDIYHeader headerWithRefreshingBlock:^{
            [weakSelf requestBannersList];
        }];
        if (@available(iOS 11.0, *)){
        } else {
            _scrollViewView.mj_header.ignoredScrollViewContentInsetTop = 20;
        }
        [_scrollViewView addSubview:self.sdcycleScrollView];
        [_scrollViewView addSubview:self.homeItemsView];
        [_scrollViewView addSubview:self.goldPriceView];
        [_scrollViewView addSubview:self.tiroView];
        [_scrollViewView addSubview:self.specGoodsView];
        [_scrollViewView addSubview:self.starGoodsView];
        [_scrollViewView addSubview:self.booomViewImage];
        [_scrollViewView addSubview:self.bottomLabel];
    }
    return _scrollViewView;
}
/**
 懒加载
 
 @return 新底部
 */
- (UIView *)booomViewImage {
    if (!_booomViewImage) {
        _booomViewImage = [[UIView alloc] init];
        _booomViewImage.backgroundColor = COLOR_WHITE;
        [_booomViewImage addSubview:self.activityImageView];
    }
    return _booomViewImage;
}
/**
 懒加载
 
 @return 热门活动
 */
- (UIImageView *)activityImageView {
    if (!_activityImageView) {
        _activityImageView = [[UIImageView alloc] init];
        _activityImageView.image = [UIImage imageNamed:@"home_activity"];
        [_activityImageView addTapAction:@selector(activityImageViewDidTap) target:self];
    }
    return _activityImageView;
}
/**
 懒加载

 @return 轮播
 */
- (SDCycleScrollView *)sdcycleScrollView
{
    if (!_sdcycleScrollView) {
        _sdcycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_WIDTH * 320.00 / 750.00) delegate:self placeholderImage:[UIImage imageNamed:@"activity_placehoder"]];
        _sdcycleScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
        _sdcycleScrollView.currentPageDotColor = COLOR_WHITE; // 自定义分页控件小圆标颜色
        _sdcycleScrollView.pageDotColor = RGBA(232, 232, 232, 0.7);
        _sdcycleScrollView.pageControlDotSize = CGSizeMake(5, 5);
        _sdcycleScrollView.autoScrollTimeInterval = 3;
    }
    return _sdcycleScrollView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
