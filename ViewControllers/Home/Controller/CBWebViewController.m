//
//  CBWebViewController.m
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/8/24.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "CBWebViewController.h"
#import <WebKit/WebKit.h>
#import "ONEShareTool.h"
#import <FDFullscreenPopGesture/UINavigationController+FDFullscreenPopGesture.h>
#import "AllNetWorkRquest.h"
#import "ApplyBuyOrderViewController.h"
#import "FlowCashViewController.h"
#import "SaveCashViewController.h"
#import "GoldUpAndDownViewController.h"
#import "SavePotViewController.h"
#import "GoBuyTool.h"
#import "BindBankCarViewController.h"
#import "ProductDetailWebViewVC.h"
#import "AllViewController.h"
#import "SubAllListViewController.h"
#import "FindViewController.h"
#import "MineViewController.h"
#import "CouponsViewController.h"
#import "ProductDetailModel.h"
#import "HomeViewController.h"

static void *WkwebBrowserContext = &WkwebBrowserContext;

@interface CBWebViewController ()<WKNavigationDelegate, WKUIDelegate, WKScriptMessageHandler, LoginToolDelegate>

/** webView */
@property(nonatomic, strong) WKWebView *webView;
/** 进度条 */
@property (nonatomic,strong) UIProgressView *progressView;
/** 分享按钮 */
@property(nonatomic, strong) UIButton *shareButton;
/** loginTool */
@property(nonatomic, strong) LoginTool *loginTool;
/** 原始链接 */
@property(nonatomic, copy) NSString *originalUrlSting;
/** 强制刷新 */
@property(nonatomic, strong) WKNavigation *backNavigation;

@property(nonatomic,strong)ProductDetailModel *detailModel;

/** 分享内容 */
@property(nonatomic, strong) NSDictionary *shareDict;

/** lastURLString */
@property(nonatomic, copy) NSString *lastUrlString;


@end

@implementation CBWebViewController
#pragma mark - 事件处理
- (void)successLogin {
    if (isLogin()) {
        CBWebViewController *newSelf = [[CBWebViewController alloc] init];
        newSelf.urlString = self.urlString;
        newSelf.productID = self.productID;
        newSelf.isDisplayNativeBtn = self.isDisplayNativeBtn;
        [self.navigationController pushViewController:newSelf animated:NO];
        NSArray *arr = self.navigationController.viewControllers;
        NSMutableArray *newArr = [[NSMutableArray alloc] initWithArray:arr];
        [newArr removeObjectAtIndex:arr.count - 2];
        [self.navigationController setViewControllers:newArr animated:NO];
    } else {
        [self.webView reload];
    }    
}
- (void)callAction {
    NSMutableString *str=[[NSMutableString alloc] initWithFormat:@"tel:%@", @"4008-017-862"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}

- (void)shareButtonDidClick {
    if ([self.shareDict isKindOfClass:[NSDictionary class]]) {
        [[ONEShareTool sharedInstance] showShareViewWithShareDict:self.shareDict];
    }
}
- (void)shareButtonDidTap {
    if ([self.shareDict isKindOfClass:[NSDictionary class]]) {
        if ([self.shareDict[@"shouldAlert"] integerValue] == 1) {
            [[ONEShareTool sharedInstance] showShareViewWithShareDict:self.shareDict];
        }
    }
}

- (void)back {
    if (self.webView.canGoBack) {
        self.backNavigation = [self.webView goBack];
        [self.webView goBack];
    } else {
        //删除所有的回调事件
        [self.webView.configuration.userContentController removeScriptMessageHandlerForName:@"loginAction"];
        [self.webView.configuration.userContentController removeScriptMessageHandlerForName:@"shareAction"];
        [self.webView.configuration.userContentController removeScriptMessageHandlerForName:@"newShareAction"];
        [self.webView.configuration.userContentController removeScriptMessageHandlerForName:@"prodectAction"];
        [self.webView.configuration.userContentController removeScriptMessageHandlerForName:@"callUs"];
        [self.webView.configuration.userContentController removeScriptMessageHandlerForName:@"authAction"];
        [self.webView.configuration.userContentController removeScriptMessageHandlerForName:@"popToRoot"];
        [self.webView.configuration.userContentController removeScriptMessageHandlerForName:@"prodectDetailAction"];
        [self.webView.configuration.userContentController removeScriptMessageHandlerForName:@"allProductListAction"];
        [self.webView.configuration.userContentController removeScriptMessageHandlerForName:@"subProductListAction"];
        [self.webView.configuration.userContentController removeScriptMessageHandlerForName:@"discoverHomeAction"];
        [self.webView.configuration.userContentController removeScriptMessageHandlerForName:@"mineHomeAction"];
        [self.webView.configuration.userContentController removeScriptMessageHandlerForName:@"couponCenterAction"];
        [self.webView.configuration.userContentController removeScriptMessageHandlerForName:@"getToken"];

        [self.navigationController popViewControllerAnimated:YES];
    }
}
/**
 *  页面开始加载时调用
 *
 *  @param webView    实现该代理的webview
 *  @param navigation 当前navigation
 */
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    NSString *urlString =  webView.URL.absoluteString;
    if ([self.urlString isEqualToString:urlString]) {
        
    } else {
        if ([urlString jk_containsaString:@"accesstoken"]) {
    
        } else {
            NSString *token = @"";
            if (isLogin()) {
                token = [[UserData instance] getUserInfo].token;
                if ([urlString jk_containsaString:@"?"]) {
                    urlString = [urlString stringByAppendingString:[NSString stringWithFormat:@"&accesstoken=%@", token]];
                } else {
                    urlString = [urlString stringByAppendingString:[NSString stringWithFormat:@"?accesstoken=%@", token]];
                }
            } else {
                if ([urlString jk_containsaString:@"?"]) {
                    urlString = [urlString stringByAppendingString:[NSString stringWithFormat:@"&accesstoken=%@", token]];
                } else {
                    urlString = [urlString stringByAppendingString:[NSString stringWithFormat:@"?accesstoken=%@", token]];
                }
            }
            //        NSLog(@"修改后%@", urlString);
            NSURL *url = [NSURL URLWithString:urlString];
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            [webView loadRequest:request];
        }
    }
}
//在发送请求之前，决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    //如果是跳转一个新页面
    if (navigationAction.targetFrame == nil) {
        [webView loadRequest:navigationAction.request];
    }
    decisionHandler(WKNavigationActionPolicyAllow);
}

/**
 *  页面加载完成之后调用
 *
 *  @param webView    实现该代理的webview
 *  @param navigation 当前navigation
 */
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    if ([navigation isEqual:self.backNavigation]) {
        [webView reload];
        self.backNavigation = nil;
    }
//    self.lastUrlString = webView.URL.absoluteString;
    
    if (isLogin()) {
        NSString *token = [[UserData instance] getUserInfo].token;
        NSString *jsString = [NSString stringWithFormat:@"huiqucanshu('%d', '%@')", YES, token];;
        [self.webView evaluateJavaScript:jsString completionHandler:^(id _Nullable result, NSError * _Nullable error) {
            //            NSLog(@"%@[]%@", result, error);
        }];
    }

    //显示或隐藏分享按钮
    if ([self.shareDict isKindOfClass:[NSDictionary class]]) {
        if (self.shareDict.allKeys.count > 0) {
            self.shareButton.hidden = NO;
        } else {
            self.shareButton.hidden = YES;
        }
    } else {
        self.shareButton.hidden = YES;
    }
}

- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
    
    if ([message.name isEqualToString:@"getToken"]) {

    }

    if ([message.name isEqualToString:@"popToRoot"]) {
        
        [self homePageAction];
    }
    if ([message.name isEqualToString:@"authAction"]) {
        BindBankCarViewController *vc = [[BindBankCarViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }
    if ([message.name isEqualToString:@"loginAction"]) {
        [self.loginTool loginAction];
    }
//    if ([message.name isEqualToString:@"shareAction"]) {
//        self.shareDict = (NSDictionary *)message.body;
//        [self shareButtonDidTap];
//    }
    if ([message.name isEqualToString:@"newShareAction"]) {
        self.shareDict = (NSDictionary *)message.body;
        [self shareButtonDidTap];
    }
    if ([message.name isEqualToString:@"prodectAction"]) {
        
        if (!isLogin())
        {
            [LoginTool loginAction];
            return;
        }

        NSDictionary *dic=(NSDictionary *)message.body;

        [self enterOrderWithProductID:dic[@"id"] isApply:dic[@"b"]];

    }
    if ([message.name isEqualToString:@"callUs"]) {
        [self callAction];

    }
    //产品详情页
    if ([message.name isEqualToString:@"prodectDetailAction"]) {
        
        NSDictionary *dic=(NSDictionary *)message.body;
        
        if (dic[@"isDisplayNativeBtn"])
        {
            [self prodectDetailAction:dic[@"id"] withH5URL:dic[@"url"] isDisplayNativeBtn:dic[@"isDisplayNativeBtn"]];
            
        }
        else
        {
            [self prodectDetailAction:dic[@"id"] withH5URL:dic[@"url"] isDisplayNativeBtn:@"0"];
            
        }
      
    }
    //下单页
//    if ([message.name isEqualToString:@"goToOrderAction"]) {
//
//        if (!isLogin())
//        {
//            [LoginTool loginAction];
//            return;
//        }
//
//        NSString *productID=(NSString *)message.body;
//
//        [self enterOrderWithProductID:productID isApply:nil];
//
//    }
    //产品一级列表页(全部列表)
    if ([message.name isEqualToString:@"allProductListAction"]) {
        
        [self allProductListAction];
    }
    //产品二级列表页
    if ([message.name isEqualToString:@"subProductListAction"]) {
        
        NSDictionary *dic=(NSDictionary *)message.body;
        [self subProductListActionWithBigProductType:dic[@"bigProductType"] productName:dic[@"productName"]];
    }
    
    //首页
    if ([message.name isEqualToString:@"HomePageAction"]) {
        
        
        [self discoverHomeAction];
    }
    
    //发现主页
    if ([message.name isEqualToString:@"discoverHomeAction"]) {
        
        
        [self discoverHomeAction];
    }
    
    //个人主页
    if ([message.name isEqualToString:@"mineHomeAction"]) {
        
        if (!isLogin())
        {
            [LoginTool loginAction];
            return;
        }
        
        
        [self mineHomeAction];
    }
    
    //卡券中心
    if ([message.name isEqualToString:@"couponCenterAction"]) {
        
        if (!isLogin())
        {
            [LoginTool loginAction];
            return;
        }
    
        
        [self couponCenterAction];
        
    }
}



/**
 懒加载
 
 @return loginTool
 */
- (LoginTool *)loginTool {
    if (!_loginTool) {
        _loginTool = [[LoginTool alloc] init];
        _loginTool.loginSuccessDelegate = self;
    }
    return _loginTool;
}


#pragma mark = WKNavigationDelegate

- (void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(nonnull NSString *)message initiatedByFrame:(nonnull WKFrameInfo *)frame completionHandler:(nonnull void (^)(BOOL))completionHandler {
//    NSLog(@"%@", message);
}
- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(nonnull NSString *)message initiatedByFrame:(nonnull WKFrameInfo *)frame completionHandler:(nonnull void (^)(void))completionHandler {
//    NSLog(@"%@", message);
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:message?:@"" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:([UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//        completionHandler();
    }])];
    [self presentViewController:alertController animated:YES completion:nil];
    completionHandler();
}

#pragma mark - 加载页面
- (void)loadWebData {
    if (isLogin()) {
        NSString *token = [[UserData instance] getUserInfo].token;
        if ([self.urlString jk_containsaString:@"?"]) {
            self.urlString = [self.urlString stringByAppendingString:[NSString stringWithFormat:@"&accesstoken=%@", token]];
        } else {
            self.urlString = [self.urlString stringByAppendingString:[NSString stringWithFormat:@"?accesstoken=%@", token]];
        }
    }
    NSURL *url = [NSURL URLWithString:self.urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:request];
}
#pragma mark - 初始化，布局
- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.isDisplayNativeBtn)
    {
        // 请求详情页接口
        [self requestProductDetailDataWithbutton:nil];
    }


    self.view.backgroundColor = COLOR_TBBACK;
    self.originalUrlSting = self.urlString;
    self.title = @"加载中···";
    
    //重写返回按钮
    UIButton *backBarButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    [backBarButton setImage:[UIImage imageNamed:@"navigationButtonReturn"] forState:UIControlStateNormal];
    [backBarButton setImage:[UIImage imageNamed:@"navigationButtonReturnClick"] forState:UIControlStateHighlighted];
    backBarButton.jk_touchAreaInsets = UIEdgeInsetsMake(20, 20, 20, 20);
    backBarButton.imageEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 25);
    [backBarButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backBarButton];

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.shareButton];

    [self.view addSubview:self.webView];
    [self.view addSubview:self.progressView];
    
    [self loadWebData];
    self.shareButton.hidden = YES;
}

//KVO监听进度条
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:NSStringFromSelector(@selector(estimatedProgress))] && object == self.webView) {
        [self.progressView setAlpha:1.0f];
        BOOL animated = self.webView.estimatedProgress > self.progressView.progress;
        [self.progressView setProgress:self.webView.estimatedProgress animated:animated];
        __weak typeof(self) weakSelf = self;
        // Once complete, fade out UIProgressView
        if(self.webView.estimatedProgress >= 1.0f) {
            [UIView animateWithDuration:0.3f delay:0.3f options:UIViewAnimationOptionCurveEaseOut animations:^{
                [weakSelf.progressView setAlpha:0.0f];
            } completion:^(BOOL finished) {
                [weakSelf.progressView setProgress:0.0f animated:NO];
            }];
        }
    } else if ([keyPath isEqualToString:@"title"]) {
        if (object == self.webView) {
            self.title = self.webView.title;
        }
        else {
            [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
        }
    }  else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

/**
 懒加载
 
 @return 分享按钮
 */
- (UIButton *)shareButton
{
    if (!_shareButton) {
        _shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _shareButton.frame = CGRectMake(0, 0, 44, 44);
        [_shareButton setTitle:@"分享" forState:UIControlStateNormal];
        [_shareButton setTitleColor:COLOR_TEXT forState:UIControlStateNormal];
        _shareButton.titleLabel.font = FONT_TITLE;
        [_shareButton addTarget:self action:@selector(shareButtonDidClick) forControlEvents:UIControlEventTouchUpInside];
        _shareButton.contentEdgeInsets = UIEdgeInsetsMake(0, 12, 0, -12);
    }
    return _shareButton;
}

- (UIProgressView *)progressView {
    if (!_progressView) {
        _progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
        _progressView.frame = CGRectMake(0, 0, self.view.bounds.size.width, 1);
        // 设置进度条的色彩
        [_progressView setTrackTintColor:COLOR_LINE];
        _progressView.progressTintColor = [UIColor greenColor];
        CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 1.0f);
        _progressView.transform = transform;//设定宽高
    }
    return _progressView;
}
/**
 懒加载
 
 @return wkWebView
 */
- (WKWebView *)webView {
    if (!_webView) {
        //清除缓存
        NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *cookiesFolderPath = [libraryPath stringByAppendingString:@"/Cookies"];
        NSError *errors;
        [[NSFileManager defaultManager] removeItemAtPath:cookiesFolderPath error:&errors];
        //创建配置
        WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
        // web内容处理池
//        config.processPool = [[WKProcessPool alloc] init];
        
//        config.mediaPlaybackRequiresUserAction = NO;//自动播放
        //创建UserContentController(提供javaScript向webView发送消息的方法)
        WKUserContentController *userContent = [[WKUserContentController alloc] init];
        

        //添加消息处理，注意：self指代的是需要遵守WKScriptMessageHandler协议，结束时需要移除
        [userContent addScriptMessageHandler:self name:@"loginAction"];
        [userContent addScriptMessageHandler:self name:@"shareAction"];
        [userContent addScriptMessageHandler:self name:@"newShareAction"];
        [userContent addScriptMessageHandler:self name:@"prodectAction"];
        [userContent addScriptMessageHandler:self name:@"callUs"];
        [userContent addScriptMessageHandler:self name:@"authAction"];
        [userContent addScriptMessageHandler:self name:@"popToRoot"];
        [userContent addScriptMessageHandler:self name:@"prodectDetailAction"];
        [userContent addScriptMessageHandler:self name:@"allProductListAction"];
        [userContent addScriptMessageHandler:self name:@"subProductListAction"];
        [userContent addScriptMessageHandler:self name:@"discoverHomeAction"];
        [userContent addScriptMessageHandler:self name:@"mineHomeAction"];
        [userContent addScriptMessageHandler:self name:@"couponCenterAction"];
        [userContent addScriptMessageHandler:self name:@"getToken"];
        
        config.userContentController = userContent;
        

        _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - NavBarH) configuration:config];
        _webView.allowsBackForwardNavigationGestures = YES;    //允许右滑返回上个链接，左滑前进

        [_webView addObserver:self forKeyPath:NSStringFromSelector(@selector(estimatedProgress)) options:0 context:WkwebBrowserContext];
        [_webView addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:NULL];

        _webView.navigationDelegate = self;
        _webView.UIDelegate = self;
    }
    return _webView;
}
- (void)dealloc {
//    NSLog(@"%s", __func__);
    [self.webView removeObserver:self forKeyPath:NSStringFromSelector(@selector(estimatedProgress))];
    [self.webView removeObserver:self forKeyPath:@"title"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



#pragma mark---------------------------原生产品详情页处理

-(void)setNewBuyBtn{
    
    
    UIView *bottomView=[[UIView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT-NavBarH-50, SCREEN_WIDTH, 50)];
    bottomView.backgroundColor=COLOR_TBBACK;
    [self.view addSubview:bottomView];
    
    UIButton *buyBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    buyBtn.frame=CGRectMake(10,7, SCREEN_WIDTH/2.0-30, 35);
    buyBtn.backgroundColor=COLOR_GLODBACKGROUD;
    [buyBtn setTitle:@"买!" forState:UIControlStateNormal];
    buyBtn.titleLabel.font=FONT(15);
    buyBtn.layer.masksToBounds=YES;
    buyBtn.layer.cornerRadius=10;
    [buyBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [buyBtn addTarget:self action:@selector(buyBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:buyBtn];
    
    
    
    
    //新手特权
    UIButton *shareBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    shareBtn.frame=CGRectMake(SCREEN_WIDTH/2.0+20,buyBtn.ct_top, buyBtn.ct_width, 35);
    shareBtn.backgroundColor=COLOR_GLODBACKGROUD;
    [shareBtn setTitle:@"分享给朋友" forState:UIControlStateNormal];
    shareBtn.titleLabel.font=FONT(15);
    shareBtn.layer.masksToBounds=YES;
    shareBtn.layer.cornerRadius=10;
    [shareBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [shareBtn addTarget:self action:@selector(shareBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:shareBtn];
    
}
-(void)setBuyBtn:(NSString *)isApplyStr{
    //买入按钮
    UIButton *buyBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    buyBtn.frame=CGRectMake(0,SCREEN_HEIGHT-NavBarH-50, SCREEN_WIDTH, 50);
    buyBtn.backgroundColor=COLOR_MAIN;
    if (isApplyStr.integerValue>0)
    {
        [buyBtn setTitle:@"立即申购" forState:UIControlStateNormal];
    }
    else
    {
        [buyBtn setTitle:@"立即买入" forState:UIControlStateNormal];
    }
    
    [buyBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [buyBtn addTarget:self action:@selector(buyBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:buyBtn];
    
}

#pragma mark--立即买入
-(void)buyBtnClick:(UIButton *)btn{
    
    if (!isLogin())
    {
        [LoginTool loginAction];
        return;
    }
    
    
    if (self.detailModel)
    {
        UIViewController *vc=[GoBuyTool goBuyChoosePageWithDetailModel:self.detailModel];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        btn.userInteractionEnabled=NO;
        [self requestProductDetailDataWithbutton:btn];
        
    }
    
    
}


#pragma mark--分享
-(void)shareBtnClick:(UIButton *)btn
{
    if (isLogin()) {
        if ([self.shareDict isKindOfClass:[NSDictionary class]]) {
            [[ONEShareTool sharedInstance] showShareViewWithShareDict:self.shareDict];
        }
    } else {
        [LoginTool loginAction];
    }
}


#pragma mark---请求产品详情

-(void)requestProductDetailDataWithbutton:(UIButton *)btn
{
    //产品大类 0：招财金；1：生财金；2：特价黄金；3：新手特权金；4：零钱罐；5：存钱罐；6：新手专享；7：存钱罐365天升级版；8：保障金；9：黄金及时赚；10：黄金看涨；11：黄金看跌；
    
    __weak typeof(self) weakSelf = self;
    
    [AllNetWorkRquest productDetailWithProductId:self.productID response:^(id responseBody) {
        

        weakSelf.detailModel=[ProductDetailModel mj_objectWithKeyValues:responseBody];
        
        //bigProductType:3：新手特权金
        if (weakSelf.detailModel.bigProductType.integerValue==3)
        {
            [self setNewBuyBtn];
        }
        else
        {
            [self setBuyBtn:weakSelf.detailModel.isApply];
        }
        
        
        if (btn)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIViewController *vc=[GoBuyTool goBuyChoosePageWithDetailModel:weakSelf.detailModel];
                [self.navigationController pushViewController:vc animated:YES];
                btn.userInteractionEnabled=YES;
                
            });
            
        }
        
    } failureBlock:^(NSString *error) {
        
        if (btn)
        {
            btn.userInteractionEnabled=YES;
        }
        
        [WSProgressHUD showErrorWithStatus:error];
        
    }];
    
}


#pragma mark--下单页
-(void)enterOrderWithProductID:(NSString *)productID isApply:(NSString *)isApplyStr{
    
    [WSProgressHUD showWithMaskType:WSProgressHUDMaskTypeBlack];
    
    //产品大类 0：招财金；1：生财金；2：特价黄金；3：新手特权金；4：零钱罐；5：存钱罐；6：新手专享；7：存钱罐365天升级版；8：保障金；9：黄金及时赚；10：黄金看涨；11：黄金看跌；
    
    __weak typeof(self) weakSelf = self;
    
    [AllNetWorkRquest productDetailWithProductId:productID response:^(id responseBody) {
        
        [WSProgressHUD dismiss];
        ProductDetailModel *model=[ProductDetailModel mj_objectWithKeyValues:responseBody];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIViewController *vc=[GoBuyTool goBuyChoosePageWithDetailModel:model];
            [weakSelf.navigationController pushViewController:vc animated:YES];
        });
    } failureBlock:^(NSString *error) {
        
        
        [WSProgressHUD dismiss];
        [WSProgressHUD showErrorWithStatus:error];
        
    }];
    
}

#pragma mark--产品详情页
-(void)prodectDetailAction:(NSString *)productID withH5URL:(NSString *)url isDisplayNativeBtn:(NSString *)isDisplayNativeBtn
{
    CBWebViewController *vc=[[CBWebViewController alloc]init];
    vc.urlString=url;
    vc.productID=productID;
    vc.isDisplayNativeBtn=isDisplayNativeBtn.integerValue;
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark--全部产品列表（一级页面）
-(void)allProductListAction
{
    if ([self.navigationController.jk_rootViewController isKindOfClass:[AllViewController class]])
    {
        
        [self.navigationController popToRootViewControllerAnimated:YES];
        
    }
    else
    {
        [self.tabBarController setSelectedIndex:1];
        [self.navigationController popToRootViewControllerAnimated:YES];
        
        
    }
}

#pragma mark--产品子列表
-(void)subProductListActionWithBigProductType:(NSString *)bigProductType productName:(NSString *)productName
{
    SubAllListViewController *vc=[[SubAllListViewController alloc]init];
    vc.productType=bigProductType.integerValue;
    vc.productName=productName;
    [self.navigationController pushViewController:vc animated:YES];
    
}

#pragma mark--发现主页（一级页面）
-(void)discoverHomeAction
{
    if ([self.navigationController.jk_rootViewController isKindOfClass:[FindViewController class]])
    {
        
        [self.navigationController popToRootViewControllerAnimated:YES];
        
    }
    else
    {
        [self.tabBarController setSelectedIndex:2];
        [self.navigationController popToRootViewControllerAnimated:YES];
        
        
    }
}

#pragma mark--个人主页（一级页面）
-(void)mineHomeAction
{
    if ([self.navigationController.jk_rootViewController isKindOfClass:[MineViewController class]])
    {
        
        [self.navigationController popToRootViewControllerAnimated:YES];
        
    }
    else
    {
        [self.tabBarController setSelectedIndex:3];
        [self.navigationController popToRootViewControllerAnimated:YES];
        
        
    }
}

#pragma mark--首页（一级页面）
-(void)homePageAction
{
    if ([self.navigationController.jk_rootViewController isKindOfClass:[HomeViewController class]])
    {
        
        [self.navigationController popToRootViewControllerAnimated:YES];
        
    }
    else
    {
        [self.tabBarController setSelectedIndex:0];
        [self.navigationController popToRootViewControllerAnimated:YES];
        
        
    }
}


#pragma mark--卡券中心
-(void)couponCenterAction
{
    CouponsViewController *vc=[[CouponsViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
    
}


@end
