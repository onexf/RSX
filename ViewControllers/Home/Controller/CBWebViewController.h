//
//  CBWebViewController.h
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/8/24.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CBWebViewController : UIViewController

/** 传进来的链接 */
@property(nonatomic, copy, nonnull) NSString *urlString;
/** 是否显示分享按钮 */
@property(assign) BOOL showShareButton;

//@property(nonatomic,copy)NSString *productURL;
@property(nonatomic,copy)NSString *productID;  //针对原生跳转
@property(nonatomic,assign)BOOL isDisplayNativeBtn;  //针对原生跳转

@end
