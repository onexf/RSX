//
//  TestViewController.m
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/8/21.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "TestViewController.h"
#import "GoldCatBankNavController.h"
#import "UIBarButtonItem+item.h"
#import <FDFullscreenPopGesture/UINavigationController+FDFullscreenPopGesture.h>
#import "RealReachability.h"
#import "TypeButton.h"
#import <INTULocationManager/INTULocationManager.h>
#import "UIView+Extension.h"

@interface TestViewController ()

/** <#name#> */
@property(nonatomic, weak) IBOutlet UIButton *button1;
@property(nonatomic, weak) IBOutlet UIButton *button2;

@property (assign) NSInteger locationStatus;

@property (nonatomic, strong) CLLocation *location;
@property (nonatomic, strong) CLGeocoder *geocoder;


@property (assign) INTULocationRequestID requestID;
@property (assign, nonatomic) INTULocationAccuracy desiredAccuracy;


/** uilabel */
@property(nonatomic, weak) IBOutlet UILabel *positionLabel;



@end

@implementation TestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = COLOR_TBBACK;
    
    self.locationStatus = 1;
    self.desiredAccuracy = INTULocationAccuracyRoom;

    
    TypeButton *button1 = [[TypeButton alloc] initWithFrame:CGRectMake(100, 100, 99, 40)];
    [button1 setTitle:@"XXX" forState:UIControlStateNormal];
    [button1 setTitle:@"XXX" forState:UIControlStateDisabled];

    [self.view addSubview:button1];
    self.button1 = button1;

    TypeButton *button2 = [[TypeButton alloc] initWithFrame:CGRectMake(100, 300, 99, 40)];
    [button2 setTitle:@"iPhone" forState:UIControlStateDisabled];
    [button2 setTitle:@"iPhone" forState:UIControlStateNormal];

    [self.view addSubview:button2];
    button2.enabled = NO;
    self.button2 = button2;
    
    
    
    UILabel *position = [[UILabel alloc] initWithFrame:CGRectMake(0, 200, SCREEN_WIDTH, 20)];
    [self.view addSubview:position];
    position.backgroundColor = COLOR_MAIN_BLUE;
    position.textColor = COLOR_Red;
    [position addTapAction:@selector(jumpToMapApp) target:self];
    self.positionLabel = position;
    
    
    INTULocationManager *manger = [INTULocationManager sharedInstance];
    
    __weak typeof(self) weakSelf = self;
//    self.requestID = [manger requestLocationWithDesiredAccuracy:self.desiredAccuracy
//                                                                timeout:MAXFLOAT
//                                                   delayUntilAuthorized:YES
//                                                                  block:
//                              ^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
//                                  __typeof(weakSelf) strongSelf = weakSelf;
//                                  if (status == INTULocationStatusSuccess) {
//                                      CLGeocoder *geocoder = [[CLGeocoder alloc] init];
//                                      [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
//                                          for (CLPlacemark *placemark in placemarks) {
//                                              NSLog(@"%@ %@ %f %f", placemark.name, placemark.addressDictionary, placemark.location.coordinate.latitude, placemark.location.coordinate.longitude);
//                                              strongSelf.positionLabel.text = [NSString stringWithFormat:@"%@-%@-%@-%@", placemark.locality, placemark.subLocality, placemark.thoroughfare, placemark.name];
//                                          }
//                                      }];
//                                  }
//                                  else if (status == INTULocationStatusTimedOut) {
//                                      // You may wish to inspect achievedAccuracy here to see if it is acceptable, if you plan to use currentLocation
//                                      strongSelf.positionLabel.text = [NSString stringWithFormat:@"Location request timed out. Current Location:\n%@", currentLocation];
//                                  }
//                                  else {
//                                      // An error occurred
//                                  }
//                                  strongSelf.requestID = NSNotFound;
//                              }];

    self.requestID =  [manger subscribeToLocationUpdatesWithBlock:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
        weakSelf.locationStatus = status;
        NSLog(@"定位信息：%@-------%ld", currentLocation, (long)status);

        if (status == INTULocationStatusSuccess) {
            weakSelf.location = currentLocation;
            CLGeocoder *geocoder = [[CLGeocoder alloc] init];
            [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
                for (CLPlacemark *placemark in placemarks) {
                    NSLog(@"%@ %@ %f %f", placemark.name, placemark.addressDictionary, placemark.location.coordinate.latitude, placemark.location.coordinate.longitude);
                    self.positionLabel.text = [NSString stringWithFormat:@"%@-%@-%@-%@", placemark.locality, placemark.subLocality, placemark.thoroughfare, placemark.name];
                }
            }];
        }
    }];
}
- (void)jumpToMapApp {
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"导航到设备" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    //自带地图
    [alertController addAction:[UIAlertAction actionWithTitle:@"自带地图" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        NSLog(@"alertController -- 自带地图");
        
        //使用自带地图导航
        MKMapItem *currentLocation =[MKMapItem mapItemForCurrentLocation];
        
        MKMapItem *toLocation = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:self.location.coordinate addressDictionary:nil]];
        
        [MKMapItem openMapsWithItems:@[currentLocation,toLocation] launchOptions:@{MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving,
                                                                                   MKLaunchOptionsShowsTrafficKey:[NSNumber numberWithBool:YES]}];
        
        
    }]];
    
    //判断是否安装了高德地图，如果安装了高德地图，则使用高德地图导航
    if ( [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"iosamap://"]]) {
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"高德地图" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            NSLog(@"alertController -- 高德地图");
            NSString *urlsting = [[NSString stringWithFormat:@"iosamap://path?sourceApplication=applicationName&sid=BGVIS1&slat=%lf&slon=%lf&sname=我的位置&did=BGVIS2&dlat=%lf&dlon=%lf&dname=%@&dev=0&m=0&t=%@",self.location.coordinate.latitude, self.location.coordinate.longitude, 31.23183, 121.517206, @"荣盛祥XXX店", @"荣盛祥"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

//            NSString *urlsting =[[NSString stringWithFormat:@"iosamap://navi?sourceApplication= &backScheme= &lat=%f&lon=%f&dev=0&style=2",self.location.coordinate.latitude,self.location.coordinate.longitude] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            [[UIApplication  sharedApplication]openURL:[NSURL URLWithString:urlsting]];
            
        }]];
    }
    
    //判断是否安装了百度地图，如果安装了百度地图，则使用百度地图导航
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"baidumap://"]]) {
        [alertController addAction:[UIAlertAction actionWithTitle:@"百度地图" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            NSLog(@"alertController -- 百度地图");
            NSString *urlsting =[[NSString stringWithFormat:@"baidumap://map/direction?origin={{我的位置}}&destination=latlng:%f,%f|name=目的地&mode=driving&coord_type=gcj02",self.location.coordinate.latitude,self.location.coordinate.longitude] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlsting]];
            
        }]];
    }
    
    //添加取消选项
    [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        [alertController dismissViewControllerAnimated:YES completion:nil];
        
    }]];
    
    //显示alertController
    [self presentViewController:alertController animated:YES completion:nil];
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    self.button1.enabled = !self.button1.enabled;
    self.button2.enabled = !self.button2.enabled;
}

- (void)updateTime{
    
}

@end
