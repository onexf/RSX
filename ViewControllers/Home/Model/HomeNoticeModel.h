//
//  HomeNoticeModel.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/7.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HomeNoticeModel : NSObject
/** 时间 */
@property(nonatomic, copy) NSString *createTime;
/** 描述 */
@property(nonatomic, copy) NSString *noticeDescription;
/** 详情 */
@property(nonatomic, copy) NSString *details;
/** 跳转链接 */
@property(nonatomic, copy) NSString *href;
/** id */
@property(nonatomic, copy) NSString *id;
/** 标题 */
@property(nonatomic, copy) NSString *title;







@end
