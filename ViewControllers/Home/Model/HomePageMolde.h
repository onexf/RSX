//
//  HomePageMolde.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/7.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <Foundation/Foundation.h>
@class BannerModel, AllListModel, HomeNoticeModel, GoldMarketModel;

@interface HomePageMolde : NSObject

/** 轮播数据 */
@property(nonatomic, strong) NSArray<BannerModel *> *banners;
/** 是否显示新手见面礼 1不是新手 0是新手*/
@property(nonatomic, copy) NSString *isNew;
/** 新手见面礼 */
@property(nonatomic, strong) NSArray<AllListModel *> *productListTiro;
/** 首页产品 */
@property(nonatomic, strong) NSArray<AllListModel *> *productList;
/** 公告 */
@property(nonatomic, strong) HomeNoticeModel *noticeModel;
/** 首页商铺 */
@property(nonatomic, strong) GoldMarketModel *shopModel;

@end
