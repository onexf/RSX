//
//  BannerModel.h
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/8/23.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BannerModel : NSObject

/** category */
@property(nonatomic, copy) NSString *category;
/** discription */
@property(nonatomic, copy) NSString *WTF;
/** 跳转链接 */
@property(nonatomic, copy) NSString *href;
/** id */
@property(nonatomic, copy) NSString *id;
/** 图片链接 */
@property(nonatomic, copy) NSString *picUrl;
/** sort */
@property(nonatomic, copy) NSString *sort;
/** title */
@property(nonatomic, copy) NSString *title;



@end
