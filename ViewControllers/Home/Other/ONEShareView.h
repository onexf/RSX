//
//  ONEShareView.h
//  CWOne
//
//  Created by Coulson_Wang on 2017/8/12.
//  Copyright © 2017年 Coulson_Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UMSocialCore/UMSocialCore.h>

@class ONEShareView;

@protocol ONEShareViewDelegate <NSObject>

- (void)didCopyShareLink:(ONEShareView *)shareView;

@end

void typedef (^SharedWithType)(UMSocialPlatformType UMShareType);

@interface ONEShareView : UIImageView

@property (weak, nonatomic) id<ONEShareViewDelegate> delegate;

/** 分享链接 */
@property (strong, nonatomic) NSString *shareUrl;
/** 分享平台 */
@property (nonatomic,copy)SharedWithType shareType;


- (void)showShareAnimaton;


@end
