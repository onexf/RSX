//
//  ONEShareView.m
//  CWOne
//
//  Created by Coulson_Wang on 2017/8/12.
//  Copyright © 2017年 Coulson_Wang. All rights reserved.
//

#import "ONEShareView.h"

#define kAnimationDuration 0.3
#define kButtonVerticalDistance 120.0
#define kBackgroundAlpha 0.9

@interface ONEShareView ()

@property (weak, nonatomic) UIButton *wechatMomentButton;
@property (weak, nonatomic) UIButton *wechatFriendButton;
@property (weak, nonatomic) UIButton *sinaWeiboButton;
@property (weak, nonatomic) UIButton *QQButton;
@property (weak, nonatomic) UIButton *QQZoneButton;
@property (weak, nonatomic) UIButton *copylinkButton;

@end

@implementation ONEShareView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI {
    UIBlurEffect *effect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:effect];
    effectView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    [self addSubview:effectView];

//    self.backgroundColor = [UIColor colorWithWhite:1.0 alpha:kBackgroundAlpha];
    self.userInteractionEnabled = YES;
    [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideShareView)]];
    
    UIButton *wechatMomentButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [wechatMomentButton setImage:[UIImage imageNamed:@"ShareWechatMomentImage"] forState:UIControlStateNormal];
    [wechatMomentButton setTitle:@"朋友圈" forState:UIControlStateNormal];
    [wechatMomentButton setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
    wechatMomentButton.titleLabel.font = FONT_SUBTEXT;
    [wechatMomentButton jk_setImagePosition:LXMImagePositionTop spacing:7];
    [wechatMomentButton sizeToFit];
    wechatMomentButton.jk_touchAreaInsets = UIEdgeInsetsMake(20, 20, 20, 20);
    [wechatMomentButton addTarget:self action:@selector(wechatMomentButtonDidTap) forControlEvents:UIControlEventTouchUpInside];
    wechatMomentButton.ct_centerX = SCREEN_WIDTH * 0.5 - SCREEN_WIDTH * 0.34;
    wechatMomentButton.ct_centerY = SCREEN_HEIGHT * 0.5;
    [self addSubview:wechatMomentButton];
    self.wechatMomentButton = wechatMomentButton;
    
    UIButton *wechatFriendButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [wechatFriendButton setImage:[UIImage imageNamed:@"ShareWechatFriendImage"] forState:UIControlStateNormal];
    [wechatFriendButton setTitle:@"微信" forState:UIControlStateNormal];
    [wechatFriendButton setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
    wechatFriendButton.titleLabel.font = FONT_SUBTEXT;
    [wechatFriendButton jk_setImagePosition:LXMImagePositionTop spacing:7];
    [wechatFriendButton sizeToFit];
    wechatFriendButton.jk_touchAreaInsets = UIEdgeInsetsMake(20, 20, 20, 20);
    [wechatFriendButton addTarget:self action:@selector(wechatFriendButtonDidTap) forControlEvents:UIControlEventTouchUpInside];
    wechatFriendButton.ct_centerX = SCREEN_WIDTH * 0.5;
    wechatFriendButton.ct_centerY = SCREEN_HEIGHT * 0.5;
    [self addSubview:wechatFriendButton];
    self.wechatFriendButton = wechatFriendButton;
    
    UIButton *sinaWeiboButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [sinaWeiboButton setImage:[UIImage imageNamed:@"ShareSinaWeiboImage"] forState:UIControlStateNormal];
    [sinaWeiboButton setTitle:@"微博" forState:UIControlStateNormal];
    [sinaWeiboButton setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
    sinaWeiboButton.titleLabel.font = FONT_SUBTEXT;
    [sinaWeiboButton jk_setImagePosition:LXMImagePositionTop spacing:7];
    [sinaWeiboButton sizeToFit];
    sinaWeiboButton.jk_touchAreaInsets = UIEdgeInsetsMake(20, 20, 20, 20);
    [sinaWeiboButton addTarget:self action:@selector(sinaWeiboButtonDidTap) forControlEvents:UIControlEventTouchUpInside];
    sinaWeiboButton.ct_centerX = SCREEN_WIDTH * 0.5 + SCREEN_WIDTH * 0.34;
    sinaWeiboButton.ct_centerY = SCREEN_HEIGHT * 0.5;
    [self addSubview:sinaWeiboButton];
    self.sinaWeiboButton = sinaWeiboButton;
    
    UIButton *QQButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [QQButton setImage:[UIImage imageNamed:@"ShareQQImage"] forState:UIControlStateNormal];
    [QQButton setTitle:@"QQ" forState:UIControlStateNormal];
    [QQButton setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
    QQButton.titleLabel.font = FONT_SUBTEXT;
    [QQButton jk_setImagePosition:LXMImagePositionTop spacing:7];
    [QQButton sizeToFit];
    QQButton.jk_touchAreaInsets = UIEdgeInsetsMake(20, 20, 20, 20);
    [QQButton addTarget:self action:@selector(QQButtonDidTap) forControlEvents:UIControlEventTouchUpInside];
    QQButton.ct_centerX = SCREEN_WIDTH * 0.5 - SCREEN_WIDTH * 0.34;
    QQButton.ct_centerY = SCREEN_HEIGHT * 0.5;
    [self addSubview:QQButton];
    self.QQButton = QQButton;
    
    UIButton *qZoneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [qZoneButton setImage:[UIImage imageNamed:@"ShareQZoneImage"] forState:UIControlStateNormal];
    [qZoneButton setTitle:@"空间" forState:UIControlStateNormal];
    [qZoneButton setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
    qZoneButton.titleLabel.font = FONT_SUBTEXT;
    [qZoneButton jk_setImagePosition:LXMImagePositionTop spacing:7];
    [qZoneButton sizeToFit];
    qZoneButton.jk_touchAreaInsets = UIEdgeInsetsMake(20, 20, 20, 20);
    [qZoneButton addTarget:self action:@selector(QQZoneButtonDidTap) forControlEvents:UIControlEventTouchUpInside];
    qZoneButton.ct_centerX = SCREEN_WIDTH * 0.5;
    qZoneButton.ct_centerY = SCREEN_HEIGHT * 0.5;
    [self addSubview:qZoneButton];
    self.QQZoneButton = qZoneButton;

    
    UIButton *copylinkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [copylinkButton setImage:[UIImage imageNamed:@"ShareCopyLinkImage"] forState:UIControlStateNormal];
    [copylinkButton setTitle:@"链接" forState:UIControlStateNormal];
    [copylinkButton setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
    copylinkButton.titleLabel.font = FONT_SUBTEXT;
    [copylinkButton jk_setImagePosition:LXMImagePositionTop spacing:7];
    [copylinkButton sizeToFit];
    copylinkButton.jk_touchAreaInsets = UIEdgeInsetsMake(20, 20, 20, 20);
    [copylinkButton addTarget:self action:@selector(copyShareLinkToPasteBoard) forControlEvents:UIControlEventTouchUpInside];
    copylinkButton.ct_centerX = SCREEN_WIDTH * 0.5 + SCREEN_WIDTH * 0.34;
    copylinkButton.ct_centerY = SCREEN_HEIGHT * 0.5;
    [self addSubview:copylinkButton];
    self.copylinkButton = copylinkButton;
    
    
    
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [closeButton setImage:[UIImage imageNamed:@"Share_close"] forState:UIControlStateNormal];
    [closeButton sizeToFit];
    closeButton.jk_touchAreaInsets = UIEdgeInsetsMake(20, 20, 20, 20);
    [closeButton addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideShareView)]];
    closeButton.ct_centerX = SCREEN_WIDTH * 0.5;
    closeButton.ct_centerY = SCREEN_HEIGHT - (IS_IPHONEX ? 60 : 40);
    [self addSubview:closeButton];
    
}

- (void)showShareAnimaton {
    [UIView animateWithDuration:kAnimationDuration animations:^{
//        self.wechatMomentButton.ct_y -= 2 * kButtonVerticalDistance;
//        self.wechatFriendButton.ct_y -= kButtonVerticalDistance;
//        self.QQButton.ct_y += kButtonVerticalDistance;
//        self.copylinkButton.ct_y += 2 * kButtonVerticalDistance;
        self.wechatMomentButton.ct_y = SCREEN_HEIGHT / 2 + SCREEN_WIDTH / 6;
        self.wechatFriendButton.ct_y = SCREEN_HEIGHT / 2 + SCREEN_WIDTH / 6;
        self.sinaWeiboButton.ct_y = SCREEN_HEIGHT / 2 + SCREEN_WIDTH / 6;
        self.QQButton.ct_y = SCREEN_HEIGHT / 2 + SCREEN_WIDTH / 2;
        self.QQZoneButton.ct_y = SCREEN_HEIGHT / 2 + SCREEN_WIDTH / 2;
        self.copylinkButton.ct_y = SCREEN_HEIGHT / 2 + SCREEN_WIDTH / 2;
    }];
}

- (void)hideShareView {
    [UIView animateWithDuration:kAnimationDuration animations:^{
        self.wechatMomentButton.ct_centerY = SCREEN_HEIGHT * 0.5;
        self.wechatFriendButton.ct_centerY = SCREEN_HEIGHT * 0.5;
        self.QQButton.ct_centerY = SCREEN_HEIGHT * 0.5;
        self.QQZoneButton.ct_centerY = SCREEN_HEIGHT * 0.5;
        self.copylinkButton.ct_centerY = SCREEN_HEIGHT * 0.5;
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}
//微信朋友圈
- (void)wechatMomentButtonDidTap {
    [self hideShareView];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kAnimationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (self.shareType) {
            self.shareType(UMSocialPlatformType_WechatTimeLine);
        }
    });
}
//微信好友
- (void)wechatFriendButtonDidTap {
    [self hideShareView];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kAnimationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (self.shareType) {
            self.shareType(UMSocialPlatformType_WechatSession);
        }
    });
}
//新浪微博
- (void)sinaWeiboButtonDidTap {
    [self hideShareView];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kAnimationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (self.shareType) {
            self.shareType(UMSocialPlatformType_Sina);
        }
    });
}
//QQ好友
- (void)QQButtonDidTap {
    [self hideShareView];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kAnimationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (self.shareType) {
            self.shareType(UMSocialPlatformType_QQ);
        }
    });
}
- (void)QQZoneButtonDidTap {
    [self hideShareView];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kAnimationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (self.shareType) {
            self.shareType(UMSocialPlatformType_Qzone);
        }
    });
}
- (void)copyShareLinkToPasteBoard {
    // 拷贝链接到剪切板
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.shareUrl;
    
    // 移除视图
    [self hideShareView];
    
    // 显示“已复制”提示
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kAnimationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if ([self.delegate respondsToSelector:@selector(didCopyShareLink:)]) {
            [self.delegate didCopyShareLink:self];
        }
    });
    
}

@end
