//
//  AppDelegate+UMSocial.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/8/30.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "AppDelegate+UMSocial.h"
#import <UMSocialQQHandler.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import "WXApi.h"
#import "WeiboSDK.h"

#define UM_APPKEY           @"59f00119734be439b9000012"    //马甲包59f00119734be439b9000012   正式包599163fcbbea836109000127

#define UM_WX_APPID         @"wx21805e8d63efaf7c"
#define UM_WX_APPSECRET     @"78d619ed94910c7277ba580508b02111"

#define UM_QQ_APPID         @"1106327362"
#define UM_QQ_APPKEY        @"GokE1KXAO4OfNZqi"

#define UM_SINA_APPKEY      @"2851800949"
#define UM_SINA_SECRET      @"291e34a0f660f6a7fbeb6e15ee4469a4"


@implementation AppDelegate (UMSocial)


- (void)um_registerConfigurations {
    //打开调试日志
    [[UMSocialManager defaultManager] openLog:YES];
    
    //设置友盟appkey
    [[UMSocialManager defaultManager] setUmSocialAppkey:UM_APPKEY];
    
    //设置微信的appKey和appSecret
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatSession appKey:UM_WX_APPID appSecret:UM_WX_APPSECRET redirectURL:@""];
    
    //设置分享到QQ互联的appKey和appSecret
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_QQ appKey:UM_QQ_APPID  appSecret:UM_QQ_APPKEY redirectURL:@""];
    
    //设置新浪的appKey和appSecret
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_Sina appKey:UM_SINA_APPKEY  appSecret:UM_SINA_SECRET redirectURL:@""];
}

+ (void)um_shareToFriendWithType:(UMSocialPlatformType)shareType title:(NSString *)title message:(NSString *)message image:(NSString *)image url:(NSString *)url delegate:(id)viewController successMsg:(NSString *)successMsg failedMsg:(NSString *)failMsg successBlock:(void(^)(void))successBlock failureBlock:(void(^)(void))failureBlock {

    NSString *type = @"0";
    if (shareType == UMSocialPlatformType_WechatTimeLine) {
        if (![WXApi isWXAppInstalled]) {
            [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[WXApi getWXAppInstallUrl]]];
            return;
        }
        message = title;
        type = @"1";
    }
    if (shareType == UMSocialPlatformType_WechatSession) {
        if (![WXApi isWXAppInstalled]) {
            [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[WXApi getWXAppInstallUrl]]];
            return;
        }
        type = @"2";
    }
    if (shareType == UMSocialPlatformType_QQ) {
        if (![TencentOAuth iphoneQQInstalled]) {
            [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"https://appstore/cn/kOsHA.i"]];
            return;
        }
        type = @"4";
    }
    if(shareType == UMSocialPlatformType_Sina){
        message = [message stringByAppendingString:url];
        if (![WeiboSDK isWeiboAppInstalled]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[WeiboSDK getWeiboAppInstallUrl]]];
            return;
        }
        type = @"3";
    }
#if DEBUG
    NSLog(@"推送的type是%@",type);
#endif
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    
    UMShareWebpageObject *webPageUrl = [[UMShareWebpageObject alloc] init];
    webPageUrl.webpageUrl = url;
    webPageUrl.title = title;
    webPageUrl.thumbImage = image;
    webPageUrl.descr = message;
    
    //分享消息对象设置分享内容对象
    messageObject.shareObject = webPageUrl;
    
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:shareType messageObject:messageObject currentViewController:nil completion:^(id data, NSError *error) {
        if (error) {
            if (failureBlock) {
                NSLog(@"%@", error);
                failureBlock();
            }
        }else{
            if (successBlock) {
//                NSLog(@"%@", data);
                successBlock();
            }
        }
    }];
}
@end
