//
//  AppDelegate+UMSocial.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/8/30.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "AppDelegate.h"
#import <UMSocialCore/UMSocialCore.h>

@interface AppDelegate (UMSocial)

/**
 初始化分享
 */
- (void)um_registerConfigurations;

/**
 调用分享

 @param shareType 平台
 @param title 标题
 @param message 信息
 @param image 图片
 @param url 链接
 @param viewController 代理
 @param successMsg 分享成功
 @param failMsg 分享失败
 @param successBlock 分享回调
 */
+ (void)um_shareToFriendWithType:(UMSocialPlatformType)shareType title:(NSString *)title message:(NSString *)message image:(NSString *)image url:(NSString *)url delegate:(id)viewController successMsg:(NSString *)successMsg failedMsg:(NSString *)failMsg successBlock:(void(^)(void))successBlock failureBlock:(void(^)(void))failureBlock;

@end
