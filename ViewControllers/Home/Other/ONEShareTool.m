//
//  ONEShareTool.m
//  CWOne
//
//  Created by Coulson_Wang on 2017/8/12.
//  Copyright © 2017年 Coulson_Wang. All rights reserved.
//

#import "ONEShareTool.h"
#import "ONEShareView.h"
#import <UMSocialCore/UMSocialCore.h>
#import "AppDelegate+UMSocial.h"
#import "NetworkSingleton+Coupon.h"
#import "CouponModel.h"
static ONEShareTool *_instance;

@interface ONEShareTool () <ONEShareViewDelegate>

@property (weak, nonatomic) ONEShareView *shareView;

@end

@implementation ONEShareTool

- (ONEShareView *)shareView {
    if (!_shareView) {
        ONEShareView *shareView = [[ONEShareView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        shareView.delegate = self;
        [[UIApplication sharedApplication].keyWindow addSubview:shareView];
        _shareView = shareView;
    }
    return _shareView;
}

+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[ONEShareTool alloc] init];
    });
    return _instance;
}
- (void)showShareViewWithShareDict:(NSDictionary *)shareDict {
    
    if ([shareDict[@"url"] jk_containsaString:@"Inviting1"]) {
        self.shareView.shareUrl = isLogin() ? ([shareDict[@"url"] stringByAppendingString:[NSString stringWithFormat:@"?inviteCode=%@", [UserData instance].getUserInfo.inviteCode]]) : shareDict[@"url"];
    } else {
        self.shareView.shareUrl = shareDict[@"url"];
    }

    self.shareView.shareType = ^(UMSocialPlatformType UMShareType) {

        NSString *shareUrlString, *title, *content;
        
        shareUrlString = self.shareView.shareUrl;
        title = shareDict[@"title"];
        content = shareDict[@"content"];
        NSString *image = shareDict[@"image"];
        switch (UMShareType) {
            case UMSocialPlatformType_Sina:
            {
                NSLog(@"新浪微博");
            }
                break;
            case UMSocialPlatformType_WechatSession:
            {
                NSLog(@"微信聊天");
            }
                break;
            case UMSocialPlatformType_WechatTimeLine:
            {
                NSLog(@"微信朋友圈");
            }
                break;
            case UMSocialPlatformType_QQ:
            {
                NSLog(@"QQ好友");
            }
                break;
            default:
                break;
        }
        [AppDelegate um_shareToFriendWithType:UMShareType title:title message:content image:image url:shareUrlString delegate:nil successMsg:@"分享成功" failedMsg:@"分享失败" successBlock:^{
            [WSProgressHUD showSuccessWithStatus:@"分享成功"];
//            weakSelf.image = nil;
        } failureBlock:^{
            [WSProgressHUD showSuccessWithStatus:@"分享失败"];
//            weakSelf.image = nil;
        }];
    };
    [self.shareView showShareAnimaton];
}

#pragma mark - ONEShareViewDelegate
- (void)didCopyShareLink:(ONEShareView *)shareView {
    [WSProgressHUD showSuccessWithStatus:@"已复制"];
}
@end
