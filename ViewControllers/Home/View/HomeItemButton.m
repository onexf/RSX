//
//  HomeItemButton.m
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/8/24.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "HomeItemButton.h"

@implementation HomeItemButton

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setTitleColor:UIColorFromHex(0x333333) forState:UIControlStateNormal];
        self.titleLabel.font = FONT_SUBTEXT;
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self jk_setImagePosition:LXMImagePositionTop spacing:5];
    self.titleLabel.ct_width = SCREEN_WIDTH / 4;

}
@end
