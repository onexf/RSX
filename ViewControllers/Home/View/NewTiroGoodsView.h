//
//  NewTiroGoodsView.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/11/16.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AllListModel;
typedef NS_ENUM(NSInteger, NewHomeTiroGoodsType) {
    NewHomeTiroGoodsTypeOther = 0,//非黄金专享
    NewHomeTiroGoodsTypeGold,//特价黄金
};

@interface NewTiroGoodsView : UIView

/** 类型 */
@property(nonatomic ,assign) NewHomeTiroGoodsType goodsType;

/** 数据 */
@property(nonatomic, strong) AllListModel *goodsData;

@end
