//
//  HomeGoldPriceView.m
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/9/6.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "HomeGoldPriceView.h"
#import "CWCalendarLabel.h"
#import "CBWebViewController.h"
#import "ProductDetailWebViewVC.h"
#import <FDFullscreenPopGesture/UINavigationController+FDFullscreenPopGesture.h>
#import "AllNetWorkRquest.h"
#import "GoBuyTool.h"
//#import ""
@interface HomeGoldPriceView ()
/** clock */
@property(nonatomic, strong) UIImageView *clockImageView;
/** 查看实时金价走势 */
@property(nonatomic, strong) UILabel *goldPriceChatLabel;
/**  | 参考上海黄金交易所 / 国际金价 */
@property(nonatomic, strong) UILabel *goldPriceChatSubLabel;
/** 金价 */
@property(nonatomic, strong) CWCalendarLabel *goldPrice;
/** 单位 */
@property(nonatomic, strong) UILabel *goldUnit;
/** 市场价 */
@property(nonatomic, strong) UILabel *normalPrice;
/** 市场价 */
@property(nonatomic, strong) UILabel *normalPriceValue;
/** 横线 */
@property(nonatomic, strong) UIView *lineOnPrice;
/** sep */
@property(nonatomic, strong) UIView *sepLine;
/** 买入黄金 */
@property(nonatomic, strong) UIButton *buyGoldButton;
/** 金价走势 */
@property(nonatomic, strong) UIButton *historyPrice;

@end

@implementation HomeGoldPriceView

- (void)setCurrentGoldPrice:(NSString *)currentGoldPrice {
    _currentGoldPrice = currentGoldPrice;
    [self.goldPrice showNextText:currentGoldPrice withDirection:CWCalendarLabelScrollToTop];
}
- (void)buyGoldButtonDidTap {
    if (isLogin()) {
        __weak typeof(self) weakSelf = self;
        [WSProgressHUD showWithMaskType:WSProgressHUDMaskTypeBlack];
        [AllNetWorkRquest productDetailWithProductId:@"0" response:^(ProductDetailModel *model, NSString *errMsg) {
            [WSProgressHUD dismiss];
            if (errMsg) {
                [WSProgressHUD showErrorWithStatus:errMsg];
            } else {
                UIViewController *vc=[GoBuyTool goBuyChoosePageWithDetailModel:model];
                [weakSelf.jk_viewController.navigationController pushViewController:vc animated:YES];
            }
        }];
    } else {
        [LoginTool loginAction];
    }
}
- (void)historyPriceDidTap {
    CBWebViewController *webViewController = [[CBWebViewController alloc] init];
    webViewController.urlString = @"https://www.zhaojinmao.cn/testwebApp/prodect/trend/trend.html";
    webViewController.showShareButton = NO;
    webViewController.fd_interactivePopDisabled = YES;
    [self.jk_viewController.navigationController pushViewController:webViewController animated:YES];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.clockImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.goldPriceChatLabel);
        make.left.equalTo(self).offset(LEFT_RIGHT_MARGIN);
    }];
    [self.goldPriceChatLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(14);
        make.left.equalTo(self.clockImageView.mas_right).offset(7);
    }];
    [self.goldPriceChatSubLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.goldPriceChatLabel);
        make.right.equalTo(self).offset(-LEFT_RIGHT_MARGIN);
    }];
    [self.goldPrice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goldPriceChatSubLabel.mas_bottom).offset(25);
        make.centerX.equalTo(self).offset(-10);
    }];
    [self.goldUnit mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.goldPrice).offset(-6);
        make.left.equalTo(self.goldPrice.mas_right).offset(5);
    }];
    [self.normalPrice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goldPrice.mas_bottom).offset(5);
        make.centerX.equalTo(self).offset(-25);
    }];
    [self.normalPriceValue mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.normalPrice);
        make.left.equalTo(self.normalPrice.mas_right).offset(4);
    }];
    [self.lineOnPrice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.centerY.equalTo(self.normalPriceValue);
        make.height.equalTo(@1);
        make.right.equalTo(self.normalPriceValue);
    }];
    [self.sepLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.normalPrice.mas_bottom).offset(16);
        make.left.right.equalTo(self);
        make.height.equalTo(@1);
    }];
    CGFloat width = 126;
    [self.buyGoldButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.sepLine.mas_bottom).offset(20);
        make.width.equalTo(@(width));
        make.height.equalTo(@38);
        make.left.equalTo(self.mas_centerX).offset(15);
        make.bottom.equalTo(self).offset(-20);
    }];
    [self.historyPrice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_centerX).offset(-15);
        make.top.bottom.width.equalTo(self.buyGoldButton);
    }];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = COLOR_WHITE;
        [self addSubview:self.clockImageView];
        [self addSubview:self.goldPriceChatLabel];
        [self addSubview:self.goldPriceChatSubLabel];
        [self addSubview:self.goldPrice];
        [self addSubview:self.goldUnit];
        [self addSubview:self.normalPrice];
        [self addSubview:self.normalPriceValue];
        [self addSubview:self.lineOnPrice];
        [self addSubview:self.sepLine];
        [self addSubview:self.buyGoldButton];
        [self addSubview:self.historyPrice];
    }
    return self;
}
/**
 懒加载
 
 @return 购买按钮
 */
- (UIButton *)historyPrice {
    if (!_historyPrice) {
        _historyPrice = [[UIButton alloc] init];
        [_historyPrice setTitle:@"金价走势" forState:UIControlStateNormal];
        [_historyPrice setTitleColor:COLOR_MAIN forState:UIControlStateNormal];
        _historyPrice.titleLabel.font = FONT(19);
        _historyPrice.backgroundColor = COLOR_WHITE;
        _historyPrice.layer.cornerRadius = 18.0f;
        [_historyPrice addTarget:self action:@selector(historyPriceDidTap) forControlEvents:UIControlEventTouchUpInside];
        _historyPrice.layer.borderWidth = 1;
        _historyPrice.layer.borderColor = COLOR_MAIN.CGColor;
    }
    return _historyPrice;
}
/**
 懒加载
 
 @return 购买按钮
 */
- (UIButton *)buyGoldButton {
    if (!_buyGoldButton) {
        _buyGoldButton = [[UIButton alloc] init];
        [_buyGoldButton setTitle:@"立即购买" forState:UIControlStateNormal];
        [_buyGoldButton setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
        _buyGoldButton.titleLabel.font = FONT(19);
        _buyGoldButton.backgroundColor = COLOR_MAIN;
        _buyGoldButton.layer.cornerRadius = 18.0f;
        [_buyGoldButton addTarget:self action:@selector(buyGoldButtonDidTap) forControlEvents:UIControlEventTouchUpInside];
    }
    return _buyGoldButton;
}

/**
 懒加载
 
 @return sepLine
 */
- (UIView *)sepLine {
    if (!_sepLine) {
        _sepLine = [[UIView alloc] init];
        _sepLine.backgroundColor = UIColorFromHex(0xc9caca);
    }
    return _sepLine;
}
/**
 懒加载
 
 @return line
 */
- (UIView *)lineOnPrice {
    if (!_lineOnPrice) {
        _lineOnPrice = [[UIView alloc] init];
        _lineOnPrice.backgroundColor = RGBA(153, 153, 153, 1);
    }
    return _lineOnPrice;
}
/**
 懒加载
 
 @return 市场价格
 */
- (UILabel *)normalPriceValue {
    if (!_normalPriceValue) {
        _normalPriceValue = [[UILabel alloc] init];
        _normalPriceValue.textColor = RGBA(153, 153, 153, 1);
        _normalPriceValue.text = @"310.00元/克";
        _normalPriceValue.font = FONT_SUBTEXT;
    }
    return _normalPriceValue;
}
/**
 懒加载
 
 @return 市场价
 */
- (UILabel *)normalPrice {
    if (!_normalPrice) {
        _normalPrice = [[UILabel alloc] init];
        _normalPrice.text = @"市场价:";
        _normalPrice.textColor = RGBA(153, 153, 153, 1);
        _normalPrice.font = FONT_SUBTEXT;
    }
    return _normalPrice;
}
/**
 懒加载
 
 @return 元
 */
- (UILabel *)goldUnit {
    if (!_goldUnit) {
        _goldUnit = [[UILabel alloc] init];
        _goldUnit.textColor = COLOR_MAIN;
        _goldUnit.text = @"元/克";
        _goldUnit.font = FONT_TITLE;
    }
    return _goldUnit;
}
/**
 懒加载
 
 @return 金价
 */
- (CWCalendarLabel *)goldPrice {
    if (!_goldPrice) {
        _goldPrice = [[CWCalendarLabel alloc] init];
        _goldPrice.font = FONT_BOLD(31);
        _goldPrice.textColor = COLOR_MAIN;
        _goldPrice.text = @"";
    }
    return _goldPrice;
}
/**
 懒加载
 
 @return ...
 */
- (UILabel *)goldPriceChatSubLabel {
    if (!_goldPriceChatSubLabel) {
        _goldPriceChatSubLabel = [[UILabel alloc] init];
        _goldPriceChatSubLabel.font = FONT_SUBTEXT;
        _goldPriceChatSubLabel.textColor = RGBA(153, 153, 153, 1);
        _goldPriceChatSubLabel.text = @"参考上海黄金交易所 / 国际金价";
    }
    return _goldPriceChatSubLabel;
}
/**
 懒加载
 
 @return 查看实时金价走势
 */
- (UILabel *)goldPriceChatLabel {
    if (!_goldPriceChatLabel) {
        _goldPriceChatLabel = [[UILabel alloc] init];
        _goldPriceChatLabel.font = FONT_TITLE;
        _goldPriceChatLabel.textColor = COLOR_TEXT;
        _goldPriceChatLabel.text = @"实时金价";
    }
    return _goldPriceChatLabel;
}
/**
 懒加载
 
 @return clock
 */
- (UIImageView *)clockImageView {
    if (!_clockImageView) {
        _clockImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home_shishijinjia"]];
    }
    return _clockImageView;
}
@end
