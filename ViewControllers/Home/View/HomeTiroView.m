//
//  HomeTiroView.m
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/9/22.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "HomeTiroView.h"
#import "TiroGoodsView.h"
#import "NewTiroGoodsView.h"
#import "AllListModel.h"
@interface HomeTiroView ()
/** 礼包 */
@property(nonatomic, strong) UIImageView *tiroImageView;
/** 新手见面礼 */
@property(nonatomic, strong) UILabel *tiroLabel;
/** 新手专享 */
@property(nonatomic, strong) NewTiroGoodsView *otherGoodsView;
/** 新手特权金 */
@property(nonatomic, strong) NewTiroGoodsView *goldGoodsView;

@end

@implementation HomeTiroView

- (void)setProductListTiro:(NSArray<AllListModel *> *)productListTiro {
    _productListTiro = productListTiro;
    if (productListTiro.count > 1) {
        self.otherGoodsView.goodsData = productListTiro[0];
        self.goldGoodsView.goodsData = productListTiro[1];
    }
}
- (void)layoutSubviews {
    [super layoutSubviews];
    [self.tiroImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.tiroLabel);
        make.left.equalTo(self).offset(16);
    }];
    [self.tiroLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(15);
        make.left.equalTo(self.tiroImageView.mas_right).offset(6);
    }];
    
    [self.otherGoodsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tiroImageView.mas_bottom).offset(15);
        make.left.equalTo(self).offset(16);
    }];
    [self.goldGoodsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.width.equalTo(self.otherGoodsView);
        make.left.equalTo(self.otherGoodsView.mas_right).offset(6);
        make.right.equalTo(self).offset(-16);
        make.bottom.equalTo(self).offset(-20);
    }];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = COLOR_WHITE;
        [self addSubview:self.tiroLabel];
        [self addSubview:self.otherGoodsView];
        [self addSubview:self.goldGoodsView];
        [self addSubview:self.tiroImageView];
    }
    return self;
}

/**
 懒加载
 
 @return 特价黄金
 */
- (NewTiroGoodsView *)goldGoodsView {
    if (!_goldGoodsView) {
        _goldGoodsView = [[NewTiroGoodsView alloc] init];
        _goldGoodsView.goodsType = NewHomeTiroGoodsTypeGold;
    }
    return _goldGoodsView;
}

/**
 懒加载
 
 @return 非黄金
 */
- (NewTiroGoodsView *)otherGoodsView {
    if (!_otherGoodsView) {
        _otherGoodsView = [[NewTiroGoodsView alloc] init];
        _otherGoodsView.goodsType = NewHomeTiroGoodsTypeOther;
    }
    return _otherGoodsView;
}

/**
 懒加载
 
 @return 新手见面礼
 */
- (UILabel *)tiroLabel {
    if (!_tiroLabel) {
        _tiroLabel = [[UILabel alloc] init];
        _tiroLabel.font = FONT(17);
        _tiroLabel.text = @"新手大礼包";
        _tiroLabel.textColor = COLOR_TEXT;
    }
    return _tiroLabel;
}
/**
 懒加载
 
 @return 礼包
 */
- (UIImageView *)tiroImageView {
    if (!_tiroImageView) {
        _tiroImageView = [[UIImageView alloc] init];
        _tiroImageView.image = [UIImage imageNamed:@"home_dalibao"];
    }
    return _tiroImageView;
}

@end

