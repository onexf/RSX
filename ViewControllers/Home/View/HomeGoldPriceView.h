//
//  HomeGoldPriceView.h
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/9/6.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeGoldPriceView : UIView

/** 实时金价 */
@property(nonatomic, copy) NSString *currentGoldPrice;


@end
