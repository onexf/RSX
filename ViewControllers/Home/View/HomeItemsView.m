//
//  HomeItemsView.m
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/8/25.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "HomeItemsView.h"
#import "HomeItemButton.h"
@interface HomeItemsView ()
/** 邀请现金奖 */
@property(nonatomic, strong) HomeItemButton *inviteButton;
/** 天天挖金矿 */
@property(nonatomic, strong) HomeItemButton *digGoldButton;
/** 黄金商城 */
@property(nonatomic, strong) HomeItemButton *goldMall;
/** 黄金商铺 */
@property(nonatomic, strong) HomeItemButton *goldShops;

@end


@implementation HomeItemsView


- (void)actionWithButton:(UIButton *)button {
    if (self.actionDelegate && [self.actionDelegate respondsToSelector:@selector(actionWithItemSting:)]) {
        [self.actionDelegate actionWithItemSting:button.titleLabel.text];
    }
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = COLOR_WHITE;
        [self addSubview:self.inviteButton];
        [self addSubview:self.digGoldButton];
        [self addSubview:self.goldMall];
        [self addSubview:self.goldShops];
    }
    return self;
}


- (void)layoutSubviews {
    [super layoutSubviews];
    [self.inviteButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(17);
        make.left.bottom.equalTo(self);
        make.width.equalTo(@(SCREEN_WIDTH / 4));
    }];
    [self.digGoldButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.width.equalTo(self.inviteButton);
        make.left.equalTo(self.inviteButton.mas_right);
    }];
    [self.goldMall mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.width.equalTo(self.digGoldButton);
        make.left.equalTo(self.digGoldButton.mas_right);
    }];
    [self.goldShops mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.width.equalTo(self.goldMall);
        make.left.equalTo(self.goldMall.mas_right);
    }];
}

/**
 懒加载
 
 @return 商铺
 */
- (HomeItemButton *)goldShops {
    if (!_goldShops) {
        _goldShops = [[HomeItemButton alloc] init];
        [_goldShops setImage:[UIImage imageNamed:@"home_icon_activity"] forState:UIControlStateNormal];
        [_goldShops setTitle:@"实物提金" forState:UIControlStateNormal];
        [_goldShops addTarget:self action:@selector(actionWithButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _goldShops;
}

/**
 懒加载
 
 @return 商城
 */
- (HomeItemButton *)goldMall {
    if (!_goldMall) {
        _goldMall = [[HomeItemButton alloc] init];
        [_goldMall setImage:[UIImage imageNamed:@"home_icon_wajinkuang"] forState:UIControlStateNormal];
        [_goldMall setTitle:@"存金生息" forState:UIControlStateNormal];
        [_goldMall addTarget:self action:@selector(actionWithButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _goldMall;
}

/**
 懒加载
 
 @return 挖矿
 */
- (HomeItemButton *)digGoldButton {
    if (!_digGoldButton) {
        _digGoldButton = [[HomeItemButton alloc] init];
        [_digGoldButton setImage:[UIImage imageNamed:@"home_digItem"] forState:UIControlStateNormal];
        [_digGoldButton setTitle:@"卖出黄金" forState:UIControlStateNormal];
        [_digGoldButton addTarget:self action:@selector(actionWithButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _digGoldButton;
}


/**
 懒加载
 
 @return 邀请
 */
- (HomeItemButton *)inviteButton {
    if (!_inviteButton) {
        _inviteButton = [[HomeItemButton alloc] init];
        [_inviteButton setImage:[UIImage imageNamed:@"home_inviteItem"] forState:UIControlStateNormal];
        [_inviteButton setTitle:@"买入黄金" forState:UIControlStateNormal];
        [_inviteButton addTarget:self action:@selector(actionWithButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _inviteButton;
}


@end
