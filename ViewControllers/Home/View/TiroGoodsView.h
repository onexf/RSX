//
//  TiroGoodsView.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/6.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AllListModel;
typedef NS_ENUM(NSInteger, HomeTiroGoodsType) {
    HomeTiroGoodsTypeOther = 0,//非黄金专享
    HomeTiroGoodsTypeGold,//特价黄金
};
@interface TiroGoodsView : UIView

/** 类型 */
@property(nonatomic ,assign) HomeTiroGoodsType goodsType;

/** 数据 */
@property(nonatomic, strong) AllListModel *goodsData;
@end
