//
//  TypeButton.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/10/30.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "TypeButton.h"

@implementation TypeButton

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setTitleColor:COLOR_TEXT forState:UIControlStateNormal];
        [self setTitleColor:COLOR_Red forState:UIControlStateDisabled];
        [self setImage:[UIImage jk_imageWithColor:COLOR_Red] forState:UIControlStateDisabled];
        [self setImage:[UIImage jk_imageWithColor:[UIColor clearColor]] forState:UIControlStateNormal];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;

    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    //设置label的尺寸
    self.titleLabel.ct_x = 0;
    self.titleLabel.ct_y = 0;
    self.titleLabel.ct_width = self.ct_width;
    self.titleLabel.ct_height = self.ct_height - 1;

    //设置图片的尺寸
    self.imageView.ct_x = 0;
    self.imageView.ct_y = self.titleLabel.ct_height;
    self.imageView.ct_width = self.ct_width;
    self.imageView.ct_height = 1;
    
}

@end
