//
//  ActivityView.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/11/1.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ClickGoNextBlock)();

@interface ActivityView : UIView
/** 广告图片 */
@property(strong, nonatomic) UIImageView *adImageView;
/** title */
@property(nonatomic, copy) NSString *title;


/** 点击去看看按钮之后的回调 */
@property(copy, nonatomic) ClickGoNextBlock goNextBlock;

/**
 弹出广告插件
 
 @param animated 是否需要动画
 */
- (void)popADWithAnimated:(BOOL)animated;

@end
