//
//  HomeShopView.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/11/30.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>
@class GoldMarketModel;

@interface HomeShopView : UIView
@property(nonatomic, strong) GoldMarketModel *model;

@end
