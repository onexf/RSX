//
//  HomeTiroView.h
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/9/22.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AllListModel;
@interface HomeTiroView : UIView

/** 新手见面礼 */
@property(nonatomic, strong) NSArray<AllListModel *> *productListTiro;

@end
