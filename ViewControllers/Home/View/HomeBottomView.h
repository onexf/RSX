//
//  HomeBottomView.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/6.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JDFlipNumberView.h"

@interface HomeBottomView : UIView

/** 克数 */
@property(nonatomic, copy) NSString *currentGoldGram;
/** 管理的黄金 */
@property(nonatomic, strong) JDFlipNumberView *flipView;


@end
