//
//  NewTiroGoodsView.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/11/16.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "NewTiroGoodsView.h"
#import "AllListModel.h"
#import "UIView+Extension.h"
#import "CBWebViewController.h"
@interface NewTiroGoodsView ()
/** 黄金图片 */
@property(nonatomic, strong) UIImageView *goldImageView;
/** 价格、收益 */
@property(nonatomic, strong) UILabel *priceLabel;
/** unit */
@property(nonatomic, strong) UILabel *unitLabel;
/** subText */
@property(nonatomic, strong) UILabel *subText;
/** name */
@property(nonatomic, strong) UIButton *nameLabel;

@end

@implementation NewTiroGoodsView


- (void)productDetails {
    if (self.goodsData) {
        CBWebViewController *vc=[[CBWebViewController alloc] init];
        vc.urlString = self.goodsData.h5URL;
        vc.productID = self.goodsData.ID;
        if ([self.goodsData.riskLevel isEqualToString:@"不显示"]) {
            vc.isDisplayNativeBtn=NO;
        } else {
            vc.isDisplayNativeBtn=YES;
        }
        [self.jk_viewController.navigationController pushViewController:vc animated:YES];
    }
}
- (void)setGoodsData:(AllListModel *)goodsData {
    _goodsData = goodsData;
    self.subText.text = goodsData.smallTitle2;
    [self.nameLabel setTitle:goodsData.productName forState:UIControlStateNormal];
    if ([goodsData.isGoldProduct isEqualToString:@"1"]) {
        self.priceLabel.text = goodsData.realTimeGoldPrice;
        self.unitLabel.text = @"元/克";
        self.goldImageView.image = [UIImage imageNamed:@"home_Bullion"];
    } else {
        self.priceLabel.text = goodsData.profitRatio;
        self.unitLabel.text = @"%";
        self.goldImageView.image = [UIImage imageNamed:@"home_zhuanxiang"];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.goldImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(21);
        make.left.equalTo(self).offset(12);
        make.height.width.equalTo(@48);
    }];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.goldImageView.mas_centerY).offset(4);
        make.left.equalTo(self.goldImageView.mas_right).offset(6);
    }];
    [self.unitLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.priceLabel).offset(-4);
        make.left.equalTo(self.priceLabel.mas_right).offset(3);
    }];
    [self.subText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.priceLabel.mas_bottom).offset(4);
        make.left.equalTo(self.priceLabel);
        make.right.equalTo(self);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goldImageView.mas_bottom).offset(33);
        make.centerX.equalTo(self);
        make.bottom.equalTo(self).offset(-17);
        make.width.equalTo(@79);
        make.height.equalTo(@26);
    }];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.layer.cornerRadius = 7;
        self.backgroundColor = RGBA(248, 228, 208, 1);
        [self addTapAction:@selector(productDetails) target:self];
        [self addSubview:self.goldImageView];
        [self addSubview:self.priceLabel];
        [self addSubview:self.unitLabel];
        [self addSubview:self.subText];
        [self addSubview:self.nameLabel];
    }
    return self;
}

/**
 懒加载
 
 @return name
 */
- (UIButton *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UIButton alloc] init];
        _nameLabel.titleLabel.font = FONT_TITLE;
        [_nameLabel setTitleColor:COLOR_SUBTEXT forState:UIControlStateNormal];
        _nameLabel.userInteractionEnabled = NO;
        _nameLabel.layer.cornerRadius = 7;
        _nameLabel.layer.borderColor = COLOR_SUBTEXT.CGColor;
        _nameLabel.layer.borderWidth = 0.5;
    }
    return _nameLabel;
}

/**
 懒加载
 
 @return 黄金价格
 */
- (UILabel *)subText {
    if (!_subText) {
        _subText = [[UILabel alloc] init];
        _subText.font = FONT_SUBTEXT;
        _subText.textColor = COLOR_SUBTEXT;
        _subText.text = @"预期年化收益率";
        _subText.numberOfLines = 0;
    }
    return _subText;
}

/**
 懒加载
 
 @return 单位
 */
- (UILabel *)unitLabel {
    if (!_unitLabel) {
        _unitLabel = [[UILabel alloc] init];
        _unitLabel.text = @"元/克";
        _unitLabel.font = FONT_SUBTEXT;
    }
    return _unitLabel;
}

/**
 懒加载
 
 @return 价格
 */
- (UILabel *)priceLabel {
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc] init];
        _priceLabel.textColor = COLOR_TEXT;
        _priceLabel.font = FONT(22);
        _priceLabel.text = @"249";
    }
    return _priceLabel;
}
/**
 懒加载
 
 @return 黄金
 */
- (UIImageView *)goldImageView {
    if (!_goldImageView) {
        _goldImageView = [[UIImageView alloc] init];
        _goldImageView.image = [UIImage imageNamed:@"home_Bullion"];
    }
    return _goldImageView;
}

@end
