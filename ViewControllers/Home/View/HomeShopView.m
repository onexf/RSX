//
//  HomeShopView.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/11/30.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "HomeShopView.h"
#import "UIView+Extension.h"
#import "GoldMarketModel.h"
@interface HomeShopView ()

/** name */
@property(nonatomic, strong) UILabel *nameLabel;
/** 类型图标 */
@property(nonatomic, strong) UIImageView *iconicImageView;
/** 竖线 */
@property(nonatomic, strong) UIView *horLine;
/** 图片 */
@property(nonatomic, strong) UIImageView *leftImageView;
/** 店名 */
@property(nonatomic, strong) UILabel *shopNameLabel;
/** 存 */
@property(nonatomic, strong) UIButton *saveButton;
/** 取 */
@property(nonatomic, strong) UIButton *getButton;
/** 地址 */
@property(nonatomic, strong) UILabel *addressLabel;
/** 电话 */
@property(nonatomic, strong) UIButton *phoneButton;

@end


@implementation HomeShopView

- (void)setModel:(GoldMarketModel *)model {
    _model = model;
    [self.leftImageView sd_setImageWithURL:[NSURL URLWithString:model.banner] placeholderImage:[UIImage imageNamed:@"shop_shopImage"]];
    self.shopNameLabel.text = model.name;
    if (model.isSupportPull > 0 && model.isSupportStored > 0) {
        self.saveButton.hidden = NO;
        self.getButton.hidden = NO;
        [self.saveButton setTitle:@"存金" forState:UIControlStateNormal];
        [self.getButton setTitle:@"提金" forState:UIControlStateNormal];
    }
    if (model.isSupportPull > 0 && model.isSupportStored <= 0) {
        self.saveButton.hidden = NO;
        self.getButton.hidden = YES;
        [self.saveButton setTitle:@"存金" forState:UIControlStateNormal];
    }
    if (model.isSupportPull <= 0 && model.isSupportStored > 0) {
        self.saveButton.hidden = NO;
        self.getButton.hidden = YES;
        [self.saveButton setTitle:@"提金" forState:UIControlStateNormal];
    }
    if (model.isSupportPull <= 0 && model.isSupportStored <= 0) {
        self.saveButton.hidden = YES;
        self.getButton.hidden = YES;
    }
    self.addressLabel.text = [NSString stringWithFormat:@"%@%@%@", model.city, model.district, model.address];
}
- (void)phoneButtonDidTap:(UIButton *)button {
    NSMutableString *str=[[NSMutableString alloc] initWithFormat:@"tel:%@", self.model.phoneNumber];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.iconicImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(15);
        make.left.equalTo(self).offset(16);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.iconicImageView);
        make.left.equalTo(self.iconicImageView.mas_right).offset(6);
    }];
    [self.horLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.nameLabel.mas_bottom).offset(11);
        make.left.equalTo(self.iconicImageView);
        make.right.equalTo(self).offset(-16);
        make.height.equalTo(@1);
    }];
    
    [self.leftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.horLine.mas_bottom).offset(16);
        make.width.equalTo(@90);
        make.height.equalTo(@64.5);
        make.left.equalTo(self).offset(16);
        make.bottom.equalTo(self).offset(-16);
    }];
    [self.shopNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.leftImageView).offset(-3);
        make.left.equalTo(self.leftImageView.mas_right).offset(20);
    }];
    [self.saveButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.leftImageView);
        make.left.equalTo(self.leftImageView.mas_right).offset(20);
        make.height.equalTo(@20);
        make.width.equalTo(@34.5);
    }];
    [self.getButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.saveButton);
        make.left.equalTo(self.saveButton.mas_right).offset(7);
    }];
    [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.leftImageView.mas_right).offset(20);
        make.bottom.equalTo(self.leftImageView).offset(3);
    }];
    [self.phoneButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.leftImageView);
        make.right.equalTo(self).offset(-LEFT_RIGHT_MARGIN);
        make.width.height.equalTo(@43);
    }];
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = COLOR_WHITE;
        [self addSubview:self.nameLabel];
        [self addSubview:self.iconicImageView];
        [self addSubview:self.horLine];
        [self addSubview:self.leftImageView];
        [self addSubview:self.shopNameLabel];
        [self addSubview:self.saveButton];
        [self addSubview:self.getButton];
        [self addSubview:self.addressLabel];
        [self addSubview:self.phoneButton];
    }
    return self;
}
/**
 懒加载
 
 @return PHONE
 */
- (UIButton *)phoneButton {
    if (!_phoneButton) {
        _phoneButton = [[UIButton alloc] init];
        [_phoneButton setImage:[UIImage imageNamed:@"shop_phone"] forState:UIControlStateNormal];
        _phoneButton.jk_touchAreaInsets = UIEdgeInsetsMake(20, 20, 20, 20);
        [_phoneButton addTarget:self action:@selector(phoneButtonDidTap:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _phoneButton;
}
/**
 懒加载
 
 @return address
 */
- (UILabel *)addressLabel {
    if (!_addressLabel) {
        _addressLabel = [[UILabel alloc] init];
        _addressLabel.textColor = COLOR_SUBTEXT;
        _addressLabel.font = FONT_SUBTEXT;
    }
    return _addressLabel;
}
/**
 懒加载
 
 @return get
 */
- (UIButton *)getButton {
    if (!_getButton) {
        _getButton = [[UIButton alloc] init];
        [_getButton setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
        [_getButton setTitle:@"提金" forState:UIControlStateNormal];
        _getButton.titleLabel.font = FONT(13);
        [_getButton setBackgroundImage:[UIImage imageNamed:@"shop_saveButton"] forState:UIControlStateNormal];
        _getButton.userInteractionEnabled = NO;
    }
    return _getButton;
}
/**
 懒加载
 
 @return save
 */
- (UIButton *)saveButton {
    if (!_saveButton) {
        _saveButton = [[UIButton alloc] init];
        [_saveButton setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
        [_saveButton setTitle:@"存金" forState:UIControlStateNormal];
        _saveButton.titleLabel.font = FONT(13);
        [_saveButton setBackgroundImage:[UIImage imageNamed:@"shop_saveButton"] forState:UIControlStateNormal];
        _saveButton.userInteractionEnabled = NO;
    }
    return _saveButton;
}
/**
 懒加载
 
 @return name
 */
- (UILabel *)shopNameLabel {
    if (!_shopNameLabel) {
        _shopNameLabel = [[UILabel alloc] init];
        _shopNameLabel.textColor = COLOR_TEXT;
        _shopNameLabel.font = FONT(15);
    }
    return _shopNameLabel;
}
/**
 懒加载
 
 @return leftImage
 */
- (UIImageView *)leftImageView {
    if (!_leftImageView) {
        _leftImageView = [[UIImageView alloc] init];
        _leftImageView.image = [UIImage imageNamed:@"shop_shopImage"];
    }
    return _leftImageView;
}

/**
 懒加载
 
 @return shuxian
 */
- (UIView *)horLine {
    if (!_horLine) {
        _horLine = [[UIView alloc] init];
        _horLine.backgroundColor = COLOR_LINE;
    }
    return _horLine;
}
/**
 懒加载
 
 @return image
 */
- (UIImageView *)iconicImageView {
    if (!_iconicImageView) {
        _iconicImageView = [[UIImageView alloc] init];
        _iconicImageView.image = [UIImage imageNamed:@"home_mx"];
    }
    return _iconicImageView;
}
/**
 懒加载
 
 @return 特价黄金
 */
- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.textColor = COLOR_TEXT;
        _nameLabel.font = FONT(17);
        _nameLabel.text = @"黄金商铺";
    }
    return _nameLabel;
}

@end
