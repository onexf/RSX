//
//  SDMessageView.h
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/9/5.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HomeNoticeModel;
@interface SDMessageView : UIView

/** 公告 */
@property(nonatomic, strong) HomeNoticeModel *noticeData;
@end
