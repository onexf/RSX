//
//  HomeGoodsView.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/6.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AllListModel;
@interface HomeGoodsView : UIView

/** 产品数据 */
@property(nonatomic, strong) AllListModel *goodsData;

@end
