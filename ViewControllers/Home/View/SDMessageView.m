//
//  SDMessageView.m
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/9/5.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "SDMessageView.h"
#import "UIView+Extension.h"
#import "HomeNoticeModel.h"
#import "CBWebViewController.h"

@interface SDMessageView ()
/** 喇叭 */
@property(nonatomic, strong) UIImageView *imageView;
/** 文字 */
@property(nonatomic, strong) UILabel *textLabel;
/** 关闭 */
@property(nonatomic, strong) UIImageView *closeImageView;

@end

@implementation SDMessageView

- (void)setNoticeData:(HomeNoticeModel *)noticeData {
    _noticeData = noticeData;
    self.textLabel.text = noticeData.title;
}
- (void)textLabelDidTap {
    if (self.noticeData.href.length > 0) {
        CBWebViewController *noticeDetail = [[CBWebViewController alloc] init];
        noticeDetail.showShareButton = NO;
        noticeDetail.urlString = self.noticeData.href;
        [[self jk_viewController].navigationController pushViewController:noticeDetail animated:YES];
    }
}
- (void)closeImageViewDidTap {
//    NSLog(@"关闭公告");
    self.hidden = YES;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(self).offset(LEFT_RIGHT_MARGIN);
    }];
    [self.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(self.imageView.mas_right).offset(7);
        make.width.equalTo(@(SCREEN_WIDTH - 70));
    }];
    [self.closeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.right.equalTo(self).offset(-LEFT_RIGHT_MARGIN);
    }];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = RGBA(0, 0, 0, 0.1);
        [self addSubview:self.imageView];
        [self addSubview:self.textLabel];
        [self addSubview:self.closeImageView];
    }
    return self;
}
/**
 懒加载
 
 @return 关闭
 */
- (UIImageView *)closeImageView {
    if (!_closeImageView) {
        _closeImageView = [[UIImageView alloc] init];
        _closeImageView.image = [UIImage imageNamed:@"home_close"];
        [_closeImageView addTapAction:@selector(closeImageViewDidTap) target:self];
    }
    return _closeImageView;
}
/**
 懒加载
 
 @return 文字
 */
- (UILabel *)textLabel {
    if (!_textLabel) {
        _textLabel = [[UILabel alloc] init];
        _textLabel.font = FONT_SUBTEXT;
        _textLabel.textColor = COLOR_WHITE;
        _textLabel.text = @"关于邀请好友活动规则升级公告";
        [_textLabel addTapAction:@selector(textLabelDidTap) target:self];
    }
    return _textLabel;
}
/**
 懒加载
 
 @return 喇叭
 */
- (UIImageView *)imageView {
    if (!_imageView) {
        _imageView = [[UIImageView alloc] init];
        _imageView.image = [UIImage imageNamed:@"home_gonggao"];
    }
    return _imageView;
}
@end
