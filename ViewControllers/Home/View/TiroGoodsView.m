//
//  TiroGoodsView.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/6.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "TiroGoodsView.h"
#import "AllListModel.h"
#import "UIView+Extension.h"
#import "CBWebViewController.h"
@interface TiroGoodsView ()
/** 黄金图片 */
@property(nonatomic, strong) UIImageView *goldImageView;
/** 价格、收益 */
@property(nonatomic, strong) UILabel *priceLabel;
/** unit */
@property(nonatomic, strong) UILabel *unitLabel;
/** subText */
@property(nonatomic, strong) UILabel *subText;
/** name */
@property(nonatomic, strong) UILabel *nameLabel;
/** arrow */
@property(nonatomic, strong) UIImageView *arrowImageView;

@end

@implementation TiroGoodsView

- (void)productDetails {
    if (self.goodsData) {
        CBWebViewController *vc=[[CBWebViewController alloc] init];
        vc.urlString = self.goodsData.h5URL;
        vc.productID = self.goodsData.ID;
        if ([self.goodsData.riskLevel isEqualToString:@"不显示"]) {
            vc.isDisplayNativeBtn=NO;
        } else {
            vc.isDisplayNativeBtn=YES;
        }
        [self.jk_viewController.navigationController pushViewController:vc animated:YES];
    }
}
- (void)setGoodsData:(AllListModel *)goodsData {
    _goodsData = goodsData;
    self.subText.text = goodsData.smallTitle2;
    self.nameLabel.text = goodsData.productName;
    if ([goodsData.isGoldProduct isEqualToString:@"1"]) {
        self.priceLabel.text = goodsData.realTimeGoldPrice;
        self.unitLabel.text = @"元/克";
    } else {
        self.priceLabel.text = goodsData.profitRatio;
        self.unitLabel.text = @"%";
    }
}

- (void)setGoodsType:(HomeTiroGoodsType)goodsType {
    _goodsType = goodsType;
    switch (goodsType) {
        case HomeTiroGoodsTypeOther:
        {
            self.nameLabel.text = @"新手专享";
            self.subText.text = @"预期年化收益率";
            self.priceLabel.text = @"15";
            self.unitLabel.text = @"%";
        }
            break;
            
        default:
        {
            self.nameLabel.text = @"新手特权金";
            self.subText.text = @"超低价格，限时抢购";
            self.priceLabel.text = @"";
            self.unitLabel.text = @"元/克";
        }
            break;
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.goldImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(self).offset(LEFT_RIGHT_MARGIN);
    }];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.mas_centerY).offset(-3);
        make.left.equalTo(self.goldImageView.mas_right).offset(LEFT_RIGHT_MARGIN);
    }];
    [self.unitLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.priceLabel).offset(-4);
        make.left.equalTo(self.priceLabel.mas_right).offset(3);
    }];
    [self.subText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_centerY).offset(3);
        make.left.equalTo(self.priceLabel);
    }];
    
    [self.arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.right.equalTo(self).offset(-LEFT_RIGHT_MARGIN);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.right.equalTo(self.arrowImageView.mas_left).offset(-5);
    }];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = COLOR_WHITE;
        [self addTapAction:@selector(productDetails) target:self];
        [self addSubview:self.goldImageView];
        [self addSubview:self.priceLabel];
        [self addSubview:self.unitLabel];
        [self addSubview:self.subText];
        [self addSubview:self.nameLabel];
        [self addSubview:self.arrowImageView];
    }
    return self;
}

/**
 懒加载
 
 @return 箭头
 */
- (UIImageView *)arrowImageView {
    if (!_arrowImageView) {
        _arrowImageView = [[UIImageView alloc] init];
        _arrowImageView.image = [UIImage imageNamed:@"home_More"];
    }
    return _arrowImageView;
}

/**
 懒加载
 
 @return name
 */
- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.font = FONT_BUTTON_TEXT;
        _nameLabel.textColor = COLOR_TEXT;
        _nameLabel.text = @"新手专享";
    }
    return _nameLabel;
}

/**
 懒加载
 
 @return 黄金价格
 */
- (UILabel *)subText {
    if (!_subText) {
        _subText = [[UILabel alloc] init];
        _subText.font = FONT_SUBTEXT;
        _subText.textColor = COLOR_SUBTEXT;
        _subText.text = @"预期年化收益率";
    }
    return _subText;
}

/**
 懒加载
 
 @return 单位
 */
- (UILabel *)unitLabel {
    if (!_unitLabel) {
        _unitLabel = [[UILabel alloc] init];
        _unitLabel.text = @"元/克";
        _unitLabel.font = FONT_SUBTEXT;
    }
    return _unitLabel;
}

/**
 懒加载
 
 @return 价格
 */
- (UILabel *)priceLabel {
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc] init];
        _priceLabel.textColor = COLOR_TEXT;
        _priceLabel.font = FONT(24);
        _priceLabel.text = @"249";
    }
    return _priceLabel;
}
/**
 懒加载
 
 @return 黄金
 */
- (UIImageView *)goldImageView {
    if (!_goldImageView) {
        _goldImageView = [[UIImageView alloc] init];
        _goldImageView.image = [UIImage imageNamed:@"home_Bullion"];
    }
    return _goldImageView;
}

@end
