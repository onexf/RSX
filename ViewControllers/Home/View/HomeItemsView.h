//
//  HomeItemsView.h
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/8/25.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HomeItemActionDelegate <NSObject>

- (void)actionWithItemSting:(NSString *)string;

@end


@interface HomeItemsView : UIView

@property (nonatomic, weak) id <HomeItemActionDelegate> actionDelegate;

@end
