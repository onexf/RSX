//
//  HomeBottomView.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/6.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "HomeBottomView.h"

@interface HomeBottomView ()<JDFlipNumberViewDelegate>

/** 累计为用户管理黄金 */
@property(nonatomic, strong) UILabel *titleLabel;
/** 市场有风险 投资需谨慎 */
@property(nonatomic, strong) UILabel *subTitle;
/** 克 */
@property(nonatomic, strong) UILabel *unitLabel;
//上次的克重
@property(nonatomic, copy) NSString *lastGoldGram;


@end

@implementation HomeBottomView

- (void)setCurrentGoldGram:(NSString *)currentGoldGram {
    _currentGoldGram = currentGoldGram;
    self.flipView.value = self.lastGoldGram.integerValue;
    __weak typeof(self) weakSelf = self;
    [self.flipView animateToValue:currentGoldGram.integerValue duration:2 completion:^(BOOL finished) {
        weakSelf.lastGoldGram = currentGoldGram;
    }];
}
- (void)layoutSubviews {
    [super layoutSubviews];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(19);
        make.centerX.equalTo(self);
    }];
    [self.subTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.bottom.equalTo(self).offset(-LEFT_RIGHT_MARGIN);
    }];
    [self.flipView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self);
        make.height.equalTo(@44);
        make.width.equalTo(@260);
    }];
    [self.unitLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(self.flipView.mas_right);
    }];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = COLOR_TBBACK;
        [self addSubview:self.titleLabel];
        [self addSubview:self.subTitle];
        [self addSubview:self.flipView];
        [self addSubview:self.unitLabel];
    }
    return self;
}

/**
 懒加载
 
 @return subTitle
 */
- (UILabel *)subTitle {
    if (!_subTitle) {
        _subTitle = [[UILabel alloc] init];
        _subTitle.text = @"市场有风险 投资需谨慎";
        _subTitle.font = FONT_SUBTEXT;
        _subTitle.textColor = COLOR_SUBTEXT;
    }
    return _subTitle;
}
/**
 懒加载
 
 @return title
 */
- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = @"累计为用户管理黄金";
        _titleLabel.font = FONT_TEXT;
        _titleLabel.textColor = COLOR_SUBTEXT;
    }
    return _titleLabel;
}
/**
 懒加载
 
 @return 克
 */
- (UILabel *)unitLabel {
    if (!_unitLabel) {
        _unitLabel = [[UILabel alloc] init];
        _unitLabel.text = @"克";
        _unitLabel.textColor = COLOR_TEXT;
        _unitLabel.font = FONT_BOLD(18);
    }
    return _unitLabel;
}

/**
 懒加载
 
 @return 黄金
 */
- (JDFlipNumberView *)flipView {
    if (!_flipView) {
        _flipView = [[JDFlipNumberView alloc] initWithDigitCount:8 imageBundleName:@"JDFlipNumberViewIOS6"];
        _flipView.delegate = self;
        _flipView.reverseFlippingDisabled = YES;
    }
    return _flipView;
}

@end
