//
//  HomeGoodsView.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/9/6.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "HomeGoodsView.h"
#import "AllListModel.h"
#import "UIView+Extension.h"
#import "CBWebViewController.h"
@interface HomeGoodsView ()
/** name */
@property(nonatomic, strong) UILabel *nameLabel;
/** 类型图标 */
@property(nonatomic, strong) UIImageView *iconicImageView;
/** 价格，收益率 */
@property(nonatomic, strong) UILabel *priceLabel;
/** 单位 */
@property(nonatomic, strong) UILabel *unitKLabel;
/** des */
@property(nonatomic, strong) UILabel *subLabel;

/** 立即购买 */
@property(nonatomic, strong) UILabel *buyNow;
/** 已售 */
@property(nonatomic, strong) UILabel *sellCount;
/** risk */
@property(nonatomic, strong) UILabel *riskLabel;
/** 竖线 */
@property(nonatomic, strong) UIView *horLine;

@end

@implementation HomeGoodsView

- (void)productDetail {
    if (self.goodsData) {
        CBWebViewController *vc=[[CBWebViewController alloc] init];
        vc.urlString = self.goodsData.h5URL;
        vc.productID = self.goodsData.ID;
        vc.isDisplayNativeBtn=YES;
        [self.jk_viewController.navigationController pushViewController:vc animated:YES];
    }
}

- (void)setGoodsData:(AllListModel *)goodsData {
    _goodsData = goodsData;
    self.nameLabel.text = goodsData.productName;
    self.subLabel.text = goodsData.smallTitle2;
    self.riskLabel.text = [NSString stringWithFormat:@" %@ ", goodsData.riskLevel];
    self.buyNow.text = [NSString stringWithFormat:@" %@天 ", goodsData.termOfInvestment];
    if ([goodsData.isGoldProduct isEqualToString:@"1"]) {//黄金
        self.priceLabel.text = goodsData.realTimeGoldPrice;
        self.iconicImageView.image = [UIImage imageNamed:@"home_tj"];
        self.unitKLabel.textColor = COLOR_Red;
        self.buyNow.textColor = COLOR_Red;
        self.sellCount.textColor = COLOR_Red;
        self.sellCount.text = [NSString stringWithFormat:@" %@克起购 ", goodsData.purchaseStartAmount];
    } else {
        self.priceLabel.text = goodsData.profitRatio;
        self.iconicImageView.image = [UIImage imageNamed:@"home_mx"];
        self.unitKLabel.text = @"%";
        self.buyNow.textColor = COLOR_Red;
        self.sellCount.textColor = COLOR_Red;
        NSString *start = goodsData.purchaseStartAmount.integerValue > 10000 ? [NSString stringWithFormat:@"%ld万", goodsData.purchaseStartAmount.integerValue / 10000] : goodsData.purchaseStartAmount;
        self.sellCount.text = [NSString stringWithFormat:@" %@元起购 ", start];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(30);
        make.left.equalTo(self).offset(LEFT_RIGHT_MARGIN);
    }];
    [self.unitKLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.priceLabel).offset(-4);
        make.left.equalTo(self.priceLabel.mas_right).offset(4);
    }];
    [self.subLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.priceLabel.mas_bottom).offset(9);
        make.left.equalTo(self.priceLabel);
    }];
    [self.iconicImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(-4);
        make.right.equalTo(self);
    }];
    [self.buyNow mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLabel);
        make.centerY.equalTo(self.subLabel);
        make.height.equalTo(@17);
    }];
    [self.sellCount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.height.equalTo(self.buyNow);
        make.left.equalTo(self.buyNow.mas_right).offset(5);
    }];
    [self.riskLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.height.equalTo(self.buyNow);
        make.left.equalTo(self.sellCount.mas_right).offset(5);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.priceLabel);
        make.left.equalTo(self.mas_centerX);
    }];
    [self.horLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.nameLabel);
        make.bottom.equalTo(self.buyNow);
        make.right.equalTo(self.nameLabel.mas_left).offset(-30);
        make.width.equalTo(@1);
    }];

}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addTapAction:@selector(productDetail) target:self];
        self.backgroundColor = COLOR_WHITE;
        [self addSubview:self.nameLabel];
        [self addSubview:self.iconicImageView];
        [self addSubview:self.priceLabel];
        [self addSubview:self.unitKLabel];
        [self addSubview:self.subLabel];
        [self addSubview:self.buyNow];
        [self addSubview:self.sellCount];
        [self addSubview:self.riskLabel];
        [self addSubview:self.horLine];
    }
    return self;
}
/**
 懒加载
 
 @return shuxian
 */
- (UIView *)horLine {
    if (!_horLine) {
        _horLine = [[UIView alloc] init];
        _horLine.backgroundColor = COLOR_LINE;
    }
    return _horLine;
}
/**
 懒加载
 
 @return fengxian
 */
- (UILabel *)riskLabel {
    if (!_riskLabel) {
        _riskLabel = [[UILabel alloc] init];
        _riskLabel.font = FONT_SUBTEXT;
        _riskLabel.textColor = COLOR_SUBTEXT;
        _riskLabel.text = @"";
        _riskLabel.layer.borderColor = COLOR_SUBTEXT.CGColor;
        _riskLabel.layer.borderWidth = 1;
    }
    return _riskLabel;
}
/**
 懒加载
 
 @return 已售
 */
- (UILabel *)sellCount {
    if (!_sellCount) {
        _sellCount = [[UILabel alloc] init];
        _sellCount.font = FONT_SUBTEXT;
        _sellCount.textColor = COLOR_Red;
        _sellCount.text = @"";
        _sellCount.layer.borderColor = COLOR_Red.CGColor;
        _sellCount.layer.borderWidth = 1;
    }
    return _sellCount;
}
/**
 懒加载
 
 @return 立即
 */
- (UILabel *)buyNow {
    if (!_buyNow) {
        _buyNow = [[UILabel alloc] init];
        _buyNow.text = @"";
        _buyNow.textColor = COLOR_Red;
        _buyNow.font = FONT_SUBTEXT;
        _buyNow.layer.borderColor = COLOR_Red.CGColor;
        _buyNow.layer.borderWidth = 1;
    }
    return _buyNow;
}
/**
 懒加载
 
 @return w文字
 */
- (UILabel *)subLabel {
    if (!_subLabel) {
        _subLabel = [[UILabel alloc] init];
        _subLabel.font = FONT_SUBTEXT;
        _subLabel.textColor = COLOR_SUBTEXT;
        _subLabel.text = @"";
    }
    return _subLabel;
}
/**
 懒加载
 
 @return 单位
 */
- (UILabel *)unitKLabel {
    if (!_unitKLabel) {
        _unitKLabel = [[UILabel alloc] init];
        _unitKLabel.textColor = COLOR_Red;
        _unitKLabel.text = @"元/克";
        _unitKLabel.font = FONT_SUBTEXT;
    }
    return _unitKLabel;
}
/**
 懒加载
 
 @return 价格
 */
- (UILabel *)priceLabel {
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc] init];
        _priceLabel.text = @"";
        _priceLabel.textColor = COLOR_Red;
        _priceLabel.font = FONT(25);
    }
    return _priceLabel;
}
/**
 懒加载
 
 @return image
 */
- (UIImageView *)iconicImageView {
    if (!_iconicImageView) {
        _iconicImageView = [[UIImageView alloc] init];
        _iconicImageView.image = [UIImage imageNamed:@"home_mx"];
    }
    return _iconicImageView;
}

/**
 懒加载
 
 @return 特价黄金
 */
- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.textColor = COLOR_TEXT;
        _nameLabel.font = FONT_TITLE;
        _nameLabel.text = @"";
    }
    return _nameLabel;
}

@end
