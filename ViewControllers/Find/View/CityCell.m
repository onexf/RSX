//
//  CityCell.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/11/29.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "CityCell.h"
@interface CityCell ()
/** label */
@property(nonatomic, strong) UILabel *cityNameLabel;

@end


@implementation CityCell

- (void)setCityName:(NSString *)cityName {
    _cityName = cityName;
    self.cityNameLabel.text = cityName;
}
- (void)layoutSubviews {
    [super layoutSubviews];
    [self.cityNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.contentView);
        make.width.equalTo(@63);
        make.height.equalTo(@25);
    }];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.cityNameLabel];
    }
    return self;
}
/**
 懒加载
 
 @return 城市名
 */
- (UILabel *)cityNameLabel {
    if (!_cityNameLabel) {
        _cityNameLabel = [[UILabel alloc] init];
        _cityNameLabel.backgroundColor = COLOR_MAIN;
        _cityNameLabel.layer.cornerRadius = 2;
        _cityNameLabel.clipsToBounds = YES;
        _cityNameLabel.textColor = COLOR_WHITE;
        _cityNameLabel.font = FONT_SUBTEXT;
        _cityNameLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _cityNameLabel;
}

+ (instancetype)cellWithTableview:(UITableView *)tableView {
    static NSString *identifier = @"CityCell";
    CityCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[CityCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.contentView.backgroundColor = COLOR_WHITE;
        cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    }
    return cell;
}

@end
