//
//  CityCell.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/11/29.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CityCell : UITableViewCell


+ (instancetype)cellWithTableview:(UITableView *)tableView;
/** cityName */
@property(nonatomic, strong) NSString *cityName;
@end
