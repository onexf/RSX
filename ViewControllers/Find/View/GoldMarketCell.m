//
//  GoldMarketCell.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/11/28.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "GoldMarketCell.h"
#import "GoldMarketModel.h"
@interface GoldMarketCell ()
/** 图片 */
@property(nonatomic, strong) UIImageView *leftImageView;
/** 店名 */
@property(nonatomic, strong) UILabel *nameLabel;
/** 存 */
@property(nonatomic, strong) UIButton *saveButton;
/** 取 */
@property(nonatomic, strong) UIButton *getButton;
/** 地址 */
@property(nonatomic, strong) UILabel *addressLabel;
/** 电话 */
@property(nonatomic, strong) UIButton *phoneButton;

@end

@implementation GoldMarketCell

- (void)setFrame:(CGRect)frame {
    frame.size.height -= 8;
    [super setFrame:frame];
}

- (void)phoneButtonDidTap:(UIButton *)button {
    NSMutableString *str=[[NSMutableString alloc] initWithFormat:@"tel:%@", self.model.phoneNumber];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}
- (void)setModel:(GoldMarketModel *)model {
    _model = model;
    [self.leftImageView sd_setImageWithURL:[NSURL URLWithString:model.banner] placeholderImage:[UIImage imageNamed:@"shop_shopImage"]];
    self.nameLabel.text = model.name;
    if (model.isSupportPull > 0 && model.isSupportStored > 0) {
        self.saveButton.hidden = NO;
        self.getButton.hidden = NO;
        [self.saveButton setTitle:@"存金" forState:UIControlStateNormal];
        [self.getButton setTitle:@"提金" forState:UIControlStateNormal];
    }
    if (model.isSupportPull > 0 && model.isSupportStored <= 0) {
        self.saveButton.hidden = NO;
        self.getButton.hidden = YES;
        [self.saveButton setTitle:@"存金" forState:UIControlStateNormal];
    }
    if (model.isSupportPull <= 0 && model.isSupportStored > 0) {
        self.saveButton.hidden = NO;
        self.getButton.hidden = YES;
        [self.saveButton setTitle:@"提金" forState:UIControlStateNormal];
    }
    if (model.isSupportPull <= 0 && model.isSupportStored <= 0) {
        self.saveButton.hidden = YES;
        self.getButton.hidden = YES;
    }
    self.addressLabel.text = [NSString stringWithFormat:@"%@%@%@", model.city, model.district, model.address];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.leftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.width.equalTo(@90);
        make.height.equalTo(@64.5);
        make.left.equalTo(self.contentView).offset(20);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.leftImageView).offset(-3);
        make.left.equalTo(self.leftImageView.mas_right).offset(20);
    }];
    [self.saveButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.leftImageView);
        make.left.equalTo(self.nameLabel);
        make.height.equalTo(@20);
        make.width.equalTo(@34.5);
    }];
    [self.getButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.saveButton);
        make.left.equalTo(self.saveButton.mas_right).offset(7);
    }];
    [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLabel);
        make.bottom.equalTo(self.leftImageView).offset(3);
    }];
    [self.phoneButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.contentView).offset(-LEFT_RIGHT_MARGIN);
        make.width.height.equalTo(@43);
    }];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.leftImageView];
        [self.contentView addSubview:self.nameLabel];
        [self.contentView addSubview:self.saveButton];
        [self.contentView addSubview:self.getButton];
        [self.contentView addSubview:self.addressLabel];
        [self.contentView addSubview:self.phoneButton];
    }
    return self;
}
/**
 懒加载
 
 @return PHONE
 */
- (UIButton *)phoneButton {
    if (!_phoneButton) {
        _phoneButton = [[UIButton alloc] init];
        [_phoneButton setImage:[UIImage imageNamed:@"shop_phone"] forState:UIControlStateNormal];
        _phoneButton.jk_touchAreaInsets = UIEdgeInsetsMake(20, 20, 20, 20);
        [_phoneButton addTarget:self action:@selector(phoneButtonDidTap:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _phoneButton;
}
/**
 懒加载
 
 @return address
 */
- (UILabel *)addressLabel {
    if (!_addressLabel) {
        _addressLabel = [[UILabel alloc] init];
        _addressLabel.textColor = COLOR_SUBTEXT;
        _addressLabel.font = FONT_SUBTEXT;
        _addressLabel.text = @"青岛市莱西市XXXXXXX";
    }
    return _addressLabel;
}
/**
 懒加载
 
 @return get
 */
- (UIButton *)getButton {
    if (!_getButton) {
        _getButton = [[UIButton alloc] init];
        [_getButton setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
        [_getButton setTitle:@"提金" forState:UIControlStateNormal];
        _getButton.titleLabel.font = FONT(13);
        [_getButton setBackgroundImage:[UIImage imageNamed:@"shop_saveButton"] forState:UIControlStateNormal];
        _getButton.userInteractionEnabled = NO;
    }
    return _getButton;
}
/**
 懒加载
 
 @return save
 */
- (UIButton *)saveButton {
    if (!_saveButton) {
        _saveButton = [[UIButton alloc] init];
        [_saveButton setTitleColor:COLOR_WHITE forState:UIControlStateNormal];
        [_saveButton setTitle:@"存金" forState:UIControlStateNormal];
        _saveButton.titleLabel.font = FONT(13);
        [_saveButton setBackgroundImage:[UIImage imageNamed:@"shop_saveButton"] forState:UIControlStateNormal];
        _saveButton.userInteractionEnabled = NO;
    }
    return _saveButton;
}
/**
 懒加载
 
 @return name
 */
- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.textColor = COLOR_TEXT;
        _nameLabel.font = FONT(15);
    }
    return _nameLabel;
}
/**
 懒加载
 
 @return leftImage
 */
- (UIImageView *)leftImageView {
    if (!_leftImageView) {
        _leftImageView = [[UIImageView alloc] init];
        _leftImageView.image = [UIImage imageNamed:@"shop_shopImage"];
    }
    return _leftImageView;
}
+ (instancetype)cellWithTableview:(UITableView *)tableView {
    static NSString *identifier = @"NewAllHomeCell";
    GoldMarketCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[GoldMarketCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.contentView.backgroundColor = COLOR_WHITE;
        cell.layer.cornerRadius = 4;
        cell.clipsToBounds = YES;
    }
    return cell;
}

@end
