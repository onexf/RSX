//
//  FailedView.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/11/29.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "FailedView.h"

@interface FailedView ()

/** 图片uiimage */
@property(nonatomic, strong) UIImageView *imageView;
/** des */
@property(nonatomic, strong) UILabel *desLabel;

@end

@implementation FailedView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = COLOR_TBBACK;
        [self addSubview:self.imageView];
        [self addSubview:self.desLabel];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.centerY.equalTo(self).offset(-80);
        make.width.equalTo(@104.5);
        make.height.equalTo(@100);
    }];
    [self.desLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.imageView.mas_bottom).offset(20);
        make.centerX.equalTo(self);
    }];
}
/**
 懒加载
 
 @return 文字
 */
- (UILabel *)desLabel {
    if (!_desLabel) {
        _desLabel = [[UILabel alloc] init];
        _desLabel.font = FONT_SUBTEXT;
        _desLabel.textColor = UIColorFromHex(0x666666);
        _desLabel.text = @"定位失败，请手动选择所在城市";
    }
    return _desLabel;
}
/**
 懒加载
 
 @return tupian
 */
- (UIImageView *)imageView {
    if (!_imageView) {
        _imageView = [[UIImageView alloc] init];
        _imageView.image = [UIImage imageNamed:@"shop_getLocaltion_failed"];
    }
    return _imageView;
}
@end
