//
//  CitysView.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/11/29.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "CityView.h"
#import "UIView+Extension.h"
#import "CityCell.h"

@interface CityView ()<UITableViewDelegate, UITableViewDataSource>
/** 城市列表 */
@property(nonatomic, strong) UITableView *tableView;


@end


@implementation CityView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = RGBA(77, 77, 77, 0.7);
//        [self addTapAction:@selector(dismissAction) target:self];
        [self addSubview:self.tableView];
    }
    return self;
}
- (void)dismissAction {
    if (self.disDelegate && [self.disDelegate respondsToSelector:@selector(cityViewDiss)]) {
        [self.disDelegate cityViewDiss];
    }
}
- (void)layoutSubviews {
    [super layoutSubviews];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.equalTo(self);
        make.width.equalTo(@85.5);
        make.height.equalTo(@336);
    }];
}
/**
 懒加载
 
 @return 城市列表
 */
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 48;
        _tableView.tableFooterView = [UIView new];
        _tableView.backgroundColor = [UIColor clearColor];
    }
    return _tableView;
}

#pragma mark - UITableView

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.disDelegate && [self.disDelegate respondsToSelector:@selector(didSelectedCityName:)]) {
        [self.disDelegate didSelectedCityName:self.cities[indexPath.row]];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.cities.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cityName = self.cities[indexPath.row];
    CityCell *cell = [CityCell cellWithTableview:tableView];
    cell.cityName = cityName;
    return cell;
}

@end
