//
//  CitysView.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/11/29.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CityViewDisDelegate <NSObject>

@optional
- (void)cityViewDiss;

@required
/**
 选择的城市名

 @param cityName cityName
 */
- (void)didSelectedCityName:(NSString *)cityName;

@end

@interface CityView : UIView

@property (nonatomic, weak) id <CityViewDisDelegate> disDelegate;

/** 城市 */
@property(nonatomic, strong) NSArray *cities;

@end
