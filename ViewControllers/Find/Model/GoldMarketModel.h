//
//  GoldMarketModel.h
//  GoldCatBank
//
//  Created by 阿七 on 2017/11/28.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GoldMarketModel : NSObject

/** address */
@property(nonatomic, copy) NSString *address;
/** banner */
@property(nonatomic, copy) NSString *banner;
/** city */
@property(nonatomic, copy) NSString *city;
/** district */
@property(nonatomic, copy) NSString *district;
/** id */
@property(nonatomic, assign) NSInteger id;
/** isSupportPull */
@property(nonatomic, assign) NSInteger isSupportPull;
/** isSupportStored */
@property(nonatomic, assign) NSInteger isSupportStored;
/** logo */
@property(nonatomic, copy) NSString *logo;
/** name */
@property(nonatomic, copy) NSString *name;
/** phoneNumber */
@property(nonatomic, copy) NSString *phoneNumber;
/** province */
@property(nonatomic, copy) NSString *province;

@end
