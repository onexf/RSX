//
//  CityListViewController.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/11/28.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "CityListViewController.h"

@interface CityListViewController ()

@end

@implementation CityListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = COLOR_TBBACK;
    self.title = @"选择城市";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
