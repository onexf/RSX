//
//  GoldMarketViewController.m
//  GoldCatBank
//
//  Created by 阿七 on 2017/11/28.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "GoldMarketViewController.h"

@interface GoldMarketViewController ()

@end

@implementation GoldMarketViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = COLOR_TBBACK;
    self.title = @"金店详情";

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
