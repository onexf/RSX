//
//  FindViewController.m
//  GoldCatBank
//
//  Created by 王鑫锋 on 2017/8/21.
//  Copyright © 2017年 王鑫锋. All rights reserved.
//

#import "FindViewController.h"
#import <FDFullscreenPopGesture/UINavigationController+FDFullscreenPopGesture.h>
#import "MJRefresh.h"
#import "TestViewController.h"

#import <INTULocationManager/INTULocationManager.h>
#import "UIView+Extension.h"
#import "CityListViewController.h"
#import "MJRefresh.h"
#import "GoldMarketViewController.h"
#import "GoldMarketCell.h"
#import "GoldMarketModel.h"
#import "FailedView.h"
#import "NetworkSingleton+Shop.h"
#import "CityView.h"
@interface FindViewController ()<UITableViewDelegate, UITableViewDataSource, CityViewDisDelegate>
/** 导航栏 */
@property(nonatomic, strong) UIView *titleView;
/** 标题 */
@property(nonatomic, strong) UILabel *titleLabel;
/** 地理位置 */
@property(nonatomic, strong) UIView *positionView;
/** 定位 */
@property(nonatomic, strong) UILabel *leftLabel;
/** 城市 */
@property(nonatomic, strong) UILabel *rightLabel;
/** 箭头 */
@property(nonatomic, strong) UIButton *arrowImage;
/** 定位状态 */
@property (assign) NSInteger locationStatus;
/** 定位坐标 */
@property (nonatomic, strong) CLLocation *location;
/** 位置编码 */
@property (nonatomic, strong) CLGeocoder *geocoder;

@property (assign) INTULocationRequestID requestID;
/** 定位精度 */
@property (assign, nonatomic) INTULocationAccuracy desiredAccuracy;
/** 列表 */
@property(nonatomic, strong) UITableView *tableView;
/** 定位失败 */
@property(nonatomic, strong) FailedView *requestLocationfailed;
/** citys */
@property(nonatomic, strong) CityView *cityView;
/** 金店 */
@property(nonatomic, strong) NSArray *shops;
/** 城市名 */
@property(nonatomic, copy) NSString *currentCityName;

@end

@implementation FindViewController

#pragma mark - 事件处理
- (void)didSelectedCityName:(NSString *)cityName {
    [self positionViewDidTap:self.arrowImage];
    self.currentCityName = cityName;
    self.leftLabel.text = @"当前城市";
    self.rightLabel.text = cityName;
    [self.tableView.mj_header beginRefreshing];
}

- (void)positionViewDidTap:(UIButton *)button {
    self.arrowImage.selected = !button.selected;
    if (button.selected) {
        [self.view addSubview:self.cityView];
        [UIView animateWithDuration:0.3 animations:^{
            self.cityView.frame = self.tableView.frame;
        }];
    } else {
        [UIView animateWithDuration:0.3 animations:^{
            self.cityView.frame = CGRectMake(SCREEN_WIDTH, self.tableView.ct_y , SCREEN_WIDTH, self.tableView.ct_height);
        } completion:^(BOOL finished) {
            [self.cityView removeFromSuperview];
        }];
    }
}
/**
 定位
 */
- (void)requestPosition {
    self.leftLabel.text = @"定位中···";
    INTULocationManager *manger = [INTULocationManager sharedInstance];
    __weak typeof(self) weakSelf = self;
    self.requestID =  [manger subscribeToLocationUpdatesWithBlock:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
        __typeof(weakSelf) strongSelf = weakSelf;
        strongSelf.locationStatus = status;
//        NSLog(@"定位信息：%@-------%ld", currentLocation, (long)status);
        if (status == INTULocationStatusSuccess) {
            [[INTULocationManager sharedInstance] forceCompleteLocationRequest:strongSelf.requestID];
            strongSelf.requestID = NSNotFound;

            strongSelf.location = currentLocation;
            CLGeocoder *geocoder = [[CLGeocoder alloc] init];
            [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
                for (CLPlacemark *placemark in placemarks) {
                    NSLog(@"%@ %@ %f %f", placemark.name, placemark.addressDictionary, placemark.location.coordinate.latitude, placemark.location.coordinate.longitude);
                    NSString *cityName = placemark.locality.length > 0 ? [placemark.locality stringByReplacingOccurrencesOfString:@"市" withString:@""] : placemark.name;
                    strongSelf.leftLabel.text = @"当前定位";
                    strongSelf.rightLabel.text = cityName;
                    //请求列表数据
                    strongSelf.currentCityName = cityName;
                    [strongSelf.tableView.mj_header beginRefreshing];
                }
            }];
        } else {
            strongSelf.leftLabel.text = @"定位失败";
            strongSelf.rightLabel.text = @"";
            //展示定位失败
            [strongSelf.view addSubview:strongSelf.requestLocationfailed];
        }
    }];
}

- (void)requestShopListWithDistrict:(NSString *)district {
    __weak typeof(self) weakSelf = self;
    [NetworkSingleton shop_getAllListWithDict:@{@"district" : district} Response:^(NSArray<GoldMarketModel *> *list, NSString *errMsg) {
        [weakSelf.requestLocationfailed removeFromSuperview];
        [weakSelf.tableView.mj_header endRefreshing];
        if (errMsg) {
            [WSProgressHUD showErrorWithStatus:errMsg];
        } else {
            weakSelf.shops = list;
        }
        [weakSelf.tableView reloadData];
    }];
}

- (void)getCityList {
    __weak typeof(self) weakSelf = self;
    [NetworkSingleton shop_getAllCityResponse:^(NSArray<NSString *> *list, NSString *errMsg) {
        weakSelf.cityView.cities = list;
    }];
}
#pragma mark - 初始化，布局
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    if (self.arrowImage.selected) {
        [self positionViewDidTap:self.arrowImage];
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self getCityList];
    
    self.locationStatus = 1;
    self.view.backgroundColor = COLOR_MAIN;
    self.fd_prefersNavigationBarHidden = YES;
    [self.view addSubview:self.tableView];
    
    [self.view addSubview:self.titleView];
    [self.view addSubview:self.positionView];
    
    [self setNavTitle];
    [self setPosition];
    [self requestPosition];
    
}
- (void)setPosition {
    [self.positionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@44);
    }];
    [self.leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.positionView);
        make.left.equalTo(self.positionView).offset(LEFT_RIGHT_MARGIN);
    }];
    [self.arrowImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.positionView);
        make.right.equalTo(self.positionView).offset(-LEFT_RIGHT_MARGIN);
    }];
    [self.rightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.positionView);
        make.right.equalTo(self.arrowImage.mas_left).offset(-4);
    }];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.positionView.mas_bottom);
        make.left.right.bottom.equalTo(self.view);
    }];
}

- (void)setNavTitle {
    [self.titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0, *)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.height.equalTo(@44);
        } else {
            make.top.equalTo(self.view);
            make.height.equalTo(@64);
        }
        make.left.right.equalTo(self.view);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.titleView);
        if (@available(iOS 11.0, *)) {
            make.centerY.equalTo(self.titleView);
        } else {
            make.centerY.equalTo(self.titleView).offset(10);
        }
    }];
}
#pragma mark - UItableView
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    GoldMarketViewController *goldMarketVC = [[GoldMarketViewController alloc] init];
//    [self.navigationController pushViewController:goldMarketVC animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.shops.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    GoldMarketCell *cell = [GoldMarketCell cellWithTableview:tableView];
    cell.model = self.shops[indexPath.row];
    return cell;
}
/**
 懒加载
 
 @return citys
 */
- (CityView *)cityView {
    if (!_cityView) {
        _cityView = [[CityView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH, self.tableView.ct_y , SCREEN_WIDTH, self.tableView.ct_height)];
        _cityView.disDelegate = self;
    }
    return _cityView;
}
/**
 懒加载
 
 @return requestLocationfailed
 */
- (FailedView *)requestLocationfailed {
    if (!_requestLocationfailed) {
        _requestLocationfailed = [[FailedView alloc] initWithFrame:self.tableView.frame];
    }
    return _requestLocationfailed;
}
/**
 懒加载
 
 @return shops
 */
- (NSArray *)shops {
    if (!_shops) {
        _shops = [[NSArray alloc] init];
    }
    return _shops;
}
/**
 懒加载
 
 @return 金店列表
 */
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.contentInset = UIEdgeInsetsMake(7, 0, 0, 0);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.rowHeight = 114;
        _tableView.backgroundColor = COLOR_TBBACK;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        __weak typeof(self) weakSelf = self;
        _tableView.mj_header = [MJDIYHeader headerWithRefreshingBlock:^{
            [weakSelf requestShopListWithDistrict:weakSelf.currentCityName];
        }];
    }
    return _tableView;
}
/**
 懒加载
 
 @return 城市
 */
- (UILabel *)rightLabel {
    if (!_rightLabel) {
        _rightLabel = [[UILabel alloc] init];
        _rightLabel.textColor = COLOR_TEXT;
        _rightLabel.font = FONT_SUBTEXT;
    }
    return _rightLabel;
}
/**
 懒加载
 
 @return 定位中
 */
- (UILabel *)leftLabel {
    if (!_leftLabel) {
        _leftLabel = [[UILabel alloc] init];
        _leftLabel.textColor = COLOR_TEXT;
        _leftLabel.font = FONT(15);
    }
    return _leftLabel;
}
/**
 懒加载
 
 @return 箭头
 */
- (UIButton *)arrowImage {
    if (!_arrowImage) {
        _arrowImage = [[UIButton alloc] init];
        [_arrowImage setImage:[UIImage imageNamed:@"shop_localtion_normal"] forState:UIControlStateNormal];
        [_arrowImage setImage:[UIImage imageNamed:@"shop_localtion_selected"] forState:UIControlStateSelected];
        _arrowImage.jk_touchAreaInsets = UIEdgeInsetsMake(20, 40, 20, 20);
        [_arrowImage addTarget:self action:@selector(positionViewDidTap:) forControlEvents:UIControlEventTouchUpInside];
        _arrowImage.timeInterval = 0.3;

    }
    return _arrowImage;
}
/**
 懒加载
 
 @return 定位
 */
- (UIView *)positionView {
    if (!_positionView) {
        _positionView = [[UIView alloc] init];
        _positionView.backgroundColor = COLOR_WHITE;
        [_positionView addSubview:self.leftLabel];
        [_positionView addSubview:self.rightLabel];
        [_positionView addSubview:self.arrowImage];
    }
    return _positionView;
}
/**
 懒加载
 
 @return 标题
 */
- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = FONT_NAV;
        _titleLabel.text = @"荣盛祥金店";
        _titleLabel.textColor = COLOR_WHITE;
    }
    return _titleLabel;
}
/**
 懒加载
 
 @return 导航栏
 */
- (UIView *)titleView {
    if (!_titleView) {
        _titleView = [[UIView alloc] init];
        _titleView.backgroundColor = COLOR_MAIN;
        [_titleView addSubview:self.titleLabel];
    }
    return _titleView;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
